﻿using Abp.AutoMapper;
using Abp.Domain.Services;
using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.MicroKernel.Registration;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.EntityFrameworkCore;

namespace ItGroup.Saex
{
    [DependsOn(
        typeof(SaexManagerModule))]
    public class SaexServiceModule : AbpModule
    {
        public override void Initialize()
        {
            var thisAssembly = typeof(SaexServiceModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            IocManager.IocContainer.Register(Classes.FromAssembly(thisAssembly).BasedOn<IDomainService>().LifestylePerThread().WithServiceSelf());
        }

        public override void PreInitialize()
        {
        }
    }
}
