﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using CsvHelper.Configuration.Attributes;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseImport : Entity<Guid>
    {
        private static string[] formats = { "dd/MM/yyyy", "dd/M/yyyy", "d/M/yyyy", "d/MM/yyyy",
                    "dd/MM/yy", "dd/M/yy", "d/M/yy", "d/MM/yy"};
        public CaseImport()
        {
        }

        public CaseBaseline ConvertToCaseBaseline()
        {
            var log = new CaseBaseline();
            log.AccountNumber = this.Cuenta;
            log.AccountNumber2 = this.Cuenta1;
            log.Name = this.Nombre;
            log.UploadDate = this.CreationTime;
            log.DueDate = this.Due;
            log.Nest = this.Nest;
            log.Product = this.Producto;
            log.DueBalance = this.AtrasoTotal;
            log.LC = this.LC;
            log.OriginalAmount = this.Balance;

            log.City = this.Pueblo;
            log.Employee = this.Empleado;
            log.Supervisor = this.Supervisor;
            log.Comments = this.Pandemia;

            log.DueDays = this.Atraso;
            log.Progress = this.Etapa;
            //log.CreationTime = this.CreationTime;
            return log;
        }

        public string Cuenta { get; set; }

        [Name("Cuenta 1")]
        public string Cuenta1 { get; set; }

        public string Nombre { get; set; }

        public int Atraso { get; set; }

        public string Etapa { get; set; }

        public DateTime Due { get; set; }

        public DateTime Nest { get; set; }

        [Name("Atraso $")]
        public decimal AtrasoTotal { get; set; }

        public decimal Balance { get; set; }

        public decimal LC { get; set; }

        public string Pueblo { get; set; }

        public string Producto { get; set; }

        public string Empleado { get; set; }

        public string Supervisor { get; set; }

        public string Pandemia { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
