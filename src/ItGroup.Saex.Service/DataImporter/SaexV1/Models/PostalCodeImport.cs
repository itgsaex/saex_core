﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    public class PostalCodeImport
    {
        public string codigoPostal { get; set; }

        public string codigoPostalID { get; set; }

        public string modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string nombreArea { get; set; }
    }
}
