﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemStateImport
    {
        public string Definicion { get; set; }

        public string EstadoSistema { get; set; }

        public string id { get; set; }

        public int MacroEstadoID { get; set; }

        public bool Promesa { get; set; }

        public int SubStatusID { get; set; }
    }
}
