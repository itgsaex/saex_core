﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.Service.DataImporter.SaexV1.Models
{
   public class RegionImport
    {
        public string regionID { get; set; }
        public string nombre { get; set; }
        public string id { get; set; }
        public string ParaReporte { get; set; }
    }
}
