﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities.V1
{
    public class Actividad
    {
        public Guid ActividadID { get; set; }

        public string ActivityCode { get; set; }

        public string AgenteID { get; set; }

        public string CasoID { get; set; }

        public string Comentario { get; set; }

        public string ContactoCon { get; set; }

        public int Estatus { get; set; }

        public DateTime? ExportDate { get; set; }

        public DateTime Fecha { get; set; }

        public double? Latitud { get; set; }

        public double? Longitud { get; set; }

        public string LugarVisitado { get; set; }

        public string RazonAtraso { get; set; }

        public int? Trans_Status { get; set; }

        public DateTime TransferDate { get; set; }
    }

    public class ActividadDetalle
    {
        public Guid ActividadID { get; set; }

        public string ActivityCode { get; set; }

        public string Comentario { get; set; }

        public string Parametros { get; set; }

        public string Pregunta { get; set; }

        public bool Requerido { get; set; }

        public string Resultado { get; set; }

        public int Tipo { get; set; }
    }

    public class ActividadLinea
    {
        public Guid ActividadID { get; set; }

        public string ActivityCode { get; set; }

        public string Comentario { get; set; }

        public string ContactoCon { get; set; }

        public DateTime? CreadoFechaHora { get; set; }

        public string LugarVisitado { get; set; }

        public string RazonAtraso { get; set; }

        public string ReactionCode { get; set; }

        public string ReasonCode { get; set; }
    }

    public class ActividadReactionReasonDetalle
    {
        public Guid ActividadID { get; set; }

        public string ActivityCode { get; set; }

        public string Comentario { get; set; }

        public string Parametros { get; set; }

        public string Pregunta { get; set; }

        public bool Requerido { get; set; }

        public string Resultado { get; set; }

        public int Tipo { get; set; }

        public short TipoDetalle { get; set; }
    }

    public class ActivityCodeV1
    {
        public string Activity_Code { get; set; }

        public bool Activo { get; set; }

        public string Descripcion { get; set; }

        public bool EsGlobal { get; set; }

        public string id { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public short Tipo { get; set; }

        public string TipoCaso { get; set; }
    }

    public class ActivityCodeDetalle
    {
        public string ActivityCode { get; set; }

        public short ActivityCodeType { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public int? Orden { get; set; }

        public string Parametros { get; set; }

        public string Pregunta { get; set; }

        public bool? Requerido { get; set; }

        public int Tipo { get; set; }

        public string uid { get; set; }

        public string ValorPorDefecto { get; set; }
    }

    public class ActivityCodeTypeV1
    {
        public short? CodeID { get; set; }

        public string Descripcion { get; set; }
    }

    public class AgenteCaso
    {
        public string AgenteID { get; set; }

        public string CasoID { get; set; }
    }

    public class AgenteCasoDetail
    {
        public string AgenteID { get; set; }

        public decimal? balanceBOM { get; set; }

        public string BSW { get; set; }

        public string CasoID { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? DPD_BOM { get; set; }

        public string etapasDeDelincuenciaBOM { get; set; }

        public string stateCodeBOM { get; set; }

        public DateTime stateEntryBOM { get; set; }
    }

    public class AgenteGps
    {
        public string AgenteID { get; set; }

        public DateTime FechaHoraCaptura { get; set; }

        public double Latitud { get; set; }

        public double Longitud { get; set; }

        public int TipoCaptura { get; set; }
    }

    public class AgenteGrupoCasos
    {
        public string AgenteID { get; set; }

        public string AgenteSourceID { get; set; }

        public DateTime Fecha { get; set; }

        public string GrupoCasosID { get; set; }

        public string GrupoCasosTableID { get; set; }
    }

    public class AgenteInternoPromesa
    {
        public string AgenteExterno { get; set; }

        public string AgenteInterno { get; set; }

        public string CasoID { get; set; }

        public DateTime? Fecha { get; set; }

        public string Promesa { get; set; }
    }

    public class Alerta
    {
        public string AlertaID { get; set; }

        public string Descripcion { get; set; }
    }

    public class AlertaCaso
    {
        public string AlertaID { get; set; }

        public string CasoID { get; set; }

        public string Comentario { get; set; }

        public DateTime? Fecha { get; set; }

        public int Origen { get; set; }
    }

    public class AuditTrail
    {
        public string DescripcionHumana { get; set; }

        public DateTime? Fecha { get; set; }

        public string Pantalla { get; set; }

        public decimal? TransactionID { get; set; }

        public int? TransactionTypeID { get; set; }

        public string Usuario { get; set; }
    }

    public class BSWBucket
    {
        public int? BucketValue { get; set; }

        public string id { get; set; }

        public int? MaxValue { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string ProductNumber { get; set; }
    }

    public class BSWCondition
    {
        public int? Condition { get; set; }

        public string id { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string ProductNumber { get; set; }

        public string Value { get; set; }
    }

    public class BSWRulePerProduct
    {
        public string id { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string ProductNumber { get; set; }

        public string RuleTable { get; set; }
    }

    public class BSWRuleTable
    {
        public string Descripcion { get; set; }

        public string RuleTable { get; set; }
    }

    public class BSWValue
    {
        public string Value { get; set; }
    }

    public class CaseFields
    {
        public string CaseTypeID { get; set; }

        public int CategoriaID { get; set; }

        public string ColumnName { get; set; }

        public int? downLoadOption { get; set; }

        public int FieldID { get; set; }

        public string HumanName { get; set; }

        public int Sequence { get; set; }

        public string SourceTargetType { get; set; }

        public string TargetColumn { get; set; }
    }

    public class Caso
    {
        public bool Activo { get; set; }

        public decimal Cargos { get; set; }

        public string CasoID { get; set; }

        public string Ciudad { get; set; }

        public string CiudadEstadoCliente { get; set; }

        public string Cobrador { get; set; }

        public DateTime? CobroDesdeHora { get; set; }

        public DateTime? CobroHastaHora { get; set; }

        public string Comentario { get; set; }

        public string Condicion { get; set; }

        public int? DiaCobro { get; set; }

        public int DiasDelFinDeMes { get; set; }

        public int DiasVencimiento { get; set; }

        public int? DiaVisita { get; set; }

        public string Direccion2 { get; set; }

        public string Direccion3 { get; set; }

        public string DireccionCliente1 { get; set; }

        public string DireccionCliente2 { get; set; }

        public string DireccionClienteZipCode { get; set; }

        public string DireccionPrincipal { get; set; }

        public string Email { get; set; }

        public string EstadoRiesgo { get; set; }

        public string Estatus { get; set; }

        public int? EstatusActividad { get; set; }

        public string EstatusAjustador { get; set; }

        public string EstatusSistema { get; set; }

        public string EtapasDeDelicuencia { get; set; }

        public DateTime FechaEntradaSAEx { get; set; }

        public string FechaNacimiento { get; set; }

        public string FechaNacimientoCodeudor { get; set; }

        public DateTime? FechaOrigen { get; set; }

        public DateTime? FechaPago { get; set; }

        public DateTime? FechaUltPago { get; set; }

        public string FechaVencimiento { get; set; }

        public int? Flag { get; set; }

        public int? Frecuencia { get; set; }

        public int? HoraCobro { get; set; }

        public int? HoraVisita { get; set; }

        public string IdentificacionID { get; set; }

        public decimal Interes { get; set; }

        public int? Jueves { get; set; }

        public double? Latitud { get; set; }

        public double? LatitudRuta { get; set; }

        public double? Longitud { get; set; }

        public double? LongitudRuta { get; set; }

        public string LugarCobro { get; set; }

        public string LugarVisita { get; set; }

        public int? Lunes { get; set; }

        public int? Martes { get; set; }

        public string MaturityDate { get; set; }

        public int? Miercoles { get; set; }

        public decimal MontoOriginal { get; set; }

        public decimal MontoPago { get; set; }

        public decimal? MontoVencido { get; set; }

        public string Nombre { get; set; }

        public string Nombre2 { get; set; }

        public string NumeroCuenta { get; set; }

        public bool Obligatorio { get; set; }

        public string PersonaContacto { get; set; }

        public int? Prioridad { get; set; }

        public string ProductNumber { get; set; }

        public string Pueblo { get; set; }

        public bool? Referido { get; set; }

        public int? Sabado { get; set; }

        public string SeguroSocial { get; set; }

        public string StateCode { get; set; }

        public string SubStatusSistema { get; set; }

        public string Sucursal { get; set; }

        public string Telefono2 { get; set; }

        public string Telefono3 { get; set; }

        public string TelefonoPrincipal { get; set; }

        public string Terminos { get; set; }

        public string TipoCuenta { get; set; }

        public int? Viernes { get; set; }

        public DateTime? VisitaDesdeHora { get; set; }

        public DateTime? VisitaHastaHora { get; set; }

        public string ZipCode { get; set; }
    }

    public class CasoSort
    {
        public string AgenteID { get; set; }

        public string CasoID { get; set; }

        public int SortIndex { get; set; }
    }

    public class CasosPlanificadosNoVisitados
    {
        public string AgenteID { get; set; }

        public string CasoID { get; set; }

        public DateTime? Fecha { get; set; }
    }

    public class Categoria
    {
        public int? CategoriaID { get; set; }

        public int? CategoriaSequence { get; set; }

        public string Descripcion { get; set; }
    }

    public class CodigoExcusa
    {
        public string Descripcion { get; set; }

        public string ExcusaID { get; set; }

        public string id { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public short Tipo { get; set; }
    }

    public class CodigoPostal
    {
        public string codigoPostal { get; set; }

        public string codigoPostalID { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string nombreArea { get; set; }
    }

    public class CondicionCaso
    {
        public string CondicionCasoID { get; set; }

        public string Descripcion { get; set; }
    }

    public class DiasDeCierre
    {
        public int? Dia { get; set; }

        public string id { get; set; }

        public int? Mes { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public int? Year { get; set; }
    }

    public class DiasFeriados
    {
        public int? Dia { get; set; }

        public string id { get; set; }

        public int? Mes { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public int? Year { get; set; }
    }

    public class EstadoRiesgo
    {
        public string Descripcion { get; set; }

        public int? EstadoRiesgoID { get; set; }

        public string Valor { get; set; }
    }

    public class EstadosAjustador
    {
        public string Definicion { get; set; }

        public string EstadoAjustador { get; set; }

        public string id { get; set; }

        public int? MacroEstadoID { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public bool? Promesa { get; set; }
    }

    public class EstadosSistema
    {
        public string Definicion { get; set; }

        public string EstadoSistema { get; set; }

        public string id { get; set; }

        public int? MacroEstadoID { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public bool? Promesa { get; set; }

        public int? SubStatusID { get; set; }
    }

    public class EtapasDeDelicuencia
    {
        public string CondicionCierta { get; set; }

        public string CondicionFalsa { get; set; }

        public bool condicionFalsaRequerida { get; set; }

        public string CondicionSeleccion { get; set; }

        public string id { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public decimal? Precedencia { get; set; }

        public string Regla { get; set; }
    }

    public class Evento
    {
        public Guid? ActividadID { get; set; }

        public string ActivityCodeID { get; set; }

        public string AgenteID { get; set; }

        public string CasoID { get; set; }

        public string Comentario { get; set; }

        public string Descripcion { get; set; }

        public short? Estatus { get; set; }

        public string EventoID { get; set; }

        public DateTime? Fin { get; set; }

        public DateTime? Inicio { get; set; }

        public short? Tipo { get; set; }
    }

    public class GrupoCasosTable
    {
        public string GruposCasosTableID { get; set; }
    }

    public class HistorialContrasena
    {
        public int? contador { get; set; }

        public byte[] password { get; set; }

        public string userName { get; set; }
    }

    public class Horario
    {
        public string Descripcion { get; set; }

        public string DomIN { get; set; }

        public string DomOUT { get; set; }

        public long HorarioID { get; set; }

        public string JueIN { get; set; }

        public string JueOUT { get; set; }

        public string LunIN { get; set; }

        public string LunOUT { get; set; }

        public string MarIN { get; set; }

        public string MarOUT { get; set; }

        public string MieIN { get; set; }

        public string MieOUT { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string SabIN { get; set; }

        public string SabOUT { get; set; }

        public string VieIN { get; set; }

        public string VieOUT { get; set; }
    }

    public class InternalAgent_External_Agent
    {
        public string externalAgentUsername { get; set; }

        public string internalAgentUsername { get; set; }
    }

    public class InternalAgentComment
    {
        public string casoID { get; set; }

        public string comment { get; set; }

        public DateTime date { get; set; }

        public string id { get; set; }

        public string internalAgent { get; set; }
    }

    public class KPI
    {
        public double ActualDia { get; set; }

        public double ActualMes { get; set; }

        public double ActualSemana { get; set; }

        public string AgenteID { get; set; }

        public string GrupoKPI { get; set; }

        public double MetaDia { get; set; }

        public double MetaMes { get; set; }

        public double MetaSemana { get; set; }

        public string NombreKPI { get; set; }

        public double PctDia { get; set; }

        public double PctMes { get; set; }

        public double PctSemana { get; set; }
    }

    public class LugarVisita
    {
        public string Descripcion { get; set; }

        public string id { get; set; }

        public string LugarID { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }
    }

    public class MacroEstado
    {
        public string Descripcion { get; set; }

        public int? MacroEstadoID { get; set; }
    }

    public class Mensajeria
    {
        public string AgenteID { get; set; }

        public string Asunto { get; set; }

        public string CasoID { get; set; }

        public DateTime? EnviadoFechaHora { get; set; }

        public string EnviadoPor { get; set; }

        public short Estatus { get; set; }

        public DateTime? LeidoFechaHora { get; set; }

        public long MensajeID { get; set; }

        public string NotaEmisor { get; set; }

        public string NotaReceptor { get; set; }

        public DateTime? RecibidoFechaHora { get; set; }
    }

    public class ParametrosScorecard
    {
        public decimal? Estandar { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public decimal? NivelDePago { get; set; }

        public string ScoreID { get; set; }
    }

    public class Perfil
    {
        public string description { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public int? perfilID { get; set; }

        public bool? status { get; set; }
    }

    public class Perfil_Usuario
    {
        public int? perfilID { get; set; }

        public string userName { get; set; }
    }

    public class PoliticasSistema
    {
        public string Descripcion { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public int OptionParameter { get; set; }

        public string Parametro { get; set; }

        public string TipoDato { get; set; }

        public string Valor { get; set; }
    }

    public class ProductSystem
    {
        public string SystemName { get; set; }

        public string TableName { get; set; }
    }

    public class Promise
    {
        public string casoID { get; set; }

        public DateTime? Fecha { get; set; }

        public DateTime? FechaPromesa { get; set; }
    }

    public class PropiedadCaso
    {
        public int? Alerta { get; set; }

        public string CasoID { get; set; }

        public string Categoria { get; set; }

        public bool DataProtegida { get; set; }

        public string Nota { get; set; }

        public string Propiedad { get; set; }

        public string Valor { get; set; }
    }

    public class Query
    {
        public bool Activo { get; set; }

        public string fieldsToPivot { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public int QryID { get; set; }

        public string QryName { get; set; }

        public string QrySqlText { get; set; }

        public string QryType { get; set; }

        public string SerializedQryDef { get; set; }
    }

    public class QueryEstadoRiesgo
    {
        public int? EstadoRiesgoID { get; set; }

        public int? QryID { get; set; }
    }

    public class QueryFields
    {
        public int FieldID { get; set; }

        public bool ListaHija { get; set; }

        public bool ListaMaestra { get; set; }

        public bool ListaRiesgo { get; set; }

        public int QryFieldID { get; set; }
    }

    public class QueryParameters
    {
        public int ParamID { get; set; }

        public int QryID { get; set; }

        public string Value { get; set; }
    }

    public class RegionV1
    {
        public string id { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string nombre { get; set; }

        public bool ParaReporte { get; set; }

        public int? regionID { get; set; }
    }

    public class ReglaReferido
    {
        public int? RuleID { get; set; }

        public string WhereClause { get; set; }
    }

    public class ReporteCuentasAsignadas
    {
        public string CaseAssignType { get; set; }

        public string CasoID { get; set; }

        public string Estatus { get; set; }

        public decimal? MontoVencido { get; set; }

        public string Nombre { get; set; }

        public string NumeroCuenta { get; set; }

        public bool? Obligatorio { get; set; }

        public string ProductNumber { get; set; }

        public string Pueblo { get; set; }

        public string ZipCode { get; set; }
    }

    public class Ruta
    {
        public string id { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string nombre { get; set; }

        public int? rutaID { get; set; }
    }

    public class Ruta_CodigoPostal
    {
        public string codigoPostal { get; set; }

        public int? RutaID { get; set; }
    }

    public class Ruta_Producto
    {
        public string NumeroProducto { get; set; }

        public int? RutaID { get; set; }
    }

    public class RutaCaso
    {
        public string CasoID { get; set; }

        public DateTime? CobroDesdeHora { get; set; }

        public DateTime? CobroHastaHora { get; set; }

        public string Comentario { get; set; }

        public int? DiaCobro { get; set; }

        public int? DiaVisita { get; set; }

        public int EstatusActividad { get; set; }

        public int? Flag { get; set; }

        public int? Frecuencia { get; set; }

        public int? HoraCobro { get; set; }

        public int? HoraVisita { get; set; }

        public int? Jueves { get; set; }

        public double? LatitudRuta { get; set; }

        public double? LongitudRuta { get; set; }

        public string LugarCobro { get; set; }

        public string LugarVisita { get; set; }

        public int? Lunes { get; set; }

        public int? Martes { get; set; }

        public int? Miercoles { get; set; }

        public string RutaID { get; set; }

        public int? Sabado { get; set; }

        public int? Viernes { get; set; }

        public DateTime? VisitaDesdeHora { get; set; }

        public DateTime? VisitaHastaHora { get; set; }
    }

    public class SAExClientAccess
    {
        public string accessCode { get; set; }

        public int? accessId { get; set; }

        public int? accessOrder { get; set; }

        public string accessType { get; set; }

        public bool? active { get; set; }

        public string codeType { get; set; }

        public int? level { get; set; }

        public string name { get; set; }

        public int? parentAccess { get; set; }
    }

    public class SAExClientProfilesAccess
    {
        public int? accessID { get; set; }

        public int? perfilID { get; set; }
    }

    public class SAExClientUser
    {
        public int? active { get; set; }

        public bool canModified { get; set; }

        public string collectorID { get; set; }

        public DateTime? FechaActivacion { get; set; }

        public DateTime? FechaInactivacion { get; set; }

        public DateTime? FechaUltimoAcceso { get; set; }

        public string firstLastName { get; set; }

        public long? horarioID { get; set; }

        public bool? initialPassword { get; set; }

        public DateTime modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string name { get; set; }

        public byte[] password { get; set; }

        public DateTime? passwordDate { get; set; }

        public string position { get; set; }

        public string secondLastName { get; set; }

        public string telephoneNumber1 { get; set; }

        public string telephoneNumber2 { get; set; }

        public long? UltActividad { get; set; }

        public string userID { get; set; }

        public string userName { get; set; }
    }

    public class ScreenColumnOrder
    {
        public string datagrid { get; set; }

        public int? displayposition { get; set; }

        public string field { get; set; }

        public int? position { get; set; }

        public string screen { get; set; }

        public string username { get; set; }
    }

    public class SesionLog
    {
        public string AgenteID { get; set; }

        public int Estatus { get; set; }

        public DateTime Fecha { get; set; }

        public long MillageIn { get; set; }

        public long MillageOut { get; set; }

        public DateTime? TimeIn { get; set; }

        public DateTime? TimeOut { get; set; }
    }

    public class SortFieldsCaso
    {
        public string LabelCampo { get; set; }

        public string NombreCampo { get; set; }

        public string TipoFiltro { get; set; }

        public string Uso { get; set; }
    }

    public class SubStatus
    {
        public string Descripcion { get; set; }

        public int? SubStatusID { get; set; }
    }

    public class Supervisor_InternalAgent
    {
        public string internalAgentUsername { get; set; }

        public string supervisorUsername { get; set; }
    }

    public class TipoCaso
    {
        public string defaultColor { get; set; }

        public string Descripcion { get; set; }

        public byte[] IconImage { get; set; }

        public string ProductNumber { get; set; }

        public string TipoCasoID { get; set; }

        public string TipoListaMaestra { get; set; }

        public bool UsaActionCode { get; set; }
    }

    public class TipoCasoActivityCodes
    {
        public string ActivityCode { get; set; }

        public short TipoActivityCode { get; set; }

        public string TipoCaso { get; set; }

        public string uid { get; set; }
    }

    public class TipoContacto
    {
        public string Descripcion { get; set; }

        public string id { get; set; }

        public DateTime? modifyDate { get; set; }

        public string modifyUser { get; set; }

        public string Tipo_Contacto { get; set; }
    }

    public class TransactionType
    {
        public string TransactionDescription { get; set; }

        public int? TransactionTypeID { get; set; }
    }
}
