﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTypeImport
    {
        public string defaultColor { get; set; }

        public string Descripcion { get; set; }

        public string IconImage { get; set; }

        public string ProductNumber { get; set; }

        public int TipoCasoID { get; set; }

        public string TipoListaMaestra { get; set; }

        public bool UsaActionCode { get; set; }
    }
}
