﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(CaseImport))]
    public class ImportResponse
    {
        public ImportResponse()
        {
        }

        public ImportResponse(bool isSuccess, string message = null, Case @case = null)
        {
            IsSuccess = isSuccess;
            Message = message;
        }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }
    }
}
