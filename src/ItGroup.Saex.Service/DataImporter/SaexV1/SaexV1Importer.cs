﻿using Abp.Domain.Services;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.DomainEntities.V1;
using ItGroup.Saex.Service.DataImporter.SaexV1.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Service.DataImporter.FileImport
{
    public class SaexV1Importer : IDomainService
    {
        protected readonly CaseTypeManager _caseTypeManager;

        protected readonly ClosingDateManager _closingDateManager;

        protected readonly CaseImportManager _caseImportManager;

        protected readonly DelinquencyStateManager _delinquencyStateManager;

        protected readonly MacroStateManager _macroStateManager;

        protected readonly CaseManager _manager;

        protected readonly PostalCodeManager _postalCodemanager;

        protected readonly PostalCodeManager _postalCodeManager;

        protected readonly ProductManager _productManager;

        protected readonly RegionManager _regionManager;

        protected readonly RouteManager _routeManager;

        protected readonly SystemStateManager _systemStateManager;

        protected readonly SystemSubStateManager _systemSubStateManager;

        public SaexV1Importer(
            CaseManager manager,
            CaseTypeManager caseTypeManager,
            PostalCodeManager postalCodemanager,
            DelinquencyStateManager delinquencyStateManager,
            ProductManager productManager,
            SystemStateManager systemStateManager,
            SystemSubStateManager systemSubStateManager,
            MacroStateManager macroStateManager,
            PostalCodeManager postalCodeManager,
            RegionManager regionManager,
            RouteManager routeManager,
            ClosingDateManager closingDateManager,
            CaseImportManager caseImportManager
            )
        {
            _manager = manager;
            _postalCodemanager = postalCodemanager;
            _delinquencyStateManager = delinquencyStateManager;
            _productManager = productManager;
            _systemStateManager = systemStateManager;
            _systemSubStateManager = systemSubStateManager;
            _caseTypeManager = caseTypeManager;
            _macroStateManager = macroStateManager;
            _postalCodeManager = postalCodeManager;
            _regionManager = regionManager;
            _routeManager = routeManager;
            _closingDateManager = closingDateManager;
            _caseImportManager = caseImportManager;
        }

        public async Task<ImportResponse> ImportCase(CaseBaseline input, ImportType importType)
        {
            
            var res = await _caseImportManager.CreateAsync(input);

            var isSuccess = res.Id != Guid.Empty || res.Id != null;

            return GetResponse(isSuccess);
        }

        public async Task<ImportResponse> ImportCaseTypes(TipoCaso input)
        {
            var existingRecord = await _caseTypeManager.GetByCode(input.TipoCasoID.ToString());

            if (existingRecord != null)
                return new ImportResponse(false, "Record already exists");

            var model = new CaseType()
            {
                Code = input.TipoCasoID.ToString(),
                DefaultColor = input.defaultColor,
                Description = input.Descripcion,
                //IconImage = input.IconImage,
                MasterListType = input.TipoListaMaestra,
            };

            var product = await _productManager.GetByCode(input.ProductNumber);

            if (product != null)
            {
                model.Product = product;
                model.ProductId = product.Id;
            }

            var res = await _caseTypeManager.CreateAsync(model);

            var isSuccess = res.Id != null || res.Id != Guid.Empty;

            return GetResponse(isSuccess);
        }

        public async Task<ImportResponse> ImportClosingDate(DiasDeCierre input)
        {
            var existingRecord = _closingDateManager.GetAll().Where(r => r.StartDate.Year == input.Year && r.StartDate.Month == input.Mes && r.StartDate.Day == input.Dia).FirstOrDefault();

            if (existingRecord != null)
                return new ImportResponse(false, "Record already exists");

            var model = new ClosingDate()
            {
                StartDate = new DateTime(input.Year.Value, input.Mes.Value, input.Dia.Value),
                EndDate = new DateTime(input.Year.Value, input.Mes.Value, input.Dia.Value),
                IsAllDay = true,
                Type = Enumerations.Enumerations.ClosingDateType.ClosingDate
            };

            var res = await _closingDateManager.CreateAsync(model);

            var isSuccess = res.Id != null || res.Id != Guid.Empty;

            return GetResponse(isSuccess);
        }

        public async Task<ImportResponse> ImportHoliday(DiasFeriados input)
        {
            var existingRecord = _closingDateManager.GetAll().Where(r => r.StartDate.Year == input.Year && r.StartDate.Month == input.Mes && r.StartDate.Day == input.Dia).FirstOrDefault();

            if (existingRecord != null)
                return new ImportResponse(false, "Record already exists");

            var model = new ClosingDate()
            {
                StartDate = new DateTime(input.Year.Value, input.Mes.Value, input.Dia.Value),
                EndDate = new DateTime(input.Year.Value, input.Mes.Value, input.Dia.Value),
                IsAllDay = true,
                Type = Enumerations.Enumerations.ClosingDateType.Holiday
            };

            var res = await _closingDateManager.CreateAsync(model);

            var isSuccess = res.Id != null || res.Id != Guid.Empty;

            return GetResponse(isSuccess);
        }

        public async Task<ImportResponse> ImportMacroStates(MacroEstado input)
        {
            var existingRecord = await _macroStateManager.GetByCode(input.MacroEstadoID.ToString());

            if (existingRecord != null)
                return new ImportResponse(false, "Record already exists");

            var model = new MacroState()
            {
                Code = input.MacroEstadoID.ToString(),
                Description = input.Descripcion
            };

            var res = await _macroStateManager.CreateAsync(model);

            var isSuccess = res.Id != null || res.Id != Guid.Empty;

            return GetResponse(isSuccess);
        }

        public async Task<ImportResponse> ImportPostalCodes(CodigoPostal input)
        {
            var existingRecord = _postalCodeManager.GetByCode(input.codigoPostal.ToString());

            if (existingRecord != null)
                return new ImportResponse(false, "Record already exists");

            var model = new PostalCode()
            {
                Code = input.codigoPostal,
                Area = input.nombreArea
            };

            var res = await _postalCodeManager.CreateAsync(model);

            var isSuccess = res.Id != null || res.Id != Guid.Empty;

            return GetResponse(isSuccess);
        }

        public async Task<ImportResponse> ImportRegions(RegionV1 input)
        {
            var existingRecord = _regionManager.GetAll().Where(r => r.Code == input.regionID.ToString()).FirstOrDefault();

            if (existingRecord != null)
                return new ImportResponse(false, "Record already exists");

            var model = new Region()
            {
                Code = input.regionID.ToString(),
                Name = input.nombre
            };

            var res = await _regionManager.CreateAsync(model);

            var isSuccess = res.Id != null || res.Id != Guid.Empty;

            return GetResponse(isSuccess);
        }

        public async Task<ImportResponse> ImportRoutes(Ruta input)
        {
            var existingRecord = _routeManager.GetAll().Where(r => r.Name == input.nombre).FirstOrDefault();

            if (existingRecord != null)
                return new ImportResponse(false, "Record already exists");

            var model = new Route()
            {
                Name = input.nombre
            };

            var res = await _routeManager.CreateAsync(model);

            var isSuccess = res.Id != null || res.Id != Guid.Empty;

            return GetResponse(isSuccess);
        }

        public async Task<ImportResponse> ImportSystemStates(EstadosSistema input)
        {
            var existingRecord = await _caseTypeManager.GetByCode(input.EstadoSistema.ToString());

            if (existingRecord != null)
                return new ImportResponse(false, "Record already exists");

            var model = new SystemState()
            {
                Code = input.EstadoSistema,
                Description = input.Definicion,
                IsPromise = input.Promesa.Value,
            };

            var subState = await _systemSubStateManager.GetByCode(input.SubStatusID.ToString());
            model.SubState = subState;

            var macroState = await _macroStateManager.GetByCode(input.MacroEstadoID.ToString());
            model.MacroState = macroState;

            var res = await _systemStateManager.CreateAsync(model);
            var isSuccess = res.Id != null || res.Id != Guid.Empty;

            return GetResponse(isSuccess);
        }

        public async Task<ImportResponse> ImportSystemSubStates(SubStatus input)
        {
            var existingRecord = await _systemSubStateManager.GetByCode(input.SubStatusID.ToString());

            if (existingRecord != null)
                return new ImportResponse(false, "Record already exists");

            var model = new SystemSubState()
            {
                Code = input.SubStatusID.ToString(),
                Description = input.Descripcion
            };

            var res = await _systemSubStateManager.CreateAsync(model);
            var isSuccess = res.Id != null || res.Id != Guid.Empty;

            return GetResponse(isSuccess);
        }

        private ImportResponse GetResponse(bool isSuccess)
        {
            return new ImportResponse(isSuccess, isSuccess ? null : "Error during import");
        }

        private List<Customer> ImportCustomers(CaseImport input)
        {
            return null;

            /*var customers = new List<Customer>();

            if (!string.IsNullOrWhiteSpace(input.Nombre))
            {
                var postalCode = _postalCodemanager.GetByCode(input.DireccionClienteZipCode.Substring(0, 5).ToString());

                var address = new Address()
                {
                    PostalCode = postalCode,
                    City = input.Pueblo ?? input.Ciudad,
                    AddressLine1 = input.DireccionCliente1,
                    AddressLine2 = input.DireccionCliente2,
                };

                if (input.Latitud != null)
                    address.Latitude = float.Parse(input.Latitud.ToString());

                if (input.Longitud != null)
                    address.Longitude = float.Parse(input.Longitud.ToString());

                var addressList = new List<Address>();
                addressList.Add(address);

                var principalCustomer = new Customer()
                {
                    FullName = input.Nombre,
                    Addresses = addressList,
                    PhoneNumber = input.TelefonoPrincipal,
                    EmailAddress = input.Email,
                    Ssn = input.SeguroSocial,
                    BirthDate = DateTime.Parse(input.FechaNacimiento),
                    IsPrincipal = true
                };

                customers.Add(principalCustomer);
            }

            if (!string.IsNullOrWhiteSpace(input.Nombre2))
            {
                var secondCustomer = new Customer()
                {
                    FullName = input.Nombre2,
                    PhoneNumber = input.Telefono2,
                    IsPrincipal = false
                };

                customers.Add(secondCustomer);
            }

            return customers;*/
        }
    }
}
