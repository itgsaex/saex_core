﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ItGroup.Saex.Authorization.Accounts.Dto;

namespace ItGroup.Saex.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
