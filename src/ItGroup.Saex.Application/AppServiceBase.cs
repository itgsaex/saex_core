﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.MultiTenancy;
using static ItGroup.Saex.Enumerations.Enumerations;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Newtonsoft.Json;
using ItGroup.Saex.DomainEntities;
using EnumsNET;
using Abp.Domain.Entities.Auditing;

namespace ItGroup.Saex
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class AppServiceBase : ApplicationService
    {
        public AuditLogManager _auditManager;

        //protected AppServiceBase(AuditLogManager auditManager)
        //{
        //    _auditManager = auditManager;
        //    LocalizationSourceName = SaexConsts.LocalizationSourceName;
        //}

        protected AppServiceBase()
        {
            LocalizationSourceName = SaexConsts.LocalizationSourceName;
        }

        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual async Task<User> GetCurrentUserAsync()
        {
            var user = await UserManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }

            return user;
        }

        protected async Task LogTransaction(Entity<Guid> entity, EntityDto<Guid> inputObject, AuditLogOperationType operationType, string description)
        {
            var auditObjectValue = "";

            if (inputObject != null)
                auditObjectValue = JsonConvert.SerializeObject(inputObject);

            var domainAuditObjectValue = JsonConvert.SerializeObject(entity);

            var user = await GetCurrentUserAsync();

            await _auditManager.CreateAsync(new AuditLog(entity.GetType().Name, entity.Id.ToString(), Enums.GetName(operationType), domainAuditObjectValue, auditObjectValue, user.UserName, description));
        }
    }
}
