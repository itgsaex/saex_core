﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.UI;
using EnumsNET;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Authorization.Accounts;
using ItGroup.Saex.Authorization.Roles;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Roles.Dto;
using ItGroup.Saex.Users.Dto;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Users
{
    [AbpAuthorize(PermissionNames.Pages_Users_View)]
    public class UserAppService : AsyncCrudAppService<User, UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>, IUserAppService
    {
        private readonly IAbpSession _abpSession;

        private readonly AgentManager _agentManager;

        private readonly AuditLogManager _auditManager;

        private readonly LogInManager _logInManager;

        private readonly IPasswordHasher<User> _passwordHasher;

        private readonly RoleManager _roleManager;

        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<User, long> _userRepository;

        private readonly UserManager _userManager;

        public UserAppService(
            IRepository<User, long> repository,
            UserManager userManager,
            RoleManager roleManager,
            IRepository<Role> roleRepository,
            IPasswordHasher<User> passwordHasher,
            IAbpSession abpSession,
            LogInManager logInManager,
            AgentManager agentManager,
            AuditLogManager auditManager,
            IRepository<User, long> userRepository)
            : base(repository)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _roleRepository = roleRepository;
            _passwordHasher = passwordHasher;
            _abpSession = abpSession;
            _logInManager = logInManager;
            _agentManager = agentManager;
            _auditManager = auditManager;
            _userRepository = userRepository;
        }

        public async Task ChangeLanguage(ChangeUserLanguageDto input)
        {
            await SettingManager.ChangeSettingForUserAsync(
                AbpSession.ToUserIdentifier(),
                LocalizationSettingNames.DefaultLanguage,
                input.LanguageName
            );
        }

        public async Task<bool> ChangePassword(ChangePasswordDto input)
        {
            if (_abpSession.UserId == null)
            {
                throw new UserFriendlyException("Please log in before attemping to change password.");
            }
            long userId = _abpSession.UserId.Value;
            var user = await _userManager.GetUserByIdAsync(userId);
            var loginAsync = await _logInManager.LoginAsync(user.UserName, input.CurrentPassword, shouldLockout: false);
            if (loginAsync.Result != AbpLoginResultType.Success)
            {
                throw new UserFriendlyException("Your 'Existing Password' did not match the one on record.  Please try again or contact an administrator for assistance in resetting your password.");
            }
            
            user.Password = _passwordHasher.HashPassword(user, input.NewPassword);
            CurrentUnitOfWork.SaveChanges();
            return true;
        }

        public override async Task<UserDto> Create(CreateUserDto input)
        {
            CheckCreatePermission();

            var user = ObjectMapper.Map<User>(input);

            user.TenantId = AbpSession.TenantId;
            user.IsEmailConfirmed = true;

            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);

            CheckErrors(await _userManager.CreateAsync(user, input.Password));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
            }

            CurrentUnitOfWork.SaveChanges();

            if (input.AgentType != null)
            {
                var agent = new Agent()
                {
                    UserId = user.Id,
                    Type = (AgentType)input.AgentType,
                    CollectorId = input.CollectorId
                };

                await _agentManager.CreateAsync(agent);
            } else
            {
                user.Agent = new Agent();
            }

            await LogTransaction(user, input, AuditLogOperationType.Create, "Created an User record");

            return new UserDto(user);
        }

        public override async Task Delete(EntityDto<long> input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);

            await _userManager.DeleteAsync(user);

            await LogTransaction(user, null, AuditLogOperationType.Delete, "Deleted an User record");
        }

        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var roles = await _roleRepository.GetAllListAsync();
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(roles));
        }

        public async Task<bool> ResetPassword(ResetPasswordDto input)
        {
            if (_abpSession.UserId == null)
            {
                throw new UserFriendlyException("Please log in before attemping to reset password.");
            }
            long currentUserId = _abpSession.UserId.Value;
            var currentUser = await _userManager.GetUserByIdAsync(currentUserId);
            var loginAsync = await _logInManager.LoginAsync(currentUser.UserName, input.AdminPassword, shouldLockout: false);
            if (loginAsync.Result != AbpLoginResultType.Success)
            {
                throw new UserFriendlyException("Your 'Admin Password' did not match the one on record.  Please try again.");
            }
            if (currentUser.IsDeleted || !currentUser.IsActive)
            {
                return false;
            }
            var roles = await _userManager.GetRolesAsync(currentUser);
            if (!roles.Contains(StaticRoleNames.Tenants.Admin))
            {
                throw new UserFriendlyException("Only administrators may reset passwords.");
            }

            var user = await _userManager.GetUserByIdAsync(input.UserId);
            if (user != null)
            {
                user.Password = _passwordHasher.HashPassword(user, input.NewPassword);
                CurrentUnitOfWork.SaveChanges();
            }

            return true;
        }

        public async Task<bool> UserResetPassword(UserResetPasswordDto input)
        {
            //if (_abpSession.UserId == null)
            //{
            //    throw new UserFriendlyException("Please log in before attemping to reset password.");
            //}
            //long currentUserId = _abpSession.UserId.Value;
            //var currentUser = await _userManager.GetUserByIdAsync(currentUserId);
            //var loginAsync = await _logInManager.LoginAsync(currentUser.UserName, input.AdminPassword, shouldLockout: false);
            //if (loginAsync.Result != AbpLoginResultType.Success)
            //{
            //    throw new UserFriendlyException("Your 'Admin Password' did not match the one on record.  Please try again.");
            //}
            
            //var roles = await _userManager.GetRolesAsync(currentUser);
            //if (!roles.Contains(StaticRoleNames.Tenants.Admin))
            //{
            //    throw new UserFriendlyException("Only administrators may reset passwords.");
            //}
            
            var user = await _userManager.GetUserByIdAsync(input.UserId);
            if (user != null)
            {
                if (user.IsDeleted || !user.IsActive)
                {
                    return false;
                }

                if (!new Regex(AccountAppService.PasswordRegex).IsMatch(input.NewPassword))
                {
                    throw new UserFriendlyException("Passwords must be at least 8 characters, contain a lowercase, uppercase, and number.");
                }

                if (input.NewPassword == input.ConfirmPassword)
                {
                    user.Password = _passwordHasher.HashPassword(user, input.NewPassword);
                    user.PasswordResetCode = string.Empty;
                    user.PasswordReset = false;
                    CurrentUnitOfWork.SaveChanges();
                }
            }

            return true;
        }

        public async Task<PagedResultDto<UserDto>> Search(PagedUserResultRequestDto filters)
        {
            var res = new PagedResultDto<UserDto>();

            var query = _userManager.Users.Include(x => x.Agent).AsQueryable();

            switch (filters.Status)
            {
                case UserStatus.Active:
                    query = query.Where(x => x.IsActive);
                    break;

                case UserStatus.Inactive:
                    query = query.Where(x => !x.IsActive);
                    break;

                case UserStatus.Deleted:
                    query = query.Where(x => x.IsDeleted);
                    break;

                default:
                    break;
            }

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Select(x => new UserDto(x)).Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();

            return res;
        }

        public override async Task<UserDto> Update(UserDto input)
        {
            CheckUpdatePermission();
           
            var user = await _userManager.GetUserByIdAsync(input.Id);
            var originalUser = user.Clone() as User;

            MapToEntity(input, user);

            using (UnitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var res = await Task.FromResult(_agentManager.GetAllWithDeletedQuery().Where(x => x.UserId == user.Id).FirstOrDefault());

                if (res == null)
                {
                    var agent = new Agent()
                    {
                        UserId = user.Id,
                        Type = input.AgentType,
                        CollectorId = input.CollectorId
                    };

                    await _agentManager.CreateAsync(agent);
                }
                else
                {
                    res.IsDeleted = false;
                    res.Type = input.AgentType;
                    res.CollectorId = input.CollectorId;
                    await _agentManager.UpdateAsync(res);
                }
            }
            
            CheckErrors(await _userManager.UpdateAsync(user));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
            }

            await LogTransaction(originalUser, input, AuditLogOperationType.Update, "Updated an User record");

            return await Get(input);
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedUserResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        protected override IQueryable<User> CreateFilteredQuery(PagedUserResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Roles)
                .WhereIf(!input.Keyword.IsNullOrWhiteSpace(), x => x.UserName.Contains(input.Keyword) || x.Name.Contains(input.Keyword) || x.EmailAddress.Contains(input.Keyword))
                .WhereIf(input.IsActive.HasValue, x => x.IsActive == input.IsActive);
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            var user = await Repository.GetAllIncluding(x => x.Roles, y => y.Agent).FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityNotFoundException(typeof(User), id);
            }

            return user;
        }

        protected async Task LogTransaction(User entity, EntityDto<long> inputObject, AuditLogOperationType operationType, string description)
        {
            var auditObjectValue = "";

            if (inputObject != null)
                auditObjectValue = JsonConvert.SerializeObject(inputObject);

            var domainAuditObjectValue = JsonConvert.SerializeObject(entity);

            var user = await _userManager.FindByIdAsync(AbpSession.GetUserId().ToString()); ;

            await _auditManager.CreateAsync(new AuditLog(entity.GetType().Name, entity.Id.ToString(), Enums.GetName(operationType), domainAuditObjectValue, auditObjectValue, user.UserName, description));
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            user.SetNormalizedNames();
            return user;
        }

        protected override void MapToEntity(UserDto input, User user)
        {
            ObjectMapper.Map(input, user);
            user.SetNormalizedNames();
        }

        protected override UserDto MapToEntityDto(User user)
        {
            var roles = _roleManager.Roles.Where(r => user.Roles.Any(ur => ur.RoleId == r.Id)).Select(r => r.NormalizedName);

            var userDto = base.MapToEntityDto(user);
            userDto.RoleNames = roles.ToArray();

            var agent = _agentManager.GetAllQuery().Where(x => x.UserId == user.Id).FirstOrDefault();

            if (agent != null)
                userDto.AgentType = agent.Type;

            return userDto;
        }

        public List<UserDto> CustomGetAll()
        {
            var results = _userRepository.GetAll().ToList();
            return results.Select(u => new UserDto(u)).ToList();
        }
    }
}
