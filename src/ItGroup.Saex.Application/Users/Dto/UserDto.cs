using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Users.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<long>
    {
        public UserDto()
        {
        }

        public UserDto(User input)
        {
            Id = input.Id;
            UserName = input.UserName;
            Name = input.Name;
            Surname = input.Surname;
            MotherSurname = input.MotherSurname;
            EmailAddress = input.EmailAddress;
            IsActive = input.IsActive;
            FullName = input.FullName;
            CreationTime = input.CreationTime;
            PhoneNumber = input.PhoneNumber;
            AlternatePhone = input.AlternatePhone;
            Agent = input.Agent;
            LastModifierUserId = input.LastModifierUserId;
            LastModificationTime = input.LastModificationTime;
        }

        public AgentType? AgentType { get; set; }

        public DateTime CreationTime { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public bool PasswordReset { get; set; }
        public string PasswordResetCode { get; set; }
        
        public string FullName { get; set; }

        public bool IsActive { get; set; }

        public DateTime? LastLoginTime { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        public string[] RoleNames { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        public string MotherSurname { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        public string PhoneNumber { get; set; }

        public string AlternatePhone { get; set; }

        public Agent Agent { get; set; }

        public long? LastModifierUserId { get; set; }

        public string LastModifierUserName { get; set; }

        public DateTime? LastModificationTime { get; set; }
        public string CollectorId { get; set; }
    }
}