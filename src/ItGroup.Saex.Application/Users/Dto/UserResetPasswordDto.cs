﻿using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.Users.Dto
{
    public class UserResetPasswordDto
    {
        public UserResetPasswordDto(long UserId)
        {
            this.UserId = UserId;
        }

        [Required]
        public long UserId { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }

    }
}
