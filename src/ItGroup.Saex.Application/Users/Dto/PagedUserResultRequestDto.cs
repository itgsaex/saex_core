﻿using Abp.Application.Services.Dto;
using System;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Users.Dto
{
    //custom PagedResultRequestDto
    public class PagedUserResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }

        public bool? IsActive { get; set; }

        public UserStatus Status { get; set; }
    }
}
