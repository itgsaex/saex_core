using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}