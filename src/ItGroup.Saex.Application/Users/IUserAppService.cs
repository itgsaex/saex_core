using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.Roles.Dto;
using ItGroup.Saex.Users.Dto;

namespace ItGroup.Saex.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task ChangeLanguage(ChangeUserLanguageDto input);

        Task<ListResultDto<RoleDto>> GetRoles();

        Task<PagedResultDto<UserDto>> Search(PagedUserResultRequestDto filters);

        List<UserDto> CustomGetAll();
    }
}
