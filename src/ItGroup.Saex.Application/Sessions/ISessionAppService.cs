﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ItGroup.Saex.Sessions.Dto;

namespace ItGroup.Saex.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
