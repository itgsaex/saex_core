﻿using System.Threading.Tasks;
using ItGroup.Saex.Configuration.Dto;

namespace ItGroup.Saex.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
