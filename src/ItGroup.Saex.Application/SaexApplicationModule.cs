﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ItGroup.Saex.Authorization;

namespace ItGroup.Saex
{
    [DependsOn(
        typeof(SaexCoreModule),
        typeof(AbpAutoMapperModule))]
    public class SaexApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            var thisAssembly = typeof(SaexApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }

        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<SaexAuthorizationProvider>();
        }
    }
}
