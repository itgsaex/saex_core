using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Roles;
using ItGroup.Saex.Authorization.Roles;

namespace ItGroup.Saex.Roles.Dto
{
    public class CreateRoleDto
    {
        [StringLength(Role.MaxDescriptionLength)]
        public string Description { get; set; }

        [StringLength(AbpRoleBase.MaxDisplayNameLength)]
        public string DisplayName { get; set; }

        public List<string> GrantedPermissions { get; set; }

        [Required]
        [StringLength(AbpRoleBase.MaxNameLength)]
        public string Name { get; set; }

        public string NormalizedName { get; set; }

        public bool IsActive { get; set; }
    }
}
