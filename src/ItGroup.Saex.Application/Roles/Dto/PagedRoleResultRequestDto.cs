﻿using Abp.Application.Services.Dto;

namespace ItGroup.Saex.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

