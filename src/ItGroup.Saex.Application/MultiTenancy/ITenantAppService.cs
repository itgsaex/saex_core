﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.MultiTenancy.Dto;

namespace ItGroup.Saex.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

