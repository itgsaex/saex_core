using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IBucketAppService : IApplicationService
    {
        Task<BucketDto> Create(BucketCreateDto input);

        Task Delete(Guid id);

        Task<BucketDto> Get(Guid id);

        Task<PagedResultDto<BucketDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<BucketListDto>> Search(BucketSearchModel filters);

        Task<BucketDto> Update(BucketUpdateDto input);
    }
}