using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class BucketAppService : AppServiceBase, IBucketAppService
    {
        protected new readonly BucketManager _manager;

        public BucketAppService(BucketManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Buckets_Modify)]
        public async Task<BucketDto> Create(BucketCreateDto input)
        {
            var model = ObjectMapper.Map<Bucket>(input);

            await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Bucket record");

            return new BucketDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Buckets_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Bucket record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Buckets_View)]
        public async Task<BucketDto> Get(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(model, null, AuditLogOperationType.View, "Viewed a Bucket record");

            return new BucketDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Buckets_View)]
        public async Task<PagedResultDto<BucketDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<BucketDto>();

            var models = await _manager.GetAll().ToListAsync() as IReadOnlyList<Bucket>;

            output.TotalCount = models.Count();
            output.Items = models.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new BucketDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Buckets_View)]
        public async Task<PagedResultDto<BucketListDto>> Search(BucketSearchModel filters)
        {
            var model = new PagedResultDto<BucketListDto>();

            var models = await _manager.GetFiltered(filters);

            model.TotalCount = models.TotalCount;
            model.Items = models.Items.Select(x => new BucketListDto(x)).ToList();

            return model;
        }

        [AbpAuthorize(PermissionNames.Pages_Buckets_Modify)]
        public async Task<BucketDto> Update(BucketUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("Bucket id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as Bucket;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.ProductId = input.ProductId;
            model.MaxValue = input.MaxValue;
            model.MinValue = input.MinValue;

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Bucket record");

            return new BucketDto(model);
        }
    }
}
