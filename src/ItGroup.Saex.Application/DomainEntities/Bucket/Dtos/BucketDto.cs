﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class BucketDto : EntityDto<Guid>
    {
        public BucketDto()
        {
        }

        public BucketDto(Bucket input)
        {
            Id = input.Id;
            MinValue = input.MinValue;
            MaxValue = input.MaxValue;

            if (input.Product != null)
                Product = new ProductDto(input.Product);
        }

        public int MaxValue { get; set; }

        public int MinValue { get; set; }

        public ProductDto Product { get; set; }
    }
}