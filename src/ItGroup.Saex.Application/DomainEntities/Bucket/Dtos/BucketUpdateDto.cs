﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Bucket))]
    public class BucketUpdateDto : EntityDto<Guid>
    {
        public BucketUpdateDto() { }

        public int MinValue { get; set; }

        public int MaxValue { get; set; }

        public Guid ProductId { get; set; }
    }
}