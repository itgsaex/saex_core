﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Bucket))]
    public class BucketCreateDto : EntityDto<Guid>
    {
        public BucketCreateDto()
        {
        }

        public int MaxValue { get; set; }

        public int MinValue { get; set; }

        public Guid ProductId { get; set; }
    }
}
