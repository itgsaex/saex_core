using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IActivityCodeDetailAppService : IApplicationService
    {
        List<ActivityCodeDetail> GetAllActivityCodeDetails();
    }
}
