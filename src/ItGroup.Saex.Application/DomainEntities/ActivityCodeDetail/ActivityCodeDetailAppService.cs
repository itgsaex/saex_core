using System.Collections.Generic;
using System.Linq;
using ItGroup.Saex.Manager.DomainEntities;

namespace ItGroup.Saex.DomainEntities
{
    public class ActivityCodeDetailAppService : IActivityCodeDetailAppService
    {
        protected new readonly ActivityCodeDetailManager _manager;

        public ActivityCodeDetailAppService(ActivityCodeDetailManager manager) : base()
        {
            _manager = manager;
        }

        public List<ActivityCodeDetail> GetAllActivityCodeDetails()
        {
            var results = _manager.GetAllActivityCodeDetails().ToList();
            return results;
        }
    }
}
