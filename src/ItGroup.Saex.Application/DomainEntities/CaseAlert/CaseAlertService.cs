﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Manager.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseAlertAppService : ICaseAlertAppService
    {
        protected new readonly CaseAlertManager _manager;
        public CaseAlertAppService(CaseAlertManager manager) : base()
        {
            _manager = manager;
        }
        public List<Alert> GetAlerts()
        {
            var result = _manager.GetAlerts().ToList();
            return result;
        }

        public List<CaseAlert> GetCaseAlerts(string agentID)
        {
            var result = _manager.GetCaseAlerts(agentID).ToList();
            return result;
        }
    }
}
