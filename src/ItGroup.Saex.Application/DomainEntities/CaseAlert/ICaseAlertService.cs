﻿using Abp.Application.Services;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseAlertAppService : IApplicationService
    {
        List<Alert> GetAlerts();

        List<CaseAlert> GetCaseAlerts(string agentID);
    }
}
