using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemStatusAppService : AppServiceBase, ISystemStatusAppService
    {
        protected new readonly SystemStatusManager _manager;

        public SystemStatusAppService(SystemStatusManager manager) : base()
        {
            _manager = manager;
        }

        [AbpAuthorize(PermissionNames.Pages_Users)]
        public async Task<SystemStatusDto> Create(SystemStatusCreateDto input)
        {
            var category = ObjectMapper.Map<SystemStatus>(input);
            var result = await _manager.CreateAsync(category);

            return new SystemStatusDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Users)]
        public async Task Delete(Guid id)
        {
            var category = await _manager.GetAsync(id);
            if (category == null) throw new UserFriendlyException("SystemStatus does not found for the provided Id.");

            await _manager.DeleteAsync(category);
        }

        [AbpAuthorize(PermissionNames.Pages_Users)]
        public async Task<SystemStatusDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("SystemStatus not found for the provided Id");

            return new SystemStatusDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Users)]
        public async Task<PagedResultDto<SystemStatusDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<SystemStatusDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<SystemStatus>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new SystemStatusDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Users)]
        public async Task<PagedResultDto<SystemStatusDto>> Search(SystemStatusSearchModel filters)
        {
            var res = new PagedResultDto<SystemStatusDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new SystemStatusDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Users)]
        public async Task<SystemStatusDto> Update(SystemStatusUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("SystemStatus id is null.");

            var ca = await _manager.GetAsync((Guid)input.Id);
            if (ca == null) throw new EntityNotFoundException("SystemStatus does not exist in database.");

            await _manager.UpdateAsync(ca);

            return new SystemStatusDto(ca);
        }
    }
}