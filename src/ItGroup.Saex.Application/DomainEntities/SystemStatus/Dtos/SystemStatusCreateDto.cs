﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(SystemStatus))]
    public class SystemStatusCreateDto
    {
        public SystemStatusCreateDto() { }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }
    }
}