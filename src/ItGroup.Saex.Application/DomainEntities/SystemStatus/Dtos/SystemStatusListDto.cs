﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemStatusListDto : EntityDto<Guid>
    {
        public SystemStatusListDto() { }
        public SystemStatusListDto(SystemStatus input)
        {
            Name = input.Name;
            Description = input.Description;
            Type = input.Type;
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }
    }

}