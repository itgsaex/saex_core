using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISystemStatusAppService : IApplicationService
    {
        Task<SystemStatusDto> Create(SystemStatusCreateDto input);

        Task Delete(Guid id);

        Task<SystemStatusDto> Get(Guid id);

        Task<PagedResultDto<SystemStatusDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<SystemStatusDto>> Search(SystemStatusSearchModel filters);

        Task<SystemStatusDto> Update(SystemStatusUpdateDto input);
    }
}