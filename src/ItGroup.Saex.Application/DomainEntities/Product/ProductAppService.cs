using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ProductAppService : AppServiceBase, IProductAppService
    {
        protected new readonly ProductManager _manager;

        public ProductAppService(ProductManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Products_Modify)]
        public async Task<ProductDto> Create(ProductCreateDto input)
        {
            var model = ObjectMapper.Map<Product>(input);

            var exist = await _manager.GetByCode(input.Code);
            if (exist != null)
            {
                throw new EntityNotFoundException(L("RecordExist"));
            }

            var result = await _manager.CreateAsync(model);
            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Product");

            return new ProductDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Products_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Product not found for the provided Id.");

            await _manager.DeleteAsync(model);
            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Product");
        }

        [AbpAuthorize(PermissionNames.Pages_Products_View)]
        public async Task<ProductDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("Product not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Product");

            return new ProductDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Products_View)]
        public async Task<PagedResultDto<ProductDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ProductDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<Product>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ProductDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Products_View)]
        public async Task<List<ProductListDto>> GetAllForList()
        {
            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<Product>;

            return results.Select(x => new ProductListDto(x)).ToList();
        }

        public List<Product> GetAllProducts()
        {
            var products = _manager.GetAll();
            return products.ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_Products_View)]
        public async Task<PagedResultDto<ProductListDto>> Search(ProductSearchModel filters)
        {
            var res = new PagedResultDto<ProductListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ProductListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Products_Modify)]
        public async Task<ProductDto> Update(ProductUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("Product id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as Product;
            if (model == null) throw new EntityNotFoundException("Product does not exist in database.");

            model.Code = input.Code;
            model.Description = input.Description;

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Product");

            return new ProductDto(model);
        }
    }
}
