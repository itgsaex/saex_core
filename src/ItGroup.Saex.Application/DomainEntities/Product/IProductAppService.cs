using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IProductAppService : IApplicationService
    {
        Task<ProductDto> Create(ProductCreateDto input);

        Task Delete(Guid id);

        Task<ProductDto> Get(Guid id);

        Task<PagedResultDto<ProductDto>> GetAll(PagedResultRequestDto paging);

        Task<List<ProductListDto>> GetAllForList();

        Task<PagedResultDto<ProductListDto>> Search(ProductSearchModel filters);

        Task<ProductDto> Update(ProductUpdateDto input);

        List<Product> GetAllProducts();
    }
}