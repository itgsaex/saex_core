﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseProductDto : EntityDto<Guid>
    {
        public CaseProductDto()
        {
        }

        public CaseProductDto(CaseProduct input)
        {
            Id = input.Id;
            Value = input.Value;

            if (input.Product != null)
                Product = new ProductDto(input.Product);
        }

        public ProductDto Product { get; set; }

        public string Value { get; set; }
    }
}
