﻿using ItGroup.Saex.Manager.DomainEntities;
using System.Collections.Generic;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class KPIAppService : IKPIAppService
    {
        protected new readonly KPIManager _manager;
        public KPIAppService(KPIManager manager) : base()
        {
            _manager = manager;
        }
        public List<KPI> GetAllKPIs()
        {
            var result = _manager.GetAllKPIs().ToList();
            return result;
        }

        public List<KPI> GetKPIs(string agentID)
        {
            var result = _manager.GetKPIsForAgent(agentID).ToList();
            return result;
        }
    }
}
