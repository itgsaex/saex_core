﻿using Abp.Application.Services;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public interface IKPIAppService : IApplicationService
    {
        List<KPI> GetAllKPIs();

        List<KPI> GetKPIs(string agentID);
    }
}
