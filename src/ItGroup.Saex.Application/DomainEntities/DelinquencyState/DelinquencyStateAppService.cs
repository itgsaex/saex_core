using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using CsvHelper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class DelinquencyStateAppService : AppServiceBase, IDelinquencyStateAppService
    {
        protected new readonly DelinquencyStateManager _manager;

        public DelinquencyStateAppService(DelinquencyStateManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_DelinquencyStates_Modify)]
        public async Task<DelinquencyStateDto> Create(DelinquencyStateCreateDto input)
        {
            var model = ObjectMapper.Map<DelinquencyState>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Delinquency State record");

            return new DelinquencyStateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_DelinquencyStates_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Delinquency State not found for the provided Id.");

            await _manager.DeleteAsync(model);
            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Delinquency State record");
        }

        [AbpAuthorize(PermissionNames.Pages_DelinquencyStates_View)]
        public async Task<DelinquencyStateDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("Delinquency State not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Delinquency State record");

            return new DelinquencyStateDto(result);
        }

        [AbpAuthorize]
        public async Task<List<DelinquencyStateListDto>> GetAllForList()
        {
            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<DelinquencyState>;

            return results.Select(x => new DelinquencyStateListDto(x)).ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_DelinquencyStates_Modify)]
        public async Task Import(IFormFile file)
        {
            TextReader reader = new StreamReader(file.OpenReadStream());

            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Configuration.IgnoreQuotes = true;
                csvReader.Configuration.LineBreakInQuotedFieldIsBadData = true;
                csvReader.Configuration.HasHeaderRecord = true;
                csvReader.Configuration.CultureInfo = CultureInfo.InvariantCulture;

                var records = csvReader.GetRecords<DelinquencyStateImportDto>();
                CultureInfo provider = CultureInfo.InvariantCulture;

                foreach (var input in records)
                {
                    var state = _manager.GetAllQuery().Where(x => x.Condition.ToLower() == input.CondicionSeleccion.ToLower()).FirstOrDefault();

                    if (state == null)
                    {
                        var delinquencyState = new DelinquencyState()
                        {
                            Condition = input.CondicionSeleccion,
                            Precedence = input.Precedencia,
                            Rule = input.Regla,
                            SuccessfulRuleValue = input.CondicionCierta,
                            UnsuccessfulRuleValue = input.CondicionFalsa,
                            IsRequired = input.CondicionFalsaRequerida
                        };

                        await _manager.CreateAsync(delinquencyState);
                    }
                }
            }
        }

        [AbpAuthorize(PermissionNames.Pages_DelinquencyStates_View)]
        public async Task<PagedResultDto<DelinquencyStateListDto>> Search(DelinquencyStateSearchModel filters)
        {
            var res = new PagedResultDto<DelinquencyStateListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new DelinquencyStateListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_DelinquencyStates_Modify)]
        public async Task<DelinquencyStateDto> Update(DelinquencyStateUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("DelinquencyState id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as DelinquencyState;
            if (model == null) throw new EntityNotFoundException("DelinquencyState does not exist in database.");

            model.Condition = input.Condition;
            model.IsRequired = input.IsRequired;
            model.Precedence = input.Precedence;
            model.Rule = input.Rule;
            model.SuccessfulRuleValue = input.SuccessfulRuleValue;
            model.UnsuccessfulRuleValue = input.UnsuccessfulRuleValue;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Delinquency State record");

            return new DelinquencyStateDto(model);
        }
    }
}
