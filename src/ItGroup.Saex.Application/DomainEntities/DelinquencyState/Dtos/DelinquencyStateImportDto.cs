﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class DelinquencyStateImportDto
    {
        public DelinquencyStateImportDto()
        {
        }

        public string CondicionCierta { get; set; }

        public string CondicionFalsa { get; set; }

        public bool CondicionFalsaRequerida { get; set; }

        public string CondicionSeleccion { get; set; }

        public string Precedencia { get; set; }

        public string Regla { get; set; }
    }
}
