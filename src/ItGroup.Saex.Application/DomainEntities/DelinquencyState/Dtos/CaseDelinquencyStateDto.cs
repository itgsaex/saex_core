﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseDelinquencyStateDto : EntityDto<Guid>
    {
        public CaseDelinquencyStateDto()
        {
        }

        public CaseDelinquencyStateDto(CaseDelinquencyState input)
        {
            Id = input.Id;
            Value = input.Value;

            if (input.DelinquencyState != null)
                DelinquencyState = new DelinquencyStateDto(input.DelinquencyState);
        }

        public DelinquencyStateDto DelinquencyState { get; set; }

        public string Value { get; set; }
    }
}
