﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class DelinquencyStateDto : EntityDto<Guid>
    {
        public DelinquencyStateDto()
        {
        }

        public DelinquencyStateDto(DelinquencyState input)
        {
            Id = input.Id;
            Condition = input.Condition;
            Precedence = input.Precedence;
            Rule = input.Rule;
            SuccessfulRuleValue = input.SuccessfulRuleValue;
            UnsuccessfulRuleValue = input.UnsuccessfulRuleValue;
            IsRequired = input.IsRequired;
        }

        public string Condition { get; set; }

        public bool IsRequired { get; set; }

        public string Precedence { get; set; }

        public string Rule { get; set; }

        public string SuccessfulRuleValue { get; set; }

        public string UnsuccessfulRuleValue { get; set; }
    }
}