using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IDelinquencyStateAppService : IApplicationService
    {
        Task<DelinquencyStateDto> Create(DelinquencyStateCreateDto input);

        Task Delete(Guid id);

        Task<DelinquencyStateDto> Get(Guid id);

        Task<List<DelinquencyStateListDto>> GetAllForList();

        Task Import(IFormFile file);

        Task<PagedResultDto<DelinquencyStateListDto>> Search(DelinquencyStateSearchModel filters);

        Task<DelinquencyStateDto> Update(DelinquencyStateUpdateDto input);
    }
}
