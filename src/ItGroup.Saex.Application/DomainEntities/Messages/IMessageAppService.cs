using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IMessageAppService : IApplicationService
    {
        Task<MessageDto> Create(MessageCreateDto input);

        Task Delete(Guid id);

        Task<MessageDto> Get(Guid id);

        Task<PagedResultDto<MessageListDto>> Search(MessageSearchModel filters);

        List<Message> GetMessagesForUser(long? userId);
    }
}