﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.DomainEntities.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Message))]
    public class MessageReplyDto
    {
        public MessageReplyDto()
        {
        }

        public MessageReplyDto(MessageReply input)
        {
            ParentMessageId = input.ParentMessageId;
            Body = input.Body;
            CreationTime = input.CreationTime;
            SenderName = input.Sender.FullName;
        }

        public string Body { get; set; }

        public DateTime CreationTime { get; set; }

        public Guid ParentMessageId { get; set; }

        public string SenderName { get; set; }
    }
}