﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Users.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class MessageReplyInputDto : EntityDto<Guid>
    {
        public MessageReplyInputDto()
        {
        }

        public string Body { get; set; }

        public Guid MessageId { get; set; }
    }
}