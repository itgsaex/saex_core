﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Users.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentMessageDto
    {
        public AgentMessageDto()
        {
        }

        public AgentMessageDto(User user, List<Message> messages)
        {
            if (user != null)
                Recipient = new UserDto(user);

            if (Messages != null && Messages.Any())
                Messages = messages.Select(x => new MessageDto(x)).ToList();
        }

        public List<MessageDto> Messages { get; set; }

        public UserDto Recipient { get; set; }
    }
}