﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class MessageListDto : EntityDto<Guid>
    {
        public MessageListDto()
        {
        }

        public MessageListDto(Message input)
        {
            Id = input.Id;
            Subject = input.Subject;
            Body = input.Body;
            CreationTime = input.CreationTime;

            if (input.Recipient != null)
                Recipient = new UserDto(input.Recipient);

            if (input.Sender != null)
                Sender = new UserDto(input.Sender);

            if (input.Replies != null)
                Replies = input.Replies.Select(x => new MessageReplyDto(x)).OrderBy(y => y.CreationTime).ToList();
        }

        public string Body { get; set; }

        public UserDto Recipient { get; set; }

        public List<MessageReplyDto> Replies { get; set; }

        public UserDto Sender { get; set; }

        public string Subject { get; set; }

        public DateTime CreationTime { get; set; }
    }
}