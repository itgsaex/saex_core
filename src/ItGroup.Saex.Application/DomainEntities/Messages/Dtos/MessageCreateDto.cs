﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Message))]
    public class MessageCreateDto : EntityDto<Guid>
    {
        public MessageCreateDto()
        {
        }

        public string Body { get; set; }

        public long RecipientId { get; set; }

        public string Subject { get; set; }
    }
}
