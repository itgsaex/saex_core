using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.DomainEntities.Messages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class MessageAppService : AppServiceBase, IMessageAppService
    {
        protected readonly AgentManager _agentManager;

        protected readonly MessageManager _manager;

        public MessageAppService(MessageManager manager, AgentManager agentManager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _agentManager = agentManager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Messages_Modify)]
        public async Task<MessageDto> Create(MessageCreateDto input)
        {
            var model = ObjectMapper.Map<Message>(input);
            model.SenderId = (long)AbpSession.UserId;

            var result = await _manager.CreateAsync(model);
            await LogTransaction(model, input, AuditLogOperationType.Create, "Sent a message");

            return new MessageDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Messages_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Message not found for the provided Id.");

            await _manager.DeleteAsync(model);
            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a message");
        }

        [AbpAuthorize(PermissionNames.Pages_Messages_View)]
        public async Task<MessageDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("Message not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a message");

            return new MessageDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Messages_Modify)]
        public async Task<MessageDto> Reply(MessageReplyInputDto input)
        {
            var message = await _manager.GetAsync(input.MessageId);
            if (message == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            var reply = new MessageReply()
            {
                Body = input.Body,
                CreationTime = DateTime.UtcNow,
                ParentMessageId = input.MessageId,
                CreatorUserId = AbpSession.UserId,
                SenderId = (long)AbpSession.UserId
            };

            message.Replies.Add(reply);

            var result = await _manager.UpdateAsync(message);

            await LogTransaction(result, null, AuditLogOperationType.Create, "Replied to a message");

            return new MessageDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Messages_View)]
        public async Task<PagedResultDto<MessageListDto>> Search(MessageSearchModel filters)
        {
            var res = new PagedResultDto<MessageListDto>();

            var results = await _manager.GetFiltered(filters, AbpSession.UserId);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new MessageListDto(x)).ToList();

            return res;
        }

        public List<Message> GetMessagesForUser(long? userId)
        {
            var filters = new MessageSearchModel();
            var results = _manager.GetFiltered(filters, userId);
            return (List<Message>)results.Result.Items;
        }
    }
}
