using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IQuestionAppService : IApplicationService
    {
        Task<QuestionDto> Create(QuestionCreateDto input);

        Task Delete(Guid id);

        Task<QuestionDto> Get(Guid id);

        Task<PagedResultDto<QuestionDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<QuestionDto>> Search(QuestionSearchModel filters);

        Task<QuestionDto> Update(QuestionUpdateDto input);
    }
}