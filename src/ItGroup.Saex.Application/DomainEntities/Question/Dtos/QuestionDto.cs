﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class QuestionDto : EntityDto<Guid>
    {
        public QuestionDto()
        {
        }

        public QuestionDto(Question input)
        {
            Id = input.Id;
            Description = input.Description;
            Type = input.Type;
            IsRequired = input.IsRequired;
            Position = input.Position;

            if (input.Parameters != null && input.Parameters.Any())
                Parameters = input.Parameters.Select(x => new QuestionParameterDto(x)).ToList();

            if (input.ActivityCode != null)
                ActivityCodeId = input.ActivityCode.Id;
        }

        public QuestionDto(ActivityCodeDetail detail)
        {
            Id = detail.Id;
            Description = detail.Question;
            Type = (QuestionType)detail.Type;
            IsRequired = detail.Required.GetValueOrDefault();
            Position = detail.Order.GetValueOrDefault();
            ActivityCodeId = detail.ActivityCode.Id;

            if (detail.Parameters != null)
            {
                var parameters = detail.Parameters.Split(";").ToList();
                Parameters = new List<QuestionParameterDto>();
                if (parameters.Count > 0)
                {
                    foreach (var question in parameters)
                    {
                        var parameterDto = new QuestionParameterDto()
                        {
                            QuestionId = Guid.NewGuid(),
                            Description = question
                        };

                        Parameters.Add(parameterDto);
                    }
                }
            }
        }

        public Guid ActivityCodeId { get; set; }

        public string Description { get; set; }

        public bool IsRequired { get; set; }

        public List<QuestionParameterDto> Parameters { get; set; }

        public int Position { get; set; }

        public QuestionType Type { get; set; }
    }
}