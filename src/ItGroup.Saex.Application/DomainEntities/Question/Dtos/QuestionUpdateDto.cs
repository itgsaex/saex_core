﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Question))]
    public class QuestionUpdateDto : EntityDto<Guid>
    {
        public QuestionUpdateDto()
        {
        }

        public Guid ActivityCodeId { get; set; }

        public Guid CaseEventId { get; set; }

        public string Description { get; set; }

        public List<QuestionParameterCreateDto> Parameters { get; set; }

        public Guid? ParentQuestionId { get; set; }
    }
}