﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    public class QuestionParameterUpdateDto : EntityDto<Guid>
    {
        public QuestionParameterUpdateDto()
        {
        }

        public string Description { get; set; }

        public Guid QuestionId { get; set; }

        public string Value { get; set; }
    }
}