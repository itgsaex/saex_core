﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class QuestionParameterListDto : EntityDto<Guid>
    {
        public QuestionParameterListDto() { }

        public QuestionParameterListDto(QuestionParameter input)
        {
            QuestionId = input.QuestionId;
            Description = input.Description;
            Value = input.Value;
        }

        public Guid QuestionId { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }
    }

}