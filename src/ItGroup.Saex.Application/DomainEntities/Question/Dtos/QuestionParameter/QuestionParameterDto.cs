﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class QuestionParameterDto : EntityDto<Guid>
    {
        public QuestionParameterDto()
        {
        }

        public QuestionParameterDto(QuestionParameter input)
        {
            QuestionId = input.QuestionId;
            Description = input.Description;
            Value = input.Value;
        }

        public string Description { get; set; }

        public bool IsDefault { get; set; }

        public Guid QuestionId { get; set; }

        public string Value { get; set; }
    }
}
