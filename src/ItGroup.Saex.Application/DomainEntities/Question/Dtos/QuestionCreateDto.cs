﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Question))]
    public class QuestionCreateDto : EntityDto<Guid>
    {
        public QuestionCreateDto()
        {
            Parameters = new List<QuestionParameterCreateDto>();
        }

        public Guid? ActivityCodeId { get; set; }

        public Guid? CaseEventId { get; set; }

        public string DefaultOption { get; set; }

        public string Description { get; set; }

        public bool IsRequired { get; set; }

        public List<QuestionParameterCreateDto> Parameters { get; set; }

        public Guid? ParentQuestionId { get; set; }

        public int Position { get; set; }

        public QuestionType Type { get; set; }
    }
}
