using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class QuestionAppService : AppServiceBase, IQuestionAppService
    {
        protected new readonly QuestionManager _manager;

        public QuestionAppService(QuestionManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_Modify)]
        public async Task<QuestionDto> Create(QuestionCreateDto input)
        {
            var model = ObjectMapper.Map<Question>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Question");

            return new QuestionDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Question not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Question");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_View)]
        public async Task<QuestionDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("Question not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Question");

            return new QuestionDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_View)]
        public async Task<PagedResultDto<QuestionDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<QuestionDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<Question>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new QuestionDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_View)]
        public async Task<PagedResultDto<QuestionDto>> Search(QuestionSearchModel filters)
        {
            var res = new PagedResultDto<QuestionDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new QuestionDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_Modify)]
        public async Task<QuestionDto> Update(QuestionUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("Question id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as Question;
            if (model == null) throw new EntityNotFoundException("Question does not exist in database.");

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Question");

            return new QuestionDto(model);
        }
    }
}
