﻿using Abp.Application.Services;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISortFieldsCaseService : IApplicationService
    {
        List<SortFieldsCase> GetSortFieldsCase();
    }
}
