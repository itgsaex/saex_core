﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using ItGroup.Saex.Manager.DomainEntities;
using System.Collections.Generic;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class SortFieldsCaseService : ISortFieldsCaseService
    {
        protected new readonly SortFieldsCaseManager _manager;
        public SortFieldsCaseService(SortFieldsCaseManager manager) : base()
        {
            _manager = manager;
        }

        public List<SortFieldsCase> GetSortFieldsCase()
        {
            var result = _manager.GetAll().ToList();
            return result;
        }
    }
}
