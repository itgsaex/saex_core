using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AdjusterStateAppService : AppServiceBase, IAdjusterStateAppService
    {
        protected new readonly MacroStateManager _macroStateManager;

        protected new readonly AdjusterStateManager _manager;

        public AdjusterStateAppService(AdjusterStateManager manager, MacroStateManager macroStateManager, AuditLogManager auditManager
) : base()
        {
            _manager = manager;
            _macroStateManager = macroStateManager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_AdjusterStates_Modify)]
        public async Task<AdjusterStateDto> Create(AdjusterStateCreateDto input)
        {
            var model = ObjectMapper.Map<AdjusterState>(input);

            var macroState = await _macroStateManager.GetAsync(input.MacroStateId);
            if (macroState == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            model.MacroStateId = macroState.Id;
            model.MacroState = macroState;

            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created an Adjuster State record");

            return new AdjusterStateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_AdjusterStates_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Adjuster State not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted an Adjuster State record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_AdjusterStates_View)]
        public async Task<AdjusterStateDto> Get(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("AdjusterState not found for the provided Id");

            await LogTransaction(model, null, AuditLogOperationType.View, "Viewed an Adjuster State record");

            return new AdjusterStateDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_AdjusterStates_View)]
        public async Task<PagedResultDto<AdjusterStateDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<AdjusterStateDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<AdjusterState>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new AdjusterStateDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_AdjusterStates_View)]
        public async Task<PagedResultDto<AdjusterStateDto>> Search(AdjusterStateSearchModel filters)
        {
            var res = new PagedResultDto<AdjusterStateDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new AdjusterStateDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_AdjusterStates_Modify)]
        public async Task<AdjusterStateDto> Update(AdjusterStateUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("AdjusterState id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as AdjusterState;
            if (model == null) throw new EntityNotFoundException("AdjusterState does not exist in database.");

            model.Code = input.Code;
            model.Description = input.Description;

            var macroState = await _macroStateManager.GetAsync(input.MacroStateId);
            if (macroState == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            model.MacroStateId = macroState.Id;

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated an Adjuster State record");

            return new AdjusterStateDto(model);
        }
    }
}
