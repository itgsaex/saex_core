﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(AdjusterState))]
    public class AdjusterStateUpdateDto : EntityDto<Guid>
    {
        public AdjusterStateUpdateDto() { }

        public string Code { get; set; }

        public string Description { get; set; }

        public bool IsPromise { get; set; }

        public Guid MacroStateId { get; set; }
    }
}