﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class AdjusterStateListDto : EntityDto<Guid>
    {
        public AdjusterStateListDto()
        {
        }

        public AdjusterStateListDto(AdjusterState input)
        {
            Id = input.Id;
            Code = input.Code;
            Description = input.Description;
            MacroState = new MacroStateDto(input.MacroState);
            IsPromise = input.IsPromise;
        }

        public string Code { get; set; }

        public string Description { get; set; }

        public bool IsPromise { get; set; }

        public MacroStateDto MacroState { get; set; }
    }
}