using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAdjusterStateAppService : IApplicationService
    {
        Task<AdjusterStateDto> Create(AdjusterStateCreateDto input);

        Task<AdjusterStateDto> Update(AdjusterStateUpdateDto input);

        Task Delete(Guid id);

        Task<AdjusterStateDto> Get(Guid id);

        Task<PagedResultDto<AdjusterStateDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<AdjusterStateDto>> Search(AdjusterStateSearchModel filters);
    }
}

