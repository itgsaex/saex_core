using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IPostalCodeAppService : IApplicationService
    {
        Task<PostalCodeDto> Create(PostalCodeCreateDto input);

        Task Delete(Guid id);

        Task<PostalCodeDto> Get(Guid id);

        Task<List<PostalCodeListDto>> GetAllForList();

        Task<PagedResultDto<PostalCodeListDto>> Search(PostalCodeSearchModel filters);

        Task<PostalCodeDto> Update(PostalCodeUpdateDto input);
    }
}
