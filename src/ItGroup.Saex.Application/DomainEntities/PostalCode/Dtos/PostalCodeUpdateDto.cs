﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(PostalCode))]
    public class PostalCodeUpdateDto : EntityDto<Guid>
    {
        public PostalCodeUpdateDto()
        {
        }

        public PostalCodeUpdateDto(PostalCode input)
        {
            Id = input.Id;
            Code = input.Code;
            Area = input.Area;
        }

        public string Area { get; set; }

        public string Code { get; set; }
    }
}