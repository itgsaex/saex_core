﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(PostalCode))]
    public class PostalCodeCreateDto : EntityDto<Guid>
    {
        public PostalCodeCreateDto()
        {
        }

        public string Area { get; set; }

        public string Code { get; set; }
    }
}
