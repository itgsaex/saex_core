﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class PostalCodeDto : EntityDto<Guid>
    {
        public PostalCodeDto()
        {
        }

        public PostalCodeDto(PostalCode input)
        {
            Id = input.Id;
            Code = input.Code;
            Area = input.Area;
        }

        public string Area { get; set; }

        public string Code { get; set; }
    }
}
