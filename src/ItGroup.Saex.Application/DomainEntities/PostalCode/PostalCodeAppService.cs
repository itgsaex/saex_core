using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class PostalCodeAppService : AppServiceBase, IPostalCodeAppService
    {
        protected new readonly PostalCodeManager _manager;

        public PostalCodeAppService(PostalCodeManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_PostalCodes_Modify)]
        public async Task<PostalCodeDto> Create(PostalCodeCreateDto input)
        {
            var postalCodeInDB = _manager.GetByCode(input.Code);
            if (postalCodeInDB != null) throw new UserFriendlyException(L("PostalCodeAlreadySaved"));


            var model = ObjectMapper.Map<PostalCode>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Postal Code");

            return new PostalCodeDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_PostalCodes_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("PostalCode not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Postal Code");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_PostalCodes_View)]
        public async Task<PostalCodeDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("PostalCode not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Postal Code");

            return new PostalCodeDto(result);
        }

        [AbpAuthorize]
        public async Task<List<PostalCodeListDto>> GetAllForList()
        {
            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<PostalCode>;

            return results.Select(x => new PostalCodeListDto(x)).ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_PostalCodes_View)]
        public async Task<PagedResultDto<PostalCodeListDto>> Search(PostalCodeSearchModel filters)
        {
            var res = new PagedResultDto<PostalCodeListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new PostalCodeListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_PostalCodes_Modify)]
        public async Task<PostalCodeDto> Update(PostalCodeUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("PostalCode id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as PostalCode;

            if (model == null) throw new EntityNotFoundException("PostalCode does not exist in database.");

            model.Code = input.Code;
            model.Area = input.Area;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a postal code");

            return new PostalCodeDto(model);
        }
    }
}
