﻿using ItGroup.Saex.Manager.DomainEntities;
using System.Collections.Generic;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CategoryService : ICategoryService
    {
        protected new readonly CategoryManager _manager;
        public CategoryService(CategoryManager manager) : base()
        {
            _manager = manager;
        }

        public List<Category> GetAllCategories()
        {
            var result = _manager.GetAllCategories().OrderBy(o => o.CategoryId).ToList();
            return result;
        }
    }
}
