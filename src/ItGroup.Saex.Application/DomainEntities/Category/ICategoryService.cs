﻿using Abp.Application.Services;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICategoryService : IApplicationService
    {
        List<Category> GetAllCategories();
    }
}
