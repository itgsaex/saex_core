using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IRegionAppService : IApplicationService
    {
        Task Create(RegionCreateDto input);

        Task Delete(Guid id);

        Task<RegionDto> Get(Guid id);

        Task<List<RegionListDto>> GetAllForList();

        Task<PagedResultDto<RegionListDto>> Search(RegionSearchModel filters);

        Task Update(RegionUpdateDto input);
    }
}