﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class AssignRegionDto
    {
        public AssignRegionDto()
        {
        }

        public AssignRegionDto(Agent agent)
        {
            Agent = new AgentDto(agent);

            if (agent.Regions != null && agent.Regions.Any())
                Regions = agent.Regions.Select(x => new RegionDto(x.Region)).ToList();

            if (agent.Lists != null && agent.Lists.Any())
                Lists = agent.Lists.Select(x => new ListDto(x.List)).ToList();

            if (agent.WorkingHour != null)
            {
                WorkingHour = new WorkingHourDto(agent.WorkingHour);
            }
        }

        public AgentDto Agent { get; set; }

        public List<RegionDto> Regions { get; set; }

        public List<ListDto> Lists { get; set; }

        public WorkingHourDto WorkingHour { get; set; }
    }
}