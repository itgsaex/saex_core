﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    public class AssignRegionCreateDto : EntityDto<Guid>
    {
        public AssignRegionCreateDto()
        {
        }

        public List<Guid> AgentIds { get; set; }

        public List<Guid> RegionIds { get; set; }

        public List<Guid> ListIds { get; set; }

        public Guid? WorkingHourId { get; set; }
    }
}