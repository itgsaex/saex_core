﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class RegionDto : EntityDto<Guid>
    {
        public RegionDto()
        {
            Routes = new List<RouteDto>();
        }

        public RegionDto(Region input)
        {
            Id = input.Id;
            Name = input.Name;
            ForReport = input.ForReport;
            RegionId = input.RegionId.GetValueOrDefault();

            if (input.Routes != null && input.Routes.Any())
                Routes = input.Routes.Select(x => new RouteDto(x.Route)).ToList();
        }

        public string Name { get; set; }

        public bool ForReport { get; set; }

        public List<RouteDto> Routes { get; set; }

        public int RegionId { get; set; }
    }
}