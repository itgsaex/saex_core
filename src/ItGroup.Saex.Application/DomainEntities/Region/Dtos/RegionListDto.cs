﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class RegionListDto : EntityDto<Guid>
    {
        public RegionListDto()
        {
        }

        public RegionListDto(Region input, bool? isSelected = null)
        {
            Id = input.Id;

            Name = input.Name;
            ForReport = input.ForReport;
            RegionID = input.RegionId;

            if (input.Routes != null && input.Routes.Any())
                Routes = input.Routes.Select(x => new RouteDto(x.Route)).ToList();

            if (isSelected != null)
                IsSelected = (bool)isSelected;
        }

        public bool IsSelected { get; set; }

        public string Name { get; set; }

        public bool ForReport { get; set; }

        public List<RouteDto> Routes { get; set; }

        public int? RegionID { get; set; }
    }

    public class ListTableDto : EntityDto<Guid>
    {
        public ListTableDto()
        {
        }

        public ListTableDto(ListDto input, bool? isSelected = null)
        {
            Id = input.Id;

            Name = input.Name;

            if (isSelected != null)
                IsSelected = (bool)isSelected;
        }

        public bool IsSelected { get; set; }

        public string Name { get; set; }
    }
}
