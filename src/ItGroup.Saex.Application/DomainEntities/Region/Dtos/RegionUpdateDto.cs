﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Region))]
    public class RegionUpdateDto : EntityDto<Guid>
    {
        public RegionUpdateDto()
        {
        }

        public string Name { get; set; }

        public bool ForReport { get; set; }

        public List<Guid> Routes { get; set; }
    }
}