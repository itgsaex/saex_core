using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Sessions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class RegionAppService : AppServiceBase, IRegionAppService
    {
        protected readonly AgentManager _agentManager;

        protected readonly ListManager _listManager;

        protected readonly RegionManager _manager;

        protected readonly RouteManager _routeManager;

        protected readonly WorkingHourManager _workingHourManager;

        protected readonly SessionAppService _sessionAppService;

        public RegionAppService(RegionManager manager,
                                AuditLogManager auditManager,
                                RouteManager routeManager,
                                AgentManager agentManager,
                                ListManager listManager,
                                WorkingHourManager workingHourManager,
                                SessionAppService sessionAppService
         ) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
            _routeManager = routeManager;
            _agentManager = agentManager;
            _listManager = listManager;
            _workingHourManager = workingHourManager;
            _sessionAppService = sessionAppService;
        }

        [AbpAuthorize(PermissionNames.Pages_Regions_Modify)]
        public async Task AssignRegion(AssignRegionCreateDto input)
        {
            if (input.AgentIds == null || !input.AgentIds.Any())
                throw new EntityNotFoundException(L("IdIsNullError"));

            foreach (var id in input.AgentIds)
            {
                var agent = await _agentManager.GetAsync(id);
                if (agent == null)
                    throw new EntityNotFoundException(L("RecordNotFoundError"));

                //Remove every existing assigned region
                var existing = await _manager.GetAssignedByAgentId(id);
                foreach (var item in existing)
                {
                    await _manager.DeleteAssignedAsync(item);
                }

                //Assign the selected regions
                var agentRegions = new List<AgentRegion>();
                foreach (var regionId in input.RegionIds)
                {
                    var region = await _manager.GetAsync(regionId);
                    if (region == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

                    var ar = new AgentRegion()
                    {
                        RegionId = region.Id,
                        AgentId = agent.Id,
                        CreationDate = DateTime.UtcNow,
                        CreatorUserId = (long)AbpSession.UserId,
                    };

                    agentRegions.Add(ar);
                }

                //Remove every existing assigned lists
                var existingLists = await _listManager.GetAssignedByAgentId(id);
                foreach (var item in existingLists)
                {
                    await _listManager.DeleteAssignedAsync(item);
                }

                //Assign the selected lists
                var agentLists = new List<AgentList>();
                foreach (var listId in input.ListIds)
                {
                    var list = await _listManager.GetAsync(listId);
                    if (list == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

                    var ar = new AgentList()
                    {
                        ListId = list.Id,
                        AgentId = agent.Id,
                        CreationDate = DateTime.UtcNow,
                        CreatorUserId = (long)AbpSession.UserId,
                    };

                    agentLists.Add(ar);
                }

                agent.Lists = agentLists;
                agent.Regions = agentRegions;

                if (input.WorkingHourId.HasValue)
                {
                    agent.WorkingHourId = input.WorkingHourId.Value;
                    var workingHour = await _workingHourManager.GetAsync(input.WorkingHourId.Value);
                    agent.WorkingHour = workingHour;
                }

                await _agentManager.UpdateAsync(agent);
                await LogTransaction(agent, input, AuditLogOperationType.Create, "Assigned a Region");
            }
        }

        [AbpAuthorize(PermissionNames.Pages_Regions_Modify)]
        public async Task Create(RegionCreateDto input)
        {
            var region = new Region()
            {
                Name = input.Name,
                ForReport = input.ForReport
            };

            var routes = new List<RegionRoute>();
            foreach (var id in input.Routes)
            {
                var route = await _routeManager.GetAsync(id);

                routes.Add(new RegionRoute() { RouteId = route.Id });
            }

            region.Routes = routes;
            var regionId = _manager.GetAll().Max(r => r.RegionId.GetValueOrDefault()) + 1;
            region.RegionId = regionId;

            await _manager.CreateAsync(region);
            await LogTransaction(region, input, AuditLogOperationType.Create, "Created a Region record");
        }

        [AbpAuthorize(PermissionNames.Pages_Regions_Modify)]
        public async Task Delete(Guid id)
        {
            var res = await _manager.GetAsync(id);
            if (res == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await _manager.DeleteAsync(res);
            await LogTransaction(res, null, AuditLogOperationType.Delete, "Deleted a Region record");
        }

        [AbpAuthorize(PermissionNames.Pages_Regions_View)]
        public async Task<RegionDto> Get(Guid id)
        {
            var res = await _manager.GetAsync(id);
            if (res == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(res, null, AuditLogOperationType.View, "Viewed a Region record");
            return new RegionDto(res);
        }

        [AbpAuthorize(PermissionNames.Pages_Regions_View)]
        public async Task<PagedResultDto<AssignRegionDto>> GetAllAssignedRegions(AssignRegionSearchModel input)
        {
            var result = new PagedResultDto<AssignRegionDto>();

            var assignedRegions = await _agentManager.GetAllQuery()
                                            .Where(x => x.Regions.Any() || x.Lists.Any())
                                            .OrderByDescending(x =>
                                               x.Regions.Select(y => y.CreationDate).FirstOrDefault())
                                            .ToListAsync();

            result.TotalCount = assignedRegions.Count();
            result.Items = assignedRegions.Select(x => new AssignRegionDto(x)).ToList();

            return result;
        }

        [AbpAuthorize]
        public async Task<List<RegionListDto>> GetAllForList()
        {
            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<Region>;

            return results.Select(x => new RegionListDto(x)).ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_Regions_View)]
        public async Task<AssignRegionDto> GetAssignedRegions(Guid agentId)
        {
            if (agentId == null || agentId == Guid.Empty)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var agent = await _agentManager.GetAsync(agentId);
            if (agent == null)
                throw new EntityNotFoundException(L("RecordNotFoundError"));

            return new AssignRegionDto(agent);
        }

        [AbpAuthorize(PermissionNames.Pages_Regions_View)]
        public async Task<PagedResultDto<RegionListDto>> Search(RegionSearchModel filters)
        {
            var res = new PagedResultDto<RegionListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new RegionListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Regions_Modify)]
        public async Task Update(RegionUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as Region;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.Name = input.Name;
            model.ForReport = input.ForReport;

            var routes = new List<RegionRoute>();
            foreach (var id in input.Routes)
            {
                var route = await _routeManager.GetAsync(id);
                routes.Add(new RegionRoute() { RouteId = route.Id });
            }

            model.Routes = routes;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Region record");
        }

        [AbpAuthorize(PermissionNames.Pages_Regions_Modify)]
        public async Task UpdateAssignRegion(AssignRegionUpdateDto input)
        {
            if (input.AgentId == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _agentManager.GetAsync(input.AgentId);
            var oldModel = model.Clone() as Agent;

            if (model == null)
                throw new EntityNotFoundException(L("RecordNotFoundError"));


            //Remove every existing assigned region
            var existing = await _manager.GetAssignedByAgentId(model.Id);
            foreach (var item in existing)
            {
                await _manager.DeleteAssignedAsync(item);
            }

            //Assign the selected regions
            var agentRegions = new List<AgentRegion>();
            foreach (var regionId in input.RegionIds)
            {
                var region = await _manager.GetAsync(regionId);
                if (region == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

                var ar = new AgentRegion()
                {
                    RegionId = region.Id,
                    AgentId = model.Id,
                    CreationDate = DateTime.UtcNow,
                    CreatorUserId = (long)AbpSession.UserId,
                };

                agentRegions.Add(ar);
            }

            //Remove every existing assigned lists
            var existingLists = await _listManager.GetAssignedByAgentId(model.Id);
            foreach (var item in existingLists)
            {
                await _listManager.DeleteAssignedAsync(item);
            }

            //Assign the selected lists
            var agentLists = new List<AgentList>();
            foreach (var listId in input.ListIds)
            {
                var list = await _listManager.GetAsync(listId);
                if (list == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

                var ar = new AgentList()
                {
                    ListId = list.Id,
                    AgentId = model.Id,
                    CreationDate = DateTime.UtcNow,
                    CreatorUserId = (long)AbpSession.UserId,
                };

                agentLists.Add(ar);
            }

            model.Regions = agentRegions;
            model.Lists = agentLists;

            if (input.WorkingHourId.HasValue)
            {
                model.WorkingHourId = input.WorkingHourId.Value;
                var workingHour = await _workingHourManager.GetAsync(input.WorkingHourId.Value);
                model.WorkingHour = workingHour;
            }

            await _agentManager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated an Assigned Region record");
        }
    }
}
