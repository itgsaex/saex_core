﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(MacroState))]
    public class MacroStateUpdateDto : EntityDto<Guid>
    {
        public MacroStateUpdateDto()
        {
        }

        public MacroStateUpdateDto(MacroState input)
        {
            this.Id = input.Id;
            this.Code = input.Code;
            this.Description = input.Description;
        }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}