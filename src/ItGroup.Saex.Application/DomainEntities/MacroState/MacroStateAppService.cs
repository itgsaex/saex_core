using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class MacroStateAppService : AppServiceBase, IMacroStateAppService
    {
        protected new readonly MacroStateManager _manager;

        public MacroStateAppService(MacroStateManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_MacroStates_Modify)]
        public async Task<MacroStateDto> Create(MacroStateCreateDto input)
        {
            var model = ObjectMapper.Map<MacroState>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Macro State record");

            return new MacroStateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_MacroStates_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Macro State not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Macro State record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_MacroStates_View)]
        public async Task<MacroStateDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("MacroState not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Macro State record");

            return new MacroStateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_MacroStates_View)]
        public async Task<PagedResultDto<MacroStateDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<MacroStateDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<MacroState>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new MacroStateDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_MacroStates_View)]
        public async Task<List<MacroStateListDto>> GetAllForList()
        {
            return await _manager.GetAll().Select(x => new MacroStateListDto(x)).ToListAsync();
        }

        [AbpAuthorize(PermissionNames.Pages_MacroStates_View)]
        public async Task<PagedResultDto<MacroStateListDto>> Search(MacroStateSearchModel filters)
        {
            var res = new PagedResultDto<MacroStateListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new MacroStateListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_MacroStates_Modify)]
        public async Task<MacroStateDto> Update(MacroStateUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("MacroState id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as MacroState;
            if (model == null) throw new EntityNotFoundException("MacroState does not exist in database.");
            model.Code = input.Code;
            model.Description = input.Description;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a macro state record");

            return new MacroStateDto(model);
        }
    }
}
