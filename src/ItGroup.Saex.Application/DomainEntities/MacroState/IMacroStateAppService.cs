using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IMacroStateAppService : IApplicationService
    {
        Task<MacroStateDto> Create(MacroStateCreateDto input);

        Task Delete(Guid id);

        Task<MacroStateDto> Get(Guid id);

        Task<PagedResultDto<MacroStateDto>> GetAll(PagedResultRequestDto paging);

        Task<List<MacroStateListDto>> GetAllForList();

        Task<PagedResultDto<MacroStateListDto>> Search(MacroStateSearchModel filters);

        Task<MacroStateDto> Update(MacroStateUpdateDto input);
    }
}