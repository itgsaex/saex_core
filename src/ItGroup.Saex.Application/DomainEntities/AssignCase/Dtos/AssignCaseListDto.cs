﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Users.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AssignCaseListDto : EntityDto<Guid>
    {
        public AssignCaseListDto()
        {
        }

        public AssignCaseListDto(AgentCase input)
        {
            Agent = input.Agent;

            Case = input.Case;

            IsActive = input.IsActive;

            Type = input.Type;

            CreationTime = input.CreationTime;

            if (input.TransferedBy != null)
                TransferedBy = new UserDto(input.TransferedBy);
        }

        public virtual Agent Agent { get; set; }

        public virtual Case Case { get; set; }

        public DateTime CreationTime { get; set; }

        public bool IsActive { get; set; }

        public UserDto TransferedBy { get; set; }

        public TransferType Type { get; set; }
    }
}
