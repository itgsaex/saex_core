﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(AgentCase))]
    public class AssignCaseCreateDto : EntityDto<Guid>
    {
        public AssignCaseCreateDto()
        {
        }

        public Guid AgentId { get; set; }

        public Guid CaseId { get; set; }

        public TransferType Type { get; set; }
    }
}
