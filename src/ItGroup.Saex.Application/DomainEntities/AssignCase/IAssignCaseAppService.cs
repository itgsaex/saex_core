using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAssignCaseAppService : IApplicationService
    {
        Task<AssignCaseDto> Create(AssignCaseCreateDto input);

        Task Delete(Guid id);

        Task<AssignCaseDto> Get(Guid id);

        Task<PagedResultDto<AssignCaseDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<AssignCaseDto>> Search(AgentCaseSearchModel filters);

        Task<AssignCaseDto> Update(AssignCaseUpdateDto input);

        Task SaveTransfer(TransferAgentModel model);
    }
}
