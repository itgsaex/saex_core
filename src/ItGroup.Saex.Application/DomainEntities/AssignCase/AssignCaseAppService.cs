using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Sessions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class TransferAgentModel
    {
        public Guid FromAgentId { get; set; } 
        public Guid ToAgentId { get; set; } 
        public List<Guid> SelectedCases { get; set; } 
        public TransferType TransferType { get; set; }
    }

    public class AssignCaseAppService : AppServiceBase, IAssignCaseAppService
    {
        protected readonly AgentCaseManager _manager;
        private readonly IRepository<AgentTransfer, Guid> _agentTransferRepository;

        public AssignCaseAppService(AgentCaseManager manager, AuditLogManager auditManager, IRepository<AgentTransfer, Guid> agentTransferRepository) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
            _agentTransferRepository = agentTransferRepository;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<AssignCaseDto> Create(AssignCaseCreateDto input)
        {
            var model = ObjectMapper.Map<AgentCase>(input);

            model.TransferedBy = await GetCurrentUserAsync();

            await EvalTransferType(input.Type, input.CaseId);

            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created an Assigned Case record.");

            return new AssignCaseDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_AssignCases_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted an Assigned Case record.");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_AssignCases_View)]
        public async Task<AssignCaseDto> Get(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            return new AssignCaseDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_AssignCases_View)]
        public async Task<PagedResultDto<AssignCaseDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<AssignCaseDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<AgentCase>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new AssignCaseDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_AssignCases_View)]
        public async Task<PagedResultDto<AssignCaseDto>> Search(AgentCaseSearchModel filters)
        {
            var res = new PagedResultDto<AssignCaseDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new AssignCaseDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_AssignCases_Modify)]
        public async Task<AssignCaseDto> Update(AssignCaseUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as AgentCase;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.AgentId = input.AgentId;
            model.CaseId = input.CaseId;
            model.IsActive = input.IsActive;
            model.Type = input.Type;
            model.TransferedBy = await GetCurrentUserAsync();

            await EvalTransferType(input.Type, input.CaseId);

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated an Assigned Case record");

            return new AssignCaseDto(model);
        }

        private async Task EvalTransferType(TransferType transferType, Guid caseId)
        {
            if (transferType == TransferType.Transfer)
            {
                //If the case was transfered, remove it from other agents.
                var existing = await _manager.GetAllQuery().Where(x => x.CaseId == caseId && x.IsActive).ToListAsync();
                foreach (var assignedCase in existing)
                {
                    assignedCase.IsActive = false;
                    await _manager.UpdateAsync(assignedCase);
                }
            }
        }

        public async Task SaveTransfer(TransferAgentModel model)
        {
            foreach (var caseId in model.SelectedCases)
            {
                var agentTransfer = new AgentTransfer();
                
                agentTransfer.FromAgentId = model.FromAgentId;
                agentTransfer.ToAgentId = model.ToAgentId;
                agentTransfer.CaseId = caseId;
                agentTransfer.TransferType = model.TransferType;

                await _agentTransferRepository.InsertAsync(agentTransfer);
            }
        }
    }
}