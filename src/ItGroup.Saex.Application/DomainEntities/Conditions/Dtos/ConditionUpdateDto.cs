﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Condition))]
    public class ConditionUpdateDto : EntityDto<Guid>
    {
        public ConditionUpdateDto()
        {
        }

        public string Description { get; set; }

        public Guid ProductId { get; set; }

        public string Value { get; set; }
    }
}