﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class ConditionDto : EntityDto<Guid>
    {
        public ConditionDto()
        {
        }

        public ConditionDto(Condition input)
        {
            Id = input.Id;
            Description = input.Description;
            Value = input.Value;

            if (input.Product != null)
                Product = new ProductDto(input.Product);
        }

        public string Description { get; set; }

        public ProductDto Product { get; set; }

        public string Value { get; set; }
    }
}