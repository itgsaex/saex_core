using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IConditionAppService : IApplicationService
    {
        Task<ConditionDto> Create(ConditionCreateDto input);

        Task Delete(Guid id);

        Task<ConditionDto> Get(Guid id);

        Task<PagedResultDto<ConditionDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<ConditionListDto>> Search(ConditionSearchModel filters);

        Task<ConditionDto> Update(ConditionUpdateDto input);
    }
}