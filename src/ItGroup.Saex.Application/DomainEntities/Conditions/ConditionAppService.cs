using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using ItGroup.Saex.Authorization;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ConditionAppService : AppServiceBase, IConditionAppService
    {
        protected readonly ConditionManager _manager;

        public ConditionAppService(ConditionManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Conditions_Modify)]
        public async Task<ConditionDto> Create(ConditionCreateDto input)
        {
            var model = ObjectMapper.Map<Condition>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Condition record");

            return new ConditionDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Conditions_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await _manager.DeleteAsync(model);
            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Condition record");
        }

        [AbpAuthorize(PermissionNames.Pages_Conditions_View)]
        public async Task<ConditionDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Condition record");

            return new ConditionDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Conditions_View)]
        public async Task<PagedResultDto<ConditionDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ConditionDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<Condition>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ConditionDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Conditions_View)]
        public async Task<PagedResultDto<ConditionListDto>> Search(ConditionSearchModel filters)
        {
            var res = new PagedResultDto<ConditionListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ConditionListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Conditions_Modify)]
        public async Task<ConditionDto> Update(ConditionUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("Condition id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as Condition;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.ProductId = input.ProductId;
            model.Value = input.Value;
            model.Description = input.Description;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Condition record");

            return new ConditionDto(model);
        }
    }
}
