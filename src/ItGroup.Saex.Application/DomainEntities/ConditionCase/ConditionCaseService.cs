﻿using ItGroup.Saex.Manager.DomainEntities;
using System.Collections.Generic;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class ConditionCaseService : IConditionCaseService
    {
        protected new readonly CaseConditionManager _manager;
        public ConditionCaseService(CaseConditionManager manager) : base()
        {
            _manager = manager;
        }

        public List<ConditionCase> GetAllConditionCase()
        {
            var results = _manager.GetConditionCases().ToList();
            return results;
        }
    }
}
