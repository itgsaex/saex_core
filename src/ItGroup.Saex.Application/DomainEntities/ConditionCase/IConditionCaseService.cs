﻿using Abp.Application.Services;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public interface IConditionCaseService : IApplicationService
    {
        List<ConditionCase> GetAllConditionCase();
    }
}
