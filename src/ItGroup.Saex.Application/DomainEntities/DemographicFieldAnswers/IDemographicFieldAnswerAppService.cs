﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IDemographicFieldAnswerAppService : IApplicationService
    {
        Task Delete(Guid id);

        Task<List<DemographicFieldAnswer>> GetAllDemographicFieldAnswerss();

        Task AddDemographicFieldAnswer(DemographicFieldAnswer demographicField);
    }
}
