﻿using Abp.Authorization;
using Abp.UI;
using ItGroup.Saex.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldAnswerAppService : AppServiceBase, IDemographicFieldAnswerAppService
    {
        protected readonly DemographicFieldAnswersManager _manager;

        public DemographicFieldAnswerAppService(DemographicFieldAnswersManager manager)
        {
            _manager = manager;
        }

        [AbpAuthorize(PermissionNames.Pages_DemographicFields)]
        public async Task Delete(Guid id)
        {
            var res = await _manager.GetAsync(id);

            if (res is null)
                throw new UserFriendlyException(L("RecordNotFoundError"));

            await _manager.DeleteAsync(res);

            await LogTransaction(res, null, AuditLogOperationType.Delete, "Deleted a Demographic Field Answer record");
        }

        public async Task<List<DemographicFieldAnswer>> GetAllDemographicFieldAnswerss()
        {
            var result = _manager.GetAllQuery();
            return result.ToList();
        }

        public async Task AddDemographicFieldAnswer(DemographicFieldAnswer demographicField)
        {
            await _manager.CreateAsync(demographicField);
        }
    }
}
