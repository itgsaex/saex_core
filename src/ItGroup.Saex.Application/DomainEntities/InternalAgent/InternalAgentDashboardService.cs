﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.Manager.DomainEntities;
using ItGroup.Saex.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public class SaveInternalAgentRequest
    {
        public long InternalAgentId { get; set; }

        public List<InternalAgentCommentDto> Comments { get; set; }
        public List<InternalAgentPromiseDto> Promises { get; set; }
    }

    public class InternalAgentCommentDto
    {
        public string CaseID { get; set; } 
        public string Comment { get; set; }
    }

    public class InternalAgentPromiseDto
    {
        public string CaseID { get; set; }
        public string Promise { get; set; }
    }

    public class InternalAgentDashboardService : IInternalAgentDashboardService
    {
        private readonly IRepository<Agent, Guid> _agentRepository;
        private readonly IRepository<Region, Guid> _regionRepository;
        private readonly IRepository<Route, Guid> _routeRepository;
        private readonly IRepository<PostalCode, Guid> _postalCodeRepository;
        private readonly IRepository<ActivityCode, Guid> _activityCodeRepository;
        private readonly IRepository<Alert, int> _alertRepository;
        private readonly IRepository<MacroState, Guid> _macroStateRepository;
        private readonly IRepository<SystemSubState, Guid> _subStateRepository;
        private readonly ICaseRecentActivityRepository _caseRecentActivityRepository;
        private readonly IRepository<InternalAgentComment, Guid> _internalAgentCommentRepository;
        private readonly IRepository<InternalAgentPromise, Guid> _internalAgentPromiseRepository;
        private readonly IRepository<Case, Guid> _caseRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<AgentRegion, Guid> _agentRegionRepository;

        public InternalAgentDashboardService(IRepository<Agent, Guid> agentRepository, 
                                             IRepository<Region, Guid> regionRepository,
                                             IRepository<Route, Guid> routeRepository,
                                             IRepository<PostalCode, Guid> postalCodeRepository,
                                             IRepository<ActivityCode, Guid> activityCodeRepository,
                                             IRepository<Alert, int> alertRepository,
                                             IRepository<MacroState, Guid> macroStateRepository,
                                             IRepository<SystemSubState, Guid> subStateRepository,
                                             ICaseRecentActivityRepository caseRecentActivityRepository,
                                             IRepository<InternalAgentComment, Guid> internalAgentCommentRepository,
                                             IRepository<InternalAgentPromise, Guid> internalAgentPromiseRepository,
                                             IRepository<Case, Guid> caseRepository,
                                             IRepository<User, long> userRepository,
                                             IRepository<AgentRegion, Guid> agentRegionRepository)
        {
            _agentRepository = agentRepository;
            _regionRepository = regionRepository;
            _routeRepository = routeRepository;
            _postalCodeRepository = postalCodeRepository;
            _alertRepository = alertRepository;
            _macroStateRepository = macroStateRepository;
            _subStateRepository = subStateRepository;
            _activityCodeRepository = activityCodeRepository;
            _caseRecentActivityRepository = caseRecentActivityRepository;
            _internalAgentCommentRepository = internalAgentCommentRepository;
            _internalAgentPromiseRepository = internalAgentPromiseRepository;
            _caseRepository = caseRepository;
            _userRepository = userRepository;
            _agentRegionRepository = agentRegionRepository;
        }

        public List<ActivityCodeDto> GetActivityCodes()
        {
            var result = _activityCodeRepository.GetAll().Select(x => new ActivityCodeDto(x)).ToList();
            return result;
        }

        public List<Alert> GetAlerts()
        {
            var result = _alertRepository.GetAllList();
            return result;
        }

        public List<string> GetDelinquencyStages()
        {
            List<string> result = new List<string>();
            result.Add("P");
            result.Add("PP");
            result.Add("R");
            return result;
        }

        public List<string> GetEstados()
        {
            List<string> result = new List<string>();

            var macroEstates = _macroStateRepository.GetAllList();
            foreach (var m in macroEstates)
            {
                result.Add(m.Description);
            }

            var subEstates = _subStateRepository.GetAllList();
            foreach (var s in subEstates)
            {
                result.Add(s.Description);
            }

            return result;
        }

        public List<AgentDto> GetExternalAgents(long? internalAgent)
        {
            var result = new List<Agent>();

            if (internalAgent.HasValue)
            {
                result = _agentRepository.GetAll()
                        .Include(x => x.User)
                        .ThenInclude(u => u.ExternalAgentInternalAgents)
                        .Where(x => x.Type == Enumerations.Enumerations.AgentType.External && x.User.ExternalAgentInternalAgents.Any(i => i.InternalAgentId == internalAgent))
                        .ToList();
            } 
            else
            {
                result = _agentRepository.GetAllIncluding(x => x.User).Where(x => x.Type == Enumerations.Enumerations.AgentType.External).ToList();
            }

            return result.Select(x => new AgentDto(x)).ToList();
        }

        public List<RegionDto> GetRegions(long? internalAgent)
        {
            var result = new List<Region>();

            if (internalAgent.HasValue)
            {
                var externalAgents = _agentRepository.GetAll()
                        .Include(x => x.User)
                        .ThenInclude(u => u.ExternalAgentInternalAgents)
                        .Where(x => x.Type == Enumerations.Enumerations.AgentType.External && x.User.ExternalAgentInternalAgents.Any(i => i.InternalAgentId == internalAgent))
                        .ToList();

                foreach (var externalAgent in externalAgents)
                {
                    var externalAgentRecord = _agentRepository.GetAll().Where(a => a.UserId == externalAgent.UserId).FirstOrDefault();
                    var agentRegions = _agentRegionRepository.GetAll().Where(r => r.AgentId == externalAgentRecord.Id).ToList();

                    foreach (var agentRegion in agentRegions)
                    {
                        var region = _regionRepository.GetAll().Where(r => r.Id == agentRegion.RegionId).FirstOrDefault();

                        if (region != null)
                        {
                            if (!result.Contains(region))
                            {
                                result.Add(region);
                            }
                        }
                    }

                }
            }
            else
            {
                result = _regionRepository.GetAll().ToList();
            }

            return result.Select(x => new RegionDto(x)).ToList();
        }

        public List<RouteDto> GetRoutes(long? internalAgent)
        {
            var result = new List<Route>();

            if (internalAgent.HasValue)
            {
                var externalAgents = _agentRepository.GetAll()
                        .Include(x => x.User)
                        .ThenInclude(u => u.ExternalAgentInternalAgents)
                        .Where(x => x.Type == Enumerations.Enumerations.AgentType.External && x.User.ExternalAgentInternalAgents.Any(i => i.InternalAgentId == internalAgent))
                        .ToList();

                foreach (var externalAgent in externalAgents)
                {
                    var externalAgentRecord = _agentRepository.GetAll().Where(a => a.UserId == externalAgent.UserId).FirstOrDefault();
                    var agentRegions = _agentRegionRepository.GetAll().Where(r => r.AgentId == externalAgentRecord.Id).ToList();

                    foreach (var agentRegion in agentRegions)
                    {
                        var region = _regionRepository.GetAll().Include(r => r.Routes).Where(r => r.Id == agentRegion.RegionId).FirstOrDefault();

                        foreach (var regionRoute in region.Routes)
                        {
                            var route = _routeRepository.GetAll().Where(r => r.Id == regionRoute.RouteId).FirstOrDefault();

                            if (route != null)
                            {
                                if (!result.Contains(route))
                                {
                                    result.Add(route);
                                }
                            }
                        }
                    }

                }
            }
            else
            {
                result = _routeRepository.GetAll().ToList();
            }

            return result.Select(x => new RouteDto(x)).ToList();
        }

        public List<PostalCodeDto> GetPostalCodes(long? internalAgent)
        {
            var result = new List<PostalCode>();

            if (internalAgent.HasValue)
            {
                var externalAgents = _agentRepository.GetAll()
                        .Include(x => x.User)
                        .ThenInclude(u => u.ExternalAgentInternalAgents)
                        .Where(x => x.Type == Enumerations.Enumerations.AgentType.External && x.User.ExternalAgentInternalAgents.Any(i => i.InternalAgentId == internalAgent))
                        .ToList();

                foreach (var externalAgent in externalAgents)
                {
                    var externalAgentRecord = _agentRepository.GetAll().Where(a => a.UserId == externalAgent.UserId).FirstOrDefault();
                    var agentRegions = _agentRegionRepository.GetAll().Where(r => r.AgentId == externalAgentRecord.Id).ToList();

                    foreach (var agentRegion in agentRegions)
                    {
                        var region = _regionRepository.GetAll().Include(r => r.Routes).Where(r => r.Id == agentRegion.RegionId).FirstOrDefault();

                        foreach (var regionRoute in region.Routes)
                        {
                            var route = _routeRepository.GetAll().Include(r => r.PostalCodes).Where(r => r.Id == regionRoute.RouteId).FirstOrDefault();

                            foreach (var routePostalCode in route.PostalCodes)
                            {
                                var postalCode = _postalCodeRepository.GetAll().Where(p => p.Id == routePostalCode.PostalCodeId).FirstOrDefault();

                                if (postalCode != null)
                                {
                                    if (!result.Contains(postalCode))
                                    {
                                        result.Add(postalCode);
                                    }
                                }
                            }
                        }
                    }

                }
            }
            else
            {
                result = _postalCodeRepository.GetAll().ToList();
            }

            return result.Select(x => new PostalCodeDto(x)).ToList();
        }

        //public List<PostalCodeDto> GetPostalCodes(Guid? routeId)
        //{
        //    var result = new List<PostalCode>();

        //    if (routeId.HasValue)
        //    {
        //        result = _postalCodeRepository.GetAll()
        //                .Include(x => x.Routes).ThenInclude(a => a.Route)
        //                .Where(x => x.Routes.Any(r => r.RouteId == routeId))
        //                .ToList();
        //    }
        //    else
        //    {
        //        result = _postalCodeRepository.GetAll().ToList();
        //    }

        //    return result.Select(x => new PostalCodeDto(x)).ToList();
        //}

        //public List<RegionDto> GetRegions(Guid? externalAgent)
        //{
        //    var result = new List<Region>();

        //    if (externalAgent.HasValue)
        //    {
        //        result = _regionRepository.GetAll()
        //                .Include(x => x.Agents).ThenInclude(a => a.Agent)
        //                .Where(x => x.Agents.Any(a => a.AgentId == externalAgent))
        //                .ToList();
        //    }
        //    else
        //    {
        //        result = _regionRepository.GetAll().ToList();
        //    }

        //    return result.Select(x => new RegionDto(x)).ToList();
        //}

        //public List<RouteDto> GetRoutes(Guid? regionId)
        //{
        //    var result = new List<Route>();

        //    if (regionId.HasValue)
        //    {
        //        result = _routeRepository.GetAll()
        //                .Include(x => x.Regions).ThenInclude(a => a.Region)
        //                .Where(x => x.Regions.Any(r => r.RegionId == regionId))
        //                .ToList();
        //    }
        //    else
        //    {
        //        result = _routeRepository.GetAll().ToList();
        //    }

        //    return result.Select(x => new RouteDto(x)).ToList();
        //}

        public List<string> GetStatus()
        {
            List<string> result = new List<string>();
            result.Add("ACTIVO");
            result.Add("INACTIVO");
            return result;
        }

        public Task<List<InternalAgentDashboardModel>> ReadInternalAgentCases(ReadInternalAgentCasesParametersModel model)
        {
            var results = _caseRecentActivityRepository.ReadInternalAgentCases(model);
            return results;
        }

        public async Task SaveInternalAgentComments(SaveInternalAgentRequest request)
        {
            var internalAgentRecord = await _userRepository.GetAll().Where(u => u.Id == request.InternalAgentId).FirstOrDefaultAsync();

            if (request.Comments != null && request.Comments.Count > 0)
            {
                foreach (var comment in request.Comments)
                {
                    var newComment = new InternalAgentComment();
                    var caseRecord = await _caseRepository.GetAll().Where(c => c.CaseID == comment.CaseID).FirstOrDefaultAsync();

                    newComment.Case = caseRecord;
                    newComment.CaseID = caseRecord.Id;
                    newComment.Date = DateTime.Now;
                    newComment.InternalAgent = internalAgentRecord;
                    newComment.InternalAgentId = internalAgentRecord.Id;
                    newComment.Comment = comment.Comment;

                    await _internalAgentCommentRepository.InsertAsync(newComment);
                }
            }

            if (request.Promises != null && request.Promises.Count > 0)
            {
                foreach (var promise in request.Promises)
                {
                    var caseRecord = await _caseRepository.GetAll().Where(c => c.CaseID == promise.CaseID).FirstOrDefaultAsync();
                    var editPromise = await _internalAgentPromiseRepository.GetAll().Include(p => p.InternalAgent).Include(p => p.Case).Where(p => p.InternalAgentId == request.InternalAgentId && p.CaseID == caseRecord.Id).FirstOrDefaultAsync();

                    if (editPromise != null)
                    {
                        editPromise.Promise = promise.Promise;
                        editPromise.Date = DateTime.Now;
                        await _internalAgentPromiseRepository.UpdateAsync(editPromise);
                    } 
                    else
                    {
                        var newPromise = new InternalAgentPromise();

                        newPromise.Case = caseRecord;
                        newPromise.CaseID = caseRecord.Id;
                        newPromise.Date = DateTime.Now;
                        newPromise.InternalAgent = internalAgentRecord;
                        newPromise.InternalAgentId = internalAgentRecord.Id;
                        newPromise.Promise = promise.Promise;

                        await _internalAgentPromiseRepository.InsertAsync(newPromise);
                    }
                }
            }
        }
    }
}