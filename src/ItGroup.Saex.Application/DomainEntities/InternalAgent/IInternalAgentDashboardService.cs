﻿using Abp.Application.Services;
using Abp.Authorization.Users;
using Abp.Runtime.Validation;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IInternalAgentDashboardService : IApplicationService
    {
        List<AgentDto> GetExternalAgents(long? internalAgent);
        List<RegionDto> GetRegions(long? internalAgent);
        List<RouteDto> GetRoutes(long? internalAgent);
        List<PostalCodeDto> GetPostalCodes(long? internalAgent);
        //List<RegionDto> GetRegions(Guid? externalAgent);
        //List<RouteDto> GetRoutes(Guid? regionId);
        //List<PostalCodeDto> GetPostalCodes(Guid? routeId);
        List<ActivityCodeDto> GetActivityCodes();
        List<string> GetDelinquencyStages();
        List<Alert> GetAlerts();
        List<string> GetStatus();
        List<string> GetEstados();
        Task<List<InternalAgentDashboardModel>> ReadInternalAgentCases(ReadInternalAgentCasesParametersModel model);
        Task SaveInternalAgentComments(SaveInternalAgentRequest request);
    }
}
