using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemSubStateAppService : AppServiceBase, ISystemSubStateAppService
    {
        protected new readonly SystemSubStateManager _manager;

        public SystemSubStateAppService(SystemSubStateManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_SystemSubStates_Modify)]
        public async Task<SystemSubStateDto> Create(SystemSubStateCreateDto input)
        {
            var model = ObjectMapper.Map<SystemSubState>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a System Sub State");

            return new SystemSubStateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_SystemSubStates_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("System SubState not found for the provided Id.");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_SystemSubStates_View)]
        public async Task<SystemSubStateDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("System SubState not found for the provided Id");

            return new SystemSubStateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_SystemSubStates_View)]
        public async Task<PagedResultDto<SystemSubStateDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<SystemSubStateDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<SystemSubState>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new SystemSubStateDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_SystemSubStates_View)]
        public async Task<List<SystemSubStateListDto>> GetAllForList()
        {
            return await _manager.GetAll().Select(x => new SystemSubStateListDto(x)).ToListAsync();
        }

        [AbpAuthorize(PermissionNames.Pages_SystemSubStates_View)]
        public async Task<PagedResultDto<SystemSubStateListDto>> Search(SystemSubStateSearchModel filters)
        {
            var res = new PagedResultDto<SystemSubStateListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new SystemSubStateListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_SystemSubStates_Modify)]
        public async Task<SystemSubStateDto> Update(SystemSubStateUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("SystemSubState id is null.");

            var res = await _manager.GetAsync((Guid)input.Id);
            if (res == null) throw new EntityNotFoundException("SystemSubState does not exist in database.");
            res.Code = input.Code;
            res.Description = input.Description;

            await _manager.UpdateAsync(res);

            return new SystemSubStateDto(res);
        }
    }
}
