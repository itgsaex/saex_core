using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISystemSubStateAppService : IApplicationService
    {
        Task<SystemSubStateDto> Create(SystemSubStateCreateDto input);

        Task Delete(Guid id);

        Task<SystemSubStateDto> Get(Guid id);

        Task<PagedResultDto<SystemSubStateDto>> GetAll(PagedResultRequestDto paging);

        Task<List<SystemSubStateListDto>> GetAllForList();

        Task<PagedResultDto<SystemSubStateListDto>> Search(SystemSubStateSearchModel filters);

        Task<SystemSubStateDto> Update(SystemSubStateUpdateDto input);
    }
}