﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseSystemSubStateDto : EntityDto<Guid>
    {
        public CaseSystemSubStateDto()
        {
        }

        public CaseSystemSubStateDto(CaseSystemSubState input)
        {
            Id = input.Id;
            Value = input.Value;

            if (input.SystemSubState != null)
                SystemSubState = new SystemSubStateDto(input.SystemSubState);
        }

        public SystemSubStateDto SystemSubState { get; set; }

        public string Value { get; set; }
    }
}
