﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(CaseEvent))]
    public class CaseEventCreateDto : EntityDto<Guid>
    {
        public CaseEventCreateDto()
        {
        }

        public Guid AgentId { get; set; }

        public Guid CaseId { get; set; }

        public string Description { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime StartDate { get; set; }

        public Guid Type { get; set; }
    }
}
