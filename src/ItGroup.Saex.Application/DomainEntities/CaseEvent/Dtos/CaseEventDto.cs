﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseEventDto : EntityDto<Guid>
    {
        public CaseEventDto()
        {
        }

        public CaseEventDto(CaseEvent input)
        {
            Id = input.Id;
            Type = input.Type;
            StartDate = input.Start;
            EndDate = input.End;
            Description = input.Description;
            Case = input.Case;
            Agent = input.Agent;
        }

        public virtual Agent Agent { get; set; }

        public virtual Case Case { get; set; }

        public string Description { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime StartDate { get; set; }

        public CaseEventType Type { get; set; }
    }
}