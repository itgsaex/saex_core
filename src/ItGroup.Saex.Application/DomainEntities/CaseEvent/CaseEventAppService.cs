using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseEventAppService : AppServiceBase, ICaseEventAppService
    {
        protected new readonly CaseEventManager _manager;

        public CaseEventAppService(CaseEventManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<CaseEventDto> Create(CaseEventCreateDto input)
        {
            var model = ObjectMapper.Map<CaseEvent>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Case Event record");

            return new CaseEventDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("CaseEvent not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Case Event record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<CaseEventDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("CaseEvent not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Case Event record");

            return new CaseEventDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CaseEventDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<CaseEventDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<CaseEvent>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new CaseEventDto(x)).ToList();

            return output;
        }

        public async Task InsertCaseEvent(CaseEvent caseEvent)
        {
            await _manager.CreateAsync(caseEvent);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CaseEventDto>> Search(CaseEventSearchModel filters)
        {
            var res = new PagedResultDto<CaseEventDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new CaseEventDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<CaseEventDto> Update(CaseEventUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("CaseEvent id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as CaseEvent;
            if (model == null) throw new EntityNotFoundException("CaseEvent does not exist in database.");

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Case Event record");

            return new CaseEventDto(model);
        }
    }
}
