using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseEventAppService : IApplicationService
    {
        Task<CaseEventDto> Create(CaseEventCreateDto input);

        Task Delete(Guid id);

        Task<CaseEventDto> Get(Guid id);

        Task<PagedResultDto<CaseEventDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<CaseEventDto>> Search(CaseEventSearchModel filters);

        Task<CaseEventDto> Update(CaseEventUpdateDto input);

        Task InsertCaseEvent(CaseEvent caseEvent);
    }
}