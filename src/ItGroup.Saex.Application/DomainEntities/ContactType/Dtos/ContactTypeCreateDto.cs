﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(ContactType))]
    public class ContactTypeCreateDto : EntityDto<Guid>
    {
        public ContactTypeCreateDto()
        {
        }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}
