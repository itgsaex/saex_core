using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ContactTypeAppService : AppServiceBase, IContactTypeAppService
    {
        protected new readonly ContactTypeManager _manager;

        public ContactTypeAppService(ContactTypeManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_ContactTypes_Modify)]
        public async Task<ContactTypeDto> Create(ContactTypeCreateDto input)
        {
            var model = ObjectMapper.Map<ContactType>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Contact Type record");

            return new ContactTypeDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ContactTypes_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Contact Type record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_ContactTypes_View)]
        public async Task<ContactTypeDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Contact Type record");

            return new ContactTypeDto(result);
        }

        public async Task<List<ContactType>> GetAllContactTypes()
        {
            var result = _manager.GetAllQuery();
            return result.ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_ContactTypes_View)]
        public async Task<PagedResultDto<ContactTypeDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ContactTypeDto>();
            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<ContactType>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ContactTypeDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_ContactTypes_View)]
        public async Task<PagedResultDto<ContactTypeDto>> Search(ContactTypeSearchModel filters)
        {
            var res = new PagedResultDto<ContactTypeDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ContactTypeDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_ContactTypes_Modify)]
        public async Task<ContactTypeDto> Update(ContactTypeUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as ContactType;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.Code = input.Code;
            model.Description = input.Description;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Contact Type record");

            return new ContactTypeDto(model);
        }
    }
}
