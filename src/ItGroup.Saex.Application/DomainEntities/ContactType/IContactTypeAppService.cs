using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IContactTypeAppService : IApplicationService
    {
        Task<ContactTypeDto> Create(ContactTypeCreateDto input);

        Task Delete(Guid id);

        Task<ContactTypeDto> Get(Guid id);

        Task<List<ContactType>> GetAllContactTypes();

        Task<PagedResultDto<ContactTypeDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<ContactTypeDto>> Search(ContactTypeSearchModel filters);

        Task<ContactTypeDto> Update(ContactTypeUpdateDto input);
    }
}