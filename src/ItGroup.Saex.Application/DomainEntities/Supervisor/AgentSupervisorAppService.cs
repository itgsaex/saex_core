using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Users.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public class AssignInternalAgentsAppService : AppServiceBase, IAssignInternalAgentsAppService
    {
        private readonly AssignInternalAgentsManager _assignInternalAgentsManager;

        public AssignInternalAgentsAppService (AssignInternalAgentsManager assignInternalAgentsManager) : base()
        {
            _assignInternalAgentsManager = assignInternalAgentsManager;
        }

        public async Task AssignExternalAgents(long internalAgentId, List<UserDto> externalAgents)
        {
            List<User> externalAgentsUserRecord = new List<User>();

            foreach (var externalAgent in externalAgents)
            {
                var userRecord = ObjectMapper.Map<User>(externalAgent);
                externalAgentsUserRecord.Add(userRecord);
            }

            await _assignInternalAgentsManager.AssignExternalAgents(internalAgentId, externalAgentsUserRecord);
        }

        public async Task AssignInternalAgents(long supervisorId, List<UserDto> internalAgents)
        {
            List<User> internalAgentsUserRecord = new List<User>();

            foreach (var internalAgent in internalAgents)
            {
                var userRecord = ObjectMapper.Map<User>(internalAgent);
                internalAgentsUserRecord.Add(userRecord);
            }

            await _assignInternalAgentsManager.AssignInternalAgents(supervisorId, internalAgentsUserRecord);
        }

        public async Task DeleteInternalAgentExternalAgent(long internalAgentId, long externalAgentId)
        {
            await _assignInternalAgentsManager.DeleteInternalAgentExternalAgent(internalAgentId, externalAgentId);
        }

        public async Task DeleteSupervisorInternalAgent(long supervisorId, long internalAgentId)
        {
            await _assignInternalAgentsManager.DeleteSupervisorInternalAgent(supervisorId, internalAgentId);
        }

        public async Task<List<UserDto>> GetExternalAgents()
        {
            var results = (await _assignInternalAgentsManager.GetExternalAgents()).OrderBy(u => u.FullName).Select(x => new UserDto(x)).ToList();
            return results;
        }

        public async Task<List<UserDto>> GetInternalAgentExternalAgents(long internalAgentId)
        {
            var results = (await _assignInternalAgentsManager.GetInternalAgentExternalAgents(internalAgentId)).OrderBy(u => u.FullName).Select(x => new UserDto(x)).ToList();
            return results;
        }

        public async Task<List<UserDto>> GetInternalAgents()
        {
            var results = (await _assignInternalAgentsManager.GetInternalAgents()).OrderBy(x => x.FullName).Select(x => new UserDto(x)).ToList();
            return results;
        }

        public async Task<UserDto> GetUserRecord(long userId)
        {
            var supervisor = await _assignInternalAgentsManager.GetUserRecord(userId);
            var result = new UserDto(supervisor);
            return result;
        }

        public async Task<List<UserDto>> GetSupervisorInternalAgents(long supervisorId)
        {
            var results = (await _assignInternalAgentsManager.GetSupervisorInternalAgents(supervisorId)).OrderBy(u => u.FullName).Select(x => new UserDto(x)).ToList();
            return results;
        }

        public async Task<List<UserDto>> GetSupervisors()
        {
            var results = (await _assignInternalAgentsManager.GetSupervisors()).OrderBy(u => u.FullName).Select(x => new UserDto(x)).ToList();
            return results;
        }
    }
}