﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AssignAgentDto //: EntityDto<Guid>
    {
        public AssignAgentDto()
        {
        }

        public AssignAgentDto(Agent input, bool isSelected)
        {
            // Id = input.Id;
            //Type = input.Type;
            IsSelected = isSelected;

            if (input != null)
                Agent = new AgentDto(input);
        }

        public AgentDto Agent { get; set; }

        public bool IsSelected { get; set; }

        public SupervisorType Type { get; set; }
    }
}
