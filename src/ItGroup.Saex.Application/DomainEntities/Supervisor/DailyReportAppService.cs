﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Models;
using ItGroup.Saex.Users.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public class DailyReportAppService : AppServiceBase, IDailyReportAppService
    {
        private readonly ICaseRecentActivityRepository _caseRecentActivityRepository;
        private readonly IRepository<Agent, Guid> _agentRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Region, Guid> _regionRepository;
        private readonly IRepository<AgentRegion, Guid> _agentRegionRepository;
        private readonly IRepository<Route, Guid> _routeRepository;
        private readonly IRepository<PostalCode, Guid> _postalCodeRepository;
        private readonly IRepository<AgentTransfer, Guid> _agentTransferRepository;
        private readonly IRepository<Case, Guid> _caseRepository;

        public DailyReportAppService(ICaseRecentActivityRepository caseRecentActivityRepository,
                                     IRepository<Agent, Guid> agentRepository,
                                     IRepository<User, long> userRepository,
                                     IRepository<Region, Guid> regionRepository,
                                     IRepository<AgentRegion, Guid> agentRegionRepository,
                                     IRepository<Route, Guid> routeRepository,
                                     IRepository<PostalCode, Guid> postalCodeRepository,
                                     IRepository<AgentTransfer, Guid> agentTransferRepository,
                                     IRepository<Case, Guid> caseRepository) : base()
        {
            _caseRecentActivityRepository = caseRecentActivityRepository;
            _agentRepository = agentRepository;
            _userRepository = userRepository;
            _regionRepository = regionRepository;
            _agentRegionRepository = agentRegionRepository;
            _routeRepository = routeRepository;
            _postalCodeRepository = postalCodeRepository;
            _agentTransferRepository = agentTransferRepository;
            _caseRepository = caseRepository;
        }

        public Task<List<DailyReportModel>> DailyReport(DailyReportParameterModel model)
        {
            var result = _caseRecentActivityRepository.DailyReport(model);
            return result;
        }

        public List<AgentDto> GetExternalAgents(long? supervisor)
        {
            var result = new List<Agent>();

            if (supervisor.HasValue)
            {
                var internalAgents = _agentRepository.GetAll()
                                     .Include(x => x.User)
                                     .ThenInclude(u => u.InternalAgentSupervisors)
                                     .Include(x => x.User)
                                     .ThenInclude(u => u.InternalAgentExternalAgents)
                                     .Where(x => x.Type == Enumerations.Enumerations.AgentType.Internal && x.User.InternalAgentSupervisors.Any(i => i.SupervisorId == supervisor) && !x.User.IsDeleted && x.User.IsActive)
                                     .ToList();

                foreach (var internalAgent in internalAgents)
                {
                    if (internalAgent.User.InternalAgentExternalAgents != null)
                    {
                        foreach (var externalAgent in internalAgent.User.InternalAgentExternalAgents)
                        {
                            var externalAgentRecord = _userRepository.GetAllIncluding(u => u.Agent).Where(u => u.Id == externalAgent.ExternalAgentId && !u.IsDeleted && u.IsActive).FirstOrDefault();
                            if (externalAgentRecord != null && !result.Contains(externalAgentRecord.Agent))
                            {
                                result.Add(externalAgentRecord.Agent);
                            }
                        }
                    }
                }
            }
            else
            {
                result = _agentRepository.GetAllIncluding(x => x.User).Where(x => x.Type == Enumerations.Enumerations.AgentType.External && !x.User.IsDeleted && x.User.IsActive).ToList();
            }

            return result.OrderBy(a => a.User.FullName).Select(x => new AgentDto(x)).ToList();
        }

        public List<AgentDto> GetInternalAgents(long? supervisor)
        {
            var result = new List<Agent>();

            if (supervisor.HasValue)
            {
                result = _agentRepository.GetAll()
                                     .Include(x => x.User)
                                     .ThenInclude(u => u.InternalAgentSupervisors)
                                     .Include(x => x.User)
                                     .ThenInclude(u => u.InternalAgentExternalAgents)
                                     .Where(x => x.Type == Enumerations.Enumerations.AgentType.Internal && x.User.InternalAgentSupervisors.Any(i => i.SupervisorId == supervisor) && !x.User.IsDeleted && x.User.IsActive)
                                     .ToList();
            }
            else
            {
                result = _agentRepository.GetAllIncluding(x => x.User).Where(x => x.Type == Enumerations.Enumerations.AgentType.Internal && !x.User.IsDeleted && x.User.IsActive).ToList();
            }

            return result.OrderBy(x => x.User.FullName).Select(x => new AgentDto(x)).ToList();
        }

        public List<RegionDto> GetRegions(long? supervisor)
        {
            var result = new List<Region>();

            if (supervisor.HasValue)
            {
                var internalAgents = _agentRepository.GetAll()
                                      .Include(x => x.User)
                                      .ThenInclude(u => u.InternalAgentSupervisors)
                                      .Include(x => x.User)
                                      .ThenInclude(u => u.InternalAgentExternalAgents)
                                      .Where(x => x.Type == Enumerations.Enumerations.AgentType.Internal && x.User.InternalAgentSupervisors.Any(i => i.SupervisorId == supervisor) && !x.User.IsDeleted && x.User.IsActive)
                                      .ToList();

                foreach (var internalAgent in internalAgents)
                {
                    if (internalAgent.User.InternalAgentExternalAgents != null)
                    {
                        foreach (var externalAgent in internalAgent.User.InternalAgentExternalAgents)
                        {
                            var externalAgentRecord = _agentRepository.GetAll().Where(a => a.UserId == externalAgent.ExternalAgentId).FirstOrDefault();
                            var agentRegions = _agentRegionRepository.GetAll().Where(r => r.AgentId == externalAgentRecord.Id).ToList();

                            foreach (var agentRegion in agentRegions)
                            {
                                var region = _regionRepository.GetAll().Where(r => r.Id == agentRegion.RegionId && !r.IsDeleted).FirstOrDefault();

                                if (region != null)
                                {
                                    if (!result.Contains(region))
                                    {
                                        result.Add(region);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                result = _regionRepository.GetAll().Where(r => !r.IsDeleted).ToList();
            }

            return result.OrderBy(x => x.Name).Select(x => new RegionDto(x)).ToList();
        }

        public List<RouteDto> GetRoutes(long? supervisor)
        {
            var result = new List<Route>();

            if (supervisor.HasValue)
            {
                var internalAgents = _agentRepository.GetAll()
                                       .Include(x => x.User)
                                       .ThenInclude(u => u.InternalAgentSupervisors)
                                       .Include(x => x.User)
                                       .ThenInclude(u => u.InternalAgentExternalAgents)
                                       .Where(x => x.Type == Enumerations.Enumerations.AgentType.Internal && x.User.InternalAgentSupervisors.Any(i => i.SupervisorId == supervisor) && !x.User.IsDeleted && x.User.IsActive)
                                       .ToList();

                foreach (var internalAgent in internalAgents)
                {
                    if (internalAgent.User.InternalAgentExternalAgents != null)
                    {
                        foreach (var externalAgent in internalAgent.User.InternalAgentExternalAgents)
                        {
                            var externalAgentRecord = _agentRepository.GetAll().Where(a => a.UserId == externalAgent.ExternalAgentId).FirstOrDefault();
                            var agentRegions = _agentRegionRepository.GetAll().Where(r => r.AgentId == externalAgentRecord.Id).ToList();

                            foreach (var agentRegion in agentRegions)
                            {
                                var region = _regionRepository.GetAll().Include(r => r.Routes).Where(r => r.Id == agentRegion.RegionId && !r.IsDeleted).FirstOrDefault();

                                foreach (var regionRoute in region.Routes)
                                {
                                    var route = _routeRepository.GetAll().Where(r => r.Id == regionRoute.RouteId && !r.IsDeleted).FirstOrDefault();

                                    if (route != null)
                                    {
                                        if (!result.Contains(route))
                                        {
                                            result.Add(route);
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                result = _routeRepository.GetAll().Where(r => !r.IsDeleted).ToList();
            }

            return result.OrderBy(r => r.Name).Select(x => new RouteDto(x)).ToList();
        }

        public List<PostalCodeDto> GetPostalCodes(long? supervisor)
        {
            var result = new List<PostalCode>();

            if (supervisor.HasValue)
            {
                var internalAgents = _agentRepository.GetAll()
                                        .Include(x => x.User)
                                        .ThenInclude(u => u.InternalAgentSupervisors)
                                        .Include(x => x.User)
                                        .ThenInclude(u => u.InternalAgentExternalAgents)
                                        .Where(x => x.Type == Enumerations.Enumerations.AgentType.Internal && x.User.InternalAgentSupervisors.Any(i => i.SupervisorId == supervisor) && !x.User.IsDeleted && x.User.IsActive)
                                        .ToList();

                foreach (var internalAgent in internalAgents)
                {
                    if (internalAgent.User.InternalAgentExternalAgents != null)
                    {
                        foreach (var externalAgent in internalAgent.User.InternalAgentExternalAgents)
                        {
                            var externalAgentRecord = _agentRepository.GetAll().Where(a => a.UserId == externalAgent.ExternalAgentId).FirstOrDefault();
                            var agentRegions = _agentRegionRepository.GetAll().Where(r => r.AgentId == externalAgentRecord.Id).ToList();

                            foreach (var agentRegion in agentRegions)
                            {
                                var region = _regionRepository.GetAll().Include(r => r.Routes).Where(r => r.Id == agentRegion.RegionId && !r.IsDeleted).FirstOrDefault();

                                foreach (var regionRoute in region.Routes)
                                {
                                    var route = _routeRepository.GetAll().Include(r => r.PostalCodes).Where(r => r.Id == regionRoute.RouteId).FirstOrDefault();

                                    foreach (var routePostalCode in route.PostalCodes)
                                    {
                                        var postalCode = _postalCodeRepository.GetAll().Where(p => p.Id == routePostalCode.PostalCodeId && !p.IsDeleted).FirstOrDefault();

                                        if (postalCode != null)
                                        {
                                            if (!result.Contains(postalCode))
                                            {
                                                result.Add(postalCode);
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                result = _postalCodeRepository.GetAll().Where(p => !p.IsDeleted).ToList();
            }

            return result.OrderBy(p => p.Code).Select(x => new PostalCodeDto(x)).ToList();
        }

        public Task<List<PlannedCaseWitoutVisitsModel>> PlannedCaseWitoutVisits(DailyReportParameterModel model)
        {
            var result = _caseRecentActivityRepository.PlannedCaseWitoutVisits(model);
            return result;
        }

        public Task<List<ProyeccionPerdidaModel>> ProyeccionPerdida(ProyeccionPerdidaParameterModel model)
        {
            var result = _caseRecentActivityRepository.ProyeccionPerdidas(model);
            return result;
        }

        public async Task SaveTransfer(TansferModel model)
        {
            var fromUserRecord = await _userRepository.GetAll().Include(u => u.Agent).Where(u => u.Id == model.SelectedInternalAgent).FirstOrDefaultAsync();
            var toUserRecord = await _userRepository.GetAll().Include(u => u.Agent).Where(u => u.Id == model.SelectedInternalToAgent).FirstOrDefaultAsync();

            foreach (var caseId in model.SelectedCases)
            {
                var caseRecord = await _caseRepository.GetAll().Where(c => c.CaseID == caseId).FirstOrDefaultAsync();
                var caseALreadyTransfer = _agentTransferRepository.GetAll().Where(t => t.CaseId == caseRecord.Id && t.FromAgentId == fromUserRecord.Agent.Id && t.ToAgentId == toUserRecord.Agent.Id).FirstOrDefault();
                
                if (caseALreadyTransfer != null)
                {
                    continue;
                }

                var agentTransfer = new AgentTransfer();
                agentTransfer.FromAgentId = fromUserRecord.Agent.Id;
                agentTransfer.FromAgent = fromUserRecord.Agent;
                agentTransfer.ToAgentId = toUserRecord.Agent.Id;
                agentTransfer.ToAgent = toUserRecord.Agent;
                agentTransfer.CaseId = caseRecord.Id;
                agentTransfer.Case = caseRecord;

                if (model.AsignationType == "Transfer")
                    agentTransfer.TransferType = Enumerations.Enumerations.TransferType.Transfer;
                else
                    agentTransfer.TransferType = Enumerations.Enumerations.TransferType.Shared;
                
                await _agentTransferRepository.InsertAsync(agentTransfer);
            }   
        }

        public async Task<List<CaseDto>> GetCopyOrTransferCases(long internalAgent)
        {
            List<Case> cases = new List<Case>();
            var internalAgenteUserRecord = await _userRepository.GetAll().Include(u => u.Agent).Where(u => u.Id == internalAgent).FirstOrDefaultAsync();
            var transfers = await _agentTransferRepository.GetAll().Include(t => t.Case).ThenInclude(c => c.CaseType).Where(t => t.ToAgentId == internalAgenteUserRecord.Agent.Id).ToListAsync();

            foreach (var transfer in transfers)
            {
                if (transfer.Case != null)
                {
                    cases.Add(transfer.Case);
                }
            }

            return cases.Select(c => new CaseDto(c)).ToList();
        }

        public async Task DeleteTransfer(TansferModel model)
        {
            var toUserRecord = await _userRepository.GetAll().Include(u => u.Agent).Where(u => u.Id == model.SelectedInternalToAgent).FirstOrDefaultAsync();

            foreach (var caseId in model.SelectedCases)
            {
                var agentTransfer = await _agentTransferRepository.GetAll().Where(t => t.ToAgentId == toUserRecord.Agent.Id && t.CaseId == Guid.Parse(caseId)).FirstOrDefaultAsync();

                if (agentTransfer != null)
                    await _agentTransferRepository.DeleteAsync(agentTransfer);
            }
        }
    }
}