﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.Models;
using ItGroup.Saex.Users.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace ItGroup.Saex.DomainEntities
{
    public interface IDailyReportAppService : IApplicationService
    {
        Task<List<DailyReportModel>> DailyReport(DailyReportParameterModel model);
        Task<List<PlannedCaseWitoutVisitsModel>> PlannedCaseWitoutVisits(DailyReportParameterModel model);
        List<AgentDto> GetExternalAgents(long? supervisor);
        Task<List<ProyeccionPerdidaModel>> ProyeccionPerdida(ProyeccionPerdidaParameterModel model);
        List<AgentDto> GetInternalAgents(long? supervisor);
        List<RegionDto> GetRegions(long? supervisor);
        List<RouteDto> GetRoutes(long? supervisor);
        List<PostalCodeDto> GetPostalCodes(long? supervisor);
        Task SaveTransfer(TansferModel model);
        Task<List<CaseDto>> GetCopyOrTransferCases(long internalAgent);

        Task DeleteTransfer(TansferModel model);
    }
}