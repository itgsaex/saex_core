using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.Users.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAssignInternalAgentsAppService : IApplicationService
    {
        Task<List<UserDto>> GetSupervisors();
        Task<UserDto> GetUserRecord(long userId);
        Task<List<UserDto>> GetInternalAgents();
        Task<List<UserDto>> GetExternalAgents();
        Task<List<UserDto>> GetSupervisorInternalAgents(long supervisorId);
        Task<List<UserDto>> GetInternalAgentExternalAgents(long internalAgentId);
        Task AssignInternalAgents(long supervisorId, List<UserDto> internalAgents);
        Task AssignExternalAgents(long internalAgentId, List<UserDto> externalAgents);
        Task DeleteSupervisorInternalAgent(long supervisorId, long internalAgentId);
        Task DeleteInternalAgentExternalAgent(long internalAgentId, long externalAgentId);
    }
}
