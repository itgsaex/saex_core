using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAddressAppService : IApplicationService
    {
        Task<AddressDto> Create(AddressCreateDto input);

        Task Delete(Guid id);

        Task<AddressDto> Get(Guid id);

        Task<PagedResultDto<AddressDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<AddressDto>> Search(AddressSearchModel filters);

        Task<AddressDto> Update(AddressUpdateDto input);
    }
}