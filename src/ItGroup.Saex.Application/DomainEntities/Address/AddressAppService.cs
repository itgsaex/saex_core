using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AddressAppService : AppServiceBase, IAddressAppService
    {
        protected new readonly AddressManager _manager;

        public AddressAppService(AddressManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<AddressDto> Create(AddressCreateDto input)
        {
            var address = ObjectMapper.Map<Address>(input);
            var result = await _manager.CreateAsync(address);

            await LogTransaction(result, input, AuditLogOperationType.Create, "Created a Address record");

            return new AddressDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task Delete(Guid id)
        {
            var address = await _manager.GetAsync(id);
            if (address == null) throw new UserFriendlyException("Address not found for the provided Id.");

            await _manager.DeleteAsync(address);
            await LogTransaction(address, null, AuditLogOperationType.Delete, "Deleted an Address record");
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<AddressDto> Get(Guid id)
        {
            var address = await _manager.GetAsync(id);
            if (address == null) throw new UserFriendlyException("Address not found for the provided Id");

            await LogTransaction(address, null, AuditLogOperationType.View, "Viewed a Address record");
            return new AddressDto(address);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<AddressDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<AddressDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<Address>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new AddressDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<AddressDto>> Search(AddressSearchModel filters)
        {
            var res = new PagedResultDto<AddressDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;

            res.Items = results.Items.Select(x => new AddressDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<AddressDto> Update(AddressUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("Address id is null.");

            var address = await _manager.GetAsync((Guid)input.Id);
            var oldAddress = address.Clone() as Address;
            if (address == null) throw new EntityNotFoundException("Address does not exist in database.");

            await _manager.UpdateAsync(address);

            await LogTransaction(oldAddress, input, AuditLogOperationType.Update, "Updated a Address record");

            return new AddressDto(address);
        }
    }
}
