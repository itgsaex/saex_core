﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Address))]
    public class AddressUpdateDto : EntityDto<Guid>
    {
        public AddressUpdateDto()
        {
        }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }

        public string Name { get; set; }

        public string State { get; set; }

        public AddressType Type { get; set; }
    }
}
