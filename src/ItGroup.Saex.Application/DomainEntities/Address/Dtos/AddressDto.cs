﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AddressDto : EntityDto<Guid>
    {
        public AddressDto()
        {
        }

        public AddressDto(Address input)
        {
            Id = input.Id;
            Name = input.Name;
            AddressLine1 = input.AddressLine1;
            AddressLine2 = input.AddressLine2;
            City = input.City;
            State = input.State;
            Country = input.Country;
            Latitude = input.Latitude;
            Longitude = input.Longitude;
            Type = input.Type;
        }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public float? Latitude { get; set; }

        public float? Longitude { get; set; }

        public string Name { get; set; }

        public string State { get; set; }

        public AddressType Type { get; set; }
    }
}
