﻿using Abp.Application.Services.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldTypeListDto : EntityDto<Guid>
    {
        public DemographicFieldTypeListDto() { }

        public DemographicFieldTypeListDto(DemographicFieldType input)
        {
            Id = input.Id;
            Name = input.Name;
            DefaultControl = input.DefaultControl;
            DataType = input.DataType;
            IsActive = input.IsActive;

            if (input.Properties != null)
                Properties = JsonConvert.DeserializeObject<JObject>(input.Properties);
        }

        public string Name { get; set; }

        public string DefaultControl { get; set; }

        public string DataType { get; set; }

        public JObject Properties { get; set; }

        public bool IsActive { get; set; }
    }
}
