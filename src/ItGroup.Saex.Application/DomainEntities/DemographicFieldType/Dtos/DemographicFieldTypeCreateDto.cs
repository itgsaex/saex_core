﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Newtonsoft.Json.Linq;
using System;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(DemographicFieldType))]
    public class DemographicFieldTypeCreateDto : EntityDto<Guid>
    {
        public string Name { get; set; }

        public string DefaultControl { get; set; }

        public string DataType { get; set; }

        public JObject Properties { get; set; }

        public bool IsActive { get; set; }
    }
}
