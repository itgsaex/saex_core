﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldTypeAppService : AppServiceBase, IDemographicFieldTypeAppService
    {
        protected readonly DemographicFieldTypeManager _manager;

        public DemographicFieldTypeAppService(DemographicFieldTypeManager manager, AuditLogManager auditManager)
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        public async Task<List<DemographicFieldType>> GetAllDemographicFieldTypes()
        {
            var result = _manager.GetAllQuery();
            return result.ToList();
        }
    }
}
