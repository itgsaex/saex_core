﻿using Abp.Application.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IDemographicFieldTypeAppService : IApplicationService
    {
        Task<List<DemographicFieldType>> GetAllDemographicFieldTypes();
    }
}
