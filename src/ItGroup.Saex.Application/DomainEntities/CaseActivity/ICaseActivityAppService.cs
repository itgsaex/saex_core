using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.Web.Host.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseActivityAppService : IApplicationService
    {
        Task<CaseActivityDto> Create(CaseActivityCreateDto input);

        Task<CaseActivityDto> Update(CaseActivityUpdateDto input);

        Task Delete(Guid id);

        Task<CaseActivityDto> Get(Guid id);

        Task<PagedResultDto<CaseActivityDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<CaseActivityDto>> Search(CaseActivitySearchModel filters);

        Task SyncActivityModel(ActivitySyncModel input);

        Task<List<CaseRecentActivity>> GetRecentActivities(string caseNumber);

        void SetCaseMandatory(string caseNumber, string newValue);

        void SaveCaseFields(List<CaseFieldHelper> data);
    }
}

