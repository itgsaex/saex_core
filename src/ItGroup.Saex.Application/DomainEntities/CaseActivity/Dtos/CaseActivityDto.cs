﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseActivityDto : EntityDto<Guid>
    {
        public CaseActivityDto()
        {
        }

        public CaseActivityDto(CaseActivity input)
        {
            Id = input.Id;
            Comments = input.Comments;
            CreationTime = input.CreationTime;

            if (input.Agent != null)
                Agent = new AgentDto(input.Agent);

            if (input.ContactType != null)
                ContactType = new ContactTypeDto(input.ContactType);

            if (input.VisitPlace != null)
                VisitPlace = new VisitPlaceDto(input.VisitPlace);

            if (input.PaymentDelayReason != null)
                PaymentDelayReason = new PaymentDelayReasonDto(input.PaymentDelayReason);
        }

        public AgentDto Agent { get; set; }

        public string Comments { get; set; }

        public ContactTypeDto ContactType { get; set; }

        public DateTime CreationTime { get; set; }

        public PaymentDelayReasonDto PaymentDelayReason { get; set; }

        public VisitPlaceDto VisitPlace { get; set; }
    }
}
