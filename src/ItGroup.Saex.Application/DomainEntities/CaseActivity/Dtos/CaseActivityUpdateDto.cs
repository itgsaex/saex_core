﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(CaseActivity))]
    public class CaseActivityUpdateDto : EntityDto<Guid>
    {
        public CaseActivityUpdateDto() { }

        public string Comments { get; set; }

        public Guid AgentId { get; set; }

        public Guid ContactTypeId { get; set; }

        public Guid VisitPlaceId { get; set; }
    }
}