﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    class CaseRecentActivityDto : EntityDto<Guid>
    {
        public CaseRecentActivityDto()
        {
        }

        public CaseRecentActivityDto(CaseRecentActivity input)
        {
            Id = input.Id;
            CreationTime = input.CreationTime;
            ActionCode = input.ActionCode;
            ActionDescription = input.ActionDescription;
            ReactionCode = input.ReactionCode;
            ReactionDescription = input.ReactionDescription;
            ReasonCode = input.ReasonCode;
            ReasonDescription = input.ReasonDescription;
            Comment = input.Comment;
        }

        public DateTime CreationTime { get; set; }

        public string ActionCode { get; set; }

        public string ActionDescription { get; set; }

        public string ReactionCode { get; set; }

        public string ReactionDescription { get; set; }

        public string ReasonCode { get; set; }

        public string ReasonDescription { get; set; }

        public string Comment { get; set; }

    }
}
