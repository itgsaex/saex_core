﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseActivityListDto : EntityDto<Guid>
    {
        public CaseActivityListDto()
        {
        }

        public CaseActivityListDto(CaseActivity input)
        {
            Id = input.Id;
            Comments = input.Comments;

            if (input.Agent != null)
                Agent = new AgentDto(input.Agent);

            if (input.ContactType != null)
                ContactType = new ContactTypeDto(input.ContactType);

            if (input.VisitPlace != null)
                VisitPlace = new VisitPlaceDto(input.VisitPlace);
        }

        public AgentDto Agent { get; set; }

        public string Comments { get; set; }

        public ContactTypeDto ContactType { get; set; }

        public VisitPlaceDto VisitPlace { get; set; }
    }
}