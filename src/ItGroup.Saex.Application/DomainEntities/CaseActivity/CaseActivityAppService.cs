using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Web.Host.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseActivityAppService : AppServiceBase, ICaseActivityAppService
    {
        protected new readonly CaseActivityManager _manager;

        public CaseActivityAppService(CaseActivityManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<CaseActivityDto> Create(CaseActivityCreateDto input)
        {
            var model = ObjectMapper.Map<CaseActivity>(input);

            await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created an Activity record");

            return new CaseActivityDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Case Activity not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted an Activity record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<CaseActivityDto> Get(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("CaseActivity not found for the provided Id");

            await LogTransaction(model, null, AuditLogOperationType.View, "Viewed an Activity record");

            return new CaseActivityDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CaseActivityDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<CaseActivityDto>();

            var models = await _manager.GetAll().ToListAsync() as IReadOnlyList<CaseActivity>;

            output.TotalCount = models.Count();
            output.Items = models.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new CaseActivityDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CaseActivityDto>> Search(CaseActivitySearchModel filters)
        {
            var res = new PagedResultDto<CaseActivityDto>();

            var models = await _manager.GetFiltered(filters);

            res.TotalCount = models.TotalCount;
            res.Items = models.Items.Select(x => new CaseActivityDto(x)).ToList();

            return res;
        }

        public async Task SyncActivityModel(ActivitySyncModel input)
        {
            await _manager.FlushActivitySyncModel(input);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<CaseActivityDto> Update(CaseActivityUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("CaseActivity id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as CaseActivity;
            if (model == null) throw new EntityNotFoundException("CaseActivity does not exist in database.");

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated an activity record");

            return new CaseActivityDto(model);
        }

        public async Task<List<CaseRecentActivity>> GetRecentActivities(string caseNumber)
        {
            var res = await _manager.GetRecentActivity(caseNumber);
            return res;
        }

        public void SetCaseMandatory(string caseNumber, string newValue)
        {
            _manager.SetCaseMandatory(caseNumber, newValue);
        }

        public async Task<List<CaseFieldHelper>> ReadCaseFields(Guid? product)
        {
            return await _manager.ReadCaseFields(product);
        }

        public void SaveCaseFields(List<CaseFieldHelper> data)
        {
            _manager.SaveCaseFields(data);
        }
    }
}
