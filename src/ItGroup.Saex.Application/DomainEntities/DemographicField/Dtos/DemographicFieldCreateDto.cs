﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Newtonsoft.Json.Linq;
using System;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(DemographicField))]
    public class DemographicFieldCreateDto : EntityDto<Guid>
    {
        public string Name { get; set; }

        public string Label { get; set; }

        public bool IsRequired { get; set; }

        public int SortOrder { get; set; }

        public bool IsActive { get; set; }

        public Guid DemographicFieldTypeId { get; set; }

        public JObject Properties { get; set; }
    }
}
