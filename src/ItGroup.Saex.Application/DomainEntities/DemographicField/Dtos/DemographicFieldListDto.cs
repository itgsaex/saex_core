﻿using Abp.Application.Services.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldListDto : EntityDto<Guid>
    {
        public DemographicFieldListDto() { }

        public DemographicFieldListDto(DemographicField input)
        {
            Id = input.Id;
            Name = input.Name;
            Label = input.Label;
            IsRequired = input.IsRequired;
            SortOrder = input.SortOrder;
            IsActive = input.IsActive;
            DemographicFieldTypeId = input.DemographicFieldTypeId;

            if (input.Properties != null)
                Properties = JsonConvert.DeserializeObject<JObject>(input.Properties);
        }

        public string Name { get; set; }

        public string Label { get; set; }

        public bool IsRequired { get; set; }

        public JObject Properties { get; set; }

        public int SortOrder { get; set; }

        public bool IsActive { get; set; }

        public Guid DemographicFieldTypeId { get; set; }
    }
}
