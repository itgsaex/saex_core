﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IDemographicFieldAppService : IApplicationService
    {
        Task Create(DemographicFieldCreateDto input);

        Task Delete(Guid id);

        Task<DemographicFieldDto> Get(Guid id);

        Task<List<DemographicFieldListDto>> GetAllForList();

        Task<PagedResultDto<DemographicFieldListDto>> Search(DemographicFieldSearchModel filters);

        Task Update(DemographicFieldUpdateDto input);

        Task<List<DemographicField>> GetAllDemographicFields();
    }
}
