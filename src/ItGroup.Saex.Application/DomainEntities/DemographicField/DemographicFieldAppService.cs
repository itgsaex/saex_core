﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using ItGroup.Saex.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldAppService : AppServiceBase, IDemographicFieldAppService
    {
        protected readonly DemographicFieldManager _manager;

        public DemographicFieldAppService(DemographicFieldManager manager, AuditLogManager auditManager)
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_DemographicFields)]
        public async Task Create(DemographicFieldCreateDto input)
        {
            var demographicField = new DemographicField
            {
                Name = input.Name,
                Label = input.Label,
                IsRequired = input.IsRequired,
                SortOrder = input.SortOrder,
                IsActive = input.IsActive,
                DemographicFieldTypeId = input.DemographicFieldTypeId
            };

            if (input.Properties != null)
                demographicField.Properties = JsonConvert.SerializeObject(input.Properties);

            await _manager.CreateAsync(demographicField);

            await LogTransaction(demographicField, input, AuditLogOperationType.Create, "Created a Demographic Fields record");
        }

        [AbpAuthorize(PermissionNames.Pages_DemographicFields)]
        public async Task Delete(Guid id)
        {
            var res = await _manager.GetAsync(id);

            if (res is null)
                throw new UserFriendlyException(L("RecordNotFoundError"));

            await _manager.DeleteAsync(res);

            await LogTransaction(res, null, AuditLogOperationType.Delete, "Deleted a Demographic Fields record");
        }

        [AbpAuthorize(PermissionNames.Pages_DemographicFields)]
        public async Task<DemographicFieldDto> Get(Guid id)
        {
            var res = await _manager.GetAsync(id);

            if (res is null)
                throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(res, null, AuditLogOperationType.View, "Viewed a Demographic Fields record");

            return new DemographicFieldDto(res);
        }

        [AbpAuthorize]
        public async Task<List<DemographicFieldListDto>> GetAllForList()
        {
            var results = await _manager.GetAll().ToListAsync();

            return results.Select(x => new DemographicFieldListDto(x)).ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_DemographicFields)]
        public async Task<PagedResultDto<DemographicFieldListDto>> Search(DemographicFieldSearchModel filters)
        {
            var res = new PagedResultDto<DemographicFieldListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new DemographicFieldListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_DemographicFields)]
        public async Task Update(DemographicFieldUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _manager.GetAsync(input.Id);
            var oldModel = model.Clone() as DemographicField;
            if (model is null)
                throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.Name = input.Name;
            model.Label = input.Label;
            model.IsRequired = input.IsRequired;
            model.SortOrder = input.SortOrder;
            model.IsActive = input.IsActive;
            model.DemographicFieldTypeId = input.DemographicFieldTypeId;
            model.Properties = input.Properties is null ? null : JsonConvert.SerializeObject(input.Properties);

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Demographic Fields record");
        }

        public async Task<List<DemographicField>> GetAllDemographicFields()
        {
            var result = _manager.GetAllQuery();
            return result.ToList();
        }
    }
}
