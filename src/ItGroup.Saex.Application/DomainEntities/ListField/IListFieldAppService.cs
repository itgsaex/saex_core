using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IListFieldAppService : IApplicationService
    {
        Task<ListFieldDto> Create(ListFieldCreateDto input);

        Task Delete(Guid id);

        Task<ListFieldDto> Get(Guid id);

        Task<PagedResultDto<ListFieldDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<ListFieldDto>> Search(ListFieldSearchModel filters);

        Task<ListFieldDto> Update(ListFieldUpdateDto input);
    }
}