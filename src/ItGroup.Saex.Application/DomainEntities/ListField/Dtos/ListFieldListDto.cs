﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListFieldListDto : EntityDto<Guid>
    {
        public ListFieldListDto()
        {
        }

        public ListFieldListDto(ListField input)
        {
            Id = input.Id;
            ProductId = input.ProductId;
            ListId = input.ListId;
            FieldName = input.FieldName;
            FieldId = input.FieldId;
            Position = input.Position;

            if (input.ListFieldCategory != null)
                ListFieldCategory = new ListFieldCategoryDto(input.ListFieldCategory);
        }

        public string FieldId { get; set; }

        public string FieldName { get; set; }

        public ListFieldCategoryDto ListFieldCategory { get; set; }

        public Guid ListId { get; set; }

        public int Position { get; set; }

        public Guid ProductId { get; set; }
    }
}