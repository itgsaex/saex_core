﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(ListField))]
    public class ListFieldCreateDto : EntityDto<Guid>
    {
        public ListFieldCreateDto()
        {
        }

        public string FieldId { get; set; }

        public string FieldName { get; set; }

        public Guid ListFieldCategoryId { get; set; }

        public Guid ListId { get; set; }

        public int Position { get; set; }

        public Guid ProductId { get; set; }
    }
}
