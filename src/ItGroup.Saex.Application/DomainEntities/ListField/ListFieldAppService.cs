using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListFieldAppService : AppServiceBase, IListFieldAppService
    {
        protected new readonly ListFieldManager _manager;

        public ListFieldAppService(ListFieldManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_ListFields_Modify)]
        public async Task<ListFieldDto> Create(ListFieldCreateDto input)
        {
            var model = ObjectMapper.Map<ListField>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a List Field record");

            return new ListFieldDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ListFields_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("List Field not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a List Field record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_ListFields_View)]
        public async Task<ListFieldDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("List Field not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a List Field record");

            return new ListFieldDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ListFields_View)]
        public async Task<PagedResultDto<ListFieldDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ListFieldDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<ListField>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ListFieldDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_ListFields_View)]
        public async Task<PagedResultDto<ListFieldDto>> Search(ListFieldSearchModel filters)
        {
            var res = new PagedResultDto<ListFieldDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ListFieldDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_ListFields_Modify)]
        public async Task<ListFieldDto> Update(ListFieldUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("ListField id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as ListField;
            if (model == null) throw new EntityNotFoundException("ListField does not exist in database.");

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a List Field record");

            return new ListFieldDto(model);
        }
    }
}
