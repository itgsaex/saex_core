﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.Web.Host.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseFieldsImportAppService : IApplicationService
    {
        Task<CaseFieldsImportDto> Create(CaseFieldsImportDto input);

        Task<CaseFieldsImportDto> Update(CaseFieldsImportDto input);

        Task Delete(Guid id);

        Task<CaseFieldsImportDto> Get(Guid id);

        Task<PagedResultDto<CaseFieldsImportDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<CaseFieldsImportDto>> Search(CaseFieldImportSearchModel filters);

        //Task SyncActivityModel(ActivitySyncModel input);
    }
}

