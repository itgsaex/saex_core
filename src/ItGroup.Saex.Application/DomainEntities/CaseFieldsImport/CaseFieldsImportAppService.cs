﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Web.Host.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseFieldsImportAppService : AppServiceBase, ICaseFieldsImportAppService
    {
        protected readonly CaseFieldImportManager _manager;

        public CaseFieldsImportAppService(CaseFieldImportManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_ValidFieldsForLists_Modify)]
        public async Task<CaseFieldsImportDto> Create(CaseFieldsImportDto input)
        {
            var model = ObjectMapper.Map<CaseFieldImport>(input);

            await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Case Field record");

            return new CaseFieldsImportDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_ValidFieldsForLists_View)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Case Field not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted an Case Field record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_ValidFieldsForLists_View)]
        public async Task<CaseFieldsImportDto> Get(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Case Field not found for the provided Id");

            await LogTransaction(model, null, AuditLogOperationType.View, "Viewed an Case Field record");

            return new CaseFieldsImportDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_ValidFieldsForLists_View)]
        public async Task<PagedResultDto<CaseFieldsImportDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<CaseFieldsImportDto>();

            var models = await _manager.GetAll().ToListAsync() as IReadOnlyList<CaseFieldImport>;

            output.TotalCount = models.Count();
            output.Items = models.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new CaseFieldsImportDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_ValidFieldsForLists_View)]
        public async Task<PagedResultDto<CaseFieldsImportDto>> Search(CaseFieldImportSearchModel filters)
        {
            var res = new PagedResultDto<CaseFieldsImportDto>();

            var models = await _manager.GetFiltered(filters);

            res.TotalCount = models.TotalCount;
            res.Items = models.Items.Select(x => new CaseFieldsImportDto(x)).ToList();

            return res;
        }

        //public async Task SyncActivityModel(ActivitySyncModel input)
        //{
        //    await _manager.FlushActivitySyncModel(input);
        //}

        [AbpAuthorize(PermissionNames.Pages_ValidFieldsForLists_Modify)]
        public async Task<CaseFieldsImportDto> Update(CaseFieldsImportDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("Case Field id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as CaseFieldImport;
            if (model == null) throw new EntityNotFoundException("Case Field does not exist in database.");

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Case Field record");

            return new CaseFieldsImportDto(model);
        }
    }
}
