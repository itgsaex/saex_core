﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseFieldsImportDto : EntityDto<Guid>
    {
        public CaseFieldsImportDto()
        {
        }

        public CaseFieldsImportDto(CaseFieldImport input)
        {
            Id = input.Id;
            CaseTypeID = input.CaseTypeID;
            CategoriaID = input.CategoriaID;
            ColumnName = input.ColumnName;
            DownLoadOption = input.DownLoadOption;
            FieldId = input.FieldId;
            HumanName = input.HumanName;
            Sequence = input.Sequence;
            SourceTargetType = input.SourceTargetType;
            TargetColumn = input.TargetColumn;
        }

        public Guid? CaseTypeID { get; set; }

        public int CategoriaID { get; set; }

        public string ColumnName { get; set; }

        public bool DownLoadOption { get; set; }

        public int FieldId { get; set; }

        public string HumanName { get; set; }

        public int Sequence { get; set; }

        public string SourceTargetType { get; set; }

        public string TargetColumn { get; set; }
    }
}
