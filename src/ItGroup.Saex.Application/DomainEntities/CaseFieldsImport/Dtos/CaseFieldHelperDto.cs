﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseFieldHelperDto //: EntityDto<Guid>
    {
        public CaseFieldHelperDto()
        {
        }

        public CaseFieldHelperDto(CaseFieldHelper input)
        {
            //Id = input.Id;
            Selected = input.Selected;
            FieldID = input.FieldId;
            ColumnName = input.ColumnName;
            HumanName = input.HumanName;
            CaseTypeDescription = input.CaseTypeDescription;
            Sequence = input.Sequence;
            CategoriaID = input.CategoriaId;
            MasterList = input.MasterList;
            ChildList = input.ChildList;
            RiskList = input.RiskList;
        }

        public bool Selected { get; set; }

        public string ColumnName { get; set; }

        public string HumanName { get; set; }

        public string CaseTypeDescription { get; set; }

        public bool MasterList { get; set; }

        public bool ChildList { get; set; }

        public bool RiskList { get; set; }

        public int CategoriaID { get; set; }

        public int Sequence { get; set; }

        public int FieldID { get; set; }
    }
}
