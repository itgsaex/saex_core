using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseAppService : IApplicationService
    {
        Task<CaseDto> Get(Guid id);

        Task<PagedResultDto<CaseDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<CaseListDto>> Search(CaseSearchModel filters);

        Task<List<Case>> GetCases();

        Task<List<vwCases>> GetAgentCases(string agentID);

        Task<int> UpdateCases(List<CaseTransferDto> cases);
    }
}
