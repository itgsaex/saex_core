using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using CsvHelper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseAppService : AppServiceBase, ICaseAppService
    {
        protected readonly CaseTypeManager _caseTypeManager;

        protected readonly DelinquencyStateManager _delinquencyStateManager;

        protected readonly CaseManager _manager;

        protected readonly PostalCodeManager _postalCodemanager;

        protected readonly ProductManager _productManager;

        protected readonly SystemStateManager _systemStateManager;

        protected readonly SystemSubStateManager _systemSubStateManager;

        public CaseAppService(
            CaseManager manager,
            CaseTypeManager caseTypeManager,
            PostalCodeManager postalCodemanager,
            DelinquencyStateManager delinquencyStateManager,
            ProductManager productManager,
            SystemStateManager systemStateManager,
            SystemSubStateManager systemSubStateManager,
            AuditLogManager auditManager
            ) : base()
        {
            _manager = manager;
            _postalCodemanager = postalCodemanager;
            _delinquencyStateManager = delinquencyStateManager;
            _productManager = productManager;
            _systemStateManager = systemStateManager;
            _systemSubStateManager = systemSubStateManager;
            _caseTypeManager = caseTypeManager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<CaseDto> Get(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Case not found for the provided Id");

            await LogTransaction(model, null, AuditLogOperationType.View, "Viewed a Case record");

            return new CaseDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CaseDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<CaseDto>();

            var models = await _manager.GetAll().ToListAsync() as IReadOnlyList<Case>;

            output.TotalCount = models.Count();
            output.Items = models.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new CaseDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CaseListDto>> Search(CaseSearchModel filters)
        {
            var model = new PagedResultDto<CaseListDto>();

            var models = await _manager.GetFiltered(filters);

            model.TotalCount = models.TotalCount;
            model.Items = models.Items.Select(x => new CaseListDto(x)).ToList();

            return model;
        }

        public Task<List<Case>> GetCases()
        {
            var result = _manager.GetAll().ToListAsync();
            return result;
        }

        public Task<List<vwCases>> GetAgentCases(string agentID)
        {
            var result = _manager.GetCases(agentID);
            return result;
        }

        public async Task<int> UpdateCases(List<CaseTransferDto> cases)
        {
            try
            {
                foreach (var sendCase in cases)
                {
                    var dbCase = await _manager.GetByCaseIDAsync(sendCase.CaseID);

                    // Column changes
                    dbCase.Latitude = sendCase.Latitude;
                    dbCase.Longitude = sendCase.Longitude;
                    dbCase.LatitudeRoute = sendCase.LatitudeRoute;
                    dbCase.LongitudeRoute = sendCase.LongitudeRoute;
                    dbCase.IsMandatory = sendCase.IsMandatory;
                    dbCase.IsPlanned = sendCase.IsPlanned;
                    dbCase.Monday = sendCase.Monday;
                    dbCase.Tuesday = sendCase.Tuesday;
                    dbCase.Wednesday = sendCase.Wednesday;
                    dbCase.Thursday = sendCase.Thursday;
                    dbCase.Friday = sendCase.Friday;
                    dbCase.Saturday = sendCase.Saturday;
                    dbCase.VisitStartTime = sendCase.VisitStartTime;
                    dbCase.VisitEndTime = sendCase.VisitEndTime;
                    dbCase.VisitPlace = sendCase.VisitPlace;
                    dbCase.CollectionDay = sendCase.CollectionDay;
                    dbCase.CollectionStartTime = sendCase.CollectionStartTime;
                    dbCase.CollectionEndTime = sendCase.CollectionEndTime;
                    dbCase.CollectionPlace = sendCase.CollectionPlace;

                    await _manager.UpdateAsync(dbCase);
                }
                return 1;
            }
            catch (Exception e)
            {
                Logger.Error($"Error in UpdateCases {e.Message}");
                return 0;
            }
        }
    }
}
