﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CustomerCreateDto : EntityDto<Guid>
    {
        public CustomerCreateDto()
        {
        }

        public List<AddressCreateDto> Addresses { get; set; }

        public DateTime BirthDate { get; set; }

        public string CustomerId { get; set; }

        public string EmailAddress { get; set; }

        public string FullName { get; set; }

        public string IdentificationNumber { get; set; }

        public string PhoneNumber { get; set; }

        public string Ssn { get; set; }
    }
}
