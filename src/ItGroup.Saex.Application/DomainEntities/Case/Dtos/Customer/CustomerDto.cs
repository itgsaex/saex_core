﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CustomerDto : EntityDto<Guid>
    {
        public CustomerDto()
        {
        }

        public CustomerDto(Customer input)
        {
            BirthDate = input.BirthDate;
            CustomerId = input.CustomerId;
            EmailAddress = input.EmailAddress;
            FullName = input.FullName;
            IdentificationNumber = input.IdentificationNumber;
            PhoneNumber = input.PhoneNumber;
            Ssn = input.Ssn;

            if (input.Addresses != null)
                Addresses = input.Addresses.Select(x => new AddressDto(x)).ToList();
        }

        public List<AddressDto> Addresses { get; set; }

        public DateTime? BirthDate { get; set; }

        public string CustomerId { get; set; }

        public string EmailAddress { get; set; }

        public string FullName { get; set; }

        public string IdentificationNumber { get; set; }

        public string PhoneNumber { get; set; }

        public string Ssn { get; set; }
    }
}
