﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTransferDto
    {
        public string CaseID { get; set; }

        public string AccountNumber { get; set; }

        public string Fullname { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Fulladdress { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }

        public string Phone { get; set; }

        public string Phone2 { get; set; }

        public string Phone3 { get; set; }

        public string Status { get; set; }

        public string DelicacyStages { get; set; }

        public double BalanceAmount { get; set; }

        public string ExtraFields { get; set; }

        public string Type { get; set; }

        public string SocialSecurity { get; set; }

        public Boolean IsActive { get; set; }

        public string Priority { get; set; }

        public string Contact { get; set; }

        public string Email { get; set; }

        public string Condition { get; set; }

        public DateTime? LastPaymentDate { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double LatitudeRoute { get; set; }

        public double LongitudeRoute { get; set; }

        // Tactic - Begin
        public bool? IsPlanned { get; set; }

        public bool? Monday { get; set; }

        public bool? Tuesday { get; set; }

        public bool? Wednesday { get; set; }

        public bool? Thursday { get; set; }

        public bool? Friday { get; set; }

        public bool? Saturday { get; set; }

        public DateTime? VisitStartTime { get; set; }

        public DateTime? VisitEndTime { get; set; }

        public string VisitPlace { get; set; }

        public int? CollectionDay { get; set; }

        public DateTime? CollectionStartTime { get; set; }

        public DateTime? CollectionEndTime { get; set; }

        public string CollectionPlace { get; set; }
        // Tactic - End

        public bool IsMandatory { get; set; }
    }
}