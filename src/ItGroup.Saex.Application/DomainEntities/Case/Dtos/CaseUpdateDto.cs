﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Case))]
    public class CaseUpdateDto : EntityDto<Guid>
    {
        public CaseUpdateDto() { }

        public string FullName { get; set; }

        public string Type { get; set; }

        public string ClientId { get; set; }

        public string AccountNumber { get; set; }

        public string PhoneNumber { get; set; }

        public string EmailAddress { get; set; }

        public string Status { get; set; }

        public string SSN { get; set; }

        public string TermId { get; set; }

        public double OwedAmount { get; set; }

        public double OriginalAmount { get; set; }

        public double PaidAmount { get; set; }

        public double InterestRate { get; set; }

        public string BranchId { get; set; }

        public string CollectorId { get; set; }

        public string StateCode { get; set; }

        public DateTime ImportedDate { get; set; }

        public Guid ProductId { get; set; }

        public Guid SystemStatusId { get; set; }

        public Guid RiskStatus { get; set; }

        public Guid DelinquencyStatus { get; set; }

        public string ExtraFieldsJson { get; set; }
    }
}