﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseListDto : EntityDto<Guid>
    {
        private CaseBaseline @case;

        public CaseListDto()
        {
        }

        public CaseListDto(CaseReportModel input)
        {
            Id = Guid.NewGuid();
            Assignationtype = "Assignation Type goes here..."; //input.Assignationtype;
            Charges = 0;
            Dpd = ""; //input.Dpd;
            EnterDate = new DateTime(); //input.EnterDate;
            ExtraFieldsJson = "";
            LastPaymentDate = null;
            OriginalAmount = 0;
            OwedAmount = 0;
            PaidAmount = 0;
            StateCode = input.StateCode;
            Status = input.EstatusSistema;
            ImportedDate = DateTime.Now;
            ImportType = ImportType.Automatic;
            AccountNumber = input.NumeroCuenta;
            PrincipalCustomerName = input.Nombre;
            PostalCode = input.ZipCode;
            ProductName = input.ProductNumber;
            CaseID = input.CasoID;
        }
        public CaseListDto(Case input)
        {
            Id = input.Id;
            Assignationtype = "Assignation Type goes here..."; //input.Assignationtype;
            Charges = input.Charges;
            Dpd = "Dpd Name"; //input.Dpd;
            EnterDate = new DateTime(); //input.EnterDate;
            ExtraFieldsJson = input.ExtraFieldsJson;
            // IsRequired = input.IsRequired; //TODO: Validate this field
            LastPaymentDate = input.LastPaymentDate;
            OriginalAmount = input.OriginalAmount;
            OwedAmount = input.OwedAmount;
            PaidAmount = input.PaidAmount;
            StateCode = input.StateCode;
            Status = input.Status;
            ImportedDate = input.ImportedDate;
            ImportType = input.ImportType;
            AccountNumber = input.AccountNumber;
            PrincipalCustomerName = input.Fullname;
            PostalCode = input.ZipCode;
            ProductName = input.CaseType?.ProductNumber;
            CaseID = input.CaseID;

            if (input.DelinquencyState != null)
                DelinquencyState = new CaseDelinquencyStateDto(input.DelinquencyState);

            if (input.CaseActivities != null && input.CaseActivities.Any())
            {
                var caseActivity = input.CaseActivities.OrderByDescending(x => x.CreationTime).Select(y => y.ActivityCodes).FirstOrDefault();

                if (caseActivity != null)
                {
                    var activityCode = caseActivity.Select(x => x.ActivityCode).FirstOrDefault();

                    ActivityCodeName = activityCode.Code;
                }
            }

            //if (input.Customers != null && input.Customers.Any())
            //{
            //    var principalCustomer = input.Customers.Where(x => x.IsPrincipal).FirstOrDefault();
            //    var principalAddress = principalCustomer.Addresses.FirstOrDefault();

            //    PrincipalCustomerName = principalCustomer.FullName;
            //    PostalCode = principalAddress.PostalCode.Code;
            //}

            if (input.Comments != null && input.Comments.Any())
            {
                LastestComment = input.Comments.OrderByDescending(x => x.CreationTime).Select(y => new CaseCommentDto(y)).FirstOrDefault();
            }

            //if (input.Route != null)
            //{
            //    this.RouteName = input.Route.Name;
            //}

            //if (input.Product != null && input.Product.Product != null)
            //{
            //    this.ProductName = input.Product.Product.Code;
            //}
        }

        public CaseListDto(CaseBaseline @case)
        {
            this.@case = @case;
        }

        public string AccountNumber { get; set; }

        public string ActivityCodeName { get; set; }

        public string Assignationtype { get; set; }

        public string BranchId { get; set; }

        public double Charges { get; set; }

        public CaseDelinquencyStateDto DelinquencyState { get; set; }

        public string Dpd { get; set; }

        public DateTime EnterDate { get; set; }

        public string ExtraFieldsJson { get; set; }

        public DateTime? ImportedDate { get; set; }

        public ImportType ImportType { get; set; }

        public bool IsRequired { get; set; }

        public CaseCommentDto LastestComment { get; set; }

        //TODO: validate if this is necessary
        //public string LastestExternalAgentComment { get; set; }
        //public string LastestInternalAgentComment { get; set; }

        public DateTime? LastPaymentDate { get; set; }

        public double OriginalAmount { get; set; }

        public double OwedAmount { get; set; }

        public double PaidAmount { get; set; }

        public string PostalCode { get; set; }

        public string PrincipalCustomerName { get; set; }

        public string ProductName { get; set; }

        public string RouteName { get; set; }

        public string StateCode { get; set; }

        public string Status { get; set; }

        public string CaseID { get; set; }
    }
}
