﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseDto : EntityDto<Guid>
    {
        public CaseDto()
        {
        }

        public CaseDto(Case input)
        {
            Id = input.Id;
            AccountType = input.AccountType;
            BranchId = input.BranchId;
            CaseNumber = input.CaseNumber;
            Charges = input.Charges;
            CollectorId = input.CollectorId;
            Condition = input.Condition;
            ContactPerson = input.ContactPerson;
            DueDate = input.DueDate;
            ExtraFieldsJson = input.ExtraFieldsJson;
            ImportedDate = input.ImportedDate;
            InterestRate = input.InterestRate;
            LastPaymentDate = input.LastPaymentDate;
            MaturityDate = input.MaturityDate;
            OriginalAmount = input.OriginalAmount;
            OriginDate = input.OriginDate;
            OwedAmount = input.OwedAmount;
            PaidAmount = input.PaidAmount;
            StateCode = input.StateCode;
            Status = input.Status;
            Terms = input.Terms;
            City = input.City;
            Zipcode = input.ZipCode;
            Name = input.Fullname;

            AccountNumber = input.AccountNumber;

            if (input.Agents != null)
                Agents = input.Agents.Select(x => new AgentDto(x.Agent)).ToList();

            if (input.CaseActivities != null)
                CaseActivities = input.CaseActivities.Select(x => new CaseActivityDto(x)).ToList();

            if (input.CaseEvents != null)
                CaseEvents = input.CaseEvents.Select(x => new CaseEventDto(x)).ToList();

            if (input.CaseType != null)
            {
                CaseType = new CaseTypeDto(input.CaseType);
                ProductNumber = input.CaseType.ProductNumber;
            }

            if (input.Customers != null)
                Customers = input.Customers.Select(x => new CustomerDto(x)).ToList();

            if (input.Payments != null)
                Payments = input.Payments.Select(x => new CasePaymentDto(x)).ToList();

            if (input.Tactic != null)
                Tactic = new CaseTacticDto(input.Tactic);

            if (input.Product != null)
                Product = new CaseProductDto(input.Product);

            if (input.SystemState != null)
                SystemState = new CaseSystemStateDto(input.SystemState);

            if (input.DelinquencyState != null)
                DelinquencyState = new CaseDelinquencyStateDto(input.DelinquencyState);
        }

        public string AccountNumber { get; set; }

        public string AccountType { get; set; }

        public List<AgentDto> Agents { get; set; }

        public string BranchId { get; set; }

        public List<CaseActivityDto> CaseActivities { get; set; }

        public List<CaseEventDto> CaseEvents { get; set; }

        public string CaseNumber { get; set; }

        public CaseTypeDto CaseType { get; set; }

        public double Charges { get; set; }

        public string CollectorId { get; set; }

        public string Condition { get; set; }

        public string ContactPerson { get; set; }

        public List<CustomerDto> Customers { get; set; }

        public CaseDelinquencyStateDto DelinquencyState { get; set; }

        public DateTime? DueDate { get; set; }

        public string ExtraFieldsJson { get; set; }

        public DateTime? ImportedDate { get; set; }

        public double InterestRate { get; set; }

        public DateTime? LastPaymentDate { get; set; }

        public DateTime? MaturityDate { get; set; }

        public double OriginalAmount { get; set; }

        public DateTime? OriginDate { get; set; }

        public double OwedAmount { get; set; }

        public double PaidAmount { get; set; }

        public List<CasePaymentDto> Payments { get; set; }

        public CaseProductDto Product { get; set; }

        public RouteDto Route { get; set; }

        public string StateCode { get; set; }

        public string Status { get; set; }

        public CaseSystemStateDto SystemState { get; set; }

        public CaseTacticDto Tactic { get; set; }

        public string Terms { get; set; }

        public string ProductNumber { get; set; }

        public string City { get; set; }

        public string Zipcode { get; set; }

        public string Name { get; set; }
    }
}
