using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseTypeAppService : IApplicationService
    {
        Task<CaseTypeDto> Create(CaseTypeCreateDto input);

        Task Delete(Guid id);

        Task<CaseTypeDto> Get(Guid id);

        Task<PagedResultDto<CaseTypeDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<CaseTypeDto>> GetAllOrdered(PagedResultRequestDto paging);

        Task<PagedResultDto<CaseTypeListDto>> Search(CaseTypeSearchModel filters);

        Task<CaseTypeDto> Update(CaseTypeUpdateDto input);

        Task<List<CaseType>> GetAllCaseTypes();
    }
}
