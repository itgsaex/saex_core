﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTypeDto : EntityDto<Guid>
    {
        public CaseTypeDto()
        {
        }

        public CaseTypeDto(CaseType input)
        {
            Id = input.Id;
            DefaultColor = input.DefaultColor;
            IconImage = input.IconImage;
            Description = input.Description;
            Code = input.Code;

            if (input.Product != null)
                Product = new ProductDto(input.Product);
        }

        public string Code { get; set; }

        public string DefaultColor { get; set; }

        public string Description { get; set; }

        public string IconImage { get; set; }

        public virtual ProductDto Product { get; set; }
    }
}
