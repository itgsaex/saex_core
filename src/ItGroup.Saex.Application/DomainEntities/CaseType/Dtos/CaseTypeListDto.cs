﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTypeListDto : EntityDto<Guid>
    {
        public CaseTypeListDto()
        {
        }

        public CaseTypeListDto(CaseType input)
        {
            Id = input.Id;
            DefaultColor = input.DefaultColor;
            IconImage = input.IconImage;
            Description = input.Description;
            Code = input.Code;

            if (input.Product != null)
                ProductName = input.Product.Description;
        }

        public string Code { get; set; }

        public string DefaultColor { get; set; }

        public string Description { get; set; }

        public string IconImage { get; set; }

        public string ProductName { get; set; }
    }
}
