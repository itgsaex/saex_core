﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(CaseType))]
    public class CaseTypeCreateDto : EntityDto<Guid>
    {
        public CaseTypeCreateDto()
        {
        }

        public string Code { get; set; }

        public string DefaultColor { get; set; }

        public string Description { get; set; }

        public string IconImage { get; set; }

        public Guid ProductId { get; set; }
    }
}
