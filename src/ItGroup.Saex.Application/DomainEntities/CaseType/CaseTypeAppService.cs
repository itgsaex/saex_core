using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTypeAppService : AppServiceBase, ICaseTypeAppService
    {
        protected readonly CaseTypeManager _manager;

        public CaseTypeAppService(CaseTypeManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_CaseTypes_Modify)]
        public async Task<CaseTypeDto> Create(CaseTypeCreateDto input)
        {
            var model = ObjectMapper.Map<CaseType>(input);

            var exist = await _manager.GetByCode(input.Code);
            if (exist != null)
            {
                throw new EntityNotFoundException(L("RecordExist"));
            }

            var result = await _manager.CreateAsync(model);
            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Case Type record");

            return new CaseTypeDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_CaseTypes_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);

            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await _manager.DeleteAsync(model);

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Case Type record");
        }

        [AbpAuthorize(PermissionNames.Pages_CaseTypes_View)]
        public async Task<CaseTypeDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);

            if (result == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Case Type record");

            return new CaseTypeDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_CaseTypes_View)]
        public async Task<PagedResultDto<CaseTypeDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<CaseTypeDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<CaseType>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new CaseTypeDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_CaseTypes_View)]
        public async Task<PagedResultDto<CaseTypeDto>> GetAllOrdered(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<CaseTypeDto>();

            var results = await _manager.GetAllOrdered().ToListAsync() as IReadOnlyList<CaseType>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new CaseTypeDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_CaseTypes_View)]
        public async Task<PagedResultDto<CaseTypeListDto>> Search(CaseTypeSearchModel filters)
        {
            var res = new PagedResultDto<CaseTypeListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new CaseTypeListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_CaseTypes_Modify)]
        public async Task<CaseTypeDto> Update(CaseTypeUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _manager.GetAsync(input.Id);
            var oldModel = model.Clone() as CaseType;

            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.DefaultColor = input.DefaultColor;
            model.Description = input.Description;
            model.IconImage = input.IconImage;
            model.ProductId = input.ProductId;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Case Type record");

            return new CaseTypeDto(model);
        }

        public async Task<List<CaseType>> GetAllCaseTypes()
        {
            var result = _manager.GetAllQuery();
            return result.ToList();
        }
    }
}
