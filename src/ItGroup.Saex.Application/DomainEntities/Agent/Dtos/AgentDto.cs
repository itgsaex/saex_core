﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Users.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentDto : EntityDto<Guid>
    {
        public AgentDto()
        {
        }

        public AgentDto(Agent input)
        {
            if (input != null)
            {
                Id = input.Id;
                UserId = input.UserId;

                if (input.Type != null)
                    Type = (AgentType)input.Type;

                if (input.User != null)
                    User = new UserDto(input.User);
            }
        }

        public AgentType Type { get; set; }

        public UserDto User { get; set; }

        public long UserId { get; set; }
    }
}
