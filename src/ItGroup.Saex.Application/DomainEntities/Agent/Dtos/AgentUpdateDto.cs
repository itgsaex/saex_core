﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Agent))]
    public class AgentUpdateDto : EntityDto<Guid>
    {
        public AgentUpdateDto()
        {
        }

        public AgentType Type { get; set; }

        public long UserId { get; set; }
    }
}