using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAgentAppService : IApplicationService
    {
        Task<AgentDto> Create(AgentCreateDto input);

        Task Delete(Guid id);

        Task<AgentDto> Get(Guid id);

        Task<PagedResultDto<AgentDto>> GetAll(PagedResultRequestDto paging);

        Task<List<AgentListDto>> GetAllForList();

        Task<List<Agent>> GetAllAgentsForReport();

        Task<PagedResultDto<AgentDto>> Search(AgentSearchModel filters);

        Task<AgentDto> Update(AgentUpdateDto input);

        Task<List<Agent>> GetAllAgents();

        Task<List<vwAgent>> GetAgentConfig(string agentId);

        Task<PagedResultDto<AgentDto>> GetAgentsWithOutAssign(PagedResultRequestDto paging);
    }
}