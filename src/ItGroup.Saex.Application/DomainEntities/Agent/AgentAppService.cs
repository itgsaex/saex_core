using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using ItGroup.Saex.Authorization;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentAppService : AppServiceBase, IAgentAppService
    {
        protected readonly AgentManager _manager;
        protected readonly RegionManager _regionManager;
        protected readonly ListManager _listManager;

        public AgentAppService(AgentManager manager, AuditLogManager auditManager, RegionManager regionManager, ListManager listManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
            _regionManager = regionManager;
            _listManager = listManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Agents_Modify)]
        public async Task<AgentDto> Create(AgentCreateDto input)
        {
            var model = ObjectMapper.Map<Agent>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created an Agent record");

            return new AgentDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Agents_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Agent was not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted an Agent record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Agents_View)]
        public async Task<AgentDto> Get(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Agent not found for the provided Id");

            await LogTransaction(model, null, AuditLogOperationType.View, "Viewed an Agent record");

            return new AgentDto(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Agents_View)]
        public async Task<PagedResultDto<AgentDto>> GetAll(PagedResultRequestDto paging = null)
        {
            if (paging == null)
            {
                paging = new PagedResultRequestDto();
                paging.SkipCount = 0;
                paging.MaxResultCount = 25;
            }

            var output = new PagedResultDto<AgentDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<Agent>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new AgentDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Agents_View)]
        public async Task<List<AgentListDto>> GetAllForList()
        {
            return await _manager.GetAll().Select(x => new AgentListDto(x)).ToListAsync();
        }

        [AbpAuthorize(PermissionNames.Pages_Agents_View)]
        public Task<List<Agent>> GetAllAgentsForReport()
        {
            var results = _manager.GetAllWithDeletedQuery().Where(a => a.Type == AgentType.External).OrderBy(a => a.User.FullName).ToList();
            return Task.FromResult(results); 
        }

        [AbpAuthorize(PermissionNames.Pages_Agents_View)]
        public async Task<PagedResultDto<AgentDto>> Search(AgentSearchModel filters)
        {
            var res = new PagedResultDto<AgentDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new AgentDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Agents_Modify)]
        public async Task<AgentDto> Update(AgentUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("Agent id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as Agent;
            if (model == null) throw new EntityNotFoundException("Agent does not exist in database.");

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated an Agent record");

            return new AgentDto(model);
        }

        public Task<List<Agent>> GetAllAgents()
        {
            var results = _manager.GetAllQuery().ToList();
            return Task.FromResult(results);
        }

        public async Task<List<vwAgent>> GetAgentConfig(string agentId)
        {
            var results = await _manager.GetAgentConfig(agentId);
            return results;
        }

        public async Task<PagedResultDto<AgentDto>> GetAgentsWithOutAssign(PagedResultRequestDto paging = null)
        {
            var output = new PagedResultDto<AgentDto>();

            var results = await _manager.GetAll().ToListAsync();
            List<Agent> agentsWithoutAssign = new List<Agent>();

            foreach (var agent in results)
            {
                var assignedRegion = await _regionManager.GetAssignedByAgentId(agent.Id);
                var assignedList = await _listManager.GetAssignedByAgentId(agent.Id);

                if (assignedRegion.Count == 0 && assignedList.Count == 0)
                {
                    agentsWithoutAssign.Add(agent);
                }
            }

            output.TotalCount = agentsWithoutAssign.Count();
            output.Items = agentsWithoutAssign.Select(x => new AgentDto(x)).ToList();

            return output;
        }
    }
}
