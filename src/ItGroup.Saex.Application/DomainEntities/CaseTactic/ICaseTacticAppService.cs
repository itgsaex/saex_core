using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseTacticAppService : IApplicationService
    {
        Task<CaseTacticDto> Get(Guid id);

        Task<CaseTacticDto> Update(CaseTacticInputDto input);
    }
}