using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTacticAppService : AppServiceBase, ICaseTacticAppService
    {
        protected readonly CaseTacticManager _manager;

        public CaseTacticAppService(CaseTacticManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<CaseTacticDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("CaseTactic not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Case Tactic record");

            return new CaseTacticDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<CaseTacticDto> Update(CaseTacticInputDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("CaseTactic id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as CaseTactic;
            if (model == null) throw new EntityNotFoundException("CaseTactic does not exist in database.");

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Case Tactic record");
            return new CaseTacticDto(model);
        }
    }
}
