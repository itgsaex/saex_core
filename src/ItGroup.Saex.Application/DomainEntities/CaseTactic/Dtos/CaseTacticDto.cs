﻿using Abp.Application.Services.Dto;
using System;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTacticDto : EntityDto<Guid>
    {
        public CaseTacticDto()
        {
        }

        public CaseTacticDto(CaseTactic input)
        {
            Id = input.Id;
            CaseId = input.CaseId;
            AgentId = input.AgentId;
            IsPlanned = input.IsPlanned;
            VisitDays = input.VisitDays;
            VisitStartTime = input.VisitStartTime;
            VisitEndTime = input.VisitEndTime;
            CollectionDayOfMonth = input.CollectionDayOfMonth;
            CollectionTimeStart = input.CollectionTimeStart;
            CollectionTimeEnd = input.CollectionTimeEnd;

            if (input.VisitPlace != null)
                VisitPlace = new VisitPlaceDto(input.VisitPlace);
        }

        public Guid AgentId { get; set; }

        public Guid CaseId { get; set; }

        public int CollectionDayOfMonth { get; set; }

        public string CollectionTimeEnd { get; set; }

        public string CollectionTimeStart { get; set; }

        public bool IsPlanned { get; set; }

        public string VisitDays { get; set; }

        public string VisitEndTime { get; set; }

        public VisitPlaceDto VisitPlace { get; set; }

        public string VisitStartTime { get; set; }
    }
}