﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(CaseTactic))]
    public class CaseTacticInputDto : EntityDto<Guid>
    {
        public CaseTacticInputDto(CaseTactic input)
        {
            CaseId = input.CaseId;
            AgentId = input.AgentId;
            IsPlanned = input.IsPlanned;
            VisitDays = input.VisitDays;
            VisitStartTime = input.VisitStartTime;
            VisitEndTime = input.VisitEndTime;
            CollectionDayOfMonth = input.CollectionDayOfMonth;
            CollectionTimeStart = input.CollectionTimeStart;
            CollectionTimeEnd = input.CollectionTimeEnd;
            VisitPlaceId = input.VisitPlaceId;
        }

        public Guid AgentId { get; set; }

        public Guid CaseId { get; set; }

        public int CollectionDayOfMonth { get; set; }

        public string CollectionTimeEnd { get; set; }

        public string CollectionTimeStart { get; set; }

        public bool IsPlanned { get; set; }

        public string VisitDays { get; set; }

        public string VisitEndTime { get; set; }

        public Guid VisitPlaceId { get; set; }

        public string VisitStartTime { get; set; }
    }
}