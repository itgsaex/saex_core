using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CasePaymentAppService : AppServiceBase, ICasePaymentAppService
    {
        protected new readonly CasePaymentManager _manager;

        public CasePaymentAppService(CasePaymentManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<CasePaymentDto> Create(CasePaymentCreateDto input)
        {
            var model = ObjectMapper.Map<CasePayment>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Payment record");

            return new CasePaymentDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Case Payment not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Payment record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<CasePaymentDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("CasePayment not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Payment record");

            return new CasePaymentDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CasePaymentDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<CasePaymentDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<CasePayment>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new CasePaymentDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CasePaymentDto>> Search(CasePaymentSearchModel filters)
        {
            var res = new PagedResultDto<CasePaymentDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new CasePaymentDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<CasePaymentDto> Update(CasePaymentUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("CasePayment id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as CasePayment;
            if (model == null) throw new EntityNotFoundException("CasePayment does not exist in database.");

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Payment record");

            return new CasePaymentDto(model);
        }
    }
}
