using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICasePaymentAppService : IApplicationService
    {
        Task<CasePaymentDto> Create(CasePaymentCreateDto input);

        Task Delete(Guid id);

        Task<CasePaymentDto> Get(Guid id);

        Task<PagedResultDto<CasePaymentDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<CasePaymentDto>> Search(CasePaymentSearchModel filters);

        Task<CasePaymentDto> Update(CasePaymentUpdateDto input);
    }
}