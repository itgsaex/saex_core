﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(CasePayment))]
    public class CasePaymentCreateDto : EntityDto<Guid>
    {
        public CasePaymentCreateDto()
        {
        }

        public Guid CaseId { get; set; }

        public double PaymentAmount { get; set; }

        public DateTime PaymentDate { get; set; }
    }
}
