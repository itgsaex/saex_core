﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(CasePayment))]
    public class CasePaymentUpdateDto : EntityDto<Guid>
    {
        public CasePaymentUpdateDto()
        {
        }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}