﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CasePaymentListDto : EntityDto<Guid>
    {
        public CasePaymentListDto()
        {
        }

        public CasePaymentListDto(CasePayment input)
        {
            Id = input.Id;
            PaymentAmount = input.PaymentAmount;
            PaymentDate = input.PaymentDate;

            if (input.Case != null)
                Case = new CaseDto(input.Case);
        }

        public CaseDto Case { get; set; }

        public double PaymentAmount { get; set; }

        public DateTime PaymentDate { get; set; }
    }
}