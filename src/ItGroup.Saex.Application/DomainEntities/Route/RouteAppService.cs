using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class RouteAppService : AppServiceBase, IRouteAppService
    {
        protected readonly RouteManager _manager;

        protected readonly ProductManager _productManager;

        public RouteAppService(RouteManager manager, ProductManager productManager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _productManager = productManager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Routes_Modify)]
        public async Task Create(RouteCreateDto input)
        {
            var route = new Route()
            {
                Name = input.Name
            };

            var products = new List<RouteProduct>();
            foreach (var id in input.Products)
            {
                var rp = new RouteProduct()
                {
                    ProductId = id
                };

                products.Add(rp);
            }

            var postalCodes = new List<RoutePostalCode>();
            foreach (var id in input.PostalCodes)
            {
                var rpc = new RoutePostalCode()
                {
                    PostalCodeId = id
                };

                postalCodes.Add(rpc);
            }

            route.PostalCodes = postalCodes;
            route.Products = products;
            var routeID = _manager.GetAll().Max(r => r.RouteId.GetValueOrDefault()) + 1;
            route.RouteId = routeID;

            await _manager.CreateAsync(route);
            await LogTransaction(route, input, AuditLogOperationType.Create, "Created a Route record");
        }

        [AbpAuthorize(PermissionNames.Pages_Routes_Modify)]
        public async Task Delete(Guid id)
        {
            var res = await _manager.GetAsync(id);
            if (res == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await _manager.DeleteAsync(res);
            await LogTransaction(res, null, AuditLogOperationType.Delete, "Deleted a Route record");
        }

        [AbpAuthorize(PermissionNames.Pages_Routes_View)]
        public async Task<RouteDto> Get(Guid id)
        {
            var res = await _manager.GetAsync(id);
            if (res == null) throw new UserFriendlyException(L("RecordNotFoundError"));
            await LogTransaction(res, null, AuditLogOperationType.View, "Viewed a Route record");

            return new RouteDto(res);
        }

        [AbpAuthorize]
        public async Task<List<RouteListDto>> GetAllForList()
        {
            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<Route>;

            return results.Select(x => new RouteListDto(x)).ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_Routes_View)]
        public async Task<PagedResultDto<RouteListDto>> Search(RouteSearchModel filters)
        {
            var res = new PagedResultDto<RouteListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new RouteListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Routes_Modify)]
        public async Task Update(RouteUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as Route;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.Name = input.Name;

            var products = new List<RouteProduct>();
            foreach (var id in input.Products)
            {
                var rp = new RouteProduct()
                {
                    ProductId = id
                };

                products.Add(rp);
            }

            var postalCodes = new List<RoutePostalCode>();
            foreach (var id in input.PostalCodes)
            {
                var rpc = new RoutePostalCode()
                {
                    PostalCodeId = id
                };

                postalCodes.Add(rpc);
            }

            model.PostalCodes = postalCodes;
            model.Products = products;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Route record");
        }
    }
}
