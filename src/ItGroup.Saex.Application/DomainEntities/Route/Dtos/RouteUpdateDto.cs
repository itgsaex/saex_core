﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(Route))]
    public class RouteUpdateDto : EntityDto<Guid>
    {
        public RouteUpdateDto()
        {
        }

        public string Name { get; set; }

        public List<Guid> PostalCodes { get; set; }

        public List<Guid> Products { get; set; }
    }
}