﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class RouteListDto : EntityDto<Guid>
    {
        public RouteListDto()
        {
        }

        public RouteListDto(Route input)
        {
            Id = input.Id;
            Name = input.Name;
            RouteID = input.RouteId;

            if (input.PostalCodes != null && input.PostalCodes.Any())
                PostalCodes = input.PostalCodes.Select(x => new PostalCodeDto(x.PostalCode)).ToList();
        }

        public string Name { get; set; }

        public int? RouteID { get; set; }

        public List<PostalCodeDto> PostalCodes { get; set; }
    }
}