﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class RouteDto : EntityDto<Guid>
    {
        public RouteDto()
        {
        }

        public RouteDto(Route input)
        {
            Id = input.Id;
            Name = input.Name;
            RouteId = input.RouteId.GetValueOrDefault();

            if (input.PostalCodes != null && input.PostalCodes.Any())
            {
                PostalCodes = input.PostalCodes.Select(x => new PostalCodeListDto(x.PostalCode)).ToList();
            }
            else
            {
                PostalCodes = new List<PostalCodeListDto>();
            }
            

            if (input.Products != null && input.Products.Any())
            {
                Products = input.Products.Select(x => new ProductListDto(x.Product)).ToList();
            }
            else
            {
                Products = new List<ProductListDto>();
            }
                
        }

        public string Name { get; set; }

        public List<PostalCodeListDto> PostalCodes { get; set; }

        public List<ProductListDto> Products { get; set; }

        public int RouteId { get; set; }
    }
}