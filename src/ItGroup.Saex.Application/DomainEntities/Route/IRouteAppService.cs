using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IRouteAppService : IApplicationService
    {
        Task Create(RouteCreateDto input);

        Task Delete(Guid id);

        Task<RouteDto> Get(Guid id);

        Task<List<RouteListDto>> GetAllForList();

        Task<PagedResultDto<RouteListDto>> Search(RouteSearchModel filters);

        Task Update(RouteUpdateDto input);
    }
}
