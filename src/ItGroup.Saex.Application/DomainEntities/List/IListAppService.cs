using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IListAppService : IApplicationService
    {
        Task<ListDto> Create(ListCreateDto input);

        Task Delete(Guid id);

        Task<ListDto> Get(Guid id);

        Task<PagedResultDto<ListDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<ListDto>> Search(ListSearchModel filters);

        Task<ListDto> Update(ListUpdateDto input);

        Task<List<ListDto>> GetAllForList();
    }
}