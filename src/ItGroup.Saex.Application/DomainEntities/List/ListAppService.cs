using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListAppService : AppServiceBase, IListAppService
    {
        protected new readonly ListManager _manager;

        public ListAppService(ListManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        public async Task<ListDto> Create(ListCreateDto input)
        {
            var model = ObjectMapper.Map<List>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a List record");

            return new ListDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Lists_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("List not found for the provided Id.");

            await _manager.DeleteAsync(model);
            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a List record");
        }

        [AbpAuthorize(PermissionNames.Pages_Lists_View)]
        public async Task<ListDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("List not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a List record");

            return new ListDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Lists_View)]
        public async Task<PagedResultDto<ListDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ListDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<List>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ListDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Lists_View)]
        public async Task<PagedResultDto<ListDto>> Search(ListSearchModel filters)
        {
            var res = new PagedResultDto<ListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ListDto(x)).ToList();

            return res;
        }

        public async Task<ListDto> Update(ListUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("List id is null.");
            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as List;
            if (model == null) throw new EntityNotFoundException("List does not exist in database.");

            model.Name = input.Name;
            model.Query = input.Query;
            model.IsActive = input.IsActive;
            model.EstadoRiesgoId = input.EstadoRiesgoId;
            model.FieldsToPivot = input.FieldsToPivot;
            model.ProductSystemTable = input.ProductSystemTable;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a List record");

            return new ListDto(model);
        }

        public async Task<List<ListDto>> GetAllForList()
        {
            var results = await _manager.GetAll().Where(l => l.Type == ListType.Child).Select(l => new ListDto(l)).ToListAsync();
            return results;
        }
    }
}
