﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(List))]
    public class ListCreateDto : EntityDto<Guid>
    {
        public ListCreateDto()
        {
        }

        public virtual List<ListFieldCreateDto> Fields { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }

        public string Query { get; set; }

        public string SerializedQuery { get; set; }

        public ListType Type { get; set; }

        public int ReferenceId { get; set; }

        public string FieldsToPivot { get; set; }

        public int EstadoRiesgoId { get; set; }
        public string ProductSystemTable { get; set; }
    }
}
