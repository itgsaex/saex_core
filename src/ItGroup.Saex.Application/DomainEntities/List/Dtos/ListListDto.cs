﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListListDto : EntityDto<Guid>
    {
        public ListListDto()
        {
        }

        public ListListDto(List input)
        {
            Id = input.Id;
            Name = input.Name;
            Type = input.Type;
            IsActive = input.IsActive;
            Query = input.Query;
            SerializedQuery = input.SerializedQuery;
            ReferenceId = input.ReferenceId;
            FieldsToPivot = input.FieldsToPivot;
            EstadoRiesgoId = input.EstadoRiesgoId;
            ProductSystemTable = input.ProductSystemTable;
        }

        public virtual List<ListFieldDto> Fields { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }

        public string Query { get; set; }

        public string SerializedQuery { get; set; }

        public ListType Type { get; set; }

        public int ReferenceId { get; set; }

        public string FieldsToPivot { get; set; }

        public int EstadoRiesgoId { get; set; }
        public string ProductSystemTable { get; set; }
    }
}