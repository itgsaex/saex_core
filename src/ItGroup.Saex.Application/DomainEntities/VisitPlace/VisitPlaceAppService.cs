using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using EnumsNET;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class VisitPlaceAppService : AppServiceBase, IVisitPlaceAppService
    {
        protected readonly VisitPlaceManager _manager;

        public VisitPlaceAppService(VisitPlaceManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_VisitPlaces_Modify)]
        public async Task<VisitPlaceDto> Create(VisitPlaceCreateDto input)
        {
            var res = ObjectMapper.Map<VisitPlace>(input);
            var result = await _manager.CreateAsync(res);
            await LogTransaction(result, input, AuditLogOperationType.Create, "Created a Visit Place record");

            return new VisitPlaceDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_VisitPlaces_Modify)]
        public async Task Delete(Guid id)
        {
            var res = await _manager.GetAsync(id);
            if (res == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(res, null, AuditLogOperationType.Delete, "Deleted a Visit Place record");

            await _manager.DeleteAsync(res);
        }

        [AbpAuthorize(PermissionNames.Pages_VisitPlaces_View)]
        public async Task<VisitPlaceDto> Get(Guid id)
        {
            var res = await _manager.GetAsync(id);
            if (res == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(res, null, AuditLogOperationType.View, "Viewed a Visit Place record");

            return new VisitPlaceDto(res);
        }

        public async Task<List<VisitPlace>> GetAllVisitPlaces()
        {
            var result = _manager.GetAllQuery();
            return result.ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_VisitPlaces_View)]
        public async Task<List<VisitPlaceListDto>> GetAll()
        {
            var output = new List<VisitPlaceListDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<VisitPlace>;

            return results.Select(x => new VisitPlaceListDto(x)).ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_VisitPlaces_View)]
        public async Task<PagedResultDto<VisitPlaceListDto>> Search(VisitPlaceSearchModel filters)
        {
            var output = new PagedResultDto<VisitPlaceListDto>();

            var results = await _manager.GetFiltered(filters);

            output.TotalCount = results.TotalCount;
            output.Items = results.Items.Select(x => new VisitPlaceListDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_VisitPlaces_Modify)]
        public async Task<VisitPlaceDto> Update(VisitPlaceUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("VisitPlace id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as VisitPlace;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.Code = input.Code;
            model.Description = input.Description;
            model.LastModificationTime = DateTime.UtcNow;
            model.LastModifierUserId = AbpSession.UserId;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Visit Place record");

            return new VisitPlaceDto(model);
        }

        //private async Task LogTransaction(VisitPlace domainObject, EntityDto<Guid> inputObject, AuditLogOperationType operationType)
        //{
        //    var audtiObjectValue = "";

        //    if (inputObject != null)
        //        audtiObjectValue = JsonConvert.SerializeObject(inputObject);

        //    var domainAudtiObjectValue = JsonConvert.SerializeObject(domainObject);

        //    var user = await UserManager.FindByIdAsync(AbpSession.UserId.ToString());

        //    await _audit.CreateAsync(new AuditLog("VisitPlace", domainObject.Id.ToString(), Enums.GetName(operationType), audtiObjectValue, domainAudtiObjectValue, user.UserName));
        //}
    }
}
