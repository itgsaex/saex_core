using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IVisitPlaceAppService : IApplicationService
    {
        Task<VisitPlaceDto> Create(VisitPlaceCreateDto input);

        Task Delete(Guid id);

        Task<VisitPlaceDto> Get(Guid id);

        Task<List<VisitPlaceListDto>> GetAll();

        Task<List<VisitPlace>> GetAllVisitPlaces();

        Task<PagedResultDto<VisitPlaceListDto>> Search(VisitPlaceSearchModel filters);

        Task<VisitPlaceDto> Update(VisitPlaceUpdateDto input);
    }
}