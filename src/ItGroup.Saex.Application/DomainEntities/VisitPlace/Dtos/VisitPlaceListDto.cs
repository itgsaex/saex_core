﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class VisitPlaceListDto : EntityDto<Guid>
    {
        public VisitPlaceListDto()
        {
        }

        public VisitPlaceListDto(VisitPlace input)
        {
            Id = input.Id;
            Code = input.Code;
            Description = input.Description;
        }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}