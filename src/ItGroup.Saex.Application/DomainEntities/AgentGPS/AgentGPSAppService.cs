using System.Threading.Tasks;
using ItGroup.Saex.Manager.DomainEntities;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentGPSAppService : AppServiceBase, IAgentGPSAppService
    {
        protected new readonly AgentGPSManager _manager;

        public AgentGPSAppService(AgentGPSManager manager) : base()
        {
            _manager = manager;
        }

        public async Task SyncAgentGPSModel(AgentGPS input)
        {
            await _manager.PostAgentGPSLocation(input);
        }
    }
}
