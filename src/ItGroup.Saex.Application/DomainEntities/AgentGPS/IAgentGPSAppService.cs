using Abp.Application.Services;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAgentGPSAppService : IApplicationService
    {
        Task SyncAgentGPSModel(AgentGPS input);
    }
}

