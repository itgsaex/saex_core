﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseCommentListDto : EntityDto<Guid>
    {
        public CaseCommentListDto()
        {
        }

        public CaseCommentListDto(CaseComment input)
        {
            Id = input.Id;

            Comment = input.Comment;

            if (input.Case != null)
                Case = new CaseDto(input.Case);

            if (input.Agent != null)
                Agent = new AgentDto(input.Agent);
        }

        public AgentDto Agent { get; set; }

        public CaseDto Case { get; set; }

        public string Comment { get; set; }
    }
}
