﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(CaseComment))]
    public class CaseCommentInputDto : EntityDto<Guid>
    {
        public CaseCommentInputDto()
        {
        }

        public Guid CaseId { get; set; }

        public string Comment { get; set; }
    }
}
