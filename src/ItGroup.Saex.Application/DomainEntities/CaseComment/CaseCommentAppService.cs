using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseCommentAppService : AppServiceBase, ICaseCommentAppService
    {
        protected readonly CaseCommentManager _manager;

        public CaseCommentAppService(CaseCommentManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<CaseCommentDto> Create(CaseCommentInputDto input)
        {
            var model = ObjectMapper.Map<CaseComment>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Case Comment record");

            return new CaseCommentDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            var currentUser = AbpSession.UserId;

            if (currentUser != model.CreatorUserId)
                throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Case Comment record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<CaseCommentDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a comment record");

            return new CaseCommentDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CaseCommentDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<CaseCommentDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<CaseComment>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new CaseCommentDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_View)]
        public async Task<PagedResultDto<CaseCommentDto>> Search(CaseCommentSearchModel filters)
        {
            var res = new PagedResultDto<CaseCommentDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new CaseCommentDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_Cases_Modify)]
        public async Task<CaseCommentDto> Update(CaseCommentInputDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as CaseComment;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a comment record");

            return new CaseCommentDto(model);
        }
    }
}
