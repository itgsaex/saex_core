using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseCommentAppService : IApplicationService
    {
        Task<CaseCommentDto> Create(CaseCommentInputDto input);

        Task Delete(Guid id);

        Task<CaseCommentDto> Get(Guid id);

        Task<PagedResultDto<CaseCommentDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<CaseCommentDto>> Search(CaseCommentSearchModel filters);

        Task<CaseCommentDto> Update(CaseCommentInputDto input);
    }
}
