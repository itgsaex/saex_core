using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IProductRuleAppService : IApplicationService
    {
        Task<ProductRuleDto> Create(ProductRuleCreateDto input);

        Task Delete(Guid id);

        Task<ProductRuleDto> Get(Guid id);

        Task<PagedResultDto<ProductRuleDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<ProductRuleListDto>> Search(ProductRuleSearchModel filters);

        Task<ProductRuleDto> Update(ProductRuleUpdateDto input);
    }
}