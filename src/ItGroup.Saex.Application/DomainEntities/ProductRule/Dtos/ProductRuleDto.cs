﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ProductRuleDto : EntityDto<Guid>
    {
        public ProductRuleDto()
        {
        }

        public ProductRuleDto(ProductRule input)
        {
            Id = input.Id;
            Type = input.Type;

            if (input.Product != null)
                Product = new ProductDto(input.Product);
        }

        public ProductDto Product { get; set; }

        public RuleType Type { get; set; }
    }
}