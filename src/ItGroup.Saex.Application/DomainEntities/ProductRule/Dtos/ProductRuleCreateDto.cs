﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(ProductRule))]
    public class ProductRuleCreateDto : EntityDto<Guid>
    {
        public ProductRuleCreateDto()
        {
        }

        public Guid ProductId { get; set; }

        public RuleType Type { get; set; }
    }
}
