using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ProductRuleAppService : AppServiceBase, IProductRuleAppService
    {
        protected new readonly ProductRuleManager _manager;

        public ProductRuleAppService(ProductRuleManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_ProductRules_Modify)]
        public async Task<ProductRuleDto> Create(ProductRuleCreateDto input)
        {
            //validate if exist
            var exist = _manager.GetAllQuery().Where(x => x.ProductId == input.ProductId).Any();

            if (exist)
                throw new UserFriendlyException(L("ProductRuleDuplicateError"));

            var model = ObjectMapper.Map<ProductRule>(input);

            var result = await _manager.CreateAsync(model);
            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Product Rule");

            return new ProductRuleDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ProductRules_Modify)]
        public async Task Delete(Guid id)
        {
            var res = await _manager.GetAsync(id);
            if (res == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(res, null, AuditLogOperationType.Delete, "Deleted a Product Rule");

            await _manager.DeleteAsync(res);
        }

        [AbpAuthorize(PermissionNames.Pages_ProductRules_View)]
        public async Task<ProductRuleDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Product Rule");

            return new ProductRuleDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ProductRules_View)]
        public async Task<PagedResultDto<ProductRuleDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ProductRuleDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<ProductRule>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ProductRuleDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_ProductRules_View)]
        public async Task<PagedResultDto<ProductRuleListDto>> Search(ProductRuleSearchModel filters)
        {
            var res = new PagedResultDto<ProductRuleListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ProductRuleListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_ProductRules_Modify)]
        public async Task<ProductRuleDto> Update(ProductRuleUpdateDto input)
        {
            if (input.Id == null)
                throw new UserFriendlyException("Id cannot be null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as ProductRule;
            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            model.ProductId = input.ProductId;
            model.Type = input.Type;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Product Rule");

            return new ProductRuleDto(model);
        }
    }
}
