﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.Web.Host.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IQueryFieldsAppService : IApplicationService
    {
        Task<QueryFieldsDto> Create(QueryFieldsDto input);

        Task<QueryFieldsDto> Update(QueryFieldsDto input);

        Task Delete(Guid id);

        Task<QueryFieldsDto> Get(Guid id);

        Task<PagedResultDto<QueryFieldsDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<QueryFieldsDto>> Search(QueryFieldsSearchModel filters);

        //Task SyncActivityModel(ActivitySyncModel input);
    }
}

