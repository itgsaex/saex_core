﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItGroup.Saex.DomainEntities
{
    public class QueryFieldsDto : EntityDto<Guid>
    {
        public QueryFieldsDto()
        {
        }

        public QueryFieldsDto(QueryFields input)
        {
            Id = input.Id;
            FieldID = input.FieldId;
            MasterList = input.MasterList;
            ChildList = input.ChildList;
            RiskList = input.RiskList;
        }

        public int FieldID { get; set; }

        public bool MasterList { get; set; }

        public bool ChildList { get; set; }

        public bool RiskList { get; set; }
    }
}