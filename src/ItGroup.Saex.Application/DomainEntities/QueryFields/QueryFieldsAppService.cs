﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Web.Host.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class QueryFieldsAppService : AppServiceBase, IQueryFieldsAppService
    {
        protected readonly QueryFieldsManager _manager;

        public QueryFieldsAppService(QueryFieldsManager manager) : base()
        {
            _manager = manager;
        }

        public async Task<QueryFieldsDto> Create(QueryFieldsDto input)
        {
            var model = ObjectMapper.Map<QueryFields>(input);

            await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Query Field record");

            return new QueryFieldsDto(model);
        }

        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Query Field not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted an Query Field record");

            await _manager.DeleteAsync(model);
        }

        public async Task<QueryFieldsDto> Get(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Query Field not found for the provided Id");

            await LogTransaction(model, null, AuditLogOperationType.View, "Viewed an Query Field record");

            return new QueryFieldsDto(model);
        }

        public async Task<PagedResultDto<QueryFieldsDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<QueryFieldsDto>();

            var models = await _manager.GetAll().ToListAsync() as IReadOnlyList<QueryFields>;

            output.TotalCount = models.Count();
            output.Items = models.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new QueryFieldsDto(x)).ToList();

            return output;
        }

        public async Task<PagedResultDto<QueryFieldsDto>> Search(QueryFieldsSearchModel filters)
        {
            var res = new PagedResultDto<QueryFieldsDto>();

            var models = await _manager.GetFiltered(filters);

            res.TotalCount = models.TotalCount;
            res.Items = models.Items.Select(x => new QueryFieldsDto(x)).ToList();

            return res;
        }

        //public async Task SyncActivityModel(ActivitySyncModel input)
        //{
        //    await _manager.FlushActivitySyncModel(input);
        //}

        public async Task<QueryFieldsDto> Update(QueryFieldsDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("Query Field id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);

            if (model == null) throw new EntityNotFoundException("Query Field does not exist in database.");

            await _manager.UpdateAsync(model);

            return new QueryFieldsDto(model);
        }
    }
}
