using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemStateAppService : AppServiceBase, ISystemStateAppService
    {
        protected new readonly MacroStateManager _macroStateManager;

        protected new readonly SystemStateManager _systemStateManager;

        protected new readonly SystemSubStateManager _systemSubStateManager;

        public SystemStateAppService(
            SystemStateManager systemStateManager,
            SystemSubStateManager systemSubStateManager,
            MacroStateManager macroStateManager,
            AuditLogManager auditManager
        ) : base()
        {
            _systemSubStateManager = systemSubStateManager;
            _systemStateManager = systemStateManager;
            _macroStateManager = macroStateManager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_SystemStates_Modify)]
        public async Task<SystemStateDto> Create(SystemStateCreateDto input)
        {
            var systemState = ObjectMapper.Map<SystemState>(input);

            if (input.SubStateId != null)
            {
                var substate = await _systemSubStateManager.GetAsync((Guid)input.SubStateId);
                if (substate == null) throw new UserFriendlyException(L("RecordNotFoundError"));

                systemState.SubStateId = substate.Id;
            }

            var macroState = await _macroStateManager.GetAsync(input.MacroStateId);
            if (macroState == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            systemState.MacroStateId = macroState.Id;

            var result = await _systemStateManager.CreateAsync(systemState);

            await LogTransaction(result, input, AuditLogOperationType.Create, "Created a System State");

            return new SystemStateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_SystemStates_Modify)]
        public async Task Delete(Guid id)
        {
            var res = await _systemStateManager.GetAsync(id);
            if (res == null) throw new UserFriendlyException("System State not found for the provided Id.");

            await LogTransaction(res, null, AuditLogOperationType.Delete, "Deleted a System State");

            await _systemStateManager.DeleteAsync(res);
        }

        [AbpAuthorize(PermissionNames.Pages_SystemStates_View)]
        public async Task<SystemStateDto> Get(Guid id)
        {
            var result = await _systemStateManager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("System State not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a System State");

            return new SystemStateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_SystemStates_View)]
        public async Task<PagedResultDto<SystemStateDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<SystemStateDto>();

            var results = await _systemStateManager.GetAll().ToListAsync() as IReadOnlyList<SystemState>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new SystemStateDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_SystemStates_View)]
        public async Task<PagedResultDto<SystemStateDto>> Search(SystemStateSearchModel filters)
        {
            var res = new PagedResultDto<SystemStateDto>();

            var results = await _systemStateManager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new SystemStateDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_SystemStates_Modify)]
        public async Task<SystemStateDto> Update(SystemStateUpdateDto input)
        {
            var model = await _systemStateManager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as SystemState;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.Code = input.Code;
            model.Description = input.Description;

            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            model.SubStateId = input.SubStateId;

            if (input.SubStateId != null)
            {
                var substate = await _systemSubStateManager.GetAsync((Guid)input.SubStateId);
                if (substate == null) throw new UserFriendlyException(L("RecordNotFoundError"));
                model.SubStateId = substate.Id;
            }

            var macroState = await _macroStateManager.GetAsync(input.MacroStateId);
            if (macroState == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            model.MacroStateId = macroState.Id;

            await _systemStateManager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a System State");

            return new SystemStateDto(model);
        }
    }
}
