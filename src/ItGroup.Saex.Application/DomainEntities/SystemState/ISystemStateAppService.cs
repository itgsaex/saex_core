using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISystemStateAppService : IApplicationService
    {
        Task<SystemStateDto> Create(SystemStateCreateDto input);

        Task Delete(Guid id);

        Task<SystemStateDto> Get(Guid id);

        Task<PagedResultDto<SystemStateDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<SystemStateDto>> Search(SystemStateSearchModel filters);

        Task<SystemStateDto> Update(SystemStateUpdateDto input);
    }
}