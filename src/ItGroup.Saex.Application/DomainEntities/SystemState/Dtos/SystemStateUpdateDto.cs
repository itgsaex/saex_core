﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(SystemState))]
    public class SystemStateUpdateDto : EntityDto<Guid>
    {
        public SystemStateUpdateDto()
        {
        }

        public string Code { get; set; }

        public string Description { get; set; }

        public Guid MacroStateId { get; set; }

        public Guid? SubStateId { get; set; }
    }
}