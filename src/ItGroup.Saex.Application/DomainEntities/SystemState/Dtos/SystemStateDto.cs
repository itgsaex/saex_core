﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemStateDto : EntityDto<Guid>
    {
        public SystemStateDto()
        {
        }

        public SystemStateDto(SystemState input)
        {
            Id = input.Id;
            Code = input.Code;
            Description = input.Description;

            if (input.SubState != null)
                SubState = new SystemSubStateDto(input.SubState);

            if (input.MacroState != null)
                MacroState = new MacroStateDto(input.MacroState);
        }

        public string Code { get; set; }

        public string Description { get; set; }

        public virtual MacroStateDto MacroState { get; set; }

        public virtual SystemSubStateDto SubState { get; set; }
    }
}