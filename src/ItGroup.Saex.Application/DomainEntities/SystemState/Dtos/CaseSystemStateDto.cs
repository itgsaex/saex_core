﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseSystemStateDto : EntityDto<Guid>
    {
        public CaseSystemStateDto()
        {
        }

        public CaseSystemStateDto(CaseSystemState input)
        {
            Id = input.Id;
            Value = input.Value;

            if (input.SystemState != null)
                SystemState = new SystemStateDto(input.SystemState);
        }

        public SystemStateDto SystemState { get; set; }

        public string Value { get; set; }
    }
}
