using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using ItGroup.Saex.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IReportAppService : IApplicationService
    {
        Task<PagedResultDto<AuditReportModel>> Audit(AuditReportSearchModel filters);

        Task<PagedResultDto<CaseReportModel>> Cases(CaseReportSearchModel filters);

        Task<IReadOnlyList<UsersReport>> GetUsersReportAsync(UsersReportRequest request);
        Task<List<string>> AuditLogTables();
    }
}
