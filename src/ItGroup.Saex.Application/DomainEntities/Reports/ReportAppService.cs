using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using ItGroup.Saex.Manager.DomainEntities.Report.Models.Kpi;
using ItGroup.Saex.Manager.DomainEntities.Report.Models.ReceivedCasesVsAssigned;
using ItGroup.Saex.Manager.DomainEntities.Report.Models.UnassignedCases;
using ItGroup.Saex.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public class ReportAppService : AppServiceBase, IReportAppService
    {
        protected readonly ReportManager _manager;

        protected readonly ProductManager _productManager;

        public ReportAppService(
            ReportManager manager,
            ProductManager productManager
        ) : base()
        {
            _manager = manager;
            _productManager = productManager;
        }

        [AbpAuthorize(PermissionNames.Pages_Audit_Report_View)]
        public async Task<PagedResultDto<AuditReportModel>> Audit(AuditReportSearchModel filters)
        {
            return await _manager.GetAuditReport(filters);
        }

        public async Task<List<string>> AuditLogTables()
        {
            return await _manager.AuditLogTables();
        }

        public async Task<PagedResultDto<CaseReportModel>> Cases(CaseReportSearchModel filters)
        {
            var res = await _manager.GetCasesReport(filters);
            return res;
        }

        //[AbpAuthorize(PermissionNames.Pages_Cases_Report_View)]
        public async Task<KpiReportModel> Kpi(KpiReportSearchModel filters)
        {
            return await _manager.GetKpiReport(filters);
        }

        //[AbpAuthorize(PermissionNames.Pages_Cases_Report_View)]
        public ReceivedCasesVsAssignedReportModel ReceivedCasesVsAssigned()
        {
            return _manager.GetReceivedCasesVsAssignedReport();
        }

        //[AbpAuthorize(PermissionNames.Pages_Cases_Report_View)]
        public async Task<List<Case>> TransferedCases()
        {
            return await _manager.GetTransferedCasesReport();
        }

        //[AbpAuthorize(PermissionNames.Pages_Cases_Report_View)]
        public async Task<UnassignedCasesReportModel> UnassignedCases(UnassignedCasesReportSearchModel filters)
        {
            Product product = null;

            if (filters.ProductId != null)
            {
                product = await _productManager.GetAsync((Guid)filters.ProductId);

                if (product == null) throw new EntityNotFoundException(L("RecordNotFoundError"));
            }

            var cases = await _manager.GetUnassignedCasesReport(filters);

            var model = new UnassignedCasesReportModel()
            {
                Product = product,
                Cases = cases.Select(x => new UnassignedCasesListItemModel(x)).ToList(),
                PostalCode = filters.PostalCode
            };

            return model;
        }

        //[AbpAuthorize(PermissionNames.Pages_Cases_Report_View)]
        public async Task<UnfilteredCasesByMasterListReportModel> UnfilteredCasesByMasterList(UnfilteredCasesByMasterListSearchModel filters)
        {
            Product product = null;

            var cases = await _manager.GetUnfilteredCasesByMasterListReport(filters);

            var model = new UnfilteredCasesByMasterListReportModel()
            {
                AccountNumber = filters.AccountNumber,
                Cases = cases.Select(x => new UnfilteredCasesByMasterListItemModel(x)).ToList(),
                CustomerName = filters.CustomerName
            };

            return model;
        }
     
        public Task<IReadOnlyList<UsersReport>> GetUsersReportAsync(UsersReportRequest request) =>
            _manager.GetUsersReportAsync(request);
    }
}
