﻿using System;
using System.Collections.Generic;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities.Reports.Dtos
{
    public class KpiReportDto
    {
        public KpiReportDateRangeType DateType { get; set; }

        public DateTime EndDate { get; set; }

        public int Messages { get; set; }

        public int MortagesBetterOrStable { get; set; }

        public int PotentialBetterOrStable { get; set; }

        public int PreLossBetterOrStable { get; set; }

        public DateTime PrintDate { get; set; }

        public int PromisesAmount { get; set; }

        public int ReceivedPayments { get; set; }

        public int ReferralsBetterOrStable { get; set; }

        public int RemovedAccounts { get; set; }

        public int Repositions { get; set; }

        public int Scorecards_PortentialplusPreLoss { get; set; }

        public int Scorecards_ReferredEarlyStageAndCommercial { get; set; }

        public int Scorecards_ReferredMortgage { get; set; }

        public DateTime StartDate { get; set; }

        public int TransferedAccounts { get; set; }

        public int VisitedAccounts { get; set; }
    }
}
