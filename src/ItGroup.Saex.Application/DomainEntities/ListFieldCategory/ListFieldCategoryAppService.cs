using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListFieldCategoryAppService : AppServiceBase, IListFieldCategoryAppService
    {
        protected new readonly ListFieldCategoryManager _manager;

        public ListFieldCategoryAppService(ListFieldCategoryManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_ListFieldCategories_Modify)]
        public async Task<ListFieldCategoryDto> Create(ListFieldCategoryCreateDto input)
        {
            var model = ObjectMapper.Map<ListFieldCategory>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a List Field Category record");

            return new ListFieldCategoryDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ListFieldCategories_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("List Field Category not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a List Field Category record");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_ListFieldCategories_View)]
        public async Task<ListFieldCategoryDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("List Field Category not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a List Field Category record");

            return new ListFieldCategoryDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ListFieldCategories_View)]
        public async Task<PagedResultDto<ListFieldCategoryDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ListFieldCategoryDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<ListFieldCategory>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ListFieldCategoryDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_ListFieldCategories_View)]
        public async Task<PagedResultDto<ListFieldCategoryDto>> Search(ListFieldCategorySearchModel filters)
        {
            var res = new PagedResultDto<ListFieldCategoryDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ListFieldCategoryDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_ListFieldCategories_Modify)]
        public async Task<ListFieldCategoryDto> Update(ListFieldCategoryUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("ListFieldCategory id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as ListFieldCategory;
            if (model == null) throw new EntityNotFoundException("ListFieldCategory does not exist in database.");

            await _manager.UpdateAsync(model);

            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a List Field Category record");

            return new ListFieldCategoryDto(model);
        }
    }
}
