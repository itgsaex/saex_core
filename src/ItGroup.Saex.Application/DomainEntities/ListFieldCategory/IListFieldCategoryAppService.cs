using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IListFieldCategoryAppService : IApplicationService
    {
        Task<ListFieldCategoryDto> Create(ListFieldCategoryCreateDto input);

        Task Delete(Guid id);

        Task<ListFieldCategoryDto> Get(Guid id);

        Task<PagedResultDto<ListFieldCategoryDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<ListFieldCategoryDto>> Search(ListFieldCategorySearchModel filters);

        Task<ListFieldCategoryDto> Update(ListFieldCategoryUpdateDto input);
    }
}