﻿using ItGroup.Saex.Manager.DomainEntities;
using System.Collections.Generic;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class EstadoRiesgoService : IEstadoRiesgoService
    {
        protected new readonly EstadoRiesgoManager _manager;
        public EstadoRiesgoService(EstadoRiesgoManager manager) : base()
        {
            _manager = manager;
        }

        public List<EstadoRiesgo> GetAllEstadoRiesgos()
        {
            var result = _manager.GetAllEstadoRiesgos().ToList();
            return result;
        }
    }
}
