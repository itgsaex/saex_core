﻿using Abp.Application.Services;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public interface IEstadoRiesgoService : IApplicationService
    {
        List<EstadoRiesgo> GetAllEstadoRiesgos();
    }
}
