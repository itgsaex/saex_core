using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class PaymentDelayReasonAppService : AppServiceBase, IPaymentDelayReasonAppService
    {
        protected new readonly PaymentDelayReasonManager _manager;

        public PaymentDelayReasonAppService(PaymentDelayReasonManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_PaymentDelayReasons_Modify)]
        public async Task<PaymentDelayReasonDto> Create(PaymentDelayReasonCreateDto input)
        {
            var model = ObjectMapper.Map<PaymentDelayReason>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Payment Delay Reason");

            return new PaymentDelayReasonDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_PaymentDelayReasons_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Payment Delay Reason");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_PaymentDelayReasons_View)]
        public async Task<PaymentDelayReasonDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Payment Delay Reason");

            return new PaymentDelayReasonDto(result);
        }

        public async Task<List<PaymentDelayReason>> GetAllPaymentDelayReasons()
        {
            var result = _manager.GetAllQuery();
            return result.ToList();
        }

        [AbpAuthorize(PermissionNames.Pages_PaymentDelayReasons_View)]
        public async Task<PagedResultDto<PaymentDelayReasonDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<PaymentDelayReasonDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<PaymentDelayReason>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new PaymentDelayReasonDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_PaymentDelayReasons_View)]
        public async Task<PagedResultDto<PaymentDelayReasonListDto>> Search(PaymentDelayReasonSearchModel filters)
        {
            var res = new PagedResultDto<PaymentDelayReasonListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new PaymentDelayReasonListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_PaymentDelayReasons_Modify)]
        public async Task<PaymentDelayReasonDto> Update(PaymentDelayReasonUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as PaymentDelayReason;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.Code = input.Code;
            model.Description = input.Description;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Payment Delay Reason");

            return new PaymentDelayReasonDto(model);
        }
    }
}
