using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IPaymentDelayReasonAppService : IApplicationService
    {
        Task<PaymentDelayReasonDto> Create(PaymentDelayReasonCreateDto input);

        Task Delete(Guid id);

        Task<PaymentDelayReasonDto> Get(Guid id);

        Task<PagedResultDto<PaymentDelayReasonDto>> GetAll(PagedResultRequestDto paging);

        Task<List<PaymentDelayReason>> GetAllPaymentDelayReasons();

        Task<PagedResultDto<PaymentDelayReasonListDto>> Search(PaymentDelayReasonSearchModel filters);

        Task<PaymentDelayReasonDto> Update(PaymentDelayReasonUpdateDto input);
    }
}