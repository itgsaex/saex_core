﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(PaymentDelayReason))]
    public class PaymentDelayReasonUpdateDto : EntityDto<Guid>
    {
        public PaymentDelayReasonUpdateDto()
        {
        }

        public PaymentDelayReasonUpdateDto(PaymentDelayReason input)
        {
            Id = input.Id;
            Code = input.Code;
            Description = input.Description;
        }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}