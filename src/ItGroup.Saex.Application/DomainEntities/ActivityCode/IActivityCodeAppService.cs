using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IActivityCodeAppService : IApplicationService
    {
        Task<ActivityCodeDto> Create(ActivityCodeCreateDto input);

        Task Delete(Guid id);

        Task<ActivityCodeDto> Get(Guid id);

        Task<PagedResultDto<ActivityCodeDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<ActivityCodeListDto>> Search(ActivityCodeSearchModel filters);

        Task<ActivityCodeDto> Update(ActivityCodeUpdateDto input);

        Task<List<ActivityCode>> GetAllActivityCodes();
    }
}
