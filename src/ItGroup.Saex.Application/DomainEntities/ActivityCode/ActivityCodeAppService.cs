using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.UI;
using ItGroup.Saex.Authorization;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ActivityCodeAppService : AppServiceBase, IActivityCodeAppService
    {
        protected new readonly ActivityCodeManager _manager;
        private readonly IRepository<ActivityCodeDetail, Guid> _detailRepository;

        public ActivityCodeAppService(ActivityCodeManager manager, AuditLogManager auditManager, IRepository<ActivityCodeDetail, Guid> detailRepository
) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
            _detailRepository = detailRepository;
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_View)]
        public async Task<ActivityCodeDto> Create(ActivityCodeCreateDto input)
        {
            var model = new ActivityCode()
            {
                Code = input.Code,
                Description = input.Description,
                Type = input.ActivityCodeType,
                IsActive = input.IsActive,
                IsGlobal = input.IsGlobal
            };

            var questions = new List<ActivityCodeDetail>();

            if (input.Questions != null && input.Questions.Any())
            {
                foreach (var question in input.Questions)
                {
                    var q = new ActivityCodeDetail()
                    {
                        Question = question.Description,
                        Type = (int)question.Type,
                        Order = question.Position,
                        Required = question.IsRequired,
                        ActivityCodeType = (int)model.Type,
                        ActivityCode = model
                    };

                    if (question.Parameters != null && question.Parameters.Any())
                    {
                        List<string> options = new List<string>();
                        foreach (var option in question.Parameters)
                        {
                            options.Add(option.Description);
                        }

                        q.Parameters = string.Join(";",options);
                    }

                    questions.Add(q);
                }

            }

            model.ActivityCodeDetails = questions;

            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created an Activity Code record");
            return new ActivityCodeDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Activity Code not found for the provided Id.");

            await _manager.DeleteAsync(model);
            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted an Activity Code record");
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_View)]
        public async Task<ActivityCodeDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("ActivityCode not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed an Activity Code record");
            return new ActivityCodeDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_View)]
        public async Task<PagedResultDto<ActivityCodeDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ActivityCodeDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<ActivityCode>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ActivityCodeDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_View)]
        public async Task<PagedResultDto<ActivityCodeListDto>> Search(ActivityCodeSearchModel filters)
        {
            var res = new PagedResultDto<ActivityCodeListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ActivityCodeListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_ActivityCodes_Modify)]
        public async Task<ActivityCodeDto> Update(ActivityCodeUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("ActivityCode id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);

            if (model == null) 
                throw new EntityNotFoundException("ActivityCode does not exist in database.");

            var oldModel = model.Clone() as ActivityCode;
           
            model.Description = input.Description;
            model.Type = input.Type;
            model.IsActive = input.IsActive;
            model.IsGlobal = input.IsGlobal;

            foreach (var detail in model.ActivityCodeDetails)
            {
                _detailRepository.Delete(detail);
            }

            model.ActivityCodeDetails.ToList().Clear();

            var questions = new List<ActivityCodeDetail>();

            if (input.Questions != null)
            {
                foreach (var question in input.Questions)
                {
                    var q = new ActivityCodeDetail()
                    {
                        Question = question.Description,
                        Type = (int)question.Type,
                        Order = question.Position,
                        Required = question.IsRequired,
                        ActivityCodeType = (int)model.Type,
                        ActivityCode = model
                    };

                    if (question.Parameters != null && question.Parameters.Any())
                    {
                        q.Parameters = string.Join(";", question.Parameters);
                    }

                    questions.Add(q);
                    _detailRepository.Insert(q);
                }
            }

            if (questions.Count == 0)
            {
                questions = _detailRepository.GetAll().Include(d => d.ActivityCode).Where(d => d.ActivityCode.Id == input.Id).ToList();
            }

            model.ActivityCodeDetails = questions;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated an Activity Code record");

            return new ActivityCodeDto(model);
        }

        public async Task<List<ActivityCode>> GetAllActivityCodes()
        {
            var results = _manager.GetAll().ToList();
            return results;
        }
    }
}
