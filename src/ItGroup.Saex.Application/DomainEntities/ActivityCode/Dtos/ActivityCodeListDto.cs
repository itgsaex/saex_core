﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ActivityCodeListDto : EntityDto<Guid>
    {
        public ActivityCodeListDto()
        {
        }

        public ActivityCodeListDto(ActivityCode input)
        {
            Id = input.Id;
            Code = input.Code;
            Description = input.Description;
            Type = input.Type;

            if (input.ActivityCodeDetails != null && input.ActivityCodeDetails.Any())
            {
                Questions = input.ActivityCodeDetails.Select(x => new QuestionDto(x)).ToList();
            }

            //if (input.Questions != null && input.Questions.Any())
            //    Questions = input.Questions.Select(x => new QuestionDto(x)).ToList();
        }

        public string Code { get; set; }

        public string Description { get; set; }

        public List<QuestionDto> Questions { get; set; }

        public ActivityCodeType Type { get; set; }
    }
}