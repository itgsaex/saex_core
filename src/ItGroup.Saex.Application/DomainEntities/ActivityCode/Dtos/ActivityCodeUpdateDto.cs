﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(ActivityCode))]
    public class ActivityCodeUpdateDto : EntityDto<Guid>
    {
        public ActivityCodeUpdateDto() { }

        public string Code { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public bool IsGlobal { get; set; }

        public ActivityCodeType Type { get; set; }

        public List<QuestionCreateDto> Questions { get; set; }
    }
}