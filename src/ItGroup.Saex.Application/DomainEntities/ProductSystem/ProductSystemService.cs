﻿using Abp.Application.Services;
using ItGroup.Saex.Manager.DomainEntities.ProductSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItGroup.Saex.DomainEntities.ProductSystem
{
    public class ProductSystemService : IProductSystemService
    {
        protected readonly ProductSystemManager _manager;
        public ProductSystemService(ProductSystemManager manager) : base()
        {
            _manager = manager;
        }

        public List<ItGroup.Saex.DomainEntities.ProductSystem.ProductSystem> GetAllProductSystems()
        {
            var result = _manager.GetAllProductSystems().ToList();
            return result;
        }
    }
}
