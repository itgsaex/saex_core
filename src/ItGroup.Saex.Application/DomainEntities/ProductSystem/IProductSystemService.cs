﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities.ProductSystem
{
    public interface IProductSystemService : IApplicationService
    {
        List<ItGroup.Saex.DomainEntities.ProductSystem.ProductSystem> GetAllProductSystems();
    }
}
