//using Abp.Application.Services.Dto;
//using Abp.Authorization;
//using Abp.Domain.Entities;
//using Abp.UI;
//using AutoMapper;
//using ItGroup.Saex.Authorization;
//using ItGroup.Saex.DomainEntities;
//using Microsoft.EntityFrameworkCore;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace ItGroup.Saex.DomainEntities
//{
//    public class AgentSupervisorAppService : AppServiceBase, IAgentSupervisorAppService
//    {
//        protected new readonly AgentSupervisorManager _manager;

//        public AgentSupervisorAppService(AgentSupervisorManager manager) : base()
//        {
//            _manager = manager;
//        }

//        [AbpAuthorize(PermissionNames.Pages_Agents_Modify)]
//        public async Task<AgentSupervisorDto> Create(AgentSupervisorCreateDto input)
//        {
//            var category = ObjectMapper.Map<SupervisedAgents>(input);
//            var result = await _manager.CreateAsync(category);

//            return new AgentSupervisorDto(result);
//        }

//        [AbpAuthorize(PermissionNames.Pages_Agents_Modify)]
//        public async Task Delete(Guid id)
//        {
//            var category = await _manager.GetAsync(id);
//            if (category == null) throw new UserFriendlyException("Agent Supervisor not found for the provided Id.");

//            await _manager.DeleteAsync(category);
//        }

//        [AbpAuthorize(PermissionNames.Pages_Agents_View)]
//        public async Task<AgentSupervisorDto> Get(Guid id)
//        {
//            var result = await _manager.GetAsync(id);
//            if (result == null) throw new UserFriendlyException("Agent Supervisor not found for the provided Id");

//            return new AgentSupervisorDto(result);
//        }

//        [AbpAuthorize(PermissionNames.Pages_Agents_View)]
//        public async Task<PagedResultDto<AgentSupervisorDto>> GetAll(PagedResultRequestDto paging)
//        {
//            var output = new PagedResultDto<AgentSupervisorDto>();

//            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<SupervisedAgents>;

//            output.TotalCount = results.Count();
//            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new AgentSupervisorDto(x)).ToList();

//            return output;
//        }

//        [AbpAuthorize(PermissionNames.Pages_Agents_View)]
//        public async Task<PagedResultDto<AgentSupervisorDto>> Search(AgentSupervisorSearchModel filters)
//        {
//            var res = new PagedResultDto<AgentSupervisorDto>();

//            var results = await _manager.GetFiltered(filters);

//            res.TotalCount = results.TotalCount;
//            res.Items = results.Items.Select(x => new AgentSupervisorDto(x)).ToList();

//            return res;
//        }

//        [AbpAuthorize(PermissionNames.Pages_Agents_Modify)]
//        public async Task<AgentSupervisorDto> Update(AgentSupervisorUpdateDto input)
//        {
//            if (input.Id == null)
//                throw new EntityNotFoundException("AgentSupervisor id is null.");

//            var ca = await _manager.GetAsync((Guid)input.Id);
//            if (ca == null) throw new EntityNotFoundException("AgentSupervisor does not exist in database.");

//            await _manager.UpdateAsync(ca);

//            return new AgentSupervisorDto(ca);
//        }
//    }
//}
