using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IWorkingHourAppService : IApplicationService
    {
        Task Create(WorkingHourCreateDto input);

        Task Delete(Guid id);

        Task<WorkingHourDto> Get(Guid id);

        Task<PagedResultDto<WorkingHourDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<WorkingHourListDto>> Search(WorkingHourSearchModel filters);

        Task<WorkingHourDto> Update(WorkingHourUpdateDto input);

        Task<List<WorkingHourDto>> GetAllForList();
    }
}