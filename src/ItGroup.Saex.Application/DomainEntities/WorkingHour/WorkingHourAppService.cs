using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class WorkingHourAppService : AppServiceBase, IWorkingHourAppService
    {
        protected readonly WorkingHourManager _manager;

        public WorkingHourAppService(WorkingHourManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_WorkingHours_Modify)]
        public async Task Create(WorkingHourCreateDto input)
        {
            
            var model = new WorkingHour()
            {
                FridayEndTime = DateTime.Parse(input.FridayEndTime, CultureInfo.InvariantCulture).TimeOfDay,
                FridayStartTime = DateTime.Parse(input.FridayStartTime, CultureInfo.InvariantCulture).TimeOfDay,
                MondayEndTime = DateTime.Parse(input.MondayEndTime, CultureInfo.InvariantCulture).TimeOfDay,
                MondayStartTime = DateTime.Parse(input.MondayStartTime, CultureInfo.InvariantCulture).TimeOfDay,
                Name = input.Name,
                SaturdayEndTime = DateTime.Parse(input.SaturdayEndTime, CultureInfo.InvariantCulture).TimeOfDay,
                SaturdayStartTime = DateTime.Parse(input.SaturdayStartTime, CultureInfo.InvariantCulture).TimeOfDay,
                SundayEndTime = DateTime.Parse(input.SundayEndTime, CultureInfo.InvariantCulture).TimeOfDay,
                SundayStartTime = DateTime.Parse(input.SundayStartTime, CultureInfo.InvariantCulture).TimeOfDay,
                ThursdayEndTime = DateTime.Parse(input.ThursdayEndTime, CultureInfo.InvariantCulture).TimeOfDay,
                ThursdayStartTime = DateTime.Parse(input.ThursdayStartTime, CultureInfo.InvariantCulture).TimeOfDay,
                TuesdayEndTime = DateTime.Parse(input.TuesdayEndTime, CultureInfo.InvariantCulture).TimeOfDay,
                TuesdayStartTime = DateTime.Parse(input.TuesdayStartTime, CultureInfo.InvariantCulture).TimeOfDay,
                WednesdayEndTime = DateTime.Parse(input.WednesdayEndTime, CultureInfo.InvariantCulture).TimeOfDay,
                WednesdayStartTime = DateTime.Parse(input.WednesdayStartTime, CultureInfo.InvariantCulture).TimeOfDay
            };

            await _manager.CreateAsync(model);
            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Working Hours record");
        }

        [AbpAuthorize(PermissionNames.Pages_WorkingHours_Modify)]
        public async Task Delete(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(result, null, AuditLogOperationType.Delete, "Deleted a Working Hours record");

            await _manager.DeleteAsync(result);
        }

        [AbpAuthorize(PermissionNames.Pages_WorkingHours_View)]
        public async Task<WorkingHourDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException(L("RecordNotFoundError"));

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Working Hours record");

            return new WorkingHourDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_WorkingHours_View)]
        public async Task<PagedResultDto<WorkingHourDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<WorkingHourDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<WorkingHour>;

            output.TotalCount = results.Count();
            output.Items = results.Select(x => new WorkingHourDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_WorkingHours_View)]
        public async Task<PagedResultDto<WorkingHourListDto>> Search(WorkingHourSearchModel filters)
        {
            var res = new PagedResultDto<WorkingHourListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new WorkingHourListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_WorkingHours_Modify)]
        public async Task<WorkingHourDto> Update(WorkingHourUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException(L("IdIsNullError"));

            var model = await _manager.GetAsync(input.Id);
            var oldModel = model.Clone() as WorkingHour;
            if (model == null) throw new EntityNotFoundException(L("RecordNotFoundError"));

            model.FridayEndTime = DateTime.Parse(input.FridayEndTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.FridayStartTime = DateTime.Parse(input.FridayStartTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.MondayEndTime = DateTime.Parse(input.MondayEndTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.MondayStartTime = DateTime.Parse(input.MondayStartTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.Name = input.Name;
            model.SaturdayEndTime = DateTime.Parse(input.SaturdayEndTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.SaturdayStartTime = DateTime.Parse(input.SaturdayStartTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.SundayEndTime = DateTime.Parse(input.SundayEndTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.SundayStartTime = DateTime.Parse(input.SundayStartTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.ThursdayEndTime = DateTime.Parse(input.ThursdayEndTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.ThursdayStartTime = DateTime.Parse(input.ThursdayStartTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.TuesdayEndTime = DateTime.Parse(input.TuesdayEndTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.TuesdayStartTime = DateTime.Parse(input.TuesdayStartTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.WednesdayEndTime = DateTime.Parse(input.WednesdayEndTime, CultureInfo.InvariantCulture).TimeOfDay;
            model.WednesdayStartTime = DateTime.Parse(input.WednesdayStartTime, CultureInfo.InvariantCulture).TimeOfDay;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Working Hours record");

            return new WorkingHourDto(model);
        }

        public async Task<List<WorkingHourDto>> GetAllForList()
        {
            var results = await _manager.GetAll().Select(l => new WorkingHourDto(l)).ToListAsync();
            return results;
        }
    }
}
