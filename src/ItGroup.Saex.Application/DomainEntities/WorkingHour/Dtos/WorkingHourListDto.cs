﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class WorkingHourListDto : EntityDto<Guid>
    {
        public WorkingHourListDto()
        {
        }

        public WorkingHourListDto(WorkingHour input)
        {
            Id = input.Id;
            FridayEndTime = input.FridayEndTime;
            FridayStartTime = input.FridayStartTime;
            MondayEndTime = input.MondayEndTime;
            MondayStartTime = input.MondayStartTime;
            Name = input.Name;
            SaturdayEndTime = input.SaturdayEndTime;
            SaturdayStartTime = input.SaturdayStartTime;
            SundayEndTime = input.SundayEndTime;
            SundayStartTime = input.SundayStartTime;
            ThursdayEndTime = input.ThursdayEndTime;
            ThursdayStartTime = input.ThursdayStartTime;
            TuesdayEndTime = input.TuesdayEndTime;
            TuesdayStartTime = input.TuesdayStartTime;
            WednesdayEndTime = input.WednesdayEndTime;
            WednesdayStartTime = input.WednesdayStartTime;
        }

        public TimeSpan FridayEndTime { get; set; }

        public TimeSpan FridayStartTime { get; set; }

        public TimeSpan MondayEndTime { get; set; }

        public TimeSpan MondayStartTime { get; set; }

        public string Name { get; set; }

        public TimeSpan SaturdayEndTime { get; set; }

        public TimeSpan SaturdayStartTime { get; set; }

        public TimeSpan SundayEndTime { get; set; }

        public TimeSpan SundayStartTime { get; set; }

        public TimeSpan ThursdayEndTime { get; set; }

        public TimeSpan ThursdayStartTime { get; set; }

        public TimeSpan TuesdayEndTime { get; set; }

        public TimeSpan TuesdayStartTime { get; set; }

        public TimeSpan WednesdayEndTime { get; set; }

        public TimeSpan WednesdayStartTime { get; set; }
    }
}