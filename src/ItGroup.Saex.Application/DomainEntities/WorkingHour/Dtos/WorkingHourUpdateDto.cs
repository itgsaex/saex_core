﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;
using System.Globalization;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(WorkingHour))]
    public class WorkingHourUpdateDto : EntityDto<Guid>
    {
        public WorkingHourUpdateDto()
        {
        }

        public WorkingHourUpdateDto(WorkingHour input)
        {
            Id = input.Id;
            FridayEndTime = DateTime.Today.Add(input.FridayEndTime).ToString("hh:mm tt");
            FridayStartTime = DateTime.Today.Add(input.FridayStartTime).ToString("hh:mm tt");
            MondayEndTime = DateTime.Today.Add(input.MondayEndTime).ToString("hh:mm tt");
            MondayStartTime = DateTime.Today.Add(input.MondayStartTime).ToString("hh:mm tt");
            Name = input.Name;
            SaturdayEndTime = DateTime.Today.Add(input.SaturdayEndTime).ToString("hh:mm tt");
            SaturdayStartTime = DateTime.Today.Add(input.SaturdayStartTime).ToString("hh:mm tt");
            SundayEndTime = DateTime.Today.Add(input.SundayEndTime).ToString("hh:mm tt");
            SundayStartTime = DateTime.Today.Add(input.SundayStartTime).ToString("hh:mm tt");
            ThursdayEndTime = DateTime.Today.Add(input.ThursdayEndTime).ToString("hh:mm tt");
            ThursdayStartTime = DateTime.Today.Add(input.ThursdayStartTime).ToString("hh:mm tt");
            TuesdayEndTime = DateTime.Today.Add(input.TuesdayEndTime).ToString("hh:mm tt");
            TuesdayStartTime = DateTime.Today.Add(input.TuesdayStartTime).ToString("hh:mm tt");
            WednesdayEndTime = DateTime.Today.Add(input.WednesdayEndTime).ToString("hh:mm tt");
            WednesdayStartTime = DateTime.Today.Add(input.WednesdayStartTime).ToString("hh:mm tt");
        }

        public string FridayEndTime { get; set; }

        public string FridayStartTime { get; set; }

        public string MondayEndTime { get; set; }

        public string MondayStartTime { get; set; }

        public string Name { get; set; }

        public string SaturdayEndTime { get; set; }

        public string SaturdayStartTime { get; set; }

        public string SundayEndTime { get; set; }

        public string SundayStartTime { get; set; }

        public string ThursdayEndTime { get; set; }

        public string ThursdayStartTime { get; set; }

        public string TuesdayEndTime { get; set; }

        public string TuesdayStartTime { get; set; }

        public string WednesdayEndTime { get; set; }

        public string WednesdayStartTime { get; set; }
    }
}