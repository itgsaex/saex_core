﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(WorkingHour))]
    public class WorkingHourCreateDto : EntityDto<Guid>
    {
        public WorkingHourCreateDto()
        {
        }

        public string FridayEndTime { get; set; }

        public string FridayStartTime { get; set; }

        public string MondayEndTime { get; set; }

        public string MondayStartTime { get; set; }

        public string Name { get; set; }

        public string SaturdayEndTime { get; set; }

        public string SaturdayStartTime { get; set; }

        public string SundayEndTime { get; set; }

        public string SundayStartTime { get; set; }

        public string ThursdayEndTime { get; set; }

        public string ThursdayStartTime { get; set; }

        public string TuesdayEndTime { get; set; }

        public string TuesdayStartTime { get; set; }

        public string WednesdayEndTime { get; set; }

        public string WednesdayStartTime { get; set; }
    }
}
