using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using CsvHelper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Service.DataImporter.FileImport;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ImportAppService : AppServiceBase, IImportAppService
    {
        protected readonly CaseTypeManager _caseTypeManager;

        protected readonly DelinquencyStateManager _delinquencyStateManager;

        protected readonly MacroStateManager _macroStateManager;

        protected readonly CaseManager _manager;

        protected readonly PostalCodeManager _postalCodemanager;

        protected readonly ProductManager _productManager;

        protected readonly SaexV1Importer _saexImporter;

        protected readonly SystemStateManager _systemStateManager;

        protected readonly SystemSubStateManager _systemSubStateManager;

        protected readonly CaseImportManager _caseImportManager;

        public ImportAppService(
            CaseManager manager,
            CaseTypeManager caseTypeManager,
            PostalCodeManager postalCodemanager,
            DelinquencyStateManager delinquencyStateManager,
            ProductManager productManager,
            SystemStateManager systemStateManager,
            SystemSubStateManager systemSubStateManager,
            MacroStateManager macroStateManager,
            SaexV1Importer saexImporter,
            AuditLogManager auditManager,
            CaseImportManager caseImportManager
            ) : base()
        {
            _manager = manager;
            _postalCodemanager = postalCodemanager;
            _delinquencyStateManager = delinquencyStateManager;
            _productManager = productManager;
            _systemStateManager = systemStateManager;
            _systemSubStateManager = systemSubStateManager;
            _caseTypeManager = caseTypeManager;
            _macroStateManager = macroStateManager;
            _saexImporter = saexImporter;
            _auditManager = auditManager;
            _caseImportManager = caseImportManager;
        }

        public async Task<List<ImportResponseDto>> CaseImports(CaseImportDto input)
        {
            var response = new List<ImportResponseDto>();
            var now = DateTime.Now;

            foreach (var @case in input.Cases)
            {
                @case.CreationTime = now;

                var res = await _saexImporter.ImportCase(@case.ConvertToCaseBaseline(), input.ImportType);

                //var log = new CaseImportOutputDto(@case.id, res);

                //response.Add(log);
            }

            //await LogTransaction(null, input, AuditLogOperationType.Create, "Imported a case file");

            return response;
        }

        [AbpAuthorize(PermissionNames.Pages_Import)]
        public async Task<List<CaseImportOutputDto>> Cases(CaseImportDto input)
        {
            var response = new List<CaseImportOutputDto>();
            var now = DateTime.Now;

            foreach (var @case in input.Cases)
            {
                @case.CreationTime = now;

                var baseline = @case.ConvertToCaseBaseline();
                var res = await _saexImporter.ImportCase(baseline, input.ImportType);
                
                var log = new CaseImportOutputDto(baseline.Id.ToString(), res);
                
                response.Add(log);
            }

            //await LogTransaction(null, input, AuditLogOperationType.Create, "Imported a case file");

            return response;
        }

        [AbpAuthorize(PermissionNames.Pages_Import)]
        public Task<PagedResultDto<CaseBaseline>> CaseBaselines()
        {
            var cases = _caseImportManager.GetFiltered(null);
            return cases;
        }

        [AbpAuthorize(PermissionNames.Pages_Import)]
        public async Task<List<ImportResponseDto>> CaseTypes(CaseTypeImportDto input)
        {
            var response = new List<ImportResponseDto>();

            foreach (var type in input.CaseTypes)
            {
                //var res = await _saexImporter.ImportCaseTypes(type);

                //var log = new ImportResponseDto(type.TipoCasoID.ToString(), res);

                //response.Add(log);
            }

            await LogTransaction(null, input, AuditLogOperationType.Create, "Imported a Case Type file");

            return response;
        }

        [AbpAuthorize(PermissionNames.Pages_Import)]
        public async Task<List<ImportResponseDto>> MacroStates(MacroStateImportDto input)
        {
            var response = new List<ImportResponseDto>();

            foreach (var type in input.MacroStates)
            {
                //var res = await _saexImporter.ImportMacroStates(type);

                //var log = new ImportResponseDto(type.MacroEstadoID.ToString(), res);

                //response.Add(log);
            }

            await LogTransaction(null, input, AuditLogOperationType.Create, "Imported a Macro States file");

            return response;
        }

        [AbpAuthorize(PermissionNames.Pages_Import)]
        public async Task<List<ImportResponseDto>> PostalCodes(PostalCodeImportDto input)
        {
            var response = new List<ImportResponseDto>();

            foreach (var pc in input.PostalCodes)
            {
                //var res = await _saexImporter.ImportPostalCodes(pc);

                //var log = new ImportResponseDto(pc.codigoPostal.ToString(), res);

                //response.Add(log);
            }

            await LogTransaction(null, input, AuditLogOperationType.Create, "Imported a Postal Code file");

            return response;
        }

        [AbpAuthorize(PermissionNames.Pages_Import)]
        public async Task<List<ImportResponseDto>> SystemStates(SystemStateImportDto input)
        {
            var response = new List<ImportResponseDto>();

            foreach (var type in input.SystemStates)
            {
                //var res = await _saexImporter.ImportSystemStates(type);

                //var log = new ImportResponseDto(type.EstadoSistema.ToString(), res);

                //response.Add(log);
            }

            await LogTransaction(null, input, AuditLogOperationType.Create, "Imported a System State file");

            return response;
        }

        [AbpAuthorize(PermissionNames.Pages_Import)]
        public async Task<List<ImportResponseDto>> SystemSubStates(SystemSubStateImportDto input)
        {
            var response = new List<ImportResponseDto>();

            foreach (var type in input.SystemSubStates)
            {
                //var res = await _saexImporter.ImportSystemSubStates(type);

                //var log = new ImportResponseDto(type.SubStatusID.ToString(), res);

                //response.Add(log);
            }

            await LogTransaction(null, input, AuditLogOperationType.Create, "Imported a System Sub States file");

            return response;
        }
    }
}
