﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseBaselineDto : EntityDto<Guid>
    {
        public CaseBaselineDto(List<CaseBaseline> caseImports)
        {
            CaseImports = caseImports;
        }

        public List<CaseBaseline> CaseImports { get; set; }
    }
}
