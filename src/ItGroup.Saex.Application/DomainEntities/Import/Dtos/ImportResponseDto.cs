﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    public class ImportResponseDto
    {
        public ImportResponseDto()
        {
        }

        public ImportResponseDto(string recordId, ImportResponse response)
        {
            RecordId = recordId;
            Response = response;
        }

        public string RecordId { get; set; }

        public ImportResponse Response { get; set; }
    }
}
