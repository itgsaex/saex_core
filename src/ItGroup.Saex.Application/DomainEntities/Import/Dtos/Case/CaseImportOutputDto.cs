﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseImportOutputDto : ImportResponseDto
    {
        public CaseImportOutputDto()
        {
        }

        public CaseImportOutputDto(string caseNumber, ImportResponse response)
        {
            CaseNumber = caseNumber;
            Response = response;
        }

        public string CaseNumber { get; set; }
    }
}
