﻿//using Abp.Application.Services.Dto;
//using Abp.AutoMapper;
//using ItGroup.Saex.DomainEntities;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;

//namespace ItGroup.Saex.DomainEntities
//{
//    public class CaseMappingInput
//    {
//        public CaseMappingInput()
//        {
//        }

//        public CaseMappingInput(CaseImportDto input)
//        {
//            AccountNumber = input.NumeroCuenta;
//            AccountType = input.TipoCuenta;
//            // * Agents = input.agents;
//            BranchId = input.Sucursal;
//            // * CaseActivities = input.caseActivities;
//            // * CaseEvents = input.caseEvents;
//            CaseNumber = input.CasoID;
//            // * CaseType = input.CaseType;
//            Charges = input.Cargos;
//            CollectorId = input.Cobrador;
//            Condition = input.Condicion;
//            ContactPerson = input.PersonaContacto;
//            // * Customers = input.customers;
//            // * DelinquencyState = input.EtapasDeDelicuencia;
//            DueDate = input.FechaVencimiento;
//            // * ExtraFieldsJson = input.extraFieldsJson;
//            ImportedDate = input.FechaEntradaSAEx;
//            InterestRate = input.Interes;
//            LastPaymentDate = input.FechaUltPago;
//            MaturityDate = input.MaturityDate;
//            OriginalAmount = input.MontoOriginal;
//            OriginDate = input.FechaOrigen;
//            OwedAmount = input.MontoVencido;
//            PaidAmount = input.MontoPago;
//            // * Payments = input.payments;
//            // * Product = input.product;
//            // * Route = input.route;
//            StateCode = input.StateCode;
//            Status = input.Estatus;
//            // * SystemState = input.systemState;
//            // * Tactic = input.tactic;
//            Terms = input.Terminos;
//        }

//        public string AccountNumber { get; set; }

//        public string AccountType { get; set; }

//        public List<AgentDto> Agents { get; set; }

//        public string BranchId { get; set; }

//        public List<CaseActivityDto> CaseActivities { get; set; }

//        public List<CaseEventDto> CaseEvents { get; set; }

//        public string CaseNumber { get; set; }

//        public CaseType CaseType { get; set; }

//        public double Charges { get; set; }

//        public string CollectorId { get; set; }

//        public string Condition { get; set; }

//        public string ContactPerson { get; set; }

//        public List<Customer> Customers { get; set; }

//        public CaseDelinquencyState DelinquencyState { get; set; }

//        public DateTime? DueDate { get; set; }

//        public string ExtraFieldsJson { get; set; }

//        public DateTime? ImportedDate { get; set; }

//        public double InterestRate { get; set; }

//        public DateTime? LastPaymentDate { get; set; }

//        public DateTime? MaturityDate { get; set; }

//        public double OriginalAmount { get; set; }

//        public DateTime? OriginDate { get; set; }

//        public double OwedAmount { get; set; }

//        public double PaidAmount { get; set; }

//        public List<CasePayment> Payments { get; set; }

//        public CaseProduct Product { get; set; }

//        public Route Route { get; set; }

//        public string StateCode { get; set; }

//        public string Status { get; set; }

//        public CaseSystemState SystemState { get; set; }

//        public CaseTactic Tactic { get; set; }

//        public string Terms { get; set; }
//    }
//}
