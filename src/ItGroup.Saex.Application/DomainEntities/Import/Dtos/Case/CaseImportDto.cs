﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(CaseBaseline))]
    public class CaseImportDto : EntityDto<Guid>
    {
        public CaseImportDto()
        {
        }

        public CaseImportDto(List<CaseImport> cases, ImportType importType)
        {
            Cases = cases;
            ImportType = importType;
        }

        public List<CaseImport> Cases { get; set; }

        public ImportType ImportType { get; set; }
    }
}
