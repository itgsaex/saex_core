﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemStateImportDto : EntityDto<Guid>
    {
        public SystemStateImportDto(List<SystemStateImport> systemStates)
        {
            SystemStates = systemStates;
        }

        public List<SystemStateImport> SystemStates { get; set; }
    }
}
