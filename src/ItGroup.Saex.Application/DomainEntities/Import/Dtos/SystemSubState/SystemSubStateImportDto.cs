﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemSubStateImportDto : EntityDto<Guid>
    {
        public SystemSubStateImportDto(List<SystemSubStateImport> systemSubStates)
        {
            SystemSubStates = systemSubStates;
        }

        public List<SystemSubStateImport> SystemSubStates { get; set; }
    }
}
