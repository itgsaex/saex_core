﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTypeImportDto : EntityDto<Guid>
    {
        public CaseTypeImportDto(List<CaseTypeImport> caseTypes)
        {
            CaseTypes = caseTypes;
        }

        public List<CaseTypeImport> CaseTypes { get; set; }
    }
}
