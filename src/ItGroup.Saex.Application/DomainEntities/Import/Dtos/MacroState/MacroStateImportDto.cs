﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    public class MacroStateImportDto : EntityDto<Guid>
    {
        public MacroStateImportDto(List<MacroStateImport> macroStates)
        {
            MacroStates = macroStates;
        }

        public List<MacroStateImport> MacroStates { get; set; }
    }
}
