﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.DomainEntities
{
    public class PostalCodeImportDto : EntityDto<Guid>
    {
        public PostalCodeImportDto(List<PostalCodeImport> postalCodes)
        {
            PostalCodes = postalCodes;
        }

        public List<PostalCodeImport> PostalCodes { get; set; }
    }
}
