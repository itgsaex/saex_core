using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IImportAppService : IApplicationService
    {
        Task<List<CaseImportOutputDto>> Cases(CaseImportDto input);

        Task<List<ImportResponseDto>> CaseTypes(CaseTypeImportDto input);

        Task<List<ImportResponseDto>> MacroStates(MacroStateImportDto input);

        Task<List<ImportResponseDto>> PostalCodes(PostalCodeImportDto input);

        Task<List<ImportResponseDto>> SystemStates(SystemStateImportDto input);

        Task<List<ImportResponseDto>> CaseImports(CaseImportDto input);

        Task<List<ImportResponseDto>> SystemSubStates(SystemSubStateImportDto input);

        Task<PagedResultDto<CaseBaseline>> CaseBaselines();
    }
}
