﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class AuditLogListDto : FullAuditedEntityDto<Guid>
    {
        public AuditLogListDto()
        {
        }

        public AuditLogListDto(AuditLog input)
        {
            Id = input.Id;
            TableName = input.TableName;
            Description = input.Description;
            RecordId = input.RecordId;
            PreviousValue = input.PreviousValue;
            NewValue = input.NewValue;
            OperationType = input.OperationType;
            CreatorUsername = input.CreatorUsername;
            CreationTime = input.CreationTime;
        }

        public string CreatorUsername { get; set; }

        public string Description { get; set; }

        public string NewValue { get; set; }

        public string OperationType { get; set; }

        public string PreviousValue { get; set; }

        public string RecordId { get; set; }

        public string TableName { get; set; }
    }
}
