﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(AuditLog))]
    public class AuditLogCreateDto
    {
        public AuditLogCreateDto()
        {
        }

        public string CreatorUsername { get; set; }

        public string Description { get; set; }

        public string NewValue { get; set; }

        public string OperationType { get; set; }

        public string PreviousValue { get; set; }

        public string RecordId { get; set; }

        public string TableName { get; set; }
    }
}
