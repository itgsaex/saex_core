using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<AuditLogDto> Create(AuditLogCreateDto input);

        Task<AuditLogDto> Get(Guid id);

        Task<PagedResultDto<AuditLogDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<AuditLogListDto>> Search(AuditLogSearchModel filters);

        Task<List<InterfacelogModel>> GetLastRunInterfaceLog();

        Task<List<ErrorlogModel>> GetErrorLog();

        Task<int> ExecuteCaseProcessing();
    }
}