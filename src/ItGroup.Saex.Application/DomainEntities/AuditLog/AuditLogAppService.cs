using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public class AuditLogAppService : AppServiceBase, IAuditLogAppService
    {
        protected readonly AuditLogManager _manager;
        private readonly ICaseRecentActivityRepository _repositoryCaseRecentActivity;

        public AuditLogAppService(AuditLogManager manager, ICaseRecentActivityRepository repositoryCaseRecentActivity) : base()
        {
            _manager = manager;
            _repositoryCaseRecentActivity = repositoryCaseRecentActivity;
        }

        [AbpAuthorize(PermissionNames.Pages_AuditLogs_View)]
        public async Task<AuditLogDto> Create(AuditLogCreateDto input)
        {
            var log = ObjectMapper.Map<AuditLog>(input);
            log.CreatorUserId = AbpSession.UserId;
            log.CreationTime = DateTime.UtcNow;

            var result = await _manager.CreateAsync(log);

            return new AuditLogDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_AuditLogs_View)]
        public async Task<AuditLogDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("AuditLog not found for the provided Id");

            return new AuditLogDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_AuditLogs_View)]
        public async Task<PagedResultDto<AuditLogDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<AuditLogDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<AuditLog>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new AuditLogDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_AuditLogs_View)]
        public async Task<PagedResultDto<AuditLogListDto>> Search(AuditLogSearchModel filters)
        {
            var res = new PagedResultDto<AuditLogListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new AuditLogListDto(x)).ToList();

            return res;
        }

        public async Task<List<InterfacelogModel>> GetLastRunInterfaceLog()
        {
            var results = await _repositoryCaseRecentActivity.GetLastRunInterfaceLog();

            return results;
        }

        public async Task<List<ErrorlogModel>> GetErrorLog()
        {
            var results = await _repositoryCaseRecentActivity.GetErrorLog();

            return results;
        }

        [UnitOfWork(isTransactional: false)]
        public async Task<int> ExecuteCaseProcessing()
        {
            var result = await _repositoryCaseRecentActivity.ExecuteCaseProcessing();
            return result;
        }
    }
}
