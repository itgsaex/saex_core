﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentActivityDto : EntityDto<Guid>
    {
        public AgentActivityDto()
        {
        }

        public AgentActivityDto(List<AgentActivity> activities)
        {
            Agent = activities.Select(x => new AgentDto(x.Agent)).FirstOrDefault();
            Details = activities.Select(x => new AgentActivityDetailsDto(x)).ToList();
        }

        public AgentActivityDto(Agent agent, List<AgentActivity> activities)
        {
            Agent = new AgentDto(agent);
            Details = activities.Select(x => new AgentActivityDetailsDto(x)).ToList();
        }

        public virtual AgentDto Agent { get; set; }

        public virtual List<AgentActivityDetailsDto> Details { get; set; }
    }
}
