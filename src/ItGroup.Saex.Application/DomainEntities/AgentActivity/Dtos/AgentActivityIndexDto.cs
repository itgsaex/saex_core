﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentActivityIndexDto : EntityDto<Guid>
    {
        public AgentActivityIndexDto()
        {
        }

        public AgentActivityIndexDto(List<Agent> agents, List<AgentActivity> activities)
        {
            Agents = agents.Select(x => new AgentListDto(x)).ToList();

            if (activities != null && activities.Any())
            {
                var agent = activities.Select(x => x.Agent).FirstOrDefault();
                Activity = new AgentActivityDto(agent, activities);
            }
        }

        public AgentActivityDto Activity { get; set; }

        public List<AgentListDto> Agents { get; set; }
    }
}
