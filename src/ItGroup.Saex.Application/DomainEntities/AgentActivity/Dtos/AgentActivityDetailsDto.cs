﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentActivityDetailsDto : EntityDto<Guid>
    {
        public AgentActivityDetailsDto()
        {
        }

        public AgentActivityDetailsDto(AgentActivity input)
        {
            Latitude = input.Latitude;
            Longitude = input.Longitude;
            TransStatus = input.TransStatus;

            if (input.CaseActivity != null)
                CaseActivity = new CaseActivityDto(input.CaseActivity);
        }

        public virtual CaseActivityDto CaseActivity { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }

        public int TransStatus { get; set; }
    }
}
