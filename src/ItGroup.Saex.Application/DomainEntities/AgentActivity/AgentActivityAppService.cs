using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Manager.DomainEntities;
using ItGroup.Saex.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentActivityAppService : AppServiceBase, IAgentActivityAppService
    {
        protected readonly AgentActivityManager _manager;
        protected readonly AgentGPSManager _agentGPSManager;

        public AgentActivityAppService(AgentActivityManager manager, AgentGPSManager agentGPSManager) : base()
        {
            _manager = manager;
            _agentGPSManager = agentGPSManager;
        }

        [AbpAuthorize(PermissionNames.Pages_AgentActivity_View)]
        public async Task<List<AgentActivityModel>> GetByAgentId(Guid id, DateTime? date = null)
        {
            var model = new AgentActivitySearchModel(agentId: id, date: date);
            var result = await _manager.GetFiltered(model);
            var agentGpsResult = await _agentGPSManager.GetFiltered(model);
            result.AddRange(agentGpsResult);
            
            return result.OrderBy(r => r.Date).ToList();
        }


    }
}
