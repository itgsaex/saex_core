using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ItGroup.Saex.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAgentActivityAppService : IApplicationService
    {
        Task<List<AgentActivityModel>> GetByAgentId(Guid id, DateTime? date = null);
    }
}