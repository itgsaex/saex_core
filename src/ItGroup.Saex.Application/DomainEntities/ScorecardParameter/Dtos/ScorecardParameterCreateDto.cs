﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(ScorecardParameter))]
    public class ScorecardParameterCreateDto : EntityDto<Guid>
    {
        public ScorecardParameterCreateDto()
        {
        }

        public string Description { get; set; }

        public string PaymentLevel { get; set; }

        public string StandardValue { get; set; }
    }
}
