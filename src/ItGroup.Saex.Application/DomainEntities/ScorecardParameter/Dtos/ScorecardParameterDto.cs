﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class ScorecardParameterDto : EntityDto<Guid>
    {
        public ScorecardParameterDto()
        {
        }

        public ScorecardParameterDto(ScorecardParameter input)
        {
            Id = input.Id;
            Description = input.Description;
            StandardValue = input.StandardValue;
            PaymentLevel = input.PaymentLevel;
        }

        public string Description { get; set; }

        public string PaymentLevel { get; set; }

        public string StandardValue { get; set; }
    }
}