﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(ScorecardParameter))]
    public class ScorecardParameterUpdateDto : EntityDto<Guid>
    {
        public ScorecardParameterUpdateDto()
        {
        }

        public string Description { get; set; }

        public string PaymentLevel { get; set; }

        public string StandardValue { get; set; }
    }
}