using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ScorecardParameterAppService : AppServiceBase, IScorecardParameterAppService
    {
        protected new readonly ScorecardParameterManager _manager;

        public ScorecardParameterAppService(ScorecardParameterManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_ScorecardParameters_Modify)]
        public async Task<ScorecardParameterDto> Create(ScorecardParameterCreateDto input)
        {
            var model = ObjectMapper.Map<ScorecardParameter>(input);
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Scorecard Parameter");

            return new ScorecardParameterDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ScorecardParameters_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Scorecard Parameter not found for the provided Id.");

            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Scorecard Parameter");

            await _manager.DeleteAsync(model);
        }

        [AbpAuthorize(PermissionNames.Pages_ScorecardParameters_View)]
        public async Task<ScorecardParameterDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("Scorecard Parameter not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Scorecard Parameter");

            return new ScorecardParameterDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ScorecardParameters_View)]
        public async Task<PagedResultDto<ScorecardParameterDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ScorecardParameterDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<ScorecardParameter>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ScorecardParameterDto(x)).ToList();

            return output;
        }

        [AbpAuthorize(PermissionNames.Pages_ScorecardParameters_View)]
        public async Task<PagedResultDto<ScorecardParameterListDto>> Search(ScorecardParameterSearchModel filters)
        {
            var res = new PagedResultDto<ScorecardParameterListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ScorecardParameterListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_ScorecardParameters_Modify)]
        public async Task<ScorecardParameterDto> Update(ScorecardParameterUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("ScorecardParameter id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as ScorecardParameter;
            if (model == null) throw new EntityNotFoundException("ScorecardParameter does not exist in database.");

            model.Description = input.Description;
            model.PaymentLevel = input.PaymentLevel;
            model.StandardValue = input.StandardValue;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Scorecard Parameter");

            return new ScorecardParameterDto(model);
        }
    }
}
