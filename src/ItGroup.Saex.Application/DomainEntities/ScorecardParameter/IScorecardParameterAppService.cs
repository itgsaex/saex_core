using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IScorecardParameterAppService : IApplicationService
    {
        Task<ScorecardParameterDto> Create(ScorecardParameterCreateDto input);

        Task Delete(Guid id);

        Task<ScorecardParameterDto> Get(Guid id);

        Task<PagedResultDto<ScorecardParameterDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<ScorecardParameterListDto>> Search(ScorecardParameterSearchModel filters);

        Task<ScorecardParameterDto> Update(ScorecardParameterUpdateDto input);
    }
}