using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISystemPolicyAppService : IApplicationService
    {
        Task<SystemPolicyDto> Create(SystemPolicyCreateDto input);

        Task Delete(Guid id);

        Task<SystemPolicyDto> Get(Guid id);

        Task<PagedResultDto<SystemPolicyDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<SystemPolicyListDto>> Search(SystemPolicySearchModel filters);

        Task<SystemPolicyDto> Update(SystemPolicyUpdateDto input);

        Task<List<SystemPolicy>> GetAllSystemPolicies();
    }
}