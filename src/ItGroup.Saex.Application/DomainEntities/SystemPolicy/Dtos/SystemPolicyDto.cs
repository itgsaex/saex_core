﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemPolicyDto : EntityDto<Guid>
    {
        public SystemPolicyDto()
        {
        }

        public SystemPolicyDto(SystemPolicy input)
        {
            Id = input.Id;
            Code = input.Code;
            Description = input.Description;
            Value = input.Value;
        }

        public string Code { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }
    }
}