﻿using Abp.Application.Services;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISortCaseService : IApplicationService
    {
        List<SortCase> GetAllSortCases();
    }
}
