﻿using ItGroup.Saex.Manager.DomainEntities;
using System.Collections.Generic;
using System.Linq;

namespace ItGroup.Saex.DomainEntities
{
    public class SortCaseService : ISortCaseService
    {
        protected new readonly SortCaseManager _manager;
        public SortCaseService(SortCaseManager manager) : base()
        {
            _manager = manager;
        }

        public List<SortCase> GetAllSortCases()
        {
            var result = _manager.GetAllSortCases().ToList();
            return result;
        }
    }
}
