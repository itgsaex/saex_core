using Abp.Application.Services;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISessionLogAppService : IApplicationService
    {
        Task SyncSessionLogModel(SessionLog input);
    }
}

