using System.Threading.Tasks;
using ItGroup.Saex.Manager.DomainEntities;

namespace ItGroup.Saex.DomainEntities
{
    public class SessionLogAppService : AppServiceBase, ISessionLogAppService
    {
        protected new readonly SessionLogManager _manager;

        public SessionLogAppService(SessionLogManager manager) : base()
        {
            _manager = manager;
        }

        public async Task SyncSessionLogModel(SessionLog input)
        {
            await _manager.PostSessionLog(input);
        }
    }
}
