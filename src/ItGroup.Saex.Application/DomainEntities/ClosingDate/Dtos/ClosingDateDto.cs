﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ClosingDateDto : EntityDto<Guid>
    {
        public ClosingDateDto()
        {
        }

        public ClosingDateDto(ClosingDate input)
        {
            Id = input.Id;
            StartDate = input.StartDate;
            EndDate = input.EndDate;
            IsAllDay = input.IsAllDay;
            Type = input.Type;
        }

        public DateTime EndDate { get; set; }

        public bool IsAllDay { get; set; }

        public DateTime StartDate { get; set; }

        public ClosingDateType Type { get; set; }
    }
}