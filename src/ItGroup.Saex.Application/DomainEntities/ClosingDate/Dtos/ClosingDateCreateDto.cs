﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    [AutoMapTo(typeof(ClosingDate))]
    public class ClosingDateCreateDto : EntityDto<Guid>
    {
        public ClosingDateCreateDto()
        {
        }

        public DateTime EndDate { get; set; }

        public bool IsAllDay { get; set; }

        public DateTime StartDate { get; set; }

        public ClosingDateType Type { get; set; }
    }
}
