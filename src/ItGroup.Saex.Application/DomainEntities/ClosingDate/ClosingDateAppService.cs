using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.UI;
using AutoMapper;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ClosingDateAppService : AppServiceBase, IClosingDateAppService
    {
        protected new readonly ClosingDateManager _manager;

        public ClosingDateAppService(ClosingDateManager manager, AuditLogManager auditManager) : base()
        {
            _manager = manager;
            _auditManager = auditManager;
        }

        [AbpAuthorize(PermissionNames.Pages_ClosingDates_Modify)]
        public async Task<ClosingDateDto> Create(ClosingDateCreateDto input)
        {
            var model = ObjectMapper.Map<ClosingDate>(input);
            model.IsAllDay = true;
            var result = await _manager.CreateAsync(model);

            await LogTransaction(model, input, AuditLogOperationType.Create, "Created a Closing Date record");

            return new ClosingDateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ClosingDates_Modify)]
        public async Task Delete(Guid id)
        {
            var model = await _manager.GetAsync(id);
            if (model == null) throw new UserFriendlyException("Closing Date not found for the provided Id.");

            await _manager.DeleteAsync(model);
            await LogTransaction(model, null, AuditLogOperationType.Delete, "Deleted a Closing Date record");
        }

        [AbpAuthorize(PermissionNames.Pages_ClosingDates_View)]
        public async Task<ClosingDateDto> Get(Guid id)
        {
            var result = await _manager.GetAsync(id);
            if (result == null) throw new UserFriendlyException("ClosingDate not found for the provided Id");

            await LogTransaction(result, null, AuditLogOperationType.View, "Viewed a Closing Date record");

            return new ClosingDateDto(result);
        }

        [AbpAuthorize(PermissionNames.Pages_ClosingDates_View)]
        public async Task<PagedResultDto<ClosingDateDto>> GetAll(PagedResultRequestDto paging)
        {
            var output = new PagedResultDto<ClosingDateDto>();

            var results = await _manager.GetAll().ToListAsync() as IReadOnlyList<ClosingDate>;

            output.TotalCount = results.Count();
            output.Items = results.Skip(paging.SkipCount).Take(paging.MaxResultCount).Select(x => new ClosingDateDto(x)).ToList();

            return output;
        }

        public List<ClosingDate> GetAllClosingDates()
        {
            var results = _manager.GetAll().ToList();
            return results;
        }

        [AbpAuthorize(PermissionNames.Pages_ClosingDates_View)]
        public async Task<PagedResultDto<ClosingDateListDto>> Search(ClosingDateSearchModel filters)
        {
            var res = new PagedResultDto<ClosingDateListDto>();

            var results = await _manager.GetFiltered(filters);

            res.TotalCount = results.TotalCount;
            res.Items = results.Items.Select(x => new ClosingDateListDto(x)).ToList();

            return res;
        }

        [AbpAuthorize(PermissionNames.Pages_ClosingDates_Modify)]
        public async Task<ClosingDateDto> Update(ClosingDateUpdateDto input)
        {
            if (input.Id == null)
                throw new EntityNotFoundException("ClosingDate id is null.");

            var model = await _manager.GetAsync((Guid)input.Id);
            var oldModel = model.Clone() as ClosingDate;
            if (model == null) throw new EntityNotFoundException("ClosingDate does not exist in database.");

            model.StartDate = input.StartDate;
            model.EndDate = input.EndDate;

            await _manager.UpdateAsync(model);
            await LogTransaction(oldModel, input, AuditLogOperationType.Update, "Updated a Closing Date record");

            return new ClosingDateDto(model);
        }
    }
}
