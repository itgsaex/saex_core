using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IClosingDateAppService : IApplicationService
    {
        Task<ClosingDateDto> Create(ClosingDateCreateDto input);

        Task Delete(Guid id);

        Task<ClosingDateDto> Get(Guid id);

        Task<PagedResultDto<ClosingDateDto>> GetAll(PagedResultRequestDto paging);

        Task<PagedResultDto<ClosingDateListDto>> Search(ClosingDateSearchModel filters);

        Task<ClosingDateDto> Update(ClosingDateUpdateDto input);

        List<ClosingDate> GetAllClosingDates();
    }
}