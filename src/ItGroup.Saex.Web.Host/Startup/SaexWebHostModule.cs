﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ItGroup.Saex.Configuration;

namespace ItGroup.Saex.Web.Host.Startup
{
    [DependsOn(
       typeof(SaexWebCoreModule))]
    public class SaexWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public SaexWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SaexWebHostModule).GetAssembly());
        }
    }
}
