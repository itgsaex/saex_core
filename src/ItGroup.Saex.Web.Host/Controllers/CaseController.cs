﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.EntityFrameworkCore;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CaseController : SaexControllerBase
    {
        private readonly ICaseAppService _caseService;

        public CaseController(ICaseAppService caseService)
        {
            _caseService = caseService;
        }

        [HttpGet]
        public List<Case> GetCases()
        {
            try
            {
                var results = _caseService.GetCases();
                return results.Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet("{agentID}")]
        public async Task<List<vwCases>> GetCasesAsync(string agentID)
        {
            try
            {
                var cases = await _caseService.GetAgentCases(agentID);
                return cases;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}