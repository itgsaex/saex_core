using Microsoft.AspNetCore.Antiforgery;
using ItGroup.Saex.Controllers;

namespace ItGroup.Saex.Web.Host.Controllers
{
    public class AntiForgeryController : SaexControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
