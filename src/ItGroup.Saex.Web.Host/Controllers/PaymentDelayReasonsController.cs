﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentDelayReasonsController : SaexControllerBase
    {
        private readonly IPaymentDelayReasonAppService _reasonsService;

        public PaymentDelayReasonsController(IPaymentDelayReasonAppService reasonsService)
        {
            _reasonsService = reasonsService;
        }

        [HttpGet]
        public List<PaymentDelayReason> GetAllPaymentDelayReasons()
        {
            var result = _reasonsService.GetAllPaymentDelayReasons().Result;
            return result;
        }
    }
}