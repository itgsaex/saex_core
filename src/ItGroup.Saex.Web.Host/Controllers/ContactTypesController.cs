﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;
using Abp.Application.Services.Dto;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactTypesController : SaexControllerBase
    {
        private readonly IContactTypeAppService _contactTypeAppService;

        public ContactTypesController(IContactTypeAppService contactTypeAppService)
        {
            _contactTypeAppService = contactTypeAppService;
        }

        [HttpGet]
        public List<ContactType> GetAlerts()
        {
            var result = _contactTypeAppService.GetAllContactTypes().Result;
            return result;
        }
    }
}