﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivityCodeDetailController : SaexControllerBase
    {
        private readonly IActivityCodeDetailAppService _appService;

        public ActivityCodeDetailController(IActivityCodeDetailAppService appService)
        {
            _appService = appService;
        }

        [HttpGet]
        public List<ActivityCodeDetail> GetAgents()
        {
            var result = _appService.GetAllActivityCodeDetails();
            return result;
        }
    }
}