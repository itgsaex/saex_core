﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : SaexControllerBase
    {
        private readonly ICategoryService _categoryTypeAppService;

        public CategoryController(ICategoryService categoryTypeAppService)
        {
            _categoryTypeAppService = categoryTypeAppService;
        }

        [HttpGet]
        public List<Category> GetCategories()
        {
            var result = _categoryTypeAppService.GetAllCategories();
            return result;
        }
    }
}