﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;


namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemPolicyController : SaexControllerBase
    {
        private readonly ISystemPolicyAppService _systemPolicyAppService;

        public SystemPolicyController(ISystemPolicyAppService systemPolicyAppService)
        {
            _systemPolicyAppService = systemPolicyAppService;
        }

        [HttpGet]
        public List<SystemPolicy> GetSystemPolicies()
        {
            var result = _systemPolicyAppService.GetAllSystemPolicies().Result;
            return result;
        }
    }
}