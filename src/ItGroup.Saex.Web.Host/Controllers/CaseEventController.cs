﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;
using System.Net.Http;
using System.Net;
using System;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CaseEventController : SaexControllerBase
    {
        private readonly ICaseEventAppService _appService;

        public CaseEventController(ICaseEventAppService appService)
        {
            _appService = appService;
        }

        [HttpPost]
        public HttpResponseMessage PutCaseEvents([FromBody] List<CaseEvent> models)
        {
            try
            {
                if (models != null)
                {
                    foreach (var model in models)
                    {
                        _appService.InsertCaseEvent(model);
                    }
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}