﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultsController : SaexControllerBase
    {
        private readonly IKPIAppService _kpiService;

        public ResultsController(IKPIAppService kpiService)
        {
            _kpiService = kpiService;
        }

        [HttpGet]
        public List<KPI> GetKPIs()
        {
            var result = _kpiService.GetAllKPIs();
            return result;
        }

        [HttpGet("{agentID}")]
        public List<KPI> GetKPIsFor(string agentID)
        {
            var result = _kpiService.GetKPIs(agentID);
            return result;
        }
    }
}