﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClosingDatesController : SaexControllerBase
    {
        private readonly IClosingDateAppService _closingDateAppService;

        public ClosingDatesController(IClosingDateAppService closingDateAppService)
        {
            _closingDateAppService = closingDateAppService;
        }

        [HttpGet]
        public List<ClosingDate> GetCategories()
        {
            var result = _closingDateAppService.GetAllClosingDates();
            return result;
        }
    }
}