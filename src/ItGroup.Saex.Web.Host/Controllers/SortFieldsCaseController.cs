﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SortFieldsCaseController : SaexControllerBase
    {
        private readonly ISortFieldsCaseService _sortFieldsCaseService;

        public SortFieldsCaseController(ISortFieldsCaseService sortFieldsCaseService)
        {
            _sortFieldsCaseService = sortFieldsCaseService;
        }

        [HttpGet("SortFieldsCase")]
        public List<SortFieldsCase> GetAlerts()
        {
            var result = _sortFieldsCaseService.GetSortFieldsCase();
            return result;
        }
    }
}