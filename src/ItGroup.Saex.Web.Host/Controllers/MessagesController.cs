﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : SaexControllerBase
    {
        private readonly IMessageAppService _messageService;

        public MessagesController(IMessageAppService messageService)
        {
            _messageService = messageService;
        }

        [HttpGet]
        public List<Message> GetMessagesAsync()
        {
            var result = _messageService.GetMessagesForUser(null);
            return result;
        }

        [HttpGet("{userId}")]
        public List<Message> GetMessagesForAsync(long userId)
        {
            var result = _messageService.GetMessagesForUser(userId);
            return result;
        }
    }
}