﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemographicFieldTypesController : SaexControllerBase
    {
        private readonly IDemographicFieldTypeAppService _demographicFieldTypeApp;

        public DemographicFieldTypesController(IDemographicFieldTypeAppService demographicFieldTypeApp)
        {
            _demographicFieldTypeApp = demographicFieldTypeApp;
        }

        [HttpGet]
        public List<DemographicFieldType> GetDemographicFieldTypes()
        {
            var result = _demographicFieldTypeApp.GetAllDemographicFieldTypes().Result;
            return result;
        }
    }
}