﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoRiesgoController : SaexControllerBase
    {
        private readonly IEstadoRiesgoService _estadoRiesgoService;

        public EstadoRiesgoController(IEstadoRiesgoService estadoRiesgoService)
        {
            _estadoRiesgoService = estadoRiesgoService;
        }

        [HttpGet]
        public List<EstadoRiesgo> GetEstadoRiesgos()
        {
            var result = _estadoRiesgoService.GetAllEstadoRiesgos();
            return result;
        }
    }
}