﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;


namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConditionCaseController : SaexControllerBase
    {
        private readonly IConditionCaseService _conditionCaseService;

        public ConditionCaseController(IConditionCaseService conditionCaseService)
        {
            _conditionCaseService = conditionCaseService;
        }

        [HttpGet]
        public List<ConditionCase> GetConditionCases()
        {
            var result = _conditionCaseService.GetAllConditionCase();
            return result;
        }
    }
}