﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;


namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SortCaseController : SaexControllerBase
    {
        private readonly ISortCaseService _sortCaseService;

        public SortCaseController(ISortCaseService sortCaseService)
        {
            _sortCaseService = sortCaseService;
        }

        [HttpGet]
        public List<SortCase> GetSystemPolicies()
        {
            var result = _sortCaseService.GetAllSortCases();
            return result;
        }
    }
}