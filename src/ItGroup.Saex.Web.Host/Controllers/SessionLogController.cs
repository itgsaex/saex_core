﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;
using System.Net.Http;
using System.Net;
using System;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionLogController : SaexControllerBase
    {
        private readonly ISessionLogAppService _appService;

        public SessionLogController(ISessionLogAppService appService)
        {
            _appService = appService;
        }

        [HttpPost]
        public HttpResponseMessage PutActivities([FromBody] List<SessionLog> models)
        {
            try
            {
                if (models != null)
                {
                    foreach (var model in models)
                    {
                        _appService.SyncSessionLogModel(model);
                    }
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}