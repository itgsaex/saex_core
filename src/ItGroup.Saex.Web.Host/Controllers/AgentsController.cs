﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgentsController : SaexControllerBase
    {
        private readonly IAgentAppService _agentAppService;

        public AgentsController(IAgentAppService agentAppService)
        {
            _agentAppService = agentAppService;
        }

        [HttpGet]
        public List<Agent> GetAgents()
        {
            var result = _agentAppService.GetAllAgents().Result;
            return result;
        }

        [HttpGet("{agentId}")]
        public List<vwAgent> GetAgentConfig(string agentId)
        {
            var result = _agentAppService.GetAgentConfig(agentId).Result;
            return result;
        }
    }
}