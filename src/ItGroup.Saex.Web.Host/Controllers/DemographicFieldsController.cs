﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemographicFieldsController : SaexControllerBase
    {
        private readonly IDemographicFieldAppService _demographicFieldApp;

        public DemographicFieldsController(IDemographicFieldAppService demographicFieldApp)
        {
            _demographicFieldApp = demographicFieldApp;
        }

        [HttpGet]
        public List<DemographicField> GetDemographicFields()
        {
            var result = _demographicFieldApp.GetAllDemographicFields().Result;
            return result;
        }
    }
}