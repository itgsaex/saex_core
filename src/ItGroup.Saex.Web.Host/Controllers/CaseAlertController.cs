﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CaseAlertController : SaexControllerBase
    {
        private readonly ICaseAlertAppService _caseAlertService;

        public CaseAlertController(ICaseAlertAppService caseAlertService)
        {
            _caseAlertService = caseAlertService;
        }

        [HttpGet("Alert")]
        public List<Alert> GetAlerts()
        {
            var result = _caseAlertService.GetAlerts();
            return result;
        }

        [HttpGet("{agentID}")]
        public List<CaseAlert> GetCaseAlerts(string agentID)
        {
            var result = _caseAlertService.GetCaseAlerts(agentID);
            return result;
        }
    }
}