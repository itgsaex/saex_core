﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivityCodeController : SaexControllerBase
    {
        private readonly IActivityCodeAppService _appService;

        public ActivityCodeController(IActivityCodeAppService appService)
        {
            _appService = appService;
        }

        [HttpGet]
        public List<ActivityCode> GetAgents()
        {
            var result = _appService.GetAllActivityCodes().Result;
            return result;
        }
    }
}