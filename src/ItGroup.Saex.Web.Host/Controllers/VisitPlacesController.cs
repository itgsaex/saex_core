﻿using System.Collections.Generic;
using System.Linq;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;
using Abp.Application.Services.Dto;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VisitPlacesController : SaexControllerBase
    {
        private readonly IVisitPlaceAppService _visitPlaceAppService;

        public VisitPlacesController(IVisitPlaceAppService visitPlaceAppService)
        {
            _visitPlaceAppService = visitPlaceAppService;
        }

        [HttpGet]
        public List<VisitPlace> GetVisitPlaces()
        {
            var result = _visitPlaceAppService.GetAllVisitPlaces().Result;
            return result;
        }
    }
}