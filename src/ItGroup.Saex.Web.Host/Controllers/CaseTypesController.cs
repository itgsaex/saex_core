﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CaseTypesController : SaexControllerBase
    {
        private readonly ICaseTypeAppService _caseTypeAppService;

        public CaseTypesController(ICaseTypeAppService caseTypeAppService)
        {
            _caseTypeAppService = caseTypeAppService;
        }

        [HttpGet]
        public List<CaseType> GetCaseTypes()
        {
            var result = _caseTypeAppService.GetAllCaseTypes().Result;
            return result;
        }
    }
}