﻿using System.Collections.Generic;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Web.Host.Models;
using System.Net.Http;
using System.Net;
using System;

namespace ItGroup.Saex.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivityController : SaexControllerBase
    {
        private readonly ICaseActivityAppService _appService;

        public ActivityController(ICaseActivityAppService appService)
        {
            _appService = appService;
        }

        [HttpPost]
        public HttpResponseMessage PutActivities([FromBody] List<ActivitySyncModel> models)
        {
            try
            {
                if (models != null)
                {
                    foreach (var model in models)
                    {
                        _appService.SyncActivityModel(model);
                    }
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}