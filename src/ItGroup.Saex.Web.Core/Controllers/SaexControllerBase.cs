using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using ItGroup.Saex.Authorization.Users;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace ItGroup.Saex.Controllers
{
    public abstract class SaexControllerBase : AbpController
    {
        protected int MaxResultCount = AppConsts.MaxResultsCount;

        protected SaexControllerBase()
        {
            LocalizationSourceName = SaexConsts.LocalizationSourceName;
        }

        protected async Task<bool> CanModify(string permissionName)
        {
            return await PermissionChecker.IsGrantedAsync(permissionName);
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
