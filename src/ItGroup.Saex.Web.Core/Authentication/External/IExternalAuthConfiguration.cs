﻿using System.Collections.Generic;

namespace ItGroup.Saex.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
