﻿using Abp.AutoMapper;
using ItGroup.Saex.Authentication.External;

namespace ItGroup.Saex.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
