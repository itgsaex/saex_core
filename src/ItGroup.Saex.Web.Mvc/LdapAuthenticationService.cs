﻿using Abp.UI;
using Novell.Directory.Ldap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.Web
{
    public class LdapAuthenticationService : IAuthenticationService
    {
        public bool ValidateUser(string domainName, string ip, string username, string password, string format)
        {

            if (string.IsNullOrWhiteSpace(format))
                format = "{1}\\{0}";

            string userDn = string.Format(format,username,domainName);

            /*if(ItGroup.Saex.Web.Startup.MyAppData.Configuration.GetSection("UITheme").Value == "green")
            {
                userDn = $"{username}@{domainName}";
            }
            else
            {
                userDn = $@"{username}\{domainName}";
            }*/

            try
            {
                using (var connection = new LdapConnection { SecureSocketLayer = false })
                {
                    connection.Connect(ip, LdapConnection.DefaultPort);
                    connection.Bind(userDn, password);

                    if (connection.Bound)
                        return true;
                }
            }
            catch (LdapException ex)
            {
                var message = ex.Message;
                var innerException = ex.InnerException;
                while (innerException != null)
                {
                    message = string.Format("{0} - {1}", message,ex.InnerException.Message);
                    innerException = innerException.InnerException;
                }
                // Log exception
                throw new UserFriendlyException(message);
            }
            return false;
        }
    }
    public interface IAuthenticationService
    {
        bool ValidateUser(string domainName, string ip, string username, string password, string format);
    }
}
