﻿namespace ItGroup.Saex.Web.Startup
{
    public class PageNames
    {
        public const string About = "About";

        public const string ActivityCodes = "Activity Codes";

        public const string AdjusterStates = "Adjuster States";

        public const string AgentActivity = "Agent Activity";

        public const string Agents = "Agents";

        public const string AgentSupervisors = "Agent Supervisors";

        public const string AssignCases = "Assign Cases";

        public const string AssignRegions = "Assign Regions";

        public const string AuditLogs = "Audit Logs";

        public const string Buckets = "Buckets";

        public const string Cases = "Cases";

        public const string CasesTypes = "Case Types";

        public const string ClosingDates = "Closing Dates";

        public const string Conditions = "Conditions";

        public const string ContactTypes = "Contact Types";

        public const string DelinquencyStates = "Delinquency States";

        public const string Home = "Home";

        public const string Import = "Data Import";

        public const string ListFieldCategories = "List Field Categories";

        public const string ListFields = "List Fields";

        public const string Lists = "Lists";

        public const string MacroStates = "Macro States";

        public const string Messages = "Messages";

        public const string PaymentDelayReasons = "Payment Delay Reasons";

        public const string PostalCodes = "Postal Codes";

        public const string DemographicFields = "Demographic Fields";

        public const string ProductRules = "Product Rules";

        public const string Products = "Products";

        public const string Regions = "Regions";

        public const string Report_Audit = "Audit Report";

        public const string Report_KPI = "KPI Report";

        public const string Report_ReceivedVsAssign = "Comparison of Cases Received and Assigned Report";

        public const string Report_Profiles = "Porfiles Report";

        public const string Report_Cases = "Cases Report";

        public const string Report_Users = "Users Report";

        public const string Roles = "Roles";

        public const string Routes = "Routes";

        public const string ScorecardParameters = "Scorecard Parameters";

        public const string SystemPolicies = "System Policies";

        public const string SystemStates = "System States";

        public const string SystemSubStates = "System SubStates";

        public const string Tenants = "Tenants";

        public const string Users = "Users";

        public const string VisitPlaces = "Visit Places";

        public const string WorkingHours = "Working Hours";

        public const string BaselineCasesImport = "Baseline Case Import";

        public const string Report_AddressManagement = "Address Management Report";

        public const string Report_UnassignedCases = "Unassigned Cases Report";

        public const string Report_UnfilteredCasesByMaster = "Unfiltered Cases By Master Report";

        public const string Report_TransferredCases = "Transferred & Shared Cases Report";

        public const string Report_AgentActivity = "Agent Activity Report";

        public const string Report_BucketsDelinquencyMortgage = "Buckets Delinquency Mortgage Report";

        public const string Report_BucketsDelinquencyMortgageWithIndicator = "Buckets Delinquency Mortgage With Indicator Report";
        
        public const string Report_AgentLocationRegistry = "Agent Location Registry Report";

        public const string Report_KPISolvedCases = "KPI Solved Cases Report";

        public const string Report_DailyAgentActivityEffectiveness = "Daily Agent Activity and Effectiveness Report";

        public const string Report_SummaryAgentActivityEffectiveness = "Summary Agent Activity and Effectiveness Report";

        public const string Report_LoginAuditTrail = "Login Audit Trail Report";

        public const string RunUpdate = "Run Update";

        public const string AssignExternalAgents = "Assign External Agents";
        
        public const string ValidFieldsForLists = "Valid Fields For Lists";

        public const string MasterLists = "Master Lists";

        public const string ChildLists = "Child Lists";

        public const string RiskLists = "Risk Lists";

        public const string Report_ActivityReport = "Activity Report";

        public const string InternalAgentDashboard = "Internal Agent Dashboard";
        public const string DailyReport = "Daily Report";
        public const string ProyeccionPerdida = "Proyeccion Perdida";
        public const string PortfolioSupervisor = "Portfolio Supervisor";
        public const string AssignAccountsReport = "Assign Accounts Report";
        public const string DuplicateAccountsReport = "Duplicate Accounts Report";
        public const string AssignRoutes = "Assign Routes Report";
    }
}