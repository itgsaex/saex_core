﻿using Abp.Application.Navigation;
using Abp.Localization;
using ItGroup.Saex.Authorization;
using Microsoft.Extensions.Configuration;

namespace ItGroup.Saex.Web.Startup
{
    /// <summary>
    /// This class defines menus for the application.
    /// </summary>
    public class SaexNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            var configuration = MyAppData.Configuration;
            if (configuration.GetSection("Theme").Value.ToUpper() == "BANCOPOPULAR")
            {
                context.Manager.MainMenu
                .AddItem(new MenuItemDefinition(
                        PageNames.AgentActivity,
                        L("AgentActivity"),
                        url: "AgentActivity",
                        icon: "supervisor_account",
                        requiredPermissionName: PermissionNames.Pages_AgentActivity_View
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Security",
                        L("Security"),
                        icon: "visibility",
                        requiredPermissionName: PermissionNames.Menu_Security
                    ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Tenants,
                                L("Tenants"),
                                url: "Tenants",
                                icon: "business",
                                requiredPermissionName: PermissionNames.Pages_Tenants_View
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Users,
                                L("Users"),
                                url: "Users",
                                icon: "people",
                                requiredPermissionName: PermissionNames.Pages_Users_View
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Roles,
                                L("Roles"),
                                url: "Roles",
                                icon: "local_offer",
                                requiredPermissionName: PermissionNames.Pages_Roles_View
                            )
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "AgentSupervisors",
                        L("AssignInternalAgents"),
                        url: "AgentSupervisors",
                        icon: "supervisor_account",
                        requiredPermissionName: PermissionNames.Pages_AgentSupervisors_View
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.AssignExternalAgents,
                        L("AssignExternalAgents"),
                        url: "AssignExternalAgents",
                        icon: "supervisor_account",
                        requiredPermissionName: PermissionNames.Pages_AssignExternalAgents_View
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.InternalAgentDashboard,
                        L("InternalAgentDashboard"),
                        url: "InternalAgentDashboard",
                        icon: "supervisor_account",
                        requiredPermissionName: PermissionNames.Pages_InternalAgentDashboard_View
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.DailyReport,
                        L("DailyReport"),
                        url: "DailyReport",
                        icon: "supervisor_account",
                        requiredPermissionName: PermissionNames.Pages_DailyReport_View
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.ProyeccionPerdida,
                        L("ProyeccionPerdida"),
                        url: "ProyeccionPerdida",
                        icon: "supervisor_account",
                        requiredPermissionName: PermissionNames.Pages_ProyeccionPerdida_View
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.PortfolioSupervisor,
                        L("PortfolioSupervisor"),
                        url: "PortfolioSupervisor",
                        icon: "supervisor_account",
                        requiredPermissionName: PermissionNames.Pages_PortfolioSupervisor_View
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Lists,
                        L("Lists"),
                        icon: "list",
                        requiredPermissionName: PermissionNames.Menu_Lists
                    ).AddItem(
                            new MenuItemDefinition(
                                PageNames.MasterLists,
                                L("MasterLists"),
                                url: "Lists/Lists?listType=" + Enumerations.Enumerations.ListType.Master,
                                icon: "list",
                                requiredPermissionName: PermissionNames.Pages_MasterLists_View
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ChildLists,
                                L("ChildLists"),
                                url: "Lists/Lists?listType=" + Enumerations.Enumerations.ListType.Child,
                                icon: "list",
                                requiredPermissionName: PermissionNames.Pages_ChildLists_View
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.RiskLists,
                                L("RiskLists"),
                                url: "Lists/Lists?listType=" + Enumerations.Enumerations.ListType.Risk,
                                icon: "list",
                                requiredPermissionName: PermissionNames.Pages_RiskLists_View
                            )
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Maintenance",
                        L("Maintenance"),
                        icon: "history",
                        requiredPermissionName: PermissionNames.Menu_Maintenance
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.PostalCodes,
                            L("PostalCodes"),
                            url: "PostalCodes",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_PostalCodes_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Routes,
                            L("Routes"),
                            url: "Routes",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Routes_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Regions,
                            L("Regions"),
                            url: "Regions",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Regions_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.AssignRegions,
                            L("AssignRegions"),
                            url: "AssignRegions",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_AssignRegions_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.AssignCases,
                            L("AssignCases"),
                            url: "AssignCases",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_AssignCases_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.AdjusterStates,
                            L("AdjusterStates"),
                            url: "AdjusterStates",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_AdjusterStates_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.SystemStates,
                            L("SystemStates"),
                            url: "SystemStates",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_SystemStates_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.VisitPlaces,
                            L("VisitPlaces"),
                            url: "VisitPlaces",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_VisitPlaces_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ContactTypes,
                            L("ContactTypes"),
                            url: "ContactTypes",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_ContactTypes_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.PaymentDelayReasons,
                            L("PaymentDelayReasons"),
                            url: "PaymentDelayReasons",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_PaymentDelayReasons_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ActivityCodes,
                            L("ActivityCodes"),
                            url: "ActivityCodes",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_ActivityCodes_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ClosingDates,
                            L("ClosingDates"),
                            url: "ClosingDates",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_ClosingDates_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.DelinquencyStates,
                            L("DelinquencyStates"),
                            url: "DelinquencyStates",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_DelinquencyStates_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ValidFieldsForLists,
                            L("ValidFieldsForLists"),
                            url: "ValidFieldsForLists",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_ValidFieldsForLists_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Buckets,
                            L("Buckets"),
                            url: "Buckets",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Buckets_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Conditions,
                            L("Conditions"),
                            url: "Conditions",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Conditions_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ProductRules,
                            L("ProductRules"),
                            url: "ProductRules",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Products_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.WorkingHours,
                            L("WorkingHours"),
                            url: "WorkingHours",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_WorkingHours_View
                        )
                    )
                ).AddItem(new MenuItemDefinition(
                        "Messages",
                        L("Messages"),
                        url: "Messages",
                        icon: "speaker_notes",
                        requiredPermissionName: PermissionNames.Pages_Messages_View
                        )
                ).AddItem(new MenuItemDefinition(
                        "Configuration",
                        L("Configuration"),
                        icon: "apps"
                        ).AddItem(
                        new MenuItemDefinition(
                            PageNames.SystemPolicies,
                            L("SystemPolicies"),
                            url: "SystemPolicies",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_SystemPolicies_View
                        )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ScorecardParameters,
                                L("ScorecardParameters"),
                                url: "ScorecardParameters",
                                icon: "",
                                requiredPermissionName: PermissionNames.Pages_ScorecardParameters_View
                            )
                        )
              ).AddItem(new MenuItemDefinition(
                        "Reports",
                        L("Reports"),
                        icon: "view_list",
                        requiredPermissionName: PermissionNames.Pages_Reports_View
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_Audit,
                                L("AuditReport"),
                                url: "reports/audit",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_Audit_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_Cases,
                                L("CaseReport"),
                                url: "reports/cases",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_Cases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_Users,
                                L("UsersReport"),
                                url: "reports/users",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_Users_Report)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_KPI,
                                L("KPIReport"),
                                url: "reports/kpi",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_KPI_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_ReceivedVsAssign,
                                L("ReceivedVsAssignedCasesReport"),
                                url: "reports/receivedvsassignedcases",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_ReceivedVsAssignedCases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_UnassignedCases,
                                L("UnassignedCasesReport"),
                                url: "reports/unassignedCases",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_UnassignedCases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_UnfilteredCasesByMaster,
                                L("UnfilteredCasesByMasterReport"),
                                url: "reports/unfilteredCasesByMaster",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_UnfilteredCases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_TransferredCases,
                                L("TransferredCasesReport"),
                                url: "reports/transferredCases",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_TransferredCases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_BucketsDelinquencyMortgageWithIndicator,
                                L("BucketsDelinquencyMortgageWithIndicatorReport"),
                                url: "reports/bucketsdelinquencymortgagewithindicator",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_BucketsDelinquencyMortgageWithIndicator_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_AgentLocationRegistry,
                                L("AgentLocationRegistryReport"),
                                url: "reports/agentlocationregistry",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_AgentLocationRegistry_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_DailyAgentActivityEffectiveness,
                                L("DailyAgentActivityEffectivenessReport"),
                                url: "reports/dailyagentactivityeffectiveness",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_DailyAgentActivityEffectiveness_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_SummaryAgentActivityEffectiveness,
                                L("SummaryAgentActivityEffectivenessReport"),
                                url: "reports/summaryagentactivityeffectiveness",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_SummaryAgentActivityEffectiveness_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_ActivityReport,
                                L("ActivityReport"),
                                url: "reports/activityreport",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_Activity_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.AssignAccountsReport,
                                L("AssignAccountsReport"),
                                url: "reports/assignaccountsreport",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_AssignAccounts_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.DuplicateAccountsReport,
                                L("DuplicateAccountsReport"),
                                url: "reports/duplicateaccountsreport",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_DuplicateAccounts_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.AssignRoutes,
                                L("AssignRoutes"),
                                url: "reports/rutasasignadas",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_AssignRoutes_Report_View)
                         )
              ).AddItem(new MenuItemDefinition(
                        PageNames.RunUpdate,
                        L("RunUpdate"),
                        url: "UpdateRun",
                        icon: "restore",
                        requiredPermissionName: PermissionNames.Pages_UpdateRun_View
                        )
              );
            } 
            else
            {
                context.Manager.MainMenu
                .AddItem(new MenuItemDefinition(
                        PageNames.AgentActivity,
                        L("AgentActivity"),
                        url: "AgentActivity",
                        icon: "supervisor_account",
                        requiredPermissionName: PermissionNames.Pages_AgentActivity_View
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Security",
                        L("Security"),
                        icon: "visibility",
                        requiredPermissionName: PermissionNames.Menu_Security
                    ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Tenants,
                                L("Tenants"),
                                url: "Tenants",
                                icon: "business",
                                requiredPermissionName: PermissionNames.Pages_Tenants_View
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Users,
                                L("Users"),
                                url: "Users",
                                icon: "people",
                                requiredPermissionName: PermissionNames.Pages_Users_View
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Roles,
                                L("Roles"),
                                url: "Roles",
                                icon: "local_offer",
                                requiredPermissionName: PermissionNames.Pages_Roles_View
                            )
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Lists,
                        L("Lists"),
                        icon: "list",
                        requiredPermissionName: PermissionNames.Menu_Lists
                    ).AddItem(
                            new MenuItemDefinition(
                                PageNames.MasterLists,
                                L("MasterLists"),
                                url: "Lists/Lists?listType=" + Enumerations.Enumerations.ListType.Master,
                                icon: "list",
                                requiredPermissionName: PermissionNames.Pages_MasterLists_View
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ChildLists,
                                L("ChildLists"),
                                url: "Lists/Lists?listType=" + Enumerations.Enumerations.ListType.Child,
                                icon: "list",
                                requiredPermissionName: PermissionNames.Pages_ChildLists_View
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.RiskLists,
                                L("RiskLists"),
                                url: "Lists/Lists?listType=" + Enumerations.Enumerations.ListType.Risk,
                                icon: "list",
                                requiredPermissionName: PermissionNames.Pages_RiskLists_View
                            )
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Maintenance",
                        L("Maintenance"),
                        icon: "history",
                        requiredPermissionName: PermissionNames.Menu_Maintenance
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.PostalCodes,
                            L("PostalCodes"),
                            url: "PostalCodes",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_PostalCodes_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.DemographicFields,
                            L("DemographicFields"),
                            url: "DemographicFields",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_DemographicFields
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Routes,
                            L("Routes"),
                            url: "Routes",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Routes_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Regions,
                            L("Regions"),
                            url: "Regions",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Regions_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.AssignRegions,
                            L("AssignRegions"),
                            url: "AssignRegions",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_AssignRegions_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.AssignCases,
                            L("AssignCases"),
                            url: "AssignCases",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_AssignCases_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.AdjusterStates,
                            L("AdjusterStates"),
                            url: "AdjusterStates",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_AdjusterStates_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.SystemStates,
                            L("SystemStates"),
                            url: "SystemStates",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_SystemStates_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.VisitPlaces,
                            L("VisitPlaces"),
                            url: "VisitPlaces",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_VisitPlaces_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ContactTypes,
                            L("ContactTypes"),
                            url: "ContactTypes",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_ContactTypes_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.PaymentDelayReasons,
                            L("PaymentDelayReasons"),
                            url: "PaymentDelayReasons",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_PaymentDelayReasons_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ActivityCodes,
                            L("ActivityCodes"),
                            url: "ActivityCodes",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_ActivityCodes_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ClosingDates,
                            L("ClosingDates"),
                            url: "ClosingDates",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_ClosingDates_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.DelinquencyStates,
                            L("DelinquencyStates"),
                            url: "DelinquencyStates",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_DelinquencyStates_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ValidFieldsForLists,
                            L("ValidFieldsForLists"),
                            url: "ValidFieldsForLists",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_ValidFieldsForLists_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Buckets,
                            L("Buckets"),
                            url: "Buckets",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Buckets_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Conditions,
                            L("Conditions"),
                            url: "Conditions",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Conditions_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.ProductRules,
                            L("ProductRules"),
                            url: "ProductRules",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_Products_View
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.WorkingHours,
                            L("WorkingHours"),
                            url: "WorkingHours",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_WorkingHours_View
                        )
                    )
                ).AddItem(new MenuItemDefinition(
                        "Messages",
                        L("Messages"),
                        url: "Messages",
                        icon: "speaker_notes",
                        requiredPermissionName: PermissionNames.Pages_Messages_View
                        )
                ).AddItem(new MenuItemDefinition(
                        "Configuration",
                        L("Configuration"),
                        icon: "apps"
                        ).AddItem(
                        new MenuItemDefinition(
                            PageNames.SystemPolicies,
                            L("SystemPolicies"),
                            url: "SystemPolicies",
                            icon: "",
                            requiredPermissionName: PermissionNames.Pages_SystemPolicies_View
                        )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ScorecardParameters,
                                L("ScorecardParameters"),
                                url: "ScorecardParameters",
                                icon: "",
                                requiredPermissionName: PermissionNames.Pages_ScorecardParameters_View
                            )
                        )
              ).AddItem(new MenuItemDefinition(
                        "Reports",
                        L("Reports"),
                        icon: "view_list",
                        requiredPermissionName: PermissionNames.Pages_Reports_View
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_Audit,
                                L("AuditReport"),
                                url: "reports/audit",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_Audit_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_Cases,
                                L("CaseReport"),
                                url: "reports/cases",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_Cases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_Users,
                                L("UsersReport"),
                                url: "reports/users",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_Users_Report)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_KPI,
                                L("KPIReport"),
                                url: "reports/kpi",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_KPI_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_ReceivedVsAssign,
                                L("ReceivedVsAssignedCasesReport"),
                                url: "reports/receivedvsassignedcases",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_ReceivedVsAssignedCases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_Profiles,
                                L("ProfilesReport"),
                                url: "reports/profiles",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_Profiles_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_AddressManagement,
                                L("AddressManagementReport"),
                                url: "reports/addressManagement",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_AddressManagement_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_UnassignedCases,
                                L("UnassignedCasesReport"),
                                url: "reports/unassignedCases",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_UnassignedCases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_UnfilteredCasesByMaster,
                                L("UnfilteredCasesByMasterReport"),
                                url: "reports/unfilteredCasesByMaster",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_UnfilteredCases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_TransferredCases,
                                L("TransferredCasesReport"),
                                url: "reports/transferredCases",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_TransferredCases_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_AgentActivity,
                                L("AgentActivityReport"),
                                url: "reports/agentactivity",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_AgentActivity_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_BucketsDelinquencyMortgage,
                                L("BucketsDelinquencyMortgageReport"),
                                url: "reports/bucketsdelinquencymortgage",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_BucketsDelinquencyMortgage_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_AgentLocationRegistry,
                                L("AgentLocationRegistryReport"),
                                url: "reports/agentlocationregistry",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_AgentLocationRegistry_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_KPISolvedCases,
                                L("KPISolvedCasesReport"),
                                url: "reports/kpisolvedcases",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_AgentLocationRegistry_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_DailyAgentActivityEffectiveness,
                                L("DailyAgentActivityEffectivenessReport"),
                                url: "reports/dailyagentactivityeffectiveness",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_DailyAgentActivityEffectiveness_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_SummaryAgentActivityEffectiveness,
                                L("SummaryAgentActivityEffectivenessReport"),
                                url: "reports/summaryagentactivityeffectiveness",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_SummaryAgentActivityEffectiveness_Report_View)
                         ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Report_LoginAuditTrail,
                                L("LoginAuditTrailReport"),
                                url: "reports/loginaudittrail",
                                icon: "view_list",
                                requiredPermissionName: PermissionNames.Pages_LoginAuditTrail_Report_View)
                         )
              ).AddItem(new MenuItemDefinition(
                            PageNames.Import,
                            L("BaselineCases"),
                            url: "BaselineCasesImport",
                            icon: "cloud_upload",
                            requiredPermissionName: PermissionNames.Pages_BaselineCasesImport_View
                        )
              );
            }
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, SaexConsts.LocalizationSourceName);
        }
    }
}
