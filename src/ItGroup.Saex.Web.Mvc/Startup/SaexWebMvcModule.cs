﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ItGroup.Saex.Configuration;

namespace ItGroup.Saex.Web.Startup
{
    [DependsOn(typeof(SaexWebCoreModule))]
    public class SaexWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public SaexWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<SaexNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.Register(typeof(DevExpress.AspNetCore.Reporting.WebDocumentViewer.WebDocumentViewerController), Abp.Dependency.DependencyLifeStyle.Transient);
            IocManager.Register(typeof(DevExpress.AspNetCore.Reporting.QueryBuilder.QueryBuilderController), Abp.Dependency.DependencyLifeStyle.Transient);
            IocManager.Register(typeof(DevExpress.AspNetCore.Reporting.ReportDesigner.ReportDesignerController), Abp.Dependency.DependencyLifeStyle.Transient);

            IocManager.RegisterAssemblyByConvention(typeof(SaexWebMvcModule).GetAssembly());
        }
    }
}
