﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(SystemPolicyDto))]
    public class EditSystemPolicyModalViewModel : SystemPolicyDto
    {
    }
}