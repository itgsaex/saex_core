﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class SystemPolicyListViewModel
    {
        public SystemPolicyListViewModel()
        {
        }

        public SystemPolicyListViewModel(PagedResultDto<SystemPolicyListDto> systemPolicies)
        {
            SystemPolicies = systemPolicies;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<SystemPolicyListDto> SystemPolicies { get; set; }
    }
}
