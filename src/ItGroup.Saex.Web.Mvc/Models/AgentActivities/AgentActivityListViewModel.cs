﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AgentActivityViewModel
    {
        public AgentActivityViewModel(AgentActivityIndexDto input)
        {
            AvailableAgents = input.Agents;

            if (input.Activity != null)
            {
                Agent = input.Activity.Agent;
                Activity = input.Activity;
            }
        }

        public AgentActivityDto Activity { get; set; }

        public AgentDto Agent { get; set; }

        public List<AgentListDto> AvailableAgents { get; set; }
    }
}
