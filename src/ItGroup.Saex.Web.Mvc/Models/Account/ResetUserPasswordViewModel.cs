using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Roles.Dto;
using ItGroup.Saex.Users.Dto;

namespace ItGroup.Saex.Web.Models.Users
{
    public class ResetUserPasswordViewModel
    {
        [Required]
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public long UserId { get; set; }

    }
}
