﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(CaseTypeDto))]
    public class EditCaseTypeModalViewModel : CaseTypeUpdateDto
    {
        public EditCaseTypeModalViewModel()
        {
        }

        public EditCaseTypeModalViewModel(List<ProductListDto> products)
        {
            Products = products;
        }

        public List<ProductListDto> Products { get; set; }
    }
}
