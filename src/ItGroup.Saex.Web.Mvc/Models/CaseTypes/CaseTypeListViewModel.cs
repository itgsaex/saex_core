﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class CaseTypeListViewModel : CaseTypeCreateDto
    {
        public CaseTypeListViewModel()
        {
        }

        public CaseTypeListViewModel(PagedResultDto<CaseTypeListDto> caseTypes, List<ProductListDto> products)
        {
            CaseTypes = caseTypes;
            Products = products;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<CaseTypeListDto> CaseTypes { get; set; }

        public List<ProductListDto> Products { get; set; }
    }
}
