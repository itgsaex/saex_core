﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Web.Startup;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class UnfilteredCasesViewModel
    {
        public List<ProductListDto> Products { get; set; }

        public string SelectedProduct { get; set; }

        public string SelectedProductCode { get; set; }

        public string AccountNumber { get; set; }

        public string ClientName { get; set; }

        public string ZipCode { get; set; }
    }
}