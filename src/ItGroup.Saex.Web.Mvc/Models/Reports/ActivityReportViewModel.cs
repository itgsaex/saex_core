﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Web.Startup;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ActivityReportViewModel
    {
        public List<Agent> AgentsFrom { get; set; }

        public List<Agent> AgentsTo { get; set; }

        public Agent SelectedAgentFrom { get; set; }
        public Agent SelectedAgentTo { get; set; }

        public Guid? SelectedAgentFromID { get; set; }
        public Guid? SelectedAgentToID { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateFrom { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateTo { get; set; }

        public long? SupervisorID { get; set; }
        public string SupervisorName { get; set; }

        public long? InternalAgentID { get; set; }
        public string InternalAgentName { get; set; }
        public long? AgentFromUserID { get; set; }
        public long? AgentToUserID { get; set; }

        public string AgentFromName { get; set; }
        public string AgentToName { get; set; }
    }
}
