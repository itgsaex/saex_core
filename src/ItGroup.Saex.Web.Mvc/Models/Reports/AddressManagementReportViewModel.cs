﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AddressManagementReportViewModel
    {
        [DataType(DataType.Date)]
        public DateTime DateFrom { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateTo { get; set; }
    }
}