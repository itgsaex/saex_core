﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Web.Startup;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class DailyAgentActivityEffectivenessReportViewModel
    {
        public List<Agent> AgentDtos { get; set; }

        public Agent SelectedAgent { get; set; }

        public Guid? SelectedAgentID { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateFrom { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateTo { get; set; }
    }
}
