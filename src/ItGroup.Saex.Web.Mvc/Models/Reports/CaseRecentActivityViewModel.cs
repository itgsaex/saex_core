﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Enumerations;
using ItGroup.Saex.Models;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Web.Startup;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class CaseRecentActivityViewModel
    {
        public CaseRecentActivityViewModel()
        {
        }

        public CaseRecentActivityViewModel(
           string caseNumber, 
           List<CaseRecentActivity> activities
        )
        {
            CaseNumber = caseNumber;
            Activities = activities;
        }

        public List<CaseRecentActivity> Activities { get; set; }

        public string CaseNumber { get; set; }
    }
}