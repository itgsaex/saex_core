﻿using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    public abstract class ReportViewModel<TReportData>
    {
        public IReadOnlyList<TReportData> ReportData { get; set; }
    }
}
