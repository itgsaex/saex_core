﻿using System.Collections.Generic;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class UnassignedCasesViewModel
    {
        public List<Product> Products { get; set; }

        public Product SelectedProduct { get; set; }

        public string SelectedProductCode { get; set; }

        public string AccountNumber { get; set; }

        public string ClientName { get; set; }

        public string ZipCode { get; set; }
    }
}