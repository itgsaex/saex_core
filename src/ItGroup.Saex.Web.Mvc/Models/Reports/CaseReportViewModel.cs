﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Enumerations;
using ItGroup.Saex.Models;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Web.Startup;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class CaseReportViewModel
    {
        public CaseReportViewModel()
        {
        }

        public CaseReportViewModel(
            List<CaseReportModel> cases,
            List<AgentListDto> agents,
            List<string> delinquencyStates,
            List<PostalCodeListDto> postalCodes,
            List<RegionListDto> regions,
            List<RouteListDto> routes,
            string accountNumber,
            string name
         )
        {
            Agents = agents;
            DelinquencyStates = delinquencyStates;
            PostalCodes = postalCodes;
            Regions = regions;
            Routes = routes;
            AccountNumber = accountNumber;
            Name = name;
            Cases = cases;
            AgentId = Guid.Empty;
            DelinquencyStateId = string.Empty;
            RouteId = Guid.Empty;
            RegionId = Guid.Empty;

            AssignTypes = new List<string>();
            AssignTypes.Add("Region");
            AssignTypes.Add("Lista");
            AssignTypes.Add("Transfer");
            AssignTypes.Add("Shared");
        }

        public string AccountNumber { get; set; }

        public Guid AgentId { get; set; }

        public List<AgentListDto> Agents { get; set; }

        public List<CaseReportModel> Cases { get; set; }

        public EnumerationModel DateTypes { get; set; }

        public string DelinquencyStateId { get; set; }

        public List<string> DelinquencyStates { get; set; }

        public string Name { get; set; }

        public Guid PostalCodeId { get; set; }

        public List<PostalCodeListDto> PostalCodes { get; set; }

        public Guid RegionId { get; set; }

        public List<RegionListDto> Regions { get; set; }

        public Guid RouteId { get; set; }

        public List<RouteListDto> Routes { get; set; }

        public string AssignType { get; set; }

        public List<string> AssignTypes { get; set; }
    }

    public class AssignAccountsReportViewModel
    {
        public string AgentID { get; set; }
        public List<AgentListDto> Agents { get; set; }
        public int? RegionID { get; set; }
        public List<RegionListDto> Regions { get; set; }
        public int? RouteID { get; set; }
        public List<RouteListDto> Routes { get; set; }
    }
}
