﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Web.Startup;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class BucketsDelinquencyMortgageReportViewModel
    {
        public List<string> AgingDtos { get; set; }

        public string SelectedAging { get; set; }

        public List<string> VisitedDtos { get; set; }

        public string SelectedVisit { get; set; }
    }
}
