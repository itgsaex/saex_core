﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Web.Startup;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AuditReportViewModel
    {
        public AuditReportViewModel()
        {
        }

        public AuditReportViewModel(List<UserDto> users)
        {
            DateTypes = new EnumerationModel(DateType);
            PageNames = new List<string>();
            User = new UserDto();

            Users = users;
            //Results = results;

            var pageNames = new PageNames();
            foreach (var prop in pageNames.GetType().GetProperties())
            {
                PageNames.Add(prop.GetValue(pageNames, null).ToString());
            }
        }

        public DateRangeType DateType { get; set; }

        public EnumerationModel DateTypes { get; set; }

        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        public List<string> PageNames { get; set; }

        //public PagedResultDto<AuditReportModel> Results { get; set; }
        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }

        public UserDto User { get; set; }

        public List<UserDto> Users { get; set; }

        public List<string> AuditLogTables { get; set; }
        public string SelectedAuditLogTable { get; set; }
    }
}