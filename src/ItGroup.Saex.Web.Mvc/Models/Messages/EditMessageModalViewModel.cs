﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(MessageDto))]
    public class EditMessageModalViewModel : MessageDto
    {
    }
}