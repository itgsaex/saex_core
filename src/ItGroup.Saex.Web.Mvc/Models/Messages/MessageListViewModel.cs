﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class MessageListViewModel : MessageCreateDto
    {
        public MessageListViewModel()
        {
        }

        public MessageListViewModel(PagedResultDto<MessageListDto> recipients)
        {
            Recipients = recipients;
        }

        public List<AgentListDto> AvailableAgents { get; set; }

        public bool CanModify { get; set; }

        public PagedResultDto<MessageListDto> Recipients { get; set; }
    }
}
