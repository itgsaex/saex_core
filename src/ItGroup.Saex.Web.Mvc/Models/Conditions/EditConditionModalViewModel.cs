﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(ConditionDto))]
    public class EditConditionModalViewModel : ConditionUpdateDto
    {
        public EditConditionModalViewModel()
        {
            Products = new List<ProductListDto>();
        }

        public List<ProductListDto> Products { get; set; }

        public List<string> ConditionsTypes { get; set; } = new List<string>() { "WORST", "STABLE", "BETTER" };
    }
}