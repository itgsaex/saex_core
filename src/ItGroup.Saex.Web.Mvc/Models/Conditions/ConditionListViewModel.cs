﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ConditionListViewModel : ConditionCreateDto
    {
        public ConditionListViewModel()
        {
            Products = new List<ProductListDto>();
        }

        public ConditionListViewModel(PagedResultDto<ConditionListDto> conditions)
        {
            Conditions = conditions;
        }

        public List<ProductListDto> Products { get; set; }

        public bool CanModify { get; set; }

        public PagedResultDto<ConditionListDto> Conditions { get; set; }

        public List<string> ConditionsTypes { get; set; } = new List<string>() { "WORST", "STABLE", "BETTER"};
    }
}
