﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AssignAgentsModalViewModel
    {
        public AssignAgentsModalViewModel()
        {
            Agents = new List<AssignAgentDto>();

            Supervisor = new AgentDto();

            SupervisorTypes = new EnumerationModel(SupervisorType);
        }

        public List<AssignAgentDto> Agents { get; set; }

        public AgentDto Supervisor { get; set; }

        public Guid SupervisorId { get; set; }

        public SupervisorType SupervisorType { get; set; }

        public EnumerationModel SupervisorTypes { get; set; }
    }
}
