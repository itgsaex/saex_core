﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Users.Dto;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AssignInternalAgentViewModel
    {
        public List<UserDto> Supervisors { get; set; }

        public UserDto Supervisor { get; set; }

        public List<UserDto> InternalAgents { get; set; }

        public bool CanModify { get; set; }

        public AssignInternalAgentViewModel()
        {
            Supervisors = new List<UserDto>();
            InternalAgents = new List<UserDto>();
        }
    }

    public class AssignExternalAgentViewModel
    {
        public List<UserDto> ExternalAgents { get; set; }

        public UserDto InternalAgent { get; set; }

        public List<UserDto> InternalAgents { get; set; }

        public bool CanModify { get; set; }

        public AssignExternalAgentViewModel()
        {
            ExternalAgents = new List<UserDto>();
            InternalAgents = new List<UserDto>();
        }
    }
}
