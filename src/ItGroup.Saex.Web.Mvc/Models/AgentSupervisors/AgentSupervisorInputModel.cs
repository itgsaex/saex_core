﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AgentSupervisorInputModel
    {
        public AgentSupervisorInputModel()
        {
        }

        public Guid AgentId { get; set; }

        public List<Guid> SelectedCases { get; set; }
    }
}
