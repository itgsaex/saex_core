﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class RouteListViewModel : RouteCreateDto
    {
        public RouteListViewModel()
        {
        }

        public RouteListViewModel(
            PagedResultDto<RouteListDto> routes,
            List<PostalCodeListDto> availablePostalCodes,
            List<ProductListDto> availableProducts
            )
        {
            Routes = routes;
            AvailablePostalCodes = availablePostalCodes;
            AvailableProducts = availableProducts;
        }

        public List<PostalCodeListDto> AvailablePostalCodes { get; set; }

        public List<ProductListDto> AvailableProducts { get; set; }

        public bool CanModify { get; set; }

        public PagedResultDto<RouteListDto> Routes { get; set; }
    }
}
