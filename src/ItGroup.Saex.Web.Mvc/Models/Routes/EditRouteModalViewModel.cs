﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(RouteDto))]
    public class EditRouteModalViewModel : RouteDto
    {
        public EditRouteModalViewModel()
        {
        }

        public EditRouteModalViewModel(
            List<PostalCodeListDto> availablePostalCodes,
            List<ProductListDto> availableProducts,
            RouteDto route
            )
        {
            AvailablePostalCodes = availablePostalCodes;
            AvailableProducts = availableProducts;
            Id = route.Id;
            Name = route.Name;
            Products = route.Products;
            PostalCodes = route.PostalCodes;
        }

        public List<PostalCodeListDto> AvailablePostalCodes { get; set; }

        public List<ProductListDto> AvailableProducts { get; set; }
    }
}