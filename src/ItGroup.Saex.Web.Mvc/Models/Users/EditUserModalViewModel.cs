using System.Collections.Generic;
using System.Linq;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Roles.Dto;
using ItGroup.Saex.Users.Dto;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.Models.Users
{
    public class EditUserModalViewModel
    {
        public EditUserModalViewModel()
        {
            AgentTypes = new EnumerationModel(Type);
        }

        public AgentType? AgentType { get; set; }

        public EnumerationModel AgentTypes { get; set; }

        public PagedResultDto<AuditLogListDto> AuditLogs { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }

        public UserDto User { get; set; }

        public bool CanResetPassword { get; set; }

        private AgentType Type { get; set; }

        public bool UserIsInRole(RoleDto role)
        {
            return User.RoleNames != null && User.RoleNames.Any(r => r == role.NormalizedName);
        }
    }
}
