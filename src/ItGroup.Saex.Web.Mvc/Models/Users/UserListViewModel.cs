using System.Collections.Generic;
using ItGroup.Saex.Roles.Dto;
using ItGroup.Saex.Users.Dto;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.Models.Users
{
    public class UserListViewModel
    {
        public UserListViewModel()
        {
            AgentTypes = new EnumerationModel(AgentType);
        }

        public AgentType AgentType { get; set; }

        public EnumerationModel AgentTypes { get; set; }

        public bool CanModify { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }

        public IReadOnlyList<UserDto> Users { get; set; }
    }
}