﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ContactTypeListViewModel
    {
        public ContactTypeListViewModel()
        {
        }

        public ContactTypeListViewModel(PagedResultDto<ContactTypeDto> contactTypes)
        {
            ContactTypes = contactTypes;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<ContactTypeDto> ContactTypes { get; set; }
    }
}
