﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class VisitPlaceListViewModel : VisitPlaceDto
    {
        public VisitPlaceListViewModel()
        {
        }

        public VisitPlaceListViewModel(PagedResultDto<VisitPlaceListDto> visitPlaces)
        {
            VisitPlaces = visitPlaces;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<VisitPlaceListDto> VisitPlaces { get; set; }
    }
}
