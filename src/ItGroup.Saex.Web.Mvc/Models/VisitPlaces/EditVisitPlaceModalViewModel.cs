﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(VisitPlaceDto))]
    public class EditVisitPlaceModalViewModel : VisitPlaceDto
    {
    }
}