﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(SystemSubState))]
    public class EditSystemSubStateModalViewModel : SystemSubStateUpdateDto
    {
        public EditSystemSubStateModalViewModel(SystemSubStateDto input)
        {
            this.Id = input.Id;
            this.Code = input.Code;
            this.Description = input.Description;
        }
    }
}