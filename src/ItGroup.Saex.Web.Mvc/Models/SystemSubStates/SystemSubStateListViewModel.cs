﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class SystemSubStateListViewModel : SystemSubStateCreateDto
    {
        public SystemSubStateListViewModel()
        {
            SystemSubStates = new PagedResultDto<SystemSubStateListDto>();
        }

        public SystemSubStateListViewModel(PagedResultDto<SystemSubStateListDto> systemSubStates)
        {
            SystemSubStates = systemSubStates;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<SystemSubStateListDto> SystemSubStates { get; set; }
    }
}
