﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class CaseBaselinesViewModel : CaseImportDto
    {
        public CaseBaselinesViewModel()
        {
        }

        public CaseBaselinesViewModel(PagedResultDto<CaseBaseline> cases)
        {
            Cases = cases;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<CaseBaseline> Cases { get; set; }
    }
}
