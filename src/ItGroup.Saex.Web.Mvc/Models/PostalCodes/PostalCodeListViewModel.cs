﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class PostalCodeListViewModel
    {
        public PostalCodeListViewModel()
        {
        }

        public PostalCodeListViewModel(PagedResultDto<PostalCodeListDto> postalCodes)
        {
            PostalCodes = postalCodes;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<PostalCodeListDto> PostalCodes { get; set; }
    }
}
