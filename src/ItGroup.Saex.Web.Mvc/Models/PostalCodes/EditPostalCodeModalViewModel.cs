﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(PostalCodeDto))]
    public class EditPostalCodeModalViewModel : PostalCodeDto
    {
    }
}