﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class BucketListViewModel : BucketCreateDto
    {
        public BucketListViewModel()
        {
            Products = new List<ProductListDto>();
        }

        public BucketListViewModel(PagedResultDto<BucketListDto> buckets)
        {
            Buckets = buckets;
        }

        public PagedResultDto<BucketListDto> Buckets { get; set; }

        public bool CanModify { get; set; }

        public List<ProductListDto> Products { get; set; }
    }
}
