﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(BucketDto))]
    public class EditBucketModalViewModel : BucketUpdateDto
    {
        public EditBucketModalViewModel()
        {
            Products = new List<ProductListDto>();
        }

        public List<ProductListDto> Products { get; set; }
    }
}