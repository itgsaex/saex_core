﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class DelinquencyStateListViewModel : DelinquencyStateDto
    {
        public DelinquencyStateListViewModel()
        {
        }

        public DelinquencyStateListViewModel(PagedResultDto<DelinquencyStateListDto> delinquencyStates)
        {
            DelinquencyStates = delinquencyStates;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<DelinquencyStateListDto> DelinquencyStates { get; set; }
    }
}
