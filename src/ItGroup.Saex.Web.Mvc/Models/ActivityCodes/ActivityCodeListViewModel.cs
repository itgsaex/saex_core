﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ActivityCodeListViewModel : ActivityCodeCreateDto
    {
        public ActivityCodeListViewModel() : base()
        {
            Question = new QuestionCreateDto();

            ActivityCodeTypes = new EnumerationModel(ActivityCodeType);

            QuestionTypes = new EnumerationModel(QuestionType);
        }

        public ActivityCodeListViewModel(PagedResultDto<ActivityCodeListDto> activityCodes) : base()
        {
            ActivityCodes = activityCodes;

            Question = new QuestionCreateDto();

            ActivityCodeTypes = new EnumerationModel(ActivityCodeType);

            QuestionTypes = new EnumerationModel(QuestionType);
        }

        public PagedResultDto<ActivityCodeListDto> ActivityCodes { get; set; }

        public EnumerationModel ActivityCodeTypes { get; set; }

        public bool CanModify { get; set; }

        public QuestionCreateDto Question { get; set; }

        public EnumerationModel QuestionTypes { get; set; }

        private QuestionType QuestionType { get; set; }
    }
}
