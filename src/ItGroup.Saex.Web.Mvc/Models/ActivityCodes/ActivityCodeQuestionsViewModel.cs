﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ActivityCodeQuestionsViewModel
    {
        public ActivityCodeQuestionsViewModel()
        {
        }

        public string Description { get; set; }

        public List<QuestionParameterCreateDto> Parameters { get; set; }

        public List<QuestionDto> Questions { get; set; }
    }
}
