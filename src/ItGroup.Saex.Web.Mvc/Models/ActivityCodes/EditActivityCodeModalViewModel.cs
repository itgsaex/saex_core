﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(ActivityCodeDto))]
    public class EditActivityCodeModalViewModel : ActivityCodeDto
    {
        public EditActivityCodeModalViewModel() : base()
        {
            Question = new QuestionDto();

            ActivityCodeTypes = new EnumerationModel(Type);

            QuestionTypes = new EnumerationModel(QuestionType);
        }

        public EnumerationModel ActivityCodeTypes { get; set; }

        public QuestionDto Question { get; set; }

        public EnumerationModel QuestionTypes { get; set; }

        private QuestionType QuestionType { get; set; }
    }
}
