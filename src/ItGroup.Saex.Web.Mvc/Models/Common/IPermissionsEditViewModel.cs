﻿using System.Collections.Generic;
using ItGroup.Saex.Roles.Dto;

namespace ItGroup.Saex.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}