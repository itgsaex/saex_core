﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(SystemState))]
    public class EditSystemStateModalViewModel : SystemStateUpdateDto
    {
        public EditSystemStateModalViewModel()
        {
            MacroStates = new List<MacroStateListDto>();
            SubStates = new List<SystemSubStateListDto>();
        }

        public EditSystemStateModalViewModel(SystemStateDto input)
        {
            this.Id = input.Id;
            this.Code = input.Code;
            this.Description = input.Description;
            this.MacroStateId = input.MacroState.Id;

            if (input.SubState != null)
                this.SubStateId = input.SubState.Id;
        }

        public List<MacroStateListDto> MacroStates { get; set; }

        public List<SystemSubStateListDto> SubStates { get; set; }
    }
}