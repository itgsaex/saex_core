﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class SystemStateListViewModel : SystemStateCreateDto
    {
        public SystemStateListViewModel()
        {
            MacroStates = new List<MacroStateListDto>();
            SubStates = new List<SystemSubStateListDto>();
        }

        public SystemStateListViewModel(PagedResultDto<SystemStateDto> systemStates)
        {
            SystemStates = systemStates;
        }

        public bool CanModify { get; set; }

        public List<MacroStateListDto> MacroStates { get; set; }

        public List<SystemSubStateListDto> SubStates { get; set; }

        public PagedResultDto<SystemStateDto> SystemStates { get; set; }
    }
}
