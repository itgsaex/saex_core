﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Models;

namespace ItGroup.Saex.Web.ViewModels
{
    public class InternalAgentDashboardViewModel
    {
        public InternalAgentDashboardViewModel()
        {
            PromiseTypes = new List<string>();
            PromiseTypes.Add("Firme");
            PromiseTypes.Add("Dudosa");
        }

        public List<string> PromiseTypes { get; set; }

        public long? SelectedExternalAgent { get; set; }
        public List<AgentDto> ExternalAgents { get; set; }

        public int? SelectedRegion { get; set; }
        public List<RegionDto> Regions { get; set; }

        public int? SelectedRoute { get; set; }
        public List<RouteDto> Routes { get; set; }

        public string SelectedPostalCode { get; set; }
        public List<PostalCodeDto> PostalCodes { get; set; }

        public string SelectedActivityCode { get; set; }
        public List<ActivityCodeDto> ActivityCodes { get; set; }

        public string SelectedDelinquencyStage { get; set; }
        public List<string> DelinquencyStages { get; set; }

        public int? SelectedAlert { get; set; }
        public List<Alert> Alerts { get; set; }

        public string SelectedStatus { get; set; }
        public List<string> Status { get; set; }

        public string SelectedEstado { get; set; }
        public List<string> Estados { get; set; }

        public string AccountNumber { get; set; }
        public string Name { get; set; }

        public long? InternalAgentId { get; set; }

        public List<InternalAgentDashboardModel> InternalAgentCases { get; set; }
    }


    public class PortfolioSupervisorViewModel
    {
        public PortfolioSupervisorViewModel()
        {
            AsignationTypes = new List<string>();
            AsignationTypes.Add("Transfer");
            AsignationTypes.Add("Shared");
        }

        public long? SelectedInternalAgent { get; set; }
        public long? SelectedToInternalAgent { get; set; }
        public List<AgentDto> InternalAgents { get; set; }

        public long? SelectedExternalAgent { get; set; }
        public List<AgentDto> ExternalAgents { get; set; }

        public int? SelectedRegion { get; set; }
        public List<RegionDto> Regions { get; set; }

        public int? SelectedRoute { get; set; }
        public List<RouteDto> Routes { get; set; }

        public string SelectedPostalCode { get; set; }
        public List<PostalCodeDto> PostalCodes { get; set; }

        public string SelectedDelinquencyStage { get; set; }
        public List<string> DelinquencyStages { get; set; }

        public string AccountNumber { get; set; }
        public string Name { get; set; }

        public long? SupervisorId { get; set; }

        public string SelectedAsignationType { get; set; }
        public List<string> AsignationTypes { get; set; }

        public List<InternalAgentDashboardModel> Cases { get; set; }

        public List<Guid> SelectedCases { get; set; }
    }

}