﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ProductRuleListViewModel : ProductRule
    {
        public ProductRuleListViewModel()
        {
            Products = new List<ProductListDto>();
        }

        public ProductRuleListViewModel(PagedResultDto<ProductRuleListDto> productRules)
        {
            ProductRules = productRules;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<ProductRuleListDto> ProductRules { get; set; }

        public List<ProductListDto> Products { get; set; }
    }
}
