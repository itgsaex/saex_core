﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(ProductRuleDto))]
    public class EditProductRuleModalViewModel : ProductRuleUpdateDto
    {
        public EditProductRuleModalViewModel()
        {
            Products = new List<ProductListDto>();
        }

        public List<ProductListDto> Products { get; set; }
    }
}