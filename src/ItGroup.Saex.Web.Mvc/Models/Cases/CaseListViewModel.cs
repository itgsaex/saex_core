﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class CaseListViewModel : CaseImportDto
    {
        public CaseListViewModel()
        {
        }

        public CaseListViewModel(PagedResultDto<CaseListDto> cases)
        {
            Cases = cases;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<CaseListDto> Cases { get; set; }
    }
}
