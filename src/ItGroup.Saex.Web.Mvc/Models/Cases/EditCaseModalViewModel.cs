﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(CaseDto))]
    public class EditCaseModalViewModel
    {
        public EditCaseModalViewModel()
        {
        }

        public EditCaseModalViewModel(CaseDto @case)
        {
            Case = @case;
        }

        public CaseDto Case { get; set; }
    }
}
