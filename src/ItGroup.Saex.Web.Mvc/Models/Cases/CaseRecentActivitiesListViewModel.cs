﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class CaseRecentActivitiesListViewModel
    {
        public CaseRecentActivitiesListViewModel()
        {
        }

        public CaseRecentActivitiesListViewModel(PagedResultDto<CaseRecentActivityListDto> activities)
        {
            Activities = activities;
        }

        public PagedResultDto<CaseRecentActivityListDto> Activities { get; set; }
    }
}
