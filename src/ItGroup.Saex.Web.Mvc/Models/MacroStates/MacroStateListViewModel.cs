﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class MacroStateListViewModel : MacroStateCreateDto
    {
        public MacroStateListViewModel()
        {
            MacroStates = new PagedResultDto<MacroStateListDto>();
        }

        public MacroStateListViewModel(PagedResultDto<MacroStateListDto> macroStates)
        {
            MacroStates = macroStates;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<MacroStateListDto> MacroStates { get; set; }
    }
}
