﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(MacroState))]
    public class EditMacroStateModalViewModel : MacroStateUpdateDto
    {
        public EditMacroStateModalViewModel(MacroStateDto input)
        {
            this.Id = input.Id;
            this.Code = input.Code;
            this.Description = input.Description;
        }
    }
}