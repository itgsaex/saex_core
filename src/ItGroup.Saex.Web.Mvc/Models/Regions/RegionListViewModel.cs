﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class RegionListViewModel : RegionCreateDto
    {
        public RegionListViewModel()
        {
        }

        public RegionListViewModel(
            PagedResultDto<RegionListDto> regions,
            List<RouteListDto> availableRoutes
            )
        {
            Regions = regions;
            AvailableRoutes = availableRoutes ?? new List<RouteListDto>();
        }

        public List<RouteListDto> AvailableRoutes { get; set; }

        public bool CanModify { get; set; }

        public PagedResultDto<RegionListDto> Regions { get; set; }
    }
}
