﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(RegionDto))]
    public class EditRegionModalViewModel : RegionDto
    {
        public EditRegionModalViewModel()
        {
        }

        public EditRegionModalViewModel(
            List<RouteListDto> availableRoutes,
            RegionDto region
            )
        {
            AvailableRoutes = availableRoutes ?? new List<RouteListDto>();
            Id = region.Id;
            Name = region.Name;
            ForReport = region.ForReport;
            Routes = region.Routes ?? new List<RouteDto>();
        }

        public List<RouteListDto> AvailableRoutes { get; set; }
    }
}