﻿using System.Collections.Generic;
using System.Linq;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Models;

namespace ItGroup.Saex.Web.ViewModels
{
    public class UpdateRunViewModel
    {
        public UpdateRunViewModel()
        {
        }

        public UpdateRunViewModel(
            List<InterfacelogModel> interfaceLogs,
            List<ErrorlogModel> errorLogs
            )
        {
            InterfaceLogs = interfaceLogs;
            ErrorLogs = errorLogs;
        }

        public List<InterfacelogModel> InterfaceLogs { get; set; }

        public List<ErrorlogModel> ErrorLogs { get; set; }

        public string InitialDate
        {
            get
            {
                string retval = "";
                
                if (InterfaceLogs != null && InterfaceLogs.Count > 0)
                {
                    var intitialDate = InterfaceLogs.OrderBy(i => i.InterfaceDate).FirstOrDefault();

                    if (intitialDate != null)
                    {
                        retval = intitialDate.InterfaceDate.ToString("g");
                    }
                }

                return retval;
            }
        }

        public string FinalDate
        {
            get
            {
                string retval = "";

                if (InterfaceLogs != null && InterfaceLogs.Count > 0)
                {
                    var finalDate = InterfaceLogs.OrderByDescending(i => i.InterfaceDate).FirstOrDefault();

                    if (finalDate != null)
                    {
                        retval = finalDate.InterfaceDate.ToString("g");
                    }
                }

                return retval;
            }
        }
    }
}