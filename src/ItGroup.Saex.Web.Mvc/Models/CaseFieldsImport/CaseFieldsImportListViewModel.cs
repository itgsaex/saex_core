﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Enumerations;
using ItGroup.Saex.Models;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Web.Startup;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class CaseFieldsImportListViewModel : CaseFieldsImportDto
    {
        public CaseFieldsImportListViewModel()
        {
        }

        public CaseFieldsImportListViewModel(
            List<CaseTypeDto> caseTypes,
            List<CaseFieldHelperDto> caseFields, 
            List<Category> categories)
        {
            CaseTypes = caseTypes;
            CaseFields = caseFields;
            SelectedCaseTypeId = Guid.Empty;
            Categories = categories;
        }

        public bool CanModify { get; set; }

        public List<CaseFieldHelperDto> CaseFields { get; set; }

        public List<CaseTypeDto> CaseTypes { get; set; }

        public Guid? SelectedCaseTypeId { get; set; }

        public List<Category> Categories { get; set; }
    }
}
