﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ListsViewModel
    {
        public ListsViewModel()
        {
        }

        public ListsViewModel(PagedResultDto<ListDto> lists)
        {
            Lists = lists;
        }

        public ItGroup.Saex.Enumerations.Enumerations.ListType ListType { get; set; }
        public string PageName { get; set; }

        public string Title { get; set; }

        public bool CanModify { get; set; }

        public PagedResultDto<ListDto> Lists { get; set; }
    }
}
