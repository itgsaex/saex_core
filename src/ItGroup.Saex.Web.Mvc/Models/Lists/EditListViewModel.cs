﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class EditListViewModel
    {
        public EditListViewModel()
        {
        }

        public EditListViewModel(ListDto list, List<CaseFieldHelper> validField, 
                                 List<ItGroup.Saex.DomainEntities.ProductSystem.ProductSystem> productSystems,
                                 List<EstadoRiesgo> estadoRiesgos)
        {
            List = list;
            ValidField = validField;
            ProductSystems = productSystems;
            EstadoRiesgos = estadoRiesgos;
            ProductSystemTable = list.ProductSystemTable;
            EstadoRiesgoId = list.EstadoRiesgoId;
        }

        public ListDto List { get; set; }
        public List<ItGroup.Saex.DomainEntities.ProductSystem.ProductSystem> ProductSystems { get; set; }
        public List<EstadoRiesgo> EstadoRiesgos { get; set; }
        public string ProductSystemTable { get; set; }
        public int EstadoRiesgoId { get; set; }

        public Enumerations.Enumerations.ListType ListType { get; set; }

        public List<CaseFieldHelper> ValidField { get; set; }

    }
}
