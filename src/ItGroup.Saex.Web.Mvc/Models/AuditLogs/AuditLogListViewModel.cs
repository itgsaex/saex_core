﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AuditLogListViewModel : AuditLogDto
    {
        public AuditLogListViewModel()
        {
        }

        public AuditLogListViewModel(PagedResultDto<AuditLogListDto> auditLogs)
        {
            AuditLogs = auditLogs;
        }

        public PagedResultDto<AuditLogListDto> AuditLogs { get; set; }
    }
}