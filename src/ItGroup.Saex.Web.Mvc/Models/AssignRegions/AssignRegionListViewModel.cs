﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AssignRegionListViewModel : AssignRegionCreateDto
    {
        public AssignRegionListViewModel()
        {
        }

        public AssignRegionListViewModel(
            PagedResultDto<AssignRegionDto> assignedRegions,
            List<RegionListDto> availableRegions
            )
        {
            AssignedRegions = assignedRegions;
            AvailableRegions = availableRegions ?? new List<RegionListDto>();
        }

        public PagedResultDto<AssignRegionDto> AssignedRegions { get; set; }

        public List<RegionListDto> AvailableRegions { get; set; }

        public bool CanModify { get; set; }
    }
}
