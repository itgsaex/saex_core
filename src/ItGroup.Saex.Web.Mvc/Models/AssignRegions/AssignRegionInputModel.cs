﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(AssignRegionDto))]
    public class AssignRegionInputModel
    {
        public AssignRegionInputModel()
        {
        }

        public List<Guid> SelectedRegions { get; set; }
    }
}