﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(AssignRegionDto))]
    public class AssignRegionModalViewModel : AssignRegionDto
    {
        public AssignRegionModalViewModel()
        {
            SelectedRegions = new List<RegionDto>();
            AvailableRegions = new List<RegionListDto>();
            Agents = new PagedResultDto<AgentDto>();
            AvailableLists = new List<ListDto>();
            SelectedLists = new List<ListDto>();
        }

        public PagedResultDto<AgentDto> Agents { get; set; }

        public List<RegionListDto> AvailableRegions { get; set; }

        public List<RegionDto> SelectedRegions { get; set; }

        public List<ListDto> AvailableLists { get; set; }

        public List<ListDto> SelectedLists { get; set; }

        public List<WorkingHourDto> AvailableWorkingHours { get; set; }

        public List<WorkingHourDto> SelectedWorkingHours { get; set; }
    }
}