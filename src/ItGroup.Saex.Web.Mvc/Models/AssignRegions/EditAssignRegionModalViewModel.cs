﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(AssignRegionDto))]
    public class EditAssignRegionModalViewModel : AssignRegionDto
    {
        public EditAssignRegionModalViewModel()
        {
            AvailableRegions = new List<RegionListDto>();
            AvailableLists = new List<ListTableDto>();
        }

        public List<RegionListDto> AvailableRegions { get; set; }

        public List<ListTableDto> AvailableLists { get; set; }

        public List<WorkingHourDto> AvailableWorkingHours { get; set; }

        public List<WorkingHourDto> SelectedWorkingHours { get; set; }
    }
}