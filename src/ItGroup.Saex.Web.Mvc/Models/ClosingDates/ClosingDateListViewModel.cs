﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ClosingDateListViewModel
    {
        public ClosingDateListViewModel()
        {
        }

        public ClosingDateListViewModel(PagedResultDto<ClosingDateListDto> closingDates)
        {
            ClosingDates = closingDates;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<ClosingDateListDto> ClosingDates { get; set; }
    }
}
