﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(ClosingDateDto))]
    public class EditClosingDateModalViewModel : ClosingDateDto
    {
        private EditClosingDateModalViewModel()
        {
            Types = new EnumerationModel(Type).Members;
        }

        public List<EnumerationModel> Types { get; set; }
    }
}