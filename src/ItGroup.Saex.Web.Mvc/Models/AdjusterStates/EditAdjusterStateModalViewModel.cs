﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(AdjusterState))]
    public class EditAdjusterStateModalViewModel : AdjusterStateUpdateDto
    {
        public EditAdjusterStateModalViewModel()
        {
            MacroStates = new List<MacroStateListDto>();
        }

        public EditAdjusterStateModalViewModel(AdjusterStateDto input)
        {
            this.Id = input.Id;
            this.Code = input.Code;
            this.Description = input.Description;
            this.MacroStateId = input.MacroState.Id;
        }

        public List<MacroStateListDto> MacroStates { get; set; }
    }
}