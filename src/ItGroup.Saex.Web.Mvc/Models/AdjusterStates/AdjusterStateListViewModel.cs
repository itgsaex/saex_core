﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Web.Models;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AdjusterStateListViewModel : AdjusterStateCreateDto
    {
        public AdjusterStateListViewModel()
        {
            MacroStates = new List<MacroStateListDto>();
        }

        public AdjusterStateListViewModel(PagedResultDto<AdjusterStateDto> adjusterStates)
        {
            AdjusterStates = adjusterStates;
        }

        public PagedResultDto<AdjusterStateDto> AdjusterStates { get; set; }

        public bool CanModify { get; set; }

        public List<MacroStateListDto> MacroStates { get; set; }
    }
}
