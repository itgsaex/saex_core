﻿using Abp.AutoMapper;
using EnumsNET;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class EditWorkingHourModalViewModel 
    {
        public EditWorkingHourModalViewModel()
        {
            HoursList = new List<string>();
            HoursList.Add("12:00 AM");
            HoursList.Add("12:30 AM");
            HoursList.Add("01:00 AM");
            HoursList.Add("01:30 AM");
            HoursList.Add("02:00 AM");
            HoursList.Add("02:30 AM");
            HoursList.Add("03:00 AM");
            HoursList.Add("03:30 AM");
            HoursList.Add("04:00 AM");
            HoursList.Add("04:30 AM");
            HoursList.Add("05:00 AM");
            HoursList.Add("05:30 AM");
            HoursList.Add("06:00 AM");
            HoursList.Add("06:30 AM");
            HoursList.Add("07:00 AM");
            HoursList.Add("07:30 AM");
            HoursList.Add("08:00 AM");
            HoursList.Add("08:30 AM");
            HoursList.Add("09:00 AM");
            HoursList.Add("09:30 AM");
            HoursList.Add("10:00 AM");
            HoursList.Add("10:30 AM");
            HoursList.Add("11:00 AM");
            HoursList.Add("11:30 AM");
            HoursList.Add("12:00 PM");
            HoursList.Add("12:30 PM");
            HoursList.Add("01:00 PM");
            HoursList.Add("01:30 PM");
            HoursList.Add("02:00 PM");
            HoursList.Add("02:30 PM");
            HoursList.Add("03:00 PM");
            HoursList.Add("03:30 PM");
            HoursList.Add("04:00 PM");
            HoursList.Add("04:30 PM");
            HoursList.Add("05:00 PM");
            HoursList.Add("05:30 PM");
            HoursList.Add("06:00 PM");
            HoursList.Add("06:30 PM");
            HoursList.Add("07:00 PM");
            HoursList.Add("07:30 PM");
            HoursList.Add("08:00 PM");
            HoursList.Add("08:30 PM");
            HoursList.Add("09:00 PM");
            HoursList.Add("09:30 PM");
            HoursList.Add("10:00 PM");
            HoursList.Add("10:30 PM");
            HoursList.Add("11:00 PM");
            HoursList.Add("11:30 PM");
        }

        public List<string> HoursList { get; set; }

        public string FridayEndTime { get; set; }

        public string FridayStartTime { get; set; }

        public string MondayEndTime { get; set; }

        public string MondayStartTime { get; set; }

        public string Name { get; set; }

        public string SaturdayEndTime { get; set; }

        public string SaturdayStartTime { get; set; }

        public string SundayEndTime { get; set; }

        public string SundayStartTime { get; set; }

        public string ThursdayEndTime { get; set; }

        public string ThursdayStartTime { get; set; }

        public string TuesdayEndTime { get; set; }

        public string TuesdayStartTime { get; set; }

        public string WednesdayEndTime { get; set; }

        public string WednesdayStartTime { get; set; }

        public Guid Id { get; set; }
    }
}