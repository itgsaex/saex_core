﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ScorecardParameterListViewModel : ScorecardParameterDto
    {
        public ScorecardParameterListViewModel()
        {
        }

        public ScorecardParameterListViewModel(PagedResultDto<ScorecardParameterListDto> scorecardParameters)
        {
            ScorecardParameters = scorecardParameters;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<ScorecardParameterListDto> ScorecardParameters { get; set; }
    }
}
