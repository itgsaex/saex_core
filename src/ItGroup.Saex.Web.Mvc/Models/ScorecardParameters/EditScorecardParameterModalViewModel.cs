﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(ScorecardParameterDto))]
    public class EditScorecardParameterModalViewModel : ScorecardParameterDto
    {
    }
}