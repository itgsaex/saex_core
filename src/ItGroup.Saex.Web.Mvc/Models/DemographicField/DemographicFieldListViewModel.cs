﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    public class DemographicFieldListViewModel : DemographicFieldCreateDto
    {
        public DemographicFieldListViewModel()
        {
            IsActive = true;
        }

        public DemographicFieldListViewModel(PagedResultDto<DemographicFieldListDto> demographicFields, List<DemographicFieldTypeListDto> demographicFieldTypes) : this()
        {
            DemographicFields = demographicFields;
            DemographicFieldTypes = demographicFieldTypes;
        }

        public PagedResultDto<DemographicFieldListDto> DemographicFields { get; set; }

        public bool CanModify { get; set; }
        public List<DemographicFieldTypeListDto> DemographicFieldTypes { get; set; }
    }
}
