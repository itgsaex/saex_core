﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(DemographicFieldDto))]
    public class EditDemographicFieldModalViewModel : DemographicFieldDto
    {
        public EditDemographicFieldModalViewModel(List<DemographicFieldTypeListDto> demographicFieldTypes, DemographicFieldDto demographicField)
        {
            DemographicFieldTypes = demographicFieldTypes;

            DemographicFieldTypeId = demographicField.DemographicFieldTypeId;
            Id = demographicField.Id;
            IsActive = demographicField.IsActive;
            IsRequired = demographicField.IsRequired;
            Label = demographicField.Label;
            Name = demographicField.Name;
            Properties = demographicField.Properties;
            SortOrder = demographicField.SortOrder;
        }

        public List<DemographicFieldTypeListDto> DemographicFieldTypes { get; set; }
    }
}
