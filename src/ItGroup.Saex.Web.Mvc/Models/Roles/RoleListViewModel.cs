﻿using System.Collections.Generic;
using ItGroup.Saex.Roles.Dto;

namespace ItGroup.Saex.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public bool CanModify { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }

        public IReadOnlyList<RoleListDto> Roles { get; set; }
    }
}
