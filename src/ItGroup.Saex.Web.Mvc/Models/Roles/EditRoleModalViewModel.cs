﻿using Abp.AutoMapper;
using ItGroup.Saex.Roles.Dto;
using ItGroup.Saex.Web.Models.Common;
using ItGroup.Saex.Authorization;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class EditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool HasPermission(FlatPermissionDto permission)
        {
            return HasPermission(permission.Name);
        }

        public bool HasPermission(string permissionName)
        {
            return GrantedPermissionNames.Contains(permissionName);
        }
    }
}
