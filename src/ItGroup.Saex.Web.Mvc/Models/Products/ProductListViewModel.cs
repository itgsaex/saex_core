﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class ProductListViewModel
    {
        public ProductListViewModel()
        {
        }

        public ProductListViewModel(PagedResultDto<ProductListDto> products)
        {
            Products = products;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<ProductListDto> Products { get; set; }
    }
}
