﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Models;

namespace ItGroup.Saex.Web.ViewModels
{

    public class DateType
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class DailyReportViewModel
    {
        public DailyReportViewModel()
        {
            DateTypes = new List<DateType>();
            DateTypes.Add(new DateType() { Id = 1, Description = "Ayer" });
            DateTypes.Add(new DateType() { Id = 2, Description = "Esta semana hasta hoy" });
            DateTypes.Add(new DateType() { Id = 3, Description = "Este mes hasta hoy" });
            DateTypes.Add(new DateType() { Id = 4, Description = "Seleccionar Fecha" });
        }

        public long? SelectedExternalAgent { get; set; }
        public List<AgentDto> ExternalAgents { get; set; }

        public int SelectedDateType { get; set; }

        public List<DateType> DateTypes { get; set; }

        [DataType(DataType.Date)]
        public DateTime? FromDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ToDate { get; set; }
    }

    public class ProyeccionPerdidaViewModel
    {
        public ProyeccionPerdidaViewModel()
        {
            EtapasDelincuencia = new List<string>();
        }

        public string SelectedEtapaDelincuencia { get; set; }
        public List<string> EtapasDelincuencia { get; set; }
    }
}