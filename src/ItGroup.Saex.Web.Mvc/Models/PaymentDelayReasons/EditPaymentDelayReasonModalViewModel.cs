﻿using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    [AutoMapFrom(typeof(PaymentDelayReasonDto))]
    public class EditPaymentDelayReasonModalViewModel : PaymentDelayReasonDto
    {
    }
}