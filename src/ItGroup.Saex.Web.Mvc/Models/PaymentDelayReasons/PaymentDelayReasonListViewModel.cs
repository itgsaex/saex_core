﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class PaymentDelayReasonListViewModel
    {
        public PaymentDelayReasonListViewModel()
        {
        }

        public PaymentDelayReasonListViewModel(PagedResultDto<PaymentDelayReasonListDto> paymentDelayReasons)
        {
            PaymentDelayReasons = paymentDelayReasons;
        }

        public bool CanModify { get; set; }

        public PagedResultDto<PaymentDelayReasonListDto> PaymentDelayReasons { get; set; }
    }
}
