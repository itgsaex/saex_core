﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AssignCaseListViewModel : AssignCaseCreateDto
    {
        public AssignCaseListViewModel()
        {
            FromAgent = new AgentDto();
            FromAgentId = null;
            ToAgent = new AgentDto();
            ToAgentId = null;
            FromAgentCases = new List<CaseListDto>();
            ToAgentCases = new List<CaseListDto>();
            Agents = new List<AgentListDto>();
            AsignationTypes = new List<string>();
            AsignationTypes.Add("Transfer");
            AsignationTypes.Add("Shared");
        }

        public AssignCaseListViewModel(
            AgentDto fromAgent,
            List<CaseListDto> fromAgentCases,
            AgentDto toAgent,
            List<CaseListDto> toAgentCases,
            List<AgentListDto> agents
            )
        {
            FromAgentCases = fromAgentCases ?? new List<CaseListDto>();
            FromAgent = fromAgent;
            FromAgentId = FromAgent.Id;
            ToAgentCases = toAgentCases ?? new List<CaseListDto>();
            ToAgent = toAgent;
            ToAgentId = ToAgent.Id;
            Agents = agents;
            AsignationTypes = new List<string>();
            AsignationTypes.Add("Transfer");
            AsignationTypes.Add("Shared");
        }

        public List<AgentListDto> Agents { get; set; }

        public bool CanModify { get; set; }

        public AgentDto FromAgent { get; set; }

        public Guid? FromAgentId { get; set; }

        public List<CaseListDto> FromAgentCases { get; set; }

        public AgentDto ToAgent { get; set; }

        public Guid? ToAgentId { get; set; }

        public List<CaseListDto> ToAgentCases { get; set; }
        public string SelectedAsignationType { get; set; }

        public List<string> AsignationTypes { get; set; }

    }
}
