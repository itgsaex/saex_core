﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.ViewModels
{
    public class EditAssignCaseModalViewModel : AssignCaseDto
    {
        public EditAssignCaseModalViewModel()
        {
            AvailableCases = new List<CaseListDto>();
        }

        public List<CaseListDto> AvailableCases { get; set; }
    }
}
