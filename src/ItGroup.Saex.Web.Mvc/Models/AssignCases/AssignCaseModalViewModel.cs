﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.ViewModels
{
    public class AssignCaseModalViewModel : AssignCaseDto
    {
        public AssignCaseModalViewModel()
        {
            FromAgent = new AgentDto();
            ToAgent = new AgentDto();
            TransferedCases = new List<CaseListDto>();
            ToAgentCases = new List<CaseListDto>();
            Agents = new List<AgentListDto>();
        }

        public AssignCaseModalViewModel(
            AgentDto fromAgent,
            List<CaseListDto> transferedCases,
            AgentDto toAgent,
            List<CaseListDto> toAgentCases,
            List<AgentListDto> agents
            )
        {
            TransferedCases = transferedCases ?? new List<CaseListDto>();
            FromAgent = fromAgent;
            ToAgentCases = toAgentCases ?? new List<CaseListDto>();
            ToAgent = toAgent;
            Agents = agents;
        }

        public List<AgentListDto> Agents { get; set; }

        public AgentDto FromAgent { get; set; }

        public AgentDto ToAgent { get; set; }

        public List<CaseListDto> ToAgentCases { get; set; }

        public List<CaseListDto> TransferedCases { get; set; }
    }
}
