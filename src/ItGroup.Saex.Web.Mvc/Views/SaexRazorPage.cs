﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace ItGroup.Saex.Web.Views
{
    public abstract class SaexRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected SaexRazorPage()
        {
            LocalizationSourceName = SaexConsts.LocalizationSourceName;
        }
    }
}
