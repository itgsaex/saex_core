﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace ItGroup.Saex.Web.Views
{
    public abstract class SaexViewComponent : AbpViewComponent
    {
        protected SaexViewComponent()
        {
            LocalizationSourceName = SaexConsts.LocalizationSourceName;
        }
    }
}
