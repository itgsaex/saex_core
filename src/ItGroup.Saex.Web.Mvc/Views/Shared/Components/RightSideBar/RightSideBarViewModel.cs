﻿using ItGroup.Saex.Configuration.Ui;

namespace ItGroup.Saex.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
