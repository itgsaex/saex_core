using Abp.AutoMapper;
using ItGroup.Saex.Sessions.Dto;

namespace ItGroup.Saex.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
