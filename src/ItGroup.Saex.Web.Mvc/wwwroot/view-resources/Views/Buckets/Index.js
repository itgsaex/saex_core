﻿(function () {
    $(function () {
        var _bucketService = abp.services.app.bucket;
        var _$modal = $('#BucketCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshBucketList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-bucket', function (e) {
            var bucketId = $(this).attr("data-bucket-id");
            var bucketName = $(this).attr('data-bucket-name');

            deleteBucket(bucketId, bucketName);
        });

        $('#myTable').on('click', '.edit-bucket', function (e) {
            var bucketId = $(this).attr("data-bucket-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'Buckets/EditBucketModal?id=' + bucketId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#BucketEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var bucket = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _bucketService.create(bucket).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new bucket!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshBucketList() {
            location.reload(true); //reload page to see new bucket!
        }

        function deleteBucket(bucketId, bucketName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), bucketName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _bucketService.delete(bucketId).done(function () {
                            refreshBucketList();
                        });
                    }
                }
            );
        }
    });
})();