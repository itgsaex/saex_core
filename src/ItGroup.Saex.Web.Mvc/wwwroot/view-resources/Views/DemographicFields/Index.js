﻿window.itg = window.itg || {};

window.itg.form = window.itg.form || {};

window.itg.form.fields = window.itg.form.fields || {};

window.itg.form.fields = {
    checkbox: {
        setValue: function (selector, checked) {
            const element = typeof selector === 'string' ? document.querySelector(selector) : selector;

            element.checked = !!checked;
        }
    },
    text: {
        setValue: function (selector, value) {
            const element = typeof selector === 'string' ? document.querySelector(selector) : selector;

            element.value = value;
            element.dispatchEvent(new Event('focus'));
            element.dispatchEvent(new Event('blur'));
        }
    },
    select: {
        setValue: function (selector, value) {
            const element = typeof selector === 'string' ? document.querySelector(selector) : selector;
            const $element = $(element);

            $element
                .val(value)
                .selectpicker('render');
            element.dispatchEvent(new Event('change'));
        }
    }
};

window.itg.removeOption = function () {
    event.target.closest('div.row').remove();
};

window.itg.addOption = function (fieldTypeId, newOption) {
    const field = document.querySelector('div[data-prop-name="' + fieldTypeId + '-Options"]');
    const options = field.querySelector('[data-options]');

    if (!newOption) {
        // Get the option value from the UI.
        const input = field.querySelector('input');

        if (!input.value) {
            window.alert('You must enter an option value.');
            input.focus();
            return;
        }

        newOption = input.value.trim();

        if (options.querySelector('[data-option="' + newOption + '"]')) {
            window.alert('There\'s already an option with a value of "' + newOption + '"');
            input.focus();
            return;
        }

        input.value = '';
        input.focus();
    }

    const option = document.querySelector('template[data-multiple-option-template]').content.firstElementChild.cloneNode(true);

    option.innerHTML = option.innerHTML.replace(/{optionValue}/gi, newOption);

    options.appendChild(option);
};

(function () {
    const _demographicFieldService = abp.services.app.demographicField;
    const $modal = $('#DemographicFieldModal');
    const $form = $modal.find('form');
    const form = $form[0];

    document.querySelector('button[data-target="#DemographicFieldModal"]').addEventListener('click', function () {
        // New button clicked
        const fields = form.querySelectorAll('input, select, textarea');

        $modal.attr('data-mode', 'create');

        // Clear all fields to leave the modal ready for new values.
        document.querySelector('div[data-options]').innerHTML = '';

        for (let i = 0; i < fields.length; i++) {
            const field = fields[i];

            if (field.tagName === 'SELECT')
                itg.form.fields.select.setValue(field, '');
            else if (field.tagName === 'INPUT')
                switch (field.type) {
                    case 'checkbox':
                        itg.form.fields.checkbox.setValue(field, field.hasAttribute('data-default') && field.getAttribute('data-default') === 'true');
                        break;
                    default:
                        itg.form.fields.text.setValue(field, '');
                        break;
                }
            else
                field.value = '';
        }
    });

    $('#myTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $('.btn.dropdown-toggle.btn-default.bs-placeholder').fadeOut();

    let func = function (e) {
        // Delete button clicked.
        const demographicFieldId = e.target.getAttribute('data-demographic-field-id');
        const demographicFieldName = e.target.getAttribute('data-demographic-field-name');

        deleteDemographicField(demographicFieldId, demographicFieldName);
    };

    let elements = document.querySelectorAll('.delete-demographic-field');
    for (let i = 0; i < elements.length; i++)
        elements[i].addEventListener('click', func);

    func = function (e) {
        // Edit button clicked.
        const demographicFieldId = this.getAttribute('data-demographic-field-id');

        e.preventDefault();

        $.ajax({
            url: abp.appPath + 'DemographicFields/EditDemographicFieldModal?id=' + demographicFieldId,
            type: 'GET',
            contentType: 'application/json',
            success: function (content) {
                const demField = content.result;

                $modal.attr('data-mode', 'edit');

                // Clear any possible options left from another field.
                document.querySelector('div[data-options]').innerHTML = '';

                // Initialize fixed fields.
                itg.form.fields.text.setValue('#Id', demField.id);
                itg.form.fields.text.setValue('#Name', demField.name);
                itg.form.fields.text.setValue('#Label', demField.label);
                itg.form.fields.text.setValue('#SortOrder', demField.sortOrder);
                itg.form.fields.checkbox.setValue('#IsActive', demField.isActive);
                itg.form.fields.checkbox.setValue('#IsRequired', demField.isRequired);
                itg.form.fields.select.setValue('#DemographicFieldTypeId', demField.demographicFieldTypeId);

                if (demField.properties)
                    // Initialize dynamic fields.
                    for (let prop in demField.properties) {
                        const propId = demField.demographicFieldTypeId + '-' + prop;
                        const propElement = document.getElementById(propId);

                        switch (propElement.tagName) {
                            case 'INPUT':
                                if (propElement.type === 'checkbox')
                                    itg.form.fields.checkbox.setValue(propElement, demField.properties[prop]);
                                else if (propElement.hasAttribute('data-options-input')) {
                                    const opts = demField.properties[prop];

                                    for (let optIdx = 0; optIdx < opts.length; optIdx++)
                                        itg.addOption(demField.demographicFieldTypeId, opts[optIdx]);
                                }
                                else
                                    itg.form.fields.text.setValue(propElement, demField.properties[prop]);
                                break;
                        }
                    }

                $modal.modal('show');
            },
            error: function () { }
        });
    };

    elements = document.querySelectorAll('.edit-demographic-field');
    for (let i = 0; i < elements.length; i++)
        elements[i].addEventListener('click', func);

    form.querySelector('button[type="submit"]').addEventListener('click', e => {
        e.preventDefault();

        if (!$form.valid())
            return;

        const demographicField = $form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        let saveMethod;

        if ($modal.attr('data-mode') === 'create') {
            delete demographicField.Id;
            saveMethod = _demographicFieldService.create;
        } else
            saveMethod = _demographicFieldService.update;

        // Boolean properties have the wrong values.
        const boolProps = $form[0].querySelectorAll('input[type="checkbox"][data-regular-property]');
        for (let i = 0; i < boolProps.length; i++)
            demographicField[boolProps[i].name] = boolProps[i].checked;

        demographicField.Properties = {};

        const fields = document.querySelectorAll('div[data-field="' + demographicField.DemographicFieldTypeId + '"]');

        for (let i = 0; i < fields.length; i++) {
            const property = fields[i].querySelector('div[data-property]');
            let value = null;

            switch (property.getAttribute('data-property-type')) {
                case 'bool':
                    value = property.querySelector('input').checked;
                    break;
                case 'string[]':
                    value = [];
                    const options = property.querySelectorAll('[data-option]');

                    for (let j = 0; j < options.length; j++)
                        value.push(options[j].getAttribute('data-option'));

                    if (!value.length) {
                        window.alert('You must enter at least one option.');
                        return;
                    }

                    break;
                case 'int':
                    value = property.querySelector('input').value;
                    value = value ? parseInt(value) : null;

                    break;
                default:
                    value = property.querySelector('input').value;

                    if (!value)
                        value = null;

                    break;
            }

            if (value === null)
                continue;

            demographicField.Properties[property.getAttribute('data-property')] = value;
        }

        abp.ui.setBusy($modal);
        saveMethod(demographicField).done(function () {
            $modal.modal('hide');
            location.reload(true); //reload page to see new demographic field!
        }).always(function () {
            abp.ui.clearBusy($modal);
        });
    });

    $modal.on('shown.bs.modal', function () {
        $modal.find('input:not([type=hidden]):first').focus();
    });

    document.getElementById('DemographicFieldTypeId').addEventListener('change', () => {
        const fieldTypeId = document.getElementById('DemographicFieldTypeId').value;
        const form = document.querySelector('form[name="demographicFieldForm"]');

        // Hide fields belonging to different field types.
        let fields = form.querySelectorAll('[data-field]');
        for (let i = 0; i < fields.length; i++)
            fields[i].classList.add('hidden');

        // Get fields for the current field type.
        fields = form.querySelectorAll('[data-field="' + fieldTypeId + '"]');

        for (let i = 0; i < fields.length; i++)
            fields[i].classList.remove('hidden');
    });

    function refreshDemographicFieldList() {
        location.reload(true); //reload page to see new demographic field!
    }

    function deleteDemographicField(demographicFieldId, demographicFieldName) {
        abp.message.confirm(
            abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), demographicFieldName),
            function (isConfirmed) {
                if (isConfirmed) {
                    _demographicFieldService.delete(demographicFieldId).done(function () {
                        refreshDemographicFieldList();
                    });
                }
            }
        );
    }
})();
