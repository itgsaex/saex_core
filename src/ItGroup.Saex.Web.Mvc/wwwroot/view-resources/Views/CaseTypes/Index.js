﻿(function () {
    $(function () {
        var _caseTypeService = abp.services.app.caseType;
        var _$modal = $('#CaseTypeCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshCaseTypeList();
        });

        $('.delete-caseType').click(function () {
            var caseTypeId = $(this).attr("data-caseType-id");
            var caseTypeName = $(this).attr('data-caseType-name');

            deleteCaseType(caseTypeId, caseTypeName);
        });

        $('.edit-caseType').click(function (e) {
            var caseTypeId = $(this).attr("data-caseType-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'CaseTypes/EditCaseTypeModal?id=' + caseTypeId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#CaseTypeEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        $('#dtDynamicVerticalScrollExample').DataTable({
            "scrollY": "50vh",
            "scrollCollapse": true,
        });
        $('.dataTables_length').addClass('bs-select');

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var caseType = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _caseTypeService.create(caseType).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new caseType!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshCaseTypeList() {
            location.reload(true); //reload page to see new caseType!
        }

        function deleteCaseType(caseTypeId, caseTypeName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), caseTypeName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        //debugger;
                        _caseTypeService.delete(caseTypeId).done(function () {
                            refreshCaseTypeList();
                        });
                    }
                }
            );
        }
    });
})();
