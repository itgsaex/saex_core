﻿(function () {
    $(function () {
        var _$modal = $('#AgentInternalAgentModal');
        var _$container = $('#searchContainer');
        var _$form = _$container.find('form');
        var _internalAgentId = getUrlParameter("InternalAgent");

        var isSuccess = getUrlParameter("success");

        if (isSuccess) {
            abp.notify.success("Agents were assigned sucessfully!");
        }

        _$form.validate({
        });

        $('#add-agents').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'AssignExternalAgents/AssignAgents?internalAgentId=' + _internalAgentId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#AssignExternalAgentsModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        $('.delete-agent').click(function () {
            var agentId = $(this).attr("data-agent-id");
            var agentName = $(this).attr('data-agent-name');

            deleteExternalAgentFromInternalAgent(agentId, agentName);
        });

        $('#internalAgent').on('change', function () {
            var internalAgent = $("#internalAgent option:selected").val();
            window.location.href = "AssignExternalAgents?InternalAgent=" + internalAgent;
        });

        function deleteExternalAgentFromInternalAgent(agentId, agentName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), agentName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        $.ajax({
                            url: abp.appPath + 'AssignExternalAgents/RemoveAgent?internalAgentId=' + _internalAgentId + '&externalAgentId=' + agentId,
                            type: 'POST',
                            contentType: 'json',
                            success: function () {
                                window.location.href = "AssignExternalAgents?InternalAgent=" + _internalAgentId;
                            },
                            error: function (e) { }
                        });
                    }
                }
            );
        }
    });
})();
