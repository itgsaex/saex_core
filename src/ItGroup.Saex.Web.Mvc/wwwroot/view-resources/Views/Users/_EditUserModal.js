﻿(function ($) {
    var _userService = abp.services.app.user;
    var _$modal = $('#UserEditModal');
    var _$form = $('form[name=UserEditForm]');
    $('.selectpicker').selectpicker();

    $(function () {
        var checkbox = $("#PasswordReset");
        var hidden = $("#div01");
        hidden.hide();
        checkbox.change(function () {
            if (checkbox.is(':checked')) {
                hidden.show();
                $('#PasswordResetCode').prop('required', true); //to add required
            } else {
                hidden.hide();
                $("#PasswordResetCode").val("");
                $('#PasswordResetCode').prop('required', false); //to remove required
            }
        });
    });


    function save() {
        if (!_$form.valid()) {
            return;
        }

        var user = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        user.roleNames = [];

        var _$roleCheckboxes = $("input[name='role']:checked");
        if (_$roleCheckboxes) {
            for (var roleIndex = 0; roleIndex < _$roleCheckboxes.length; roleIndex++) {
                var _$roleCheckbox = $(_$roleCheckboxes[roleIndex]);
                user.roleNames.push(_$roleCheckbox.val());
            }
        }

        abp.ui.setBusy(_$form);
        _userService.update(user).done(function () {
            abp.message.success(abp.localization.localize('UserUpdatedSuccessfully', 'Saex'), abp.localization.localize('Updated', 'Saex'));
            setTimeout(() => {
                _$modal.modal('hide');
                location.reload(true); //reload page to see edited user!
            }, 500);
            
        }).always(function () {
            abp.ui.clearBusy(_$modal);
        });
    }

    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $('.checkbox-elem-edit').click(function (e) {
        if (e.target.defaultValue === "AGENT" && e.target.checked) {
            $('#agent-settings-edit').attr('style', 'display: block');
        } else if (e.target.defaultValue === "AGENT" && !e.target.checked) {
            $('#agent-settings-edit').attr('style', 'display: none');
        }
    });
   
    $.AdminBSB.input.activate(_$form);

    _$modal.on('shown.bs.modal', function () {
        _$form.find('input[type=text]:first').focus();

        $('.checkbox-elem-edit').each(function (i, e) {
            if (e.defaultValue === "AGENT" && e.checked) {
                $('#agent-settings-edit').attr('style', 'display: block');
            } else if (e.defaultValue === "AGENT" && !e.checked) {
                $('#agent-settings-edit').attr('style', 'display: none');
            }
        });
    });
})(jQuery);