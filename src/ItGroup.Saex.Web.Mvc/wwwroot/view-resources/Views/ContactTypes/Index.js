﻿(function () {
    $(function () {
        var _contactTypeService = abp.services.app.contactType;
        var _$modal = $('#ContactTypeCreateModal');
        var _$form = _$modal.find('form');

        $('.colorpicker').colorpicker();

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshContactTypeList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-contactType', function (e) {
            var contactTypeId = $(this).attr("data-contactType-id");
            var contactTypeName = $(this).attr('data-contactType-name');

            deleteContactType(contactTypeId, contactTypeName);
        });

        $('#myTable').on('click', '.edit-contactType', function (e) {
            var contactTypeId = $(this).attr("data-contactType-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'ContactTypes/EditContactTypeModal?id=' + contactTypeId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#ContactTypeEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var contactType = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _contactTypeService.create(contactType).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new contactType!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshContactTypeList() {
            location.reload(true); //reload page to see new contactType!
        }

        function deleteContactType(contactTypeId, contactTypeName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), contactTypeName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        //debugger;
                        _contactTypeService.delete(contactTypeId).done(function () {
                            refreshContactTypeList();
                        });
                    }
                }
            );
        }
    });
})();
