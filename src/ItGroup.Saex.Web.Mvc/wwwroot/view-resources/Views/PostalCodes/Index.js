﻿(function () {
    $(function () {
        var _postalCodeService = abp.services.app.postalCode;
        var _$modal = $('#PostalCodeCreateModal');
        var _$form = _$modal.find('form');

        function getSearchTerm(name) {

            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }
        
        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshPostalCodeList();
        });

        $('#myTable').DataTable({
            "bPaginate": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-postalCode', function (e) {
            var postalCodeId = $(this).attr("data-postalCode-id");
            var postalCodeName = $(this).attr('data-postalCode-name');

            deletePostalCode(postalCodeId, postalCodeName);
        });

        $('#myTable').on('click', '.edit-postalCode', function (e) {
            var postalCodeId = $(this).attr("data-postalCode-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'PostalCodes/EditPostalCodeModal?id=' + postalCodeId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#PostalCodeEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var postalCode = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _postalCodeService.create(postalCode).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new postalCode!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshPostalCodeList() {
            location.reload(true); //reload page to see new postalCode!
        }

        function deletePostalCode(postalCodeId, postalCodeName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), postalCodeName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        //debugger;
                        _postalCodeService.delete(postalCodeId).done(function () {
                            refreshPostalCodeList();
                        });
                    }
                }
            );
        }
    });
})();
