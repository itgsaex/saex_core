﻿(function () {
    $(function () {
        var _systemStateService = abp.services.app.systemState;
        var _$modal = $('#SystemStateCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshSystemStateList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-systemState', function (e) {
            var systemStateId = $(this).attr("data-systemState-id");
            var systemStateName = $(this).attr('data-systemState-name');

            deleteSystemState(systemStateId, systemStateName);
        });

        $('#myTable').on('click', '.edit-systemState', function (e) {
            var systemStateId = $(this).attr("data-systemState-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'SystemStates/EditSystemStateModal?id=' + systemStateId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#SystemStateEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var systemState = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _systemStateService.create(systemState).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new systemState!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshSystemStateList() {
            location.reload(true); //reload page to see new systemState!
        }

        function deleteSystemState(systemStateId, systemStateName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), systemStateName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _systemStateService.delete(systemStateId).done(function () {
                            refreshSystemStateList();
                        });
                    }
                }
            );
        }
    });
})();