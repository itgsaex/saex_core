﻿(function () {
    $(function () {
        var _conditionService = abp.services.app.condition;
        var _$modal = $('#ConditionCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshConditionList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-condition', function (e) {
            var conditionId = $(this).attr("data-condition-id");
            var conditionName = $(this).attr('data-condition-name');

            deleteCondition(conditionId, conditionName);
        });

        $('#myTable').on('click', '.edit-condition', function (e) {
            var conditionId = $(this).attr("data-condition-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'Conditions/EditConditionModal?id=' + conditionId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#ConditionEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var condition = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _conditionService.create(condition).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new condition!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshConditionList() {
            location.reload(true); //reload page to see new condition!
        }

        function deleteCondition(conditionId, conditionName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), conditionName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _conditionService.delete(conditionId).done(function () {
                            refreshConditionList();
                        });
                    }
                }
            );
        }
    });
})();