﻿(function () {
    $(function () {
        var _systemPolicyService = abp.services.app.systemPolicy;
        var _$modal = $('#SystemPolicyCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshSystemPolicyList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-systemPolicy', function (e) {
            var systemPolicyId = $(this).attr("data-systemPolicy-id");
            var systemPolicyName = $(this).attr('data-systemPolicy-name');

            deleteSystemPolicy(systemPolicyId, systemPolicyName);
        });

        $('#myTable').on('click', '.edit-systemPolicy', function (e) {
            var systemPolicyId = $(this).attr("data-systemPolicy-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'SystemPolicies/EditSystemPolicyModal?id=' + systemPolicyId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#SystemPolicyEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var systemPolicy = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _systemPolicyService.create(systemPolicy).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new systemPolicy!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshSystemPolicyList() {
            location.reload(true); //reload page to see new systemPolicy!
        }

        function deleteSystemPolicy(systemPolicyId, systemPolicyName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), systemPolicyName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        //debugger;
                        _systemPolicyService.delete(systemPolicyId).done(function () {
                            refreshSystemPolicyList();
                        });
                    }
                }
            );
        }
    });
})();