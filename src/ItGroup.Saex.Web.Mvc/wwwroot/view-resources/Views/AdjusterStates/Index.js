﻿(function () {
    $(function () {
        var _adjusterStateService = abp.services.app.adjusterState;
        var _$modal = $('#AdjusterStateCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshAdjusterStateList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-adjusterState', function (e) {
            var adjusterStateId = $(this).attr("data-adjusterState-id");
            var adjusterStateName = $(this).attr('data-adjusterState-name');

            deleteAdjusterState(adjusterStateId, adjusterStateName);
        });

        $('#myTable').on('click', '.edit-adjusterState', function (e) {
            var adjusterStateId = $(this).attr("data-adjusterState-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'AdjusterStates/EditAdjusterStateModal?id=' + adjusterStateId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#AdjusterStateEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var adjusterState = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _adjusterStateService.create(adjusterState).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new adjusterState!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshAdjusterStateList() {
            location.reload(true); //reload page to see new adjusterState!
        }

        function deleteAdjusterState(adjusterStateId, adjusterStateName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), adjusterStateName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _adjusterStateService.delete(adjusterStateId).done(function () {
                            refreshAdjusterStateList();
                        });
                    }
                }
            );
        }
    });
})();