﻿(function () {
    $(function () {
        var _systemSubStateService = abp.services.app.systemSubState;
        var _$modal = $('#SystemSubStateCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshSystemSubStateList();
        });

        $('.delete-systemSubState').click(function () {
            var systemSubStateId = $(this).attr("data-systemSubState-id");
            var systemSubStateName = $(this).attr('data-systemSubState-name');

            deleteSystemSubState(systemSubStateId, systemSubStateName);
        });

        $('.edit-systemSubState').click(function (e) {
            var systemSubStateId = $(this).attr("data-systemSubState-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'SystemSubStates/EditSystemSubStateModal?id=' + systemSubStateId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#SystemSubStateEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        $('#dtDynamicVerticalScrollExample').DataTable({
            "scrollY": "50vh",
            "scrollCollapse": true,
        });
        $('.dataTables_length').addClass('bs-select');

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var systemSubState = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _systemSubStateService.create(systemSubState).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new systemSubState!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshSystemSubStateList() {
            location.reload(true); //reload page to see new systemSubState!
        }

        function deleteSystemSubState(systemSubStateId, systemSubStateName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), systemSubStateName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _systemSubStateService.delete(systemSubStateId).done(function () {
                            refreshSystemSubStateList();
                        });
                    }
                }
            );
        }
    });
})();