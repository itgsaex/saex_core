﻿(function () {
    $(function () {
        var _closingDateService = abp.services.app.closingDate;
        var _$modal = $('#ClosingDateCreateModal');
        var _$form = _$modal.find('form');
        $('.date').datepicker({ format: 'yyyy-mm-dd'});

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshClosingDateList();
        });

        $('#myTable').DataTable({
            "bPaginate": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-closingDate', function (e) {
            var closingDateId = $(this).attr("data-closingDate-id");
            var closingDateName = $(this).attr('data-closingDate-name');

            deleteClosingDate(closingDateId, closingDateName);
        });

        $('#myTable').on('click', '.edit-closingDate', function (e) {
            var closingDateId = $(this).attr("data-closingDate-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'ClosingDates/EditClosingDateModal?id=' + closingDateId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#ClosingDateEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var closingDate = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            var start = moment(closingDate.StartDate + ' 00:00:00').format('YYYY-MM-DDTHH:mm:ss');
            var end = moment(closingDate.StartDate + ' 23:59:59').format('YYYY-MM-DDTHH:mm:ss');

            closingDate.StartDate = start;
            closingDate.EndDate = end;

            abp.ui.setBusy(_$modal);
            _closingDateService.create(closingDate).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new closingDate!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            //_$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshClosingDateList() {
            location.reload(true); //reload page to see new closingDate!
        }

        function deleteClosingDate(closingDateId, closingDateName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), ' this record'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _closingDateService.delete(closingDateId).done(function () {
                            refreshClosingDateList();
                        });
                    }
                }
            );
        }
    });
})();