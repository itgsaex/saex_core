﻿(function ($) {
    var _closingDateService = abp.services.app.closingDate;
    var _$modal = $('#ClosingDateEditModal');
    var _$form = $('form[name=ClosingDateEditForm]');

    $('.selectpicker').selectpicker();
    $('.date').datepicker({ format: 'yyyy-mm-dd' });

    function save() {
        if (!_$form.valid()) {
            return;
        }

        var closingDate = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

        var start = moment(closingDate.StartDate + ' 00:00:00').format('YYYY-MM-DDTHH:mm:ss');
        var end = moment(closingDate.StartDate + ' 23:59:59').format('YYYY-MM-DDTHH:mm:ss');

        closingDate.StartDate = start;
        closingDate.EndDate = end;

        abp.ui.setBusy(_$form);
        _closingDateService.update(closingDate).done(function () {
            _$modal.modal('hide');
            location.reload(true); //reload page to see edited closingDate!
        }).always(function () {
            abp.ui.clearBusy(_$modal);
        });
    }

    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $.AdminBSB.input.activate(_$form);

    _$modal.on('shown.bs.modal', function () {
        //_$form.find('input[type=text]:first').focus();
    });
})(jQuery);