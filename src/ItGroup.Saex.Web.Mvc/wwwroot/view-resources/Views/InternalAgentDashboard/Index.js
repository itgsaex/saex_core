﻿(function () {
    $(function () {
        var _internalAgentDashboardService = abp.services.app.internalAgentDashboardService;
        var _$form = $('#searchForm');
        var _save = document.getElementById("save-button");
        var comments = [];
        var promises = [];

        function getSearchTerm(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        function format(d) {
            return '';
        }

        function getColumnValue(tableHeaders, item, index) {
            var headerText = tableHeaders[index].innerText;
            var source = abp.localization.getSource('Saex');

            if (headerText == source('StateCode')) {
                return item.stateCode;
            }
            else if (headerText == source('Route')) {
                return item.ruta;
            }
            else if (headerText == source('DPD')) {
                return item.diasVencimiento;
            }
            else if (headerText == source('Product')) {
                return item.productNumber;
            }
            else if (headerText == source('DelinquencyStage')) {
                return item.etapasDeDelicuencia;
            }
            else if (headerText == source('AccountNumber')) {
                return item.accountNumber;
            }
            else if (headerText == source('Name')) {
                return item.fullname;
            }
            else if (headerText == source('Balance')) {
                return item.owedAmount;
            }
            else if (headerText == source('PaymentAmount')) {
                return item.paymentAmount;
            }
            else if (headerText == source('TotalDLQAmount')) {
                return item.totalDELQAmount;
            }
            else if (headerText == source('EntryDate')) {
                return item.fechaEntradaCACS;
            }
            else if (headerText == source('LastPaymentDate')) {
                return item.lastPaymentDate;
            }
            else if (headerText == source('Estado')) {
                return item.estatusSistema;
            }
            else if (headerText == source('ActivityCode')) {
                return item.lastActivityCode;
            }
            else if (headerText == source('ActivityCodeComment')) {
                return item.lastActivityCodeComment;
            }
            else if (headerText == source('ExternalAgentComment')) {
                return item.comentarioExterno;
            }
            else if (headerText == source('LastComment')) {
                return item.ultimoComentario;
            }
            else if (headerText == source('Comment')) {
                return `<input type="text" caseid="${item.caseID}"  name="comment" value="${item.newComment}" style="width:500px;text-align:left;padding:5px;" />`
            }
            else if (headerText == source('Promise')) {
                var promiseColumn = "";
                if (item.tipoPromesa != "") {
                    if (item.tipoPromesa == "Dudosa") {
                        promiseColumn = `<select name="promise" caseid="${item.caseID}"><option value="Firme">Firme</option><option value="Dudosa" Selected>Dudosa</option></select>`
                    } else {
                        promiseColumn = `<select name="promise" caseid="${item.caseID}"><option value="Firme" Selected>Firme</option><option value="Dudosa">Dudosa</option></select>`
                    }
                }

                return promiseColumn;
            }
        }

        var dataTable = $('#dataTable').DataTable({
            "scrollY": "500px",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "serverSide": false,
            colReorder: true,
            stateSave: true,
            dom: 'Bfrtip',
            buttons: [
                'print', 'csv', 'excel', 'pdf'
            ],
            "columnDefs": [
                { "width": "100px", "targets": 0 },
                { "width": "100px", "targets": 1 },
                { "width": "100px", "targets": 2 },
                { "width": "100px", "targets": 3 },
                { "width": "250px", "targets": 4 },
                { "width": "250px", "targets": 5 },
                { "width": "250px", "targets": 6 },
                { "width": "100px", "targets": 7 },
                { "width": "250px", "targets": 8 },
                { "width": "250px", "targets": 9 },
                { "width": "250px", "targets": 10 },
                { "width": "250px", "targets": 11 },
                { "width": "250px", "targets": 12 },
                { "width": "250px", "targets": 13 },
                { "width": "600px", "targets": 14 },
                { "width": "500px", "targets": 15 },
                { "width": "500px", "targets": 16 },
                { "width": "500px", "targets": 17 },
                { "width": "250px", "targets": 18 }
            ]
        });

        dataTable.on('column-reorder', function (e, settings, details) {
            var model = _$form.serializeFormToObject();
            abp.ui.setBusy($('#container'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'InternalAgentDashboard/Search',
                data: model
            }).done((e, x) => {
                //let table = $("#dataTable").find('tbody');
                let tableHeaders = $("#dataTable thead").find('th');
                //dataTable.destroy();
                //table.empty();
                dataTable.clear().draw();

                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        dataTable.row.add([getColumnValue(tableHeaders, item, 0),
                        getColumnValue(tableHeaders, item, 1),
                        getColumnValue(tableHeaders, item, 2),
                        getColumnValue(tableHeaders, item, 3),
                        getColumnValue(tableHeaders, item, 4),
                        getColumnValue(tableHeaders, item, 5),
                        getColumnValue(tableHeaders, item, 6),
                        getColumnValue(tableHeaders, item, 7),
                        getColumnValue(tableHeaders, item, 8),
                        getColumnValue(tableHeaders, item, 9),
                        getColumnValue(tableHeaders, item, 10),
                        getColumnValue(tableHeaders, item, 11),
                        getColumnValue(tableHeaders, item, 12),
                        getColumnValue(tableHeaders, item, 13),
                        getColumnValue(tableHeaders, item, 14),
                        getColumnValue(tableHeaders, item, 15),
                        getColumnValue(tableHeaders, item, 16),
                        getColumnValue(tableHeaders, item, 17),
                        getColumnValue(tableHeaders, item, 18)
                        ]).draw(false);
                        //items = items.concat(`<tr>
                        //                 <td>${getColumnValue(tableHeaders, item, 0)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 1)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 2)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 3)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 4)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 5)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 6)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 7)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 8)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 9)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 10)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 11)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 12)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 13)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 14)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 15)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 16)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 17)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 18)}</td>
                        //            </tr>`);
                    });
                }
                //table.append(items);
            }).always((e, x) => {
                abp.ui.clearBusy($('#container'));
            });
        });

        $("#search-button").click(() => {
            //var version = $.fn.dataTable.version;
            var model = _$form.serializeFormToObject();
            abp.ui.setBusy($('#container'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'InternalAgentDashboard/Search',
                data: model
            }).done((e, x) => {
                //let table = $("#dataTable").find('tbody');
                let tableHeaders = $("#dataTable thead").find('th');
                //dataTable.destroy();
                //table.empty();
                dataTable.clear().draw();

                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        dataTable.row.add([getColumnValue(tableHeaders, item, 0),
                                         getColumnValue(tableHeaders, item, 1),
                                         getColumnValue(tableHeaders, item, 2),
                                         getColumnValue(tableHeaders, item, 3),
                                         getColumnValue(tableHeaders, item, 4),
                                         getColumnValue(tableHeaders, item, 5),
                                         getColumnValue(tableHeaders, item, 6),
                                         getColumnValue(tableHeaders, item, 7),
                                         getColumnValue(tableHeaders, item, 8),
                                         getColumnValue(tableHeaders, item, 9),
                                         getColumnValue(tableHeaders, item, 10),
                                         getColumnValue(tableHeaders, item, 11),
                                         getColumnValue(tableHeaders, item, 12),
                                         getColumnValue(tableHeaders, item, 13),
                                         getColumnValue(tableHeaders, item, 14),
                                         getColumnValue(tableHeaders, item, 15),
                                         getColumnValue(tableHeaders, item, 16),
                                         getColumnValue(tableHeaders, item, 17),
                                         getColumnValue(tableHeaders, item, 18)
                                    ]).draw(false);
                        //items = items.concat(`<tr>
                        //                 <td>${getColumnValue(tableHeaders, item, 0)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 1)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 2)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 3)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 4)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 5)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 6)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 7)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 8)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 9)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 10)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 11)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 12)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 13)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 14)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 15)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 16)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 17)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 18)}</td>
                        //            </tr>`);
                    });
                }
                //table.append(items);
                //dataTable = $('#dataTable').DataTable({
                //    "scrollY": "500px",
                //    "scrollX": "0px",
                //    "bPaginate": false,
                //    "bFilter": true,
                //    "bSort": true,
                //    "bInfo": true,
                //    "autoWidth": true,
                //    "serverSide": false,
                //    colReorder: true,
                //    stateSave: true,
                //    dom: 'Bfrtip',
                //    buttons: [
                //        'print', 'csv', 'excel', 'pdf'
                //    ],
                //    "columnDefs": [
                //        { "width": "100px", "targets": 0 },
                //        { "width": "100px", "targets": 1 },
                //        { "width": "100px", "targets": 2 },
                //        { "width": "100px", "targets": 3 },
                //        { "width": "250px", "targets": 4 },
                //        { "width": "250px", "targets": 5 },
                //        { "width": "250px", "targets": 6 },
                //        { "width": "100px", "targets": 7 },
                //        { "width": "250px", "targets": 8 },
                //        { "width": "250px", "targets": 9 },
                //        { "width": "250px", "targets": 10 },
                //        { "width": "250px", "targets": 11 },
                //        { "width": "250px", "targets": 12 },
                //        { "width": "250px", "targets": 13 },
                //        { "width": "600px", "targets": 14 },
                //        { "width": "500px", "targets": 15 },
                //        { "width": "500px", "targets": 16 },
                //        { "width": "500px", "targets": 17 },
                //        { "width": "250px", "targets": 18 }
                //    ]
                //});

            }).always((e, x) => {
                abp.ui.clearBusy($('#container'));
            });
        });

        $("#reset-button").click((e) => {
            e.preventDefault();
            dataTable.colReorder.reset();
            
            var model = _$form.serializeFormToObject();
            abp.ui.setBusy($('#container'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'InternalAgentDashboard/Search',
                data: model
            }).done((e, x) => {
                //let table = $("#dataTable").find('tbody');
                let tableHeaders = $("#dataTable thead").find('th');
                //dataTable.destroy();
                //table.empty();
                dataTable.clear().draw();

                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        dataTable.row.add([getColumnValue(tableHeaders, item, 0),
                        getColumnValue(tableHeaders, item, 1),
                        getColumnValue(tableHeaders, item, 2),
                        getColumnValue(tableHeaders, item, 3),
                        getColumnValue(tableHeaders, item, 4),
                        getColumnValue(tableHeaders, item, 5),
                        getColumnValue(tableHeaders, item, 6),
                        getColumnValue(tableHeaders, item, 7),
                        getColumnValue(tableHeaders, item, 8),
                        getColumnValue(tableHeaders, item, 9),
                        getColumnValue(tableHeaders, item, 10),
                        getColumnValue(tableHeaders, item, 11),
                        getColumnValue(tableHeaders, item, 12),
                        getColumnValue(tableHeaders, item, 13),
                        getColumnValue(tableHeaders, item, 14),
                        getColumnValue(tableHeaders, item, 15),
                        getColumnValue(tableHeaders, item, 16),
                        getColumnValue(tableHeaders, item, 17),
                        getColumnValue(tableHeaders, item, 18)
                        ]).draw(false);
                        //items = items.concat(`<tr>
                        //                 <td>${getColumnValue(tableHeaders, item, 0)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 1)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 2)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 3)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 4)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 5)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 6)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 7)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 8)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 9)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 10)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 11)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 12)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 13)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 14)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 15)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 16)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 17)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 18)}</td>
                        //            </tr>`);
                    });
                }
                //table.append(items);
            }).always((e, x) => {
                abp.ui.clearBusy($('#container'));
            });
        });

        _save.addEventListener("click", function () {
            abp.ui.setBusy($('#container'));
            var $caseId = "";
            var $comment = "";
            var $promiseCaseId = "";
            var $promise = "";
            comments = [];
            promises = [];

            var internalAgentId = $("#internalAgent-id").val();

            var rows = $("tbody tr").each(function (row) {
                $cells = $(this).find("td");
               
                $cells.each(function (col) {
                    var input = $(this).find("input");
                    var select = $(this).find("select");
                    switch (col) {
                        case 17:
                            $caseId = input.attr("caseid");
                            $comment = input.val();
                            break;
                        case 18:
                            $promiseCaseId = select.attr("caseid");
                            $promise = select.val();

                        default:
                    }
                });

                if ($comment != "")
                    comments.push({ "caseID": $caseId, "comment": $comment });

                if (typeof $promise !== "undefined" && $promise != "")
                    promises.push({ "caseID": $promiseCaseId, "promise": $promise });
            })

            var request = { "internalAgentId": parseInt(internalAgentId), "comments": comments, "promises":promises}

            _internalAgentDashboardService.saveInternalAgentComments(request).done(function () {
                var model = _$form.serializeFormToObject();
                
                abp.ajax({
                    contentType: 'application/x-www-form-urlencoded',
                    url: abp.appPath + 'InternalAgentDashboard/Search',
                    data: model
                }).done((e, x) => {
                    //let table = $("#dataTable").find('tbody');
                    let tableHeaders = $("#dataTable thead").find('th');
                    //dataTable.destroy();
                    //table.empty();
                    dataTable.clear().draw();

                    let items = "";
                    if (x.result) {
                        x.result.forEach(item => {
                            dataTable.row.add([getColumnValue(tableHeaders, item, 0),
                            getColumnValue(tableHeaders, item, 1),
                            getColumnValue(tableHeaders, item, 2),
                            getColumnValue(tableHeaders, item, 3),
                            getColumnValue(tableHeaders, item, 4),
                            getColumnValue(tableHeaders, item, 5),
                            getColumnValue(tableHeaders, item, 6),
                            getColumnValue(tableHeaders, item, 7),
                            getColumnValue(tableHeaders, item, 8),
                            getColumnValue(tableHeaders, item, 9),
                            getColumnValue(tableHeaders, item, 10),
                            getColumnValue(tableHeaders, item, 11),
                            getColumnValue(tableHeaders, item, 12),
                            getColumnValue(tableHeaders, item, 13),
                            getColumnValue(tableHeaders, item, 14),
                            getColumnValue(tableHeaders, item, 15),
                            getColumnValue(tableHeaders, item, 16),
                            getColumnValue(tableHeaders, item, 17),
                            getColumnValue(tableHeaders, item, 18)
                            ]).draw(false);
                        //items = items.concat(`<tr>
                        //                 <td>${getColumnValue(tableHeaders, item, 0)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 1)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 2)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 3)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 4)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 5)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 6)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 7)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 8)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 9)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 10)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 11)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 12)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 13)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 14)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 15)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 16)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 17)}</td>
                        //                 <td>${getColumnValue(tableHeaders, item, 18)}</td>
                        //            </tr>`);
                        });
                    }
                    //table.append(items);
                    //dataTable = $('#dataTable').DataTable({
                    //    "scrollY": "500px",
                    //    "scrollX": "0px",
                    //    "bPaginate": false,
                    //    "bFilter": true,
                    //    "bSort": true,
                    //    "bInfo": true,
                    //    "autoWidth": true,
                    //    "serverSide": false,
                    //    colReorder: true,
                    //    stateSave: true,
                    //    dom: 'Bfrtip',
                    //    buttons: [
                    //        'print', 'csv', 'excel', 'pdf'
                    //    ],
                    //    "columnDefs": [
                    //        { "width": "100px", "targets": 0 },
                    //        { "width": "100px", "targets": 1 },
                    //        { "width": "100px", "targets": 2 },
                    //        { "width": "100px", "targets": 3 },
                    //        { "width": "250px", "targets": 4 },
                    //        { "width": "250px", "targets": 5 },
                    //        { "width": "250px", "targets": 6 },
                    //        { "width": "100px", "targets": 7 },
                    //        { "width": "250px", "targets": 8 },
                    //        { "width": "250px", "targets": 9 },
                    //        { "width": "250px", "targets": 10 },
                    //        { "width": "250px", "targets": 11 },
                    //        { "width": "250px", "targets": 12 },
                    //        { "width": "250px", "targets": 13 },
                    //        { "width": "600px", "targets": 14 },
                    //        { "width": "500px", "targets": 15 },
                    //        { "width": "500px", "targets": 16 },
                    //        { "width": "500px", "targets": 17 },
                    //        { "width": "250px", "targets": 18 }
                    //    ]
                    //});

                    abp.notify.success("Se grabo exitosamente.");

                })
            }).always(function () {
                abp.ui.clearBusy($('#container'));
            });
        });
    });
})();