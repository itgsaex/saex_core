﻿(function () {
    $(function () {
        var _visitPlaceService = abp.services.app.visitPlace;
        var _$modal = $('#VisitPlaceCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshVisitPlaceList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-visitPlace', function (e) {
            var visitPlaceId = $(this).attr("data-visitPlace-id");
            var visitPlaceName = $(this).attr('data-visitPlace-name');

            deleteVisitPlace(visitPlaceId, visitPlaceName);
        });

        $('#myTable').on('click', '.edit-visitPlace', function (e) {
            var visitPlaceId = $(this).attr("data-visitPlace-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'VisitPlaces/EditVisitPlaceModal?id=' + visitPlaceId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#VisitPlaceEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var visitPlace = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _visitPlaceService.create(visitPlace).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new visitPlace!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshVisitPlaceList() {
            location.reload(true); //reload page to see new visitPlace!
        }

        function deleteVisitPlace(visitPlaceId, visitPlaceName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), visitPlaceName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        //debugger;
                        _visitPlaceService.delete(visitPlaceId).done(function () {
                            refreshVisitPlaceList();
                        });
                    }
                }
            );
        }
    });
})();