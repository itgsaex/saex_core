﻿(function () {
    $(function () {
        var _routeService = abp.services.app.route;
        var _$modal = $('#RouteCreateModal');
        var _$form = _$modal.find('form');
        var selectedProducts = [];
        var selectedPostalCodes = [];

        $('#Products').multiSelect({
            selectableHeader: "<div class='custom-header'>Choose from list</div>",
            selectionHeader: "<div class='custom-header'>Products in route</div>",
            afterSelect: function (array) {
                selectedProducts.push(array[0]);
            },
            afterDeselect: function (array) {
                var index = selectedProducts.indexOf(array[0]);

                if (index > -1) {
                    selectedProducts.splice(index);
                }
                
            }
        });

        $('#PostalCodes').multiSelect({
            selectableHeader: "<div class='custom-header'>Choose from list</div>",
            selectionHeader: "<div class='custom-header'>Postal Codes in route</div>",
            afterSelect: function (array) {
                selectedPostalCodes.push(array[0]);
            },
            afterDeselect: function (array) {
                var index = selectedPostalCodes.indexOf(array[0]);
                if (index > -1) {
                    selectedPostalCodes.splice(index);
                }
            }
        });

        $('.btn.dropdown-toggle.btn-default.bs-placeholder').fadeOut();

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshRouteList();
        });

        $('#myTable').DataTable({
            "bPaginate": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-route', function (e) {
            var routeId = $(this).attr("data-route-id");
            var routeName = $(this).attr('data-route-name');

            deleteRoute(routeId, routeName);
        });

        $('#myTable').on('click', '.edit-route', function (e) {
            var routeId = $(this).attr("data-route-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'Routes/EditRouteModal?id=' + routeId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#RouteEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var route = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            route.Products = selectedProducts;
            route.PostalCodes = selectedPostalCodes;

            abp.ui.setBusy(_$modal);
            _routeService.create(route).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new route!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshRouteList() {
            location.reload(true); //reload page to see new route!
        }

        function deleteRoute(routeId, routeName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), routeName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _routeService.delete(routeId).done(function () {
                            refreshRouteList();
                        });
                    }
                }
            );
        }
    });
})();