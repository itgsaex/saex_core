﻿(function ($) {
    var _routeService = abp.services.app.route;
    var _$modal = $('#RouteEditModal');
    var _$form = $('form[name=RouteEditForm]');
    //var selectedProducts;
    //var selectedPostalCodes;

    function save() {
        if (!_$form.valid()) {
            return;
        }

        var route = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        var model = {
            Id: route.Id,
            Name: route.Name,
            Products: $('#ProductsEdit').val(),
            PostalCodes: $('#PostalCodesEdit').val()
        };

        abp.ui.setBusy(_$form);
        _routeService.update(model).done(function () {
            _$modal.modal('hide');
            location.reload(true); //reload page to see edited route!
        }).always(function () {
            abp.ui.clearBusy(_$modal);
        });
    }

    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $.AdminBSB.input.activate(_$form);

    //On load event for modal
    _$modal.on('shown.bs.modal', function () {
        selectedProducts = [];
        selectedPostalCodes = [];
        console.log()
        //This initializes the Products multi select control...
        $('#ProductsEdit').multiSelect({
            selectableHeader: "<div class='custom-header'>Choose from list</div>",
            selectionHeader: "<div class='custom-header'>Products in route</div>",
            afterSelect: function (array) {
                var index = selectedProducts.indexOf(array[0]);
                if (index === -1) {
                    selectedProducts.push(array[0]);
                }
            },
            afterDeselect: function (array) {
                var index = selectedProducts.indexOf(array[0]);
                if (index > -1) {
                    selectedProducts.splice(index);
                }
            }
        });

        //This initializes the Postal Codes multi select control...
        $('#PostalCodesEdit').multiSelect({
            selectableHeader: "<div class='custom-header'>Choose from list</div>",
            selectionHeader: "<div class='custom-header'>Postal Codes in route</div>",
            afterSelect: function (array) {
                var index = selectedPostalCodes.indexOf(array[0]);
                if (index === -1) {
                    selectedPostalCodes.push(array[0]);
                }
                
            },
            afterDeselect: function (array) {
                var index = selectedPostalCodes.indexOf(array[0]);
                if (index > -1) {
                    selectedPostalCodes.splice(index);
                }
            }
        });

        //This Populates the selected items panel on modal load event...
        $('#SelectedProducts').children("option").each(function (i, e) {
            if (e.value && e.value !== "") {
                $('#ProductsEdit').multiSelect('select', e.value);
            }
        });

        $('#SelectedPostalCodes').children("option").each(function (i, e) {
            if (e.value && e.value !== "") {
                $('#PostalCodesEdit').multiSelect('select', e.value);
            }
        });

        _$form.find('input[type=text]:first').focus();
    });
})(jQuery);