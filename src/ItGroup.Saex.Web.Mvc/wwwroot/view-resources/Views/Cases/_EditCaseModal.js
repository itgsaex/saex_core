﻿(function ($) {
    var _caseService = abp.services.app.case;
    var _$modal = $('#CaseEditModal');
    var _$form = $('form[name=CaseEditForm]');
    $('.selectpicker').selectpicker();


    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $.AdminBSB.input.activate(_$form);

    _$modal.on('shown.bs.modal', function () {
        _$form.find('input[type=text]:first').focus();
    });
})(jQuery);
