﻿(function () {
    $(function () {
        var _caseService = abp.services.app.case;
        var _$modal = $('#CaseImportModal');
        var _$form = _$modal.find('form');

        $("#fileuploader").uploadFile({
            url: "cases/import",
            multiple: false,
            dragDrop: false,
            autoSubmit: true,
            maxFileCount: 1,
            fileName: "file"
        }); 

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshCaseList();
        });

        $('.delete-case').click(function () {
            var caseId = $(this).attr("data-case-id");
            var caseName = $(this).attr('data-case-name');

            deleteCase(caseId, caseName);
        });

        $('.edit-case').click(function (e) {
            //debugger;
            var caseId = $(this).attr("data-case-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'Cases/EditCaseModal?id=' + caseId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#CaseEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        //_$form.find('button[type="submit"]').click(function (e) {
        //    e.preventDefault();

        //    if (!_$form.valid()) {
        //        return;
        //    }

        //    var form = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        //    debugger;
        //    abp.ui.setBusy(_$modal);
        //    _caseService.import(form).done(function () {
        //        _$modal.modal('hide');
        //        location.reload(true); //reload page to see new case!
        //    }).always(function () {
        //        abp.ui.clearBusy(_$modal);
        //    });
        //});

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshCaseList() {
            location.reload(true); //reload page to see new case!
        }

        function deleteCase(caseId, caseName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), caseName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        //debugger;
                        _caseService.delete(caseId).done(function () {
                            refreshCaseList();
                        });
                    }
                }
            );
        }
    });
})();
