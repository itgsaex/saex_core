﻿(function () {
    function Hilitor(id, tag) {

        // private variables
        var targetNode = document.getElementById(id) || document.body;
        var hiliteTag = tag || "MARK";
        var skipTags = new RegExp("^(?:" + hiliteTag + "|SCRIPT|FORM|SPAN)$");
        var colors = ["#ff6", "#a0ffff", "#9f9", "#f99", "#f6f"];
        var wordColor = [];
        var colorIdx = 0;
        var matchRegExp = "";
        var openLeft = false;
        var openRight = false;

        // characters to strip from start and end of the input string
        var endRegExp = new RegExp('^[^\\w]+|[^\\w]+$', "g");

        // characters used to break up the input string into words
        var breakRegExp = new RegExp('[^\\w\'-]+', "g");

        this.setEndRegExp = function (regex) {
            endRegExp = regex;
            return endRegExp;
        };

        this.setBreakRegExp = function (regex) {
            breakRegExp = regex;
            return breakRegExp;
        };

        this.setMatchType = function (type) {
            switch (type) {
                case "left":
                    this.openLeft = false;
                    this.openRight = true;
                    break;

                case "right":
                    this.openLeft = true;
                    this.openRight = false;
                    break;

                case "open":
                    this.openLeft = this.openRight = true;
                    break;

                default:
                    this.openLeft = this.openRight = false;

            }
        };

        this.setRegex = function (input) {
            input = input.replace(endRegExp, "");
            input = input.replace(breakRegExp, "|");
            input = input.replace(/^\||\|$/g, "");
            if (input) {
                var re = "(" + input + ")";
                if (!this.openLeft) {
                    re = "\\b" + re;
                }
                if (!this.openRight) {
                    re = re + "\\b";
                }
                matchRegExp = new RegExp(re, "i");
                return matchRegExp;
            }
            return false;
        };

        this.getRegex = function () {
            var retval = matchRegExp.toString();
            retval = retval.replace(/(^\/(\\b)?|\(|\)|(\\b)?\/i$)/g, "");
            retval = retval.replace(/\|/g, " ");
            return retval;
        };

        // recursively apply word highlighting
        this.hiliteWords = function (node) {
            if (node === undefined || !node) return;
            if (!matchRegExp) return;
            if (skipTags.test(node.nodeName)) return;

            if (node.hasChildNodes()) {
                for (var i = 0; i < node.childNodes.length; i++)
                    this.hiliteWords(node.childNodes[i]);
            }
            if (node.nodeType == 3) { // NODE_TEXT
                if ((nv = node.nodeValue) && (regs = matchRegExp.exec(nv))) {
                    if (!wordColor[regs[0].toLowerCase()]) {
                        wordColor[regs[0].toLowerCase()] = colors[colorIdx++ % colors.length];
                    }

                    var match = document.createElement(hiliteTag);
                    match.appendChild(document.createTextNode(regs[0]));
                    match.style.backgroundColor = wordColor[regs[0].toLowerCase()];
                    match.style.color = "#000";

                    var after = node.splitText(regs.index);
                    after.nodeValue = after.nodeValue.substring(regs[0].length);
                    node.parentNode.insertBefore(match, after);
                }
            };
        };

        // remove highlighting
        this.remove = function () {
            var arr = document.getElementsByTagName(hiliteTag);
            while (arr.length && (el = arr[0])) {
                var parent = el.parentNode;
                parent.replaceChild(el.firstChild, el);
                parent.normalize();
            }
        };

        // start highlighting at target node
        this.apply = function (input) {
            this.remove();
            if (input === undefined || !(input = input.replace(/(^\s+|\s+$)/g, ""))) {
                return;
            }
            if (this.setRegex(input)) {
                this.hiliteWords(targetNode);
            }
            return matchRegExp;
        };

    }
	$(function () {

		var _roleService = abp.services.app.role;
		var _$modal = $('#RoleCreateModal');
		var _$form = _$modal.find('form');

		_$form.validate({
		});

		$('#RefreshButton').click(function () {
			refreshRoleList();
		});

		$('#myTable').DataTable({
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			]
		});

		$('#myTable').on('click', '.delete-role', function (e) {
			var roleId = $(this).attr("data-role-id");
			var roleName = $(this).attr('data-role-name');

			deleteRole(roleId, roleName);
		});
        let myHilitor = new Hilitor("createPermissionsSection");
        myHilitor.setMatchType("open");
        $("#findCretePermission").keyup(function () {
            console.log($("#findCretePermission").val())
            myHilitor.apply($("#findCretePermission").val());
        });
		$('#myTable').on('click', '.edit-role', function (e) {
			var roleId = $(this).attr("data-role-id");

			e.preventDefault();
			$.ajax({
				url: abp.appPath + 'Roles/EditRoleModal?roleId=' + roleId,
				type: 'POST',
				contentType: 'html',
				success: function (content) {
					$('#RoleEditModal div.modal-content').html(content);
				},
				error: function (e) { }
			});
		});

		_$form.find('button[type="submit"]').click(function (e) {
			e.preventDefault();

			if (!_$form.valid()) {
				return;
			}

			var role = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
            role.grantedPermissions = [];
			var _$permissionCheckboxes = $("input[name='permission']:checked");
			if (_$permissionCheckboxes) {
				for (var permissionIndex = 0; permissionIndex < _$permissionCheckboxes.length; permissionIndex++) {
					var _$permissionCheckbox = $(_$permissionCheckboxes[permissionIndex]);
                    role.grantedPermissions.push(_$permissionCheckbox.val());
				}
			}

			abp.ui.setBusy(_$modal);
			_roleService.create(role).done(function () {
				_$modal.modal('hide');
				location.reload(true); //reload page to see new role!
			}).always(function () {
				abp.ui.clearBusy(_$modal);
			});
		});

		_$modal.on('shown.bs.modal', function () {
			_$modal.find('input:not([type=hidden]):first').focus();
		});

		function refreshRoleList() {
			location.reload(true); //reload page to see new role!
		}

		function deleteRole(roleId, roleName) {
			abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), roleName),
				function (isConfirmed) {
					if (isConfirmed) {
						_roleService.delete({
							id: roleId
						}).done(function () {
							refreshRoleList();
						});
					}
				}
			);
		}
	});
})();
