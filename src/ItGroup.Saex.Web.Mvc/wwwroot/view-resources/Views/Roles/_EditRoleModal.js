﻿
(function ($) {
    function Hilitor(id, tag) {

        // private variables
        var targetNode = document.getElementById(id) || document.body;
        var hiliteTag = tag || "MARK";
        var skipTags = new RegExp("^(?:" + hiliteTag + "|SCRIPT|FORM|SPAN)$");
        var colors = ["#ff6", "#a0ffff", "#9f9", "#f99", "#f6f"];
        var wordColor = [];
        var colorIdx = 0;
        var matchRegExp = "";
        var openLeft = false;
        var openRight = false;

        // characters to strip from start and end of the input string
        var endRegExp = new RegExp('^[^\\w]+|[^\\w]+$', "g");

        // characters used to break up the input string into words
        var breakRegExp = new RegExp('[^\\w\'-]+', "g");

        this.setEndRegExp = function (regex) {
            endRegExp = regex;
            return endRegExp;
        };

        this.setBreakRegExp = function (regex) {
            breakRegExp = regex;
            return breakRegExp;
        };

        this.setMatchType = function (type) {
            switch (type) {
                case "left":
                    this.openLeft = false;
                    this.openRight = true;
                    break;

                case "right":
                    this.openLeft = true;
                    this.openRight = false;
                    break;

                case "open":
                    this.openLeft = this.openRight = true;
                    break;

                default:
                    this.openLeft = this.openRight = false;

            }
        };

        this.setRegex = function (input) {
            input = input.replace(endRegExp, "");
            input = input.replace(breakRegExp, "|");
            input = input.replace(/^\||\|$/g, "");
            if (input) {
                var re = "(" + input + ")";
                if (!this.openLeft) {
                    re = "\\b" + re;
                }
                if (!this.openRight) {
                    re = re + "\\b";
                }
                matchRegExp = new RegExp(re, "i");
                return matchRegExp;
            }
            return false;
        };

        this.getRegex = function () {
            var retval = matchRegExp.toString();
            retval = retval.replace(/(^\/(\\b)?|\(|\)|(\\b)?\/i$)/g, "");
            retval = retval.replace(/\|/g, " ");
            return retval;
        };

        // recursively apply word highlighting
        this.hiliteWords = function (node) {
            if (node === undefined || !node) return;
            if (!matchRegExp) return;
            if (skipTags.test(node.nodeName)) return;

            if (node.hasChildNodes()) {
                for (var i = 0; i < node.childNodes.length; i++)
                    this.hiliteWords(node.childNodes[i]);
            }
            if (node.nodeType == 3) { // NODE_TEXT
                if ((nv = node.nodeValue) && (regs = matchRegExp.exec(nv))) {
                    if (!wordColor[regs[0].toLowerCase()]) {
                        wordColor[regs[0].toLowerCase()] = colors[colorIdx++ % colors.length];
                    }

                    var match = document.createElement(hiliteTag);
                    match.appendChild(document.createTextNode(regs[0]));
                    match.style.backgroundColor = wordColor[regs[0].toLowerCase()];
                    match.style.color = "#000";

                    var after = node.splitText(regs.index);
                    after.nodeValue = after.nodeValue.substring(regs[0].length);
                    node.parentNode.insertBefore(match, after);
                }
            };
        };

        // remove highlighting
        this.remove = function () {
            var arr = document.getElementsByTagName(hiliteTag);
            while (arr.length && (el = arr[0])) {
                var parent = el.parentNode;
                parent.replaceChild(el.firstChild, el);
                parent.normalize();
            }
        };

        // start highlighting at target node
        this.apply = function (input) {
            this.remove();
            if (input === undefined || !(input = input.replace(/(^\s+|\s+$)/g, ""))) {
                return;
            }
            if (this.setRegex(input)) {
                this.hiliteWords(targetNode);
            }
            return matchRegExp;
        };

    }
    var _roleService = abp.services.app.role;
    var _$modal = $('#RoleEditModal');
    var _$form = $('form[name=RoleEditForm]');

    function save() {

        if (!_$form.valid()) {
            return;
        }

        var role = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        role.grantedPermissions = [];
        var _$permissionCheckboxes = $("input[name='permission']:checked:visible");
        if (_$permissionCheckboxes) {
            for (var permissionIndex = 0; permissionIndex < _$permissionCheckboxes.length; permissionIndex++) {
                var _$permissionCheckbox = $(_$permissionCheckboxes[permissionIndex]);
                role.grantedPermissions.push(_$permissionCheckbox.val());
            }
        }
        
        abp.ui.setBusy(_$form);
        _roleService.update(role).done(function () {
            _$modal.modal('hide');
            location.reload(true); //reload page to see edited role!
        }).always(function () {
            abp.ui.clearBusy(_$modal);
        });
    }

    let myHilitor = new Hilitor("editPermissionsSection");
    myHilitor.setMatchType("open");
    $("#findEditPermission").keyup(function () {
        myHilitor.apply($("#findEditPermission").val());
    });
    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $.AdminBSB.input.activate(_$form);

    _$modal.on('shown.bs.modal', function () {
        _$form.find('input[type=text]:first').focus();
    });
})(jQuery);