﻿(function () {
    $(function () {
        var _paymentDelayReasonService = abp.services.app.paymentDelayReason;
        var _$modal = $('#PaymentDelayReasonCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshPaymentDelayReasonList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-paymentDelayReason', function (e) {
            var paymentDelayReasonId = $(this).attr("data-paymentDelayReason-id");
            var paymentDelayReasonName = $(this).attr('data-paymentDelayReason-name');

            deletePaymentDelayReason(paymentDelayReasonId, paymentDelayReasonName);
        });

        $('#myTable').on('click', '.edit-paymentDelayReason', function (e) {
            var paymentDelayReasonId = $(this).attr("data-paymentDelayReason-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'PaymentDelayReasons/EditPaymentDelayReasonModal?id=' + paymentDelayReasonId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#PaymentDelayReasonEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var paymentDelayReason = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _paymentDelayReasonService.create(paymentDelayReason).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new paymentDelayReason!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshPaymentDelayReasonList() {
            location.reload(true); //reload page to see new paymentDelayReason!
        }

        function deletePaymentDelayReason(paymentDelayReasonId, paymentDelayReasonName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), paymentDelayReasonName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        //debugger;
                        _paymentDelayReasonService.delete(paymentDelayReasonId).done(function () {
                            refreshPaymentDelayReasonList();
                        });
                    }
                }
            );
        }
    });
})();