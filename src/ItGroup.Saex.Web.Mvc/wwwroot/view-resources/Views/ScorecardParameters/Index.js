﻿(function () {
    $(function () {
        var _scorecardParameterService = abp.services.app.scorecardParameter;
        var _$modal = $('#ScorecardParameterCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshScorecardParameterList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-scorecardParameter', function (e) {
            var scorecardParameterId = $(this).attr("data-scorecardParameter-id");
            var scorecardParameterName = $(this).attr('data-scorecardParameter-name');

            deleteScorecardParameter(scorecardParameterId, scorecardParameterName);
        });

        $('#myTable').on('click', '.edit-scorecardParameter', function (e) {
            var scorecardParameterId = $(this).attr("data-scorecardParameter-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'ScorecardParameters/EditScorecardParameterModal?id=' + scorecardParameterId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#ScorecardParameterEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var scorecardParameter = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _scorecardParameterService.create(scorecardParameter).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new scorecardParameter!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshScorecardParameterList() {
            location.reload(true); //reload page to see new scorecardParameter!
        }

        function deleteScorecardParameter(scorecardParameterId, scorecardParameterName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), scorecardParameterName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _scorecardParameterService.delete(scorecardParameterId).done(function () {
                            refreshScorecardParameterList();
                        });
                    }
                }
            );
        }
    });
})();