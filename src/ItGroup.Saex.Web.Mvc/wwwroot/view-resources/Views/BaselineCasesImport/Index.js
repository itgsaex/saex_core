﻿(function () {
    $(function () {
        let items = [];
        let progressBar = $("#progressBar");
        progressBar.parent().hide();

        $("#fileuploader").change(evt => {
            let selectedFile = evt.target.files[0];
            if (!selectedFile) return;
            let reader = new FileReader();
            reader.onload = (event) => {
                let data = event.target.result;
                items = [];
                let workbook = XLSX.read(data, { type: 'binary', cellDates: true, dateNF: 'm/d/yyyy h:mm AM/PM' });
                workbook.SheetNames.forEach(SheetName => {
                    let XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[SheetName]);
                    XL_row_object.forEach(e => {
                        e.AtrasoTotal = e["Atraso $"];
                        e.Cuenta1 = e["Cuenta 1"];
                        e.Due = e.Due.toUTCString();
                        e.Nest = e.Nest.toUTCString();


                    });
                    items = [...items, ...XL_row_object];
                });
                console.log(items);
            };

            reader.onerror = event => {
                console.log(event.target.error.code);
            };

            reader.readAsBinaryString(selectedFile);
        });

        let JSON_to_URLEncoded = (element, key, list) => {
            var list = list || [];
            if (typeof (element) == 'object') {
                for (var idx in element)
                    JSON_to_URLEncoded(element[idx], key ? key + '[' + idx + ']' : idx, list);
            } else {
                list.push(key + '=' + encodeURIComponent(element));
            }
            return list.join('&');
        }

        let sendImport = data => {
            return new Promise((resolve, reject) => {
                abp.ajax({
                    contentType: 'application/x-www-form-urlencoded',
                    url: abp.appPath + "BaselineCasesImport/Import",
                    data: JSON_to_URLEncoded({ Cases: data })
                }).done((e, x) => {
                    resolve();
                })
            }); 
        };
        $('#importForm').submit(async (e) => {
            e.preventDefault();

            if (!$('#importForm').valid()) {
                return;
            }

            $("#close-btn").prop('disabled', true);
            $("#submit-btn").prop('disabled', true);
            $("#fileuploader").prop('disabled', true);
            progressBar.parent().show();
            let size = 50;
            let arraysOfItems = [];
            for (let i = 0; i < items.length; i += size)
                arraysOfItems.push(items.slice(i, i + size));
            for (let i = 0; i < arraysOfItems.length; i++) {
                try {
                    let response = await sendImport(arraysOfItems[i]);
                    progressBar.width(`${((i + 1) / arraysOfItems.length) * 100}%`);
                    if (arraysOfItems.length - 1 == i)
                        location.reload(true);

                } catch (z) {
                }   
            }
            //abp.ajax({
            //    contentType: 'application/x-www-form-urlencoded',
            //    url: abp.appPath + "BaselineCasesImport/Import",
            //    data: JSON_to_URLEncoded({ Cases: items })
            //}).done((e, x) => {
            //    location.reload(true);
            //}).always((e, x) => {
            //    abp.ui.clearBusy($('#importForm'));
            //});
        });
        

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

    });
})();
