﻿(function () {
    $(function () {
        var _regionService = abp.services.app.region;
        var _$modal = $('#RegionCreateModal');
        var _$form = _$modal.find('form');
        var selectedRoutes = [];

        $('#Routes').multiSelect({
            selectableHeader: "<div class='custom-header'>Choose from list</div>",
            selectionHeader: "<div class='custom-header'>Routes in region</div>",
            afterSelect: function (array) {
                selectedRoutes.push(array[0]);
            },
            afterDeselect: function (array) {
                var index = selectedRoutes.indexOf(array[0]);
                if (index > -1) {
                    selectedRoutes.splice(index);
                }
            }
        });

        $('.btn.dropdown-toggle.btn-default').fadeOut();

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshRegionList();
        });

        $('#myTable').DataTable({
            "bPaginate": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-region', function (e) {
            var regionId = $(this).attr("data-region-id");
            var regionName = $(this).attr('data-region-name');

            deleteRegion(regionId, regionName);
        });

        $('#myTable').on('click', '.edit-region', function (e) {
            var regionId = $(this).attr("data-region-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'Regions/EditRegionModal?id=' + regionId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#RegionEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var region = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            region.Routes = selectedRoutes;
            region.ForReport = $("#ForReport").is(':checked');
   
            abp.ui.setBusy(_$modal);
            _regionService.create(region).done(function () {
                _$modal.modal('hide');
                window.location.href = window.location.origin + window.location.pathname; //reload page to see new region!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshRegionList() {
            window.location.href = window.location.origin + window.location.pathname; //reload page to see new region!
        }

        function deleteRegion(regionId, regionName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), regionName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _regionService.delete(regionId).done(function () {
                            refreshRegionList();
                        });
                    }
                }
            );
        }
    });
})();