﻿(function ($) {
    var _regionService = abp.services.app.region;
    var _$modal = $('#RegionEditModal');
    var _$form = $('form[name=RegionEditForm]');
    //var selectedRoutes = [];

    function save() {
        if (!_$form.valid()) {
            return;
        }

        var region = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        region.ForReport = $("#ForReport").is(':checked');

        var model = {
            Id: region.Id,
            Name: region.Name,
            Routes: $('#RoutesEdit').val(),
            ForReport: region.ForReport
        };

        abp.ui.setBusy(_$form);
        _regionService.update(model).done(function () {
            _$modal.modal('hide');
            location.reload(true); //reload page to see edited region!
        }).always(function () {
            abp.ui.clearBusy(_$modal);
        });
    }

    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $.AdminBSB.input.activate(_$form);

    //On load event for modal
    _$modal.on('shown.bs.modal', function () {
        selectedRoutes = [];

        _$form.find('input[type=text]:first').focus();
        setTimeout(function () { }, 1000);
            
        $('#RoutesEdit').multiSelect({
            selectableHeader: "<div class='custom-header'>Choose from list</div>",
            selectionHeader: "<div class='custom-header'>Routes in region</div>",
            afterSelect: function (array) {
                var index = selectedRoutes.indexOf(array[0]);
                if (index === -1) {
                    selectedRoutes.push(array[0]);
                }
            },
            afterDeselect: function (array) {
                var index = selectedRoutes.indexOf(array[0]);
                if (index > -1) {
                    selectedRoutes.splice(index);
                }
            }
        });

        $('#SelectedRoutes').children("option").each(function (i, e) {
            if (e.value && e.value !== "") {
                $('#RoutesEdit').multiSelect('select', e.value);
            }
        });

    });
})(jQuery);