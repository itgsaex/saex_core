﻿(function () {
    $(function () {
        var _productRuleService = abp.services.app.productRule;
        var _$modal = $('#ProductRuleCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshProductRuleList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-productRule', function (e) {
            var productRuleId = $(this).attr("data-productRule-id");
            var productRuleName = $(this).attr('data-productRule-name');

            deleteProductRule(productRuleId, productRuleName);
        });

        $('#myTable').on('click', '.edit-productRule', function (e) {
            var productRuleId = $(this).attr("data-productRule-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'ProductRules/EditProductRuleModal?id=' + productRuleId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#ProductRuleEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var productRule = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _productRuleService.create(productRule).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new productRule!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshProductRuleList() {
            location.reload(true); //reload page to see new productRule!
        }

        function deleteProductRule(productRuleId, productRuleName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), productRuleName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _productRuleService.delete(productRuleId).done(function () {
                            refreshProductRuleList();
                        });
                    }
                }
            );
        }
    });
})();