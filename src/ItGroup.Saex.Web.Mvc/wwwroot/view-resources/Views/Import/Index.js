﻿(function () {
    $(function () {
        $("#postalCodefileuploader").uploadFile({
            url: "CsvImport/PostalCodes",
            multiple: false,
            dragDrop: false,
            autoSubmit: true,
            maxFileCount: 1,
            fileName: "file"
        });

        $("#caseTypefileuploader").uploadFile({
            url: "CsvImport/CaseTypes",
            multiple: false,
            dragDrop: false,
            autoSubmit: true,
            maxFileCount: 1,
            fileName: "file"
        });

        $("#casefileuploader").uploadFile({
            url: "CsvImport/Cases",
            multiple: false,
            dragDrop: false,
            autoSubmit: true,
            maxFileCount: 1,
            fileName: "file"
        });

        $("#macroStatefileuploader").uploadFile({
            url: "CsvImport/MacroStates",
            multiple: false,
            dragDrop: false,
            autoSubmit: true,
            maxFileCount: 1,
            fileName: "file"
        });

        $("#systemStatefileuploader").uploadFile({
            url: "CsvImport/SystemStates",
            multiple: false,
            dragDrop: false,
            autoSubmit: true,
            maxFileCount: 1,
            fileName: "file"
        });

        $("#systemSubStatefileuploader").uploadFile({
            url: "CsvImport/SystemSubStates",
            multiple: false,
            dragDrop: false,
            autoSubmit: true,
            maxFileCount: 1,
            fileName: "file"
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });
    });
})();
