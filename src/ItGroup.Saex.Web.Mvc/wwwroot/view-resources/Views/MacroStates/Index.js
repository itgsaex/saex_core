﻿(function () {
    $(function () {
        var _macroStateService = abp.services.app.macroState;
        var _$modal = $('#MacroStateCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshMacroStateList();
        });

        $('.delete-macroState').click(function () {
            var macroStateId = $(this).attr("data-macroState-id");
            var macroStateName = $(this).attr('data-macroState-name');

            deleteMacroState(macroStateId, macroStateName);
        });

        $('#dtDynamicVerticalScrollExample').DataTable({
            "scrollY": "50vh",
            "scrollCollapse": true,
        });
        $('.dataTables_length').addClass('bs-select');

        $('.edit-macroState').click(function (e) {
            var macroStateId = $(this).attr("data-macroState-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'MacroStates/EditMacroStateModal?id=' + macroStateId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#MacroStateEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var macroState = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _macroStateService.create(macroState).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new macroState!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshMacroStateList() {
            location.reload(true); //reload page to see new macroState!
        }

        function deleteMacroState(macroStateId, macroStateName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), macroStateName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _macroStateService.delete(macroStateId).done(function () {
                            refreshMacroStateList();
                        });
                    }
                }
            );
        }
    });
})();