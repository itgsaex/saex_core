﻿(function () {
    $(function () {
        var _$form = $('#searchForm');
        var _$transferForm = $('#transferForm');
        var _save = document.getElementById("save-button");
        var selectedCases = [];

        var fromCasesDataTable = $('#fromCasesDataTable').DataTable({
            "scrollY": "250px",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                if (column == 0)
                                    return "";
                                else
                                    return data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                }
            ],
            "columnDefs": [
                { "width": "100px", "targets": 0 },
                { "width": "100px", "targets": 1 },
                { "width": "100px", "targets": 2 },
                { "width": "100px", "targets": 3 },
                { "width": "100px", "targets": 4 },
                { "width": "250px", "targets": 5 },
                { "width": "250px", "targets": 6 },
                { "width": "250px", "targets": 7 },
                { "width": "100px", "targets": 8 },
                { "width": "250px", "targets": 9 },
                { "width": "250px", "targets": 10 },
                { "width": "250px", "targets": 11 },
                { "width": "250px", "targets": 12 },
                { "width": "250px", "targets": 13 },
                { "width": "250px", "targets": 14 },
                { "width": "600px", "targets": 15 },
                { "width": "500px", "targets": 16 },
                { "width": "500px", "targets": 17 }
            ]
        });

        var selectedToAgentCasesDataTable = $('#selectedToAgenteCasesDataTable').DataTable({
            "scrollY": "250px",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                if (column == 5)
                                    return '\u200C' + data;
                                else
                                    return data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                }
            ],
            "columnDefs": [
                { "width": "100px", "targets": 0 },
                { "width": "100px", "targets": 1 },
                { "width": "100px", "targets": 2 },
                { "width": "100px", "targets": 3 },
                { "width": "250px", "targets": 4 },
                { "width": "250px", "targets": 5 },
                { "width": "250px", "targets": 6 },
                { "width": "100px", "targets": 7 },
                { "width": "250px", "targets": 8 },
                { "width": "250px", "targets": 9 },
                { "width": "250px", "targets": 10 },
                { "width": "250px", "targets": 11 },
                { "width": "250px", "targets": 12 },
                { "width": "250px", "targets": 13 },
                { "width": "600px", "targets": 14 },
                { "width": "500px", "targets": 15 },
                { "width": "500px", "targets": 16 }
            ]
        });

        var selectedToAgenteTransferCasesDataTable = $('#selectedToAgenteTransferCasesDataTable').DataTable({
            "scrollY": "250px",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                if (column == 0)
                                    return "";
                                else if (column == 1)
                                    return '\u200C' + data;
                                else
                                    return data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                }
            ],
            "columnDefs": [
                { "width": "100px", "targets": 0 },
                { "width": "250px", "targets": 1 },
                { "width": "250px", "targets": 2 },
                { "width": "250px", "targets": 3 },
                { "width": "250px", "targets": 4 },
                { "width": "250px", "targets": 5 }
            ]
        });

        function getFromAgentCases() {
            var model = _$form.serializeFormToObject();

            if (model.SelectedInternalAgent == "")
                return;

            abp.ui.setBusy($('#fromAgentCases'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'PortfolioSupervisor/Search',
                data: model
            }).done((e, x) => {
                let table = $("#fromCasesDataTable").find('tbody');
                fromCasesDataTable.destroy();
                table.empty();
                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        items = items.concat(`<tr>
                                              <td style="text-align: center;">
                                                <label>
                                                    <input type="checkbox" name="case" value="${item.caseID}" title="" class="filled-in checkbox-elem-edit" id="case-${item.caseID}" />
                                                    <label for="case-${item.caseID}" title=""></label>
                                                </label>
                                              </td>
                                         <td>${item.stateCode}</td>
                                         <td>${item.ruta}</td>
                                         <td>${item.diasVencimiento}</td>
                                         <td>${item.productNumber}</td>
                                         <td>${item.etapasDeDelicuencia}</td>
                                         <td>${item.accountNumber}</td>
                                         <td>${item.fullname}</td>
                                         <td>${item.owedAmount}</td>
                                         <td>${item.paymentAmount}</td>
                                         <td>${item.totalDELQAmount}</td>
                                         <td>${item.fechaEntradaCACS}</td>
                                         <td>${item.lastPaymentDate}</td>
                                         <td>${item.estatusSistema}</td>
                                         <td>${item.lastActivityCode}</td>
                                         <td>${item.lastActivityCodeComment}</td>
                                         <td>${item.comentarioExterno}</td>
                                         <td>${item.ultimoComentario}</td>
                                    </tr>`);
                    });
                }
                table.append(items);
                fromCasesDataTable = $('#fromCasesDataTable').DataTable({
                    "scrollY": "250px",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "autoWidth": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        if (column == 0)
                                            return "";
                                        else if (column == 6)
                                            return '\u200C' + data;
                                        else
                                            return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        }
                    ],
                    "columnDefs": [
                        { "width": "100px", "targets": 0 },
                        { "width": "100px", "targets": 1 },
                        { "width": "100px", "targets": 2 },
                        { "width": "100px", "targets": 3 },
                        { "width": "100px", "targets": 4 },
                        { "width": "250px", "targets": 5 },
                        { "width": "250px", "targets": 6 },
                        { "width": "250px", "targets": 7 },
                        { "width": "100px", "targets": 8 },
                        { "width": "250px", "targets": 9 },
                        { "width": "250px", "targets": 10 },
                        { "width": "250px", "targets": 11 },
                        { "width": "250px", "targets": 12 },
                        { "width": "250px", "targets": 13 },
                        { "width": "250px", "targets": 14 },
                        { "width": "600px", "targets": 15 },
                        { "width": "500px", "targets": 16 },
                        { "width": "500px", "targets": 17 }
                    ]
                });

            }).always((e, x) => {
                abp.ui.clearBusy($('#fromAgentCases'));
            });
        }

        function getToAgentCases() {
            var model = _$transferForm.serializeFormToObject();
            abp.ui.setBusy($('#agentToCases'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'PortfolioSupervisor/SelectedToAgentCases',
                data: model
            }).done((e, x) => {
                let table = $("#selectedToAgenteCasesDataTable").find('tbody');
                selectedToAgentCasesDataTable.destroy();
                table.empty();
                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        items = items.concat(`<tr>
                                         <td>${item.stateCode}</td>
                                         <td>${item.ruta}</td>
                                         <td>${item.diasVencimiento}</td>
                                         <td>${item.productNumber}</td>
                                         <td>${item.etapasDeDelicuencia}</td>
                                         <td>${item.accountNumber}</td>
                                         <td>${item.fullname}</td>
                                         <td>${item.owedAmount}</td>
                                         <td>${item.paymentAmount}</td>
                                         <td>${item.totalDELQAmount}</td>
                                         <td>${item.fechaEntradaCACS}</td>
                                         <td>${item.lastPaymentDate}</td>
                                         <td>${item.estatusSistema}</td>
                                         <td>${item.lastActivityCode}</td>
                                         <td>${item.lastActivityCodeComment}</td>
                                         <td>${item.comentarioExterno}</td>
                                         <td>${item.ultimoComentario}</td>
                                    </tr>`);
                    });
                }
                table.append(items);
                selectedToAgentCasesDataTable = $('#selectedToAgenteCasesDataTable').DataTable({
                    "scrollY": "250px",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        if (column == 5)
                                            return '\u200C' + data;
                                        else
                                            return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        }
                    ],
                    "columnDefs": [
                        { "width": "100px", "targets": 0 },
                        { "width": "100px", "targets": 1 },
                        { "width": "100px", "targets": 2 },
                        { "width": "100px", "targets": 3 },
                        { "width": "250px", "targets": 4 },
                        { "width": "250px", "targets": 5 },
                        { "width": "250px", "targets": 6 },
                        { "width": "100px", "targets": 7 },
                        { "width": "250px", "targets": 8 },
                        { "width": "250px", "targets": 9 },
                        { "width": "250px", "targets": 10 },
                        { "width": "250px", "targets": 11 },
                        { "width": "250px", "targets": 12 },
                        { "width": "250px", "targets": 13 },
                        { "width": "600px", "targets": 14 },
                        { "width": "500px", "targets": 15 },
                        { "width": "500px", "targets": 16 }
                    ]
                });

            }).always((e, x) => {
                abp.ui.clearBusy($('#agentToCases'));
            });

            //Buscar transferidos o compartidos
            abp.ui.setBusy($('#transferCases'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'PortfolioSupervisor/SelectedToAgentTransferCases',
                data: model
            }).done((e, x) => {
                let transferTable = $("#selectedToAgenteTransferCasesDataTable").find('tbody');
                selectedToAgenteTransferCasesDataTable.destroy();
                transferTable.empty();
                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        items = items.concat(`<tr>
                                         <td style="text-align: center;">
                                                <label>
                                                    <input type="checkbox" name="transferCase" value="${item.id}" title="" class="filled-in checkbox-elem-edit" id="case-${item.id}" />
                                                    <label for="case-${item.id}" title=""></label>
                                                </label>
                                              </td>
                                         <td>${item.accountNumber}</td>
                                         <td>${item.name}</td>
                                         <td>${item.city}</td>
                                         <td>${item.zipcode}</td>
                                         <td>${item.productNumber}</td>
                                    </tr>`);
                    });
                }
                transferTable.append(items);
                selectedToAgenteTransferCasesDataTable = $('#selectedToAgenteTransferCasesDataTable').DataTable({
                    "scrollY": "250px",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        if (column == 0)
                                            return "";
                                        else if (column == 1)
                                            return '\u200C' + data;
                                        else
                                            return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        }
                    ],
                    "columnDefs": [
                        { "width": "100px", "targets": 0 },
                        { "width": "250px", "targets": 1 },
                        { "width": "250px", "targets": 2 },
                        { "width": "250px", "targets": 3 },
                        { "width": "250px", "targets": 4 },
                        { "width": "250px", "targets": 5 }
                    ]
                });

            }).always((e, x) => {
                abp.ui.clearBusy($('#transferCases'));
            });
        }

        $("#selectedToAgentComboBox").change(() => {
            getToAgentCases();
        });

        $("#search-button").click(() => {
            getFromAgentCases();
        });

        $("#save-button").click(() => {
            var transferModel = _$transferForm.serializeFormToObject();
            var searchModel = _$form.serializeFormToObject();
            selectedCases = [];

            if (searchModel.SelectedInternalAgent == "")
                return;

            if (transferModel.SelectedToInternalAgent == "")
                return;

            if (searchModel.SelectedInternalAgent == transferModel.SelectedToInternalAgent)
                return;

            if (transferModel.SelectedAsignationType == "")
                return;

            abp.ui.setBusy($('#container'));

            var _$agentCheckboxes = $("input[name='case']:checked");
            if (_$agentCheckboxes) {
                for (var agentIndex = 0; agentIndex < _$agentCheckboxes.length; agentIndex++) {
                    var _$agentCheckbox = $(_$agentCheckboxes[agentIndex]);
                    selectedCases.push(_$agentCheckbox.val());
                }
            }

            if (selectedCases.length == 0) {
                abp.ui.clearBusy($('#container'));
                return;
            }

            $.ajax({
                url: abp.appPath + 'PortfolioSupervisor/Save',
                type: 'POST',
                dataType: "json",
                traditional: true,
                data: { 'SelectedInternalAgent': searchModel.SelectedInternalAgent, 'SelectedToInternalAgent': transferModel.SelectedToInternalAgent, 'SelectedCases': selectedCases, 'SelectedAsignationType': transferModel.SelectedAsignationType },
                success: function (res) {
                    abp.ui.clearBusy($('#container'));

                    getFromAgentCases();
                    getToAgentCases();

                    abp.notify.success("Se transfirieron/compartieron los casos.");
                },
                error: function (e) {
                    abp.ui.clearBusy($('#container'));
                }
            });
        });

        $("#delete-button").click(() => {
            var transferModel = _$transferForm.serializeFormToObject();
            var searchModel = _$form.serializeFormToObject();
            selectedCases = [];

            if (transferModel.SelectedToInternalAgent == "")
                return;

            abp.ui.setBusy($('#container'));

            var _$agentCheckboxes = $("input[name='transferCase']:checked");
            if (_$agentCheckboxes) {
                for (var agentIndex = 0; agentIndex < _$agentCheckboxes.length; agentIndex++) {
                    var _$agentCheckbox = $(_$agentCheckboxes[agentIndex]);
                    selectedCases.push(_$agentCheckbox.val());
                }
            }

            if (selectedCases.length == 0) {
                abp.ui.clearBusy($('#container'));
                return;
            }

            $.ajax({
                url: abp.appPath + 'PortfolioSupervisor/Delete',
                type: 'POST',
                dataType: "json",
                traditional: true,
                data: { 'SelectedToInternalAgent': transferModel.SelectedToInternalAgent, 'SelectedCases': selectedCases },
                success: function (res) {
                    abp.ui.clearBusy($('#container'));

                    getFromAgentCases();
                    getToAgentCases();

                    abp.notify.success("Se eliminaron los casos.");
                },
                error: function (e) {
                    abp.ui.clearBusy($('#container'));
                }
            });
        });
    });
})();