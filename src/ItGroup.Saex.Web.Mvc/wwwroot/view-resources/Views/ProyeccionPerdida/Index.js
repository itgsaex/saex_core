﻿(function () {
    $(function () {
        var _$form = $('#searchForm');
        var _save = document.getElementById("save-button");

        function getSearchTerm(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        function format(d) {
            return '';
        }

        var detailDataTable = $('#detailDataTable').DataTable({
            "scrollY": "250",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                'print', 'csv', 'excel', 'pdf'
            ],
            "columnDefs": [
                { "width": "300px", "targets": 0 },
                { "width": "150px", "targets": 1 },
                { "width": "100px", "targets": 2 },
                { "width": "250px", "targets": 3 },
                { "width": "250px", "targets": 4 },
                { "width": "250px", "targets": 5 },
                { "width": "250px", "targets": 6 },
                { "width": "250px", "targets": 7 },
                { "width": "250px", "targets": 8 },
                { "width": "250px", "targets": 9 },
                { "width": "250px", "targets": 10 }
            ]
        });

        var totalDataTable = $('#totalDataTable').DataTable({
            "scrollY": "250",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                'print', 'csv', 'excel', 'pdf'
            ],
            "columnDefs": [
                { "width": "250px", "targets": 0 },
                { "width": "250px", "targets": 1 },
                { "width": "250px", "targets": 2 },
                { "width": "250px", "targets": 3 },
                { "width": "250px", "targets": 4 },
                { "width": "250px", "targets": 5 },
                { "width": "250px", "targets": 6 },
                { "width": "250px", "targets": 7 },
                { "width": "250px", "targets": 8 }
            ]
        });

        $("#search-button").click(() => {
            var totalCases = 0;
            var totalOriginalBalance = 0;
            var totalActualBalance = 0;
            var totalPayBalance = 0;
            var totalDubiousPromises = 0;
            var totalFirmPromises = 0;

            var model = _$form.serializeFormToObject();

            abp.ui.setBusy($('#container'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'ProyeccionPerdida/Search',
                data: model
            }).done((e, x) => {
                let table = $("#detailDataTable").find('tbody');
                detailDataTable.destroy();
                table.empty();
                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        items = items.concat(`<tr>
                                         <td>${item.agente}</td>
                                         <td>${item.asignacion}</td>
                                         <td>${item.totalCasos}</td>
                                         <td>${item.balanceOriginalFormat}</td>
                                         <td>${item.balanceActualFormat}</td>
                                         <td>${item.balancePagadoFormat}</td>
                                         <td>${item.promesasFirmesFormat}</td>
                                         <td>${item.totalCasosPromesasFirmes}</td>
                                         <td>${item.promesasDudosasFormat}</td>
                                         <td>${item.totalCasosPromesasDudosas}</td>
                                         <td>${item.actualFormat}</td>
                                    </tr>`);

                        totalCases = totalCases + item.totalCasos;
                        totalOriginalBalance = totalOriginalBalance + item.balanceOriginal;
                        totalActualBalance = totalActualBalance + item.balanceActual;
                        totalPayBalance = totalPayBalance + item.balancePagado;
                        totalDubiousPromises = totalDubiousPromises + item.promesasDudosas;
                        totalFirmPromises = totalFirmPromises + item.promesasFirmes;
                    });
                }
                table.append(items);
                detailDataTable = $('#detailDataTable').DataTable({
                    "scrollY": "250",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        'print', 'csv', 'excel', 'pdf'
                    ],
                    "columnDefs": [
                        { "width": "300px", "targets": 0 },
                        { "width": "150px", "targets": 1 },
                        { "width": "100px", "targets": 2 },
                        { "width": "250px", "targets": 3 },
                        { "width": "250px", "targets": 4 },
                        { "width": "250px", "targets": 5 },
                        { "width": "250px", "targets": 6 },
                        { "width": "250px", "targets": 7 },
                        { "width": "250px", "targets": 8 },
                        { "width": "250px", "targets": 9 },
                        { "width": "250px", "targets": 10 }
                    ]
                });

                // totales
                let totalTable = $("#totalDataTable").find('tbody');
                totalDataTable.destroy();
                totalTable.empty();
                let totalItems = "";

                if (totalOriginalBalance == 0)
                    totalOriginalBalance = 1;

                var actual = totalPayBalance / totalOriginalBalance;
                var real = (totalPayBalance + totalFirmPromises) / totalOriginalBalance;
                var bestScenario = (totalPayBalance + totalFirmPromises + totalDubiousPromises) / totalOriginalBalance;

                totalItems = totalItems.concat(`<tr>
                                         <td>${totalCases}</td>
                                         <td>${totalOriginalBalance}</td>
                                         <td>${totalActualBalance}</td>
                                         <td>${totalPayBalance}</td>
                                         <td>${totalFirmPromises}</td>
                                         <td>${totalDubiousPromises}</td>
                                         <td>${actual}</td>
                                         <td>${real}</td>
                                         <td>${bestScenario}</td>
                                    </tr>`);
                totalTable.append(totalItems);
                totalDataTable = $('#totalDataTable').DataTable({
                    "scrollY": "250",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        'print', 'csv', 'excel', 'pdf'
                    ],
                    "columnDefs": [
                        { "width": "250px", "targets": 0 },
                        { "width": "250px", "targets": 1 },
                        { "width": "250px", "targets": 2 },
                        { "width": "250px", "targets": 3 },
                        { "width": "250px", "targets": 4 },
                        { "width": "250px", "targets": 5 },
                        { "width": "250px", "targets": 6 },
                        { "width": "250px", "targets": 7 },
                        { "width": "250px", "targets": 8 }
                    ]
                });

            }).always((e, x) => {
                abp.ui.clearBusy($('#container'));
            });
        });
    });
})();