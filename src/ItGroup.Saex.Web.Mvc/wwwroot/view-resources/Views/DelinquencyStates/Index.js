﻿(function () {
    $(function () {
        var _delinquencyStateService = abp.services.app.delinquencyState;
        var _$modal = $('#DelinquencyStateCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshDelinquencyStateList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-delinquencyState', function (e) {
            var delinquencyStateId = $(this).attr("data-delinquencyState-id");
            var delinquencyStateName = $(this).attr('data-delinquencyState-name');

            deleteDelinquencyState(delinquencyStateId, delinquencyStateName);
        });

        $('#myTable').on('click', '.edit-delinquencyState', function (e) {
            var delinquencyStateId = $(this).attr("data-delinquencyState-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'DelinquencyStates/EditDelinquencyStateModal?id=' + delinquencyStateId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#DelinquencyStateEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var delinquencyState = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _delinquencyStateService.create(delinquencyState).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new delinquencyState!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshDelinquencyStateList() {
            location.reload(true); //reload page to see new delinquencyState!
        }

        function deleteDelinquencyState(delinquencyStateId, delinquencyStateName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), delinquencyStateName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _delinquencyStateService.delete(delinquencyStateId).done(function () {
                            refreshDelinquencyStateList();
                        });
                    }
                }
            );
        }
    });
})();
