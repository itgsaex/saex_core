﻿(function () {
    $(function () {
        var _$modal = $('#AgentSupervisorModal');
        var _$container = $('#searchContainer');
        var _$form = _$container.find('form');
        var _supervisorId = getUrlParameter("Supervisor");

        var isSuccess = getUrlParameter("success");

        if (isSuccess) {
            abp.notify.success("Agents were assigned sucessfully!");
        }

        _$form.validate({
        });

        $('#add-agents').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'AgentSupervisors/AssignAgents?supervisorId=' + _supervisorId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#AssignAgentsModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        $('.delete-agent').click(function () {
            var agentId = $(this).attr("data-agent-id");
            var agentName = $(this).attr('data-agent-name');

            deleteInternalAgentFromSupervisor(agentId, agentName);
        });

        $('#supervisor').on('change', function () {
            var supervisor = $("#supervisor option:selected").val();
            window.location.href = "AgentSupervisors?Supervisor=" + supervisor;
        });

        function deleteInternalAgentFromSupervisor(agentId, agentName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), agentName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        $.ajax({
                            url: abp.appPath + 'AgentSupervisors/RemoveAgent?supervisorId=' + _supervisorId + '&internalAgentId=' + agentId,
                            type: 'POST',
                            contentType: 'json',
                            success: function () {
                                window.location.href = "AgentSupervisors?Supervisor=" + _supervisorId;
                            },
                            error: function (e) { }
                        });
                    }
                }
            );
        }
    });
})();
