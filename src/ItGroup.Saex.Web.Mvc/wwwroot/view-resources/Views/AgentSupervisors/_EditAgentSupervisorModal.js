﻿(function ($) {
    var _$modal = $('#EditAgentSupervisorModal');
    var _$form = $('form[name=EditAgentSupervisorForm]');
    var selectedAgents = [];

    $('.selectpicker').selectpicker();

    $('#agents-table').DataTable({
        "pagingType": "simple",
        "ordering": false,
        "info": false
    });

    function save() {
        selectedAgents = [];
        var supervisorId = $("#supervisor-id").val();
        
        var _$agentCheckboxes = $("input[name='agent']:checked");
        if (_$agentCheckboxes) {
            for (var agentIndex = 0; agentIndex < _$agentCheckboxes.length; agentIndex++) {
                var _$agentCheckbox = $(_$agentCheckboxes[agentIndex]);
                selectedAgents.push(_$agentCheckbox.val());
            }
        }

        $.ajax({
            url: abp.appPath + 'AgentSupervisors/Save',
            type: 'POST',
            dataType: "json",
            traditional: true,
            data: { 'selectedAgents': selectedAgents, 'supervisorId': supervisorId },
            success: function (res) {
                window.location.href = "AgentSupervisors?Supervisor=" + supervisorId +"&success=true";
            },
            error: function (e) { }
        });
    }

    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $.AdminBSB.input.activate(_$form);

    //On load event for modal
    _$modal.on('shown.bs.modal', function () {
        selectedRoutes = [];

        _$form.find('input[type=text]:first').focus();
    });
})(jQuery);
