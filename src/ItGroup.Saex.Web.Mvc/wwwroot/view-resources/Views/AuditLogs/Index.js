﻿(function () {
    $(function () {
        var _auditLogService = abp.services.app.auditLog;
        var _$modal = $('#AuditLogCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshAuditLogList();
        });

        $('.delete-auditLog').click(function () {
            var auditLogId = $(this).attr("data-auditLog-id");
            var auditLogName = $(this).attr('data-auditLog-name');

            deleteAuditLog(auditLogId, auditLogName);
        });

        $('#dtDynamicVerticalScrollExample').DataTable({
            "scrollY": "50vh",
            "scrollCollapse": true,
        });
        $('.dataTables_length').addClass('bs-select');

        $('.edit-auditLog').click(function (e) {
            var auditLogId = $(this).attr("data-auditLog-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'AuditLogs/EditAuditLogModal?id=' + auditLogId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#AuditLogEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var auditLog = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _auditLogService.create(auditLog).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new auditLog!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshAuditLogList() {
            location.reload(true); //reload page to see new auditLog!
        }

        function deleteAuditLog(auditLogId, auditLogName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), auditLogName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        //debugger;
                        _auditLogService.delete(auditLogId).done(function () {
                            refreshAuditLogList();
                        });
                    }
                }
            );
        }
    });
})();