﻿(function ($) {
    var _auditLogService = abp.services.app.auditLog;
    var _$modal = $('#AuditLogEditModal');
    var _$form = $('form[name=AuditLogEditForm]');

    $.AdminBSB.input.activate(_$form);

    _$modal.on('shown.bs.modal', function () {
        _$form.find('input[type=text]:first').focus();
    });
})(jQuery);