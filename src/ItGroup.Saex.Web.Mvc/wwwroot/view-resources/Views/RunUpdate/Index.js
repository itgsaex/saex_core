﻿(function () {
    $(function () {
        var _auditLogService = abp.services.app.auditLog;
        var _$runUpdate = $('#runUpdate');
        var _$form = _$runUpdate.find('form');

        $('#myTable').DataTable({
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            "order": [[2, "asc"]],
            "autoWidth": true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            abp.ui.setBusy(null);

            _auditLogService.executeCaseProcessing().done(function () {
                location.reload(true);
            }).always(function () {
                abp.ui.clearBusy();
            });
        });
    });
})();
