﻿var questions = [];
var options = [];

var removeRow = function (e) {
    //debugger;
    $(e).closest("tr").remove();
    questions = questions.filter(function (element) { return element.id !== e.id; });
};

(function () {
    $(function () {
        var _activityCodeService = abp.services.app.activityCode;
        var _$modal = $('#ActivityCodeCreateModal');
        var _$form = _$modal.find('form');


        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-activityCode', function (e) {
            var activityCodeId = $(this).attr("data-activityCode-id");
            var activityCodeName = $(this).attr('data-activityCode-name');

            deleteActivityCode(activityCodeId, activityCodeName);
        });

        $('#myTable').on('click', '.edit-activityCode', function (e) {
            var activityCodeId = $(this).attr("data-activityCode-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'ActivityCodes/EditActivityCodeModal?id=' + activityCodeId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#ActivityCodeEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        $('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var code = $('#code').val();
            var description = $('#description').val();
            var activityCodeType = $("#activityCodeType option:selected").val();
            var isActive = $("#isActive").is(':checked');
            var isGlobal = $("#isGlobal").is(':checked');

            var model = {
                code: code,
                description: description,
                isActive: isActive,
                isGlobal: isGlobal,
                questions: questions,
                activityCodeType: activityCodeType
            };
            //debugger;
            abp.ui.setBusy(_$modal);
            _activityCodeService.create(model).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new activityCode!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        var currentModel = {
            id: "",
            description: "",
            type: "",
            isRequired: "",
            position: 0,
            options: []
        };

        $('#btn-add-question').click(function () {
            var question = $('#question').val();
            var questionType = $("#question-type option:selected").val();
            var isRequired = $("#isRequired").is(':checked');
            var id = generateUUID();
            if (question && questionType) {

                var model = {
                    id: "",
                    description: "",
                    type: "",
                    isRequired: "",
                    position: 0,
                    parameters: []
                };

                model.id = id;
                model.description = question;
                model.type = questionType;
                model.isRequired = isRequired;
                model.position = questions.length + 1;

                for (let i = 0; i < options.length; i++) {
                    options[i].questionId = model.id;
                    model.parameters.push(options[i]);
                }

                questions.push(model);

                options = [];

                $("#optionTable > tbody").html("");

                $('#add-options-container').hide();

                $('#selected-questions-container').show();

                $('#questionTable tr:last').after('<tr><td>'
                    + question + '</td><td> '
                    + getQuestionType(questionType) + '</td><td>'
                    + isRequired + '</td>'
                    + '<td><label class="material-icons" id=' + id + ' onclick="removeRow(this)">clear</label></td></tr>');

                $('#question').val('');
                $("#question-type").prop('selectedIndex', 0);
                $("#isRequired").prop("checked", false);
                id = null;
            }
        });

        $('#btn-add-option').click(function () {
            var option = $('#question-option').val();
            var id = generateUUID();
            if (question) {
                var optionModel = {
                    id: id,
                    description: option,
                    isDefault: false
                    //questionId: currentModel.id
                };

                $('#add-options-container').show();

                //currentModel.options.push(optionModel);
                options.push(optionModel);

                $('#optionTable tr:last').after('<tr><td>'

                    + '<label>' +
                        '<input type="checkbox" id="isDefault-' + id + '" name="isDefault-' + id +'" title="" class="option-checkbox filled-in checkbox-elem-edit" />' +
                        '<label for="isDefault-' + id +'" title=""></label>' +
                      '</label>'
                    + '</td><td> '
                    + option + '</td>'
                    + '<td><label class="material-icons" id=' + id + ' onclick="removeRow(this)">clear</label></td></tr>');

                $('#selected-options-container').show();

                $('#question-option').val('');
                id = null;
            }
        });

        $("#question-type").change(function () {
            var selectedValue = $(this).val();

            if (selectedValue === "4") //Select type
            {
                $('#add-options-container').show();
            } else {
                $('#add-options-container').hide();
            }
        });

        function getQuestionType(type) {
            switch (type) {
                case "1":
                    return "Text";
                    break;
                case "2":
                    return "Number";
                    break;
                case "3":
                    return "Date";
                    break;
                case "4":
                    return "Select";
                    break;
                case "4":
                    return "Telephone";
                    break;
                default:
                    return "Unknown";
            }
        }

        function deleteActivityCode(activityCodeId, activityCodeName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), activityCodeName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _activityCodeService.delete(activityCodeId).done(function () {
                            location.reload(true);
                        });
                    }
                }
            );
        }
    });
})();
