﻿var questions_form = [];
var options_form = [];

function saveQuestion() {
    let _$form = $('form[name=activityCodeEditForm]');
    let question = $('#question-form').val();
    let questionType = $('#question-type-form').val();
    let isRequired = $('#isRequired-form').is(":checked");
    if (!question || !questionType) {
        abp.message.error("Debe Completar los campos: Pregunta y Tipo de Pregunta.", "Error");
        return;
    }
    let partialId = uuidv4();
    questions_form.push({ Description: question, IsRequired: isRequired, Type: parseInt(questionType), partialId});
    $('#questionTableBody').append(`
                                <tr>
                                    <td>${question}</td>
                                    <td>${options_form.find(e => e.Value === parseInt(questionType)).Name}</td>
                                    <td>${isRequired}</td>
                                    <td><label class="material-icons" id="${partialId}" onclick="removeQuestion(this)">clear</label></td>
                                </tr>`);
    $('#question-type-form').selectpicker('val', '');
    $('#question-form').val('');
}

var removeQuestion = function (e) {
    $(e).closest("tr").remove();
    questions_form = questions_form.filter(element => element.Id ? (element.Id !== e.id) : (element.partialId !== e.id));
    console.log(questions);
};
(function ($) {
    var _activityCodeService = abp.services.app.activityCode;
    var _$modal = $('#ActivityCodeEditModal');
    let _$form = $('form[name=activityCodeEditForm]');
    questions_form = $('#questionTableBody').data('questions') || [];
    options_form = $('#questionTableBody').data('question-types') || [];
    $('.selectpicker').selectpicker();
    console.log();
    function save() {
        if (!_$form.valid()) {
            
            return;
        }

        var activityCode = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        //debugger;
        abp.ui.setBusy(_$form);
        _activityCodeService.update({ ...activityCode, Questions: questions_form, IsGlobal: $('#isGlobal-form').is(":checked"), IsActive: $('#isActive-form').is(":checked") }).done(function () {
            _$modal.modal('hide');
            location.reload(true); //reload page to see edited activityCode!
        }).always(function () {
            abp.ui.clearBusy(_$modal);
        });
    }

    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $.AdminBSB.input.activate(_$form);

    _$modal.on('shown.bs.modal', function () {
        _$form.find('input[type=text]:first').focus();
    });
   
})(jQuery);

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}