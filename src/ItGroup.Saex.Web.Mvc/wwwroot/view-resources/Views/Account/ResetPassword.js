﻿$(function () {
    var _$form = $("#PasswordResetForm")

    _$form.validate({
        rules: {
            newPassword: "required",
            confirmPassword: {
                equalTo: "#NewPassword"
            }
        }
    });
    //Handle save button click
    _$form.submit(function (e) {
        e.preventDefault();

        if (!_$form.valid()) {
            return;
        }

        abp.ui.setBusy(
            $('#PasswordResetFormArea'),

            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: _$form.attr('action'),
                data: _$form.serialize()
            })
        );
    });

});