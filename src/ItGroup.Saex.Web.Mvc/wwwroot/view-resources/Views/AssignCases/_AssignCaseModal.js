﻿(function ($) {

    $('#agents-table').DataTable({
        "pagingType": "simple",
        "ordering": false,
        "info": false
    });
    $('#cases-table').DataTable({
        "pagingType": "simple",
        "ordering": false,
        "info": false
    });


})(jQuery);
