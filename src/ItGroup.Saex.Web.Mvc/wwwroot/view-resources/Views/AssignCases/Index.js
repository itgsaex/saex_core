﻿(function () {
    $(function () {

        //var _assignCaseService = abp.services.app.assignCase;
        var _$modal = $('#AssignCaseModal');
        var _$container = $('#searchContainer');
        var _$form = _$container.find('form');
        var _agentId = getUrlParameter("FromAgent");
        var _selectedCases = [];
        var _$transferForm = $('#transferForm');

        var isSuccess = getUrlParameter("success");

        if (isSuccess) {
            abp.notify.success("Cases were transfered sucessfully!");
        }

        _$form.validate({
        });

        var fromCasesDataTable = $('#from-cases-table').DataTable({
            "scrollY": "250px",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                if (column == 0)
                                    return "";
                                else if (column == 1)
                                    return '\u200C' + data;
                                else
                                    return data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                }
            ]
        });

        var selectedToAgenteTransferCasesDataTable = $('#selectedToAgenteTransferCasesDataTable').DataTable({
            "scrollY": "250px",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                if (column == 0)
                                    return "";
                                else if (column == 1)
                                    return '\u200C' + data;
                                else
                                    return data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                }
            ],
            "columnDefs": [
                { "width": "100px", "targets": 0 },
                { "width": "250px", "targets": 1 },
                { "width": "250px", "targets": 2 },
                { "width": "250px", "targets": 3 },
                { "width": "250px", "targets": 4 },
                { "width": "250px", "targets": 5 }
            ]
        });

        $("#to-agent").change(() => {
            getToAgentCases();
        });

        $("#search-button").click(() => {
            getFromAgentCases();
        });

        function getFromAgentCases() {
            var model = _$form.serializeFormToObject();

            if (model.FromAgentId == "")
                return;

            abp.ui.setBusy($('#from-cases-table'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'AssignCases/SearchFromCases',
                data: model
            }).done((e, x) => {
                let table = $("#from-cases-table").find('tbody');
                fromCasesDataTable.destroy();
                table.empty();
                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        items = items.concat(`<tr>
                                              <td style="text-align: center;">
                                                <label>
                                                    <input type="checkbox" name="case" value="${item.caseID}" title="" class="filled-in checkbox-elem-edit" id="case-${item.caseID}" />
                                                    <label for="case-${item.caseID}" title=""></label>
                                                </label>
                                              </td>
                                         <td>${item.accountNumber}</td>
                                         <td>${item.principalCustomerName}</td>
                                         <td>${item.postalCode}</td>
                                         <td>${item.productName}</td>
                                    </tr>`);
                    });
                }
                table.append(items);
                fromCasesDataTable = $('#from-cases-table').DataTable({
                    "scrollY": "250px",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        if (column == 0)
                                            return "";
                                        else if (column == 1)
                                            return '\u200C' + data;
                                        else
                                            return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        }
                    ]
                });

            }).always((e, x) => {
                abp.ui.clearBusy($('#from-cases-table'));
            });
        }

        function getToAgentCases() {
            var model = _$transferForm.serializeFormToObject();

            //Buscar transferidos o compartidos
            abp.ui.setBusy($('#transferCases'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'AssignCases/SelectedToAgentTransferCases',
                data: model
            }).done((e, x) => {
                let transferTable = $("#selectedToAgenteTransferCasesDataTable").find('tbody');
                selectedToAgenteTransferCasesDataTable.destroy();
                transferTable.empty();
                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        items = items.concat(`<tr>
                                         <td style="text-align: center;">
                                                <label>
                                                    <input type="checkbox" name="transferCase" value="${item.id}" title="" class="filled-in checkbox-elem-edit" id="case-${item.id}" />
                                                    <label for="case-${item.id}" title=""></label>
                                                </label>
                                              </td>
                                         <td>${item.accountNumber}</td>
                                         <td>${item.name}</td>
                                         <td>${item.city}</td>
                                         <td>${item.zipcode}</td>
                                         <td>${item.productNumber}</td>
                                    </tr>`);
                    });
                }
                transferTable.append(items);
                selectedToAgenteTransferCasesDataTable = $('#selectedToAgenteTransferCasesDataTable').DataTable({
                    "scrollY": "250px",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        if (column == 0)
                                            return "";
                                        else if (column == 1)
                                            return '\u200C' + data;
                                        else
                                            return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        }
                    ],
                    "columnDefs": [
                        { "width": "100px", "targets": 0 },
                        { "width": "250px", "targets": 1 },
                        { "width": "250px", "targets": 2 },
                        { "width": "250px", "targets": 3 },
                        { "width": "250px", "targets": 4 },
                        { "width": "250px", "targets": 5 }
                    ]
                });

            }).always((e, x) => {
                abp.ui.clearBusy($('#transferCases'));
            });
        }

        $("#save-button").click(() => {
            var transferModel = _$transferForm.serializeFormToObject();
            var searchModel = _$form.serializeFormToObject();
            selectedCases = [];

            if (searchModel.FromAgentId == "")
                return;

            if (transferModel.ToAgentId == "")
                return;

            if (searchModel.FromAgentId == transferModel.ToAgentId)
                return;

            if (transferModel.SelectedAsignationType == "")
                return;

            abp.ui.setBusy($('#container'));

            var _$agentCheckboxes = $("input[name='case']:checked");
            if (_$agentCheckboxes) {
                for (var agentIndex = 0; agentIndex < _$agentCheckboxes.length; agentIndex++) {
                    var _$agentCheckbox = $(_$agentCheckboxes[agentIndex]);
                    selectedCases.push(_$agentCheckbox.val());
                }
            }

            if (selectedCases.length == 0) {
                abp.ui.clearBusy($('#container'));
                return;
            }

            $.ajax({
                url: abp.appPath + 'AssignCases/Save',
                type: 'POST',
                dataType: "json",
                traditional: true,
                data: { 'SelectedFromAgent': searchModel.FromAgentId, 'SelectedToAgent': transferModel.ToAgentId, 'SelectedCases': selectedCases, 'SelectedAsignationType': transferModel.SelectedAsignationType },
                success: function (res) {
                    abp.ui.clearBusy($('#container'));

                    getFromAgentCases();
                    getToAgentCases();

                    abp.notify.success("Se transfirieron/compartieron los casos.");
                },
                error: function (e) {
                    abp.ui.clearBusy($('#container'));
                }
            });
        });

        $("#delete-button").click(() => {
            var transferModel = _$transferForm.serializeFormToObject();
            var searchModel = _$form.serializeFormToObject();
            selectedCases = [];

            if (transferModel.ToAgentId == "")
                return;

            abp.ui.setBusy($('#container'));

            var _$agentCheckboxes = $("input[name='transferCase']:checked");
            if (_$agentCheckboxes) {
                for (var agentIndex = 0; agentIndex < _$agentCheckboxes.length; agentIndex++) {
                    var _$agentCheckbox = $(_$agentCheckboxes[agentIndex]);
                    selectedCases.push(_$agentCheckbox.val());
                }
            }

            if (selectedCases.length == 0) {
                abp.ui.clearBusy($('#container'));
                return;
            }

            $.ajax({
                url: abp.appPath + 'AssignCases/Delete',
                type: 'POST',
                dataType: "json",
                traditional: true,
                data: { 'SelectedToAgent': transferModel.ToAgentId, 'SelectedCases': selectedCases },
                success: function (res) {
                    abp.ui.clearBusy($('#container'));

                    getFromAgentCases();
                    getToAgentCases();

                    abp.notify.success("Se eliminaron los casos.");
                },
                error: function (e) {
                    abp.ui.clearBusy($('#container'));
                }
            });
        });

        //remove duplicated dropdown element... dont know what happened...
        //$('.btn.dropdown-toggle.btn-default')[0].remove();

        //$("#steps-vertical").steps({
        //    headerTag: "h3",
        //    bodyTag: "section",
        //    transitionEffect: "fade",
        //    stepsOrientation: "vertical",
        //    onFinished: function (event, currentIndex) {
        
        //        var toAgentId = $("#to-agent option:selected").val();
        //        var transferType = $("#transfer-type option:selected").val();

        //        if (!toAgentId) {
        //            abp.notify.error("You must choose an agent.");
        //        }

        //        $.ajax({
        //            url: abp.appPath + 'AssignCases/Save',
        //            type: 'POST',
        //            dataType: "html",
        //            traditional: true,
        //            data: { 'fromAgentId': _agentId, 'toAgentId': toAgentId, 'selectedCases': _selectedCases, 'transferType': transferType },
        //            success: function (res) {
        //                window.location.href = "AssignCases?success=true";
        //            },
        //            error: function (e) { }
        //        });
        //    },
        //    onStepChanging: function () {

        //        var _$caseCheckboxes = $("input[name='case']:checked");
        //        _selectedCases = [];

        //        if (_$caseCheckboxes) {
        //            for (var caseIndex = 0; caseIndex < _$caseCheckboxes.length; caseIndex++) {
        //                var _$caseCheckbox = $(_$caseCheckboxes[caseIndex]);
        //                _selectedCases.push(_$caseCheckbox.val());
        //            }
        //        }

        //        if (!_selectedCases || _selectedCases.length === 0) {
        //            abp.notify.error("You must choose at least one case from an agent");
        //            return false;
        //        }

        //        return true;
        //    },
        //    onStepChanged: function () {
                
        //        $.ajax({
        //            url: abp.appPath + 'AssignCases/AssignCaseModal',
        //            type: 'POST',
        //            dataType: "html",
        //            traditional: true,
        //            data: { 'fromAgentId': _agentId, 'selectedCases': _selectedCases },
        //            success: function (content) {
        //                $('#assign-case-review-container').html(content);
        //                $('.selectpicker').selectpicker();
        //            },
        //            error: function (e) { }
        //        });
        //    }
        //});

    });
})();