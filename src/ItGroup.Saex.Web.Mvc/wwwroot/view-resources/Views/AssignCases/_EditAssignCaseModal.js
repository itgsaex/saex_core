﻿(function ($) {
    var _regionService = abp.services.app.region;
    var _$modal = $('#EditAssignCaseModal');
    var _$form = $('form[name=EditAssignCaseForm]');
    var selectedRegions = [];

    $('#regions-table').DataTable({
        "pagingType": "simple",
        "ordering": false,
        "info": false
    });

    function save() {
        selectedRegions = [];
        var agent = _$form.serializeFormToObject();

        var _$regionCheckboxes = $("input[name='region']:checked");
        if (_$regionCheckboxes) {
            for (var regionIndex = 0; regionIndex < _$regionCheckboxes.length; regionIndex++) {
                var _$regionCheckbox = $(_$regionCheckboxes[regionIndex]);
                selectedRegions.push(_$regionCheckbox.val());
            }
        }

        var model = {
            RegionIds: selectedRegions,
            AgentId: agent.Id
        };

        abp.ui.setBusy(_$form);
        _regionService.transferCase(model).done(function () {
            _$modal.modal('hide');
            location.reload(true); //reload page to see edited transferCase!
        }).always(function () {
            abp.ui.clearBusy(_$modal);
        });
    }

    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $.AdminBSB.input.activate(_$form);

    //On load event for modal
    _$modal.on('shown.bs.modal', function () {
        selectedRoutes = [];

        _$form.find('input[type=text]:first').focus();
        setTimeout(function () { }, 500);

        //This initializes the Postal Codes multi select control...

        $('#RoutesEdit').multiSelect({
            selectableHeader: "<div class='custom-header'>Choose from list</div>",
            selectionHeader: "<div class='custom-header'>Routes in transferCase</div>",
            afterSelect: function (array) {
                var index = selectedRoutes.indexOf(array[0]);
                if (index === -1) {
                    selectedRoutes.push(array[0]);
                }
            },
            afterDeselect: function (array) {
                var index = selectedRoutes.indexOf(array[0]);
                if (index > -1) {
                    selectedRoutes.splice(index);
                }
            }
        });

        $('#SelectedRoutes').children("option").each(function (i, e) {
            if (e.value && e.value !== "") {
                $('#RoutesEdit').multiSelect('select', e.value);
            }
        });
    });
})(jQuery);
