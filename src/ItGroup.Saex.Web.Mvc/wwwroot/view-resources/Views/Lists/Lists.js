﻿(function () {
    $(function () {
        //var _listService = abp.services.app.list;
        //var _$modal = $('#UserCreateModal');
        //var _$form = _$modal.find('form');

        //var status = getUrlParameter("status");
        ////debugger;
        //if (status) {
        //    // $('#user-status').val(status);
        //    $('#user-status option[value=' + status + ']').attr('selected', 'selected');

        //    var text = "";

        //    switch (status) {
        //        case "0":
        //            text = "All";
        //            break;
        //        case "1":
        //            text = "Active";
        //            break;
        //        case "2":
        //            text = "Inactive";
        //            break;
        //        case "3":
        //            text = "Deleted";
        //            break;
        //        default:
        //            text = "All";
        //    }

        //    $('#status-label').html('Viewing ' + text + ' Records')
        //}

        $('#dtDynamicVerticalScrollExample').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        //$('.dataTables_length').addClass('bs-select');

        //$('#user-status').on('change', function () {
        //    var status = $("#user-status option:selected").val();
        //    window.location.href = "/Users?status=" + status;
        //});

        //_$form.validate({
        //    rules: {
        //        Password: "required",
        //        ConfirmPassword: {
        //            equalTo: "#Password"
        //        }
        //    }
        //});

        //$('#RefreshButton').click(function () {
        //    refreshUserList();
        //});

        //$('.delete-user').click(function () {
        //    var userId = $(this).attr("data-user-id");
        //    var userName = $(this).attr('data-user-name');

        //    deleteUser(userId, userName);
        //});

        $('.edit-list').click(function (e) {
            let listId = $(this).attr("data-id");
            console.log(this)
            e.preventDefault();
            window.location.replace(abp.appPath + "Lists/EditList?id=" + listId);
            //$.ajax({
            //    url: abp.appPath + 'Lists/EditList?listId=' + listId,
            //    type: 'POST',
            //    contentType: 'html',
            //    success: function (content) {
            //        $('#UserEditModal div.modal-content').html(content);
            //    },
            //    error: function (e) { }
            //});
        });

        //$('.checkbox-elem').click(function (e) {
        //    if (e.target.defaultValue === "AGENT" && e.target.checked) {
        //        $('#agent-settings').attr('style', 'display: block');
        //    } else if (e.target.defaultValue === "AGENT" && !e.target.checked) {
        //        $('#agent-settings').attr('style', 'display: none');
        //    }
        //});

        //_$form.find('button[type="submit"]').click(function (e) {
        //    e.preventDefault();

        //    if (!_$form.valid()) {
        //        return;
        //    }

        //    var user = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        //    user.roleNames = [];
        //    var _$roleCheckboxes = $("input[name='role']:checked");
        //    if (_$roleCheckboxes) {
        //        for (var roleIndex = 0; roleIndex < _$roleCheckboxes.length; roleIndex++) {
        //            var _$roleCheckbox = $(_$roleCheckboxes[roleIndex]);
        //            user.roleNames.push(_$roleCheckbox.val());
        //        }
        //    }

        //    abp.ui.setBusy(_$modal);
        //    _userService.create(user).done(function () {
        //        _$modal.modal('hide');
        //        location.reload(true); //reload page to see new user!
        //    }).always(function () {
        //        abp.ui.clearBusy(_$modal);
        //    });
        //});

        //_$modal.on('shown.bs.modal', function () {
        //    _$modal.find('input:not([type=hidden]):first').focus();
        //});

        //function refreshUserList() {
        //    location.reload(true); //reload page to see new user!
        //}

        //function deleteUser(userId, userName) {
        //    abp.message.confirm(
        //        abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), userName),
        //        function (isConfirmed) {
        //            if (isConfirmed) {
        //                _userService.delete({
        //                    id: userId
        //                }).done(function () {
        //                    refreshUserList();
        //                });
        //            }
        //        }
        //    );
        //}
    });
})();
