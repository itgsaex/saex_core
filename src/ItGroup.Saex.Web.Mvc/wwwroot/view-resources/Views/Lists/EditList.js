﻿(function ($) {
    let _$form = $('#listForm');
    let spaceRegex = /\s/g;
    let fields = $('#builder').data('fields').sort((a, b) => {
        var textA = a.HumanName.toUpperCase();
        var textB = b.HumanName.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    console.log(fields);
    let list = $('#builder').data('list');
    let arr = [];
    let dataTable = $('#TableValue').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    let formatQuery = () => {
        let listForm = _$form.serializeFormToObject();
        let fieldsToPivot = [];
        if (list.Type != 1) {
            for (var i = 0; i < fields.length; i++) {
                let element = fields[i];
                if (element.SourceTargetType == "columna") {
                    list.Query = list.Query.replace(element.ColumnName, `caso.${element.TargetColumn}`);
                }
                else {
                    list.Query = list.Query.replace(element.ColumnName, `[${element.ColumnName}]`);
                    list.Query = list.Query.replace(`[[${element.ColumnName}]]`, `[${element.ColumnName}]`);
                    if (list.Query.includes(element.ColumnName) && !fieldsToPivot.includes(element.ColumnName)) {
                        fieldsToPivot.push(element.ColumnName);
                    }
                }
            }
            list.FieldsToPivot = fieldsToPivot.toString();
        }
        for (var i = 0; i < fields.length; i++) {
            let element = fields[i];
            list.Query = list.Query.replace(`'${element.ColumnName}'`, `[${element.ColumnName}]`);
            list.Query = list.Query.replace(`${element.ColumnName}`, `[${element.ColumnName}]`);
            list.Query = list.Query.replace(`[[${element.ColumnName}]]`, `[${element.ColumnName}]`);
            list.Query = list.Query.replace(`[[[${element.ColumnName}]]]`, `[${element.ColumnName}]`);
            list.Query = list.Query.replace(`[[[${element.ColumnName}]]]`, `[${element.ColumnName}]`);
        }
    };
    _$form.validate({
    });
    for (var i = 0; i < fields.length; i++) {
        let element = fields[i];
        if (arr.find(e => e.id == element.ColumnName) == undefined) {
            arr.push({
                id: element.ColumnName,
                label: element.HumanName,
                operators: ["equal", "not_equal", "in", "not_in", "less", "less_or_equal", "greater", "greater_or_equal", "between", "not_between", "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with", "is_empty", "is_not_empty", "is_null", "is_not_null"],
                value_separator: ','
            });
        }
    }
    $('#builder').queryBuilder({
        plugins: {
            'bt-tooltip-errors': { delay: 100 }
        },
        lang_code: abp.localization.currentLanguage.name,
        filters: arr
    });
    if (list.Query) {
        list.Query = list.Query.replace(/[\[\]]+/g, '');
        for (var i = 0; i < fields.length; i++) {
            let element = fields[i];
            list.Query = list.Query.replace(`caso.${element.TargetColumn}`, element.ColumnName);
            if (/\s/.test(element.ColumnName)) {
                list.Query = list.Query.replace(`${element.ColumnName}`, `'${element.ColumnName}'`);
                list.Query = list.Query.replace(`''${element.ColumnName}''`, `'${element.ColumnName}'`);
            }
        }
        console.log(list.Query, "test")

        $('#builder').queryBuilder('setRulesFromSQL', list.Query)
    }
    $('#builder').on("change.queryBuilder afterDeleteRule.queryBuilder", (e) => {
        let val = $('#builder').queryBuilder('getSQL', false);
        if (!val)
            return;
        list.Query = val.sql;
        $("#Query").val(val.sql)
    });
    $("#test").click(() => {
        let val = $('#builder').queryBuilder('getSQL', false);
        if (!val)
            return;
        list.Query = val.sql;
        $("#Query").val(val.sql);
        formatQuery();
        let formdata = _$form.serializeFormToObject();
        abp.ui.setBusy($('#container'));
        abp.ajax({
            contentType: 'application/x-www-form-urlencoded',
            url: abp.appPath + 'Lists/TestQuery',
            data: { ...list, ProductSystemTable: formdata.ProductSystemTable, EstadoRiesgoId: formdata.EstadoRiesgoId}
        }).done((e, x) => {
            let table = $("#TableValue").find('tbody');
            dataTable.destroy();
            table.empty();
            let items = "";
            if (x.result) {
                x.result.forEach(item => {
                    items = items.concat(`<tr>
                                         <td>${item.productNumber}</td>
                                         <td>${item.customerName}</td>
                                         <td>${item.accountNumber}</td>
                                         <td>${item.zipCode}</td>
                                    </tr>`);
                });
            }
            table.append(items);
            dataTable = $('#TableValue').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

        }).always( (e, x) => {
            abp.ui.clearBusy($('#container'));
        });
    });
    _$form.submit(function (e) {
        e.preventDefault();
       
        if (!_$form.valid()) {
            return;
        }
        
        abp.ui.setBusy($('#container'));
        formatQuery();
        let formdata = _$form.serializeFormToObject();
        abp.ajax({
            contentType: 'application/x-www-form-urlencoded',
            url: _$form.attr('action'),
            data: { ...list, IsActive: formdata.IsActive, Name: formdata.Name, ProductSystemTable: formdata.ProductSystemTable, EstadoRiesgoId: formdata.EstadoRiesgoId }
        }).done((a, x) => {
        }).always(function (a, x) {
            abp.ui.clearBusy($('#container'));
        });
    });
})(jQuery);