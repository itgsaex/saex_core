﻿(function () {
    $(function () {
        var _workingHourService = abp.services.app.workingHour;
        var _$modal = $('#WorkingHourCreateModal');
        var _$form = _$modal.find('form');

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshWorkingHourList();
        });

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-workingHour', function (e) {
            var workingHourId = $(this).attr("data-workingHour-id");
            var workingHourName = $(this).attr('data-workingHour-name');

            deleteWorkingHour(workingHourId, workingHourName);
        });

        $('#myTable').on('click', '.edit-workingHour', function (e) {
            var workingHourId = $(this).attr("data-workingHour-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'WorkingHours/EditWorkingHourModal?id=' + workingHourId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#WorkingHourEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var workingHour = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            abp.ui.setBusy(_$modal);
            _workingHourService.create(workingHour).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new workingHour!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshWorkingHourList() {
            location.reload(true); //reload page to see new workingHour!
        }

        function deleteWorkingHour(workingHourId, workingHourName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), workingHourName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _workingHourService.delete(workingHourId).done(function () {
                            refreshWorkingHourList();
                        });
                    }
                }
            );
        }
    });
})();