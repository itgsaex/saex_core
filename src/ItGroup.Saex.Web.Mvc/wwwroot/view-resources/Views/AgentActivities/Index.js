﻿(function () {
    $(function () {
        var _agentActivityAppService = abp.services.app.agentActivity;
        var _$container = $('#searchContainer');
        var _$form = _$container.find('form');
        var currentMarkers = [];
        let items = [];

        $('.date').datepicker({ format: 'yyyy-mm-dd' });
        $('.selectpicker').selectpicker();

        mapboxgl.accessToken = 'pk.eyJ1Ijoiamx1Z2FybyIsImEiOiJjazcwbmF5aDcwMGJyM2tsOW1sNHd3MDdkIn0.9LC6wQKEmtC7tRE3YHLTUw';

        var map = new mapboxgl.Map({
            container: 'map', // HTML container id
            style: 'mapbox://styles/mapbox/streets-v9', // style URL
            center: [-66.4094091, 18.2429695], // starting position as [lng, lat]
            zoom: 9
        });

        var layerList = document.getElementById('menu');
        var inputs = layerList.getElementsByTagName('input');

        function switchLayer(layer) {
            var layerId = layer.target.id;
            map.setStyle('mapbox://styles/mapbox/' + layerId);
        }

        for (var i = 0; i < inputs.length; i++) {
            inputs[i].onclick = switchLayer;
        }

        map.on('load', function () {
            map.resize();
        });

        let table = $('#dataTable').DataTable({
            "bPaginate": true,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                               if (column == 8)
                                    return '\u200C' + data;
                                else
                                    return data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                }
            ],
            columns: [
                { data: "number" },
                { data: "type" },
                { data: "latitude" },
                { data: "longitude", },
                { data: 'activity' },
                { data: 'place' },
                { data: 'date' },
                { data: 'product' },
                { data: 'accountNumber' },
                { data: 'name' }
            ],
            "columnDefs": [
                {
                    "targets": [2, 3],
                    "visible": false,
                    "searchable": false
                }
            ]
        });
        $('.dataTable').on('click', 'tbody tr', function () {
            let data = table.row(this).data();
            if (data) {
                map.setCenter([data.longitude, data.latitude])
                map.zoomTo(20, {
                    duration: 2000
                });
                $("body,html").animate(
                    {
                        scrollTop: $("#map").offset().top
                    },
                    800 //speed
                );
            }
        })

        _$form.validate({
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            abp.ui.setBusy(_$form);

            table.clear();

            if (currentMarkers !== null) {
                for (var i = currentMarkers.length - 1; i >= 0; i--) {
                    currentMarkers[i].remove();
                }
            }

            var model = _$form.serializeFormToObject();

            _agentActivityAppService.getByAgentId(model.Agent, model.Date).done(function (result) {
                items = result;
                result.forEach((item, index) => {
                    item.number = index + 1;
                    if (item.type == "") {
                        var automaticMarker = new mapboxgl.Marker({ color: "#fcbc17" })
                            .setLngLat([item.longitude, item.latitude])
                            .setPopup(new mapboxgl.Popup().setHTML("<p><b>Lectura automatica</b><br /><b>Fecha:</b> " + formatDate(item.date) + "</p>"))
                            .addTo(map);
                        currentMarkers.push(automaticMarker);
                    } else {
                        // create a HTML element for each feature
                        var el = document.createElement('div');
                        let span = document.createElement('span');
                        span.append(item.number);
                        el.appendChild(span);
                        if (item.iconText == "Auto") {
                            el.className = 'autoMarker';
                        } else if (item.iconText == "Mortgage") {
                            el.className = 'mortgageMarker';
                        } else if (item.iconText == "Boat") {
                            el.className = 'boatMarker';
                        } else if (item.iconText == "Loan") {
                            el.className = 'loanMarker';
                        } else if (item.iconText == "CreditLine") {
                            el.className = 'creditLineMarker';
                        } else if (item.iconText == "Leasing") {
                            el.className = 'leasingMarker';
                        } else if (item.iconText == "CreditCard") {
                            el.className = 'creditCardMarker';
                        } else {
                            el = null;
                        }

                        var activityMarker = new mapboxgl.Marker(el)
                            .setLngLat([item.longitude, item.latitude])
                            .setPopup(new mapboxgl.Popup().setHTML("<p><b>Numero Cuenta:</b> " + item.accountNumber + "<br /><b>Producto:</b> " + item.type + "<br /><b>Lugar:</b> " + item.place + "<br /> <b>Fecha:</b> " + formatDate(item.date) + "</p>"))
                            .addTo(map);
                        currentMarkers.push(activityMarker);
                    }

                    table.row.add({
                        "number": item.number,
                        "type": item.type,
                        "activity": item.activity,
                        "latitude": item.latitude,
                        "longitude": item.longitude,
                        "place": item.place,
                        "date": formatDate(item.date),
                        "product": item.product,
                        "accountNumber": item.accountNumber,
                        "name": item.name
                    }).draw();
                });
                $("body,html").animate(
                    {
                        scrollTop: $("#dataTable").offset().top
                    },
                    800 //speed
                );
                map.zoomTo(8, {
                    duration: 1000
                });
                map.setCenter([-66.4094091, 18.2429695]);
            }).always(function () {
                abp.ui.clearBusy(_$form);
            });
        });

        $('#RefreshButton').click(function () {
            refreshAgentActivityList();
        });

        function refreshAgentActivityList() {
            location.reload(true); //reload page to see new agentActivity!
        }
    });
})();