﻿(function () {
    $(function () {
        var _assignRegionService = abp.services.app.assignRegion;
        var _$modal = $('#AssignRegionModal');
        var _$form = _$modal.find('form');
        var selectedRegions = [];

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshAssignRegionList();
        });

        $('#myTable').DataTable({
            "bPaginate": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#myTable').on('click', '.delete-assignRegion', function (e) {
            var assignRegionId = $(this).attr("data-assignRegion-id");
            var assignRegionName = $(this).attr('data-assignRegion-name');

            deleteAssignRegion(assignRegionId, assignRegionName);
        });

        $('#myTable').on('click', '.edit-assignRegion', function (e) {
            var assignRegionId = $(this).attr("data-assignRegion-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'AssignRegions/EditAssignRegionModal?id=' + assignRegionId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#EditAssignRegionModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        $('#assignRegionsBtn').click(function (e) {
            e.preventDefault();

            //var _$regionCheckboxes = $("input[name='region']:checked");
            //if (_$regionCheckboxes) {
            //    for (var regionIndex = 0; regionIndex < _$regionCheckboxes.length; regionIndex++) {
            //        var _$regionCheckbox = $(_$regionCheckboxes[regionIndex]);
            //        selectedRegions.push(_$regionCheckbox.val());
            //    }

            //selectedRegions = [];
            //$.post(abp.appPath + 'AssignRegions/AssignRegionModal',null,
            //    function (content) {
            //        $('#AssignRegionModal div.modal-content').html(content);
            //    }, 'html');

            $.ajax({
                url: abp.appPath + 'AssignRegions/AssignRegionModal',
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#AssignRegionModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!_$form.valid()) {
                return;
            }

            var assignRegion = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js

            assignRegion.Routes = selectedRoutes;

            abp.ui.setBusy(_$modal);
            _assignRegionService.create(assignRegion).done(function () {
                _$modal.modal('hide');
                window.location.href = window.location.origin + window.location.pathname; //reload page to see new assignRegion!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshAssignRegionList() {
            window.location.href = window.location.origin + window.location.pathname; //reload page to see new assignRegion!
        }

        function deleteAssignRegion(assignRegionId, assignRegionName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), assignRegionName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _assignRegionService.delete(assignRegionId).done(function () {
                            refreshAssignRegionList();
                        });
                    }
                }
            );
        }
    });
})();