﻿(function () {
    $(function () {
        var _regionService = abp.services.app.region;
        var _$modal = $('#AssignRegionModal');
        var _$form = $('form[name=AssignRegionForm]');
        var selectedRegions = [];
        var selectedAgents = [];

        $('#agents-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        /*
         *"scrollY": "250",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": false,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
         */

        $('#regions-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#lists-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        /*$("#example-vertical").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "fade",
            stepsOrientation: "vertical",
            onFinished: function (event, currentIndex) {
                save();
            }
        });*/

        function save() {
            selectedRegions = [];
            selectedAgents = [];
            selectedWorkingHour = [];
            selectedLists = [];

            var _$regionCheckboxes = $("input[name='region']:checked");
            if (_$regionCheckboxes) {
                for (var regionIndex = 0; regionIndex < _$regionCheckboxes.length; regionIndex++) {
                    var _$regionCheckbox = $(_$regionCheckboxes[regionIndex]);
                    selectedRegions.push(_$regionCheckbox.val());
                }
            }

            var _$agentCheckboxes = $("input[name='agent']:checked");
            if (_$agentCheckboxes) {
                for (var agentIndex = 0; agentIndex < _$agentCheckboxes.length; agentIndex++) {
                    var _$agentCheckbox = $(_$agentCheckboxes[agentIndex]);
                    selectedAgents.push(_$agentCheckbox.val());
                }
            }

            var _$listCheckboxes = $("input[name='list']:checked");
            if (_$listCheckboxes) {
                for (var agentIndex = 0; agentIndex < _$listCheckboxes.length; agentIndex++) {
                    var _$listCheckbox = $(_$listCheckboxes[agentIndex]);
                    selectedLists.push(_$listCheckbox.val());
                }
            }

            var _$workingHourCheckboxes = $("input[name='workingHour']:checked");
            if (_$workingHourCheckboxes) {
                for (var agentIndex = 0; agentIndex < _$workingHourCheckboxes.length; agentIndex++) {
                    var _$workingHourCheckbox = $(_$workingHourCheckboxes[agentIndex]);
                    selectedWorkingHour.push(_$workingHourCheckbox.val());
                }
            }

            var workingHour = null;
            if (selectedWorkingHour.length > 0) {
                workingHour = selectedWorkingHour[0];
            }

            var model = {
                RegionIds: selectedRegions,
                AgentIds: selectedAgents,
                ListIds: selectedLists,
                WorkingHourId: workingHour
            };

            abp.ui.setBusy(_$modal);
            _regionService.assignRegion(model).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see edited assignRegion!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        }

        //Handle save button click
        _$form.closest('div.modal-content').find(".save-button").click(function (e) {
            e.preventDefault();
            save();
        });

        //Handle enter key
        /*_$form.find('input').on('keypress', function (e) {
            if (e.which === 13) {
                e.preventDefault();
                save();
            }
        });*/

        $.AdminBSB.input.activate(_$form);

        //On load event for modal
        /*_$modal.on('shown.bs.modal', function () {
            selectedRoutes = [];

            _$form.find('input[type=text]:first').focus();

        });*/
    });
})();