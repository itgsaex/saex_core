﻿(function () {
    $(function () {
        var _messageService = abp.services.app.message;
        var _$modal = $('#MessageCreateModal');
        var _$form = _$modal.find('form');
        $('.selectpicker').selectpicker();

        _$form.validate({
        });

        $('#RefreshButton').click(function () {
            refreshMessageList();
        });

        $('.delete-message').click(function () {
            var messageId = $(this).attr("data-message-id");
            var messageName = $(this).attr('data-message-name');

            deleteMessage(messageId, messageName);
        });

        $('.edit-message').click(function (e) {
            var messageId = $(this).attr("data-message-id");

            e.preventDefault();
            $.ajax({
                url: abp.appPath + 'Messages/EditMessageModal?id=' + messageId,
                type: 'POST',
                contentType: 'html',
                success: function (content) {
                    $('#MessageEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        _$form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();
            if (!_$form.valid()) {
                return;
            }

            let message = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
            //debugger;
            abp.ui.setBusy($("#MessageCreateModalContent"));
            let agents = $('.selectpicker').selectpicker('val');
            for (let i = 0; i < agents.length; i++) {
                if (agents[i] == "")
                    continue;
                message.RecipientId = agents[i];
                _messageService.create(message).done(() => {
                    if (i == agents.length - 1) {
                        _$modal.modal('hide');
                        location.reload(true); //reload page to see new message!
                    }
                }).always(() => {
                    if (i == agents.length - 1) {
                        abp.ui.clearBusy($("#MessageCreateModalContent"));
                    }
                });
            }
           
        });

        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        function refreshMessageList() {
            location.reload(true); //reload page to see new message!
        }

        function deleteMessage(messageId, messageName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Saex'), messageName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _messageService.delete(messageId).done(function () {
                            refreshMessageList();
                        });
                    }
                }
            );
        }

        $('.reply-message').click(function (e) {
            e.preventDefault();

            var id = $(this).attr("data-reply-message-id");

            var text = $('#' + id).find('#reply-' + id).val();

            if (!text || text === "")
                return;

            abp.ui.setBusy($('#form-' + id));

            var message = {
                messageId: id,
                body: text
            };

            _messageService.reply(message).done(function () {
                location.reload(true);
            }).always(function () {
                abp.ui.clearBusy($('#form-' + id));
            });
        });
    });
})();