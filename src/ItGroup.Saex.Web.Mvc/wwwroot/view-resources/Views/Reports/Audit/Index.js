﻿(function () {
    $(function () {
        function getSearchTerm(name) {

            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        function format(d) {
            return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>Previous Value:</td>' +
                '<td>' + d.previousValue + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>New Value:</td>' +
                '<td>' + d.newValue + '</td>' +
                '</tr>' +
                '</table>';
        }

        var table = $('#dataTable').DataTable({
           "bPaginate": true,
           "bFilter": true,
           "bSort": false,
           "bInfo": true,
           "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                'print', 'csv', 'excel', 'pdf'
            ],
            /*columnDefs: [
                { data: "creatorUsername", title: 'User', targets: ['creatorUsername'] },
                { data: 'operationType', title: 'Operation', targets: ['operationType'] },
                { data: 'tableName', title: 'Table Name', targets: ['tableName'] },
                { data: 'creationTime', title: 'Date', targets: ['creationTime'] },
                { data: 'previousValue', title: 'Previous Value', targets: ['previousValue'], visible: true },
                { data: 'newValue', title: 'New Value', targets: ['newValue'], visible: true }
            ],*/
            columns: [
                { data: "creatorUsername" },
                { data: 'creationTime' },
                { data: 'tableName' },
                { data: 'operationType' },
                { data: 'description' },
                { data: 'changes' }
            ],
            "order": [[1, 'asc']],
            "ajax": {
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: abp.appPath + "api/services/app/Report/Audit", //"/api/services/Report/Audit",
                dataSrc: "result.items",
                data: function (input) {
                    return JSON.stringify({
                        "searchText": getSearchTerm('SearchText'),
                        "SectionName": getSearchTerm('SectionName'),
                        "StartDate": getSearchTerm('StartDate'),
                        "EndDate": getSearchTerm('EndDate'),
                        "DateRangeType": getSearchTerm('DateType') === '' ? 0 : getSearchTerm('DateType'),
                        "UserId": getSearchTerm('User') === '' ? 0 : getSearchTerm('User'),
                        "SelectedAuditLogTable": getSearchTerm('SelectedAuditLogTable'),
                        "skipCount": input.start,
                        "maxResultCount": input.length
                    });
                }
            }
        });

        $('#dataTable tbody').on('click', 'td.details-control', function () {

            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });
    });
})();