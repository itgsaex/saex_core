﻿(function () {
    $(function () {
        var mUrl = abp.appPath + "Reports/CaseRecentActivities?caseNumber=";
        
        function getSearchTerm(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        function format(d) {
            return '';
        }

        var table = $('#dataTable').DataTable({
            "scrollY": "500px",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": false,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            colReorder: true,
            stateSave: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                if (column == 0) {
                                    data = node.children[0].value;
                                }

                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                if (column == 0) {
                                    data = node.children[0].value;
                                }

                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                if (column == 0) {
                                    data = node.children[0].value;
                                } 
                                return data;
                                //return '\u200C' + data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'TABLOID',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                if (column == 0) {
                                    data = node.children[0].value;
                                }

                                return data;
                            }
                        }
                    }
                }
            ],
            'columnDefs': [
                {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, row) {
                    return '<input type="button" id="' + row.casoID + '" name="' + row.casoID + '" class="btnMandatory" value="' + (row.obligatorio === true ? "YES" : "NO") + '" class="btnChk" data-caso-id="' + row.casoID + '" data-current-value=' + row.obligatorio + ' style="border:1; background:transparent; color:' + (row.obligatorio === true ? "green" : "red") + '" />';
                }
            }],
            /* First Bank */
            //columns: [
            //    { data: 'obligatorio' },
            //    { data: null, render: function (data, type, row) { return '<a class="btn btn-info btn-sm view-activities" data-caso-id="' + row.casoID + '" href="' + mUrl + row.casoID + '" data-target="#CaseRecentActivitiesModal" target="_blank">View Activity</a>'; } }, //defaultContent: "<button type=\"button\" data-toggle=\"modal\" data-target=\"#ShowActivitiesModal\" />" },
            //    { data: 'nombre' },
            //    { data: 'numeroCuenta' },
            //    { data: 'zipCode' },
            //    { data: "productNumber" },
            //    { data: 'montoVencido' },
            //    { data: 'caseAssignType' }
            //],
            /* Banco Popular */
            columns: [
                { data: 'obligatorio' },
                { data: 'nombre' },
                { data: 'numeroCuenta' },
                { data: 'zipCode' },
                { data: 'stateCode' },
                { data: 'ruta' },
                { data: 'diasVencimiento' },
                { data: 'etapasDelincuencia' },
                { data: 'montoVencido' },
                { data: 'paymentAmount' },
                { data: 'totalDELQAmount' },
                { data: 'fechaEntradaCACS' },
                { data: 'estatusSistema' },
                { data: 'lastActivityCode' },
                { data: 'lastPaymentDate' },
                { data: 'comentarioExterno' },
                { data: 'comentarioInterno' },
                { data: 'caseAssignType' }
            ],
            "order": [[1, 'asc']],
            "ajax": {
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: abp.appPath + "api/services/app/Report/Cases",
                dataSrc: "result.items",
                data: function (input) {
                    return JSON.stringify({
                        "Name": getSearchTerm('Name'),
                        "AccountNumber": getSearchTerm('AccountNumber'),
                        "AgentId": getSearchTerm('AgentId'),
                        "DelinquencyStateId": getSearchTerm('DelinquencyStateId'),
                        "PostalCodeId": getSearchTerm('PostalCodeId'),
                        "RegionId": getSearchTerm('RegionId'),
                        "RouteId": getSearchTerm('RouteId'),
                        "AssignType": getSearchTerm('AssignType')
                    });
                }
            }
        });

        $('#dataTable').delegate(".btnMandatory", "click", function () {
            // Function to change the appearance of the button in the page.
            function SetButtonValue(button) {
                var newValue = button.value;
                button.value = newValue === "YES" ? "NO" : "YES";
                button.style.color = newValue === "YES" ? "red" : "green";
            }
            var casoId = $(this).attr("data-caso-id");
            var currentValue = $(this).attr("data-current-value");
            var newValue = currentValue === "true" ? "false" : "true";
            document.getElementsByName(casoId).forEach(SetButtonValue);

            // Process to update the case record in the database.
            $.ajax({
                url: abp.appPath + 'api/services/app/CaseActivity/SetCaseMandatory?caseNumber=' + casoId + '&newValue=' + newValue,
                type: 'POST',
                contentType: 'html',
                success: function (content) { },
                error: function (e) { }
            });
        });

        $('#dataTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        $("#reset-button").click((e) => {
            e.preventDefault();
            table.colReorder.reset();
        });

    });
})();