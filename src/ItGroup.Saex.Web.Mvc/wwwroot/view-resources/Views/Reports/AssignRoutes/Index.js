﻿(function () {
    $(function () {
        var _internalAgentDashboardService = abp.services.app.internalAgentDashboardService;
        var _$form = $('#searchActivityForm');


        function getSearchTerm(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        function format(d) {
            return '';
        }

        var dataTable = $('#dataTable').DataTable({
            "scrollY": "500px",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return '\u200C' + data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                }
            ],
            "columnDefs": [
                { "width": "100px", "targets": 0 },
                { "width": "250px", "targets": 1 },
                { "width": "100px", "targets": 2 },
                { "width": "100px", "targets": 3 },
                { "width": "250px", "targets": 4 },
                { "width": "100px", "targets": 5 },
                { "width": "100px", "targets": 6 }
            ]
        });

        $("#search-button").click(() => {
            var model = _$form.serializeFormToObject();
            abp.ui.setBusy($('#searchContainer'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'Reports/RutasAsignadasSearch',
                data: model
            }).done((e, x) => {
                let table = $("#dataTable").find('tbody');
                dataTable.destroy();
                table.empty();
                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        items = items.concat(`<tr>
                                         <td>${item.numeroCuenta}</td>
                                         <td>${item.descripcion}</td>
                                         <td>${item.zipCode}</td>
                                         <td>${item.rutaActual}</td>
                                         <td>${item.agente}</td>
                                         <td>${item.estatus}</td>
                                         <td>${item.transferOrShared}</td>
                                    </tr>`);
                    });
                }
                table.append(items);
                dataTable = $('#dataTable').DataTable({
                    "scrollY": "500px",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "autoWidth": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return '\u200C' + data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        }
                    ],
                    "columnDefs": [
                        { "width": "100px", "targets": 0 },
                        { "width": "250px", "targets": 1 },
                        { "width": "100px", "targets": 2 },
                        { "width": "100px", "targets": 3 },
                        { "width": "250px", "targets": 4 },
                        { "width": "100px", "targets": 5 },
                        { "width": "100px", "targets": 6 }
                    ]
                });

            }).always((e, x) => {
                abp.ui.clearBusy($('#searchContainer'));
            });
        });
    });
})();