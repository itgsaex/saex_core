﻿(function () {
    $(function () {
        var _save = document.getElementById("save-button");

        var myRows = [];

        _save.addEventListener("click", function () {
            $(this).focus();

            abp.ui.setBusy($('#searchContainer'));

            var $fieldId = 0;
            var $chkSel = false;
            var $chkMas = false;
            var $chkChi = false;
            var $chkRis = false;
            var $category = 0;
            var $sequence = 0;

            var rows = $("tbody tr").each(function (row) {
                $cells = $(this).find("td");
                myRows[row] = {};
                $cells.each(function (col) {
                    var input = $(this).find("input");
                    switch (col) {
                        case 0:
                            $fieldId = input.attr("fieldid");
                            $chkSel = input.is(":checked");
                            break;

                        case 4:
                            $chkMas = input.is(":checked")
                            break;

                        case 5:
                            $chkChi = input.is(":checked");
                            break;

                        case 6:
                            $chkRis = input.is(":checked");
                            break;

                        case 7:
                            var options = $(this).find("option");
                            for (var i = 0; i < options.length; i++) {
                                var option = options[i];
                                if (option.selected) {
                                    $category = option.value;
                                    break;
                                }
                            }
                            break;

                        case 8:
                            $sequence = input.val();
                            break;

                        default:
                    }
                });

                myRows[row]["data"] = JSON.stringify({ "FieldId": $fieldId, "Selected": $chkSel, "MasterList": $chkMas, "ChildList": $chkChi, "RiskList": $chkRis, "CategoriaId": $category, "Sequence": $sequence });
            })

            var myObj = {};
            myObj.myrows = myRows;
            var mData = "[ ";
            for (var i = 0; i < myRows.length; i++) {
                var info = myRows[i]
                mData = mData + info.data + ",";
            }
            mData = mData + " ]";

            $.ajax({
                url: abp.appPath + 'api/services/app/CaseActivity/SaveCaseFields',
                type: 'POST',
                contentType: 'application/json',
                data: mData,
                success: function (content) { abp.notify.success("Se grabo exitosamente."); abp.ui.clearBusy($('#searchContainer')); },
                error: function (e) { alert("An error ocurred while saving fields information."); abp.ui.clearBusy($('#searchContainer')); }
            });
        });

        var mUrl = abp.appPath + "ValidFieldsForLists/Index?product=";

        function getSearchTerm(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        function format(d) {
            return '';
        }

        function save() {

            var caseFields = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
            
            abp.ui.setBusy(_$form);
            abp.ui.clearBusy(_$modal);
        }

        $('#myTable').DataTable({
            "autoWidth": true,
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {

                                if (column == 0 || column == 4 || column == 5 || column == 6) {
                                    var input = node.children[0].children[0];
                                    if (input.checked == true)
                                        return "Si";
                                    else
                                        return "No";
                                } else if (column == 7) {
                                    var input = node.children[0].children[0].children[0];
                                    return input.innerText.trim();
                                } else if (column == 8) {
                                    var input = node.children[0];
                                    return input.value;
                                } else {
                                    return data;
                                }

                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {

                                if (column == 0 || column == 4 || column == 5 || column == 6) {
                                    var input = node.children[0].children[0];
                                    if (input.checked == true)
                                        return "Si";
                                    else
                                        return "No";
                                } else if (column == 7) {
                                    var input = node.children[0].children[0].children[0];
                                    return input.innerText.trim();
                                } else if (column == 8) {
                                    var input = node.children[0];
                                    return input.value;
                                } else {
                                    return data;
                                }

                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {

                                if (column == 0 || column == 4 || column == 5 || column == 6) {
                                    var input = node.children[0].children[0];
                                    if (input.checked == true)
                                        return "Si";
                                    else
                                        return "No";
                                } else if (column == 7) {
                                    var input = node.children[0].children[0].children[0];
                                    return input.innerText.trim();
                                } else if (column == 8) {
                                    var input = node.children[0];
                                    return input.value;
                                } else {
                                    return data;
                                }

                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'TABLOID',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {

                                if (column == 0 || column == 4 || column == 5 || column == 6) {
                                    var input = node.children[0].children[0];
                                    if (input.checked == true)
                                        return "Si";
                                    else
                                        return "No";
                                } else if (column == 7) {
                                    var input = node.children[0].children[0].children[0];
                                    return input.innerText.trim();
                                } else if (column == 8) {
                                    var input = node.children[0];
                                    return input.value;
                                } else {
                                    return data;
                                }

                            }
                        }
                    }
                }
            ]
        });

        $('#myTable tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });
    });
})();