﻿(function () {
    $(function () {
        var _$form = $('#searchForm');
        var _save = document.getElementById("save-button");

        function getSearchTerm(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        function format(d) {
            return '';
        }

        var dataTable = $('#dataTable').DataTable({
            "scrollY": "250",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return '\u200C' + data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                }
            ],
            "columnDefs": [
                { "width": "100px", "targets": 0 },
                { "width": "300px", "targets": 1 },
                { "width": "50px", "targets": 2 },
                { "width": "300px", "targets": 3 },
                { "width": "150px", "targets": 4 },
                { "width": "100px", "targets": 5 },
                { "width": "250px", "targets": 6 },
                { "width": "250px", "targets": 7 },
                { "width": "500px", "targets": 8 }
            ],
            "order": [[0, 'asc']],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({ page: 'current' }).nodes();
                var last = null;

                api.column(0, { page: 'current' }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="8">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        var plannedCasesWithoutVisitdataTable = $('#PlannedCasesWithoutVisitdataTable').DataTable({
            "scrollY": "250",
            "scrollX": "0px",
            "bPaginate": false,
            "bFilter": true,
            "bSort": false,
            "bInfo": true,
            "serverSide": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return '\u200C' + data;
                            }
                        }
                    }
                },
                {
                    extend: 'pdfHtml5',
                    customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                return data;
                            }
                        }
                    }
                }
            ],
            "columnDefs": [
                { "visible": false, "targets": 0 },
                { "width": "50px", "targets": 1 },
                { "width": "300px", "targets": 2 },
                { "width": "300px", "targets": 3 },
                { "width": "150px", "targets": 4 },
                { "width": "100px", "targets": 5 },
                { "width": "100px", "targets": 6 }
            ],
            "order": [[0, 'asc']],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({ page: 'current' }).nodes();
                var last = null;

                api.column(0, { page: 'current' }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="6">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        $("#search-button").click(() => {
            var model = _$form.serializeFormToObject();

            // Search DailyReport
            abp.ui.setBusy($('#container'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'DailyReport/Search',
                data: model
            }).done((e, x) => {
                let table = $("#dataTable").find('tbody');
                dataTable.destroy();
                table.empty();
                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        items = items.concat(`<tr>
                                         <td width="500">${formatDateWithoutHour(item.fecha)}</td>
                                         <td>${item.activityCodeDesc}</td>
                                         <td>${item.diasVencimiento}</td>
                                         <td>${item.numeroCuenta}</td>
                                         <td>${item.productNumber}</td>
                                         <td>${item.pueblo}</td>
                                         <td>${item.tipoContacto}</td>
                                         <td>${item.lugarVisitado}</td>
                                         <td>${item.comentario}</td>
                                    </tr>`);
                    });
                }
                table.append(items);
                dataTable = $('#dataTable').DataTable({
                    "scrollY": "250px",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return '\u200C' + data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        }
                    ],
                    "columnDefs": [
                        { "visible":false, "targets": 0 },
                        { "width": "300px", "targets": 1 },
                        { "width": "50px", "targets": 2 },
                        { "width": "300px", "targets": 3 },
                        { "width": "150px", "targets": 4 },
                        { "width": "100px", "targets": 5 },
                        { "width": "250px", "targets": 6 },
                        { "width": "250px", "targets": 7 },
                        { "width": "500px", "targets": 8 }
                    ],
                    "order": [[0, 'asc']],
                    "drawCallback": function (settings) {
                        var api = this.api();
                        var rows = api.rows({ page: 'current' }).nodes();
                        var last = null;

                        api.column(0, { page: 'current' }).data().each(function (group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before(
                                    '<tr class="group"><td colspan="8">' + group + '</td></tr>'
                                );

                                last = group;
                            }
                        });
                    }
                });

            }).always((e, x) => {
                abp.ui.clearBusy($('#container'));
            });

            // Search PlannedCaseWithoutVisit
            abp.ui.setBusy($('#PlannedCasesWithoutVisitdataTable'));
            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: abp.appPath + 'DailyReport/SearchPlannedCasesWithoutVisit',
                data: model
            }).done((e, x) => {
                let plannedCasesWithoutVisitTable = $("#PlannedCasesWithoutVisitdataTable").find('tbody');
                plannedCasesWithoutVisitdataTable.destroy();
                plannedCasesWithoutVisitTable.empty();
                let items = "";
                if (x.result) {
                    x.result.forEach(item => {
                        items = items.concat(`<tr>
                                         <td>${formatDateWithoutHour(item.fecha)}</td>
                                         <td>${item.diasVencimiento}</td>
                                         <td>${item.numeroCuenta}</td>
                                         <td>${item.nombre}</td>
                                         <td>${item.productNumber}</td>
                                         <td>${item.pueblo}</td>
                                         <td>${item.zipCode}</td>
                                    </tr>`);
                    });
                }
                plannedCasesWithoutVisitTable.append(items);
                plannedCasesWithoutVisitdataTable = $('#PlannedCasesWithoutVisitdataTable').DataTable({
                    "scrollY": "250px",
                    "scrollX": "0px",
                    "bPaginate": false,
                    "bFilter": true,
                    "bSort": false,
                    "bInfo": true,
                    "serverSide": false,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return '\u200C' + data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            customize: function (doc) { doc.pageMargins = [5, 5, 5, 5]; },
                            exportOptions: {
                                format: {
                                    body: function (data, row, column, node) {
                                        return data;
                                    }
                                }
                            }
                        }
                    ],
                    "columnDefs": [
                        { "visible": false, "targets": 0 },
                        { "width": "50px", "targets": 1 },
                        { "width": "300px", "targets": 2 },
                        { "width": "300px", "targets": 3 },
                        { "width": "150px", "targets": 4 },
                        { "width": "100px", "targets": 5 },
                        { "width": "100px", "targets": 6 }
                    ],
                    "order": [[0, 'asc']],
                    "drawCallback": function (settings) {
                        var api = this.api();
                        var rows = api.rows({ page: 'current' }).nodes();
                        var last = null;

                        api.column(0, { page: 'current' }).data().each(function (group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before(
                                    '<tr class="group"><td colspan="6">' + group + '</td></tr>'
                                );

                                last = group;
                            }
                        });
                    }
                });

            }).always((e, x) => {
                abp.ui.clearBusy($('#PlannedCasesWithoutVisitdataTable'));
            });
        });
    });
})();