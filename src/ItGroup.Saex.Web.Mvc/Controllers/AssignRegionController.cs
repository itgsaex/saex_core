using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_AssignRegions_View)]
    public class AssignRegionsController : SaexControllerBase
    {
        private readonly AgentAppService _agentService;

        private readonly RegionManager _regionManager;

        private readonly RegionAppService _regionService;

        private readonly ListAppService _listService;

        private readonly WorkingHourAppService _workingHourService;

        public AssignRegionsController(AgentAppService agentService, RegionManager regionManager, RegionAppService regionService, ListAppService listService, WorkingHourAppService workingHourService)
        {
            _agentService = agentService;
            _regionManager = regionManager;
            _regionService = regionService;
            _listService = listService;
            _workingHourService = workingHourService;
        }

        [HttpPost]
        public async Task<ActionResult> AssignRegionModal()
        {
            var output = new AssignRegionModalViewModel();
            output.AvailableRegions = await _regionService.GetAllForList();
            output.Agents = await _agentService.GetAgentsWithOutAssign();
            output.AvailableLists = await _listService.GetAllForList();
            output.AvailableWorkingHours = await _workingHourService.GetAllForList();

            return View("_AssignRegionModal", output);
        }

        [HttpPost]
        public async Task<ActionResult> EditAssignRegionModal(Guid id)
        {
            var assignedRegions = await _regionService.GetAssignedRegions(id);
            
            var regions = new List<RegionListDto>();

            if (assignedRegions.Regions != null)
            {

                regions = await _regionManager
                                        .GetAllQuery()
                                            .Select(x =>
                                                new RegionListDto(
                                                      x,
                                                      assignedRegions.Regions.Any(z => z.Id == x.Id)))
                                            .ToListAsync();
            } else
            {
                regions = await _regionManager
                                        .GetAllQuery()
                                            .Select(x =>
                                                new RegionListDto(
                                                      x,null))
                                            .ToListAsync();
            }

            var lists = new List<ListTableDto>();

            if (assignedRegions.Lists != null)
            {

                lists = (await _listService.GetAllForList()).Select(x =>
                                                new ListTableDto(
                                                      x,
                                                      assignedRegions.Lists.Any(z => z.Id == x.Id)))
                                            .ToList();
            } else
            {
                lists = (await _listService.GetAllForList()).Select(x => new ListTableDto(x)).ToList();
            }

            var workingHours = new List<WorkingHourDto>();
            if (assignedRegions.WorkingHour != null)
            {
                workingHours = (await _workingHourService.GetAllForList()).Select(x => new WorkingHourDto(x, x.Id == assignedRegions.WorkingHour.Id)).ToList();
            } 
            else
            {
                workingHours = await _workingHourService.GetAllForList();
            }

            var output = new EditAssignRegionModalViewModel();
            output.AvailableRegions = regions;
            output.AvailableLists = lists;
            output.Agent = assignedRegions.Agent;
            output.AvailableWorkingHours = workingHours;

            return View("_EditAssignRegionModal", output);
        }

        public async Task<IActionResult> Index(AssignRegionSearchModel input)
        {
            var assignedRegions = await _regionService.GetAllAssignedRegions(input);

            var regions = await _regionService.GetAllForList();

            var dto = new PagedResultDto<AssignRegionDto>()
            {
                Items = assignedRegions.Items.ToList(),
                TotalCount = assignedRegions.TotalCount
            };

            var model = new AssignRegionListViewModel(dto, regions);

            model.CanModify = await CanModify(PermissionNames.Pages_AssignRegions_Modify);

            return View(model);
        }
    }
}
