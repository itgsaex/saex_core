using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_SystemPolicies_View)]
    public class SystemPoliciesController : SaexControllerBase
    {
        private readonly ISystemPolicyAppService _systemPolicyAppService;

        public SystemPoliciesController(ISystemPolicyAppService systemPolicyAppService)
        {
            _systemPolicyAppService = systemPolicyAppService;
        }

        public async Task<ActionResult> EditSystemPolicyModal(Guid id)
        {
            var output = await _systemPolicyAppService.Get(id);
            var model = ObjectMapper.Map<EditSystemPolicyModalViewModel>(output);

            return View("_EditSystemPolicyModal", model);
        }

        public async Task<IActionResult> Index(SystemPolicySearchModel input)
        {
            var results = await _systemPolicyAppService.Search(input);

            var model = new SystemPolicyListViewModel(results);

            model.CanModify = await CanModify(PermissionNames.Pages_SystemPolicies_Modify);

            return View(model);
        }
    }
}
