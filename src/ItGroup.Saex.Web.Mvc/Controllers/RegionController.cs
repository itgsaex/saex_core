using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Regions_View)]
    public class RegionsController : SaexControllerBase
    {
        private readonly ProductManager _productManager;

        private readonly IRegionAppService _regionAppService;

        private readonly RouteManager _routeManager;

        public RegionsController(IRegionAppService regionAppService, RouteManager routeManager, ProductManager productManager)
        {
            _regionAppService = regionAppService;
            _routeManager = routeManager;
            _productManager = productManager;
        }

        public async Task<ActionResult> EditRegionModal(Guid id)
        {
            var region = await _regionAppService.Get(id);

            var routes = await _routeManager.GetAll().Select(x => new RouteListDto(x)).ToListAsync();

            var output = new EditRegionModalViewModel(routes, region);

            return View("_EditRegionModal", output);
        }

        public async Task<IActionResult> Index(RegionSearchModel input)
        {
            var regions = await _regionAppService.Search(input);

            var routes = await _routeManager.GetAll().Select(x => new RouteListDto(x)).ToListAsync();

            var model = new RegionListViewModel(regions, routes);

            model.CanModify = await CanModify(PermissionNames.Pages_Regions_Modify);

            return View(model);
        }
    }
}
