using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_SystemStates_View)]
    public class SystemStatesController : SaexControllerBase
    {
        private readonly IMacroStateAppService _macroStateAppService;

        private readonly ISystemSubStateAppService _subStateAppService;

        private readonly ISystemStateAppService _systemStateAppService;

        public SystemStatesController
        (
            ISystemStateAppService systemStateAppService,
            IMacroStateAppService macroStateAppService,
            ISystemSubStateAppService subStateAppService
        )
        {
            _systemStateAppService = systemStateAppService;
            _macroStateAppService = macroStateAppService;
            _subStateAppService = subStateAppService;
        }

        public async Task<ActionResult> EditSystemStateModal(Guid id)
        {
            var res = await _systemStateAppService.Get(id);
            var model = new EditSystemStateModalViewModel(res);
            model.MacroStates = await _macroStateAppService.GetAllForList();
            model.SubStates = await _subStateAppService.GetAllForList();

            return View("_EditSystemStateModal", model);
        }

        public async Task<IActionResult> Index(SystemStateSearchModel input)
        {
            var systemStates = await _systemStateAppService.Search(input);

            var model = new SystemStateListViewModel(systemStates);

            model.MacroStates = await _macroStateAppService.GetAllForList();

            model.SubStates = await _subStateAppService.GetAllForList();

            model.CanModify = await CanModify(PermissionNames.Pages_SystemStates_Modify);

            return View(model);
        }
    }
}
