using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Web.ViewModels;
using ItGroup.Saex.Users;
using System.Linq;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using System.Collections.Generic;
using System;
using ItGroup.Saex.Models;
using Abp.Runtime.Session;
using Abp.Domain.Uow;

namespace ItGroup.Saex.Web.Controllers
{
    public class ReportsController : SaexControllerBase
    {
        private readonly IAgentAppService _agentAppService;

        private readonly IDelinquencyStateAppService _delinquencyStateAppService;

        private readonly IPostalCodeAppService _postalCodeAppService;

        private readonly IRegionAppService _regionAppService;

        private readonly IReportAppService _reportAppService;

        private readonly IRouteAppService _routeAppService;

        private readonly IUserAppService _userAppService;

        private readonly IProductAppService _productAppService;

        private readonly ISystemPolicyAppService _systemPolicyAppService;

        private readonly ICaseActivityAppService _caseActivityAppService;

        private readonly IAbpSession _abpSession;

        private readonly IAssignInternalAgentsAppService _assignInternalAgentsAppService;

        private readonly ICaseRecentActivityRepository _caseRecentActivityRepository;

        public ReportsController(
            IReportAppService reportAppService,
            IUserAppService userAppService,
            IRegionAppService regionAppService,
            IRouteAppService routeAppService,
            IPostalCodeAppService postalCodeAppService,
            IDelinquencyStateAppService delinquencyStateAppService,
            IAgentAppService agentAppService,
            IProductAppService productAppService,
            ISystemPolicyAppService systemPolicyAppService, 
            ICaseActivityAppService caseActivityAppService,
            IAbpSession abpSession,
            IAssignInternalAgentsAppService assignInternalAgentsAppService,
            ICaseRecentActivityRepository caseRecentActivityRepository)
        {
            _reportAppService = reportAppService;
            _userAppService = userAppService;
            _agentAppService = agentAppService;
            _regionAppService = regionAppService;
            _routeAppService = routeAppService;
            _postalCodeAppService = postalCodeAppService;
            _delinquencyStateAppService = delinquencyStateAppService;
            _productAppService = productAppService;
            _systemPolicyAppService = systemPolicyAppService;
            _caseActivityAppService = caseActivityAppService;
            _abpSession = abpSession;
            _assignInternalAgentsAppService = assignInternalAgentsAppService;
            _caseRecentActivityRepository = caseRecentActivityRepository;
        }

        [AbpMvcAuthorize(PermissionNames.Pages_Audit_Report_View)]
        public async Task<IActionResult> Audit(AuditReportSearchModel input)
        {
            // var reports = await _reportAppService.Audit(input);
            var auditLogsTable = await _reportAppService.AuditLogTables();
            var result =  _userAppService.CustomGetAll().OrderBy(u => u.FullName).ToList();

            var model = new AuditReportViewModel(result);
            model.AuditLogTables = auditLogsTable;

            return View(model);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_Cases_Report_View)]
        public async Task<IActionResult> Cases(CaseReportSearchModel input)
        {
            var agents = (await _agentAppService.GetAllAgentsForReport()).Select(a => new AgentListDto(a)).ToList();
            var regions = await _regionAppService.GetAllForList();
            var routes = await _routeAppService.GetAllForList();
            var postalCodes = await _postalCodeAppService.GetAllForList();
            var paramDelinquecyStages = (await _systemPolicyAppService.GetAllSystemPolicies()).FirstOrDefault(x => x.Code == "DELINQUENCY_STAGES");

            var delinquencyStates = new List<string>();
            if (paramDelinquecyStages != null)
            {
                delinquencyStates.AddRange(paramDelinquecyStages.Value.Split("|"));
            }
            var cases = await _reportAppService.Cases(input);

            var model = new CaseReportViewModel(cases.Items.ToList(), agents, delinquencyStates, postalCodes, regions, routes, input.AccountNumber, input.Name);

            return View(model);
        }

        //[AbpMvcAuthorize(PermissionNames.Pages_Case_Recent_Activity_Report_View)]
        public async Task<IActionResult> CaseRecentActivities(string caseNumber)
        {
            var result = await _caseActivityAppService.GetRecentActivities(caseNumber);

            var model = new CaseRecentActivityViewModel(caseNumber, result);

            return View("CaseRecentActivities", model);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_Users_Report)]
        public async Task<IActionResult> Users(UsersReportRequest request)
        {
            var viewModel = new UsersReportViewModel
            {
                ReportData = await _reportAppService.GetUsersReportAsync(request)
            };

            return View("Users", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_KPI_Report_View)]
        public async Task<IActionResult> KPI()
        {
           var agents = await _agentAppService.GetAllAgentsForReport();
            List<string> DateTypes = new List<string>();
            DateTypes.Add("Manual");
            DateTypes.Add("Hoy");
            DateTypes.Add("Semana");
            DateTypes.Add("Mes");
            var viewModel = new KPIReportViewModel();
            viewModel.AgentDtos = agents;
            viewModel.DateTypes = DateTypes;
            viewModel.DateType = "Hoy";
            viewModel.DateFrom = DateTime.Now;
            viewModel.DateTo = DateTime.Now;

            return View("KPIReport", viewModel);
        }

        public async Task<IActionResult> KPIPost(KPIReportViewModel viewModel)
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            List<string> DateTypes = new List<string>();
            DateTypes.Add("Manual");
            DateTypes.Add("Hoy");
            DateTypes.Add("Semana");
            DateTypes.Add("Mes");
            viewModel.AgentDtos = agents;
            viewModel.DateTypes = DateTypes;
            viewModel.SelectedAgent = agents.Where(a => a.Id == viewModel.SelectedAgentID).FirstOrDefault();
            return View("KPIReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_AssignAccounts_Report_View)]
        public async Task<IActionResult> AssignAccountsReport()
        {
            var agents = (await _agentAppService.GetAllAgentsForReport()).Select(a => new AgentListDto(a)).ToList();
            var regions = await _regionAppService.GetAllForList();
            var routes = await _routeAppService.GetAllForList();
            var model = new AssignAccountsReportViewModel();

            model.Agents = agents;
            model.Regions = regions;
            model.Routes = routes;

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> AssignReportSearch(ReadCuentasAsignadasParameterModel model)
        {
            var result = await _caseRecentActivityRepository.ReadCuentasAsignadas(model);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        public async Task<IActionResult> AssignAccountsReportPost(AssignAccountsReportViewModel viewModel)
        {
            var agents = (await _agentAppService.GetAllAgentsForReport()).Select(a => new AgentListDto(a)).ToList();
            var regions = await _regionAppService.GetAllForList();
            var routes = await _routeAppService.GetAllForList();
            
            viewModel.Agents = agents;
            viewModel.Regions = regions;
            viewModel.Routes = routes;

            return View("AssignAccountsReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_DuplicateAccounts_Report_View)]
        public async Task<IActionResult> DuplicateAccountsReport()
        {
            var agents = (await _agentAppService.GetAllAgentsForReport()).Select(a => new AgentListDto(a)).ToList();
            var regions = await _regionAppService.GetAllForList();
            var routes = await _routeAppService.GetAllForList();
            var model = new AssignAccountsReportViewModel();

            model.Agents = agents;
            model.Regions = regions;
            model.Routes = routes;

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> DuplicateAccountsSearch(ReadDuplicateAccountsParameterModel model)
        {
            var result = await _caseRecentActivityRepository.ReadDuplicateAccounts(model);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        public async Task<IActionResult> DuplicateAccountsReportPost(AssignAccountsReportViewModel viewModel)
        {
            var agents = await _agentAppService.GetAllForList();
            var regions = await _regionAppService.GetAllForList();
            var routes = await _routeAppService.GetAllForList();

            viewModel.Agents = agents;
            viewModel.Regions = regions;
            viewModel.Routes = routes;

            return View("DuplicateAccountsReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_AssignRoutes_Report_View)]
        public IActionResult RutasAsignadas()
        {
            var model = new RutasAsignadasParameterModel();

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> RutasAsignadasSearch(RutasAsignadasParameterModel model)
        {
            model.ToDate = model.ToDate.AddDays(1);
            var result = await _caseRecentActivityRepository.RutasAsignadas(model);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        [AbpMvcAuthorize(PermissionNames.Pages_UnassignedCases_Report_View)]
        public IActionResult UnassignedCases()
        {
            var products = _productAppService.GetAllProducts();
            var viewModel = new UnassignedCasesViewModel();
            viewModel.Products = products;
            viewModel.ZipCode = "";
            viewModel.AccountNumber = "";
            viewModel.ClientName = "";
            return View("UnassignedCasesReport", viewModel);
        }

        public IActionResult UnassignedCasesPost(UnassignedCasesViewModel viewModel)
        {
            viewModel.Products = _productAppService.GetAllProducts();
            viewModel.SelectedProduct = viewModel.Products.Where(a => a.Code.Equals(viewModel.SelectedProductCode)).FirstOrDefault();
            return View("UnassignedCasesReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_UnfilteredCases_Report_View)]
        public IActionResult UnfilteredCasesByMaster()
        {
            var products = _productAppService.GetAllProducts();
            var viewModel = new UnfilteredCasesByMasterViewModel();
            viewModel.Products = products;
            viewModel.AccountNumber = "";
            viewModel.ClientName = "";
            return View("UnfilteredCasesByMasterReport", viewModel);
        }

        public IActionResult UnfilteredCasesByMasterPost(UnfilteredCasesByMasterViewModel viewModel)
        {
            viewModel.Products = _productAppService.GetAllProducts();
            viewModel.SelectedProduct = viewModel.Products.Where(a => a.Code.Equals(viewModel.SelectedProductCode)).FirstOrDefault();
            return View("UnfilteredCasesByMasterReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_TransferredCases_Report_View)]
        public IActionResult TransferredCases()
        {
            var viewModel = new TransferredCasesReportViewModel();
            return View("TransferredCasesReport", viewModel);
        }

        public IActionResult TransferredCasesPost(TransferredCasesReportViewModel viewModel)
        {            
            return View("TransferredCasesReport", viewModel);
        }


        [AbpMvcAuthorize(PermissionNames.Pages_AddressManagement_Report_View)]
        public IActionResult AddressManagement()
        {
            var viewModel = new AddressManagementReportViewModel();
            viewModel.DateFrom = DateTime.Now;
            viewModel.DateTo = DateTime.Now;
            return View("AddressManagementReport", viewModel);
        }

        public IActionResult AddressManagementReportPost(AddressManagementReportViewModel viewModel)
        {
            return View("AddressManagementReport", viewModel);
        }


        [AbpMvcAuthorize(PermissionNames.Pages_ReceivedVsAssignedCases_Report_View)]
        public IActionResult ReceivedVsAssignedCases()
        {
            return View("ReceivedVsAssignedCasesReport");
        }

        [AbpMvcAuthorize(PermissionNames.Pages_Profiles_Report_View)]
        public IActionResult Profiles()
        {
            return View("ProfilesReport");
        }

        [HttpPost("users")]
        [AbpMvcAuthorize(PermissionNames.Pages_Users_Report)]
        [ValidateAntiForgeryToken]
        public Task<IActionResult> SearchUsers(UsersReportRequest request) =>
            Users(request);

        [AbpMvcAuthorize(PermissionNames.Pages_AgentActivity_Report_View)]
        public async Task<IActionResult> AgentActivity()
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            var viewModel = new AgentActivityReportViewModel();
            viewModel.AgentDtos = agents;
            viewModel.DateFrom = DateTime.Now;
            viewModel.DateTo = DateTime.Now;

            return View("AgentActivityReport", viewModel);
        }

        public async Task<IActionResult> AgentActivityPost(AgentActivityReportViewModel viewModel)
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            viewModel.AgentDtos = agents;
            viewModel.SelectedAgent = agents.Where(a => a.Id == viewModel.SelectedAgentID).FirstOrDefault();
            return View("AgentActivityReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_BucketsDelinquencyMortgage_Report_View)]
        public async Task<IActionResult> BucketsDelinquencyMortgage()
        {
            var viewModel = new BucketsDelinquencyMortgageReportViewModel();

            List<string> aging = new List<string>();
            aging.Add("1 - 29");
            aging.Add("30 - 59");
            aging.Add("60 - 89");
            aging.Add("90 - 119");
            aging.Add("120 - 179");
            aging.Add("180+");
            viewModel.AgingDtos = aging;

            List<string> visited = new List<string>();
            visited.Add("Si");
            visited.Add("No");
            viewModel.VisitedDtos = visited;

            return View("BucketsDelinquencyMortgageReport", viewModel);
        }

        public async Task<IActionResult> BucketsDelinquencyMortgagePost(BucketsDelinquencyMortgageReportViewModel viewModel)
        {
            List<string> aging = new List<string>();
            aging.Add("1 - 29");
            aging.Add("30 - 59");
            aging.Add("60 - 89");
            aging.Add("90 - 119");
            aging.Add("120 - 179");
            aging.Add("180+");
            viewModel.AgingDtos = aging;

            List<string> visited = new List<string>();
            visited.Add("Si");
            visited.Add("No");
            viewModel.VisitedDtos = visited;

            return View("BucketsDelinquencyMortgageReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_BucketsDelinquencyMortgageWithIndicator_Report_View)]
        public async Task<IActionResult> BucketsDelinquencyMortgageWithIndicator()
        {
            var viewModel = new BucketsDelinquencyMortgageWithIndicatorReportViewModel();

            List<string> aging = new List<string>();
            aging.Add("1 - 29");
            aging.Add("30 - 59");
            aging.Add("60 - 89");
            aging.Add("90 - 119");
            aging.Add("120 - 179");
            aging.Add("180+");
            viewModel.AgingDtos = aging;

            List<string> visited = new List<string>();
            visited.Add("Si");
            visited.Add("No");
            viewModel.VisitedDtos = visited;

            List<string> indicator = new List<string>();
            var paramBucketsReportIndicators = (await _systemPolicyAppService.GetAllSystemPolicies()).FirstOrDefault(x => x.Code == "BUCKETS_REPORT_INDICATORS");
            if (paramBucketsReportIndicators != null)
            {
                indicator.AddRange(paramBucketsReportIndicators.Value.Split("|"));
            }
            viewModel.IndicatorDtos = indicator;

            return View("BucketsDelinquencyMortgageWithIndicatorReport", viewModel);
        }

        public async Task<IActionResult> BucketsDelinquencyMortgageWithIndicatorPost(BucketsDelinquencyMortgageWithIndicatorReportViewModel viewModel)
        {
            List<string> aging = new List<string>();
            aging.Add("1 - 29");
            aging.Add("30 - 59");
            aging.Add("60 - 89");
            aging.Add("90 - 119");
            aging.Add("120 - 179");
            aging.Add("180+");
            viewModel.AgingDtos = aging;

            List<string> visited = new List<string>();
            visited.Add("Si");
            visited.Add("No");
            viewModel.VisitedDtos = visited;

            List<string> indicator = new List<string>();
            var paramBucketsReportIndicators = (await _systemPolicyAppService.GetAllSystemPolicies()).FirstOrDefault(x => x.Code == "BUCKETS_REPORT_INDICATORS");
            if (paramBucketsReportIndicators != null)
            {
                indicator.AddRange(paramBucketsReportIndicators.Value.Split("|"));
            }
            viewModel.IndicatorDtos = indicator;

            return View("BucketsDelinquencyMortgageWithIndicatorReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_AgentLocationRegistry_Report_View)]
        public async Task<IActionResult> AgentLocationRegistry()
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            var viewModel = new AgentLocationRegistryReportViewModel ();
            viewModel.AgentDtos = agents;
            viewModel.DateFrom = DateTime.Now;
            viewModel.DateTo = DateTime.Now;

            return View("AgentLocationRegistryReport", viewModel);
        }

        public async Task<IActionResult> AgentLocationRegistryPost(AgentLocationRegistryReportViewModel viewModel)
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            viewModel.AgentDtos = agents;
            viewModel.SelectedAgent = agents.Where(a => a.Id == viewModel.SelectedAgentID).FirstOrDefault();
            return View("AgentLocationRegistryReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_KPISolvedCases_Report_View)]
        public async Task<IActionResult> KPISolvedCases()
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            var viewModel = new KPISolvedCasesReportViewModel ();
            viewModel.AgentDtos = agents;
            viewModel.DateFrom = DateTime.Now;
            viewModel.DateTo = DateTime.Now;

            return View("KPISolvedCasesReport", viewModel);
        }

        public async Task<IActionResult> KPISolvedCasesPost(KPISolvedCasesReportViewModel viewModel)
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            viewModel.AgentDtos = agents;
            viewModel.SelectedAgent = agents.Where(a => a.Id == viewModel.SelectedAgentID).FirstOrDefault();
            return View("KPISolvedCasesReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_DailyAgentActivityEffectiveness_Report_View)]
        public async Task<IActionResult> DailyAgentActivityEffectiveness()
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            var viewModel = new DailyAgentActivityEffectivenessReportViewModel();
            viewModel.AgentDtos = agents;
            viewModel.DateFrom = DateTime.Now;
            viewModel.DateTo = DateTime.Now;

            return View("DailyAgentActivityEffectivenessReport", viewModel);
        }

        public async Task<IActionResult> DailyAgentActivityEffectivenessPost(DailyAgentActivityEffectivenessReportViewModel viewModel)
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            viewModel.AgentDtos = agents;
            viewModel.SelectedAgent = agents.Where(a => a.Id == viewModel.SelectedAgentID).FirstOrDefault();
            return View("DailyAgentActivityEffectivenessReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_SummaryAgentActivityEffectiveness_Report_View)]
        public async Task<IActionResult> SummaryAgentActivityEffectiveness()
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            var viewModel = new SummaryAgentActivityEffectivenessReportViewModel();
            viewModel.AgentDtos = agents;
            viewModel.DateFrom = DateTime.Now;
            viewModel.DateTo = DateTime.Now;

            return View("SummaryAgentActivityEffectivenessReport", viewModel);
        }

        public async Task<IActionResult> SummaryAgentActivityEffectivenessPost(SummaryAgentActivityEffectivenessReportViewModel viewModel)
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            viewModel.AgentDtos = agents;
            viewModel.SelectedAgent = agents.Where(a => a.Id == viewModel.SelectedAgentID).FirstOrDefault();
            return View("SummaryAgentActivityEffectivenessReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_LoginAuditTrail_Report_View)]
        public async Task<IActionResult> LoginAuditTrail()
        {
            var viewModel = new LoginAuditTrailReportViewModel();
            viewModel.DateFrom = DateTime.Now;
            viewModel.DateTo = DateTime.Now;

            return View("LoginAuditTrailReport", viewModel);
        }

        public async Task<IActionResult> LoginAuditTrailPost(LoginAuditTrailReportViewModel viewModel)
        {
            return View("LoginAuditTrailReport", viewModel);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_Activity_Report_View)]
        public async Task<IActionResult> ActivityReport()
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            var viewModel = new ActivityReportViewModel();
            viewModel.AgentsFrom = agents;
            viewModel.AgentsTo = agents;
            viewModel.DateFrom = DateTime.Now;
            viewModel.DateTo = DateTime.Now;

            return View("ActivityReport", viewModel);
        }

        public async Task<IActionResult> ActivityReportPost(ActivityReportViewModel viewModel)
        {
            var agents = await _agentAppService.GetAllAgentsForReport();
            viewModel.AgentsFrom = agents;
            viewModel.AgentsTo = agents;

            if (viewModel.SelectedAgentFromID.HasValue)
            {
                viewModel.SelectedAgentFrom = agents.Where(a => a.Id == viewModel.SelectedAgentFromID).FirstOrDefault();
                viewModel.AgentFromUserID = viewModel.SelectedAgentFrom.UserId;
                viewModel.AgentFromName = viewModel.SelectedAgentFrom.User.FullName;
            } else
            {
                viewModel.SelectedAgentFrom = null;
                viewModel.AgentFromUserID = null;
                viewModel.AgentFromName = null;
            }

            if (viewModel.SelectedAgentToID.HasValue)
            {
                viewModel.SelectedAgentTo = agents.Where(a => a.Id == viewModel.SelectedAgentToID).FirstOrDefault();
                viewModel.AgentToUserID = viewModel.SelectedAgentTo.UserId;
                viewModel.AgentToName = viewModel.SelectedAgentTo.User.FullName;
            } else
            {
                viewModel.SelectedAgentTo = null;
                viewModel.AgentToUserID = null;
                viewModel.AgentToName = null;
            }

            var userRecord = await _assignInternalAgentsAppService.GetUserRecord(_abpSession.UserId.GetValueOrDefault());

            if (userRecord.Agent != null && userRecord.Agent.Type == Enumerations.Enumerations.AgentType.Supervisor)
            {
                viewModel.SupervisorID = userRecord.Id;
                viewModel.InternalAgentID = null;
                viewModel.SupervisorName = userRecord.FullName;
                viewModel.InternalAgentName = null;
            } else if (userRecord.Agent != null && userRecord.Agent.Type == Enumerations.Enumerations.AgentType.Internal)
            {
                viewModel.SupervisorID = null;
                viewModel.InternalAgentID = userRecord.Id;
                viewModel.SupervisorName = null;
                viewModel.InternalAgentName = userRecord.FullName;
            } else
            {
                viewModel.SupervisorID = null;
                viewModel.InternalAgentID = null;
                viewModel.SupervisorName = null;
                viewModel.InternalAgentName = null;
            }

            return View("ActivityReport", viewModel);
        }
    }
}
