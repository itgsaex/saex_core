using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_PaymentDelayReasons_View)]
    public class PaymentDelayReasonsController : SaexControllerBase
    {
        private readonly IPaymentDelayReasonAppService _paymentDelayReasonAppService;

        public PaymentDelayReasonsController(IPaymentDelayReasonAppService paymentDelayReasonAppService)
        {
            _paymentDelayReasonAppService = paymentDelayReasonAppService;
        }

        public async Task<ActionResult> EditPaymentDelayReasonModal(Guid id)
        {
            var output = await _paymentDelayReasonAppService.Get(id);
            var model = ObjectMapper.Map<EditPaymentDelayReasonModalViewModel>(output);

            return View("_EditPaymentDelayReasonModal", model);
        }

        public async Task<IActionResult> Index(PaymentDelayReasonSearchModel input)
        {
            var paymentDelayReasons = await _paymentDelayReasonAppService.Search(input);

            var model = new PaymentDelayReasonListViewModel(paymentDelayReasons);

            model.CanModify = await CanModify(PermissionNames.Pages_PaymentDelayReasons_Modify);

            return View(model);
        }
    }
}
