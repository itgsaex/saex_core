using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using Abp.UI;
using ItGroup.Saex.Models;
using System.Collections.Generic;
using System.Linq;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_AgentActivity_View)]
    public class AgentActivityController : SaexControllerBase
    {
        private readonly IAgentActivityAppService _agentActivityAppService;

        private readonly IAgentAppService _agentAppService;

        public AgentActivityController(
            IAgentActivityAppService agentActivityAppService,
            IAgentAppService agentAppService
        )
        {
            _agentActivityAppService = agentActivityAppService;
            _agentAppService = agentAppService;
        }

        public async Task<List<AgentActivityModel>> GetAgentActivity(Guid agentId, DateTime? date = null)
        {
            var agent = await _agentAppService.Get(agentId);
            if (agent == null)
                throw new UserFriendlyException("Agent not found for the provided Id");

            var agentActivity = await _agentActivityAppService.GetByAgentId((Guid)agentId, date);

            return agentActivity;
        }

        public async Task<IActionResult> Index()
        {
            var model = new AgentActivityIndexDto()
            {
                Agents = (await _agentAppService.GetAllAgentsForReport()).Select(a => new AgentListDto(a)).ToList()
            };

            return View(new AgentActivityViewModel(model));
        }
    }
}
