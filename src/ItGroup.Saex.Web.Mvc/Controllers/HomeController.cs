﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.Web.Models.Users;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : SaexControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
