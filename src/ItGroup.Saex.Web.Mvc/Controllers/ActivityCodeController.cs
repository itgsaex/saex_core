using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_ActivityCodes_View)]
    public class ActivityCodesController : SaexControllerBase
    {
        private readonly IActivityCodeAppService _activityCodeAppService;

        public ActivityCodesController(IActivityCodeAppService activityCodeAppService)
        {
            _activityCodeAppService = activityCodeAppService;
        }

        public async Task<ActionResult> EditActivityCodeModal(Guid id)
        {
            var ac = await _activityCodeAppService.Get(id);
            var model = new EditActivityCodeModalViewModel();

            model.Code = ac.Code;
            model.Description = ac.Description;
            model.Id = ac.Id;
            model.IsActive = ac.IsActive;
            model.IsGlobal = ac.IsGlobal;
            model.Questions = ac.Questions;
            model.Type = ac.Type;

            return View("_EditActivityCodeModal", model);
        }

        public async Task<IActionResult> Index(ActivityCodeSearchModel input)
        {
            var activityCodes = await _activityCodeAppService.Search(input);

            var model = new ActivityCodeListViewModel(activityCodes);

            model.CanModify = await CanModify(PermissionNames.Pages_ActivityCodes_Modify);

            return View(model);
        }
    }
}
