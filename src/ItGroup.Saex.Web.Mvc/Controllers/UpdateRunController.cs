﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_UpdateRun_View)]
    public class UpdateRunController : SaexControllerBase
    {
        private readonly IAuditLogAppService _auditLogAppService;

        public UpdateRunController(IAuditLogAppService auditLogAppService)
        {
            _auditLogAppService = auditLogAppService;
        }
        public async Task<IActionResult> Index()
        {
            var interfaceLogs = await _auditLogAppService.GetLastRunInterfaceLog();
            var errorLogs = await _auditLogAppService.GetErrorLog();
            var updateRunViewModel = new UpdateRunViewModel(interfaceLogs, errorLogs);

            return View(updateRunViewModel);
        }
    }
}
