using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Conditions_View)]
    public class ConditionsController : SaexControllerBase
    {
        private readonly IConditionAppService _conditionAppService;

        private readonly IProductAppService _productAppService;

        public ConditionsController(IConditionAppService conditionAppService, IProductAppService productAppService)
        {
            _conditionAppService = conditionAppService;
            _productAppService = productAppService;
        }

        public async Task<ActionResult> EditConditionModal(Guid id)
        {
            var res = await _conditionAppService.Get(id);
            var model = ObjectMapper.Map<EditConditionModalViewModel>(res);
            model.Products = await _productAppService.GetAllForList();

            return View("_EditConditionModal", model);
        }

        public async Task<IActionResult> Index(ConditionSearchModel input)
        {
            var conditions = await _conditionAppService.Search(input);
            var products = await _productAppService.GetAllForList();

            var model = new ConditionListViewModel(conditions);

            model.Products = products;

            model.CanModify = await CanModify(PermissionNames.Pages_Conditions_Modify);

            return View(model);
        }
    }
}
