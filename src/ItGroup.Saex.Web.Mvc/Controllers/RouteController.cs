using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Routes_View)]
    public class RoutesController : SaexControllerBase
    {
        private readonly PostalCodeManager _postalCodeManager;

        private readonly ProductManager _productManager;

        private readonly IRouteAppService _routeAppService;

        public RoutesController(IRouteAppService routeAppService, PostalCodeManager postalCodeManager, ProductManager productManager)
        {
            _routeAppService = routeAppService;
            _postalCodeManager = postalCodeManager;
            _productManager = productManager;
        }

        public async Task<ActionResult> EditRouteModal(Guid id)
        {
            var route = await _routeAppService.Get(id);

            var postalCodes = await _postalCodeManager.GetAll().Select(x => new PostalCodeListDto(x)).ToListAsync();

            var products = await _productManager.GetAll().Select(x => new ProductListDto(x)).ToListAsync();

            var output = new EditRouteModalViewModel(postalCodes, products, route);

            return View("_EditRouteModal", output);
        }

        public async Task<IActionResult> Index(RouteSearchModel input)
        {
            var routes = await _routeAppService.Search(input);

            var postalCodes = await _postalCodeManager.GetAll().Select(x => new PostalCodeListDto(x)).ToListAsync();

            var products = await _productManager.GetAll().Select(x => new ProductListDto(x)).ToListAsync();

            var model = new RouteListViewModel(routes, postalCodes, products);

            model.CanModify = await CanModify(PermissionNames.Pages_Routes_Modify);

            return View(model);
        }
    }
}
