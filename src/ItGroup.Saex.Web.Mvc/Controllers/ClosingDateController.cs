using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_ClosingDates_View)]
    public class ClosingDatesController : SaexControllerBase
    {
        private readonly IClosingDateAppService _closingDateAppService;

        public ClosingDatesController(IClosingDateAppService closingDateAppService)
        {
            _closingDateAppService = closingDateAppService;
        }

        public async Task<ActionResult> EditClosingDateModal(Guid id)
        {
            var output = await _closingDateAppService.Get(id);
            var model = ObjectMapper.Map<EditClosingDateModalViewModel>(output);

            return View("_EditClosingDateModal", model);
        }

        public async Task<IActionResult> Index(ClosingDateSearchModel input)
        {
            var closingDates = await _closingDateAppService.Search(input);

            var model = new ClosingDateListViewModel(closingDates);

            model.CanModify = await CanModify(PermissionNames.Pages_ClosingDates_Modify);

            return View(model);
        }
    }
}
