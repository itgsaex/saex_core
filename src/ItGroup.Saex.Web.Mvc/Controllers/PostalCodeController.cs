using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_PostalCodes_View)]
    public class PostalCodesController : SaexControllerBase
    {
        private readonly IPostalCodeAppService _postalCodeAppService;

        public PostalCodesController(IPostalCodeAppService postalCodeAppService)
        {
            _postalCodeAppService = postalCodeAppService;
        }

        public async Task<ActionResult> EditPostalCodeModal(Guid id)
        {
            var output = await _postalCodeAppService.Get(id);
            var model = ObjectMapper.Map<EditPostalCodeModalViewModel>(output);

            return View("_EditPostalCodeModal", model);
        }

        public async Task<IActionResult> Index(PostalCodeSearchModel input, int page = 1)
        {
            var postalCodes = await _postalCodeAppService.Search(input);

            var model = new PostalCodeListViewModel(postalCodes);

            model.CanModify = await CanModify(PermissionNames.Pages_PostalCodes_Modify);

            return View(model);
        }
    }
}
