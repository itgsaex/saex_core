﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Abp.Domain.Entities;
using static ItGroup.Saex.Enumerations.Enumerations;
using DevExpress.XtraRichEdit.Model;
using ItGroup.Saex.Users;
using ItGroup.Saex.Users.Dto;
using Abp.Runtime.Session;
using ItGroup.Saex.Models;
using Abp.Domain.Uow;

namespace ItGroup.Saex.Web.Controllers
{
    public class DailyReportController : SaexControllerBase
    {
        private readonly IDailyReportAppService _dailyReportAppService;
        private readonly IAssignInternalAgentsAppService _assignInternalAgentsAppService;
        private readonly IAbpSession _abpSession;

        public DailyReportController(IDailyReportAppService dailyReportAppService,
                                     IAbpSession abpSession,
                                     IAssignInternalAgentsAppService assignInternalAgentsAppService)
        {
            _dailyReportAppService = dailyReportAppService;
            _abpSession = abpSession;
            _assignInternalAgentsAppService = assignInternalAgentsAppService;
        }

        public async Task<IActionResult> Index()
        {
            var viewModel = new DailyReportViewModel();

            var userRecord = await _assignInternalAgentsAppService.GetUserRecord(_abpSession.UserId.GetValueOrDefault());

            if (userRecord.Agent != null && userRecord.Agent.Type == Enumerations.Enumerations.AgentType.Supervisor)
            {
                viewModel.ExternalAgents = _dailyReportAppService.GetExternalAgents(userRecord.Id);   
            }
            else
            {
                viewModel.ExternalAgents = _dailyReportAppService.GetExternalAgents(null);
            }

            return View("Index",viewModel);
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> Search(DailyReportViewModel model)
        {
            var parameterModel = new DailyReportParameterModel();
            var externalAgentUserRecord = await _assignInternalAgentsAppService.GetUserRecord(model.SelectedExternalAgent.GetValueOrDefault());
            parameterModel.ExternalAgent = externalAgentUserRecord.UserName;

            if (model.SelectedDateType == 1)
            {
                // Ayer
                parameterModel.FromDate = DateTime.Today.AddDays(-1);
                parameterModel.ToDate = DateTime.Today;
            } else if (model.SelectedDateType == 2)
            {
                // Esta semana hasta hoy
                parameterModel.FromDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);
                parameterModel.ToDate = DateTime.Today.AddDays(1);
            } else if (model.SelectedDateType == 3)
            {
                // Este mes hasta hoy
                parameterModel.FromDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                parameterModel.ToDate = DateTime.Today.AddDays(1);
            } else
            {
                // Seleccionar fecha
                parameterModel.FromDate = model.FromDate.GetValueOrDefault();
                parameterModel.ToDate = model.ToDate.GetValueOrDefault().AddDays(1);
            }
            
            var result = await _dailyReportAppService.DailyReport(parameterModel);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> SearchPlannedCasesWithoutVisit(DailyReportViewModel model)
        {
            var parameterModel = new DailyReportParameterModel();
            var externalAgentUserRecord = await _assignInternalAgentsAppService.GetUserRecord(model.SelectedExternalAgent.GetValueOrDefault());
            parameterModel.ExternalAgent = externalAgentUserRecord.UserName;

            if (model.SelectedDateType == 1)
            {
                // Ayer
                parameterModel.FromDate = DateTime.Today.AddDays(-1);
                parameterModel.ToDate = DateTime.Today;
            }
            else if (model.SelectedDateType == 2)
            {
                // Esta semana hasta hoy
                parameterModel.FromDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);
                parameterModel.ToDate = DateTime.Today.AddDays(1);
            }
            else if (model.SelectedDateType == 3)
            {
                // Este mes hasta hoy
                parameterModel.FromDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                parameterModel.ToDate = DateTime.Today.AddDays(1);
            }
            else
            {
                // Seleccionar fecha
                parameterModel.FromDate = model.FromDate.GetValueOrDefault();
                parameterModel.ToDate = model.ToDate.GetValueOrDefault().AddDays(1);
            }

            var result = await _dailyReportAppService.PlannedCaseWitoutVisits(parameterModel);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }
    }
}
