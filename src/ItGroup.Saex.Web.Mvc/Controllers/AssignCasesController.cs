using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Abp.Domain.Entities;
using static ItGroup.Saex.Enumerations.Enumerations;
using ItGroup.Saex.Models;
using Abp.Domain.Uow;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_AssignCases_View)]
    public class AssignCasesController : SaexControllerBase
    {
        private readonly AgentCaseManager _agentCaseManager;

        private readonly AgentManager _agentManager;

        private readonly AgentAppService _agentService;

        private readonly AssignCaseAppService _assignCaseService;

        private readonly CaseManager _caseManager;

        private readonly IDailyReportAppService _dailyReportAppService;

        protected readonly ReportManager _reportManager;

        public AssignCasesController(AgentAppService agentService,
                                     CaseManager caseManager,
                                     AssignCaseAppService assignCaseService,
                                     AgentCaseManager agentCaseManager,
                                     AgentManager agentManager,
                                     IDailyReportAppService dailyReportAppService,
                                     ReportManager reportManager
        )
        {
            _agentService = agentService;
            _caseManager = caseManager;
            _assignCaseService = assignCaseService;
            _agentCaseManager = agentCaseManager;
            _agentManager = agentManager;
            _dailyReportAppService = dailyReportAppService;
            _reportManager = reportManager;
        }

        public async Task<ActionResult> AssignCaseModal(Guid fromAgentId, List<Guid> selectedCases, Guid? toAgentId = null)
        {
            var files = Request.Form;

            var model = new AssignCaseModalViewModel();

            model.FromAgent = await _agentManager.GetAllQuery()
                                           .Where(x => x.Id == fromAgentId)
                                           .Select(y => new AgentDto(y))
                                           .SingleOrDefaultAsync();

            model.TransferedCases = new List<CaseListDto>();

            foreach (var id in selectedCases)
            {
                var agentCase = await _caseManager.GetAsync(id);

                if (agentCase == null)
                    throw new EntityNotFoundException(L("RecordNotFoundError"));

                model.TransferedCases.Add(new CaseListDto(agentCase));
            }

            model.Agents = (await _agentService.GetAllAgentsForReport()).Select(a => new AgentListDto(a)).ToList();

            if (toAgentId != null)
            {
                model.ToAgent = await _agentManager.GetAllQuery()
                                           .Where(x => x.Id == toAgentId)
                                           .Select(y => new AgentDto(y))
                                           .SingleOrDefaultAsync();

                model.ToAgentCases = await _caseManager.GetAllQuery().Where(x => x.Agents.Where(y => y.AgentId == toAgentId).Any()).Select(x => new CaseListDto(x)).ToListAsync();
            }

            return View("_AssignCaseModal", model);
        }

        [HttpPost]
        public async Task<ActionResult> EditAssignCaseModal(Guid? agentId = null)
        {
            var output = new EditAssignCaseModalViewModel();

            //output.Agent = await _agentManager.GetAllQuery()
            //                                          .Where(x => x.Id == (Guid)agentId)
            //                                          .Select(y => new AgentDto(y))
            //                                          .ToListAsync();
            //output.AvailableCases = await _agentCaseManager.GetAllQuery()
            //                                          .Where(x => x.AgentId == (Guid)agentId)
            //                                          .Select(y => new CaseListDto(y.Case))
            //                                          .ToListAsync();

            return View("_EditAssignCaseModal", output);
        }

        public async Task<IActionResult> Index(Guid? fromAgent = null)
        {
            var model = new AssignCaseListViewModel();

            if (fromAgent != null)
            {
                model.FromAgent = await _agentManager.GetAllQuery()
                                           .Where(x => x.Id == (Guid)fromAgent)
                                           .Select(y => new AgentDto(y))
                                           .SingleOrDefaultAsync();

                model.FromAgentCases = await _caseManager.GetAllQuery()
                    .Where(x => x.Agents.Where(y => y.AgentId == fromAgent && y.IsActive).Any())
                    .Select(x => new CaseListDto(x)).ToListAsync();
            }

            model.Agents = await _agentManager.GetAllQuery()
                                .Select(y => new AgentListDto(y))
                                .ToListAsync();

            model.Agents = (await _agentService.GetAllAgentsForReport()).Select(a => new AgentListDto(a)).ToList();

            return View(model);
        }

        //[HttpPost]
        //public async Task<bool> Save(Guid fromAgentId, Guid toAgentId, List<Guid> selectedCases, TransferType transferType)
        //{
        //    if (selectedCases == null || !selectedCases.Any())
        //        throw new EntityNotFoundException(L("NoSelectedCasesError"));

        //    var model = new TransferAgentModel();
        //    model.FromAgentId = fromAgentId;
        //    model.ToAgentId = toAgentId;
        //    model.SelectedCases = selectedCases;
        //    model.TransferType = transferType;
        //    await _assignCaseService.SaveTransfer(model);

        //    return true;
        //}

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> SelectedToAgentTransferCases(AssignCaseListViewModel model)
        {
            var toAgent = (await _agentService.GetAllAgentsForReport()).Where(a => a.Id == model.ToAgentId).Select(a => new AgentListDto(a)).FirstOrDefault();
            var result = await _dailyReportAppService.GetCopyOrTransferCases(toAgent.UserId);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> SearchFromCases(AssignCaseListViewModel model)
        {
            var filter = new CaseReportSearchModel();
            filter.AgentId = model.FromAgentId;
            var result = (await _reportManager.GetCasesReport(filter)).Items.Select(x => new CaseListDto(x)).ToList();

            //var result = await _caseManager.GetAllQuery().Where(x => x.Agents.Where(y => y.AgentId == model.FromAgentId && y.IsActive).Any()).Select(x => new CaseListDto(x)).ToListAsync();
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        [HttpPost]
        public async Task<bool> Save(Guid SelectedFromAgent, Guid SelectedToAgent, List<string> SelectedCases, string SelectedAsignationType)
        {
            var fromAgent = (await _agentService.GetAllAgentsForReport()).Where(a => a.Id == SelectedFromAgent).Select(a => new AgentListDto(a)).FirstOrDefault();
            var toAgent = (await _agentService.GetAllAgentsForReport()).Where(a => a.Id == SelectedToAgent).Select(a => new AgentListDto(a)).FirstOrDefault();
            var model = new TansferModel();
            model.SelectedInternalAgent = fromAgent.UserId;
            model.SelectedInternalToAgent = toAgent.UserId;
            model.AsignationType = SelectedAsignationType;
            model.SelectedCases = SelectedCases;
            await _dailyReportAppService.SaveTransfer(model);

            return true;
        }

        [HttpPost]
        public async Task<bool> Delete(Guid SelectedToAgent, List<string> SelectedCases)
        {
            var toAgent = (await _agentService.GetAllAgentsForReport()).Where(a => a.Id == SelectedToAgent).Select(a => new AgentListDto(a)).FirstOrDefault();
            var model = new TansferModel();
            model.SelectedInternalToAgent = toAgent.UserId;
            model.SelectedCases = SelectedCases;
            await _dailyReportAppService.DeleteTransfer(model);

            return true;
        }
    }
}
