using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_AdjusterStates_View)]
    public class AdjusterStatesController : SaexControllerBase
    {
        private readonly IAdjusterStateAppService _adjusterStateAppService;

        private readonly IMacroStateAppService _macroStateAppService;

        public AdjusterStatesController
        (
            IAdjusterStateAppService adjusterStateAppService,
            IMacroStateAppService macroStateAppService
        )
        {
            _adjusterStateAppService = adjusterStateAppService;
            _macroStateAppService = macroStateAppService;
        }

        public async Task<ActionResult> EditAdjusterStateModal(Guid id)
        {
            var res = await _adjusterStateAppService.Get(id);
            var model = new EditAdjusterStateModalViewModel(res);
            model.MacroStates = await _macroStateAppService.GetAllForList();

            return View("_EditAdjusterStateModal", model);
        }

        public async Task<IActionResult> Index(AdjusterStateSearchModel input)
        {
            var adjusterStates = await _adjusterStateAppService.Search(input);

            var model = new AdjusterStateListViewModel(adjusterStates);

            model.CanModify = await CanModify(PermissionNames.Pages_AdjusterStates_Modify);

            model.MacroStates = await _macroStateAppService.GetAllForList();

            return View(model);
        }
    }
}
