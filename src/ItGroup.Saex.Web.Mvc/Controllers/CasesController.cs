using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using Microsoft.AspNetCore.Http;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Cases_View)]
    public class CasesController : SaexControllerBase
    {
        private readonly ICaseAppService _caseAppService;

        private readonly IProductAppService _productAppService;

        public CasesController(
            ICaseAppService caseAppService,
            IProductAppService productAppService
        )
        {
            _caseAppService = caseAppService;
            _productAppService = productAppService;
        }

        public async Task<ActionResult> EditCaseModal(Guid id)
        {
            var res = await _caseAppService.Get(id);
            var model = new EditCaseModalViewModel(res);

            return View("_EditCaseModal", model);
        }

        public async Task<IActionResult> Index(CaseSearchModel input)
        {
            var cases = await _caseAppService.Search(input);

            var model = new CaseListViewModel(cases);

            model.CanModify = await CanModify(PermissionNames.Pages_Cases_Modify);

            return View(model);
        }
    }
}
