using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using Microsoft.AspNetCore.Http;
using System.IO;
using CsvHelper;
using System.Globalization;
using System.Collections.Generic;
using CsvHelper.Configuration;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Users_View)]
    public class BaselineCasesImportController : SaexControllerBase
    {
        private readonly ICaseAppService _caseAppService;

        private readonly IImportAppService _importAppService;

        public BaselineCasesImportController(
            IImportAppService importAppService,
            ICaseAppService caseAppService
        )
        {
            _importAppService = importAppService;
            _caseAppService = caseAppService;
        }

        [HttpPost]
        [Abp.Domain.Uow.UnitOfWork]
        public virtual async Task<JsonResult> Import(CaseImportRequest caseTestRequest)
        {
            try
            {

                var model = new CaseImportDto(caseTestRequest.Cases, ImportType.Manual);

                var res = await _importAppService.Cases(model);

                return Json(new Abp.Web.Models.AjaxResponse {  });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        private Boolean CsvParsingExceptionOccurred(CsvHelperException csvHelperException)
        {
            return false;
        }

        public async Task<IActionResult> Index(CaseSearchModel input)
        {
            input.ImportType = Enumerations.Enumerations.ImportType.Manual;

            var cases = _importAppService.CaseBaselines();

            var model = new CaseBaselinesViewModel(cases.Result);

            return View(model);
        }
    }

    public class CaseImportRequest
    {
        public List<CaseImport> Cases { get; set; }
    }
}
