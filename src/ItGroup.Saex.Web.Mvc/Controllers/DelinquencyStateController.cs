using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using Microsoft.AspNetCore.Http;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_DelinquencyStates_View)]
    public class DelinquencyStatesController : SaexControllerBase
    {
        private readonly IDelinquencyStateAppService _delinquencyStateAppService;

        public DelinquencyStatesController(IDelinquencyStateAppService delinquencyStateAppService)
        {
            _delinquencyStateAppService = delinquencyStateAppService;
        }

        public async Task<ActionResult> EditDelinquencyStateModal(Guid id)
        {
            var output = await _delinquencyStateAppService.Get(id);
            var model = ObjectMapper.Map<EditDelinquencyStateModalViewModel>(output);

            return View("_EditDelinquencyStateModal", model);
        }

        public async Task<IActionResult> Index(DelinquencyStateSearchModel input)
        {
            var results = await _delinquencyStateAppService.Search(input);

            var model = new DelinquencyStateListViewModel(results);

            model.CanModify = await CanModify(PermissionNames.Pages_DelinquencyStates_Modify);

            return View(model);
        }
    }
}
