using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_ProductRules_View)]
    public class ProductRulesController : SaexControllerBase
    {
        private readonly IProductAppService _productAppService;

        private readonly IProductRuleAppService _productRuleAppService;

        public ProductRulesController(IProductRuleAppService productRuleAppService, IProductAppService productAppService)
        {
            _productRuleAppService = productRuleAppService;
            _productAppService = productAppService;
        }

        public async Task<ActionResult> EditProductRuleModal(Guid id)
        {
            var res = await _productRuleAppService.Get(id);
            var model = ObjectMapper.Map<EditProductRuleModalViewModel>(res);
            model.Products = await _productAppService.GetAllForList();

            return View("_EditProductRuleModal", model);
        }

        public async Task<IActionResult> Index(ProductRuleSearchModel input)
        {
            var productRules = await _productRuleAppService.Search(input);
            var products = await _productAppService.GetAllForList();

            var model = new ProductRuleListViewModel(productRules);

            model.Products = products;

            model.CanModify = await CanModify(PermissionNames.Pages_ProductRules_Modify);

            return View(model);
        }
    }
}
