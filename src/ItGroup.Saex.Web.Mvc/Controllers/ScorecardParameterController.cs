using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_ScorecardParameters_View)]
    public class ScorecardParametersController : SaexControllerBase
    {
        private readonly IScorecardParameterAppService _scorecardParameterAppService;

        public ScorecardParametersController(IScorecardParameterAppService scorecardParameterAppService)
        {
            _scorecardParameterAppService = scorecardParameterAppService;
        }

        public async Task<ActionResult> EditScorecardParameterModal(Guid id)
        {
            var output = await _scorecardParameterAppService.Get(id);
            var model = ObjectMapper.Map<EditScorecardParameterModalViewModel>(output);

            return View("_EditScorecardParameterModal", model);
        }

        public async Task<IActionResult> Index(ScorecardParameterSearchModel input)
        {
            var results = await _scorecardParameterAppService.Search(input);

            var model = new ScorecardParameterListViewModel(results);

            model.CanModify = await CanModify(PermissionNames.Pages_ScorecardParameters_Modify);

            return View(model);
        }
    }
}
