using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Products_View)]
    public class ProductsController : SaexControllerBase
    {
        private readonly IProductAppService _productAppService;

        public ProductsController(IProductAppService productAppService)
        {
            _productAppService = productAppService;
        }

        public async Task<ActionResult> EditProductModal(Guid id)
        {
            var output = await _productAppService.Get(id);
            var model = ObjectMapper.Map<EditProductModalViewModel>(output);

            return View("_EditProductModal", model);
        }

        public async Task<IActionResult> Index(ProductSearchModel input)
        {
            var products = await _productAppService.Search(input);

            var model = new ProductListViewModel(products);

            model.CanModify = await CanModify(PermissionNames.Pages_Products_Modify);

            return View(model);
        }
    }
}
