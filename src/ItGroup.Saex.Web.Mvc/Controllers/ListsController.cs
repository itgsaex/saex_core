﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ItGroup.Saex.Controllers;
using Microsoft.AspNetCore.Mvc;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Web.ViewModels;
using ItGroup.Saex.Web.Startup;
using System.Text.RegularExpressions;
using Abp.Domain.Uow;
using ItGroup.Saex.DomainEntities.ProductSystem;
using ItGroup.Saex.Authorization;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.Mvc.Controllers
{
    public class ListsController : SaexControllerBase
    {
        private readonly IListAppService _listAppService;
        private readonly ICaseRecentActivityRepository _caseFieldsAppService;

        private readonly IEstadoRiesgoService _estadoRiesgoService;
        private readonly IProductSystemService _productSystemService;



        public ListsController (IListAppService listAppService,
                                ICaseRecentActivityRepository caseFieldsAppService,
                                IProductSystemService productSystemService,
                                IEstadoRiesgoService estadoRiesgoService)
        {
            _listAppService = listAppService;
            _estadoRiesgoService = estadoRiesgoService;
            _productSystemService = productSystemService;
            _caseFieldsAppService = caseFieldsAppService;
        }

        public IActionResult Index()
        {

            return View();
        }

        public async Task<IActionResult> Lists(ItGroup.Saex.Enumerations.Enumerations.ListType listType)
        {
            var searchModel = new ListSearchModel();
            searchModel.ListType = listType;
            var list = await _listAppService.Search(searchModel);
            var model = new ListsViewModel(list);
            model.ListType = listType;
            switch (listType)
            {
                case Enumerations.Enumerations.ListType.Master:
                    model.Title = L("MasterLists");
                    model.PageName = PageNames.MasterLists;
                    model.CanModify = await CanModify(PermissionNames.Pages_MasterLists_Modify);
                    break;
                case Enumerations.Enumerations.ListType.Child:
                    model.Title = L("ChildLists");
                    model.PageName = PageNames.ChildLists;
                    model.CanModify = await CanModify(PermissionNames.Pages_ChildLists_Modify);
                    break;
                case Enumerations.Enumerations.ListType.Risk:
                    model.Title = L("RiskLists");
                    model.PageName = PageNames.RiskLists;
                    model.CanModify = await CanModify(PermissionNames.Pages_RiskLists_Modify);
                    break;
                default:
                    model.Title = "";
                    model.PageName = "";
                    break;
            }
            return View(model);
        }
      
        public async Task<IActionResult> EditList(string id, Enumerations.Enumerations.ListType? listType)
        {
            ListDto list;
            if (id != null)
                list = await _listAppService.Get(Guid.Parse(id));
            else
                list = new ListDto { Type = listType.Value };
            var validField = await _caseFieldsAppService.ReadCaseFields(null);

            var products = _productSystemService.GetAllProductSystems();
            var estados = _estadoRiesgoService.GetAllEstadoRiesgos().OrderBy(e => e.EstadoRiesgoID).ToList();

            if (list.Type == Enumerations.Enumerations.ListType.Master)
                validField = validField.Where(e => e.MasterList).ToList();

            if (list.Type == Enumerations.Enumerations.ListType.Risk)
            {
                validField = validField.Where(e => e.RiskList).ToList();
                if (!string.IsNullOrEmpty(list.Query))
                {
                    var query = list.Query;
                    foreach (var item in Regex.Matches(list.Query, @"\w*\.\w*"))
                    {
                        var str = item.ToString().Split(".");
                        if (str[0].ToLower() == "caso" || str[0].ToLower() == "cases")
                        {
                            var i = validField.FirstOrDefault(e => e.TargetColumn == str[1]);
                            query = query.Replace(item.ToString(), i.ColumnName);
                        }
                        else
                        {
                            query = query.Replace(item.ToString(), str[1]);
                        }
                    }
                    list.Query = query;
                }
            }

            if (list.Type == Enumerations.Enumerations.ListType.Child)
            {
                validField = validField.Where(e => e.ChildList).ToList();
                if(!string.IsNullOrEmpty(list.Query))
                {
                    var query = list.Query;
                    foreach (var item in Regex.Matches(list.Query, @"\w*\.\w*"))
                    {
                        var str = item.ToString().Split(".");
                        if (str[0].ToLower() == "caso" || str[0].ToLower() == "cases")
                        {
                            var i = validField.FirstOrDefault(e => e.TargetColumn == str[1]);
                            query = query.Replace(item.ToString(), i.ColumnName);
                        }
                        else
                        {
                            query = query.Replace(item.ToString(), str[1]);
                        }
                    }
                    list.Query = query;
                }
            }
           
            var model = new EditListViewModel(list, validField, products, estados);
            model.ListType = listType.GetValueOrDefault();
            return View("EditList", model);
        }

        [HttpPost]
        [UnitOfWork]        
        public virtual async Task<JsonResult> TestQuery(ListDto list)
        {
            var result = await _caseFieldsAppService.TestQuery(list.Type, list.Type == ListType.Master ? list.ProductSystemTable : "impcasos", list.Query, "Caso.NumeroCuenta,Caso.ProductNumber,Caso.ZipCode,Caso.Nombre", list.FieldsToPivot, null);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        public virtual async Task<JsonResult> SaveList(ListDto list)
        {
            try
            {
                switch (list.Type)
                {
                    case ListType.Unknown:
                        break;
                    case ListType.Master:
                        if (!await CanModify(PermissionNames.Pages_MasterLists_Modify))
                            throw new Exception("User no has access");
                        break;
                    case ListType.Child:
                        if (!await CanModify(PermissionNames.Pages_ChildLists_Modify))
                            throw new Exception("User no has access");
                        break;
                    case ListType.Risk:
                        if (!await CanModify(PermissionNames.Pages_RiskLists_Modify))
                            throw new Exception("User no has access");
                        break;
                }

                if (list.Id == Guid.Empty)
                {
                    var item = new ListCreateDto
                    {

                        IsActive = list.IsActive,
                        Name = list.Name,
                        Query = list.Query,
                        FieldsToPivot = list.FieldsToPivot,
                        Type = list.Type,
                        ReferenceId = list.ReferenceId,
                        EstadoRiesgoId = list.EstadoRiesgoId,
                        ProductSystemTable = list.ProductSystemTable
                    };
                    await _listAppService.Create(item);
                    return Json(new Abp.Web.Models.AjaxResponse { Result = item, TargetUrl = Url.Action("Lists", new { listType = item.Type }) });
                }
                else
                {
                    var item = new ListUpdateDto
                    {

                        IsActive = list.IsActive,
                        Name = list.Name,
                        Query = list.Query,
                        FieldsToPivot = list.FieldsToPivot,
                        Type = list.Type,
                        ReferenceId = list.ReferenceId,
                        EstadoRiesgoId = list.EstadoRiesgoId,
                        ProductSystemTable = list.ProductSystemTable,
                        Id = list.Id
                    };
                    var result = await _listAppService.Update(item);
                    return Json(new Abp.Web.Models.AjaxResponse { Result = result, TargetUrl = Url.Action("Lists", new { listType = item.Type }) });
                }
            }
            catch (Exception)
            {
                throw;
            }
            

            
        }
    }
}