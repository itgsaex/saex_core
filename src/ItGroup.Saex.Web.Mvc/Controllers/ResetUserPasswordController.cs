using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using ItGroup.Saex.Users;
using ItGroup.Saex.Web.Models.Users;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_ResetUserPassword_View)]
    public class ResetUserPasswordController : SaexControllerBase
    {
        private readonly IUserAppService _userAppService;

        public ResetUserPasswordController
        (
            IUserAppService userAppService
        )
        {
            _userAppService = userAppService;
        }

        public async Task<IActionResult> Index(long userId)
        {
            var model = new ResetUserPasswordViewModel { UserId = userId };
            return View("ResetUserPassword", model);
        }
    }
}
