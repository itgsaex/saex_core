﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Abp.Domain.Entities;
using static ItGroup.Saex.Enumerations.Enumerations;
using DevExpress.XtraRichEdit.Model;
using ItGroup.Saex.Users;
using ItGroup.Saex.Users.Dto;
using Abp.Runtime.Session;
using ItGroup.Saex.Models;
using Abp.Domain.Uow;

namespace ItGroup.Saex.Web.Controllers
{
    public class InternalAgentDashboardController : SaexControllerBase
    {
        private readonly IInternalAgentDashboardService _internalAgentDashboardAppService;
        private readonly IAssignInternalAgentsAppService _assignInternalAgentsAppService;
        private readonly ISystemPolicyAppService _systemPolicyAppService;
        private readonly IAbpSession _abpSession;

        public InternalAgentDashboardController(IInternalAgentDashboardService internalAgentDashboardAppService,
                                                IAssignInternalAgentsAppService assignInternalAgentsAppService,
                                                ISystemPolicyAppService systemPolicyAppService,
                                                IAbpSession abpSession)
        {
            _internalAgentDashboardAppService = internalAgentDashboardAppService;
            _assignInternalAgentsAppService = assignInternalAgentsAppService;
            _systemPolicyAppService = systemPolicyAppService;
            _abpSession = abpSession;
        }

        public async Task<IActionResult> Index()
        {
            var model = new InternalAgentDashboardViewModel();

            var userRecord = await _assignInternalAgentsAppService.GetUserRecord(_abpSession.UserId.GetValueOrDefault());

            if (userRecord.Agent != null && userRecord.Agent.Type == Enumerations.Enumerations.AgentType.Internal)
            {
                model.ExternalAgents = _internalAgentDashboardAppService.GetExternalAgents(userRecord.Id).OrderBy(a => a.User.FullName).ToList();
                model.Regions = _internalAgentDashboardAppService.GetRegions(userRecord.Id).OrderBy(r => r.Name).ToList();
                model.Routes = _internalAgentDashboardAppService.GetRoutes(userRecord.Id).OrderBy(r => r.Name).ToList();
                model.PostalCodes = _internalAgentDashboardAppService.GetPostalCodes(userRecord.Id).OrderBy(p => p.Code).ToList();
                model.InternalAgentId = userRecord.Id;
            } 
            else
            {
                model.ExternalAgents = _internalAgentDashboardAppService.GetExternalAgents(null).OrderBy(a => a.User.FullName).ToList();
                model.Regions = _internalAgentDashboardAppService.GetRegions(null).OrderBy(r => r.Name).ToList();
                model.Routes = _internalAgentDashboardAppService.GetRoutes(null).OrderBy(r => r.Name).ToList();
                model.PostalCodes = _internalAgentDashboardAppService.GetPostalCodes(null).OrderBy(p => p.Code).ToList();
                model.InternalAgentId = null;
            }

            model.ActivityCodes = _internalAgentDashboardAppService.GetActivityCodes().OrderBy(a => a.Description).ToList();

            var paramDelinquecyStages = (await _systemPolicyAppService.GetAllSystemPolicies()).FirstOrDefault(x => x.Code == "DELINQUENCY_STAGES");
            var delinquencyStates = new List<string>();
            if (paramDelinquecyStages != null)
            {
                delinquencyStates.AddRange(paramDelinquecyStages.Value.Split("|"));
            }

            model.DelinquencyStages = delinquencyStates;
            model.Alerts = _internalAgentDashboardAppService.GetAlerts().OrderBy(a => a.Description).ToList();
            model.Status = _internalAgentDashboardAppService.GetStatus();
            model.Estados = _internalAgentDashboardAppService.GetEstados();
            model.InternalAgentCases = new List<Saex.Models.InternalAgentDashboardModel>();

            return View("Index",model);
        }

        //public async Task<IActionResult> SearchPost(InternalAgentDashboardViewModel model)
        //{
        //    var userRecord = await _assignInternalAgentsAppService.GetUserRecord(_abpSession.UserId.GetValueOrDefault());
        //    var parameterModel = new ReadInternalAgentCasesParametersModel();

        //    if (userRecord.Agent != null && userRecord.Agent.Type == Enumerations.Enumerations.AgentType.Internal)
        //    {
        //        model.ExternalAgents = _internalAgentDashboardAppService.GetExternalAgents(userRecord.Id);
        //        model.Regions = _internalAgentDashboardAppService.GetRegions(null);
        //        model.Routes = _internalAgentDashboardAppService.GetRoutes(null);
        //        model.PostalCodes = _internalAgentDashboardAppService.GetPostalCodes(null);
        //        model.InternalAgentId = userRecord.Id;

        //        parameterModel.Supervisor = null;
        //        parameterModel.InternalAgent = userRecord.Id;
        //        parameterModel.InternalAgentUsername = userRecord.UserName;
        //    }
        //    else
        //    {
        //        model.ExternalAgents = _internalAgentDashboardAppService.GetExternalAgents(null);
        //        model.Regions = _internalAgentDashboardAppService.GetRegions(null);
        //        model.Routes = _internalAgentDashboardAppService.GetRoutes(null);
        //        model.PostalCodes = _internalAgentDashboardAppService.GetPostalCodes(null);
        //        model.InternalAgentId = null;

        //        parameterModel.Supervisor = null;
        //        parameterModel.InternalAgent = null;
        //        parameterModel.InternalAgentUsername = null;
        //    }

        //    model.ActivityCodes = _internalAgentDashboardAppService.GetActivityCodes();
        //    model.DelinquencyStages = _internalAgentDashboardAppService.GetDelinquencyStages();
        //    model.Alerts = _internalAgentDashboardAppService.GetAlerts();
        //    model.Status = _internalAgentDashboardAppService.GetStatus();
        //    model.Estados = _internalAgentDashboardAppService.GetEstados();

        //    if (model.SelectedExternalAgent.HasValue)
        //    {
        //        var externalAgenteUserRecord = model.ExternalAgents.Where(a => a.Id == model.SelectedExternalAgent.GetValueOrDefault()).FirstOrDefault();
        //        parameterModel.ExternalAgent = externalAgenteUserRecord.User.Id;
        //        parameterModel.ExternalAgentUsername = externalAgenteUserRecord.User.UserName;
        //    } else
        //    {
        //        parameterModel.ExternalAgent = null;
        //        parameterModel.ExternalAgentUsername = null;
        //    }

        //    if (model.SelectedRegion.HasValue)
        //    {
        //        parameterModel.RegionId = model.SelectedRegion.GetValueOrDefault();
        //    } else
        //    {
        //        parameterModel.RegionId = null;
        //    }

        //    if (model.SelectedRoute.HasValue)
        //    {
        //        parameterModel.RutaId = model.SelectedRoute.GetValueOrDefault();
        //    } else
        //    {
        //        parameterModel.RutaId = null;
        //    }

        //    if (!string.IsNullOrWhiteSpace(model.SelectedPostalCode))
        //    {
        //        parameterModel.CodigoPostal = model.SelectedPostalCode;
        //    } else
        //    {
        //        parameterModel.CodigoPostal = null;
        //    }

        //    if (!string.IsNullOrWhiteSpace(model.SelectedActivityCode))
        //    {
        //        parameterModel.ActivityCode = model.SelectedActivityCode;
        //    } else
        //    {
        //        parameterModel.ActivityCode = null;
        //    }

        //    if (!string.IsNullOrWhiteSpace(model.SelectedDelinquencyStage))
        //    {
        //        parameterModel.EtapaDeDelincuencia = model.SelectedDelinquencyStage;
        //    }
        //    else
        //    {
        //        parameterModel.EtapaDeDelincuencia = null;
        //    }

        //    if (model.SelectedAlert.HasValue)
        //    {
        //        parameterModel.AlertId = model.SelectedAlert.GetValueOrDefault();
        //    }
        //    else
        //    {
        //        parameterModel.AlertId = null;
        //    }

        //    if (!string.IsNullOrWhiteSpace(model.SelectedStatus))
        //    {
        //        parameterModel.Status = model.SelectedStatus;
        //    }
        //    else
        //    {
        //        parameterModel.Status = null;
        //    }

        //    if (!string.IsNullOrWhiteSpace(model.SelectedEstado))
        //    {
        //        parameterModel.Estado = model.SelectedEstado;
        //    }
        //    else
        //    {
        //        parameterModel.Estado = null;
        //    }

        //    if (!string.IsNullOrWhiteSpace(model.AccountNumber))
        //    {
        //        parameterModel.AccountNumber = model.AccountNumber;
        //    }
        //    else
        //    {
        //        parameterModel.AccountNumber = null;
        //    }

        //    if (!string.IsNullOrWhiteSpace(model.Name))
        //    {
        //        parameterModel.Name = model.Name;
        //    }
        //    else
        //    {
        //        parameterModel.Name = null;
        //    }

        //    model.InternalAgentCases = await _internalAgentDashboardAppService.ReadInternalAgentCases(parameterModel);

        //    return View("Index", model);
        //}

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> Search(InternalAgentDashboardViewModel model)
        {
            var userRecord = await _assignInternalAgentsAppService.GetUserRecord(_abpSession.UserId.GetValueOrDefault());
            var parameterModel = new ReadInternalAgentCasesParametersModel();

            if (userRecord.Agent != null && userRecord.Agent.Type == Enumerations.Enumerations.AgentType.Internal)
            {
                parameterModel.Supervisor = null;
                parameterModel.InternalAgent = userRecord.Id;
                parameterModel.InternalAgentUsername = userRecord.UserName;
            }
            else
            {
                parameterModel.Supervisor = null;
                parameterModel.InternalAgent = null;
                parameterModel.InternalAgentUsername = null;
            }

            if (model.SelectedExternalAgent.HasValue)
            {
                var externalAgenteUserRecord = await _assignInternalAgentsAppService.GetUserRecord(model.SelectedExternalAgent.GetValueOrDefault());
                parameterModel.ExternalAgent = externalAgenteUserRecord.Id;
                parameterModel.ExternalAgentUsername = externalAgenteUserRecord.UserName;
            }
            else
            {
                parameterModel.ExternalAgent = null;
                parameterModel.ExternalAgentUsername = null;
            }

            if (model.SelectedRegion.HasValue)
            {
                parameterModel.RegionId = model.SelectedRegion.GetValueOrDefault();
            }
            else
            {
                parameterModel.RegionId = null;
            }

            if (model.SelectedRoute.HasValue)
            {
                parameterModel.RutaId = model.SelectedRoute.GetValueOrDefault();
            }
            else
            {
                parameterModel.RutaId = null;
            }

            if (!string.IsNullOrWhiteSpace(model.SelectedPostalCode))
            {
                parameterModel.CodigoPostal = model.SelectedPostalCode;
            }
            else
            {
                parameterModel.CodigoPostal = null;
            }

            if (!string.IsNullOrWhiteSpace(model.SelectedActivityCode))
            {
                parameterModel.ActivityCode = model.SelectedActivityCode;
            }
            else
            {
                parameterModel.ActivityCode = null;
            }

            if (!string.IsNullOrWhiteSpace(model.SelectedDelinquencyStage))
            {
                parameterModel.EtapaDeDelincuencia = model.SelectedDelinquencyStage;
            }
            else
            {
                parameterModel.EtapaDeDelincuencia = null;
            }

            if (model.SelectedAlert.HasValue)
            {
                parameterModel.AlertId = model.SelectedAlert.GetValueOrDefault();
            }
            else
            {
                parameterModel.AlertId = null;
            }

            if (!string.IsNullOrWhiteSpace(model.SelectedStatus))
            {
                parameterModel.Status = model.SelectedStatus;
            }
            else
            {
                parameterModel.Status = null;
            }

            if (!string.IsNullOrWhiteSpace(model.SelectedEstado))
            {
                parameterModel.Estado = model.SelectedEstado;
            }
            else
            {
                parameterModel.Estado = null;
            }

            if (!string.IsNullOrWhiteSpace(model.AccountNumber))
            {
                parameterModel.AccountNumber = model.AccountNumber;
            }
            else
            {
                parameterModel.AccountNumber = null;
            }

            if (!string.IsNullOrWhiteSpace(model.Name))
            {
                parameterModel.Name = model.Name;
            }
            else
            {
                parameterModel.Name = null;
            }

            var result = await _internalAgentDashboardAppService.ReadInternalAgentCases(parameterModel);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }
    }
}