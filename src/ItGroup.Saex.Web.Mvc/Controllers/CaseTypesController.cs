using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_CaseTypes_View)]
    public class CaseTypesController : SaexControllerBase
    {
        private readonly ICaseTypeAppService _caseTypeAppService;

        private readonly IProductAppService _productAppService;

        public CaseTypesController(
            ICaseTypeAppService caseTypeAppService,
            IProductAppService productAppService
        )
        {
            _caseTypeAppService = caseTypeAppService;
            _productAppService = productAppService;
        }

        public async Task<ActionResult> EditCaseTypeModal(Guid id)
        {
            var output = await _caseTypeAppService.Get(id);
            var model = ObjectMapper.Map<EditCaseTypeModalViewModel>(output);
            model.Products = await _productAppService.GetAllForList();

            return View("_EditCaseTypeModal", model);
        }

        public async Task<IActionResult> Index(CaseTypeSearchModel input)
        {
            var caseTypes = await _caseTypeAppService.Search(input);

            var products = await _productAppService.GetAllForList();

            var model = new CaseTypeListViewModel(caseTypes, products);

            model.CanModify = await CanModify(PermissionNames.Pages_CaseTypes_Modify);

            return View(model);
        }
    }
}
