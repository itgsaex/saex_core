﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.Users;
using ItGroup.Saex.Web.Models.Users;
using ItGroup.Saex.Users.Dto;
using ItGroup.Saex.DomainEntities;
using static ItGroup.Saex.Enumerations.Enumerations;
using DevExpress.DataProcessing;
using Abp.Authorization;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Users_View)]
    public class UsersController : SaexControllerBase
    {
        private readonly IAuditLogAppService _auditLogAppService;

        private readonly IUserAppService _userAppService;

        private readonly IPermissionManager _permissionManager;

        public UsersController(IUserAppService userAppService, IAuditLogAppService auditLogAppService, IPermissionManager permissionManager)
        {
            _userAppService = userAppService;
            _auditLogAppService = auditLogAppService;
            _permissionManager = permissionManager;
        }

        public async Task<ActionResult> EditUserModal(long userId)
        {
            var user = await _userAppService.Get(new EntityDto<long>(userId));
            var roles = (await _userAppService.GetRoles()).Items;

            var auditModel = new AuditLogSearchModel()
            {
                UserId = userId
            };

            var auditLogs = await _auditLogAppService.Search(auditModel);

            var userName = "";
            if (user.LastModifierUserId != null)
            {
                var lastModifierUser = await _userAppService.Get(new EntityDto<long>(long.Parse(user.LastModifierUserId.ToString())));
                userName = lastModifierUser?.UserName;
            }
            user.LastModifierUserName = userName;

            var model = new EditUserModalViewModel
            {
                User = user,
                Roles = roles,
                AgentType = user.AgentType,
                AuditLogs = auditLogs
            };
            model.CanResetPassword = await CanModify(PermissionNames.Pages_ResetUserPassword_View);
            return View("_EditUserModal", model);
        }

        public async Task<ActionResult> Index(UserStatus status)
        {
            var users = (await _userAppService.Search(new PagedUserResultRequestDto { MaxResultCount = int.MaxValue, Status = status })).Items; // Paging not implemented yet
            var roles = (await _userAppService.GetRoles()).Items;
            var model = new UserListViewModel
            {
                Users = users,
                Roles = roles
            };
            model.CanModify = await CanModify(PermissionNames.Pages_Users_Modify);
            return View(model);
        }
    }
}