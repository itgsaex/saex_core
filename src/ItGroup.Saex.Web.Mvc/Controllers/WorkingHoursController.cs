using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_WorkingHours_View)]
    public class WorkingHoursController : SaexControllerBase
    {
        private readonly IWorkingHourAppService _workingHourAppService;

        public WorkingHoursController(IWorkingHourAppService workingHourAppService)
        {
            _workingHourAppService = workingHourAppService;
        }

        public async Task<ActionResult> EditWorkingHourModal(Guid id)
        {
            var output = await _workingHourAppService.Get(id);
            var model = new EditWorkingHourModalViewModel();


            //var sStarTime = DateTime.Today.Add(output.SundayStartTime);
            //var sEndTime = DateTime.Today.Add(output.SundayEndTime);

            model.SundayStartTime = DateTime.Today.Add(output.SundayStartTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.SundayEndTime = DateTime.Today.Add(output.SundayEndTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.MondayStartTime = DateTime.Today.Add(output.MondayStartTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.MondayEndTime = DateTime.Today.Add(output.MondayEndTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.TuesdayStartTime = DateTime.Today.Add(output.TuesdayStartTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.TuesdayEndTime = DateTime.Today.Add(output.TuesdayEndTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.WednesdayStartTime = DateTime.Today.Add(output.WednesdayStartTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.WednesdayEndTime = DateTime.Today.Add(output.WednesdayEndTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.ThursdayStartTime = DateTime.Today.Add(output.ThursdayStartTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.ThursdayEndTime = DateTime.Today.Add(output.ThursdayEndTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.FridayStartTime = DateTime.Today.Add(output.FridayStartTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.FridayEndTime = DateTime.Today.Add(output.FridayEndTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.SaturdayStartTime = DateTime.Today.Add(output.SaturdayStartTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.SaturdayEndTime = DateTime.Today.Add(output.SaturdayEndTime).ToString("hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
            model.Name = output.Name;
            model.Id = output.Id;

            return View("_EditWorkingHourModal", model);
        }

        public async Task<IActionResult> Index(WorkingHourSearchModel input)
        {
            var workingHours = await _workingHourAppService.Search(input);

            var model = new WorkingHourListViewModel(workingHours);

            model.CanModify = await CanModify(PermissionNames.Pages_WorkingHours_Modify);

            return View(model);
        }
    }
}
