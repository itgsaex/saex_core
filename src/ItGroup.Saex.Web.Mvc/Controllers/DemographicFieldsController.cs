using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_DemographicFields)]
    public class DemographicFieldsController : SaexControllerBase
    {
        private readonly DemographicFieldTypeManager _demographicFieldTypeManager;
        private readonly IDemographicFieldAppService _demographicFieldAppService;

        public DemographicFieldsController(IDemographicFieldAppService demographicFieldAppService, DemographicFieldTypeManager demographicFieldTypeManager)
        {
            _demographicFieldAppService = demographicFieldAppService;
            _demographicFieldTypeManager = demographicFieldTypeManager;
        }

        [HttpGet]
        public async Task<ActionResult> EditDemographicFieldModal(Guid id)
        {
            var demographicField = await _demographicFieldAppService.Get(id);

            return Ok(demographicField);
        }

        public async Task<IActionResult> Index(DemographicFieldSearchModel input)
        {
            var demographicFieldsTask = _demographicFieldAppService.Search(input);

            var model = new DemographicFieldListViewModel(await demographicFieldsTask, await GetDemographicFieldTypesAsync());
            model.CanModify = await CanModify(PermissionNames.Pages_DemographicFields_Modify);
            return View(model);
        }

        private Task<List<DemographicFieldTypeListDto>> GetDemographicFieldTypesAsync() =>
            _demographicFieldTypeManager
                .GetAll()
                .Where(x => x.IsActive)
                .OrderBy(x => x.Name)
                .Select(x => new DemographicFieldTypeListDto(x))
                .ToListAsync();
    }
}
