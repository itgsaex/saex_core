﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Abp.Domain.Entities;
using static ItGroup.Saex.Enumerations.Enumerations;
using DevExpress.XtraRichEdit.Model;
using ItGroup.Saex.Users;
using ItGroup.Saex.Users.Dto;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_AssignExternalAgents_View)]
    public class AssignExternalAgentsController : SaexControllerBase
    {
        private readonly IAssignInternalAgentsAppService _assignInternalAgentsAppService;

        public AssignExternalAgentsController(IAssignInternalAgentsAppService assignInternalAgentsAppService)
        {
            _assignInternalAgentsAppService = assignInternalAgentsAppService;
        }

        [HttpPost]
        public async Task<ActionResult> AssignAgents(long internalAgentId)
        {
            var model = new AssignExternalAgentViewModel();
            var internalAgent = await _assignInternalAgentsAppService.GetUserRecord(internalAgentId);
            var internalAgentExternalAgents = await _assignInternalAgentsAppService.GetInternalAgentExternalAgents(internalAgentId);
            model.InternalAgent = internalAgent;
            model.ExternalAgents = await _assignInternalAgentsAppService.GetExternalAgents();

            foreach (var internalAgentExternalAgent in internalAgentExternalAgents)
            {
                var deleteFromList = model.ExternalAgents.Where(a => a.Id == internalAgentExternalAgent.Id).FirstOrDefault();
                if (deleteFromList != null)
                {
                    model.ExternalAgents.Remove(deleteFromList);
                }
            }

            return View("_AssignExternalAgentsModal", model);
        }

        public async Task<IActionResult> Index(long? internalAgent = null)
        {
            var model = new AssignExternalAgentViewModel();

            if (internalAgent != null)
            {
                model.InternalAgent = await _assignInternalAgentsAppService.GetUserRecord(internalAgent.GetValueOrDefault());

                model.ExternalAgents = await _assignInternalAgentsAppService.GetInternalAgentExternalAgents(internalAgent.GetValueOrDefault());
            }

            model.InternalAgents = await _assignInternalAgentsAppService.GetInternalAgents();

            return View(model);
        }

        [HttpPost]
        public async Task RemoveAgent(long internalAgentId, long externalAgentId)
        {
            await _assignInternalAgentsAppService.DeleteInternalAgentExternalAgent(internalAgentId, externalAgentId);
        }

        [HttpPost]
        public async Task<bool> Save(long internalAgentId, List<long> selectedAgents)
        {
            var selectedExternalAgents = new List<UserDto>();

            if (selectedAgents == null || !selectedAgents.Any())
                throw new EntityNotFoundException(L("NoSelectedExternalAgentError"));

            foreach (var id in selectedAgents)
            {
                var externalAgent = await _assignInternalAgentsAppService.GetUserRecord(id);

                if (externalAgent == null)
                    throw new EntityNotFoundException(L("ExternalAgentNotFoundError"));

                selectedExternalAgents.Add(externalAgent);
            }

            if (selectedExternalAgents.Count > 0)
            {
                await _assignInternalAgentsAppService.AssignExternalAgents(internalAgentId, selectedExternalAgents);
            }

            return true;
        }
    }
}
