﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Abp.Domain.Entities;
using static ItGroup.Saex.Enumerations.Enumerations;
using DevExpress.XtraRichEdit.Model;
using ItGroup.Saex.Users;
using ItGroup.Saex.Users.Dto;
using Abp.Runtime.Session;
using ItGroup.Saex.Models;
using Abp.Domain.Uow;

namespace ItGroup.Saex.Web.Controllers
{
    public class ProyeccionPerdidaController : SaexControllerBase
    {
        private readonly IDailyReportAppService _dailyReportAppService;
        private readonly IAssignInternalAgentsAppService _assignInternalAgentsAppService;
        private readonly IAbpSession _abpSession;
        private readonly ISystemPolicyAppService _systemPolicyAppService;

        public ProyeccionPerdidaController(IDailyReportAppService dailyReportAppService,
                                     IAbpSession abpSession,
                                     IAssignInternalAgentsAppService assignInternalAgentsAppService,
                                     ISystemPolicyAppService systemPolicyAppService)
        {
            _dailyReportAppService = dailyReportAppService;
            _abpSession = abpSession;
            _assignInternalAgentsAppService = assignInternalAgentsAppService;
            _systemPolicyAppService = systemPolicyAppService;
        }

        public async Task<IActionResult> Index()
        {
            var viewModel = new ProyeccionPerdidaViewModel();
            var paramDelinquecyStages = (await _systemPolicyAppService.GetAllSystemPolicies()).FirstOrDefault(x => x.Code == "DELINQUENCY_STAGES");
            var delinquencyStates = new List<string>();

            if (paramDelinquecyStages != null)
            {
                delinquencyStates.AddRange(paramDelinquecyStages.Value.Split("|"));
                viewModel.EtapasDelincuencia = delinquencyStates;
            }
            else
            {
                viewModel.EtapasDelincuencia.Add("P");
                viewModel.EtapasDelincuencia.Add("PP");
                viewModel.EtapasDelincuencia.Add("R");
            }

            return View("Index", viewModel);
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> Search(ProyeccionPerdidaViewModel model)
        {
            var parameterModel = new ProyeccionPerdidaParameterModel();
            var supervisorUserRecord = await _assignInternalAgentsAppService.GetUserRecord(_abpSession.UserId.GetValueOrDefault());
            parameterModel.Supervisor = supervisorUserRecord.Id;
            parameterModel.EtapaDelincuencia = model.SelectedEtapaDelincuencia;

            var result = await _dailyReportAppService.ProyeccionPerdida(parameterModel);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }
    }
}
