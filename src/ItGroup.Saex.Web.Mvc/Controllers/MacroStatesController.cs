using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_MacroStates_View)]
    public class MacroStatesController : SaexControllerBase
    {
        private readonly IMacroStateAppService _macroStateAppService;

        public MacroStatesController
        (
            IMacroStateAppService macroStateAppService
        )
        {
            _macroStateAppService = macroStateAppService;
        }

        public async Task<ActionResult> EditMacroStateModal(Guid id)
        {
            var res = await _macroStateAppService.Get(id);
            var model = new EditMacroStateModalViewModel(res);

            return View("_EditMacroStateModal", model);
        }

        public async Task<IActionResult> Index(MacroStateSearchModel input)
        {
            var macroStates = await _macroStateAppService.Search(input);

            var model = new MacroStateListViewModel(macroStates);

            model.CanModify = await CanModify(PermissionNames.Pages_MacroStates_Modify);

            return View(model);
        }
    }
}
