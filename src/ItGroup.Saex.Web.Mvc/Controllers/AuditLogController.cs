using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_AuditLogs_View)]
    public class AuditLogsController : SaexControllerBase
    {
        private readonly IAuditLogAppService _auditLogAppService;

        public AuditLogsController(IAuditLogAppService auditLogAppService)
        {
            _auditLogAppService = auditLogAppService;
        }

        public async Task<ActionResult> EditAuditLogModal(Guid id)
        {
            var output = await _auditLogAppService.Get(id);
            var model = ObjectMapper.Map<EditAuditLogModalViewModel>(output);

            return View("_EditAuditLogModal", model);
        }

        public async Task<IActionResult> Index(AuditLogSearchModel input)
        {
            var auditLogs = await _auditLogAppService.Search(input);

            var model = new AuditLogListViewModel(auditLogs);

            return View(model);
        }
    }
}
