using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using Microsoft.AspNetCore.Http;
using System.IO;
using CsvHelper;
using System.Globalization;
using System.Collections.Generic;
using CsvHelper.Configuration;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Users_View)]
    public class CsvImportController : SaexControllerBase
    {
        private readonly IImportAppService _importAppService;

        public CsvImportController(
            IImportAppService importAppService
        )
        {
            _importAppService = importAppService;
        }

        [HttpPost]
        public async Task<List<CaseImportOutputDto>> Cases()
        {
            var files = Request.Form.Files;

            TextReader reader = new StreamReader(files[0].OpenReadStream());

            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Configuration.IgnoreQuotes = true;
                csvReader.Configuration.LineBreakInQuotedFieldIsBadData = true;
                csvReader.Configuration.HasHeaderRecord = true;
                csvReader.Configuration.CultureInfo = CultureInfo.InvariantCulture;

                var records = csvReader.GetRecords<CaseImport>();

                var model = new CaseImportDto(records.ToList(), ImportType.Manual);

                var res = await _importAppService.Cases(model);

                return res;
            }
        }

        [HttpPost]
        public async Task<List<ImportResponseDto>> CaseTypes()
        {
            var files = Request.Form.Files;

            TextReader reader = new StreamReader(files[0].OpenReadStream());

            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Configuration.IgnoreQuotes = true;
                csvReader.Configuration.LineBreakInQuotedFieldIsBadData = true;
                csvReader.Configuration.HasHeaderRecord = true;
                csvReader.Configuration.CultureInfo = CultureInfo.InvariantCulture;

                var records = csvReader.GetRecords<CaseTypeImport>();

                return await _importAppService.CaseTypes(new CaseTypeImportDto(records as List<CaseTypeImport>));
            }
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<List<ImportResponseDto>> MacroStates()
        {
            var files = Request.Form.Files;

            TextReader reader = new StreamReader(files[0].OpenReadStream());

            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Configuration.IgnoreQuotes = true;
                csvReader.Configuration.LineBreakInQuotedFieldIsBadData = true;
                csvReader.Configuration.HasHeaderRecord = true;
                csvReader.Configuration.CultureInfo = CultureInfo.InvariantCulture;

                var records = csvReader.GetRecords<MacroStateImport>();

                return await _importAppService.MacroStates(new MacroStateImportDto(records as List<MacroStateImport>));
            }
        }

        [HttpPost]
        public async Task<List<ImportResponseDto>> PostalCodes()
        {
            var files = Request.Form.Files;

            TextReader reader = new StreamReader(files[0].OpenReadStream());

            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Configuration.IgnoreQuotes = true;
                csvReader.Configuration.LineBreakInQuotedFieldIsBadData = true;
                csvReader.Configuration.HasHeaderRecord = true;
                csvReader.Configuration.CultureInfo = CultureInfo.InvariantCulture;

                var records = csvReader.GetRecords<PostalCodeImport>();

                var model = new PostalCodeImportDto(records.ToList());

                return await _importAppService.PostalCodes(model);
            }
        }

        [HttpPost]
        public async Task<List<ImportResponseDto>> SystemStates()
        {
            var files = Request.Form.Files;

            TextReader reader = new StreamReader(files[0].OpenReadStream());

            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Configuration.IgnoreQuotes = true;
                csvReader.Configuration.LineBreakInQuotedFieldIsBadData = true;
                csvReader.Configuration.HasHeaderRecord = true;
                csvReader.Configuration.CultureInfo = CultureInfo.InvariantCulture;

                var records = csvReader.GetRecords<SystemStateImport>();

                return await _importAppService.SystemStates(new SystemStateImportDto(records as List<SystemStateImport>));
            }
        }

        [HttpPost]
        public async Task<List<ImportResponseDto>> SystemSubStates()
        {
            var files = Request.Form.Files;

            TextReader reader = new StreamReader(files[0].OpenReadStream());

            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Configuration.IgnoreQuotes = true;
                csvReader.Configuration.LineBreakInQuotedFieldIsBadData = true;
                csvReader.Configuration.HasHeaderRecord = true;
                csvReader.Configuration.CultureInfo = CultureInfo.InvariantCulture;

                var records = csvReader.GetRecords<SystemSubStateImport>();

                return await _importAppService.SystemSubStates(new SystemSubStateImportDto(records as List<SystemSubStateImport>));
            }
        }
    }
}
