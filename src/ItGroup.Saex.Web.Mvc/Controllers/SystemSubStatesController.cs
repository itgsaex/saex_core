using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_SystemSubStates_View)]
    public class SystemSubStatesController : SaexControllerBase
    {
        private readonly ISystemSubStateAppService _systemSubStateAppService;

        public SystemSubStatesController
        (
            ISystemSubStateAppService systemSubStateAppService
        )
        {
            _systemSubStateAppService = systemSubStateAppService;
        }

        public async Task<ActionResult> EditSystemSubStateModal(Guid id)
        {
            var res = await _systemSubStateAppService.Get(id);
            var model = new EditSystemSubStateModalViewModel(res);

            return View("_EditSystemSubStateModal", model);
        }

        public async Task<IActionResult> Index(SystemSubStateSearchModel input)
        {
            var systemSubStates = await _systemSubStateAppService.Search(input);

            var model = new SystemSubStateListViewModel(systemSubStates);

            model.CanModify = await CanModify(PermissionNames.Pages_SystemSubStates_Modify);

            return View(model);
        }
    }
}
