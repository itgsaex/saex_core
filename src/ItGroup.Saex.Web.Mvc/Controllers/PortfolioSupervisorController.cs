﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Abp.Domain.Entities;
using static ItGroup.Saex.Enumerations.Enumerations;
using DevExpress.XtraRichEdit.Model;
using ItGroup.Saex.Users;
using ItGroup.Saex.Users.Dto;
using Abp.Runtime.Session;
using ItGroup.Saex.Models;
using Abp.Domain.Uow;

namespace ItGroup.Saex.Web.Controllers
{
    public class PortfolioSupervisorController : SaexControllerBase
    {
        private readonly IInternalAgentDashboardService _internalAgentDashboardAppService;
        private readonly IAssignInternalAgentsAppService _assignInternalAgentsAppService;
        private readonly IAbpSession _abpSession;
        private readonly IDailyReportAppService _dailyReportAppService;
        private readonly ISystemPolicyAppService _systemPolicyAppService;

        public PortfolioSupervisorController(IInternalAgentDashboardService internalAgentDashboardAppService,
                                             IAssignInternalAgentsAppService assignInternalAgentsAppService,
                                             IAbpSession abpSession,
                                             IDailyReportAppService dailyReportAppService,
                                             ISystemPolicyAppService systemPolicyAppService)
        {
            _internalAgentDashboardAppService = internalAgentDashboardAppService;
            _assignInternalAgentsAppService = assignInternalAgentsAppService;
            _abpSession = abpSession;
            _dailyReportAppService = dailyReportAppService;
            _systemPolicyAppService = systemPolicyAppService;
        }

        public async Task<IActionResult> Index()
        {
            var model = new PortfolioSupervisorViewModel();

            var userRecord = await _assignInternalAgentsAppService.GetUserRecord(_abpSession.UserId.GetValueOrDefault());

            if (userRecord.Agent != null && userRecord.Agent.Type == Enumerations.Enumerations.AgentType.Supervisor)
            {
                model.InternalAgents = _dailyReportAppService.GetInternalAgents(userRecord.Id);
                model.ExternalAgents = _dailyReportAppService.GetExternalAgents(userRecord.Id);
                model.Regions = _dailyReportAppService.GetRegions(userRecord.Id);
                model.Routes = _dailyReportAppService.GetRoutes(userRecord.Id);
                model.PostalCodes = _dailyReportAppService.GetPostalCodes(userRecord.Id);
                model.SupervisorId = userRecord.Id;
            }
            else
            {
                model.InternalAgents = _dailyReportAppService.GetInternalAgents(null);
                model.ExternalAgents = _dailyReportAppService.GetExternalAgents(null);
                model.Regions = _dailyReportAppService.GetRegions(null);
                model.Routes = _dailyReportAppService.GetRoutes(null);
                model.PostalCodes = _dailyReportAppService.GetPostalCodes(null);
                model.SupervisorId = null;
            }

            var paramDelinquecyStages = (await _systemPolicyAppService.GetAllSystemPolicies()).FirstOrDefault(x => x.Code == "DELINQUENCY_STAGES");

            var delinquencyStates = new List<string>();
            if (paramDelinquecyStages != null)
            {
                delinquencyStates.AddRange(paramDelinquecyStages.Value.Split("|"));
            }
            model.DelinquencyStages = delinquencyStates;
            model.Cases = new List<Saex.Models.InternalAgentDashboardModel>();

            return View("Index", model);
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> Search(PortfolioSupervisorViewModel model)
        {
            var userRecord = await _assignInternalAgentsAppService.GetUserRecord(_abpSession.UserId.GetValueOrDefault());
            var parameterModel = new ReadInternalAgentCasesParametersModel();

            if (userRecord.Agent != null && userRecord.Agent.Type == Enumerations.Enumerations.AgentType.Supervisor)
            {
                parameterModel.Supervisor = userRecord.Id;
                parameterModel.InternalAgent = model.SelectedInternalAgent.GetValueOrDefault();

                var internalAgentRecord = await _assignInternalAgentsAppService.GetUserRecord(model.SelectedInternalAgent.GetValueOrDefault());

                parameterModel.InternalAgentUsername = internalAgentRecord.UserName;
            }
            else
            {
                parameterModel.Supervisor = null;
                parameterModel.InternalAgent = model.SelectedInternalAgent.GetValueOrDefault();

                var internalAgentRecord = await _assignInternalAgentsAppService.GetUserRecord(model.SelectedInternalAgent.GetValueOrDefault());

                parameterModel.InternalAgentUsername = internalAgentRecord.UserName;
            }

            if (model.SelectedExternalAgent.HasValue)
            {
                var externalAgenteUserRecord = await _assignInternalAgentsAppService.GetUserRecord(model.SelectedExternalAgent.GetValueOrDefault());
                parameterModel.ExternalAgent = externalAgenteUserRecord.Id;
                parameterModel.ExternalAgentUsername = externalAgenteUserRecord.UserName;
            }
            else
            {
                parameterModel.ExternalAgent = null;
                parameterModel.ExternalAgentUsername = null;
            }

            if (model.SelectedRegion.HasValue)
            {
                parameterModel.RegionId = model.SelectedRegion.GetValueOrDefault();
            }
            else
            {
                parameterModel.RegionId = null;
            }

            if (model.SelectedRoute.HasValue)
            {
                parameterModel.RutaId = model.SelectedRoute.GetValueOrDefault();
            }
            else
            {
                parameterModel.RutaId = null;
            }

            if (!string.IsNullOrWhiteSpace(model.SelectedPostalCode))
            {
                parameterModel.CodigoPostal = model.SelectedPostalCode;
            }
            else
            {
                parameterModel.CodigoPostal = null;
            }

            parameterModel.ActivityCode = null;

            if (!string.IsNullOrWhiteSpace(model.SelectedDelinquencyStage))
            {
                parameterModel.EtapaDeDelincuencia = model.SelectedDelinquencyStage;
            }
            else
            {
                parameterModel.EtapaDeDelincuencia = null;
            }

            parameterModel.AlertId = null;
            parameterModel.Status = null;
            parameterModel.Estado = null;

            if (!string.IsNullOrWhiteSpace(model.AccountNumber))
            {
                parameterModel.AccountNumber = model.AccountNumber;
            }
            else
            {
                parameterModel.AccountNumber = null;
            }

            if (!string.IsNullOrWhiteSpace(model.Name))
            {
                parameterModel.Name = model.Name;
            }
            else
            {
                parameterModel.Name = null;
            }

            var result = await _internalAgentDashboardAppService.ReadInternalAgentCases(parameterModel);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> SelectedToAgentCases(PortfolioSupervisorViewModel model)
        {
            var parameterModel = new ReadInternalAgentCasesParametersModel();
            parameterModel.Supervisor = null;
            parameterModel.InternalAgent = model.SelectedToInternalAgent.GetValueOrDefault();
            var internalAgentRecord = await _assignInternalAgentsAppService.GetUserRecord(model.SelectedToInternalAgent.GetValueOrDefault());
            parameterModel.InternalAgentUsername = internalAgentRecord.UserName;
            parameterModel.ExternalAgent = null;
            parameterModel.ExternalAgentUsername = null;
            parameterModel.RegionId = null;
            parameterModel.RutaId = null;
            parameterModel.CodigoPostal = null;
            parameterModel.ActivityCode = null;
            parameterModel.EtapaDeDelincuencia = null;
            parameterModel.AlertId = null;
            parameterModel.Status = null;
            parameterModel.Estado = null;
            parameterModel.AccountNumber = null;
            parameterModel.Name = null;

            var result = await _internalAgentDashboardAppService.ReadInternalAgentCases(parameterModel);
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> SelectedToAgentTransferCases(PortfolioSupervisorViewModel model)
        {
            var result = await _dailyReportAppService.GetCopyOrTransferCases(model.SelectedToInternalAgent.GetValueOrDefault());
            return Json(new Abp.Web.Models.AjaxResponse { Result = result });
        }

        [HttpPost]
        public async Task<bool> Save(long SelectedInternalAgent, long SelectedToInternalAgent, List<string> SelectedCases, string SelectedAsignationType)
        {
            var model = new TansferModel();
            model.SelectedInternalAgent = SelectedInternalAgent;
            model.SelectedInternalToAgent = SelectedToInternalAgent;
            model.AsignationType = SelectedAsignationType;
            model.SelectedCases = SelectedCases;
            await _dailyReportAppService.SaveTransfer(model);

            return true;
        }

        [HttpPost]
        public async Task<bool> Delete(long SelectedToInternalAgent, List<string> SelectedCases)
        {
            var model = new TansferModel();
            model.SelectedInternalToAgent = SelectedToInternalAgent;
            model.SelectedCases = SelectedCases;
            await _dailyReportAppService.DeleteTransfer(model);

            return true;
        }
    }
}
