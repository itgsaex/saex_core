using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Buckets_View)]
    public class BucketsController : SaexControllerBase
    {
        private readonly IBucketAppService _bucketAppService;

        private readonly IProductAppService _productAppService;

        public BucketsController(IBucketAppService bucketAppService, IProductAppService productAppService)
        {
            _bucketAppService = bucketAppService;
            _productAppService = productAppService;
        }

        public async Task<ActionResult> EditBucketModal(Guid id)
        {
            var res = await _bucketAppService.Get(id);
            var model = ObjectMapper.Map<EditBucketModalViewModel>(res);
            model.Products = await _productAppService.GetAllForList();

            return View("_EditBucketModal", model);
        }

        public async Task<IActionResult> Index(BucketSearchModel input)
        {
            var buckets = await _bucketAppService.Search(input);

            var products = await _productAppService.GetAllForList();

            var model = new BucketListViewModel(buckets);

            model.Products = products;

            model.CanModify = await CanModify(PermissionNames.Pages_Buckets_Modify);

            return View(model);
        }
    }
}
