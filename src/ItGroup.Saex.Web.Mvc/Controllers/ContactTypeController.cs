using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_ContactTypes_View)]
    public class ContactTypesController : SaexControllerBase
    {
        private readonly IContactTypeAppService _contactTypeAppService;

        public ContactTypesController(IContactTypeAppService contactTypeAppService)
        {
            _contactTypeAppService = contactTypeAppService;
        }

        public async Task<ActionResult> EditContactTypeModal(Guid id)
        {
            var output = await _contactTypeAppService.Get(id);
            var model = ObjectMapper.Map<EditContactTypeModalViewModel>(output);

            return View("_EditContactTypeModal", model);
        }

        public async Task<IActionResult> Index(ContactTypeSearchModel input)
        {
            var contactTypes = await _contactTypeAppService.Search(input);

            var model = new ContactTypeListViewModel(contactTypes);

            model.CanModify = await CanModify(PermissionNames.Pages_ContactTypes_Modify);

            return View(model);
        }
    }
}
