﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.Mvc.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_ValidFieldsForLists_View)]
    public class ValidFieldsForListsController : SaexControllerBase
    {
        private readonly ICaseRecentActivityRepository _caseFieldsAppService;
        private readonly ICaseTypeAppService _caseTypeAppService;
        private readonly ICategoryService _categoryAppService;

        public ValidFieldsForListsController(
            ICaseRecentActivityRepository caseFieldsAppService,
            ICaseTypeAppService caseTypeAppService,
            ICategoryService categoryAppService)
        {
            _caseFieldsAppService = caseFieldsAppService;
            _caseTypeAppService = caseTypeAppService;
            _categoryAppService = categoryAppService;
        }

        public async Task<IActionResult> Index(ValidFieldsForListsSearchModel input, int page = 1)
        {
            var caseTypes = await _caseTypeAppService.GetAllOrdered(new PagedResultRequestDto{ MaxResultCount = int.MaxValue, SkipCount = 0 });

            var caseTypesList = new List<CaseTypeDto>();
            if (caseTypes != null && caseTypes.Items.Count > 0)
            {
                caseTypesList.AddRange(caseTypes.Items);
            }

            var categories = _categoryAppService.GetAllCategories();

            var model = new CaseFieldsImportListViewModel(caseTypesList, new List<CaseFieldHelperDto>(), categories);

            model.CanModify = await CanModify(PermissionNames.Pages_ValidFieldsForLists_View);

            return View(model);
        }

        public async Task<IActionResult> Post(CaseFieldsImportListViewModel model)
        {
            var caseTypes = await _caseTypeAppService.GetAllOrdered(new PagedResultRequestDto { MaxResultCount = int.MaxValue, SkipCount = 0 });

            var caseTypesList = new List<CaseTypeDto>();
            if (caseTypes != null && caseTypes.Items.Count > 0)
            {
                caseTypesList.AddRange(caseTypes.Items);
            }
            model.CaseTypes = caseTypesList;

            model.Categories = _categoryAppService.GetAllCategories();

            var caseFields = new List<CaseFieldHelper>();
            if (model.SelectedCaseTypeId != null)
            {
                caseFields = await _caseFieldsAppService.ReadCaseFields(model.SelectedCaseTypeId);
            }

            var caseFieldsList = new List<CaseFieldHelperDto>();
            foreach (var field in caseFields)
            {
                var item = new CaseFieldHelperDto();
                item.FieldID = field.FieldId;
                item.Selected = field.Selected;
                item.ColumnName = field.ColumnName;
                item.HumanName = field.HumanName;
                item.CaseTypeDescription = field.CaseTypeDescription;
                item.MasterList = field.MasterList;
                item.ChildList = field.ChildList;
                item.RiskList = field.RiskList;
                item.CategoriaID = field.CategoriaId;
                item.Sequence = field.Sequence;

                caseFieldsList.Add(item);
            }

            model.CaseFields = caseFieldsList;

            return View("Index", model);
        }
    }
}