using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_VisitPlaces_View)]
    public class VisitPlacesController : SaexControllerBase
    {
        private readonly IVisitPlaceAppService _visitPlaceAppService;

        public VisitPlacesController(IVisitPlaceAppService visitPlaceAppService)
        {
            _visitPlaceAppService = visitPlaceAppService;
        }

        public async Task<ActionResult> EditVisitPlaceModal(Guid id)
        {
            var output = await _visitPlaceAppService.Get(id);
            var model = ObjectMapper.Map<EditVisitPlaceModalViewModel>(output);

            return View("_EditVisitPlaceModal", model);
        }

        public async Task<IActionResult> Index(VisitPlaceSearchModel input)
        {
            var visitPlaces = await _visitPlaceAppService.Search(input);

            var model = new VisitPlaceListViewModel(visitPlaces);

            model.CanModify = await CanModify(PermissionNames.Pages_VisitPlaces_Modify);

            return View(model);
        }
    }
}
