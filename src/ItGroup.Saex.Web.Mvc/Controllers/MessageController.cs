using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Messages_View)]
    public class MessagesController : SaexControllerBase
    {
        private readonly IAgentAppService _agentAppService;

        private readonly IMessageAppService _messageAppService;

        public MessagesController(IMessageAppService messageAppService, IAgentAppService agentAppService)
        {
            _messageAppService = messageAppService;
            _agentAppService = agentAppService;
        }

        public async Task<ActionResult> EditMessageModal(Guid id)
        {
            var output = await _messageAppService.Get(id);
            var model = ObjectMapper.Map<EditMessageModalViewModel>(output);

            return View("_EditMessageModal", model);
        }

        public async Task<IActionResult> Index(MessageSearchModel input)
        {
            var messages = await _messageAppService.Search(input);

            var model = new MessageListViewModel(messages);
            var agents = await _agentAppService.GetAllForList();
            model.AvailableAgents = agents.Where(x => x.UserId != AbpSession.UserId).ToList();
            model.AvailableAgents.Sort((a, e) => String.Compare(a.Fullname, e.Fullname));

            return View(model);
        }
    }
}
