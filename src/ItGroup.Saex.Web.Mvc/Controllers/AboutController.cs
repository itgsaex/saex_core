﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Controllers;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : SaexControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
