using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using ItGroup.Saex.Authorization;
using ItGroup.Saex.Controllers;
using ItGroup.Saex.DomainEntities;
using System;
using ItGroup.Saex.Web.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Abp.Domain.Entities;
using static ItGroup.Saex.Enumerations.Enumerations;
using DevExpress.XtraRichEdit.Model;
using ItGroup.Saex.Users;
using ItGroup.Saex.Users.Dto;

namespace ItGroup.Saex.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_AgentSupervisors_View)]
    public class AgentSupervisorsController : SaexControllerBase
    {
        private readonly IAssignInternalAgentsAppService _assignInternalAgentsAppService;

        public AgentSupervisorsController(IAssignInternalAgentsAppService assignInternalAgentsAppService)
        {
            _assignInternalAgentsAppService = assignInternalAgentsAppService;
        }

        [HttpPost]
        public async Task<ActionResult> AssignAgents(long supervisorId)
        {
            var model = new AssignInternalAgentViewModel();
            var supervisor = await _assignInternalAgentsAppService.GetUserRecord(supervisorId);
            var supervisorInternalAgents = await _assignInternalAgentsAppService.GetSupervisorInternalAgents(supervisorId);
            model.Supervisor = supervisor;
            model.InternalAgents = await _assignInternalAgentsAppService.GetInternalAgents();

            foreach (var supervisorInternalAgent in supervisorInternalAgents)
            {
                var deleteFromList = model.InternalAgents.Where(a => a.Id == supervisorInternalAgent.Id).FirstOrDefault();
                if (deleteFromList != null)
                {
                    model.InternalAgents.Remove(deleteFromList);
                }
            }

            return View("_AssignAgentsModal", model);
        }

        public async Task<IActionResult> Index(long? supervisor = null)
        {
            var model = new AssignInternalAgentViewModel();

            if (supervisor != null)
            {
                model.Supervisor = await _assignInternalAgentsAppService.GetUserRecord(supervisor.GetValueOrDefault());

                model.InternalAgents = await _assignInternalAgentsAppService.GetSupervisorInternalAgents(supervisor.GetValueOrDefault());
            }

            model.Supervisors = await _assignInternalAgentsAppService.GetSupervisors();

            return View(model);
        }

        [HttpPost]
        public async Task RemoveAgent(long supervisorId, long internalAgentId)
        {
            await _assignInternalAgentsAppService.DeleteSupervisorInternalAgent(supervisorId, internalAgentId);
        }

        [HttpPost]
        public async Task<bool> Save(long supervisorId, List<long> selectedAgents)
        {
            var selectedInternalAgents = new List<UserDto>();

            if (selectedAgents == null || !selectedAgents.Any())
                throw new EntityNotFoundException(L("NoSelectedInternalAgentError"));

            foreach (var id in selectedAgents)
            {
                var internalAgent = await _assignInternalAgentsAppService.GetUserRecord(id);

                if (internalAgent == null)
                    throw new EntityNotFoundException(L("InternalAgentNotFoundError"));

                selectedInternalAgents.Add(internalAgent);
            }

            if (selectedInternalAgents.Count > 0)
            {
                await _assignInternalAgentsAppService.AssignInternalAgents(supervisorId, selectedInternalAgents);
            }

            return true;
        }
    }
}
