using System;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;

namespace ItGroup.Saex.Web.Reports
{
    public partial class TransferredCasesReport
    {
        public TransferredCasesReport()
        {
            InitializeComponent();
            var accountNumber = Parameters.GetByName("accountNumber");
            accountNumber.Value = null;

            var assignedAgent = Parameters.GetByName("assignedAgent");
            assignedAgent.Value = null;

            var name = Parameters.GetByName("name");
            name.Value = null;

            var productNumber = Parameters.GetByName("productNumber");
            productNumber.Value = null;

            var transferredAgent = Parameters.GetByName("transferredAgent");
            transferredAgent.Value = null;

            var zipCode = Parameters.GetByName("zipCode");
            zipCode.Value = null;

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
