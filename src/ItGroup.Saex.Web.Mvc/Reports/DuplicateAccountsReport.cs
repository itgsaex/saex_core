using System;
using System.Collections.Generic;
using DevExpress.Data;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Reports
{
    public partial class DuplicateAccountsReport
    {
        public DuplicateAccountsReport()
        {
            InitializeComponent();
        }

        public DuplicateAccountsReport(string agentID, int? regionID, int? rutaID) : this()
        {
            var agentIDParam = Parameters.GetByName("agenteID");
            agentIDParam.Value = agentID;

            var rutaIDParam = Parameters.GetByName("rutaID");
            if (rutaID.HasValue)
                rutaIDParam.Value = rutaID.Value;
            else
                rutaIDParam.Value = null;

            var regionIDParam = Parameters.GetByName("regionID");
            if (regionID.HasValue)
                regionIDParam.Value = regionID.Value;
            else
                regionIDParam.Value = null;

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
