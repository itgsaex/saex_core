using System;
using System.Collections.Generic;
using DevExpress.Data;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Reports
{
    public partial class KPICasosResueltos
    {
        public KPICasosResueltos()
        {
            InitializeComponent();
        }

        public KPICasosResueltos(string agente, DateTime desde, DateTime hasta) : this()
        {
            var paramAgente = Parameters.GetByName("agente");
            paramAgente.Value = agente ?? "Todos";

            var paramDesde = Parameters.GetByName("desde");
            paramDesde.Value = desde;

            var paramHasta = Parameters.GetByName("hasta");
            paramHasta.Value = hasta;

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
