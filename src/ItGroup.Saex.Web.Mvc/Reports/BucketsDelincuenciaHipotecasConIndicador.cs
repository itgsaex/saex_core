using System;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using Abp.AspNetCore.Mvc.Views;

namespace ItGroup.Saex.Web.Reports
{
    public partial class BucketsDelincuenciaHipotecasConIndicador
    {
        public BucketsDelincuenciaHipotecasConIndicador()
        {
            InitializeComponent();
        }

        public BucketsDelincuenciaHipotecasConIndicador(string atraso, string visitado, string indicador) : this()
        {
            var paramAtraso = Parameters.GetByName("atraso");
            paramAtraso.Value = atraso ?? "Todos";

            var paramVisitado = Parameters.GetByName("visitado");
            paramVisitado.Value = visitado ?? "Todos";

            var paramIndicador = Parameters.GetByName("indicador");
            paramIndicador.Value = indicador ?? "Todos";

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
