using System;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Reports
{
    public partial class UnfilteredCasesByMasterReport
    {
        public UnfilteredCasesByMasterReport()
        {
            InitializeComponent();
        }

        public UnfilteredCasesByMasterReport(Product product, string accountNumber, string clientName) : this()
        {
            var dateProduct = Parameters.GetByName("productType");
            dateProduct.Value = product != null ? product.Code : "";

            var accountNumberParam = Parameters.GetByName("accountNumber");
            accountNumberParam.Value = accountNumber;

            var nameParam = Parameters.GetByName("name");
            nameParam.Value = clientName;

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
