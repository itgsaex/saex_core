using System;
using System.Collections.Generic;
using DevExpress.Data;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Reports
{
    public partial class AuditoriaAccesoUsuarios
    {
        public AuditoriaAccesoUsuarios()
        {
            InitializeComponent();
        }

        public AuditoriaAccesoUsuarios(DateTime desde, DateTime hasta) : this()
        {
            var paramDesde = Parameters.GetByName("desde");
            paramDesde.Value = desde;

            var paramHasta = Parameters.GetByName("hasta");
            paramHasta.Value = hasta;

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
