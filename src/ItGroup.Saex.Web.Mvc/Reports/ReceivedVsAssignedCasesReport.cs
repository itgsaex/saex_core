using System;
using DevExpress.XtraReports.UI;

namespace ItGroup.Saex.Web.Reports
{
    public partial class ReceivedVsAssignedCasesReport
    {
        public ReceivedVsAssignedCasesReport()
        {
            InitializeComponent();
            this.FillDataSource();
        }
    }
}
