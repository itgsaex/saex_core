using System;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;

namespace ItGroup.Saex.Web.Reports
{
    public partial class AddressManagementReport
    {
        public AddressManagementReport()
        {
            InitializeComponent();
        }

        public AddressManagementReport(DateTime dateFrom, DateTime dateTo) : this()
        {
            var dateFromParam = Parameters.GetByName("dateFrom");
            dateFromParam.Value = dateFrom;

            var dateToParam = Parameters.GetByName("dateTo");
            dateToParam.Value = dateTo;

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
