using DevExpress.XtraReports.UI;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.Web.Reports
{
    public partial class Users
    {
        public Users(IEnumerable<UsersReport> reportData) : this()
        {
            DataSource = reportData;
        }

        public Users()
        {
            InitializeComponent();
        }
    }
}
