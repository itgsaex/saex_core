using System;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using Abp.AspNetCore.Mvc.Views;

namespace ItGroup.Saex.Web.Reports
{
    public partial class BucketsDelincuenciaHipotecas
    {
        public BucketsDelincuenciaHipotecas()
        {
            InitializeComponent();
        }

        public BucketsDelincuenciaHipotecas(string atraso, string visitado) : this()
        {
            var paramAtraso = Parameters.GetByName("atraso");
            paramAtraso.Value = atraso ?? "Todos";

            var paramVisitado = Parameters.GetByName("visitado");
            paramVisitado.Value = visitado ?? "Todos";

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
