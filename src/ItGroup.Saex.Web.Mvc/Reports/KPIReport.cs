using System;
using System.Collections.Generic;
using DevExpress.Data;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Reports
{
    public partial class KPIReport
    {
        public KPIReport()
        {
            InitializeComponent();
        }

        public KPIReport(string agentID,string dateType,DateTime fechaComienzo, DateTime fechaFin) : this()
        {
            var agentIDParam = Parameters.GetByName("agentID");
            agentIDParam.Value = agentID;

            var dateTypeParam = Parameters.GetByName("dateType");
            dateTypeParam.Value = dateType;

            var fechaComienzoParam = Parameters.GetByName("fechaComienzo");
            fechaComienzoParam.Value = fechaComienzo;

            var fechaFinParam = Parameters.GetByName("fechaFin");
            fechaFinParam.Value = fechaFin;

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
