using System;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;

namespace ItGroup.Saex.Web.Reports
{
    public partial class ActivityReport
    {
        public ActivityReport()
        {
            InitializeComponent();
        }

        public ActivityReport(long? supervisor, string supervisorName, long? agenteInt, string agenteIntName, long? agenteFrom, long? agenteTo, DateTime? dateFrom, DateTime? dateTo, string agentFromName, string agentToName) : this()
        {
            if (supervisor.HasValue)
            {
                lbParameters.Text = lbParameters.Text + string.Format("Supervisor: {0}" + Environment.NewLine, supervisorName);
            }

            if (agenteInt.HasValue)
            {
                lbParameters.Text = lbParameters.Text + string.Format("Agente Interno: {0}" + Environment.NewLine, agenteIntName);
            }

            if (agenteFrom.HasValue && agenteTo.HasValue)
            {
                lbParameters.Text = lbParameters.Text + string.Format("Agente Desde: {0} Hasta: {1}" + Environment.NewLine, agentFromName, agentToName);
            }

            if (dateFrom.HasValue && dateTo.HasValue)
            {
                lbParameters.Text = lbParameters.Text + string.Format("Fecha Desde: {0:MM/dd/yyyy} Hasta: {1:MM/dd/yyyy}" + Environment.NewLine, dateFrom.GetValueOrDefault(), dateTo.GetValueOrDefault());
            }

            var paramSupervisor = Parameters.GetByName("supervisor");
            if (supervisor.HasValue)
                paramSupervisor.Value = supervisor.GetValueOrDefault();
            else
                paramSupervisor.Value = null;

            var paramAgenteInt = Parameters.GetByName("agenteInt");
            if (agenteInt.HasValue)
                paramAgenteInt.Value = agenteInt.GetValueOrDefault();
            else
                paramAgenteInt.Value = null;

            var paramAgenteFrom = Parameters.GetByName("agenteFrom");
            if (agenteFrom.HasValue)
                paramAgenteFrom.Value = agenteFrom.GetValueOrDefault();
            else
                paramAgenteFrom.Value = null;

            var paramAgenteTo = Parameters.GetByName("agenteTo");
            if (agenteTo.HasValue)
                paramAgenteTo.Value = agenteTo.GetValueOrDefault();
            else
                paramAgenteTo.Value = null;

            var paramDateFrom = Parameters.GetByName("dateFrom");
            if (dateFrom.HasValue)
                paramDateFrom.Value = dateFrom.GetValueOrDefault();
            else
                paramDateFrom.Value = null;

            var paramDateTo = Parameters.GetByName("dateTo");
            if (dateTo.HasValue)
                paramDateTo.Value = dateTo.GetValueOrDefault();
            else
                paramDateTo.Value = null;

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
