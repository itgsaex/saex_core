using System;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Reports
{
    public partial class UnassignedCasesReport
    {
        public UnassignedCasesReport()
        {
            InitializeComponent();
        }

        public UnassignedCasesReport(Product product, string accountNumber, string clientName, string zipCode) : this()
        {
            var dateProduct = Parameters.GetByName("productNumber");
            dateProduct.Value = product != null ? product.Code : "";

            var accountNumberParam = Parameters.GetByName("accountNumber");
            accountNumberParam.Value = accountNumber;

            var nameParam = Parameters.GetByName("name");
            nameParam.Value = clientName;

            var zipCodeParam = Parameters.GetByName("zipCode");
            zipCodeParam.Value = zipCode;

            this.sqlDataSource1.Fill(this.Parameters);
        }
    }
}
