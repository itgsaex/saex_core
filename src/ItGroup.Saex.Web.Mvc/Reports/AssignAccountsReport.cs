using System;
using System.Collections.Generic;
using DevExpress.Data;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Reports
{
    public partial class AssignAccountsReport
    {
        public AssignAccountsReport()
        {
            InitializeComponent();
        }

        public AssignAccountsReport(string agentID, int? regionID, int? rutaID) : this()
        {
            Parameters.GetByName("agenteID").Value = agentID;

            if (rutaID.HasValue)
                Parameters.GetByName("rutaID").Value = rutaID.Value;
            else
                Parameters.GetByName("rutaID").Value = null;

            if (regionID.HasValue)
                Parameters.GetByName("regionID").Value = regionID.Value;
            else
                Parameters.GetByName("regionID").Value = null;

            this.sqlDataSource1.Fill(Parameters);
        }
    }
}
