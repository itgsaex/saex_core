﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseCommentManager : ICaseCommentManager
    {
        private readonly IRepository<CaseComment, Guid> _repository;

        public CaseCommentManager()
        {
        }

        public CaseCommentManager(
            IRepository<CaseComment, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<CaseComment> CreateAsync(CaseComment input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(CaseComment input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<CaseComment> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<CaseComment> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<CaseComment> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<CaseComment>> GetFiltered(CaseCommentSearchModel filters)
        {
            var res = new PagedResultDto<CaseComment>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x => x.Comment.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<CaseComment> UpdateAsync(CaseComment input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
