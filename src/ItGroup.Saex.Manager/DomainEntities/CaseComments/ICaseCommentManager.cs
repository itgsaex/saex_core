﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseCommentManager : IDomainService
    {
        Task<CaseComment> CreateAsync(CaseComment input);

        Task DeleteAsync(CaseComment input);

        IQueryable<CaseComment> GetAll();

        Task<CaseComment> GetAsync(Guid id);

        Task<PagedResultDto<CaseComment>> GetFiltered(CaseCommentSearchModel filters);

        Task<CaseComment> UpdateAsync(CaseComment input);
    }
}
