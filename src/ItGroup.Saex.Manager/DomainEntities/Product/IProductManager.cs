﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IProductManager : IDomainService
    {
        Task<Product> CreateAsync(Product input);

        Task<Product> UpdateAsync(Product input);

        Task DeleteAsync(Product input);

        Task<Product> GetAsync(Guid id);

        IQueryable<Product> GetAll();

        Task<PagedResultDto<Product>> GetFiltered(ProductSearchModel filters);
    }
}