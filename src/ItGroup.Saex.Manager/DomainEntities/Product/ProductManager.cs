﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ProductManager : IProductManager
    {
        private readonly IRepository<Product, Guid> _repository;

        public ProductManager()
        {
        }

        public ProductManager(
            IRepository<Product, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<Product> CreateAsync(Product input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Product input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Product> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<Product> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<Product> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<Product> GetByCode(string code)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Code.ToLower() == code.ToLower())
                            .FirstOrDefaultAsync();
        }

        public async Task<Product> GetByProductNumber(string number)
        {
            return await this.GetAllQuery()
                            .Where(x => x.ProductNumber == number)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Product>> GetFiltered(ProductSearchModel filters)
        {
            var res = new PagedResultDto<Product>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<Product> UpdateAsync(Product input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
