﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using ItGroup.Saex.DomainEntities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public interface ICaseAlertManager : IDomainService
    {
        IQueryable<Alert> GetAlerts();

        IQueryable<CaseAlert> GetCaseAlerts(string agentID);
    }
}