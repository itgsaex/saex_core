﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class CaseAlertManager : ICaseAlertManager
    {

        private readonly IRepository<CaseAlert, Guid> _caseAlertrepository;
        private readonly IRepository<Alert, int> _alertRepository;
        public CaseAlertManager() { }
        public CaseAlertManager(IRepository<CaseAlert, Guid> caseAlertrepository, IRepository<Alert, int> alertRepository)
        {
            _caseAlertrepository = caseAlertrepository;
            _alertRepository = alertRepository;
        }

        public IQueryable<Alert> GetAlerts()
        {
            return _alertRepository
                   .GetAll();
        }

        public IQueryable<CaseAlert> GetCaseAlerts(string agentID)
        {
            return _caseAlertrepository
                   .GetAll()
                   .Where(x => x.AgentID == agentID);
        }
    }
}