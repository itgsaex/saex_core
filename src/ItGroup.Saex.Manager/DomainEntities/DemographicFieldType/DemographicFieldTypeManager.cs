﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldTypeManager : IDemographicFieldTypeManager
    {
        private readonly IRepository<DemographicFieldType, Guid> _repository;

        public DemographicFieldTypeManager() { }

        public DemographicFieldTypeManager(IRepository<DemographicFieldType, Guid> repository)
        {
            _repository = repository;
        }

        public IQueryable<DemographicFieldType> GetAll() =>
            GetAllQuery();

        public IQueryable<DemographicFieldType> GetAllQuery() =>
            _repository
                .GetAll()
                .Where(x => !x.IsDeleted);

        public Task<DemographicFieldType> GetAsync(Guid id) =>
            GetAllQuery()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

        public async Task<PagedResultDto<DemographicFieldType>> GetFiltered(DemographicFieldTypeSearchModel filters)
        {
            var query = GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
                query = query.Where(x =>
                    x.Name.ToLower().Contains(filters.SearchText.ToLower())
                );

            Expression<Func<DemographicFieldType, string>> sortField = x => x.Name;

            switch (filters.SortOrder)
            {
                case SortOrder.Ascending:
                    query = query.OrderBy(sortField);
                    break;
                case SortOrder.Descending:
                    query = query.OrderByDescending(sortField);
                    break;
            }

            var results = await query.ToListAsync();

            return new PagedResultDto<DemographicFieldType>
            {
                TotalCount = results.Count,
                Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList()
            };
        }
    }
}
