﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IDemographicFieldTypeManager : IDomainService
    {
        Task<DemographicFieldType> GetAsync(Guid id);

        IQueryable<DemographicFieldType> GetAll();

        Task<PagedResultDto<DemographicFieldType>> GetFiltered(DemographicFieldTypeSearchModel filters);
    }
}
