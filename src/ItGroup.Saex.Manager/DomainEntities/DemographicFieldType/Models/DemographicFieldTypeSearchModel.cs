﻿using Abp.Application.Services.Dto;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldTypeSearchModel : PagedResultRequestDto
    {
        public string SearchText { get; set; }

        public SortOrder SortOrder { get; set; }
    }
}
