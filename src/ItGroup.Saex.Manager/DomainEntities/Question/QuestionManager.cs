﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class QuestionManager : IQuestionManager
    {
        private readonly IRepository<Question, Guid> _repository;

        public QuestionManager()
        {
        }

        public QuestionManager(
            IRepository<Question, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<Question> CreateAsync(Question input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Question input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Question> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<Question> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<Question> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Question>> GetFiltered(QuestionSearchModel filters)
        {
            var res = new PagedResultDto<Question>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Position);
            else
                query = query.OrderByDescending(x => x.Position);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<Question> UpdateAsync(Question input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}