﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IQuestionManager : IDomainService
    {
        Task<Question> CreateAsync(Question input);

        Task<Question> UpdateAsync(Question input);

        Task DeleteAsync(Question input);

        Task<Question> GetAsync(Guid id);

        IQueryable<Question> GetAll();

        Task<PagedResultDto<Question>> GetFiltered(QuestionSearchModel filters);
    }
}