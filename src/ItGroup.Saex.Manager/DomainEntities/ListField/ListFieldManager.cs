﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListFieldManager : IListFieldManager
    {
        private readonly IRepository<ListField, Guid> _repository;
        public ListFieldManager() { }
        public ListFieldManager(
            IRepository<ListField, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<ListField> CreateAsync(ListField input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(ListField input)
        {
            await _repository.DeleteAsync(input);
        }

        public async Task<ListField> UpdateAsync(ListField input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<ListField> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public IQueryable<ListField> GetAll()
        {
            return this.GetAllQuery();
        }

        public async Task<PagedResultDto<ListField>> GetFiltered(ListFieldSearchModel filters)
        {
            var res = new PagedResultDto<ListField>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.FieldName.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.FieldId.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Position);
            else
                query = query.OrderByDescending(x => x.Position);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;

        }

        public IQueryable<ListField> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }
    }
}