﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IListFieldManager : IDomainService
    {
        Task<ListField> CreateAsync(ListField input);

        Task<ListField> UpdateAsync(ListField input);

        Task DeleteAsync(ListField input);

        Task<ListField> GetAsync(Guid id);

        IQueryable<ListField> GetAll();

        Task<PagedResultDto<ListField>> GetFiltered(ListFieldSearchModel filters);
    }
}