﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemSubStateManager : ISystemSubStateManager
    {
        private readonly IRepository<SystemSubState, Guid> _repository;

        public SystemSubStateManager()
        {
        }

        public SystemSubStateManager(
            IRepository<SystemSubState, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<SystemSubState> CreateAsync(SystemSubState input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(SystemSubState input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<SystemSubState> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<SystemSubState> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<SystemSubState> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<SystemSubState> GetByCode(string code)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Code == code)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<SystemSubState>> GetFiltered(SystemSubStateSearchModel filters)
        {
            var res = new PagedResultDto<SystemSubState>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<SystemSubState> UpdateAsync(SystemSubState input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
