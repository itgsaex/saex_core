﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISystemSubStateManager : IDomainService
    {
        Task<SystemSubState> CreateAsync(SystemSubState input);

        Task<SystemSubState> UpdateAsync(SystemSubState input);

        Task DeleteAsync(SystemSubState input);

        Task<SystemSubState> GetAsync(Guid id);

        IQueryable<SystemSubState> GetAll();

        Task<PagedResultDto<SystemSubState>> GetFiltered(SystemSubStateSearchModel filters);
    }
}