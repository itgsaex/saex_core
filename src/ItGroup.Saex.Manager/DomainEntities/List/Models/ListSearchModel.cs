﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListSearchModel : PagedResultRequestDto
    {
        public ListSearchModel()
        {

        }

        public ListSearchModel(ListType listType)
        {
            ListType = listType;
        }

        public ListType ListType { get; set; }

        public string SearchText { get; set; }

        public SortOrder SortOrder { get; set; }
    }
}
