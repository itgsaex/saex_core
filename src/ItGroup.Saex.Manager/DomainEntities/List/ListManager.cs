﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListManager : IListManager
    {
        private readonly IRepository<List, Guid> _repository;

        private readonly IRepository<AgentList, Guid> _agentListRepository;

        public ListManager()
        {
        }

        public ListManager(
            IRepository<List, Guid> repository, IRepository<AgentList, Guid> agentListRepository)
        {
            _repository = repository;
            _agentListRepository = agentListRepository;
        }

        public async Task<List> CreateAsync(List input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(List input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<List> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<List> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<List> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<List>> GetFiltered(ListSearchModel filters)
        {
            var res = new PagedResultDto<List>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Query.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.SerializedQuery.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.ListType != ListType.Unknown)
            {
                query = query.Where(x => x.Type == filters.ListType);
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<List> UpdateAsync(List input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<List<AgentList>> GetAssignedByAgentId(Guid id)
        {
            var filter = new AssignRegionSearchModel();
            filter.AgentId = id;

            return await this.GetAllAssigned(filter);
        }

        public async Task<List<AgentList>> GetAllAssigned(AssignRegionSearchModel filters)
        {
            var query = _agentListRepository.GetAll()
                            .Include(x => x.Agent)
                            .Include(x => x.Agent).ThenInclude(y => y.User)
                            .Include(x => x.List)
                            .Where(x => !x.Agent.IsDeleted);

            if (filters.AgentId != null)
            {
                query = query.Where(x => x.AgentId == filters.AgentId);
            }

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Agent.User.Name.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Agent.User.Surname.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Agent.User.UserName.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Agent.User.EmailAddress.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Id);
            else
                query = query.OrderByDescending(x => x.Id);

            return await query.ToListAsync();
        }

        public async Task DeleteAssignedAsync(AgentList input)
        {
            await _agentListRepository.DeleteAsync(input);
        }
    }
}