﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IListManager : IDomainService
    {
        Task<List> CreateAsync(List input);

        Task<List> UpdateAsync(List input);

        Task DeleteAsync(List input);

        Task<List> GetAsync(Guid id);

        IQueryable<List> GetAll();

        Task<PagedResultDto<List>> GetFiltered(ListSearchModel filters);

        Task<List<AgentList>> GetAssignedByAgentId(Guid id);

        Task<List<AgentList>> GetAllAssigned(AssignRegionSearchModel filters);

        Task DeleteAssignedAsync(AgentList input);
    }
}