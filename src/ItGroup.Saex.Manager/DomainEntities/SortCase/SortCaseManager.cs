﻿using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using System;
using System.Linq;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class SortCaseManager : ISortCaseManager
    {

        private readonly IRepository<SortCase, Guid> _repository;
        public SortCaseManager() { }
        public SortCaseManager(IRepository<SortCase, Guid> SortCaseManagerRepository)
        {
            _repository = SortCaseManagerRepository;
        }

        public IQueryable<SortCase> GetAllSortCases()
        {
            return _repository
                    .GetAll();
        }
    }
}