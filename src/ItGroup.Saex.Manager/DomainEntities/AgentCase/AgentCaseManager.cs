﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentCaseManager : IAgentCaseManager
    {
        private readonly IRepository<AgentCase, Guid> _repository;

        public AgentCaseManager()
        {
        }

        public AgentCaseManager(
            IRepository<AgentCase, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<AgentCase> CreateAsync(AgentCase input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(AgentCase input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<AgentCase> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<AgentCase> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    .Include(x => x.Case)
                        .ThenInclude(x => x.Product)
                            .ThenInclude(x => x.Product)
                    .Include(x => x.Case)
                        .ThenInclude(x => x.Customers)
                            .ThenInclude(y => y.Addresses)
                                .ThenInclude(z => z.PostalCode)
                    .Include(x => x.Agent).ThenInclude(x => x.User);
        }

        public async Task<AgentCase> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<AgentCase>> GetFiltered(AgentCaseSearchModel filters)
        {
            var res = new PagedResultDto<AgentCase>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Agent.User.FullName.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Agent.User.FullName.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<AgentCase> UpdateAsync(AgentCase input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
