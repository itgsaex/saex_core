﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAgentCaseManager : IDomainService
    {
        Task<AgentCase> CreateAsync(AgentCase input);

        Task DeleteAsync(AgentCase input);

        IQueryable<AgentCase> GetAll();

        Task<AgentCase> GetAsync(Guid id);

        Task<PagedResultDto<AgentCase>> GetFiltered(AgentCaseSearchModel filters);

        Task<AgentCase> UpdateAsync(AgentCase input);
    }
}
