﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using ItGroup.Saex.Web.Host.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseActivityManager : IDomainService
    {
        Task<CaseActivity> CreateAsync(CaseActivity input);

        Task<CaseActivity> UpdateAsync(CaseActivity input);

        Task DeleteAsync(CaseActivity input);

        Task<CaseActivity> GetAsync(Guid id);

        IQueryable<CaseActivity> GetAll();

        Task<PagedResultDto<CaseActivity>> GetFiltered(CaseActivitySearchModel filters);

        Task FlushActivitySyncModel(ActivitySyncModel model);
    }
}