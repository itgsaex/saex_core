﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.Web.Host.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseActivityManager : ICaseActivityManager
    {
        private readonly IRepository<CaseActivity, Guid> _repository;

        private readonly IRepository<ActivityDetail, Guid> _repositoryActivityDetail;
        private readonly IRepository<ActivityLine, Guid> _repositoryActivityLine;
        private readonly IRepository<ActivityReactionReasonDetail, Guid> _repositoryActivityReactionReasonDetail;
        private readonly ICaseRecentActivityRepository _repositoryCaseRecentActivity;

        public CaseActivityManager()
        {
        }

        public CaseActivityManager(
            IRepository<CaseActivity, Guid> repository, 
            IRepository<ActivityDetail, Guid> repositoryActivityDetail, 
            IRepository<ActivityLine, Guid> repositoryActivityLine, 
            IRepository<ActivityReactionReasonDetail, Guid> repositoryActivityReactionReasonDetail, 
            ICaseRecentActivityRepository repositoryCaseRecentActivity)
        {
            _repository = repository;
            _repositoryActivityDetail = repositoryActivityDetail;
            _repositoryActivityLine = repositoryActivityLine;
            _repositoryActivityReactionReasonDetail = repositoryActivityReactionReasonDetail;
            _repositoryCaseRecentActivity = repositoryCaseRecentActivity;
        }

        public async Task<CaseActivity> CreateAsync(CaseActivity input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(CaseActivity input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<CaseActivity> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<CaseActivity> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<CaseActivity> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<CaseActivity>> GetFiltered(CaseActivitySearchModel filters)
        {
            var res = new PagedResultDto<CaseActivity>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Comments.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<CaseActivity> UpdateAsync(CaseActivity input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task FlushActivitySyncModel(ActivitySyncModel model)
        {
            try
            {
                if (model != null && model.CaseActivity != null)
                {
                    await _repository.InsertAsync(model.CaseActivity);
                    if (model.ActivityDetails != null)
                    {
                        foreach (var detail in model.ActivityDetails)
                        {
                            await _repositoryActivityDetail.InsertAsync(detail);
                        }
                    }

                    if (model.ActivityLines != null)
                    {
                        foreach (var line in model.ActivityLines)
                        {
                            await _repositoryActivityLine.InsertAsync(line);
                        }
                    }

                    if (model.ActivityReactionReasonDetails != null)
                    {
                        foreach (var reason in model.ActivityReactionReasonDetails)
                        {
                            await _repositoryActivityReactionReasonDetail.InsertAsync(reason);
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        public async Task<List<CaseRecentActivity>> GetRecentActivity(string caseNumber)
        {
            return await _repositoryCaseRecentActivity.GetRecentActivities(caseNumber);
        }

        public void SetCaseMandatory(string caseNumber, string newValue)
        {
            _repositoryCaseRecentActivity.SetCaseMandatory(caseNumber, newValue);
        }

        public async Task<List<CaseFieldHelper>> ReadCaseFields (Guid? product)
        {
            return await _repositoryCaseRecentActivity.ReadCaseFields(product);
        }

        public void SaveCaseFields(List<CaseFieldHelper> data)
        {
            _repositoryCaseRecentActivity.SaveCaseFields(data);
        }

        public async Task<List<TestQueryHelper>> TestQuery(ListType listType, string tableName, string whereClause, string fieldsToReturn, string fieldsToPivot, string temporaryTable)
        {
            return await _repositoryCaseRecentActivity.TestQuery(listType, tableName, whereClause, fieldsToReturn, fieldsToPivot, temporaryTable);
        }
    }
}