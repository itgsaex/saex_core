﻿using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using System;
using System.Linq;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class CaseConditionManager : ICaseConditionManager
    {

        private readonly IRepository<ConditionCase, Guid> _conditionCaseRepository;

        public CaseConditionManager() { }
        public CaseConditionManager(IRepository<ConditionCase, Guid> conditionCaseRepository)
        {
            _conditionCaseRepository = conditionCaseRepository;
        }

        public IQueryable<ConditionCase> GetConditionCases()
        {
            return _conditionCaseRepository
                   .GetAll();
        }
    }
}