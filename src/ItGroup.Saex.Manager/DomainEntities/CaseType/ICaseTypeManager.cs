﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseTypeManager : IDomainService
    {
        Task<CaseType> CreateAsync(CaseType input);

        Task DeleteAsync(CaseType input);

        IQueryable<CaseType> GetAll();

        IQueryable<CaseType> GetAllOrdered();

        Task<CaseType> GetAsync(Guid id);

        Task<PagedResultDto<CaseType>> GetFiltered(CaseTypeSearchModel filters);

        Task<CaseType> UpdateAsync(CaseType input);
    }
}
