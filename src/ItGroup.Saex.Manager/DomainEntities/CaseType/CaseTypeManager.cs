﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.EntityFrameworkCore;
using ItGroup.Saex.EntityFrameworkCore.Seed;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTypeManager : ICaseTypeManager
    {
        private readonly SaexDbContext _context;

        private readonly IRepository<CaseType, Guid> _repository;

        public CaseTypeManager()
        {
        }

        public CaseTypeManager(
            IRepository<CaseType, Guid> repository
         )
        {
            _repository = repository;
            _context = ContextHelper.Context;
        }

        public async Task<CaseType> CreateAsync(CaseType input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(CaseType input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<CaseType> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<CaseType> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    .Include(x => x.Product);
        }

        public IQueryable<CaseType> GetAllOrdered ()
        {
            return this.GetAllQuery().OrderBy(o => o.Code);
        }

        public async Task<CaseType> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<CaseType> GetByCode(string code)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Code == code)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<CaseType>> GetFiltered(CaseTypeSearchModel filters)
        {
            var res = new PagedResultDto<CaseType>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public CaseType Import(CaseType input)
        {
            using (_context)
            {
                _context.Database.OpenConnection();
                try
                {
                    _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.CaseTypes ON");
                    _context.Add(input);
                    _context.SaveChanges();
                    _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.CaseTypes OFF");
                }
                finally
                {
                    _context.Database.CloseConnection();
                }
            }

            return input;
        }

        public async Task<CaseType> UpdateAsync(CaseType input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
