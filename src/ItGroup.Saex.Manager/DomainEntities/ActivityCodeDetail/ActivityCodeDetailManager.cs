﻿using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using System;
using System.Linq;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class ActivityCodeDetailManager : IActivityCodeDetailManager
    {

        private readonly IRepository<ActivityCodeDetail, Guid> _ActivityCodeDetailrepository;
        public ActivityCodeDetailManager() { }
        public ActivityCodeDetailManager(IRepository<ActivityCodeDetail, Guid> ActivityCodeDetailrepository)
        {
            _ActivityCodeDetailrepository = ActivityCodeDetailrepository;
        }

        public IQueryable<ActivityCodeDetail> GetAllActivityCodeDetails()
        {
            return _ActivityCodeDetailrepository
                   .GetAll();
        }
    }
}