﻿using Abp.Domain.Services;
using ItGroup.Saex.DomainEntities;
using System.Linq;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public interface IActivityCodeDetailManager : IDomainService
    {
        IQueryable<ActivityCodeDetail> GetAllActivityCodeDetails();
    }
}