﻿using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using System;
using System.Linq;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class EstadoRiesgoManager : IEstadoRiesgoManager
    {

        private readonly IRepository<EstadoRiesgo, Guid> _EstadoRiesgorepository;
        public EstadoRiesgoManager() { }
        public EstadoRiesgoManager(IRepository<EstadoRiesgo, Guid> EstadoRiesgorepository)
        {
            _EstadoRiesgorepository = EstadoRiesgorepository;
        }

        public IQueryable<EstadoRiesgo> GetAllEstadoRiesgos()
        {
            var results = _EstadoRiesgorepository.GetAll();
            return results;
        }
    }
}