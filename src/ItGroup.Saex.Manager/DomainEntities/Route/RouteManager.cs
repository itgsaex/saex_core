﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class RouteManager : IRouteManager
    {
        private readonly IRepository<Route, Guid> _repository;

        public RouteManager()
        {
        }

        public RouteManager(
            IRepository<Route, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<Route> CreateAsync(Route input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Route input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Route> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<Route> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Include(x => x.PostalCodes).ThenInclude(y => y.PostalCode)
                    .Include(x => x.Products).ThenInclude(y => y.Product)
                    .Include(x => x.Regions).ThenInclude(y => y.Region)
                    .Where(x => !x.IsDeleted)
                    .OrderBy(x => x.Name);
        }

        public async Task<Route> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Route>> GetFiltered(RouteSearchModel filters)
        {
            var res = new PagedResultDto<Route>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Name.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Name);
            else
                query = query.OrderByDescending(x => x.Name);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<Route> UpdateAsync(Route input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}