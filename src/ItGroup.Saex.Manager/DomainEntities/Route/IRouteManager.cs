﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IRouteManager : IDomainService
    {
        Task<Route> CreateAsync(Route input);

        Task<Route> UpdateAsync(Route input);

        Task DeleteAsync(Route input);

        Task<Route> GetAsync(Guid id);

        IQueryable<Route> GetAll();

        Task<PagedResultDto<Route>> GetFiltered(RouteSearchModel filters);
    }
}