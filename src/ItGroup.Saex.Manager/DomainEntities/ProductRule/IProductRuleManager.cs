﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IProductRuleManager : IDomainService
    {
        Task<ProductRule> CreateAsync(ProductRule input);

        Task<ProductRule> UpdateAsync(ProductRule input);

        Task DeleteAsync(ProductRule input);

        Task<ProductRule> GetAsync(Guid id);

        IQueryable<ProductRule> GetAll();

        Task<PagedResultDto<ProductRule>> GetFiltered(ProductRuleSearchModel filters);
    }
}