﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ProductRuleManager : IProductRuleManager
    {
        private readonly IRepository<ProductRule, Guid> _repository;

        public ProductRuleManager()
        {
        }

        public ProductRuleManager(
            IRepository<ProductRule, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<ProductRule> CreateAsync(ProductRule input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(ProductRule input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<ProductRule> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<ProductRule> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Include(x => x.Product)
                    .Where(x => !x.IsDeleted);
        }

        public async Task<ProductRule> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<ProductRule>> GetFiltered(ProductRuleSearchModel filters)
        {
            var res = new PagedResultDto<ProductRule>();
            var query = this.GetAllQuery();

            //if (!string.IsNullOrWhiteSpace(filters.SearchText))
            //{
            //    query = query.Where
            //           (x =>
            //            x.Product.ToLower().Contains(filters.SearchText.ToLower())
            //           );
            //}

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<ProductRule> UpdateAsync(ProductRule input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}