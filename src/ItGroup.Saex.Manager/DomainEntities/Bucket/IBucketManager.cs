﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IBucketManager : IDomainService
    {
        Task<Bucket> CreateAsync(Bucket input);

        Task<Bucket> UpdateAsync(Bucket input);

        Task DeleteAsync(Bucket input);

        Task<Bucket> GetAsync(Guid id);

        IQueryable<Bucket> GetAll();

        Task<PagedResultDto<Bucket>> GetFiltered(BucketSearchModel filters);
    }
}