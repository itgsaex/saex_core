﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class BucketManager : IBucketManager
    {
        private readonly IRepository<Bucket, Guid> _repository;
        public BucketManager() { }
        public BucketManager(
            IRepository<Bucket, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<Bucket> CreateAsync(Bucket input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Bucket input)
        {
            await _repository.DeleteAsync(input);
        }

        public async Task<Bucket> UpdateAsync(Bucket input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<Bucket> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public IQueryable<Bucket> GetAll()
        {
            return this.GetAllQuery();
        }

        public async Task<PagedResultDto<Bucket>> GetFiltered(BucketSearchModel filters)
        {
            var res = new PagedResultDto<Bucket>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Product.Code.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;

        }

        public IQueryable<Bucket> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Include(x => x.Product)
                    .Where(x => !x.IsDeleted);
        }
    }
}