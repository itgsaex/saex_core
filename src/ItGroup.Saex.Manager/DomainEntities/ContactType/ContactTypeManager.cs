﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ContactTypeManager : IContactTypeManager
    {
        private readonly IRepository<ContactType, Guid> _repository;
        public ContactTypeManager() { }
        public ContactTypeManager(
            IRepository<ContactType, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<ContactType> CreateAsync(ContactType input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(ContactType input)
        {
            await _repository.DeleteAsync(input);
        }

        public async Task<ContactType> UpdateAsync(ContactType input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<ContactType> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public IQueryable<ContactType> GetAll()
        {
            return this.GetAllQuery();
        }

        public async Task<PagedResultDto<ContactType>> GetFiltered(ContactTypeSearchModel filters)
        {
            var res = new PagedResultDto<ContactType>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;

        }

        public IQueryable<ContactType> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }
    }
}