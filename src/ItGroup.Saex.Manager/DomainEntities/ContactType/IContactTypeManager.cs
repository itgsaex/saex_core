﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IContactTypeManager : IDomainService
    {
        Task<ContactType> CreateAsync(ContactType input);

        Task<ContactType> UpdateAsync(ContactType input);

        Task DeleteAsync(ContactType input);

        Task<ContactType> GetAsync(Guid id);

        IQueryable<ContactType> GetAll();

        Task<PagedResultDto<ContactType>> GetFiltered(ContactTypeSearchModel filters);
    }
}