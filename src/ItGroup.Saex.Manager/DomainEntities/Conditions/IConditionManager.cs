﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IConditionManager : IDomainService
    {
        Task<Condition> CreateAsync(Condition input);

        Task<Condition> UpdateAsync(Condition input);

        Task DeleteAsync(Condition input);

        Task<Condition> GetAsync(Guid id);

        IQueryable<Condition> GetAll();

        Task<PagedResultDto<Condition>> GetFiltered(ConditionSearchModel filters);
    }
}