﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ConditionManager : IConditionManager
    {
        private readonly IRepository<Condition, Guid> _repository;

        public ConditionManager()
        {
        }

        public ConditionManager(
            IRepository<Condition, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<Condition> CreateAsync(Condition input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Condition input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Condition> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<Condition> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Include(x => x.Product)
                    .Where(x => !x.IsDeleted);
        }

        public async Task<Condition> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Condition>> GetFiltered(ConditionSearchModel filters)
        {
            var res = new PagedResultDto<Condition>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<Condition> UpdateAsync(Condition input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}