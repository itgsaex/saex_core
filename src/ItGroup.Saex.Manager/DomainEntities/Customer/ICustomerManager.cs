﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICustomerManager : IDomainService
    {
        Task<Customer> CreateAsync(Customer input);

        Task DeleteAsync(Customer input);

        IQueryable<Customer> GetAll();

        Task<Customer> GetAsync(Guid id);

        Task<PagedResultDto<Customer>> GetFiltered(CustomerSearchModel filters);

        Task<Customer> UpdateAsync(Customer input);
    }
}