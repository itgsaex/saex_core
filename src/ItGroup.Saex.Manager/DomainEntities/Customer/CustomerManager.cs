﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CustomerManager : ICustomerManager
    {
        private readonly IRepository<Customer, Guid> _repository;

        public CustomerManager()
        {
        }

        public CustomerManager(
            IRepository<Customer, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<Customer> CreateAsync(Customer input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Customer input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Customer> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<Customer> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<Customer> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Customer>> GetFiltered(CustomerSearchModel filters)
        {
            var res = new PagedResultDto<Customer>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.FullName.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.CustomerId.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.PhoneNumber.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.EmailAddress.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.IdentificationNumber.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Ssn.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.FullName);
            else
                query = query.OrderByDescending(x => x.FullName);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<Customer> UpdateAsync(Customer input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
