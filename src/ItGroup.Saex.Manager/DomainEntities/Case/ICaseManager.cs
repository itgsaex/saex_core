﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseManager : IDomainService
    {
        Task<Case> CreateAsync(Case input);

        Task<Case> UpdateAsync(Case input);

        Task DeleteAsync(Case input);

        Task<Case> GetAsync(Guid id);

        Task<Case> GetByCaseIDAsync(string caseID);

        IQueryable<Case> GetAll();

        Task<PagedResultDto<Case>> GetFiltered(CaseSearchModel filters);

        Task<List<vwCases>> GetCases(string agentID);
    }
}