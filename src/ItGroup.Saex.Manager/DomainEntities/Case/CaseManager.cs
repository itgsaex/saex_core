﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore.Repositories;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseManager : ICaseManager
    {
        private readonly IRepository<Case, Guid> _repository;

        public CaseManager()
        {
        }

        public CaseManager(
            IRepository<Case, Guid> repository)
        {
            _repository = repository;   
        }

        public async Task<Case> CreateAsync(Case input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Case input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Case> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<Case> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    .Include(x => x.DelinquencyState)
                        .ThenInclude(y => y.DelinquencyState)
                    .Include(x => x.Customers)
                        .ThenInclude(y => y.Addresses)
                            .ThenInclude(z => z.PostalCode)
                    .Include(x => x.Product)
                        .ThenInclude(x => x.Product)
                    .Include(x => x.SystemState)
                        .ThenInclude(y => y.SystemState)
                    .Include(x => x.SystemSubState)
                        .ThenInclude(y => y.SystemSubState)
                    .Include(x => x.CaseActivities)
                        .ThenInclude(y => y.Agent.User)
                    .Include(x => x.CaseActivities)
                        .ThenInclude(y => y.ContactType)
                    .Include(x => x.CaseActivities)
                        .ThenInclude(y => y.VisitPlace)
                    .Include(x => x.CaseEvents)
                    .Include(x => x.Agents)
                        .ThenInclude(x => x.Agent)
                        .ThenInclude(x => x.User)
                    .Include(x => x.CaseType)
                    .Include(x => x.Tactic);
        }

        public async Task<Case> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<Case> GetByCaseIDAsync(string caseID)
        {
            return await this.GetAllQuery()
                            .Where(x => x.CaseID == caseID)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Case>> GetFiltered(CaseSearchModel filters)
        {
            var res = new PagedResultDto<Case>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.AccountNumber.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Customers.Any(y => y.FullName.ToLower() == filters.SearchText.ToLower())
                       );
            }

            if (filters.ImportType == ImportType.Manual)
            {
                query = query.Where(x => x.ImportType == ImportType.Manual);
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<Case> UpdateAsync(Case input)
        {
            return await _repository.UpdateAsync(input);
        }

        public Task<List<vwCases>> GetCases(string agentID)
        {
            var dbContext = _repository.GetDbContext() as SaexDbContext;
            return dbContext.vwCases.Where(v => v.Username == agentID).ToListAsync();
        }
    }
}
