﻿using Abp.Application.Services.Dto;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldSearchModel : PagedResultRequestDto
    {
        public string SearchText { get; set; }

        public SortOrder SortOrder { get; set; }
    }
}
