﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IDemographicFieldManager : IDomainService
    {
        Task<DemographicField> CreateAsync(DemographicField input);

        Task<DemographicField> UpdateAsync(DemographicField input);

        Task DeleteAsync(DemographicField input);

        Task<DemographicField> GetAsync(Guid id);

        IQueryable<DemographicField> GetAll();

        Task<PagedResultDto<DemographicField>> GetFiltered(DemographicFieldSearchModel filters);
    }
}
