﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldManager : IDemographicFieldManager
    {
        private readonly IRepository<DemographicField, Guid> _repository;

        public DemographicFieldManager() { }

        public DemographicFieldManager(IRepository<DemographicField, Guid> repository)
        {
            _repository = repository;
        }

        public Task<DemographicField> CreateAsync(DemographicField input) =>
            _repository.InsertAsync(input);

        public Task<DemographicField> UpdateAsync(DemographicField input) =>
            _repository.UpdateAsync(input);

        public Task DeleteAsync(DemographicField input) =>
            _repository.DeleteAsync(input);

        public IQueryable<DemographicField> GetAll() =>
            GetAllQuery();

        public IQueryable<DemographicField> GetAllQuery() =>
            _repository
                .GetAll()
                .Where(x => !x.IsDeleted);

        public Task<DemographicField> GetAsync(Guid id) =>
            GetAllQuery()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

        public async Task<PagedResultDto<DemographicField>> GetFiltered(DemographicFieldSearchModel filters)
        {
            var query = GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
                query = query.Where(x =>
                    x.Name.ToLower().Contains(filters.SearchText.ToLower()) ||
                    x.Label.ToLower().Contains(filters.SearchText.ToLower())
                );

            Expression<Func<DemographicField, int>> sortField = x => x.SortOrder;

            switch (filters.SortOrder)
            {
                case SortOrder.Ascending:
                    query = query.OrderBy(sortField);
                    break;
                case SortOrder.Descending:
                    query = query.OrderByDescending(sortField);
                    break;
            }

            var results = await query.ToListAsync();

            return new PagedResultDto<DemographicField>
            {
                TotalCount = results.Count,
                Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList()
            };
        }
    }
}
