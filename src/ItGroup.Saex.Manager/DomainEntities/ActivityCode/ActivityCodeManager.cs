﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ActivityCodeManager : IActivityCodeManager
    {
        private readonly IRepository<ActivityCode, Guid> _repository;

        public ActivityCodeManager()
        {
        }

        public ActivityCodeManager(
            IRepository<ActivityCode, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<ActivityCode> CreateAsync(ActivityCode input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(ActivityCode input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<ActivityCode> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<ActivityCode> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Include(x => x.ActivityCodeDetails)
                    .Include(x => x.Questions).ThenInclude(y => y.Parameters)
                    .Where(x => !x.IsDeleted);
        }

        public async Task<ActivityCode> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<ActivityCode>> GetFiltered(ActivityCodeSearchModel filters)
        {
            var res = new PagedResultDto<ActivityCode>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<ActivityCode> UpdateAsync(ActivityCode input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
