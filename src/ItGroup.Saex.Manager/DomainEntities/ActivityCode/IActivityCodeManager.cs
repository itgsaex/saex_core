﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IActivityCodeManager : IDomainService
    {
        Task<ActivityCode> CreateAsync(ActivityCode input);

        Task<ActivityCode> UpdateAsync(ActivityCode input);

        Task DeleteAsync(ActivityCode input);

        Task<ActivityCode> GetAsync(Guid id);

        IQueryable<ActivityCode> GetAll();

        Task<PagedResultDto<ActivityCode>> GetFiltered(ActivityCodeSearchModel filters);
    }
}