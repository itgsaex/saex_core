﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AddressManager : IAddressManager
    {
        private readonly IRepository<Address, Guid> _repository;

        public AddressManager()
        {
        }

        public AddressManager(
            IRepository<Address, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<Address> CreateAsync(Address input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Address input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Address> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<Address> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<Address> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Address>> GetFiltered(AddressSearchModel filters)
        {
            var res = new PagedResultDto<Address>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.AddressLine1.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.AddressLine2.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.City.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Country.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Name.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Name);
            else
                query = query.OrderByDescending(x => x.Name);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<Address> UpdateAsync(Address input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}