﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAddressManager : IDomainService
    {
        Task<Address> CreateAsync(Address input);

        Task<Address> UpdateAsync(Address input);

        Task DeleteAsync(Address input);

        Task<Address> GetAsync(Guid id);

        IQueryable<Address> GetAll();

        Task<PagedResultDto<Address>> GetFiltered(AddressSearchModel filters);
    }
}