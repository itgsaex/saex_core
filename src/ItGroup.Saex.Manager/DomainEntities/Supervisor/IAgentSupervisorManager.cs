﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using ItGroup.Saex.Authorization.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAssignInternalAgentsManager : IDomainService
    {
        Task<List<User>> GetSupervisors();
        Task<User> GetUserRecord(long userId);
        Task<List<User>> GetInternalAgents();
        Task<List<User>> GetExternalAgents();
        Task<List<User>> GetSupervisorInternalAgents(long supervisorId);
        Task<List<User>> GetInternalAgentExternalAgents(long internalAgentId);
        Task AssignInternalAgents(long supervisorId, List<User> internalAgents);
        Task AssignExternalAgents(long internalAgentId, List<User> externalAgents);
        Task DeleteSupervisorInternalAgents(long supervisorId);
        Task DeleteInternalAgentExternalAgents(long internalAgentId);
        Task DeleteSupervisorInternalAgent(long supervisorId, long internalAgentId);
        Task DeleteInternalAgentExternalAgent(long internalAgentId, long externalAgentId);
    }
}