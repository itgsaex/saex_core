﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AssignInternalAgentsManager : IAssignInternalAgentsManager
    {
        private readonly IRepository<SupervisorInternalAgent, Guid> _supervisorInternalAgentrepository;
        private readonly IRepository<InternalAgentExternalAgent, Guid> _internalAgentExternalAgentRepository;
        private readonly IRepository<User, long> _userRepository;

        public AssignInternalAgentsManager(IRepository<SupervisorInternalAgent, Guid> supervisorInternalAgentrepository,
                                           IRepository<User, long> userRepository,
                                           IRepository<InternalAgentExternalAgent, Guid> internalAgentExternalAgentRepository)
        {
            _supervisorInternalAgentrepository = supervisorInternalAgentrepository;
            _internalAgentExternalAgentRepository = internalAgentExternalAgentRepository;
            _userRepository = userRepository;
        }

        public async Task AssignInternalAgents(long supervisorId, List<User> internalAgents)
        {
            var supervisor = _userRepository.Get(supervisorId);

            foreach (var internalAgent in internalAgents)
            {
                var supervisorInternalAgent = new SupervisorInternalAgent();
                supervisorInternalAgent.SupervisorId = supervisorId;
                supervisorInternalAgent.Supervisor = supervisor;
                supervisorInternalAgent.InternalAgentId = internalAgent.Id;
                supervisorInternalAgent.InternaleAgent = internalAgent;
                await _supervisorInternalAgentrepository.InsertAsync(supervisorInternalAgent);
            }
        }

        public async Task<List<User>> GetInternalAgents()
        {
            var results = await _userRepository.GetAll().Include(u => u.Agent).Include(u => u.SupervisorInternalAgents).Where(u => u.Agent.Type == AgentType.Internal).ToListAsync();
            return results;
        }

        public async Task DeleteSupervisorInternalAgents(long supervisorId)
        {
            var supervisorInternalAgents = await _supervisorInternalAgentrepository.GetAll().Where(x => x.SupervisorId == supervisorId).ToListAsync();
            
            foreach (var supervisorInternalAgent in supervisorInternalAgents)
            {
                await _supervisorInternalAgentrepository.DeleteAsync(supervisorInternalAgent);
            }
        }

        public async Task<List<User>> GetSupervisorInternalAgents(long supervisorId)
        {
            List<User> results = new List<User>();

            var rows = await _supervisorInternalAgentrepository.GetAll().Include(x => x.InternaleAgent).Where(x => x.SupervisorId == supervisorId).ToListAsync();

            foreach (var row in rows)
            {
                if (row.InternaleAgent != null)
                {
                    results.Add(row.InternaleAgent);
                }
            }

            return results;
        }

        public async Task<List<User>> GetSupervisors()
        {
            var results = await _userRepository.GetAll().Include(u => u.Agent).Include(u => u.SupervisorInternalAgents).Where(u => u.Agent.Type == AgentType.Supervisor).ToListAsync();
            return results;
        }

        public async Task<List<User>> GetExternalAgents()
        {
            var results = await _userRepository.GetAll().Include(u => u.Agent).Include(u => u.ExternalAgentInternalAgents).Where(u => u.Agent.Type == AgentType.External).ToListAsync();
            return results;
        }

        public async Task<List<User>> GetInternalAgentExternalAgents(long internalAgentId)
        {
            List<User> results = new List<User>();

            var rows = await _internalAgentExternalAgentRepository.GetAll().Include(x => x.ExternalAgent).Where(x => x.InternalAgentId == internalAgentId).ToListAsync();

            foreach (var row in rows)
            {
                if (row.ExternalAgent != null)
                {
                    results.Add(row.ExternalAgent);
                }
            }

            return results;
        }

        public async Task AssignExternalAgents(long internalAgentId, List<User> externalAgents)
        {
            var internalAgent = _userRepository.Get(internalAgentId);

            foreach (var externalAgent in externalAgents)
            {
                var internalAgentExternalAgent = new InternalAgentExternalAgent();
                internalAgentExternalAgent.InternalAgentId = internalAgentId;
                internalAgentExternalAgent.InternaleAgent = internalAgent;
                internalAgentExternalAgent.ExternalAgentId = externalAgent.Id;
                internalAgentExternalAgent.ExternalAgent = externalAgent;
                await _internalAgentExternalAgentRepository.InsertAsync(internalAgentExternalAgent);
            }
        }

        public async Task DeleteInternalAgentExternalAgents(long internalAgentId)
        {
            var internalAgentExternalAgents = await _internalAgentExternalAgentRepository.GetAll().Where(x => x.InternalAgentId == internalAgentId).ToListAsync();

            foreach (var internalAgentExternalAgent in internalAgentExternalAgents)
            {
                await _internalAgentExternalAgentRepository.DeleteAsync(internalAgentExternalAgent);
            }
        }

        public async Task DeleteSupervisorInternalAgent(long supervisorId, long internalAgentId)
        {
            var supervisorInternalAgent = await _supervisorInternalAgentrepository.GetAll().Where(x => x.SupervisorId == supervisorId && x.InternalAgentId == internalAgentId).FirstOrDefaultAsync();
            if (supervisorInternalAgent != null)
            {
                await _supervisorInternalAgentrepository.DeleteAsync(supervisorInternalAgent);
            }
        }

        public async Task DeleteInternalAgentExternalAgent(long internalAgentId, long externalAgentId)
        {
            var internalAgentExternalAgent = await _internalAgentExternalAgentRepository.GetAll().Where(x => x.InternalAgentId == internalAgentId && x.ExternalAgentId == externalAgentId).FirstOrDefaultAsync();
            if (internalAgentExternalAgent != null)
            {
                await _internalAgentExternalAgentRepository.DeleteAsync(internalAgentExternalAgent);
            }
        }

        public async Task<User> GetUserRecord(long userId)
        {
            var result = await _userRepository.GetAll().Include(u => u.Agent).Where(u => u.Id == userId).FirstOrDefaultAsync();
            return result;
        }
    }
}