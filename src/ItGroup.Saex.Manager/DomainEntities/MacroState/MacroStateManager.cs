﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class MacroStateManager : IMacroStateManager
    {
        private readonly IRepository<MacroState, Guid> _repository;

        public MacroStateManager()
        {
        }

        public MacroStateManager(
            IRepository<MacroState, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<MacroState> CreateAsync(MacroState input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(MacroState input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<MacroState> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<MacroState> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<MacroState> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<MacroState> GetByCode(string code)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Code == code)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<MacroState>> GetFiltered(MacroStateSearchModel filters)
        {
            var res = new PagedResultDto<MacroState>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<MacroState> UpdateAsync(MacroState input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
