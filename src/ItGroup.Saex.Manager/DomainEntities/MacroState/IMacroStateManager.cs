﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IMacroStateManager : IDomainService
    {
        Task<MacroState> CreateAsync(MacroState input);

        Task<MacroState> UpdateAsync(MacroState input);

        Task DeleteAsync(MacroState input);

        Task<MacroState> GetAsync(Guid id);

        IQueryable<MacroState> GetAll();

        Task<PagedResultDto<MacroState>> GetFiltered(MacroStateSearchModel filters);
    }
}