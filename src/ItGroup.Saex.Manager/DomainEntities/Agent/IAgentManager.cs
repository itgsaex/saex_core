﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAgentManager : IDomainService
    {
        Task<Agent> CreateAsync(Agent input);

        Task<Agent> UpdateAsync(Agent input);

        Task DeleteAsync(Agent input);

        Task<Agent> GetAsync(Guid id);

        IQueryable<Agent> GetAll();

        Task<PagedResultDto<Agent>> GetFiltered(AgentSearchModel filters);

        Task<List<vwAgent>> GetAgentConfig(string agentId);
    }
}