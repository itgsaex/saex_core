﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.EntityFrameworkCore.Repositories;
using static ItGroup.Saex.Enumerations.Enumerations;
using ItGroup.Saex.EntityFrameworkCore;
using System.Collections.Generic;
using ItGroup.Saex.Authorization.Users;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentManager : IAgentManager
    {
        private readonly IRepository<Agent, Guid> _repository;
        private readonly IRepository<User, long> _userRepository;

        public AgentManager()
        {
        }

        public AgentManager(
            IRepository<Agent, Guid> repository, IRepository<User, long> userRepository)
        {
            _repository = repository;
            _userRepository = userRepository;
        }

        public async Task<Agent> CreateAsync(Agent input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Agent input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Agent> GetAll()
        {
            return this.GetAllQuery();
        }

        public Task<List<vwAgent>> GetAgentConfig(string agentId)
        {
            var dbContext = _repository.GetDbContext() as SaexDbContext;
            var results = dbContext.vwAgent.Where(v => v.AgentID.Equals(agentId)).ToListAsync();
            return results;
        }

        public IQueryable<Agent> GetAllQuery()
        {
            /*var users = _userRepository.GetAll().ToList();
            List<Agent> agents = new List<Agent>();

            foreach (var user in users)
            {
                var agent = _repository.GetAll().Where(a => a.UserId == user.Id).FirstOrDefault();
                
                if (agent != null)
                {
                    agent.User = user;
                    agents.Add(agent);
                }
            }

            return agents.AsQueryable();*/

            return _repository
                     .GetAll()
                     .Include(x => x.User)
                     .Include(x => x.WorkingHour)
                     .Include(x => x.Regions).ThenInclude(y => y.Region)
                     .Include(x => x.Lists).ThenInclude(y => y.List)
                     .Where(x => !x.IsDeleted && x.User.IsActive);
        }

        public IQueryable<Agent> GetAllWithDeletedQuery()
        {
            List<Agent> agents = new List<Agent>();
            var data = _userRepository
                .GetAll()
                .IgnoreQueryFilters()
                .Include(x => x.Agent)
                .Where(x => !x.IsDeleted && x.IsActive); ;
            foreach (var item in data)
            {
                if (item.Agent != null)
                {
                    if (!agents.Contains(item.Agent))
                    {
                        item.Agent.User = item;
                        agents.Add(item.Agent);
                    }
                }
            }
            return agents.AsQueryable();
        }

        public async Task<Agent> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Agent>> GetFiltered(AgentSearchModel filters)
        {
            var res = new PagedResultDto<Agent>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.User.Name.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.User.Surname.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.User.UserName.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.User.EmailAddress.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.User.Name);
            else
                query = query.OrderByDescending(x => x.User.Name);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<Agent> UpdateAsync(Agent input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
