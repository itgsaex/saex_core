﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemStateManager : ISystemStateManager
    {
        private readonly IRepository<SystemState, Guid> _repository;

        public SystemStateManager()
        {
        }

        public SystemStateManager(
            IRepository<SystemState, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<SystemState> CreateAsync(SystemState input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(SystemState input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<SystemState> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<SystemState> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    .Include(x => x.MacroState)
                    .Include(x => x.SubState);
        }

        public async Task<SystemState> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<SystemState> GetByCode(string code)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Code == code)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<SystemState>> GetFiltered(SystemStateSearchModel filters)
        {
            var res = new PagedResultDto<SystemState>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<SystemState> UpdateAsync(SystemState input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
