﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISystemStateManager : IDomainService
    {
        Task<SystemState> CreateAsync(SystemState input);

        Task<SystemState> UpdateAsync(SystemState input);

        Task DeleteAsync(SystemState input);

        Task<SystemState> GetAsync(Guid id);

        IQueryable<SystemState> GetAll();

        Task<PagedResultDto<SystemState>> GetFiltered(SystemStateSearchModel filters);
    }
}