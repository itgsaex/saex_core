﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AdjusterStateManager : IAdjusterStateManager
    {
        private readonly IRepository<AdjusterState, Guid> _repository;

        public AdjusterStateManager()
        {
        }

        public AdjusterStateManager(
            IRepository<AdjusterState, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<AdjusterState> CreateAsync(AdjusterState input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(AdjusterState input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<AdjusterState> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<AdjusterState> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    .Include(x => x.MacroState);
        }

        public async Task<AdjusterState> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<AdjusterState>> GetFiltered(AdjusterStateSearchModel filters)
        {
            var res = new PagedResultDto<AdjusterState>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<AdjusterState> UpdateAsync(AdjusterState input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}