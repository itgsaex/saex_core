﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAdjusterStateManager : IDomainService
    {
        Task<AdjusterState> CreateAsync(AdjusterState input);

        Task<AdjusterState> UpdateAsync(AdjusterState input);

        Task DeleteAsync(AdjusterState input);

        Task<AdjusterState> GetAsync(Guid id);

        IQueryable<AdjusterState> GetAll();

        Task<PagedResultDto<AdjusterState>> GetFiltered(AdjusterStateSearchModel filters);
    }
}