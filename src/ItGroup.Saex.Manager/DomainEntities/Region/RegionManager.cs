﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class RegionManager : IRegionManager
    {
        private readonly IRepository<AgentRegion, Guid> _agentRegionrepository;

        private readonly IRepository<Region, Guid> _repository;

        public RegionManager()
        {
        }

        public RegionManager(
            IRepository<Region, Guid> repository, IRepository<AgentRegion, Guid> agentRegionRepository)
        {
            _repository = repository;
            _agentRegionrepository = agentRegionRepository;
        }

        public async Task<Region> CreateAsync(Region input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task CreateAssignedAsync(AgentRegion input)
        {
            await _agentRegionrepository.InsertAsync(input);
        }

        public async Task DeleteAssignedAsync(AgentRegion input)
        {
            await _agentRegionrepository.DeleteAsync(input);
        }

        public async Task DeleteAsync(Region input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Region> GetAll()
        {
            return this.GetAllQuery();
        }

        public async Task<List<AgentRegion>> GetAllAssigned(AssignRegionSearchModel filters)
        {
            var query = _agentRegionrepository.GetAll()
                            .Include(x => x.Agent)
                            .Include(x => x.Agent).ThenInclude(y => y.User)
                            .Include(x => x.Region)
                            .Where(x => !x.Agent.IsDeleted);

            if (filters.AgentId != null)
            {
                query = query.Where(x => x.AgentId == filters.AgentId);
            }

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Agent.User.Name.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Agent.User.Surname.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Agent.User.UserName.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Agent.User.EmailAddress.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Id);
            else
                query = query.OrderByDescending(x => x.Id);

            return await query.ToListAsync();
        }

        public IQueryable<Region> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Include(x => x.Routes).ThenInclude(y => y.Route)
                    .Include(x => x.Agents).ThenInclude(y => y.Agent.User)
                    .Where(x => !x.IsDeleted)
                    .OrderBy(x => x.Name);
        }

        public async Task<PagedResultDto<AgentRegion>> GetAssigned(AssignRegionSearchModel filters)
        {
            var output = new PagedResultDto<AgentRegion>();
            var results = await this.GetAllAssigned(filters);

            output.TotalCount = results.Count();
            output.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return output;
        }

        public async Task<List<AgentRegion>> GetAssignedByAgentId(Guid id)
        {
            var filter = new AssignRegionSearchModel();
            filter.AgentId = id;

            return await this.GetAllAssigned(filter);
        }

        public async Task<Region> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Region>> GetFiltered(RegionSearchModel filters)
        {
            var res = new PagedResultDto<Region>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Name.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Name);
            else
                query = query.OrderByDescending(x => x.Name);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<Region> UpdateAsync(Region input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}