﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IRegionManager : IDomainService
    {
        Task<Region> CreateAsync(Region input);

        Task<Region> UpdateAsync(Region input);

        Task DeleteAsync(Region input);

        Task<Region> GetAsync(Guid id);

        IQueryable<Region> GetAll();

        Task<PagedResultDto<Region>> GetFiltered(RegionSearchModel filters);
    }
}