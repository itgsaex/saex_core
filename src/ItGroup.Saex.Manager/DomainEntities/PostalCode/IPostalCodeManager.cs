﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IPostalCodeManager : IDomainService
    {
        Task<PostalCode> CreateAsync(PostalCode input);

        Task<PostalCode> UpdateAsync(PostalCode input);

        Task DeleteAsync(PostalCode input);

        Task<PostalCode> GetAsync(Guid id);

        IQueryable<PostalCode> GetAll();

        Task<PagedResultDto<PostalCode>> GetFiltered(PostalCodeSearchModel filters);
    }
}