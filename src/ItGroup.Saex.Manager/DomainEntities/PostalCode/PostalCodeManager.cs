﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class PostalCodeManager : IPostalCodeManager
    {
        private readonly IRepository<PostalCode, Guid> _repository;

        public PostalCodeManager()
        {
        }

        public PostalCodeManager(
            IRepository<PostalCode, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<PostalCode> CreateAsync(PostalCode input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(PostalCode input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<PostalCode> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<PostalCode> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    .OrderBy(x => x.Code);
        }

        public async Task<PostalCode> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public PostalCode GetByCode(string code)
        {
            return this.GetAllQuery().Where(x => x.Code == code).FirstOrDefault();
        }

        public async Task<PagedResultDto<PostalCode>> GetFiltered(PostalCodeSearchModel filters)
        {
            var res = new PagedResultDto<PostalCode>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Area.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<PostalCode> UpdateAsync(PostalCode input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
