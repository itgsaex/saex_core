﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ItGroup.Saex.Manager.DomainEntities.ProductSystem
{
    public class ProductSystemManager : IProductSystemManager
    {
        private readonly IRepository<ItGroup.Saex.DomainEntities.ProductSystem.ProductSystem, Guid> _ProductSystemrepository;
        public ProductSystemManager() { }
        public ProductSystemManager(IRepository<ItGroup.Saex.DomainEntities.ProductSystem.ProductSystem, Guid> ProductSystemrepository)
        {
            _ProductSystemrepository = ProductSystemrepository;
        }

        public IQueryable<ItGroup.Saex.DomainEntities.ProductSystem.ProductSystem> GetAllProductSystems()
        {
            var results = _ProductSystemrepository.GetAll();
            return results;
        }
    }
}
