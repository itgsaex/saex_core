﻿using Abp.Domain.Services;
using System;
using ItGroup.Saex.DomainEntities;
using System.Linq;

namespace ItGroup.Saex.Manager.DomainEntities.ProductSystem
{
    public interface IProductSystemManager : IDomainService
    {
        IQueryable<ItGroup.Saex.DomainEntities.ProductSystem.ProductSystem> GetAllProductSystems();
    }
}
