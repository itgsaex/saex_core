﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class SortFieldsCaseManager : ISortFieldsCaseManager
    {

        private readonly IRepository<SortFieldsCase, Guid> _SortFieldsCaseRepository;
        public SortFieldsCaseManager() { }
        public SortFieldsCaseManager(IRepository<SortFieldsCase, Guid> SortFieldsCaseRepository)
        {
            _SortFieldsCaseRepository = SortFieldsCaseRepository;
        }

        public IQueryable<SortFieldsCase> GetAll()
        {
            return _SortFieldsCaseRepository.GetAll();
        }
    }
}