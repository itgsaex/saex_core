﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTacticManager : ICaseTacticManager
    {
        private readonly IRepository<CaseTactic, Guid> _repository;
        public CaseTacticManager() { }
        public CaseTacticManager(
            IRepository<CaseTactic, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<CaseTactic> CreateAsync(CaseTactic input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(CaseTactic input)
        {
            await _repository.DeleteAsync(input);
        }

        public async Task<CaseTactic> UpdateAsync(CaseTactic input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<CaseTactic> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public IQueryable<CaseTactic> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<CaseTactic> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }
    }
}