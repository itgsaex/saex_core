﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseTacticManager : IDomainService
    {
        Task<CaseTactic> CreateAsync(CaseTactic input);

        Task<CaseTactic> UpdateAsync(CaseTactic input);

        Task DeleteAsync(CaseTactic input);

        Task<CaseTactic> GetAsync(Guid id);

        IQueryable<CaseTactic> GetAll();
    }
}