﻿using Abp.Domain.Services;
using ItGroup.Saex.DomainEntities;
using System.Linq;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public interface IKPIManager : IDomainService
    {
        IQueryable<KPI> GetAllKPIs();

        IQueryable<KPI> GetKPIsForAgent(string agentID);
    }
}