﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore.Repositories;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class KPIManager : IKPIManager
    {

        private readonly IRepository<KPI, Guid> _KPIrepository;
        public KPIManager() { }
        public KPIManager(IRepository<KPI, Guid> KPIrepository)
        {
            _KPIrepository = KPIrepository;
        }

        public IQueryable<KPI> GetAllKPIs()
        {
            return _KPIrepository
                   .GetAll();
        }

        public IQueryable<KPI> GetKPIsForAgent(string agentID)
        {
            return _KPIrepository
                   .GetAll()
                   .Where(x => x.AgenteID == agentID);
        }
    }
}