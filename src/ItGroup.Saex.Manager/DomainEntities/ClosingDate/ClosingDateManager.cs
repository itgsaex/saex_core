﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ClosingDateManager : IClosingDateManager
    {
        private readonly IRepository<ClosingDate, Guid> _repository;

        public ClosingDateManager()
        {
        }

        public ClosingDateManager(
            IRepository<ClosingDate, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<ClosingDate> CreateAsync(ClosingDate input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(ClosingDate input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<ClosingDate> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<ClosingDate> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<ClosingDate> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<ClosingDate>> GetFiltered(ClosingDateSearchModel filters)
        {
            var res = new PagedResultDto<ClosingDate>();
            var query = this.GetAllQuery();

            //if (!string.IsNullOrWhiteSpace(filters.SearchText))
            //{
            //    query = query.Where
            //           (x =>
            //            x..ToLower().Contains(filters.SearchText.ToLower()) ||
            //            x.Description.ToLower().Contains(filters.SearchText.ToLower())
            //           );
            //}

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.StartDate);
            else
                query = query.OrderByDescending(x => x.StartDate);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<ClosingDate> UpdateAsync(ClosingDate input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}