﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IClosingDateManager : IDomainService
    {
        Task<ClosingDate> CreateAsync(ClosingDate input);

        Task<ClosingDate> UpdateAsync(ClosingDate input);

        Task DeleteAsync(ClosingDate input);

        Task<ClosingDate> GetAsync(Guid id);

        IQueryable<ClosingDate> GetAll();

        Task<PagedResultDto<ClosingDate>> GetFiltered(ClosingDateSearchModel filters);
    }
}