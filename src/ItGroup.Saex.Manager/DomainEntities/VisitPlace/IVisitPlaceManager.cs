﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IVisitPlaceManager : IDomainService
    {
        Task<VisitPlace> CreateAsync(VisitPlace input);

        Task<VisitPlace> UpdateAsync(VisitPlace input);

        Task DeleteAsync(VisitPlace input);

        Task<VisitPlace> GetAsync(Guid id);

        IQueryable<VisitPlace> GetAll();

        Task<PagedResultDto<VisitPlace>> GetFiltered(VisitPlaceSearchModel filters);
    }
}