﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class VisitPlaceManager : IVisitPlaceManager
    {
        private readonly IRepository<VisitPlace, Guid> _repository;
        public VisitPlaceManager() { }
        public VisitPlaceManager(
            IRepository<VisitPlace, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<VisitPlace> CreateAsync(VisitPlace input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(VisitPlace input)
        {
            await _repository.DeleteAsync(input);
        }

        public async Task<VisitPlace> UpdateAsync(VisitPlace input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<VisitPlace> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public IQueryable<VisitPlace> GetAll()
        {
            return this.GetAllQuery();
        }

        public async Task<PagedResultDto<VisitPlace>> GetFiltered(VisitPlaceSearchModel filters)
        {
            var res = new PagedResultDto<VisitPlace>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;

        }

        public IQueryable<VisitPlace> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }
    }
}