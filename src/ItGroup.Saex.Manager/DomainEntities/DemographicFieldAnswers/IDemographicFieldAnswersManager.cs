﻿using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IDemographicFieldAnswersManager : IDomainService
    {
        Task<DemographicFieldAnswer> CreateAsync(DemographicFieldAnswer input);

        Task<DemographicFieldAnswer> UpdateAsync(DemographicFieldAnswer input);

        Task DeleteAsync(DemographicFieldAnswer input);

        Task<DemographicFieldAnswer> GetAsync(Guid id);

        IQueryable<DemographicFieldAnswer> GetAll();
    }
}
