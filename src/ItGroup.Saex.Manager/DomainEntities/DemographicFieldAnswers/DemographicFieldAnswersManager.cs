﻿using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldAnswersManager : IDemographicFieldAnswersManager
    {
        private readonly IRepository<DemographicFieldAnswer, Guid> _repository;

        public DemographicFieldAnswersManager() { }

        public DemographicFieldAnswersManager(IRepository<DemographicFieldAnswer, Guid> repository)
        {
            _repository = repository;
        }

        public Task<DemographicFieldAnswer> CreateAsync(DemographicFieldAnswer input) =>
            _repository.InsertAsync(input);

        public Task<DemographicFieldAnswer> UpdateAsync(DemographicFieldAnswer input) =>
            _repository.UpdateAsync(input);

        public Task DeleteAsync(DemographicFieldAnswer input) =>
            _repository.DeleteAsync(input);

        public IQueryable<DemographicFieldAnswer> GetAll() =>
            GetAllQuery();

        public IQueryable<DemographicFieldAnswer> GetAllQuery() =>
            _repository
                .GetAll()
                .Where(x => !x.IsDeleted);

        public Task<DemographicFieldAnswer> GetAsync(Guid id) =>
            GetAllQuery()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
    }
}
