﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICasePaymentManager : IDomainService
    {
        Task<CasePayment> CreateAsync(CasePayment input);

        Task<CasePayment> UpdateAsync(CasePayment input);

        Task DeleteAsync(CasePayment input);

        Task<CasePayment> GetAsync(Guid id);

        IQueryable<CasePayment> GetAll();

        Task<PagedResultDto<CasePayment>> GetFiltered(CasePaymentSearchModel filters);
    }
}