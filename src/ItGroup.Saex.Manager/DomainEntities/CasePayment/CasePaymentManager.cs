﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CasePaymentManager : ICasePaymentManager
    {
        private readonly IRepository<CasePayment, Guid> _repository;
        public CasePaymentManager() { }
        public CasePaymentManager(
            IRepository<CasePayment, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<CasePayment> CreateAsync(CasePayment input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(CasePayment input)
        {
            await _repository.DeleteAsync(input);
        }

        public async Task<CasePayment> UpdateAsync(CasePayment input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<CasePayment> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public IQueryable<CasePayment> GetAll()
        {
            return this.GetAllQuery();
        }

        public async Task<PagedResultDto<CasePayment>> GetFiltered(CasePaymentSearchModel filters)
        {
            var res = new PagedResultDto<CasePayment>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                //query = query.Where
                //       (x =>
                //        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                //        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                //       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.PaymentDate);
            else
                query = query.OrderByDescending(x => x.PaymentDate);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;

        }

        public IQueryable<CasePayment> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }
    }
}