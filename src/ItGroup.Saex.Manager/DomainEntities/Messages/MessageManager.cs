﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class MessageManager : IMessageManager
    {
        private readonly IRepository<Message, Guid> _repository;

        public MessageManager()
        {
        }

        public MessageManager(
            IRepository<Message, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<Message> CreateAsync(Message input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(Message input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<Message> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<Message> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Include(x => x.Replies).ThenInclude(y => y.Sender)
                    .Include(x => x.Sender)
                    .Include(x => x.Recipient)
                    .Where(x => !x.IsDeleted);
        }

        public async Task<Message> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<Message>> GetFiltered(MessageSearchModel filters, long? userId = null)
        {
            var res = new PagedResultDto<Message>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Subject.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Body.ToLower().Contains(filters.SearchText.ToLower()) //||
                                                                                //x.Recipient.FullName.Contains(filters.SearchText.ToLower()) ||
                                                                                //x.Recipient.EmailAddress.Contains(filters.SearchText.ToLower()) ||
                                                                                //x.Sender.FullName.Contains(filters.SearchText.ToLower()) ||
                                                                                //x.Sender.EmailAddress.Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            if (userId != null)
                query = query.Where(x => x.CreatorUserId == userId || x.RecipientId == userId);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<Message> UpdateAsync(Message input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}