﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IMessageManager : IDomainService
    {
        Task<Message> CreateAsync(Message input);

        Task DeleteAsync(Message input);

        IQueryable<Message> GetAll();

        Task<Message> GetAsync(Guid id);

        Task<PagedResultDto<Message>> GetFiltered(MessageSearchModel filters, long? userId);

        Task<Message> UpdateAsync(Message input);
    }
}