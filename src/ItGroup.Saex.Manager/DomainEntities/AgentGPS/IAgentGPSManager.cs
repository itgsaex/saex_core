﻿using Abp.Domain.Services;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public interface IAgentGPSManager : IDomainService
    {
        Task PostAgentGPSLocation(AgentGPS agentGPS);

        Task<List<AgentActivityModel>> GetFiltered(AgentActivitySearchModel filters);
    }
}