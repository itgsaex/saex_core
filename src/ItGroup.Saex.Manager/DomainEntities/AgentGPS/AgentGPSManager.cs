﻿using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.ExtensionMethods.DateTimeExtension;
using ItGroup.Saex.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class AgentGPSManager : IAgentGPSManager
    {
        private readonly IRepository<AgentGPS, Guid> _AgentGPSrepository;

        private readonly IRepository<Agent, Guid> _agentRepository;

        public AgentGPSManager() { }
        public AgentGPSManager(IRepository<AgentGPS, Guid> AgentGPSrepository, IRepository<Agent, Guid> agentRepository)
        {
            _AgentGPSrepository = AgentGPSrepository;
            _agentRepository = agentRepository;
        }

        public async Task PostAgentGPSLocation(AgentGPS agentGPS)
        {
            await _AgentGPSrepository.InsertAsync(agentGPS);
        }

        public async Task<List<AgentActivityModel>> GetFiltered(AgentActivitySearchModel filters)
        {
            var query = _AgentGPSrepository.GetAll();

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Date);
            else
                query = query.OrderByDescending(x => x.Date);

            if (filters.Date != null)
            {
                var date = (DateTime)filters.Date;
                var start = date.StartOfDay();
                var end = date.EndOfDay();

                query = query.Where(x => x.Date > start && x.Date < end);
            }

            var agent = _agentRepository.GetAll().Include(x => x.User).Where(a => a.Id == filters.AgentId).FirstOrDefault();

            if (filters.AgentId != null && filters.AgentId != Guid.Empty)
                query = query.Where(x => x.AgentId == agent.User.UserName);

            return (await query.ToListAsync()).Select(x => new AgentActivityModel(x)).ToList();
        }
    }
}