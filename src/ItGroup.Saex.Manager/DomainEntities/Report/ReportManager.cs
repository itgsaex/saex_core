﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.ExtensionMethods.DateTimeExtension;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using ItGroup.Saex.Manager.DomainEntities.Report.Models.Kpi;
using ItGroup.Saex.Manager.DomainEntities.Report.Models.ReceivedCasesVsAssigned;
using ItGroup.Saex.Manager.DomainEntities.Report.Models.UnassignedCases;
using ItGroup.Saex.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ReportManager : IReportManager
    {
        private readonly IRepository<AuditLog, Guid> _auditLogsRepository;

        private readonly IRepository<Case, Guid> _caseRepository;

        private readonly UserManager _userManager;

        private readonly ICaseRecentActivityRepository _repositoryCaseRecentActivity;

        public ReportManager()
        {
        }

        public ReportManager(IRepository<AuditLog, Guid> auditLogsRepository, IRepository<Case, Guid> caseRepository, UserManager userManager, ICaseRecentActivityRepository repositoryCaseRecentActivity)
        {
            _auditLogsRepository = auditLogsRepository;
            _userManager = userManager;
            _caseRepository = caseRepository;
            _repositoryCaseRecentActivity = repositoryCaseRecentActivity;
        }

        public async Task<List<string>> AuditLogTables()
        {
            var query = await _auditLogsRepository.GetAll().Where(l => l.OperationType == "Create" || l.OperationType == "Delete" || l.OperationType == "Update" || l.OperationType == "View").OrderBy(l => l.TableName).Select(l => l.TableName).Distinct().ToListAsync();
            return query;
        }

        public async Task<PagedResultDto<AuditReportModel>> GetAuditReport(AuditReportSearchModel filters)
        {
            var res = new PagedResultDto<AuditReportModel>();

            if (filters.DateRangeType == DateRangeType.Custom && filters.StartDate == null)
                return res;

            var query = _auditLogsRepository.GetAll().Where(l => l.OperationType == "Create" || l.OperationType == "Delete" || l.OperationType == "Update" || l.OperationType == "View");

            if (filters.UserId > 0)
            {
                var user = await _userManager.FindByIdAsync(filters.UserId.ToString());
                if (user == null)
                    throw new EntityNotFoundException("User not found for the provided Id");

                query = query.Where(x => x.CreatorUserId == user.Id);
            }

            if (!string.IsNullOrEmpty(filters.SelectedAuditLogTable))
            {
                query = query.Where(x => x.TableName == filters.SelectedAuditLogTable);
            }

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                             x.CreatorUsername.ToLower().Contains(filters.SearchText.ToLower()) ||
                             x.OperationType.ToLower().Contains(filters.SearchText.ToLower()) ||
                             x.RecordId.ToLower().Contains(filters.SearchText.ToLower()) ||
                             x.TableName.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            switch (filters.DateRangeType)
            {
                //case DateRangeType.AllDates:
                //    break;

                case DateRangeType.Custom:
                    if (filters.StartDate != null && filters.EndDate != null)
                    {
                        var start = (DateTime)filters.StartDate;
                        var end = (DateTime)filters.EndDate;

                        query = query.Where(x => x.CreationTime >= start.StartOfDay() && x.CreationTime <= end.EndOfDay());
                    }
                    break;

                case DateRangeType.LastWeek:

                    var weekStart = DayOfWeek.Monday;
                    var startingDate = DateTime.Today;

                    while (startingDate.DayOfWeek != weekStart)
                        startingDate = startingDate.AddDays(-1);

                    var previousWeekStart = startingDate.AddDays(-7).StartOfDay();
                    var previousWeekEnd = startingDate.AddDays(-1).EndOfDay();

                    query = query.Where(x => x.CreationTime >= previousWeekStart && x.CreationTime <= previousWeekEnd);
                    break;

                case DateRangeType.LastMonth:

                    var today = DateTime.Today;
                    var month = new DateTime(today.Year, today.Month, 1);
                    var startLastMonth = month.AddMonths(-1);
                    var endLastMonth = month.AddDays(-1);

                    query = query.Where(x => x.CreationTime >= startLastMonth.StartOfDay() && x.CreationTime <= endLastMonth.EndOfDay());

                    break;

                case DateRangeType.LastTrimester:

                    int startMonth = 0;
                    int startYear = DateTime.Now.Year;
                    int startDay = 1;
                    int endMonth = 0;
                    int endYear = DateTime.Now.Year;
                    int endDay = 31;

                    var quarter = GetLastQuarterDates(DateTime.Now.Month);

                    switch (quarter)
                    {
                        case "Q1":
                            startMonth = 1;
                            endMonth = 3;
                            break;

                        case "Q2":
                            startMonth = 4;
                            endMonth = 6;
                            endDay = 30;
                            break;

                        case "Q3":
                            startMonth = 7;
                            endMonth = 9;
                            endDay = 30;
                            break;

                        case "Q4":
                            startMonth = 10;
                            startYear -= 1;
                            endMonth = 12;
                            endYear -= 1;
                            break;
                    }

                    var startDate = new DateTime(startYear, startMonth, startDay).StartOfDay();
                    var endDate = new DateTime(endYear, endMonth, endDay).EndOfDay();

                    query = query.Where(x => x.CreationTime >= startDate && x.CreationTime <= endDate);

                    break;

                case DateRangeType.LastYear:
                    var lastYearStartDate = new DateTime(DateTime.Now.Year - 1, 1, 1).StartOfDay();
                    var lastYearEndDate = new DateTime(DateTime.Now.Year - 1, 12, 31).EndOfDay();

                    query = query.Where(x => x.CreationTime >= lastYearStartDate && x.CreationTime <= lastYearEndDate);

                    break;
                    
                case DateRangeType.ThisWeek:
                    var thisWeekStartDay = DayOfWeek.Monday;
                    var thisWeekStart = DateTime.Today;

                    while (thisWeekStart.DayOfWeek != thisWeekStartDay)
                        thisWeekStart = thisWeekStart.AddDays(-1);

                    query = query.Where(x => x.CreationTime >= thisWeekStart.StartOfDay() && x.CreationTime <= thisWeekStart.AddDays(6).EndOfDay());

                    break;

                case DateRangeType.ThisMonth:
                    var thisYear = DateTime.Now.Year;
                    var thisMonth = DateTime.Now.Month;

                    var thisMonthStartDate = new DateTime(thisYear, thisMonth, 1).StartOfDay();
                    var thisMonthEndDate = new DateTime(thisYear, thisMonth, DateTime.Now.AddMonths(1).AddTicks(-1).Day).EndOfDay();

                    query = query.Where(x => x.CreationTime >= thisMonthStartDate && x.CreationTime <= thisMonthEndDate);

                    break;

                case DateRangeType.ThisYear:

                    var thisYearStartDate = new DateTime(DateTime.Now.Year, 1, 1).StartOfDay();
                    var thisYearEndDate = new DateTime(DateTime.Now.Year, 12, 31).EndOfDay();

                    query = query.Where(x => x.CreationTime >= thisYearStartDate && x.CreationTime <= thisYearEndDate);
                    break;

                default:
                    break;
            }

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Select(x => new AuditReportModel(x)).ToList();
            return res;
        }

        public async Task<PagedResultDto<CaseReportModel>> GetCasesReport(CaseReportSearchModel filters)
        {
            //var res = new PagedResultDto<Case>();
            //var query = this.GetAllCasesQuery();

            //if (!string.IsNullOrWhiteSpace(filters.AccountNumber))
            //{
            //    query = query.Where
            //           (x =>
            //            x.AccountNumber.ToLower().Contains(filters.AccountNumber.ToLower())
            //           );
            //}

            //if (!string.IsNullOrWhiteSpace(filters.Name))
            //{
            //    query = query.Where
            //           (x =>
            //            x.Customers.Where(y => y.FullName.Contains(filters.Name.ToLower())).Any()
            //           );
            //}

            //if (filters.AgentId != null && filters.AgentId != Guid.Empty)
            //{
            //    query = query.Where(x => x.Agents.Where(y => y.Id == filters.AgentId).Any());
            //}

            ////TODO: Implement postalcode at Case Level....
            ////if (filters.PostalCodeId != null && filters.PostalCodeId != Guid.Empty)
            ////{
            ////    query = query.Where(x => x.PostalCodes.Where(y => y.Id == filters.PostalCodeId).Any());
            ////}

            //if (filters.DelinquencyStateId != null && filters.DelinquencyStateId != Guid.Empty)
            //{
            //    query = query.Where(x => x.DelinquencyStateId == filters.DelinquencyStateId);
            //}

            //if (filters.RegionId != null && filters.RegionId != Guid.Empty)
            //{
            //    query = query.Where(x => x.Route.Regions.Where(y => y.Id == filters.RegionId).Any());
            //}

            //if (filters.RouteId != null && filters.RouteId != Guid.Empty)
            //{
            //    query = query.Where(x => x.RouteId == filters.RouteId);
            //}

            //if (filters.SortOrder == SortOrder.Ascending)
            //    query = query.OrderBy(x => x.CreationTime);
            //else
            //    query = query.OrderByDescending(x => x.CreationTime);

            //var results = await query.ToListAsync();

            //res.TotalCount = results.Count();
            //res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            //return res;

            if (string.IsNullOrWhiteSpace(filters.AccountNumber) && !filters.AgentId.HasValue && string.IsNullOrWhiteSpace(filters.AssignType) && string.IsNullOrWhiteSpace(filters.DelinquencyStateId) && string.IsNullOrWhiteSpace(filters.Name) && !filters.PostalCodeId.HasValue && !filters.RegionId.HasValue && !filters.RouteId.HasValue)
            {
                return new PagedResultDto<CaseReportModel>();
            }
            else
            {
                var res = await _repositoryCaseRecentActivity.GetCases(filters);
                return new PagedResultDto<CaseReportModel>(res.ToList().Count, res.ToList());
            }
        }

        public async Task<KpiReportModel> GetKpiReport(KpiReportSearchModel filters)
        {
            var query = GetAllCasesActivitiesQuery();

            //TODO: Implement Query, this is a sample result.
            return new KpiReportModel()
            {
                DateType = KpiReportDateRangeType.Manual,
                EndDate = new DateTime(),
                Messages = 0,
                MortagesBetterOrStable = 0,
                PotentialBetterOrStable = 0,
                PreLossBetterOrStable = 0,
                PromisesAmount = 0,
                ReceivedPayments = 0,
                ReferralsBetterOrStable = 0,
                RemovedAccounts = 0,
                Repositions = 0,
                Scorecards_PortentialplusPreLoss = 0,
                Scorecards_ReferredEarlyStageAndCommercial = 0,
                Scorecards_ReferredMortgage = 0,
                StartDate = new DateTime(),
                TransferedAccounts = 0,
                VisitedAccounts = 0
            };
        }

        public ReceivedCasesVsAssignedReportModel GetReceivedCasesVsAssignedReport()
        {
            var query = this.GetAllCasesActivitiesQuery();

            //TODO: Implement Query, this is a sample result.
            return new ReceivedCasesVsAssignedReportModel()
            {
                AccountsNotAssignedToAgents_CACS = 0,
                AccountsNotAssignedToAgents_KO = 0,
                ActiveAccountsAssignedToAgents_CACS = 0,
                ActiveAccountsAssignedToAgents_KO = 0,
                ActiveAccountsNotAssignedToAgents_CACS = 0,
                ActiveAccountsNotAssignedToAgents_KO = 0,
                AssignedAccountsToAgents_CACS = 0,
                AssignedAccountsToAgents_KO = 0,
                FilteredAccountsByMasterList_CACS = 0,
                FilteredAccountsByMasterList_KO = 0,
                ImportedAccounts_CACS = 0,
                ImportedAccounts_KO = 0,
                InactiveAccountsAssignedToAgents_CACS = 0,
                InactiveAccountsAssignedToAgents_KO = 0,
                InactiveAccountsNotAssignedToAgents_CACS = 0,
                InactiveAccountsNotAssignedToAgents_KO = 0,
                InactiveAccounts_CACS = 0,
                InactiveAccounts_KO = 0,
                SharedAccountsToAgents_CACS = 0,
                SharedAccountsToAgents_KO = 0
            };
        }

        public async Task<List<Case>> GetTransferedCasesReport()
        {
            return await this.GetAllCasesQuery().Where(x => x.Agents.Any() && x.Agents.Where(y => y.IsActive).Any()).ToListAsync();
        }

        public async Task<List<Case>> GetUnassignedCasesReport(UnassignedCasesReportSearchModel filters)
        {
            var query = this.GetAllCasesQuery().Where(x => x.Agents == null || !x.Agents.Any());

            if (!string.IsNullOrWhiteSpace(filters.AccountNumber))
            {
                query = query.Where
                       (x =>
                        x.AccountNumber.ToLower().Contains(filters.AccountNumber.ToLower())
                       );
            }

            if (!string.IsNullOrWhiteSpace(filters.CustomerName))
            {
                query = query.Where
                       (x =>
                        x.Customers.Where(y => y.FullName.ToLower().Contains(filters.CustomerName.ToLower())).Any()
                       );
            }

            if (filters.ProductId != null && filters.ProductId != Guid.Empty)
            {
                query = query.Where(x => x.ProductId == filters.ProductId);
            }

            //TODO: Implement postalcode at Case Level....
            //if (filters.PostalCodeId != null && filters.PostalCodeId != Guid.Empty)
            //{
            //    query = query.Where(x => x.PostalCodes.Where(y => y.Id == filters.PostalCodeId).Any());
            //}

            var results = await query.ToListAsync();

            return results;
        }

        public async Task<List<Case>> GetUnfilteredCasesByMasterListReport(UnfilteredCasesByMasterListSearchModel filters)
        {
            var query = this.GetAllCasesQuery().Where(x => x.ListType == ListType.Unknown && x.ProductType == filters.ProductType);

            if (!string.IsNullOrWhiteSpace(filters.AccountNumber))
            {
                query = query.Where
                       (x =>
                        x.AccountNumber.ToLower().Contains(filters.AccountNumber.ToLower())
                       );
            }

            if (!string.IsNullOrWhiteSpace(filters.CustomerName))
            {
                query = query.Where
                       (x =>
                        x.Customers.Where(y => y.FullName.ToLower().Contains(filters.CustomerName.ToLower())).Any()
                       );
            }

            var results = await query.ToListAsync();

            return results;
        }

        public async Task<IReadOnlyList<UsersReport>> GetUsersReportAsync(UsersReportRequest request)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var result = _userManager.Users;

            if (request != null)
            {
                if (request.SearchTerm != null)
                    result = result.Where(u => u.NormalizedEmailAddress.Contains(request.SearchTerm) || u.NormalizedUserName.Contains(request.SearchTerm) || u.FullName.Contains(request.SearchTerm));

                if (request.ActiveOnly.HasValue && request.ActiveOnly.Value)
                    result = result.Where(u => u.IsActive && !u.IsDeleted);
            }

            return await result.Select(u => new UsersReport(u)).ToListAsync();
        }

        private IQueryable<Case> GetAllCasesActivitiesQuery()
        {
            return _caseRepository
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    //   .Include(x => x.DelinquencyState).ThenInclude(y => y.DelinquencyState)
                    //   .Include(x => x.Customers).ThenInclude(y => y.Addresses).ThenInclude(z => z.PostalCode)
                    //   .Include(x => x.Product).ThenInclude(x => x.Product)
                    //  .Include(x => x.SystemState).ThenInclude(y => y.SystemState)
                    //   .Include(x => x.SystemSubState).ThenInclude(y => y.SystemSubState)
                    .Include(x => x.CaseActivities).ThenInclude(z => z.Agent)
                    .Include(x => x.CaseEvents).ThenInclude(zz => zz.Agent)
                    .Include(x => x.Agents)
                    .Include(x => x.CaseType)
                    .Include(x => x.Tactic);
        }

        private IQueryable<Case> GetAllCasesQuery()
        {
            return _caseRepository
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    .Include(x => x.DelinquencyState).ThenInclude(y => y.DelinquencyState)
                    .Include(x => x.Customers).ThenInclude(y => y.Addresses).ThenInclude(z => z.PostalCode)
                    .Include(x => x.Product).ThenInclude(x => x.Product)
                    .Include(x => x.SystemState).ThenInclude(y => y.SystemState)
                    .Include(x => x.SystemSubState).ThenInclude(y => y.SystemSubState)
                    .Include(x => x.CaseActivities)
                    .Include(x => x.CaseEvents)
                    .Include(x => x.Agents)
                    .Include(x => x.CaseType)
                    .Include(x => x.Tactic);
        }

        private string GetLastQuarterDates(int month)
        {
            string quarter = string.Empty;

            if (month >= 1 && month <= 3)
            {
                quarter = "Q4";
            }
            if (month >= 4 && month <= 6)
            {
                quarter = "Q1";
            }
            if (month >= 7 && month <= 9)
            {
                quarter = "Q2";
            }
            if (month >= 10 && month <= 12)
            {
                quarter = "Q3";
            }

            return quarter;
        }
    }
}
