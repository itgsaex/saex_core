﻿using System;

namespace ItGroup.Saex.Manager.DomainEntities.Report.Models
{
    public abstract class Report
    {
        protected Report()
        {
            PrintDate = DateTime.Now;
        }

        public DateTime PrintDate { get; }
    }
}
