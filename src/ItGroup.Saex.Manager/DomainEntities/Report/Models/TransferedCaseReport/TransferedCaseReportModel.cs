﻿using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using System.Linq;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class TransferedCaseReportModel : Report
    {
        public TransferedCaseReportModel()
        {
        }

        public TransferedCaseReportModel(AgentCase input)
        {
            AccountNumber = input.Case.AccountNumber;

            Balance = input.Case.OwedAmount;

            OriginalAmount = input.Case.OriginalAmount;

            Agent = input.Agent;

            if (input.Case.Customers != null)
            {
                var customer = input.Case.Customers.Where(x => x.IsPrincipal).SingleOrDefault();
                if (customer != null)
                    CustomerName = customer.FullName;
            }

            if (input.Case.Product != null)
            {
                ProductNumber = input.Case.Product.Product.ProductNumber;
            }
        }

        public string AccountNumber { get; set; }

        public Agent Agent { get; set; }

        public double Balance { get; set; }

        public string CustomerName { get; set; }

        public double OriginalAmount { get; set; }

        public string PostalCode { get; set; }

        public string ProductNumber { get; set; }

        public TransferType TransferType { get; set; }
    }
}
