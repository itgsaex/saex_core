﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class KpiReportSearchModel
    {
        public KpiReportSearchModel()
        {
        }

        public KpiReportSearchModel(Guid? agentId, Guid? regionId, KpiReportDateRangeType dateType, DateTime startDate, DateTime endDate)
        {
            AgentId = agentId;
            RegionId = regionId;
            DateType = dateType;
            StartDate = startDate;
            EndDate = endDate;
        }

        public Guid? AgentId { get; set; }

        public KpiReportDateRangeType DateType { get; set; }

        public DateTime EndDate { get; set; }

        public Guid? RegionId { get; set; }

        public DateTime StartDate { get; set; }
    }
}
