﻿using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Manager.DomainEntities.Report.Models.ReceivedCasesVsAssigned
{
    public class ReceivedCasesVsAssignedReportModel : Report
    {
        public int AccountsNotAssignedToAgents_CACS { get; set; }

        public int AccountsNotAssignedToAgents_KO { get; set; }

        public int ActiveAccountsAssignedToAgents_CACS { get; set; }

        public int ActiveAccountsAssignedToAgents_KO { get; set; }

        public int ActiveAccountsNotAssignedToAgents_CACS { get; set; }

        public int ActiveAccountsNotAssignedToAgents_KO { get; set; }

        public int AssignedAccountsToAgents_CACS { get; set; }

        public int AssignedAccountsToAgents_KO { get; set; }

        public int FilteredAccountsByMasterList_CACS { get; set; }

        public int FilteredAccountsByMasterList_KO { get; set; }

        public int ImportedAccounts_CACS { get; set; }

        public int ImportedAccounts_KO { get; set; }

        public int InactiveAccounts_CACS { get; set; }

        public int InactiveAccounts_KO { get; set; }

        public int InactiveAccountsAssignedToAgents_CACS { get; set; }

        public int InactiveAccountsAssignedToAgents_KO { get; set; }

        public int InactiveAccountsNotAssignedToAgents_CACS { get; set; }

        public int InactiveAccountsNotAssignedToAgents_KO { get; set; }

        public int SharedAccountsToAgents_CACS { get; set; }

        public int SharedAccountsToAgents_KO { get; set; }
    }
}
