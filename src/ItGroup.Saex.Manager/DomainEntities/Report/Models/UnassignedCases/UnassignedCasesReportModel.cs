﻿using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Manager.DomainEntities.Report.Models.UnassignedCases
{
    public class UnassignedCasesReportModel : Report
    {
        public UnassignedCasesReportModel()
        {
        }

        public List<UnassignedCasesListItemModel> Cases { get; set; }

        public string PostalCode { get; set; }

        public Product Product { get; set; }
    }
}
