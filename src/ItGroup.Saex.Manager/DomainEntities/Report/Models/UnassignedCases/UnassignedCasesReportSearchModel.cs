﻿using System;
using System.Collections.Generic;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Manager.DomainEntities.Report.Models.UnassignedCases
{
    public class UnassignedCasesReportSearchModel
    {
        public UnassignedCasesReportSearchModel()
        {
        }

        public UnassignedCasesReportSearchModel(Guid? productId, string accountNumber, string customerName, string postalCode)
        {
            ProductId = productId;
            AccountNumber = accountNumber;
            CustomerName = customerName;
            PostalCode = postalCode;
        }

        public string AccountNumber { get; set; }

        public string CustomerName { get; set; }

        public string PostalCode { get; set; }

        public Guid? ProductId { get; set; }
    }
}
