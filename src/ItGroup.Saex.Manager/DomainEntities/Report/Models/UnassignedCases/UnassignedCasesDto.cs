﻿using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Manager.DomainEntities.Report.Models.UnassignedCases
{
    public class UnassignedCasesListItemModel
    {
        public UnassignedCasesListItemModel()
        {
        }

        public UnassignedCasesListItemModel(Case input)
        {
            if (input.DueDate != null)
                DaysOverdue = Convert.ToInt32((DateTime.Now - (DateTime)input.DueDate).TotalDays);

            AccountNumber = input.AccountNumber;

            OwedAmount = input.OwedAmount;
            Status = input.Status;

            if (input.Customers != null)
            {
                var customer = input.Customers.Where(x => x.IsPrincipal).SingleOrDefault();
                if (customer != null)
                    CustomerName = customer.FullName;
            }

            if (input.Product != null)
            {
                ProductNumber = input.Product.Product.ProductNumber;
            }

            if (input.Route != null)
            {
                this.RouteName = input.Route.Name;
            }
        }

        public string AccountNumber { get; set; }

        public string CustomerName { get; set; }

        public int DaysOverdue { get; set; }

        public double OwedAmount { get; set; }

        public string PostalCode { get; set; }

        public string ProductNumber { get; set; }

        public string RouteName { get; set; }

        public string Status { get; set; }
    }
}
