﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AuditReportSearchModel : PagedResultRequestDto
    {
        public AuditReportSearchModel()
        {
        }

        public DateRangeType DateRangeType { get; set; }

        public DateTime? EndDate { get; set; }

        public string SearchText { get; set; }

        public string SectionName { get; set; }

        public DateTime? StartDate { get; set; }

        public long UserId { get; set; }
        
        public string SelectedAuditLogTable { get; set; }
    }
}
