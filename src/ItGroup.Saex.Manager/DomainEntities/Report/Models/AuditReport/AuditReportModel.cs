﻿using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class AuditReportModel : Report
    {

        private List<string> IgnoreColumns = new List<string>()
        {
            "DeleterUserId","DeletionTime","LastModificationTime","LastModifierUserId","CreationTime","CreatorUserId","Id"
        };

        public AuditReportModel(AuditLog input)
        {
            CreatorUsername = input.CreatorUsername;
            NewValue = input.NewValue;
            OperationType = input.OperationType;
            PreviousValue = input.PreviousValue;
            RecordId = input.RecordId;
            TableName = input.TableName;
            Id = input.Id;
            CreationTime = input.CreationTime;
            Description = input.Description;

            if (OperationType == "Update")
            {
                Changes = GetEntityChange(PreviousValue, NewValue);
            }
        }

        public string CreatorUsername { get; set; }

        public DateTime CreationTime { get; set; }

        public Guid Id { get; set; }

        public string NewValue { get; set; }

        public string OperationType { get; set; }

        public string PreviousValue { get; set; }

        public string RecordId { get; set; }

        public string TableName { get; set; }

        public string Changes { get; set; }

        public string Description { get; set; }

        private string GetEntityChange (string previousValue, string newValue)
        {
            string retval = "";
            List<string> messages = new List<string>();

            JObject sourceJObject = JsonConvert.DeserializeObject<JObject>(previousValue);
            JObject targetJObject = JsonConvert.DeserializeObject<JObject>(newValue);

            if (!JToken.DeepEquals(sourceJObject, targetJObject))
            {
                foreach (KeyValuePair<string, JToken> sourceProperty in sourceJObject)
                {

                    if (IgnoreColumns.Contains(sourceProperty.Key))
                        continue;

                    JProperty targetProp = targetJObject.Property(sourceProperty.Key);

                    if (targetProp != null)
                    {
                        if (!JToken.DeepEquals(sourceProperty.Value, targetProp.Value))
                        {
                            messages.Add(String.Format("Column {0} change from {1} to {2}", sourceProperty.Key, sourceProperty.Value, targetProp.Value));
                        }
                    }
                }
            }
            else
            {
                retval = "";
            }

            if (messages.Count > 0)
            {
                retval = messages.Join("<br />");
            }

            return retval;
        }
    }
}
