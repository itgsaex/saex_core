﻿using ItGroup.Saex.Authorization.Users;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.Manager.DomainEntities.Report.Models
{
    public class UsersReport : Report
    {
        public UsersReport() { }

        public UsersReport(User user) : this()
        {
            if (user is null)
                throw new ArgumentNullException(nameof(user));

            PhoneNumber = user.PhoneNumber;
            IsActive = user.IsActive;
            Name = user.Name;
            UserName = user.UserName;
            EmailAddress = user.EmailAddress;
            IsEmailConfirmed = user.IsEmailConfirmed;
            FullName = user.FullName;
            Surname = user.Surname;
            IsDeleted = user.IsDeleted;
        }

        public string PhoneNumber { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public string FullName { get; set; }

        public string Surname { get; set; }

        public bool IsDeleted { get; set; }

        public static IReadOnlyList<UsersReport> Preview() =>
            new[]
            {
                new UsersReport
                {
                    IsActive = true,
                    Name = "Preview",
                    Surname = "001",
                    UserName = "preview001",
                    EmailAddress = "preview001@test.com",
                    IsEmailConfirmed = true,
                    FullName = "Preview 001",
                    IsDeleted = false
                },
                new UsersReport
                {
                    IsActive = true,
                    Name = "Preview",
                    Surname = "002",
                    UserName = "preview002",
                    EmailAddress = "preview002@test.com",
                    IsEmailConfirmed = false,
                    FullName = "Preview 002",
                    IsDeleted = false
                },
                new UsersReport
                {
                    IsActive = false,
                    IsDeleted = true,
                    Name = "Preview",
                    Surname = "003",
                    UserName = "preview003",
                    EmailAddress = "preview003@test.com",
                    IsEmailConfirmed = true,
                    FullName = "Preview 003"
                }
            };
    }
}
