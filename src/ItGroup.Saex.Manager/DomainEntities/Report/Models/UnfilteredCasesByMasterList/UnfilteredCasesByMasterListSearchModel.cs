﻿using System;
using System.Collections.Generic;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Manager.DomainEntities.Report.Models.UnassignedCases
{
    public class UnfilteredCasesByMasterListSearchModel
    {
        public UnfilteredCasesByMasterListSearchModel()
        {
        }

        public UnfilteredCasesByMasterListSearchModel(ProductType productType, string accountNumber, string customerName)
        {
            ProductType = productType;
            this.AccountNumber = accountNumber;
            this.CustomerName = customerName;
        }

        public string AccountNumber { get; set; }

        public string CustomerName { get; set; }

        public ProductType ProductType { get; set; }
    }
}
