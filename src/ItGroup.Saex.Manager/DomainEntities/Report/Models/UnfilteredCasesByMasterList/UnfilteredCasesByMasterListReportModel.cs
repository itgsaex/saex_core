﻿using ItGroup.Saex.DomainEntities;
using System.Collections.Generic;

namespace ItGroup.Saex.Manager.DomainEntities.Report.Models.UnassignedCases
{
    public class UnfilteredCasesByMasterListReportModel : Report
    {
        public UnfilteredCasesByMasterListReportModel()
        {
        }

        public List<UnfilteredCasesByMasterListItemModel> Cases { get; set; }

        public string CustomerName { get; set; }

        public string AccountNumber { get; set; }
    }
}
