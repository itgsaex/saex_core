﻿using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Manager.DomainEntities.Report.Models.UnassignedCases
{
    public class UnfilteredCasesByMasterListItemModel
    {
        public UnfilteredCasesByMasterListItemModel()
        {
        }

        public UnfilteredCasesByMasterListItemModel(Case input)
        {
            AccountNumber = input.AccountNumber;

            //if (PostalCode != null)
            //    PostalCode = input.PostalCode;

            if (input.Customers != null)
            {
                var customer = input.Customers.Where(x => x.IsPrincipal).SingleOrDefault();
                if (customer != null)
                    CustomerName = customer.FullName;
            }

            if (input.Product != null)
            {
                ProductName = input.Product.Product.Code;
            }
        }

        public string AccountNumber { get; set; }

        public string CustomerName { get; set; }

        public int DaysOverdue { get; set; }

        public string OwedAmount { get; set; }

        public PostalCode PostalCode { get; set; }

        public string ProductName { get; set; }
    }
}
