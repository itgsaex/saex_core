﻿namespace ItGroup.Saex.Manager.DomainEntities.Report.Models
{
    public class UsersReportRequest : ReportRequest
    {
        public bool? ActiveOnly { get; set; }

        public string SearchTerm { get; set; }
    }
}
