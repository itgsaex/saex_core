﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using ItGroup.Saex.Manager.DomainEntities.Report.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IReportManager : IDomainService
    {
        Task<PagedResultDto<AuditReportModel>> GetAuditReport(AuditReportSearchModel filters);

        Task<IReadOnlyList<UsersReport>> GetUsersReportAsync(UsersReportRequest request);

        Task<List<string>> AuditLogTables();
    }
}
