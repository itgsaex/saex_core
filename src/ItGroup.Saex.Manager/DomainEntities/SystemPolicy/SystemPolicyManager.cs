﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemPolicyManager : ISystemPolicyManager
    {
        private readonly IRepository<SystemPolicy, Guid> _repository;
        public SystemPolicyManager() { }
        public SystemPolicyManager(
            IRepository<SystemPolicy, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<SystemPolicy> CreateAsync(SystemPolicy input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(SystemPolicy input)
        {
            await _repository.DeleteAsync(input);
        }

        public async Task<SystemPolicy> UpdateAsync(SystemPolicy input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<SystemPolicy> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public IQueryable<SystemPolicy> GetAll()
        {
            return this.GetAllQuery();
        }

        public async Task<PagedResultDto<SystemPolicy>> GetFiltered(SystemPolicySearchModel filters)
        {
            var res = new PagedResultDto<SystemPolicy>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Value.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;

        }

        public IQueryable<SystemPolicy> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }
    }
}