﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ISystemPolicyManager : IDomainService
    {
        Task<SystemPolicy> CreateAsync(SystemPolicy input);

        Task<SystemPolicy> UpdateAsync(SystemPolicy input);

        Task DeleteAsync(SystemPolicy input);

        Task<SystemPolicy> GetAsync(Guid id);

        IQueryable<SystemPolicy> GetAll();

        Task<PagedResultDto<SystemPolicy>> GetFiltered(SystemPolicySearchModel filters);
    }
}