﻿using Abp.Domain.Services;
using ItGroup.Saex.DomainEntities;
using System.Threading.Tasks;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public interface ISessionLogManager : IDomainService
    {
        Task PostSessionLog(SessionLog sessionLog);
    }
}