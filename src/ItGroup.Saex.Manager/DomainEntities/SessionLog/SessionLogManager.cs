﻿using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using System;
using System.Threading.Tasks;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class SessionLogManager : ISessionLogManager
    {

        private readonly IRepository<SessionLog, Guid> _SessionLogrepository;
        public SessionLogManager() { }
        public SessionLogManager(IRepository<SessionLog, Guid> SessionLogrepository)
        {
            _SessionLogrepository = SessionLogrepository;
        }

        public async Task PostSessionLog(SessionLog sessionLog)
        {
            await _SessionLogrepository.InsertAsync(sessionLog);
        }
    }
}