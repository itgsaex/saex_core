﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IDelinquencyStateManager : IDomainService
    {
        Task<DelinquencyState> CreateAsync(DelinquencyState input);

        Task<DelinquencyState> UpdateAsync(DelinquencyState input);

        Task DeleteAsync(DelinquencyState input);

        Task<DelinquencyState> GetAsync(Guid id);

        IQueryable<DelinquencyState> GetAll();

        Task<PagedResultDto<DelinquencyState>> GetFiltered(DelinquencyStateSearchModel filters);
    }
}