﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class DelinquencyStateManager : IDelinquencyStateManager
    {
        private readonly IRepository<DelinquencyState, Guid> _repository;

        public DelinquencyStateManager()
        {
        }

        public DelinquencyStateManager(
            IRepository<DelinquencyState, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<DelinquencyState> CreateAsync(DelinquencyState input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(DelinquencyState input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<DelinquencyState> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<DelinquencyState> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<DelinquencyState> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<DelinquencyState> GetByValue(string value)
        {
            return await this.GetAllQuery()
                            .Where(x => x.SuccessfulRuleValue == value || x.UnsuccessfulRuleValue == value)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<DelinquencyState>> GetFiltered(DelinquencyStateSearchModel filters)
        {
            var res = new PagedResultDto<DelinquencyState>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Condition.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Rule.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Precedence.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<DelinquencyState> UpdateAsync(DelinquencyState input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
