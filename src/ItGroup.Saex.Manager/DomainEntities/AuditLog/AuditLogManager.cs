﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AuditLogManager : IAuditLogManager
    {
        private readonly IRepository<AuditLog, Guid> _repository;

        public AuditLogManager()
        {
        }

        public AuditLogManager(
            IRepository<AuditLog, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<AuditLog> CreateAsync(AuditLog input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(AuditLog input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<AuditLog> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<AuditLog> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<AuditLog> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<AuditLog>> GetFiltered(AuditLogSearchModel filters)
        {
            var res = new PagedResultDto<AuditLog>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.TableName.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.RecordId.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.PreviousValue.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.NewValue.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.UserId != null && filters.UserId > 0)
            {
                query = query.Where(x => x.CreatorUserId == filters.UserId);
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<AuditLog> UpdateAsync(AuditLog input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
