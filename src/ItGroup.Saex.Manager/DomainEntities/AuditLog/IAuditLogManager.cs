﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAuditLogManager : IDomainService
    {
        Task<AuditLog> CreateAsync(AuditLog input);

        Task<AuditLog> UpdateAsync(AuditLog input);

        Task DeleteAsync(AuditLog input);

        Task<AuditLog> GetAsync(Guid id);

        IQueryable<AuditLog> GetAll();

        Task<PagedResultDto<AuditLog>> GetFiltered(AuditLogSearchModel filters);
    }
}