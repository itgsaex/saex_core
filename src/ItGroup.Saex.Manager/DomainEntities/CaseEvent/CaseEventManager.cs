﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseEventManager : ICaseEventManager
    {
        private readonly IRepository<CaseEvent, Guid> _repository;

        public CaseEventManager()
        {
        }

        public CaseEventManager(
            IRepository<CaseEvent, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<CaseEvent> CreateAsync(CaseEvent input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(CaseEvent input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<CaseEvent> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<CaseEvent> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<CaseEvent> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<CaseEvent>> GetFiltered(CaseEventSearchModel filters)
        {
            var res = new PagedResultDto<CaseEvent>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<CaseEvent> UpdateAsync(CaseEvent input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}