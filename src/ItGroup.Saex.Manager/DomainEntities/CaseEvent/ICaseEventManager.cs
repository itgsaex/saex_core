﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseEventManager : IDomainService
    {
        Task<CaseEvent> CreateAsync(CaseEvent input);

        Task<CaseEvent> UpdateAsync(CaseEvent input);

        Task DeleteAsync(CaseEvent input);

        Task<CaseEvent> GetAsync(Guid id);

        IQueryable<CaseEvent> GetAll();

        Task<PagedResultDto<CaseEvent>> GetFiltered(CaseEventSearchModel filters);
    }
}