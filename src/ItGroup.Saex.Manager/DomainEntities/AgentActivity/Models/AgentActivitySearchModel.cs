﻿using Abp.Application.Services.Dto;
using ItGroup.Saex.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentActivitySearchModel : PagedResultRequestDto
    {
        public AgentActivitySearchModel()
        {
        }

        public AgentActivitySearchModel(Guid agentId, DateTime? date = null, string searchText = null, SortOrder sortOrder = SortOrder.Ascending)
        {
            AgentId = agentId;
            Date = date;
            SearchText = searchText;
            SortOrder = sortOrder;
        }

        public Guid AgentId { get; set; }

        public DateTime? Date { get; set; }

        public string SearchText { get; set; }

        public SortOrder SortOrder { get; set; }
    }
}
