﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using ItGroup.Saex.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IAgentActivityManager : IDomainService
    {
        Task<AgentActivity> CreateAsync(AgentActivity input);

        Task DeleteAsync(AgentActivity input);

        IQueryable<AgentActivity> GetAll();

        Task<AgentActivity> GetAsync(Guid id);

        Task<List<AgentActivityModel>> GetFiltered(AgentActivitySearchModel filters);

        Task<AgentActivity> UpdateAsync(AgentActivity input);
    }
}
