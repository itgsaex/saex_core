﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.ExtensionMethods.DateTimeExtension;
using ItGroup.Saex.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentActivityManager : IAgentActivityManager
    {
        private readonly IRepository<AgentActivity, Guid> _repository;
        private readonly IRepository<CaseActivity, Guid> _caseActivityRepository;

        public AgentActivityManager()
        {
        }

        public AgentActivityManager(
            IRepository<AgentActivity, Guid> repository,
            IRepository<CaseActivity, Guid> caseActivityRepository)
        {
            _repository = repository;
            _caseActivityRepository = caseActivityRepository;
        }

        public async Task<AgentActivity> CreateAsync(AgentActivity input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(AgentActivity input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<AgentActivity> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<AgentActivity> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Include(x => x.Agent).ThenInclude(y => y.User)
                    .Include(x => x.CaseActivity)
                    .Include(x => x.CaseActivity.ActivityCodes)
                    .Include(x => x.CaseActivity.VisitPlace);
        }

        public async Task<AgentActivity> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<List<AgentActivityModel>> GetFiltered(AgentActivitySearchModel filters)
        {
            var query = _caseActivityRepository.GetAll();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Agent.User.FullName.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Agent.CollectorId.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Agent.Id.ToString().Contains(filters.SearchText)
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Date);
            else
                query = query.OrderByDescending(x => x.Date);

            if (filters.Date != null)
            {
                var date = (DateTime)filters.Date;
                var start = date.StartOfDay();
                var end = date.EndOfDay();

                query = query.Where(x => x.Date > start && x.Date < end);
            }

            if (filters.AgentId != null && filters.AgentId != Guid.Empty)
                query = query.Where(x => x.AgentId == filters.AgentId);

            return (await query.Include(x => x.VisitPlace).Include(x => x.Case).ThenInclude(c => c.CaseType).ToListAsync()).Select(x => new AgentActivityModel(x)).ToList();
        }

        public async Task<AgentActivity> UpdateAsync(AgentActivity input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}