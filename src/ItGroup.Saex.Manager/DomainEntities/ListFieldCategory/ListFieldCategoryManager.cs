﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListFieldCategoryManager : IListFieldCategoryManager
    {
        private readonly IRepository<ListFieldCategory, Guid> _repository;
        public ListFieldCategoryManager() { }
        public ListFieldCategoryManager(
            IRepository<ListFieldCategory, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<ListFieldCategory> CreateAsync(ListFieldCategory input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(ListFieldCategory input)
        {
            await _repository.DeleteAsync(input);
        }

        public async Task<ListFieldCategory> UpdateAsync(ListFieldCategory input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<ListFieldCategory> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public IQueryable<ListFieldCategory> GetAll()
        {
            return this.GetAllQuery();
        }

        public async Task<PagedResultDto<ListFieldCategory>> GetFiltered(ListFieldCategorySearchModel filters)
        {
            var res = new PagedResultDto<ListFieldCategory>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;

        }

        public IQueryable<ListFieldCategory> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }
    }
}