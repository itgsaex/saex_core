﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IListFieldCategoryManager : IDomainService
    {
        Task<ListFieldCategory> CreateAsync(ListFieldCategory input);

        Task<ListFieldCategory> UpdateAsync(ListFieldCategory input);

        Task DeleteAsync(ListFieldCategory input);

        Task<ListFieldCategory> GetAsync(Guid id);

        IQueryable<ListFieldCategory> GetAll();

        Task<PagedResultDto<ListFieldCategory>> GetFiltered(ListFieldCategorySearchModel filters);
    }
}