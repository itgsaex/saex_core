﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseImportManager : IDomainService
    {
        Task<CaseBaseline> CreateAsync(CaseBaseline input);

        Task DeleteAsync(CaseBaseline input);

        IQueryable<CaseBaseline> GetAll();

        Task<CaseBaseline> GetAsync(Guid id);

        Task<PagedResultDto<CaseBaseline>> GetFiltered(CaseImportSearchModel filters);

        Task<CaseBaseline> UpdateAsync(CaseBaseline input);
    }
}
