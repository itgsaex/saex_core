﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseImportManager : ICaseImportManager
    {
        private readonly IRepository<CaseBaseline, Guid> _repository;

        public CaseImportManager()
        {
        }

        public CaseImportManager(
            IRepository<CaseBaseline, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<CaseBaseline> CreateAsync(CaseBaseline input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(CaseBaseline input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<CaseBaseline> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<CaseBaseline> GetAllQuery()
        {
            return _repository
                    .GetAll();
        }

        public Task<List<CaseBaseline>> GetAllQueryAsync()
        {
            return _repository
                    .GetAllListAsync();
        }


        public async Task<CaseBaseline> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<CaseBaseline>> GetFiltered(CaseImportSearchModel filters)
        {
            var items = this.GetAllQuery().ToList();
            var res = new PagedResultDto<CaseBaseline>(items.Count, items);

            //if (!string.IsNullOrWhiteSpace(filters.SearchText))
            //{
            //    //query = query.Where
            //    //       (x =>
            //    //       // x.Customer.FullName.ToLower().Contains(filters.SearchText.ToLower()) ||
            //    //        x.AccountNumber.ToLower().Contains(filters.SearchText.ToLower()) // ||
            //                                                                             //x.PhoneNumber.ToLower().Contains(filters.SearchText.ToLower()) ||
            //                                                                             // x.EmailAddress.ToLower().Contains(filters.SearchText.ToLower()) ||
            //                                                                             //  x.ClientId.ToLower().Contains(filters.SearchText.ToLower())
            //           );
            //}

            /*if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();*/

            //res.TotalCount = results.Count();
            //res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<CaseBaseline> UpdateAsync(CaseBaseline input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
