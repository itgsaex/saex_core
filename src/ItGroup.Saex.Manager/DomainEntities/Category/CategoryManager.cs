﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace ItGroup.Saex.Manager.DomainEntities
{
    public class CategoryManager : ICategoryManager
    {

        private readonly IRepository<Category, Guid> _Categoryrepository;
        public CategoryManager() { }
        public CategoryManager(IRepository<Category, Guid> Categoryrepository)
        {
            _Categoryrepository = Categoryrepository;
        }

        public IQueryable<Category> GetAllCategories()
        {
            return _Categoryrepository
                   .GetAll().OrderBy(o => o.Description);
        }
    }
}