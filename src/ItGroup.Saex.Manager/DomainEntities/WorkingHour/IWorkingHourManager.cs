﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IWorkingHourManager : IDomainService
    {
        Task<WorkingHour> CreateAsync(WorkingHour input);

        Task<WorkingHour> UpdateAsync(WorkingHour input);

        Task DeleteAsync(WorkingHour input);

        Task<WorkingHour> GetAsync(Guid id);

        IQueryable<WorkingHour> GetAll();

        Task<PagedResultDto<WorkingHour>> GetFiltered(WorkingHourSearchModel filters);
    }
}