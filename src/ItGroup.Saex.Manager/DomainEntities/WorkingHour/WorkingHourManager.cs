﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class WorkingHourManager : IWorkingHourManager
    {
        private readonly IRepository<WorkingHour, Guid> _repository;

        public WorkingHourManager()
        {
        }

        public WorkingHourManager(
            IRepository<WorkingHour, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<WorkingHour> CreateAsync(WorkingHour input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(WorkingHour input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<WorkingHour> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<WorkingHour> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<WorkingHour> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<WorkingHour>> GetFiltered(WorkingHourSearchModel filters)
        {
            var res = new PagedResultDto<WorkingHour>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
                query = query.Where(x => x.Name.ToLower().Contains(filters.SearchText));

            query = query.OrderBy(x => x.Name);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<WorkingHour> UpdateAsync(WorkingHour input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}