﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class QueryFieldsManager : IQueryFieldsManager
    {
        private readonly IRepository<QueryFields, Guid> _repository;

        public QueryFieldsManager()
        {
        }

        public QueryFieldsManager(
            IRepository<QueryFields, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<QueryFields> CreateAsync(QueryFields input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(QueryFields input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<QueryFields> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<QueryFields> GetAllQuery()
        {
            return _repository.GetAll();
        }

        public async Task<QueryFields> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<QueryFields>> GetFiltered(QueryFieldsSearchModel filters)
        {
            var res = new PagedResultDto<QueryFields>();
            var query = this.GetAllQuery();

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.FieldId);
            else
                query = query.OrderByDescending(x => x.FieldId);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<QueryFields> UpdateAsync(QueryFields input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
