﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IQueryFieldsManager : IDomainService
    {
        Task<QueryFields> CreateAsync(QueryFields input);

        Task DeleteAsync(QueryFields input);

        IQueryable<QueryFields> GetAll();

        Task<QueryFields> GetAsync(Guid id);

        Task<PagedResultDto<QueryFields>> GetFiltered(QueryFieldsSearchModel filters);

        Task<QueryFields> UpdateAsync(QueryFields input);
    }
}
