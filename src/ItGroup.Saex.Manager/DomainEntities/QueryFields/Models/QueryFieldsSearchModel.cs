﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class QueryFieldsSearchModel : PagedResultRequestDto
    {
        public QueryFieldsSearchModel()
        {
        }

        public string SearchText { get; set; }

        public SortOrder SortOrder { get; set; }
    }
}
