﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IScorecardParameterManager : IDomainService
    {
        Task<ScorecardParameter> CreateAsync(ScorecardParameter input);

        Task<ScorecardParameter> UpdateAsync(ScorecardParameter input);

        Task DeleteAsync(ScorecardParameter input);

        Task<ScorecardParameter> GetAsync(Guid id);

        IQueryable<ScorecardParameter> GetAll();

        Task<PagedResultDto<ScorecardParameter>> GetFiltered(ScorecardParameterSearchModel filters);
    }
}