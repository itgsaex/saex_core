﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ScorecardParameterManager : IScorecardParameterManager
    {
        private readonly IRepository<ScorecardParameter, Guid> _repository;
        public ScorecardParameterManager() { }
        public ScorecardParameterManager(
            IRepository<ScorecardParameter, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<ScorecardParameter> CreateAsync(ScorecardParameter input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(ScorecardParameter input)
        {
            await _repository.DeleteAsync(input);
        }

        public async Task<ScorecardParameter> UpdateAsync(ScorecardParameter input)
        {
            return await _repository.UpdateAsync(input);
        }

        public async Task<ScorecardParameter> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public IQueryable<ScorecardParameter> GetAll()
        {
            return this.GetAllQuery();
        }

        public async Task<PagedResultDto<ScorecardParameter>> GetFiltered(ScorecardParameterSearchModel filters)
        {
            var res = new PagedResultDto<ScorecardParameter>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.StandardValue.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.PaymentLevel.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;

        }

        public IQueryable<ScorecardParameter> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }
    }
}