﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface IPaymentDelayReasonManager : IDomainService
    {
        Task<PaymentDelayReason> CreateAsync(PaymentDelayReason input);

        Task DeleteAsync(PaymentDelayReason input);

        IQueryable<PaymentDelayReason> GetAll();

        Task<PaymentDelayReason> GetAsync(Guid id);

        Task<PagedResultDto<PaymentDelayReason>> GetFiltered(PaymentDelayReasonSearchModel filters);

        Task<PaymentDelayReason> UpdateAsync(PaymentDelayReason input);
    }
}