﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class PaymentDelayReasonManager : IPaymentDelayReasonManager
    {
        private readonly IRepository<PaymentDelayReason, Guid> _repository;

        public PaymentDelayReasonManager()
        {
        }

        public PaymentDelayReasonManager(
            IRepository<PaymentDelayReason, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<PaymentDelayReason> CreateAsync(PaymentDelayReason input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(PaymentDelayReason input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<PaymentDelayReason> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<PaymentDelayReason> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<PaymentDelayReason> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<PaymentDelayReason>> GetFiltered(PaymentDelayReasonSearchModel filters)
        {
            var res = new PagedResultDto<PaymentDelayReason>();
            var query = this.GetAllQuery();

            if (!string.IsNullOrWhiteSpace(filters.SearchText))
            {
                query = query.Where
                       (x =>
                        x.Code.ToLower().Contains(filters.SearchText.ToLower()) ||
                        x.Description.ToLower().Contains(filters.SearchText.ToLower())
                       );
            }

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.Code);
            else
                query = query.OrderByDescending(x => x.Code);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.ToList();
            return res;
        }

        public async Task<PaymentDelayReason> UpdateAsync(PaymentDelayReason input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}