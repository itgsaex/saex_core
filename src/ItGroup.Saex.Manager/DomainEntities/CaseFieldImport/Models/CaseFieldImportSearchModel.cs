﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseFieldImportSearchModel : PagedResultRequestDto
    {
        public CaseFieldImportSearchModel()
        {
        }

        public string SearchText { get; set; }

        public SortOrder SortOrder { get; set; }
    }

    public class ValidFieldsForListsSearchModel
    {
        public ValidFieldsForListsSearchModel()
        {
        }

        public ValidFieldsForListsSearchModel(Guid? product)
        {
            Product = product;
        }
        
        public Guid? Product { get; set; }
    }
}
