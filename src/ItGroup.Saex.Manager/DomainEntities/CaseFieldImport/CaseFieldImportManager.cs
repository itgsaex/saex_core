﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseFieldImportManager : ICaseFieldImportManager
    {
        private readonly ICaseRecentActivityRepository _caseRecentRepository;
        private readonly ICaseFieldsImportRepository _repository;

        public CaseFieldImportManager()
        {
        }

        public CaseFieldImportManager(
            ICaseRecentActivityRepository caseRecentActivityRepository,
            ICaseFieldsImportRepository repository)
        {
            _caseRecentRepository = caseRecentActivityRepository;
            _repository = repository;
        }

        public async Task<CaseFieldImport> CreateAsync(CaseFieldImport input)
        {
            return await _repository.InsertAsync(input);
        }

        public async Task DeleteAsync(CaseFieldImport input)
        {
            await _repository.DeleteAsync(input);
        }

        public IQueryable<CaseFieldImport> GetAll()
        {
            return this.GetAllQuery();
        }

        public IQueryable<CaseFieldImport> GetAllQuery()
        {
            return _repository
                    .GetAll()
                    .Where(x => !x.IsDeleted);
        }

        public async Task<CaseFieldImport> GetAsync(Guid id)
        {
            return await this.GetAllQuery()
                            .Where(x => x.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<PagedResultDto<CaseFieldImport>> GetFiltered(CaseFieldImportSearchModel filters)
        {
            var res = new PagedResultDto<CaseFieldImport>();
            var query = this.GetAllQuery();

            //if (!string.IsNullOrWhiteSpace(filters.SearchText))
            //{
            //    //query = query.Where
            //    //       (x =>
            //    //       // x.Customer.FullName.ToLower().Contains(filters.SearchText.ToLower()) ||
            //    //        x.AccountNumber.ToLower().Contains(filters.SearchText.ToLower()) // ||
            //                                                                             //x.PhoneNumber.ToLower().Contains(filters.SearchText.ToLower()) ||
            //                                                                             // x.EmailAddress.ToLower().Contains(filters.SearchText.ToLower()) ||
            //                                                                             //  x.ClientId.ToLower().Contains(filters.SearchText.ToLower())
            //           );
            //}

            if (filters.SortOrder == SortOrder.Ascending)
                query = query.OrderBy(x => x.CreationTime);
            else
                query = query.OrderByDescending(x => x.CreationTime);

            var results = await query.ToListAsync();

            res.TotalCount = results.Count();
            res.Items = results.Skip(filters.SkipCount).Take(filters.MaxResultCount).ToList();
            return res;
        }

        public async Task<CaseFieldImport> UpdateAsync(CaseFieldImport input)
        {
            return await _repository.UpdateAsync(input);
        }
    }
}
