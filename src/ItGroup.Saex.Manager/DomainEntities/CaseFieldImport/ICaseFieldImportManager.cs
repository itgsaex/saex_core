﻿using Abp.Application.Services.Dto;
using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public interface ICaseFieldImportManager : IDomainService
    {
        Task<CaseFieldImport> CreateAsync(CaseFieldImport input);

        Task DeleteAsync(CaseFieldImport input);

        IQueryable<CaseFieldImport> GetAll();

        Task<CaseFieldImport> GetAsync(Guid id);

        Task<PagedResultDto<CaseFieldImport>> GetFiltered(CaseFieldImportSearchModel filters);

        Task<CaseFieldImport> UpdateAsync(CaseFieldImport input);
    }
}
