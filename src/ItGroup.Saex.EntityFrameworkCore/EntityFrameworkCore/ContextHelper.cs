﻿using System;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using ItGroup.Saex.EntityFrameworkCore.Seed.Host;
using ItGroup.Saex.EntityFrameworkCore.Seed.Tenants;

namespace ItGroup.Saex.EntityFrameworkCore.Seed
{
    public static class ContextHelper
    {
        public static SaexDbContext Context { get; set; }

        public static void InitContext(IIocResolver iocResolver)
        {
            WithDbContext<SaexDbContext>(iocResolver, Init);
        }

        public static void SeedHostDb(IIocResolver iocResolver)
        {
            WithDbContext<SaexDbContext>(iocResolver, SeedHostDb);
        }

        public static void SeedHostDb(SaexDbContext context)
        {
            context.SuppressAutoSetTenantId = true;

            // Host seed
            new InitialHostDbBuilder(context).Create();

            // Default tenant seed (in host database).
            new DefaultTenantBuilder(context).Create();
            new TenantRoleAndUserBuilder(context, 1).Create();
        }

        private static void Init(SaexDbContext context)
        {
            Context = context;
        }

        private static void WithDbContext<TDbContext>(IIocResolver iocResolver, Action<TDbContext> contextAction)
            where TDbContext : DbContext
        {
            using (var uowManager = iocResolver.ResolveAsDisposable<IUnitOfWorkManager>())
            {
                using (var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress))
                {
                    var context = uowManager.Object.Current.GetDbContext<TDbContext>(MultiTenancySides.Host);

                    contextAction(context);

                    uow.Complete();
                }
            }
        }
    }
}
