﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ItGroup.Saex.Configuration;
using ItGroup.Saex.Web;

namespace ItGroup.Saex.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class SaexDbContextFactory : IDesignTimeDbContextFactory<SaexDbContext>
    {
        public SaexDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SaexDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            SaexDbContextConfigurer.Configure(builder, configuration.GetConnectionString(SaexConsts.ConnectionStringName));

            return new SaexDbContext(builder.Options);
        }
    }
}
