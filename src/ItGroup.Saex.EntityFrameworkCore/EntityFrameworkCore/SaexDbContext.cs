﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ItGroup.Saex.Authorization.Roles;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.MultiTenancy;
using ItGroup.Saex.DomainEntities;
using Microsoft.EntityFrameworkCore.Internal;

namespace ItGroup.Saex.EntityFrameworkCore
{
    public class SaexDbContext : AbpZeroDbContext<Tenant, Role, User, SaexDbContext>
    {
        /* Define a DbSet for each entity of the application */

        public SaexDbContext(DbContextOptions<SaexDbContext> options)
            : base(options)
        {
        }

        public DbSet<ActivityCode> ActivityCodes { get; set; }

        public DbSet<SessionLog> SessionLogs { get; set; }

        public DbSet<ActivityCodeDetail> ActivityCodeDetails { get; set; }

        public DbSet<SortCase> SortCases { get; set; }

        public DbSet<vwAgent> vwAgent { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<AgentGPS> AgentGPSs { get; set; }

        public DbSet<SortFieldsCase> SortFieldsCases { get; set; }

        public DbSet<AdjusterState> AdjusterStates { get; set; }

        public DbSet<AgentActivity> AgentActivities { get; set; }

        public DbSet<AgentCase> AgentCases { get; set; }

        public DbSet<AgentRegion> AgentRegions { get; set; }

        public DbSet<AgentRoute> AgentRoutes { get; set; }

        public DbSet<Agent> Agents { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Bucket> Buckets { get; set; }

        public DbSet<DemographicFieldAnswer> DemographicFieldAnswers { get; set; }

        public DbSet<CaseActivity> CaseActivities { get; set; }

        public DbSet<CaseActivityCode> CaseActivityCodes { get; set; }

        public DbSet<CaseDelinquencyState> CaseDelinquencyStates { get; set; }

        public DbSet<CaseEvent> CaseEvents { get; set; }

        public DbSet<CaseFieldImport> CaseFieldImports { get; set; }

        public DbSet<CasePayment> CasePayments { get; set; }

        public DbSet<Case> Cases { get; set; }

        public DbSet<CaseSystemState> CaseSystemStates { get; set; }

        public DbSet<CaseTactic> CaseTactics { get; set; }

        public DbSet<CaseType> CaseTypes { get; set; }

        public DbSet<ClosingDate> ClosingDates { get; set; }

        public DbSet<Condition> Conditions { get; set; }

        public DbSet<ContactType> ContactTypes { get; set; }

        public DbSet<DelinquencyState> DelinquencyStates { get; set; }

        public DbSet<AuditLog> DomainEntitiesAudits { get; set; }

        public DbSet<ListFieldCategory> ListFieldCategories { get; set; }

        public DbSet<ListField> ListFields { get; set; }

        public DbSet<List> Lists { get; set; }

        public DbSet<MacroState> MacroStates { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<PaymentDelayReason> PaymentDelayReasons { get; set; }

        public DbSet<PostalCode> PostalCodes { get; set; }

        public DbSet<ProductRule> ProductRules { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<RegionRoute> RegionRoutes { get; set; }

        public DbSet<Region> Regions { get; set; }

        public DbSet<RoutePostalCode> RoutePostalCodes { get; set; }

        public DbSet<ConditionCase> ConditionCases { get; set; }

        public DbSet<RouteProduct> RouteProducts { get; set; }

        public DbSet<Route> Routes { get; set; }

        public DbSet<ScorecardParameter> ScorecardParameters { get; set; }

        public DbSet<SystemPolicy> SystemPolicies { get; set; }

        public DbSet<SystemState> SystemStates { get; set; }

        public DbSet<SystemSubState> SystemSubStates { get; set; }

        public DbSet<VisitPlace> VisitPlaces { get; set; }

        public DbSet<WorkingHour> WorkingHours { get; set; }

        public DbSet<DemographicFieldType> DemographicFieldTypes { get; set; }

        public DbSet<DemographicField> DemographicFields { get; set; }

        public DbSet<Alert> Alerts { get; set; }

        public DbSet<CaseAlert> CaseAlerts { get; set; }

        public DbSet<KPI> KPIs { get; set; }

        public DbSet<EstadoRiesgo> EstadoRiesgos { get; set; }

        public DbSet<vwCases> vwCases { get; set; }

        public DbSet<ActivityDetail> ActivityDetails { get; set; }

        public DbSet<ActivityLine> ActivityLines { get; set; }

        public DbSet<CaseBaseline> CaseBaseline { get; set; }

        public DbSet<ActivityReactionReasonDetail> ActivityReactionReasonDetails { get; set; }

        public DbSet<AgentList> AgentLists { get; set; }
        
        public DbSet<QueryFields> QueryFields { get; set; }

        public DbSet<SupervisorInternalAgent> SupervisorInternalAgents { get; set; }

        public DbSet<InternalAgentExternalAgent> InternalAgentExternalAgents { get; set; }

        public DbSet<ItGroup.Saex.DomainEntities.ProductSystem.ProductSystem> ProductSystems { get; set; }

        public DbSet<AgentTransfer> AgentTransfers { get; set; }

        public DbSet<InternalAgentComment> InternalAgentComments { get; set; }

        public DbSet<InternalAgentPromise> InternalAgentPromises { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Many to Many relationship definitions

            modelBuilder.Entity<RoutePostalCode>()
               .HasKey(x => new { x.RouteId, x.PostalCodeId });

            modelBuilder.Entity<RoutePostalCode>()
                .HasOne(x => x.Route)
                .WithMany(x => x.PostalCodes)
                .HasForeignKey(x => x.RouteId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<RoutePostalCode>()
                .HasOne(x => x.PostalCode)
                .WithMany(x => x.Routes)
                .HasForeignKey(x => x.PostalCodeId);

            modelBuilder.Entity<RouteProduct>()
               .HasKey(x => new { x.RouteId, x.ProductId });

            modelBuilder.Entity<RouteProduct>()
                .HasOne(x => x.Route)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.RouteId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<RouteProduct>()
                .HasOne(x => x.Product)
                .WithMany(x => x.Routes)
                .HasForeignKey(x => x.ProductId);

            modelBuilder.Entity<AgentRoute>()
               .HasKey(x => new { x.RouteId, x.AgentId });

            modelBuilder.Entity<AgentRoute>()
                .HasOne(x => x.Route)
                .WithMany(x => x.Agents)
                .HasForeignKey(x => x.RouteId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<AgentRoute>()
                .HasOne(x => x.Agent)
                .WithMany(x => x.Routes)
                .HasForeignKey(x => x.AgentId);

            modelBuilder.Entity<CaseActivityCode>()
               .HasKey(x => new { x.CaseActivityId, x.ActivityCodeId });

            modelBuilder.Entity<CaseActivityCode>()
                .HasOne(x => x.CaseActivity)
                .WithMany(x => x.ActivityCodes)
                .HasForeignKey(x => x.CaseActivityId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<CaseActivityCode>()
                .HasOne(x => x.ActivityCode)
                .WithMany(x => x.CaseActivities)
                .HasForeignKey(x => x.ActivityCodeId);

            modelBuilder.Entity<AgentCase>()
               .HasKey(x => new { x.CaseId, x.AgentId });

            modelBuilder.Entity<AgentCase>()
                .HasOne(x => x.Agent)
                .WithMany(x => x.Cases)
                .HasForeignKey(x => x.AgentId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<AgentCase>()
                .HasOne(x => x.Case)
                .WithMany(x => x.Agents)
                .HasForeignKey(x => x.CaseId);

            modelBuilder.Entity<AgentRegion>()
               .HasKey(x => new { x.RegionId, x.AgentId });

            modelBuilder.Entity<AgentRegion>()
                .HasOne(x => x.Agent)
                .WithMany(x => x.Regions)
                .HasForeignKey(x => x.AgentId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<AgentRegion>()
                .HasOne(x => x.Region)
                .WithMany(x => x.Agents)
                .HasForeignKey(x => x.RegionId);

            modelBuilder.Entity<AgentList>()
               .HasKey(x => new { x.ListId, x.AgentId });

            modelBuilder.Entity<AgentList>()
                .HasOne(x => x.Agent)
                .WithMany(x => x.Lists)
                .HasForeignKey(x => x.AgentId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<AgentList>()
                .HasOne(x => x.List)
                .WithMany(x => x.Agents)
                .HasForeignKey(x => x.ListId);

            modelBuilder.Entity<RegionRoute>()
               .HasKey(x => new { x.RegionId, x.RouteId });

            modelBuilder.Entity<RegionRoute>()
                .HasOne(x => x.Route)
                .WithMany(x => x.Regions)
                .HasForeignKey(x => x.RouteId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<RegionRoute>()
                .HasOne(x => x.Region)
                .WithMany(x => x.Routes)
                .HasForeignKey(x => x.RegionId);

            modelBuilder.Entity<DemographicFieldType>()
               .HasKey(x => x.Id);

            modelBuilder.Entity<DemographicFieldType>()
               .HasIndex(x => x.Name).IsUnique();

            modelBuilder.Entity<DemographicField>()
               .HasKey(x => x.Id);

            modelBuilder.Entity<DemographicField>()
               .HasIndex(x => x.Name).IsUnique();

            modelBuilder.Entity<SupervisorInternalAgent>()
             .HasKey(x => new { x.SupervisorId, x.InternalAgentId, x.Id });

            modelBuilder.Entity<SupervisorInternalAgent>()
                .HasOne(x => x.Supervisor)
                .WithMany(x => x.SupervisorInternalAgents)
                .HasForeignKey(x => x.SupervisorId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<SupervisorInternalAgent>()
               .HasOne(x => x.InternaleAgent)
               .WithMany(x => x.InternalAgentSupervisors)
               .HasForeignKey(x => x.InternalAgentId)
               .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<InternalAgentExternalAgent>()
            .HasKey(x => new { x.InternalAgentId, x.ExternalAgentId, x.Id });

            modelBuilder.Entity<InternalAgentExternalAgent>()
                .HasOne(x => x.InternaleAgent)
                .WithMany(x => x.InternalAgentExternalAgents)
                .HasForeignKey(x => x.InternalAgentId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<InternalAgentExternalAgent>()
               .HasOne(x => x.ExternalAgent)
               .WithMany(x => x.ExternalAgentInternalAgents)
               .HasForeignKey(x => x.ExternalAgentId)
               .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<AgentTransfer>()
              .HasKey(x => new { x.Id });

            modelBuilder.Entity<AgentTransfer>()
                .HasOne(x => x.FromAgent)
                .WithMany(x => x.FromTransfers)
                .HasForeignKey(x => x.FromAgentId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<AgentTransfer>()
                .HasOne(x => x.ToAgent)
                .WithMany(x => x.ToTransfers)
                .HasForeignKey(x => x.ToAgentId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<AgentTransfer>()
                .HasOne(x => x.Case)
                .WithMany(x => x.CaseTransfers)
                .HasForeignKey(x => x.CaseId)
                .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<InternalAgentComment>()
            .HasKey(x => new { x.Id });

            modelBuilder.Entity<InternalAgentComment>()
           .HasOne(x => x.InternalAgent)
           .WithMany(x => x.InternalAgentComments)
           .HasForeignKey(x => x.InternalAgentId)
           .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<InternalAgentComment>()
           .HasOne(x => x.Case)
           .WithMany(x => x.InternalAgentComments)
           .HasForeignKey(x => x.CaseID)
           .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<InternalAgentPromise>()
            .HasKey(x => new { x.Id });

            modelBuilder.Entity<InternalAgentPromise>()
           .HasOne(x => x.InternalAgent)
           .WithMany(x => x.InternalAgentPromises)
           .HasForeignKey(x => x.InternalAgentId)
           .OnDelete(deleteBehavior: DeleteBehavior.Restrict);

            modelBuilder.Entity<InternalAgentPromise>()
           .HasOne(x => x.Case)
           .WithMany(x => x.InternalAgentPromises)
           .HasForeignKey(x => x.CaseID)
           .OnDelete(deleteBehavior: DeleteBehavior.Restrict);
        }
    }
}
