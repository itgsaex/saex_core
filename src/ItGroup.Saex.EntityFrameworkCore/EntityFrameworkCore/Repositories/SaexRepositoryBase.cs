﻿using Abp.Data;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.EntityFrameworkCore.Repositories
{
    /// <summary>
    /// Base class for custom repositories of the application.
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    /// <typeparam name="TPrimaryKey">Primary key type of the entity</typeparam>
    public abstract class SaexRepositoryBase<TEntity, TPrimaryKey> : EfCoreRepositoryBase<SaexDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected SaexRepositoryBase(IDbContextProvider<SaexDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        // Add your common methods for all repositories
    }

    /// <summary>
    /// Base class for custom repositories of the application.
    /// This is a shortcut of <see cref="SaexRepositoryBase{TEntity,TPrimaryKey}"/> for <see cref="int"/> primary key.
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    public abstract class SaexRepositoryBase<TEntity> : SaexRepositoryBase<TEntity, int>, IRepository<TEntity>
        where TEntity : class, IEntity<int>
    {
        protected SaexRepositoryBase(IDbContextProvider<SaexDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        // Do not add any method here, add to the class above (since this inherits it)!!!
    }

    public class CaseRecentActivityRepository : SaexRepositoryBase<CaseRecentActivity, Guid>, ICaseRecentActivityRepository
    {
        private readonly IActiveTransactionProvider _transactionProvider;

        public CaseRecentActivityRepository(IDbContextProvider<SaexDbContext> dbContextProvider, IActiveTransactionProvider transactionProvider)
            : base(dbContextProvider)
        {
            _transactionProvider = transactionProvider;
        }

        private DbCommand CreateCommand(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var command = Context.Database.GetDbConnection().CreateCommand(); //new SqlCommand(commandText, new SqlConnection(Context.Database.GetDbConnection().ConnectionString));
            command.CommandText = commandText;
            command.CommandType = commandType;
            command.CommandTimeout = 0;
            command.Transaction = GetActiveTransaction();

            foreach (var parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }

            return command;
        }

        private async Task EnsureConnectionOpenAsync()
        {
            var connection = Context.Database.GetDbConnection();

            if (connection.State != ConnectionState.Open)
            {
                await connection.OpenAsync();
            }
        }

        private DbTransaction GetActiveTransaction()
        {
            return (DbTransaction)_transactionProvider.GetActiveTransaction(new ActiveTransactionProviderArgs {
                {"ContextType", typeof(SaexDbContext) },
                {"MultiTenancySide", MultiTenancySide }
            });
        }

        public async Task<List<CaseRecentActivity>> GetRecentActivities(string caseNumber)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@caseNumber", caseNumber));

            using (var command = CreateCommand("GetRecentCaseActivities", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<CaseRecentActivity>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new CaseRecentActivity();
                            item.Id = Guid.Parse(dataReader["Id"].ToString());
                            item.CreationTime = DateTime.Parse(dataReader["CreationTime"].ToString());
                            item.ActionCode = dataReader["ActionCode"].ToString();
                            item.ActionDescription = dataReader["ActionDescription"].ToString();
                            item.ReactionCode = dataReader["ReactionCode"].ToString();
                            item.ReactionDescription = dataReader["ReactionDescription"].ToString();
                            item.ReasonCode = dataReader["ReasonCode"].ToString();
                            item.ReasonDescription = dataReader["ReasonDescription"].ToString();
                            item.Comment = dataReader["Comment"].ToString();

                            result.Add(item);
                        }
                    }

                    dataReader.Close();

                    return result;
                }
            }
        }

        public async Task<List<CaseReportModel>> GetCases(CaseReportSearchModel model)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();

            if (model.AgentId.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@agent", model.AgentId));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@agent", DBNull.Value));
            }

            if (model.RegionId.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@region", model.RegionId));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@region", DBNull.Value));
            }

            if (model.RouteId.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@route", model.RouteId));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@route", DBNull.Value));
            }

            if (model.PostalCodeId.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@zipcode", model.PostalCodeId));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@zipcode", DBNull.Value));
            }

            if (!string.IsNullOrWhiteSpace(model.AccountNumber))
            {
                sqlParameters.Add(new SqlParameter("@accountNumber", model.AccountNumber));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@accountNumber", DBNull.Value));
            }

            if (!string.IsNullOrWhiteSpace(model.Name))
            {
                sqlParameters.Add(new SqlParameter("@name", model.Name));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@name", DBNull.Value));
            }

            if (!string.IsNullOrWhiteSpace(model.DelinquencyStateId))
            {
                sqlParameters.Add(new SqlParameter("@delinquencyState", model.DelinquencyStateId));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@delinquencyState", DBNull.Value));
            }

            if (!string.IsNullOrWhiteSpace(model.AssignType))
            {
                sqlParameters.Add(new SqlParameter("@tipoAsignacion", model.AssignType));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@tipoAsignacion", DBNull.Value));
            }

            using (var command = CreateCommand("ReadCases", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<CaseReportModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new CaseReportModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "CaseAssignType":
                                        item.CaseAssignType = dataReader["CaseAssignType"].ToString();
                                        break;
                                    case "CasoID":
                                        item.CasoID = dataReader["CasoID"].ToString();
                                        break;
                                    case "ComentarioExterno":
                                        item.ComentarioExterno = dataReader["ComentarioExterno"].ToString();
                                        break;
                                    case "ComentarioInterno":
                                        item.ComentarioInterno = dataReader["ComentarioInterno"].ToString();
                                        break;
                                    case "DiasVencimiento":
                                        item.DiasVencimiento = int.Parse(dataReader["DiasVencimiento"].ToString());
                                        break;
                                    case "EstatusSistema":
                                        item.EstatusSistema = dataReader["EstatusSistema"].ToString();
                                        break;
                                    case "EtapasDeDelicuencia":
                                        item.EtapasDelincuencia = dataReader["EtapasDeDelicuencia"].ToString();
                                        break;
                                    case "FechaEntradaCACS":
                                        item.FechaEntradaCACS = dataReader["FechaEntradaCACS"].ToString();
                                        break;
                                    case "LastActivityCode":
                                        item.LastActivityCode = dataReader["LastActivityCode"].ToString();
                                        break;
                                    case "LastPaymentDate":
                                        item.LastPaymentDate = dataReader["LastPaymentDate"].ToString();
                                        break;
                                    case "MontoVencido":
                                        item.MontoVencido = decimal.Parse(dataReader["MontoVencido"].ToString());
                                        break;
                                    case "Nombre":
                                        item.Nombre = dataReader["Nombre"].ToString();
                                        break;
                                    case "NumeroCuenta":
                                        item.NumeroCuenta = "\u200C" + dataReader["NumeroCuenta"].ToString();
                                        break;
                                    case "Obligatorio":
                                        item.Obligatorio = Convert.ToBoolean(dataReader["Obligatorio"]);
                                        break;
                                    case "PaymentAmount":
                                        item.PaymentAmount = dataReader["PaymentAmount"].ToString();
                                        break;
                                    case "ProductNumber":
                                        item.ProductNumber = dataReader["ProductNumber"].ToString();
                                        break;
                                    case "Ruta":
                                        item.Ruta = dataReader["Ruta"].ToString();
                                        break;
                                    case "StateCode":
                                        item.StateCode = dataReader["StateCode"].ToString();
                                        break;
                                    case "TotalDELQAmount":
                                        item.TotalDELQAmount = dataReader["TotalDELQAmount"].ToString();
                                        break;
                                    case "ZipCode":
                                        item.ZipCode = dataReader["ZipCode"].ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();
                    return result;
                }
            }
        }

        public async Task<List<InterfacelogModel>> GetLastRunInterfaceLog()
        {
            await EnsureConnectionOpenAsync();

            using (var command = CreateCommand("ReadLastRunInterfaceLog", CommandType.StoredProcedure))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<InterfacelogModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new InterfacelogModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "compid":
                                        item.Compid = int.Parse(dataReader["compid"].ToString());
                                        break;
                                    case "route":
                                        item.Route = dataReader["route"].ToString();
                                        break;
                                    case "interfacename":
                                        item.InterfaceName = dataReader["interfacename"].ToString();
                                        break;
                                    case "tablename":
                                        item.TableName = dataReader["tablename"].ToString();
                                        break;
                                    case "begend":
                                        item.BegEnd = dataReader["begend"].ToString();
                                        break;
                                    case "interfacedate":
                                        item.InterfaceDate = DateTime.Parse(dataReader["interfacedate"].ToString());
                                        break;
                                    case "processstatus":
                                        item.ProcessStatus = int.Parse(dataReader["processstatus"].ToString());
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();
                    return result;
                }
            }
        }

        public async Task<List<ErrorlogModel>> GetErrorLog()
        {
            await EnsureConnectionOpenAsync();

            using (var command = CreateCommand("ReadErrorlog", CommandType.StoredProcedure))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<ErrorlogModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new ErrorlogModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "errordescription":
                                        item.ErrorDescription = dataReader["errordescription"].ToString();
                                        break;
                                    case "errordate":
                                        item.ErrorDate = DateTime.Parse(dataReader["errordate"].ToString());
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();
                    return result;
                }
            }
        }

        public Task<int> ExecuteCaseProcessing()
        {
            int result = 0;
            var command = CreateCommand("ExecuteCaseProcessing", CommandType.StoredProcedure);

            try
            {
                command.CommandTimeout = 0;
                command.Connection.Open();
                result = command.ExecuteNonQuery();
                command.Connection.Close();
                return Task.FromResult(result);
            } 
            catch(Exception e)
            {
                command.Connection.Close();
                return Task.FromResult(result);
            }
        }

        public void SetCaseMandatory(string caseNumber, string newValue)
        {
            var sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@caseNumber", caseNumber));
            sqlParameters.Add(new SqlParameter("@newValue", newValue));

            using (var command = CreateCommand("SetCaseMandatory", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                command.ExecuteNonQuery();
            }
        }

        public async Task<List<CaseFieldHelper>> ReadCaseFields(Guid? product)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();
            if (product != null) sqlParameters.Add(new SqlParameter("@product", product));

            var queryFields = new List<QueryFields>();

            using (var command = CreateCommand("SELECT * FROM QueryFields", CommandType.Text))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new QueryFields();
                            item.Id = Guid.Parse(dataReader["Id"].ToString());
                            item.FieldId = int.Parse(dataReader["FieldId"].ToString());
                            item.MasterList = Convert.ToBoolean(dataReader["MasterList"]);
                            item.ChildList = Convert.ToBoolean(dataReader["ChildList"]);
                            item.RiskList = Convert.ToBoolean(dataReader["RiskList"]);

                            queryFields.Add(item);
                        }
                    }

                    dataReader.Close();
                }
            }

            using (var command = CreateCommand("ReadCaseFields", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<CaseFieldHelper>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new CaseFieldHelper();
                            item.FieldId = int.Parse(dataReader["FieldId"].ToString());
                            item.ColumnName = dataReader["ColumnName"].ToString();
                            item.HumanName = dataReader["HumanName"].ToString();
                            item.CaseTypeDescription = dataReader["CaseTypeDescription"].ToString();
                            item.CategoriaId = int.Parse(dataReader["CategoriaID"].ToString());
                            item.Sequence = int.Parse(dataReader["Sequence"].ToString());
                            item.TargetColumn = dataReader["TargetColumn"].ToString();
                            item.SourceTargetType = dataReader["SourceTargetType"].ToString();

                            var queryField = queryFields.Find(x => x.FieldId == item.FieldId);
                            if (queryField != null)
                            {
                                item.Selected = true;
                                item.MasterList = queryField.MasterList;
                                item.ChildList = queryField.ChildList;
                                item.RiskList = queryField.RiskList;
                            }
                            else
                            {
                                item.Selected = false;
                                item.MasterList = false;
                                item.ChildList = false;
                                item.RiskList = false;
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();

                    return result;
                }
            }
        }

        public void SaveCaseFields(List<CaseFieldHelper> data)
        {
            var jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(data);

            var sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@data", jsonData));

            using (var command = CreateCommand("SaveCaseFields", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                command.ExecuteNonQuery();
            }
        }

        public async Task<List<TestQueryHelper>> TestQuery(ListType listType, string tableName, string whereClause, string fieldsToReturn, string fieldsToPivot, string temporaryTable)
        {
            await EnsureConnectionOpenAsync();

            var list = 0;
            switch (listType)
            {
                case ListType.Master:
                    list = 1;
                    break;
                case ListType.Child:
                    list = 2;
                    break;
                case ListType.Risk:
                    list = 3;
                    break;
                default:
                    break;
            }

            var sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@listType", list));
            if (tableName != null) sqlParameters.Add(new SqlParameter("@tableName", tableName));
            if (whereClause != null) sqlParameters.Add(new SqlParameter("@whereClause", whereClause));
            if (fieldsToReturn != null) sqlParameters.Add(new SqlParameter("@fieldsToReturn", fieldsToReturn));
            if (fieldsToPivot != null) sqlParameters.Add(new SqlParameter("@fieldsToPivot", fieldsToPivot));
            if (temporaryTable != null) sqlParameters.Add(new SqlParameter("@temporaryTable", temporaryTable));

            using (var command = CreateCommand("TestQuery", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<TestQueryHelper>();

                    if (dataReader.HasRows)
                    {

                        if (listType == ListType.Master)
                        {
                            while (dataReader.Read())
                            {
                                var item = new TestQueryHelper();

                                item.AccountNumber = dataReader["AccountNumber"].ToString();
                                item.ProductNumber = dataReader["ProductNumber"].ToString();
                                item.ZipCode = dataReader["ZipCode"].ToString();
                                item.CustomerName = dataReader["CustomerName"].ToString();

                                result.Add(item);
                            }
                        }
                        else
                        {
                            while (dataReader.Read())
                            {
                                var item = new TestQueryHelper();

                                item.AccountNumber = dataReader["NumeroCuenta"].ToString();
                                item.ProductNumber = dataReader["ProductNumber"].ToString();
                                item.ZipCode = dataReader["ZipCode"].ToString();
                                item.CustomerName = dataReader["Nombre"].ToString();

                                result.Add(item);
                            }
                        }
                    }

                    dataReader.Close();
                    return result;
                }
            }
        }
        public async Task<List<InternalAgentDashboardModel>> ReadInternalAgentCases(ReadInternalAgentCasesParametersModel model)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();

            if (model.Supervisor.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@supervisor", model.Supervisor.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@supervisor", DBNull.Value));
            }

            if (model.InternalAgent.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@internalAgent", model.InternalAgent.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@internalAgent", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(model.InternalAgentUsername))
            {
                sqlParameters.Add(new SqlParameter("@internalAgentUsername", model.InternalAgentUsername));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@internalAgentUsername", DBNull.Value));
            }

            if (model.ExternalAgent.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@externalAgent", model.ExternalAgent.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@externalAgent", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(model.ExternalAgentUsername))
            {
                sqlParameters.Add(new SqlParameter("@externalAgentUsername", model.ExternalAgentUsername));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@externalAgentUsername", DBNull.Value));
            }


            if (model.RegionId.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@regionid", model.RegionId.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@regionid", DBNull.Value));
            }

            if (model.RutaId.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@rutaid", model.RutaId.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@rutaid", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(model.CodigoPostal))
            {
                sqlParameters.Add(new SqlParameter("@codigoPostal", model.CodigoPostal));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@codigoPostal", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(model.ActivityCode))
            {
                sqlParameters.Add(new SqlParameter("@activityCode", model.ActivityCode));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@activityCode", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(model.EtapaDeDelincuencia))
            {
                sqlParameters.Add(new SqlParameter("@etapaDeDelicuencia", model.EtapaDeDelincuencia));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@etapaDeDelicuencia", DBNull.Value));
            }

            if (model.AlertId.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@alertaid", model.AlertId.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@alertaid", DBNull.Value));
            }

            if (model.Referido.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@referido", model.Referido.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@referido", DBNull.Value));
            }

            if (model.IsWorked.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@isWorked", model.IsWorked.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@isWorked", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(model.AccountNumber))
            {
                sqlParameters.Add(new SqlParameter("@numeroCuenta", model.AccountNumber));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@numeroCuenta", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(model.Name))
            {
                sqlParameters.Add(new SqlParameter("@nombre", model.Name));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@nombre", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(model.Estado))
            {
                sqlParameters.Add(new SqlParameter("@estado", model.Estado));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@estado", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(model.Status))
            {
                sqlParameters.Add(new SqlParameter("@status", model.Status));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@status", DBNull.Value));
            }


            using (var command = CreateCommand("ReadInternalAgentCases", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<InternalAgentDashboardModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new InternalAgentDashboardModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "CaseID":
                                        item.CaseID = dataReader["CaseID"].ToString();
                                        break;
                                    case "V1CaseID":
                                        item.V1CaseID = dataReader["V1CaseID"].ToString();
                                        break;
                                    case "ProductNumber":
                                        item.ProductNumber = dataReader["ProductNumber"].ToString();
                                        break;
                                    case "AccountNumber":
                                        item.AccountNumber = "\u200C" + dataReader["AccountNumber"].ToString();
                                        break;
                                    case "Fullname":
                                        item.Fullname = dataReader["Fullname"].ToString();
                                        break;
                                    case "ZipCode":
                                        item.ZipCode = dataReader["ZipCode"].ToString();
                                        break;
                                    case "City":
                                        item.City = dataReader["City"].ToString();
                                        break;
                                    case "OwedAmount":
                                        if (string.IsNullOrWhiteSpace(dataReader["OwedAmount"].ToString()))
                                            item.OwedAmount = 0.00M;
                                        else
                                            item.OwedAmount = decimal.Parse(dataReader["OwedAmount"].ToString());
                                        break;
                                    case "EtapasDeDelicuencia":
                                        item.EtapasDeDelicuencia = dataReader["EtapasDeDelicuencia"].ToString();
                                        break;
                                    case "LastActivityCode":
                                        item.LastActivityCode = dataReader["LastActivityCode"].ToString();
                                        break;
                                    case "LastActivityCodeComment":
                                        item.LastActivityCodeComment = dataReader["LastActivityCodeComment"].ToString();
                                        break;
                                    case "ComentarioExterno":
                                        item.ComentarioExterno = dataReader["ComentarioExterno"].ToString();
                                        break;
                                    case "NewComment":
                                        item.NewComment = dataReader["NewComment"].ToString();
                                        break;
                                    case "EstatusSistema":
                                        item.EstatusSistema = dataReader["EstatusSistema"].ToString();
                                        break;
                                    case "FechaEntradaCACS":
                                        item.FechaEntradaCACS = dataReader["FechaEntradaCACS"].ToString();
                                        break;
                                    case "StateCode":
                                        item.StateCode = dataReader["StateCode"].ToString();
                                        break;
                                    case "Alerta":
                                        item.Alerta = dataReader["Alerta"].ToString();
                                        break;
                                    case "DiasVencimiento":
                                        if (string.IsNullOrWhiteSpace(dataReader["DiasVencimiento"].ToString()))
                                            item.DiasVencimiento = 0;
                                        else
                                            item.DiasVencimiento = int.Parse(dataReader["DiasVencimiento"].ToString());
                                        break;
                                    case "Ruta":
                                        item.Ruta = dataReader["Ruta"].ToString();
                                        break;
                                    case "TotalDELQAmount":
                                        if (string.IsNullOrWhiteSpace(dataReader["TotalDELQAmount"].ToString()))
                                            item.TotalDELQAmount = 0.00M;
                                        else
                                            item.TotalDELQAmount = decimal.Parse(dataReader["TotalDELQAmount"].ToString());
                                        break;
                                    case "PaymentAmount":
                                        if (string.IsNullOrWhiteSpace(dataReader["PaymentAmount"].ToString()))
                                            item.PaymentAmount = 0.00M;
                                        else
                                            item.PaymentAmount = decimal.Parse(dataReader["PaymentAmount"].ToString());
                                        break;
                                    case "TipoPromesa":
                                        item.TipoPromesa = dataReader["TipoPromesa"].ToString();
                                        break;
                                    case "LastPaymentDate":
                                        item.LastPaymentDate = dataReader["LastPaymentDate"].ToString();
                                        break;
                                    case "UltimoComentario":
                                        item.UltimoComentario = dataReader["UltimoComentario"].ToString();
                                        break;
                                    case "EsTrabajada":
                                        item.EsTrabajada = bool.Parse(dataReader["EsTrabajada"].ToString());
                                        break;
                                    case "Comentario":
                                        item.Comentario = dataReader["Comentario"].ToString();
                                        break;
                                    case "FechaComentario":
                                        item.FechaComentario = dataReader["FechaComentario"].ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();
                    return result;
                }
            }
        }

        public async Task<List<DailyReportModel>> DailyReport(DailyReportParameterModel model)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();

            sqlParameters.Add(new SqlParameter("@externalAgent", model.ExternalAgent));
            sqlParameters.Add(new SqlParameter("@fromDate", model.FromDate));
            sqlParameters.Add(new SqlParameter("@toDate", model.ToDate));

            using (var command = CreateCommand("SupervisorDailyReport", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<DailyReportModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new DailyReportModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "Fecha":
                                        item.Fecha = DateTime.Parse(dataReader["Fecha"].ToString());
                                        break;
                                    case "ActivityCode":
                                        item.ActivityCode = dataReader["ActivityCode"].ToString();
                                        break;
                                    case "ActivityCodeDesc":
                                        item.ActivityCodeDesc = dataReader["ActivityCodeDesc"].ToString();
                                        break;
                                    case "NumeroCuenta":
                                        item.NumeroCuenta = dataReader["NumeroCuenta"].ToString();
                                        break;
                                    case "Nombre":
                                        item.Nombre = dataReader["Nombre"].ToString();
                                        break;
                                    case "ProductNumber":
                                        item.ProductNumber = dataReader["ProductNumber"].ToString();
                                        break;
                                    case "MontoVencido":
                                        item.MontoVencido = Decimal.Parse(dataReader["MontoVencido"].ToString());
                                        break;
                                    case "Pueblo":
                                        item.Pueblo = dataReader["Pueblo"].ToString();
                                        break;
                                    case "ZipCode":
                                        item.ZipCode = dataReader["ZipCode"].ToString();
                                        break;
                                    case "DiasVencimiento":
                                        item.DiasVencimiento = int.Parse(dataReader["DiasVencimiento"].ToString());
                                        break;
                                    case "Comentario":
                                        item.Comentario = dataReader["Comentario"].ToString();
                                        break;
                                    case "TipoProducto":
                                        item.TipoProducto = dataReader["TipoProducto"].ToString();
                                        break;
                                    case "TipoContacto":
                                        item.TipoContacto = dataReader["TipoContacto"].ToString();
                                        break;
                                    case "LugarVisitado":
                                        item.LugarVisitado = dataReader["LugarVisitado"].ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();
                    return result;
                }
            }
        }

        public async Task<List<PlannedCaseWitoutVisitsModel>> PlannedCaseWitoutVisits(DailyReportParameterModel model)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();

            sqlParameters.Add(new SqlParameter("@externalAgent", model.ExternalAgent));
            sqlParameters.Add(new SqlParameter("@fromDate", model.FromDate));
            sqlParameters.Add(new SqlParameter("@toDate", model.ToDate));

            using (var command = CreateCommand("GetPlannedCaseWithOutVisits", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<PlannedCaseWitoutVisitsModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new PlannedCaseWitoutVisitsModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "Fecha":
                                        item.Fecha = DateTime.Parse(dataReader["Fecha"].ToString());
                                        break;
                                    case "NumeroCuenta":
                                        item.NumeroCuenta = dataReader["NumeroCuenta"].ToString();
                                        break;
                                    case "Nombre":
                                        item.Nombre = dataReader["Nombre"].ToString();
                                        break;
                                    case "ProductNumber":
                                        item.ProductNumber = dataReader["ProductNumber"].ToString();
                                        break;
                                    case "MontoVencido":
                                        item.MontoVencido = Decimal.Parse(dataReader["MontoVencido"].ToString());
                                        break;
                                    case "Pueblo":
                                        item.Pueblo = dataReader["Pueblo"].ToString();
                                        break;
                                    case "ZipCode":
                                        item.ZipCode = dataReader["ZipCode"].ToString();
                                        break;
                                    case "DiasVencimiento":
                                        item.DiasVencimiento = int.Parse(dataReader["DiasVencimiento"].ToString());
                                        break;
                                    case "TipoProducto":
                                        item.TipoProducto = dataReader["TipoProducto"].ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();

                    return result;
                }
            }
        }

        public async Task<List<ProyeccionPerdidaModel>> ProyeccionPerdidas(ProyeccionPerdidaParameterModel model)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();

            sqlParameters.Add(new SqlParameter("@supervisor", model.Supervisor));
            sqlParameters.Add(new SqlParameter("@etapaDelincuencia", model.EtapaDelincuencia));

            using (var command = CreateCommand("ProyeccionPerdidas", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<ProyeccionPerdidaModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new ProyeccionPerdidaModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "Agente":
                                        item.Agente = dataReader["Agente"].ToString();
                                        break;
                                    case "Asignacion":
                                        item.Asignacion = dataReader["Asignacion"].ToString();
                                        break;
                                    case "TotalCasos":
                                        item.TotalCasos = int.Parse(dataReader["TotalCasos"].ToString());
                                        break;
                                    case "BalanceOriginal":
                                        item.BalanceOriginal = Decimal.Parse(dataReader["BalanceOriginal"].ToString());
                                        break;
                                    case "BalanceActual":
                                        item.BalanceActual = Decimal.Parse(dataReader["BalanceActual"].ToString());
                                        break;
                                    case "BalancePagado":
                                        item.BalancePagado = Decimal.Parse(dataReader["BalancePagado"].ToString());
                                        break;
                                    case "PromesasFirmes":
                                        item.PromesasFirmes = Decimal.Parse(dataReader["PromesasFirmes"].ToString());
                                        break;
                                    case "PromesasDudosas":
                                        item.PromesasDudosas = Decimal.Parse(dataReader["PromesasDudosas"].ToString());
                                        break;
                                    case "TotalCasosPromesasFirmes":
                                        item.TotalCasosPromesasFirmes = int.Parse(dataReader["TotalCasosPromesasFirmes"].ToString());
                                        break;
                                    case "TotalCasosPromesasDudosas":
                                        item.TotalCasosPromesasDudosas = int.Parse(dataReader["TotalCasosPromesasDudosas"].ToString());
                                        break;
                                    case "Actual":
                                        item.Actual = Decimal.Parse(dataReader["Actual"].ToString());
                                        break;
                                    case "Real":
                                        item.Real = Decimal.Parse(dataReader["Real"].ToString());
                                        break;
                                    case "MejorEscenario":
                                        item.MejorEscenario = Decimal.Parse(dataReader["MejorEscenario"].ToString());
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();

                    return result;
                }
            }
        }

        public async Task<List<ReadCuentasAsignadasModel>> ReadCuentasAsignadas(ReadCuentasAsignadasParameterModel model)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();

            if (!string.IsNullOrWhiteSpace(model.AgentID))
            {
                sqlParameters.Add(new SqlParameter("@agenteID", model.AgentID));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@agenteID", DBNull.Value));
            }

            if (model.RegionID.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@regionID", model.RegionID.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@regionID", DBNull.Value));
            }

            if (model.RouteID.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@rutaID", model.RouteID.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@rutaID", DBNull.Value));
            }

            sqlParameters.Add(new SqlParameter("@codigoPostal", DBNull.Value));
            sqlParameters.Add(new SqlParameter("@numeroCuenta", DBNull.Value));
            sqlParameters.Add(new SqlParameter("@nombre", DBNull.Value));
            sqlParameters.Add(new SqlParameter("@etapaDeDelicuencia", DBNull.Value));

            using (var command = CreateCommand("ReadCuentasAsignadas", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<ReadCuentasAsignadasModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new ReadCuentasAsignadasModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "ProductNumber":
                                        item.ProductNumber = dataReader["ProductNumber"].ToString();
                                        break;
                                    case "NumeroCuenta":
                                        item.NumeroCuenta = dataReader["NumeroCuenta"].ToString();
                                        break;
                                    case "Nombre":
                                        item.Nombre = dataReader["Nombre"].ToString();
                                        break;
                                    case "ZipCode":
                                        item.ZipCode = dataReader["ZipCode"].ToString();
                                        break;
                                    case "Obligatorio":
                                        item.Obligatorio = bool.Parse(dataReader["Obligatorio"].ToString());
                                        break;
                                    case "CasoID":
                                        item.CasoID = dataReader["CasoID"].ToString();
                                        break;
                                    case "MontoVencido":
                                        item.MontoVencido = decimal.Parse(dataReader["MontoVencido"].ToString()).ToString("C2");
                                        break;
                                    case "Pueblo":
                                        item.Pueblo = dataReader["Pueblo"].ToString();
                                        break;
                                    case "Estatus":
                                        item.Estatus = dataReader["Estatus"].ToString();
                                        break;
                                    case "TipoTransferencia":
                                        item.TipoTransferencia = dataReader["TipoTransferencia"].ToString();
                                        break;
                                    case "FechaAsignacion":
                                        item.FechaAsignacion = DateTime.Parse(dataReader["FechaAsignacion"].ToString()).ToShortDateString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();
                    return result;
                }
            }
        }

        public async Task<List<ReadDuplicateAccountsModel>> ReadDuplicateAccounts(ReadDuplicateAccountsParameterModel model)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();

            if (!string.IsNullOrWhiteSpace(model.AgentID))
            {
                sqlParameters.Add(new SqlParameter("@agenteID", model.AgentID));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@agenteID", DBNull.Value));
            }

            if (model.RegionID.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@regionID", model.RegionID.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@regionID", DBNull.Value));
            }

            if (model.RouteID.HasValue)
            {
                sqlParameters.Add(new SqlParameter("@rutaID", model.RouteID.GetValueOrDefault()));
            }
            else
            {
                sqlParameters.Add(new SqlParameter("@rutaID", DBNull.Value));
            }

            using (var command = CreateCommand("ReadDuplicateAccounts", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<ReadDuplicateAccountsModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new ReadDuplicateAccountsModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "ProductNumber":
                                        item.ProductNumber = dataReader["ProductNumber"].ToString();
                                        break;
                                    case "NumeroCuenta":
                                        item.NumeroCuenta = dataReader["NumeroCuenta"].ToString();
                                        break;
                                    case "Nombre":
                                        item.Nombre = dataReader["Nombre"].ToString();
                                        break;
                                    case "ZipCode":
                                        item.ZipCode = dataReader["ZipCode"].ToString();
                                        break;
                                    case "CasoID":
                                        item.CasoID = dataReader["CasoID"].ToString();
                                        break;
                                    case "MontoVencido":
                                        item.MontoVencido = decimal.Parse(dataReader["MontoVencido"].ToString()).ToString("C2");
                                        break;
                                    case "Pueblo":
                                        item.Pueblo = dataReader["Pueblo"].ToString();
                                        break;
                                    case "Estatus":
                                        item.Estatus = dataReader["Estatus"].ToString();
                                        break;
                                    case "AgenteID":
                                        item.AgenteID = dataReader["AgenteID"].ToString();
                                        break;
                                    case "AgenteNombre":
                                        item.AgenteNombre = dataReader["AgenteNombre"].ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();
                    return result;
                }
            }
        }

        public async Task<List<RutasAsignadasModel>> RutasAsignadas(RutasAsignadasParameterModel model)
        {
            await EnsureConnectionOpenAsync();

            var sqlParameters = new List<SqlParameter>();

            sqlParameters.Add(new SqlParameter("@fromDate", model.FromDate));
            sqlParameters.Add(new SqlParameter("@toDate", model.ToDate));

            using (var command = CreateCommand("RutasAsignadas", CommandType.StoredProcedure, sqlParameters.ToArray()))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<RutasAsignadasModel>();

                    if (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            var item = new RutasAsignadasModel();
                            var columns = dataReader.GetColumnSchema();
                            foreach (var column in columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "NumeroCuenta":
                                        item.NumeroCuenta = dataReader["NumeroCuenta"].ToString();
                                        break;
                                    case "Descripcion":
                                        item.Descripcion = dataReader["Descripcion"].ToString();
                                        break;
                                    case "ZipCode":
                                        item.ZipCode = dataReader["ZipCode"].ToString();
                                        break;
                                    case "RutaActual":
                                        item.RutaActual = dataReader["RutaActual"].ToString();
                                        break;
                                    case "Agente":
                                        item.Agente = dataReader["Agente"].ToString();
                                        break;
                                    case "Estatus":
                                        item.Estatus = dataReader["Estatus"].ToString();
                                        break;
                                    case "TransferOrShared":
                                        item.TransferOrShared = dataReader["TransferOrShared"].ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.Add(item);
                        }
                    }

                    dataReader.Close();
                    return result;
                }
            }
        }
    }
}
