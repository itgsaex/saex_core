﻿using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;
using ItGroup.Saex.EntityFrameworkCore.Seed;

namespace ItGroup.Saex.EntityFrameworkCore
{
    [DependsOn(
        typeof(SaexCoreModule),
        typeof(AbpZeroCoreEntityFrameworkCoreModule))]
    public class SaexEntityFrameworkModule : AbpModule
    {
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */

        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SaexEntityFrameworkModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            ContextHelper.InitContext(IocManager);

            if (!SkipDbSeed)
            {
                ContextHelper.SeedHostDb(IocManager);
            }
        }

        public override void PreInitialize()
        {
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<SaexDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        SaexDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        SaexDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
        }
    }
}
