using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ItGroup.Saex.EntityFrameworkCore
{
    public static class SaexDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<SaexDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<SaexDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
