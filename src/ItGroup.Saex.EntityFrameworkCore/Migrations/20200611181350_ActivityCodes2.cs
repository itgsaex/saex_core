﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class ActivityCodes2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CaseType",
                table: "ActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "ActivityCodes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsGlobal",
                table: "ActivityCodes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "ActivityCodeDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ActivityCodeId = table.Column<Guid>(nullable: true),
                    Question = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Parameters = table.Column<string>(nullable: true),
                    DefaultValue = table.Column<string>(nullable: true),
                    Required = table.Column<bool>(nullable: true),
                    Order = table.Column<int>(nullable: true),
                    ActivityCodeType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityCodeDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivityCodeDetails_ActivityCodes_ActivityCodeId",
                        column: x => x.ActivityCodeId,
                        principalTable: "ActivityCodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivityCodeDetails_ActivityCodeId",
                table: "ActivityCodeDetails",
                column: "ActivityCodeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityCodeDetails");

            migrationBuilder.DropColumn(
                name: "CaseType",
                table: "ActivityCodes");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "ActivityCodes");

            migrationBuilder.DropColumn(
                name: "IsGlobal",
                table: "ActivityCodes");
        }
    }
}
