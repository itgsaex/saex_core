﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class Add_Region_Route_Id_Field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RouteId",
                table: "Routes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RegionId",
                table: "Regions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RouteId",
                table: "Routes");

            migrationBuilder.DropColumn(
                name: "RegionId",
                table: "Regions");
        }
    }
}
