﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class ADD_InternalAgentPromises_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InternalAgentPromises",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    InternalAgentId = table.Column<long>(nullable: false),
                    CaseID = table.Column<Guid>(nullable: false),
                    Promise = table.Column<string>(maxLength: 100, nullable: true),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InternalAgentPromises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InternalAgentPromises_Cases_CaseID",
                        column: x => x.CaseID,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InternalAgentPromises_AbpUsers_InternalAgentId",
                        column: x => x.InternalAgentId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InternalAgentPromises_CaseID",
                table: "InternalAgentPromises",
                column: "CaseID");

            migrationBuilder.CreateIndex(
                name: "IX_InternalAgentPromises_InternalAgentId",
                table: "InternalAgentPromises",
                column: "InternalAgentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InternalAgentPromises");
        }
    }
}
