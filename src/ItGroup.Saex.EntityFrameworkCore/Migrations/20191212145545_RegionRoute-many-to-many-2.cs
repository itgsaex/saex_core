﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class RegionRoutemanytomany2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "RegionRoutes");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "RegionRoutes");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "RegionRoutes");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "RegionRoutes");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "RegionRoutes");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "RegionRoutes");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "RegionRoutes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "RegionRoutes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "RegionRoutes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "RegionRoutes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "RegionRoutes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "RegionRoutes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "RegionRoutes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "RegionRoutes",
                nullable: true);
        }
    }
}
