﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class ADD_InternalAgentComment_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropColumn(
            //    name: "FieldsToPivot",
            //    table: "Lists");

            //migrationBuilder.DropColumn(
            //    name: "ReferenceId",
            //    table: "Lists");

            migrationBuilder.CreateTable(
                name: "InternalAgentComments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    InternalAgentId = table.Column<long>(nullable: false),
                    CaseID = table.Column<Guid>(nullable: false),
                    Comment = table.Column<string>(maxLength: 500, nullable: true),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InternalAgentComments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InternalAgentComments_Cases_CaseID",
                        column: x => x.CaseID,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InternalAgentComments_AbpUsers_InternalAgentId",
                        column: x => x.InternalAgentId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InternalAgentComments_CaseID",
                table: "InternalAgentComments",
                column: "CaseID");

            migrationBuilder.CreateIndex(
                name: "IX_InternalAgentComments_InternalAgentId",
                table: "InternalAgentComments",
                column: "InternalAgentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InternalAgentComments");

            //migrationBuilder.AddColumn<string>(
            //    name: "FieldsToPivot",
            //    table: "Lists",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "ReferenceId",
            //    table: "Lists",
            //    nullable: false,
            //    defaultValue: 0);
        }
    }
}
