﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class CaseTypemobilefields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IconText",
                table: "CaseTypes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductNumber",
                table: "CaseTypes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TipoListaMaestra",
                table: "CaseTypes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "UseActionCode",
                table: "CaseTypes",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IconText",
                table: "CaseTypes");

            migrationBuilder.DropColumn(
                name: "ProductNumber",
                table: "CaseTypes");

            migrationBuilder.DropColumn(
                name: "TipoListaMaestra",
                table: "CaseTypes");

            migrationBuilder.DropColumn(
                name: "UseActionCode",
                table: "CaseTypes");
        }
    }
}
