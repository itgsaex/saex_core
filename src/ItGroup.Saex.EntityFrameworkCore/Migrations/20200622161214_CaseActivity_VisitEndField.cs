﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class CaseActivity_VisitEndField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Longitude",
                table: "CaseActivities",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "Latitude",
                table: "CaseActivities",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AddColumn<DateTime>(
                name: "VisitEndDate",
                table: "CaseActivities",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "VisitEndLatitude",
                table: "CaseActivities",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "VisitEndLongitude",
                table: "CaseActivities",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VisitEndDate",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "VisitEndLatitude",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "VisitEndLongitude",
                table: "CaseActivities");

            migrationBuilder.AlterColumn<double>(
                name: "Longitude",
                table: "CaseActivities",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Latitude",
                table: "CaseActivities",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);
        }
    }
}
