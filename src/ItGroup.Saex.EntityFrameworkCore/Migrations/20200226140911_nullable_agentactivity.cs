﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class nullable_agentactivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AgentActivities_CaseActivities_CaseActivityId",
                table: "AgentActivities");

            migrationBuilder.AlterColumn<Guid>(
                name: "CaseActivityId",
                table: "AgentActivities",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_AgentActivities_CaseActivities_CaseActivityId",
                table: "AgentActivities",
                column: "CaseActivityId",
                principalTable: "CaseActivities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AgentActivities_CaseActivities_CaseActivityId",
                table: "AgentActivities");

            migrationBuilder.AlterColumn<Guid>(
                name: "CaseActivityId",
                table: "AgentActivities",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AgentActivities_CaseActivities_CaseActivityId",
                table: "AgentActivities",
                column: "CaseActivityId",
                principalTable: "CaseActivities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
