﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class Create_Table_QueryFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QueryFields",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FieldId = table.Column<int>(nullable: false),
                    MasterList = table.Column<bool>(nullable: false),
                    ChildList = table.Column<bool>(nullable: false),
                    RiskList = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueryFields", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QueryFields");
        }
    }
}
