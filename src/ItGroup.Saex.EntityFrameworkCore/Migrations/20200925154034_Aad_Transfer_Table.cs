﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class Aad_Transfer_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AgentTransfers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FromAgentId = table.Column<Guid>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: false),
                    ToAgentId = table.Column<Guid>(nullable: false),
                    CaseId = table.Column<Guid>(nullable: false),
                    TransferType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentTransfers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgentTransfers_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgentTransfers_Agents_FromAgentId",
                        column: x => x.FromAgentId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgentTransfers_Agents_ToAgentId",
                        column: x => x.ToAgentId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AgentTransfers_CaseId",
                table: "AgentTransfers",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentTransfers_FromAgentId",
                table: "AgentTransfers",
                column: "FromAgentId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentTransfers_ToAgentId",
                table: "AgentTransfers",
                column: "ToAgentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgentTransfers");
        }
    }
}
