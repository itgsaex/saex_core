﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class case_customer_address_relationship_update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Addresses_Cases_CaseId",
                table: "Addresses");

            migrationBuilder.DropForeignKey(
                name: "FK_Customer_Cases_CaseId",
                table: "Customer");

            migrationBuilder.DropIndex(
                name: "IX_Addresses_CaseId",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "CaseId",
                table: "Addresses");

            migrationBuilder.AlterColumn<Guid>(
                name: "CaseId",
                table: "Customer",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_Cases_CaseId",
                table: "Customer",
                column: "CaseId",
                principalTable: "Cases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customer_Cases_CaseId",
                table: "Customer");

            migrationBuilder.AlterColumn<Guid>(
                name: "CaseId",
                table: "Customer",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "CaseId",
                table: "Addresses",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CaseId",
                table: "Addresses",
                column: "CaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Addresses_Cases_CaseId",
                table: "Addresses",
                column: "CaseId",
                principalTable: "Cases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_Cases_CaseId",
                table: "Customer",
                column: "CaseId",
                principalTable: "Cases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
