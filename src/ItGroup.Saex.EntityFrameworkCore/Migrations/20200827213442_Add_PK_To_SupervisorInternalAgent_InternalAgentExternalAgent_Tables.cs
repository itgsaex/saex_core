﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class Add_PK_To_SupervisorInternalAgent_InternalAgentExternalAgent_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SupervisorInternalAgents",
                table: "SupervisorInternalAgents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InternalAgentExternalAgents",
                table: "InternalAgentExternalAgents");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SupervisorInternalAgents",
                table: "SupervisorInternalAgents",
                columns: new[] { "SupervisorId", "InternalAgentId", "Id" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_InternalAgentExternalAgents",
                table: "InternalAgentExternalAgents",
                columns: new[] { "InternalAgentId", "ExternalAgentId", "Id" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SupervisorInternalAgents",
                table: "SupervisorInternalAgents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InternalAgentExternalAgents",
                table: "InternalAgentExternalAgents");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SupervisorInternalAgents",
                table: "SupervisorInternalAgents",
                columns: new[] { "SupervisorId", "InternalAgentId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_InternalAgentExternalAgents",
                table: "InternalAgentExternalAgents",
                columns: new[] { "InternalAgentId", "ExternalAgentId" });
        }
    }
}
