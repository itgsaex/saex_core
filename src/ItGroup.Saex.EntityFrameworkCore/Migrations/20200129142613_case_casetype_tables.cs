﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class case_casetype_tables : Migration
    {
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseTypes_CaseTypeId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_CaseTypes_Products_ProductId",
                table: "CaseTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CaseTypes",
                table: "CaseTypes");

            migrationBuilder.RenameTable(
                name: "CaseTypes",
                newName: "CaseType");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "CaseType",
                newName: "Name");

            migrationBuilder.RenameIndex(
                name: "IX_CaseTypes_ProductId",
                table: "CaseType",
                newName: "IX_CaseType_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CaseType",
                table: "CaseType",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseType_CaseTypeId",
                table: "Cases",
                column: "CaseTypeId",
                principalTable: "CaseType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CaseType_Products_ProductId",
                table: "CaseType",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseType_CaseTypeId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_CaseType_Products_ProductId",
                table: "CaseType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CaseType",
                table: "CaseType");

            migrationBuilder.RenameTable(
                name: "CaseType",
                newName: "CaseTypes");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "CaseTypes",
                newName: "Description");

            migrationBuilder.RenameIndex(
                name: "IX_CaseType_ProductId",
                table: "CaseTypes",
                newName: "IX_CaseTypes_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CaseTypes",
                table: "CaseTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseTypes_CaseTypeId",
                table: "Cases",
                column: "CaseTypeId",
                principalTable: "CaseTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CaseTypes_Products_ProductId",
                table: "CaseTypes",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
