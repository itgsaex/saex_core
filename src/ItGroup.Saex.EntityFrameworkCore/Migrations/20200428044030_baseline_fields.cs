﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class baseline_fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseFieldImports_CaseImportLogs_CaseImportLogId",
                table: "CaseFieldImports");

            migrationBuilder.DropTable(
                name: "CaseImportLogs");

            migrationBuilder.DropIndex(
                name: "IX_CaseFieldImports_CaseImportLogId",
                table: "CaseFieldImports");

            migrationBuilder.DropColumn(
                name: "CaseImportLogId",
                table: "CaseFieldImports");

            migrationBuilder.AddColumn<int>(
                name: "ImportType",
                table: "Cases",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImportType",
                table: "Cases");

            migrationBuilder.AddColumn<Guid>(
                name: "CaseImportLogId",
                table: "CaseFieldImports",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CaseImportLogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Activo = table.Column<bool>(nullable: false),
                    Cargos = table.Column<double>(nullable: false),
                    CasoID = table.Column<string>(nullable: true),
                    Ciudad = table.Column<string>(nullable: true),
                    CiudadEstadoCliente = table.Column<string>(nullable: true),
                    Cobrador = table.Column<string>(nullable: true),
                    CobroDesdeHora = table.Column<string>(nullable: true),
                    CobroHastaHora = table.Column<string>(nullable: true),
                    Comentario = table.Column<string>(nullable: true),
                    Condicion = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DiaCobro = table.Column<int>(nullable: true),
                    DiaVisita = table.Column<int>(nullable: true),
                    DiasDelFinDeMes = table.Column<string>(nullable: true),
                    DiasVencimiento = table.Column<int>(nullable: true),
                    Direccion2 = table.Column<string>(nullable: true),
                    Direccion3 = table.Column<string>(nullable: true),
                    DireccionCliente1 = table.Column<string>(nullable: true),
                    DireccionCliente2 = table.Column<string>(nullable: true),
                    DireccionClienteZipCode = table.Column<string>(nullable: true),
                    DireccionPrincipal = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EstadoRiesgo = table.Column<string>(nullable: true),
                    Estatus = table.Column<string>(nullable: true),
                    EstatusActividad = table.Column<int>(nullable: true),
                    EstatusAjustador = table.Column<string>(nullable: true),
                    EstatusSistema = table.Column<string>(nullable: true),
                    EtapasDeDelicuencia = table.Column<string>(nullable: true),
                    FechaEntradaSAEx = table.Column<string>(nullable: true),
                    FechaNacimiento = table.Column<string>(nullable: true),
                    FechaNacimientoCodeudor = table.Column<string>(nullable: true),
                    FechaOrigen = table.Column<string>(nullable: true),
                    FechaPago = table.Column<string>(nullable: true),
                    FechaUltPago = table.Column<string>(nullable: true),
                    FechaVencimiento = table.Column<string>(nullable: true),
                    Flag = table.Column<int>(nullable: true),
                    Frecuencia = table.Column<int>(nullable: true),
                    HoraCobro = table.Column<int>(nullable: true),
                    HoraVisita = table.Column<int>(nullable: true),
                    IdentificacionID = table.Column<string>(nullable: true),
                    Interes = table.Column<double>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Jueves = table.Column<int>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    Latitud = table.Column<float>(nullable: false),
                    LatitudRuta = table.Column<float>(nullable: false),
                    Longitud = table.Column<float>(nullable: false),
                    LongitudRuta = table.Column<float>(nullable: false),
                    LugarCobro = table.Column<string>(nullable: true),
                    LugarVisita = table.Column<string>(nullable: true),
                    Lunes = table.Column<int>(nullable: true),
                    Martes = table.Column<int>(nullable: true),
                    MaturityDate = table.Column<string>(nullable: true),
                    Miercoles = table.Column<int>(nullable: true),
                    MontoOriginal = table.Column<double>(nullable: false),
                    MontoPago = table.Column<double>(nullable: false),
                    MontoVencido = table.Column<double>(nullable: false),
                    Nombre = table.Column<string>(nullable: true),
                    Nombre2 = table.Column<string>(nullable: true),
                    NumeroCuenta = table.Column<string>(nullable: true),
                    Obligatorio = table.Column<bool>(nullable: false),
                    PersonaContacto = table.Column<string>(nullable: true),
                    Prioridad = table.Column<int>(nullable: false),
                    ProductNumber = table.Column<string>(nullable: true),
                    Pueblo = table.Column<string>(nullable: true),
                    Referido = table.Column<bool>(nullable: false),
                    Sabado = table.Column<int>(nullable: true),
                    SeguroSocial = table.Column<string>(nullable: true),
                    StateCode = table.Column<string>(nullable: true),
                    SubStatusSistema = table.Column<string>(nullable: true),
                    Sucursal = table.Column<string>(nullable: true),
                    Telefono2 = table.Column<string>(nullable: true),
                    Telefono3 = table.Column<string>(nullable: true),
                    TelefonoPrincipal = table.Column<string>(nullable: true),
                    Terminos = table.Column<string>(nullable: true),
                    TipoCuenta = table.Column<string>(nullable: true),
                    Viernes = table.Column<int>(nullable: true),
                    VisitaDesdeHora = table.Column<string>(nullable: true),
                    VisitaHastaHora = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseImportLogs", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CaseFieldImports_CaseImportLogId",
                table: "CaseFieldImports",
                column: "CaseImportLogId");

            migrationBuilder.AddForeignKey(
                name: "FK_CaseFieldImports_CaseImportLogs_CaseImportLogId",
                table: "CaseFieldImports",
                column: "CaseImportLogId",
                principalTable: "CaseImportLogs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
