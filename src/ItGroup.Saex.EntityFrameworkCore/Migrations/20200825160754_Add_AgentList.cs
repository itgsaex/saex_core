﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class Add_AgentList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "WorkingHourId",
                table: "Agents",
                nullable: true);

            //migrationBuilder.AddColumn<string>(
            //    name: "AlternatePhone",
            //    table: "AbpUsers",
            //    nullable: true);

            migrationBuilder.CreateTable(
                name: "AgentLists",
                columns: table => new
                {
                    AgentId = table.Column<Guid>(nullable: false),
                    ListId = table.Column<Guid>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentLists", x => new { x.ListId, x.AgentId });
                    table.ForeignKey(
                        name: "FK_AgentLists_Agents_AgentId",
                        column: x => x.AgentId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgentLists_Lists_ListId",
                        column: x => x.ListId,
                        principalTable: "Lists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Agents_WorkingHourId",
                table: "Agents",
                column: "WorkingHourId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentLists_AgentId",
                table: "AgentLists",
                column: "AgentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Agents_WorkingHours_WorkingHourId",
                table: "Agents",
                column: "WorkingHourId",
                principalTable: "WorkingHours",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Agents_WorkingHours_WorkingHourId",
                table: "Agents");

            migrationBuilder.DropTable(
                name: "AgentLists");

            migrationBuilder.DropIndex(
                name: "IX_Agents_WorkingHourId",
                table: "Agents");

            migrationBuilder.DropColumn(
                name: "WorkingHourId",
                table: "Agents");

            migrationBuilder.DropColumn(
                name: "AlternatePhone",
                table: "AbpUsers");
        }
    }
}
