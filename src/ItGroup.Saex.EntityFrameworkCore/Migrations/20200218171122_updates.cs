﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class updates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPromise",
                table: "SystemStates",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPromise",
                table: "SystemStates");
        }
    }
}
