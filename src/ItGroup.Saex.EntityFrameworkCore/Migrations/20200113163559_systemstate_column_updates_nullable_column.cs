﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class systemstate_column_updates_nullable_column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SystemStates_SystemSubStates_SubStateId",
                table: "SystemStates");

            migrationBuilder.AlterColumn<Guid>(
                name: "SubStateId",
                table: "SystemStates",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_SystemStates_SystemSubStates_SubStateId",
                table: "SystemStates",
                column: "SubStateId",
                principalTable: "SystemSubStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SystemStates_SystemSubStates_SubStateId",
                table: "SystemStates");

            migrationBuilder.AlterColumn<Guid>(
                name: "SubStateId",
                table: "SystemStates",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SystemStates_SystemSubStates_SubStateId",
                table: "SystemStates",
                column: "SubStateId",
                principalTable: "SystemSubStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
