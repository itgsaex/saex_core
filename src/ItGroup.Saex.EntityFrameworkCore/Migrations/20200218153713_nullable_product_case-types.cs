﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class nullable_product_casetypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseTypes_Products_ProductId",
                table: "CaseTypes");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProductId",
                table: "CaseTypes",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_CaseTypes_Products_ProductId",
                table: "CaseTypes",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseTypes_Products_ProductId",
                table: "CaseTypes");

            migrationBuilder.AlterColumn<Guid>(
                name: "ProductId",
                table: "CaseTypes",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CaseTypes_Products_ProductId",
                table: "CaseTypes",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
