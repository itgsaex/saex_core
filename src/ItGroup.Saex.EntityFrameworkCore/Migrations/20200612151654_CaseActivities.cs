﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class CaseActivities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContactedPerson",
                table: "CaseActivities",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "CaseActivities",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DelayReason",
                table: "CaseActivities",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ExportDate",
                table: "CaseActivities",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Latitude",
                table: "CaseActivities",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Longitude",
                table: "CaseActivities",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "CaseActivities",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TransStatus",
                table: "CaseActivities",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "TransferDate",
                table: "CaseActivities",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VisitedPlace",
                table: "CaseActivities",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ActivityDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ActivityId = table.Column<Guid>(nullable: false),
                    ActivityCodeID = table.Column<string>(nullable: true),
                    Result = table.Column<string>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Required = table.Column<bool>(nullable: false),
                    Parameters = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ActivityLines",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ActivityId = table.Column<Guid>(nullable: false),
                    ActivityCodeID = table.Column<string>(nullable: true),
                    VisitedPlace = table.Column<string>(nullable: true),
                    ContactedPerson = table.Column<string>(nullable: true),
                    DelayReason = table.Column<string>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    ReasonCode = table.Column<string>(nullable: true),
                    ReactionCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityLines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ActivityReactionReasonDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ActivityId = table.Column<Guid>(nullable: false),
                    ActivityCodeID = table.Column<string>(nullable: true),
                    QuestionId = table.Column<Guid>(nullable: false),
                    Question = table.Column<string>(nullable: true),
                    DetailType = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Required = table.Column<bool>(nullable: false),
                    Parameters = table.Column<string>(nullable: true),
                    Result = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityReactionReasonDetails", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityDetails");

            migrationBuilder.DropTable(
                name: "ActivityLines");

            migrationBuilder.DropTable(
                name: "ActivityReactionReasonDetails");

            migrationBuilder.DropColumn(
                name: "ContactedPerson",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "DelayReason",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "ExportDate",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "TransStatus",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "TransferDate",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "VisitedPlace",
                table: "CaseActivities");
        }
    }
}
