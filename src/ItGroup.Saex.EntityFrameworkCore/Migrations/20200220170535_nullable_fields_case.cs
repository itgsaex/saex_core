﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class nullable_fields_case : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseDelinquencyStates_DelinquencyStateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseProduct_ProductId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Routes_RouteId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseSystemStates_SystemStateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseSystemSubState_SystemSubStateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseTactics_TacticId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_DelinquencyStateId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_ProductId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_SystemStateId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_SystemSubStateId",
                table: "Cases");

            migrationBuilder.AlterColumn<Guid>(
                name: "TacticId",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "SystemSubStateId",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "SystemStateId",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "RouteId",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "ProductId",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "DelinquencyStateId",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.CreateIndex(
                name: "IX_Cases_DelinquencyStateId",
                table: "Cases",
                column: "DelinquencyStateId",
                unique: true,
                filter: "[DelinquencyStateId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_ProductId",
                table: "Cases",
                column: "ProductId",
                unique: true,
                filter: "[ProductId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_SystemStateId",
                table: "Cases",
                column: "SystemStateId",
                unique: true,
                filter: "[SystemStateId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_SystemSubStateId",
                table: "Cases",
                column: "SystemSubStateId",
                unique: true,
                filter: "[SystemSubStateId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseDelinquencyStates_DelinquencyStateId",
                table: "Cases",
                column: "DelinquencyStateId",
                principalTable: "CaseDelinquencyStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseProduct_ProductId",
                table: "Cases",
                column: "ProductId",
                principalTable: "CaseProduct",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Routes_RouteId",
                table: "Cases",
                column: "RouteId",
                principalTable: "Routes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseSystemStates_SystemStateId",
                table: "Cases",
                column: "SystemStateId",
                principalTable: "CaseSystemStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseSystemSubState_SystemSubStateId",
                table: "Cases",
                column: "SystemSubStateId",
                principalTable: "CaseSystemSubState",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseTactics_TacticId",
                table: "Cases",
                column: "TacticId",
                principalTable: "CaseTactics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseDelinquencyStates_DelinquencyStateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseProduct_ProductId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Routes_RouteId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseSystemStates_SystemStateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseSystemSubState_SystemSubStateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseTactics_TacticId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_DelinquencyStateId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_ProductId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_SystemStateId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_SystemSubStateId",
                table: "Cases");

            migrationBuilder.AlterColumn<Guid>(
                name: "TacticId",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "SystemSubStateId",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "SystemStateId",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "RouteId",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "ProductId",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DelinquencyStateId",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_DelinquencyStateId",
                table: "Cases",
                column: "DelinquencyStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_ProductId",
                table: "Cases",
                column: "ProductId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_SystemStateId",
                table: "Cases",
                column: "SystemStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_SystemSubStateId",
                table: "Cases",
                column: "SystemSubStateId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseDelinquencyStates_DelinquencyStateId",
                table: "Cases",
                column: "DelinquencyStateId",
                principalTable: "CaseDelinquencyStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseProduct_ProductId",
                table: "Cases",
                column: "ProductId",
                principalTable: "CaseProduct",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Routes_RouteId",
                table: "Cases",
                column: "RouteId",
                principalTable: "Routes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseSystemStates_SystemStateId",
                table: "Cases",
                column: "SystemStateId",
                principalTable: "CaseSystemStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseSystemSubState_SystemSubStateId",
                table: "Cases",
                column: "SystemSubStateId",
                principalTable: "CaseSystemSubState",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseTactics_TacticId",
                table: "Cases",
                column: "TacticId",
                principalTable: "CaseTactics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
