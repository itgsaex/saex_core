﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class substates_columns_updates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SystemStates_AdjusterStates_AdjusterStateId",
                table: "SystemStates");

            migrationBuilder.DropForeignKey(
                name: "FK_SystemStates_SystemSubStates_SystemSubStateId",
                table: "SystemStates");

            migrationBuilder.DropIndex(
                name: "IX_SystemStates_AdjusterStateId",
                table: "SystemStates");

            migrationBuilder.RenameColumn(
                name: "SystemSubStateId",
                table: "SystemStates",
                newName: "SubStateId");

            migrationBuilder.RenameIndex(
                name: "IX_SystemStates_SystemSubStateId",
                table: "SystemStates",
                newName: "IX_SystemStates_SubStateId");

            migrationBuilder.AddForeignKey(
                name: "FK_SystemStates_SystemSubStates_SubStateId",
                table: "SystemStates",
                column: "SubStateId",
                principalTable: "SystemSubStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SystemStates_SystemSubStates_SubStateId",
                table: "SystemStates");

            migrationBuilder.RenameColumn(
                name: "SubStateId",
                table: "SystemStates",
                newName: "SystemSubStateId");

            migrationBuilder.RenameIndex(
                name: "IX_SystemStates_SubStateId",
                table: "SystemStates",
                newName: "IX_SystemStates_SystemSubStateId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemStates_AdjusterStateId",
                table: "SystemStates",
                column: "AdjusterStateId");

            migrationBuilder.AddForeignKey(
                name: "FK_SystemStates_AdjusterStates_AdjusterStateId",
                table: "SystemStates",
                column: "AdjusterStateId",
                principalTable: "AdjusterStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SystemStates_SystemSubStates_SystemSubStateId",
                table: "SystemStates",
                column: "SystemSubStateId",
                principalTable: "SystemSubStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
