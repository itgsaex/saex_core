﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class audit_logs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "DomainEntitiesAudits",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "Agents",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "DomainEntitiesAudits");

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "Agents",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
