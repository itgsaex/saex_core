﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class agent_supervisor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AgentSupervisors_Agents_AgentId",
                table: "AgentSupervisors");

            migrationBuilder.DropIndex(
                name: "IX_AgentSupervisors_AgentId",
                table: "AgentSupervisors");

            migrationBuilder.RenameColumn(
                name: "AgentId",
                table: "AgentSupervisors",
                newName: "SupervisedId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SupervisedId",
                table: "AgentSupervisors",
                newName: "AgentId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentSupervisors_AgentId",
                table: "AgentSupervisors",
                column: "AgentId");

            migrationBuilder.AddForeignKey(
                name: "FK_AgentSupervisors_Agents_AgentId",
                table: "AgentSupervisors",
                column: "AgentId",
                principalTable: "Agents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
