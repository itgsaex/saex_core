﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class manytomany_removal_of_audit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "RouteProducts");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "RouteProducts");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "RouteProducts");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "RouteProducts");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "RouteProducts");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "RouteProducts");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "RouteProducts");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "RoutePostalCodes");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "RoutePostalCodes");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "RoutePostalCodes");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "RoutePostalCodes");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "RoutePostalCodes");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "RoutePostalCodes");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "RoutePostalCodes");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AgentRoutes");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AgentRoutes");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AgentRoutes");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AgentRoutes");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AgentRoutes");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AgentRoutes");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AgentRoutes");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AgentRegions");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AgentRegions");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AgentRegions");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AgentRegions");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AgentRegions");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AgentRegions");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AgentRegions");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AgentCases");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "RouteProducts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "RouteProducts",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "RouteProducts",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "RouteProducts",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "RouteProducts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "RouteProducts",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "RouteProducts",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "RoutePostalCodes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "RoutePostalCodes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "RoutePostalCodes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "RoutePostalCodes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "RoutePostalCodes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "RoutePostalCodes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "RoutePostalCodes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "CaseActivityCodes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "CaseActivityCodes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AgentRoutes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AgentRoutes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AgentRoutes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AgentRoutes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AgentRoutes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AgentRoutes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AgentRoutes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AgentRegions",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AgentRegions",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AgentRegions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AgentRegions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AgentRegions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AgentRegions",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AgentRegions",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AgentCases",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AgentCases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AgentCases",
                nullable: true);
        }
    }
}
