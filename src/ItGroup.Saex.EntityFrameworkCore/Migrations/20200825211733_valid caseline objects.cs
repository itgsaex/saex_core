﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class validcaselineobjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "CaseBaseline",
            //    columns: table => new
            //    {
            //        Id = table.Column<Guid>(nullable: false),
            //        UploadDate = table.Column<DateTime>(nullable: false),
            //        AccountNumber = table.Column<string>(nullable: true),
            //        AccountNumber2 = table.Column<string>(nullable: true),
            //        Name = table.Column<string>(nullable: true),
            //        DueDays = table.Column<int>(nullable: false),
            //        Progress = table.Column<string>(nullable: true),
            //        DueDate = table.Column<DateTime>(nullable: false),
            //        Nest = table.Column<DateTime>(nullable: false),
            //        DueBalance = table.Column<decimal>(nullable: false),
            //        OriginalAmount = table.Column<decimal>(nullable: false),
            //        LC = table.Column<decimal>(nullable: false),
            //        City = table.Column<string>(nullable: true),
            //        Product = table.Column<string>(nullable: true),
            //        Employee = table.Column<string>(nullable: true),
            //        Supervisor = table.Column<string>(nullable: true),
            //        Comments = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_CaseBaseline", x => x.Id);
            //    });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CaseImportLogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Atraso = table.Column<string>(nullable: true),
                    AtrasoTotal = table.Column<string>(nullable: true),
                    Balance = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    Cuenta = table.Column<string>(nullable: true),
                    Cuenta1 = table.Column<string>(nullable: true),
                    Due = table.Column<string>(nullable: true),
                    Empleado = table.Column<string>(nullable: true),
                    Etapa = table.Column<string>(nullable: true),
                    LC = table.Column<string>(nullable: true),
                    Nest = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    Pandemia = table.Column<string>(nullable: true),
                    Producto = table.Column<string>(nullable: true),
                    Pueblo = table.Column<string>(nullable: true),
                    Supervisor = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseImportLogs", x => x.Id);
                });
        }
    }
}
