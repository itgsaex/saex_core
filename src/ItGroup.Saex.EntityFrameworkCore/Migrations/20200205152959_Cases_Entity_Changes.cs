﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class Cases_Entity_Changes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProductNumber",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPrincipal",
                table: "Customer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "MasterListType",
                table: "CaseTypes",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "RouteId",
                table: "Cases",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "CaseActivityCodes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "CaseActivityCodes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "CaseActivityCodes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPrincipal",
                table: "Addresses",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "PostalCodeId",
                table: "Addresses",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "CaseComment",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    AgentId = table.Column<Guid>(nullable: false),
                    CaseId = table.Column<Guid>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaseComment_Agents_AgentId",
                        column: x => x.AgentId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CaseComment_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cases_RouteId",
                table: "Cases",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_PostalCodeId",
                table: "Addresses",
                column: "PostalCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseComment_AgentId",
                table: "CaseComment",
                column: "AgentId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseComment_CaseId",
                table: "CaseComment",
                column: "CaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Addresses_PostalCodes_PostalCodeId",
                table: "Addresses",
                column: "PostalCodeId",
                principalTable: "PostalCodes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Routes_RouteId",
                table: "Cases",
                column: "RouteId",
                principalTable: "Routes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Addresses_PostalCodes_PostalCodeId",
                table: "Addresses");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Routes_RouteId",
                table: "Cases");

            migrationBuilder.DropTable(
                name: "CaseComment");

            migrationBuilder.DropIndex(
                name: "IX_Cases_RouteId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Addresses_PostalCodeId",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "ProductNumber",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "IsPrincipal",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "MasterListType",
                table: "CaseTypes");

            migrationBuilder.DropColumn(
                name: "RouteId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "CaseActivityCodes");

            migrationBuilder.DropColumn(
                name: "IsPrincipal",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "PostalCodeId",
                table: "Addresses");
        }
    }
}
