﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class cases_entity_updates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_DelinquencyStates_DelinquencyStateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Products_ProductId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_SystemStates_SystemStateId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_DelinquencyStateId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_ProductId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_SystemStateId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Cases");

            migrationBuilder.AlterColumn<DateTime>(
                name: "BirthDate",
                table: "Customer",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "CaseTypes",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "OriginDate",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "MaturityDate",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "LastPaymentDate",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "ImportedDate",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DueDate",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "AccountType",
                table: "Cases",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<Guid>(
                name: "SystemSubStateId",
                table: "Cases",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "CaseDelinquencyStates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CaseId = table.Column<Guid>(nullable: false),
                    DelinquencyStateId = table.Column<Guid>(nullable: false),
                    IsSuccesfulCriteria = table.Column<bool>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseDelinquencyStates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaseDelinquencyStates_DelinquencyStates_DelinquencyStateId",
                        column: x => x.DelinquencyStateId,
                        principalTable: "DelinquencyStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CaseImports",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Activo = table.Column<bool>(nullable: false),
                    Cargos = table.Column<double>(nullable: false),
                    CasoID = table.Column<string>(nullable: true),
                    Ciudad = table.Column<string>(nullable: true),
                    CiudadEstadoCliente = table.Column<string>(nullable: true),
                    Cobrador = table.Column<string>(nullable: true),
                    CobroDesdeHora = table.Column<DateTime>(nullable: true),
                    CobroHastaHora = table.Column<DateTime>(nullable: true),
                    Comentario = table.Column<string>(nullable: true),
                    Condicion = table.Column<string>(nullable: true),
                    DiaCobro = table.Column<int>(nullable: true),
                    DiasDelFinDeMes = table.Column<string>(nullable: true),
                    DiasVencimiento = table.Column<int>(nullable: true),
                    DiaVisita = table.Column<int>(nullable: true),
                    Direccion2 = table.Column<string>(nullable: true),
                    Direccion3 = table.Column<string>(nullable: true),
                    DireccionCliente1 = table.Column<string>(nullable: true),
                    DireccionCliente2 = table.Column<string>(nullable: true),
                    DireccionClienteZipCode = table.Column<string>(nullable: true),
                    DireccionPrincipal = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EstadoRiesgo = table.Column<string>(nullable: true),
                    Estatus = table.Column<string>(nullable: true),
                    EstatusActividad = table.Column<int>(nullable: true),
                    EstatusAjustador = table.Column<string>(nullable: true),
                    EstatusSistema = table.Column<string>(nullable: true),
                    EtapasDeDelicuencia = table.Column<string>(nullable: true),
                    FechaEntradaSAEx = table.Column<DateTime>(nullable: true),
                    FechaNacimiento = table.Column<DateTime>(nullable: true),
                    FechaNacimientoCodeudor = table.Column<DateTime>(nullable: true),
                    FechaOrigen = table.Column<DateTime>(nullable: true),
                    FechaPago = table.Column<DateTime>(nullable: true),
                    FechaUltPago = table.Column<DateTime>(nullable: true),
                    FechaVencimiento = table.Column<DateTime>(nullable: true),
                    Flag = table.Column<int>(nullable: true),
                    Frecuencia = table.Column<int>(nullable: true),
                    HoraCobro = table.Column<int>(nullable: true),
                    HoraVisita = table.Column<int>(nullable: true),
                    IdentificacionID = table.Column<string>(nullable: true),
                    Interes = table.Column<double>(nullable: false),
                    Jueves = table.Column<int>(nullable: true),
                    Latitud = table.Column<float>(nullable: false),
                    LatitudRuta = table.Column<float>(nullable: false),
                    Longitud = table.Column<float>(nullable: false),
                    LongitudRuta = table.Column<float>(nullable: false),
                    LugarCobro = table.Column<string>(nullable: true),
                    LugarVisita = table.Column<string>(nullable: true),
                    Lunes = table.Column<int>(nullable: true),
                    Martes = table.Column<int>(nullable: true),
                    MaturityDate = table.Column<DateTime>(nullable: true),
                    Miercoles = table.Column<int>(nullable: true),
                    MontoOriginal = table.Column<double>(nullable: false),
                    MontoPago = table.Column<double>(nullable: false),
                    MontoVencido = table.Column<double>(nullable: false),
                    Nombre = table.Column<string>(nullable: true),
                    Nombre2 = table.Column<string>(nullable: true),
                    NumeroCuenta = table.Column<string>(nullable: true),
                    Obligatorio = table.Column<bool>(nullable: false),
                    PersonaContacto = table.Column<string>(nullable: true),
                    Prioridad = table.Column<int>(nullable: false),
                    ProductNumber = table.Column<string>(nullable: true),
                    Pueblo = table.Column<string>(nullable: true),
                    Referido = table.Column<bool>(nullable: false),
                    Sabado = table.Column<int>(nullable: true),
                    SeguroSocial = table.Column<string>(nullable: true),
                    StateCode = table.Column<string>(nullable: true),
                    SubStatusSistema = table.Column<string>(nullable: true),
                    Sucursal = table.Column<string>(nullable: true),
                    Telefono2 = table.Column<string>(nullable: true),
                    Telefono3 = table.Column<string>(nullable: true),
                    TelefonoPrincipal = table.Column<string>(nullable: true),
                    Terminos = table.Column<string>(nullable: true),
                    TipoCuenta = table.Column<string>(nullable: true),
                    Viernes = table.Column<int>(nullable: true),
                    VisitaDesdeHora = table.Column<DateTime>(nullable: true),
                    VisitaHastaHora = table.Column<DateTime>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseImports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CaseProduct",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CaseId = table.Column<Guid>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseProduct", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaseProduct_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CaseSystemStates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CaseId = table.Column<Guid>(nullable: false),
                    SystemStateId = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseSystemStates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaseSystemStates_SystemStates_SystemStateId",
                        column: x => x.SystemStateId,
                        principalTable: "SystemStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CaseSystemSubState",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CaseId = table.Column<Guid>(nullable: false),
                    SystemSubStateId = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseSystemSubState", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaseSystemSubState_SystemSubStates_SystemSubStateId",
                        column: x => x.SystemSubStateId,
                        principalTable: "SystemSubStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CaseFieldImports",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CaseTypeID = table.Column<string>(nullable: true),
                    CategoriaID = table.Column<int>(nullable: false),
                    ColumnName = table.Column<string>(nullable: true),
                    DownLoadOption = table.Column<bool>(nullable: false),
                    FieldId = table.Column<int>(nullable: false),
                    HumanName = table.Column<string>(nullable: true),
                    Sequence = table.Column<int>(nullable: false),
                    SourceTargetType = table.Column<string>(nullable: true),
                    TargetColumn = table.Column<string>(nullable: true),
                    CaseImportId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseFieldImports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaseFieldImports_CaseImports_CaseImportId",
                        column: x => x.CaseImportId,
                        principalTable: "CaseImports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cases_DelinquencyStateId",
                table: "Cases",
                column: "DelinquencyStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_ProductId",
                table: "Cases",
                column: "ProductId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_SystemStateId",
                table: "Cases",
                column: "SystemStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_SystemSubStateId",
                table: "Cases",
                column: "SystemSubStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CaseDelinquencyStates_DelinquencyStateId",
                table: "CaseDelinquencyStates",
                column: "DelinquencyStateId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseFieldImports_CaseImportId",
                table: "CaseFieldImports",
                column: "CaseImportId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseProduct_ProductId",
                table: "CaseProduct",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseSystemStates_SystemStateId",
                table: "CaseSystemStates",
                column: "SystemStateId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseSystemSubState_SystemSubStateId",
                table: "CaseSystemSubState",
                column: "SystemSubStateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseDelinquencyStates_DelinquencyStateId",
                table: "Cases",
                column: "DelinquencyStateId",
                principalTable: "CaseDelinquencyStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseProduct_ProductId",
                table: "Cases",
                column: "ProductId",
                principalTable: "CaseProduct",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseSystemStates_SystemStateId",
                table: "Cases",
                column: "SystemStateId",
                principalTable: "CaseSystemStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseSystemSubState_SystemSubStateId",
                table: "Cases",
                column: "SystemSubStateId",
                principalTable: "CaseSystemSubState",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseDelinquencyStates_DelinquencyStateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseProduct_ProductId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseSystemStates_SystemStateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseSystemSubState_SystemSubStateId",
                table: "Cases");

            migrationBuilder.DropTable(
                name: "CaseDelinquencyStates");

            migrationBuilder.DropTable(
                name: "CaseFieldImports");

            migrationBuilder.DropTable(
                name: "CaseProduct");

            migrationBuilder.DropTable(
                name: "CaseSystemStates");

            migrationBuilder.DropTable(
                name: "CaseSystemSubState");

            migrationBuilder.DropTable(
                name: "CaseImports");

            migrationBuilder.DropIndex(
                name: "IX_Cases_DelinquencyStateId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_ProductId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_SystemStateId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_SystemSubStateId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "CaseTypes");

            migrationBuilder.DropColumn(
                name: "SystemSubStateId",
                table: "Cases");

            migrationBuilder.AlterColumn<DateTime>(
                name: "BirthDate",
                table: "Customer",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "OriginDate",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "MaturityDate",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "LastPaymentDate",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ImportedDate",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DueDate",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AccountType",
                table: "Cases",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Cases",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_DelinquencyStateId",
                table: "Cases",
                column: "DelinquencyStateId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_ProductId",
                table: "Cases",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_SystemStateId",
                table: "Cases",
                column: "SystemStateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_DelinquencyStates_DelinquencyStateId",
                table: "Cases",
                column: "DelinquencyStateId",
                principalTable: "DelinquencyStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Products_ProductId",
                table: "Cases",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_SystemStates_SystemStateId",
                table: "Cases",
                column: "SystemStateId",
                principalTable: "SystemStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
