﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class AgentGPS2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "StartDate",
                table: "CaseEvents",
                newName: "Start");

            migrationBuilder.RenameColumn(
                name: "EndDate",
                table: "CaseEvents",
                newName: "End");

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "CaseEvents",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "RemindMe",
                table: "CaseEvents",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "AgentId",
                table: "AgentGPSs",
                nullable: true,
                oldClrType: typeof(Guid));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Location",
                table: "CaseEvents");

            migrationBuilder.DropColumn(
                name: "RemindMe",
                table: "CaseEvents");

            migrationBuilder.RenameColumn(
                name: "Start",
                table: "CaseEvents",
                newName: "StartDate");

            migrationBuilder.RenameColumn(
                name: "End",
                table: "CaseEvents",
                newName: "EndDate");

            migrationBuilder.AlterColumn<Guid>(
                name: "AgentId",
                table: "AgentGPSs",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
