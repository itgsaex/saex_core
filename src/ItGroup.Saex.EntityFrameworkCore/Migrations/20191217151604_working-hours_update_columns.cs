﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class workinghours_update_columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DayOfTheWeek",
                table: "WorkingHours");

            migrationBuilder.RenameColumn(
                name: "StartTime",
                table: "WorkingHours",
                newName: "WednesdayStartTime");

            migrationBuilder.RenameColumn(
                name: "EndTime",
                table: "WorkingHours",
                newName: "WednesdayEndTime");

            migrationBuilder.AddColumn<TimeSpan>(
                name: "FridayEndTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "FridayStartTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "MondayEndTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "MondayStartTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "WorkingHours",
                nullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "SaturdayEndTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "SaturdayStartTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "SundayEndTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "SundayStartTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "ThursdayEndTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "ThursdayStartTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "TuesdayEndTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "TuesdayStartTime",
                table: "WorkingHours",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FridayEndTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "FridayStartTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "MondayEndTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "MondayStartTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "SaturdayEndTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "SaturdayStartTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "SundayEndTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "SundayStartTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "ThursdayEndTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "ThursdayStartTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "TuesdayEndTime",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "TuesdayStartTime",
                table: "WorkingHours");

            migrationBuilder.RenameColumn(
                name: "WednesdayStartTime",
                table: "WorkingHours",
                newName: "StartTime");

            migrationBuilder.RenameColumn(
                name: "WednesdayEndTime",
                table: "WorkingHours",
                newName: "EndTime");

            migrationBuilder.AddColumn<int>(
                name: "DayOfTheWeek",
                table: "WorkingHours",
                nullable: false,
                defaultValue: 0);
        }
    }
}
