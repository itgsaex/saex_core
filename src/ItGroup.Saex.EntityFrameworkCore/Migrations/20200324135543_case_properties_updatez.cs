﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class case_properties_updatez : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ListType",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PostalCodeId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProductType",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "TransferedCases",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CaseId = table.Column<Guid>(nullable: false),
                    TranferedFromAgentId = table.Column<Guid>(nullable: true),
                    TransferedFromAgentId = table.Column<Guid>(nullable: true),
                    TransferedToAgentId = table.Column<Guid>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferedCases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransferedCases_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransferedCases_Agents_TranferedFromAgentId",
                        column: x => x.TranferedFromAgentId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransferedCases_Agents_TransferedToAgentId",
                        column: x => x.TransferedToAgentId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransferedCases_CaseId",
                table: "TransferedCases",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_TransferedCases_TranferedFromAgentId",
                table: "TransferedCases",
                column: "TranferedFromAgentId");

            migrationBuilder.CreateIndex(
                name: "IX_TransferedCases_TransferedToAgentId",
                table: "TransferedCases",
                column: "TransferedToAgentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransferedCases");

            migrationBuilder.DropColumn(
                name: "ListType",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PostalCodeId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ProductType",
                table: "Cases");
        }
    }
}
