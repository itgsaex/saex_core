﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class AgentActivity_entity_definition : Migration
    {
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseActivities_PaymentDelayReasons_PaymentDelayReasonId",
                table: "CaseActivities");

            migrationBuilder.DropTable(
                name: "AgentActivities");

            migrationBuilder.DropIndex(
                name: "IX_CaseActivities_PaymentDelayReasonId",
                table: "CaseActivities");

            migrationBuilder.DropColumn(
                name: "PaymentDelayReasonId",
                table: "CaseActivities");
        }

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "PaymentDelayReasonId",
                table: "CaseActivities",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "AgentActivities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    AgentId = table.Column<Guid>(nullable: false),
                    CaseActivityId = table.Column<Guid>(nullable: false),
                    Latitude = table.Column<float>(nullable: false),
                    Longitude = table.Column<float>(nullable: false),
                    TransStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentActivities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgentActivities_Agents_AgentId",
                        column: x => x.AgentId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AgentActivities_CaseActivities_CaseActivityId",
                        column: x => x.CaseActivityId,
                        principalTable: "CaseActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CaseActivities_PaymentDelayReasonId",
                table: "CaseActivities",
                column: "PaymentDelayReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentActivities_AgentId",
                table: "AgentActivities",
                column: "AgentId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentActivities_CaseActivityId",
                table: "AgentActivities",
                column: "CaseActivityId");

            migrationBuilder.AddForeignKey(
                name: "FK_CaseActivities_PaymentDelayReasons_PaymentDelayReasonId",
                table: "CaseActivities",
                column: "PaymentDelayReasonId",
                principalTable: "PaymentDelayReasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
