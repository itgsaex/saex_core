﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class SortFieldsCase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SortFieldsCases",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FieldName = table.Column<string>(nullable: true),
                    FieldLabel = table.Column<string>(nullable: true),
                    Use = table.Column<string>(nullable: true),
                    FilterType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SortFieldsCases", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SortFieldsCases");
        }
    }
}
