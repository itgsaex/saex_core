﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class assign_cases_entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransferedCases");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AgentCases",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "AgentCases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AgentCases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "TransferedById",
                table: "AgentCases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "AgentCases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_AgentCases_TransferedById",
                table: "AgentCases",
                column: "TransferedById");

            migrationBuilder.AddForeignKey(
                name: "FK_AgentCases_AbpUsers_TransferedById",
                table: "AgentCases",
                column: "TransferedById",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AgentCases_AbpUsers_TransferedById",
                table: "AgentCases");

            migrationBuilder.DropIndex(
                name: "IX_AgentCases_TransferedById",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "TransferedById",
                table: "AgentCases");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "AgentCases");

            migrationBuilder.CreateTable(
                name: "TransferedCases",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CaseId = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    TranferedFromAgentId = table.Column<Guid>(nullable: true),
                    TransferedFromAgentId = table.Column<Guid>(nullable: true),
                    TransferedToAgentId = table.Column<Guid>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferedCases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransferedCases_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransferedCases_Agents_TranferedFromAgentId",
                        column: x => x.TranferedFromAgentId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransferedCases_Agents_TransferedToAgentId",
                        column: x => x.TransferedToAgentId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransferedCases_CaseId",
                table: "TransferedCases",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_TransferedCases_TranferedFromAgentId",
                table: "TransferedCases",
                column: "TranferedFromAgentId");

            migrationBuilder.CreateIndex(
                name: "IX_TransferedCases_TransferedToAgentId",
                table: "TransferedCases",
                column: "TransferedToAgentId");
        }
    }
}
