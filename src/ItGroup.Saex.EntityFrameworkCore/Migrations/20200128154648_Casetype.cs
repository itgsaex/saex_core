﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class Casetype : Migration
    {
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Addresses_Customer_CustomerId",
                table: "Addresses");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseType_CaseTypeId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_CaseTactics_TacticId",
                table: "Cases");

            migrationBuilder.DropTable(
                name: "CaseType");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropIndex(
                name: "IX_CaseTactics_CaseId",
                table: "CaseTactics");

            migrationBuilder.DropIndex(
                name: "IX_Cases_CaseTypeId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_TacticId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Addresses_CustomerId",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "AccountType",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CaseTypeId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Charges",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "DueDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LastPaymentDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "MaturityDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "OriginDate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "TacticId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Addresses");

            migrationBuilder.RenameColumn(
                name: "Terms",
                table: "Cases",
                newName: "TermId");

            migrationBuilder.RenameColumn(
                name: "ContactPerson",
                table: "Cases",
                newName: "SSN");

            migrationBuilder.RenameColumn(
                name: "Condition",
                table: "Cases",
                newName: "PhoneNumber");

            migrationBuilder.RenameColumn(
                name: "CaseNumber",
                table: "Cases",
                newName: "FullName");

            migrationBuilder.AddColumn<string>(
                name: "ClientId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmailAddress",
                table: "Cases",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CaseTactics_CaseId",
                table: "CaseTactics",
                column: "CaseId",
                unique: true);
        }

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_CaseTactics_CaseId",
                table: "CaseTactics");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "EmailAddress",
                table: "Cases");

            migrationBuilder.RenameColumn(
                name: "TermId",
                table: "Cases",
                newName: "Terms");

            migrationBuilder.RenameColumn(
                name: "SSN",
                table: "Cases",
                newName: "ContactPerson");

            migrationBuilder.RenameColumn(
                name: "PhoneNumber",
                table: "Cases",
                newName: "Condition");

            migrationBuilder.RenameColumn(
                name: "FullName",
                table: "Cases",
                newName: "CaseNumber");

            migrationBuilder.AddColumn<int>(
                name: "AccountType",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "CaseTypeId",
                table: "Cases",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<double>(
                name: "Charges",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DueDate",
                table: "Cases",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastPaymentDate",
                table: "Cases",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "MaturityDate",
                table: "Cases",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "OriginDate",
                table: "Cases",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "TacticId",
                table: "Cases",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "CustomerId",
                table: "Addresses",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CaseType",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DefaultColor = table.Column<string>(nullable: true),
                    IconImage = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ProductId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaseType_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<string>(nullable: true),
                    EmailAddress = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    IdentificationNumber = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Ssn = table.Column<string>(nullable: true),
                    CaseId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customer_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CaseTactics_CaseId",
                table: "CaseTactics",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_CaseTypeId",
                table: "Cases",
                column: "CaseTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_TacticId",
                table: "Cases",
                column: "TacticId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CustomerId",
                table: "Addresses",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseType_ProductId",
                table: "CaseType",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_CaseId",
                table: "Customer",
                column: "CaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Addresses_Customer_CustomerId",
                table: "Addresses",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseType_CaseTypeId",
                table: "Cases",
                column: "CaseTypeId",
                principalTable: "CaseType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_CaseTactics_TacticId",
                table: "Cases",
                column: "TacticId",
                principalTable: "CaseTactics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
