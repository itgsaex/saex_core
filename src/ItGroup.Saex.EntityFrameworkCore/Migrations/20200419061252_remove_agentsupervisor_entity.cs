﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class remove_agentsupervisor_entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgentSupervisors");

            migrationBuilder.AddColumn<Guid>(
                name: "SupervisorId",
                table: "Agents",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SupervisorType",
                table: "Agents",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Agents_SupervisorId",
                table: "Agents",
                column: "SupervisorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Agents_Agents_SupervisorId",
                table: "Agents",
                column: "SupervisorId",
                principalTable: "Agents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Agents_Agents_SupervisorId",
                table: "Agents");

            migrationBuilder.DropIndex(
                name: "IX_Agents_SupervisorId",
                table: "Agents");

            migrationBuilder.DropColumn(
                name: "SupervisorId",
                table: "Agents");

            migrationBuilder.DropColumn(
                name: "SupervisorType",
                table: "Agents");

            migrationBuilder.CreateTable(
                name: "AgentSupervisors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    SupervisedId = table.Column<Guid>(nullable: false),
                    SupervisorId = table.Column<Guid>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentSupervisors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgentSupervisors_Agents_SupervisorId",
                        column: x => x.SupervisorId,
                        principalTable: "Agents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AgentSupervisors_SupervisorId",
                table: "AgentSupervisors",
                column: "SupervisorId");
        }
    }
}
