﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class KPI : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "KPIs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AgenteID = table.Column<string>(maxLength: 50, nullable: true),
                    GrupoKPI = table.Column<string>(maxLength: 50, nullable: true),
                    NombreKPI = table.Column<string>(maxLength: 50, nullable: true),
                    ActualDia = table.Column<float>(nullable: false),
                    ActualSemana = table.Column<float>(nullable: false),
                    ActualMes = table.Column<float>(nullable: false),
                    MetaDia = table.Column<float>(nullable: false),
                    MetaSemana = table.Column<float>(nullable: false),
                    MetaMes = table.Column<float>(nullable: false),
                    PctDia = table.Column<float>(nullable: false),
                    PctSemana = table.Column<float>(nullable: false),
                    PctMes = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KPIs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "KPIs");
        }
    }
}
