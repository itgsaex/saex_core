﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class Case_Mobile_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "BalanceAmount",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "CaseID",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CollectionDay",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CollectionEndTime",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CollectionPlace",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CollectionStartTime",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DelicacyStages",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExtraFields",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Firstname",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Friday",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fulladdress",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fullname",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Cases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsMandatory",
                table: "Cases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPlanned",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Lastname",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Latitude",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "LatitudeRoute",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Longitude",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "LongitudeRoute",
                table: "Cases",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<bool>(
                name: "Monday",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone2",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone3",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Priority",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RouteHeader",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RouteHeaderOrder",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Saturday",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SocialSecurity",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Thursday",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Tuesday",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "VisitEndTime",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VisitPlace",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "VisitStartTime",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Wednesday",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ZipCode",
                table: "Cases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BalanceAmount",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CaseID",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CollectionDay",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CollectionEndTime",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CollectionPlace",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CollectionStartTime",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Contact",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "DelicacyStages",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ExtraFields",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Firstname",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Friday",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Fulladdress",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Fullname",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "IsMandatory",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "IsPlanned",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Lastname",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LatitudeRoute",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "LongitudeRoute",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Monday",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Phone2",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Phone3",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "RouteHeader",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "RouteHeaderOrder",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Saturday",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "SocialSecurity",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Thursday",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Tuesday",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "VisitEndTime",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "VisitPlace",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "VisitStartTime",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Wednesday",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ZipCode",
                table: "Cases");
        }
    }
}
