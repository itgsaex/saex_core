﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItGroup.Saex.Migrations
{
    public partial class Add_SupervisorInternalAgent_InternalAgentExternalAgent_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InternalAgentExternalAgents",
                columns: table => new
                {
                    InternalAgentId = table.Column<long>(nullable: false),
                    ExternalAgentId = table.Column<long>(nullable: false),
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InternalAgentExternalAgents", x => new { x.InternalAgentId, x.ExternalAgentId });
                    table.ForeignKey(
                        name: "FK_InternalAgentExternalAgents_AbpUsers_ExternalAgentId",
                        column: x => x.ExternalAgentId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InternalAgentExternalAgents_AbpUsers_InternalAgentId",
                        column: x => x.InternalAgentId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SupervisorInternalAgents",
                columns: table => new
                {
                    SupervisorId = table.Column<long>(nullable: false),
                    InternalAgentId = table.Column<long>(nullable: false),
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupervisorInternalAgents", x => new { x.SupervisorId, x.InternalAgentId });
                    table.ForeignKey(
                        name: "FK_SupervisorInternalAgents_AbpUsers_InternalAgentId",
                        column: x => x.InternalAgentId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupervisorInternalAgents_AbpUsers_SupervisorId",
                        column: x => x.SupervisorId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InternalAgentExternalAgents_ExternalAgentId",
                table: "InternalAgentExternalAgents",
                column: "ExternalAgentId");

            migrationBuilder.CreateIndex(
                name: "IX_SupervisorInternalAgents_InternalAgentId",
                table: "SupervisorInternalAgents",
                column: "InternalAgentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InternalAgentExternalAgents");

            migrationBuilder.DropTable(
                name: "SupervisorInternalAgents");
        }
    }
}
