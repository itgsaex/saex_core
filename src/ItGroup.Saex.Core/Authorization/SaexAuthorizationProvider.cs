﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace ItGroup.Saex.Authorization
{
    public class SaexAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users_View, L("UsersView"));
            context.CreatePermission(PermissionNames.Pages_Roles_View, L("RolesView"));
            context.CreatePermission(PermissionNames.Pages_Tenants_View, L("TenantsView"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_PostalCodes_View, L("PostalCodesView"));
            context.CreatePermission(PermissionNames.Pages_DelinquencyStates_View, L("DelinquencyStatesView"));
            context.CreatePermission(PermissionNames.Pages_SystemPolicies_View, L("SystemPoliciesView"));
            context.CreatePermission(PermissionNames.Pages_ActivityCodes_View, L("ActivityCodesView"));
            context.CreatePermission(PermissionNames.Pages_AdjusterStates_View, L("AdjusterStatesView"));
            context.CreatePermission(PermissionNames.Pages_Agents_View, L("AgentsView"));
            context.CreatePermission(PermissionNames.Pages_Buckets_View, L("BucketsView"));
            context.CreatePermission(PermissionNames.Pages_Cases_View, L("CasesView"));
            context.CreatePermission(PermissionNames.Pages_ClosingDates_View, L("ClosingDatesView"));
            context.CreatePermission(PermissionNames.Pages_Conditions_View, L("ConditionsView"));
            context.CreatePermission(PermissionNames.Pages_ContactTypes_View, L("ContactTypesView"));
            context.CreatePermission(PermissionNames.Pages_ListFieldCategories_View, L("ListFieldCategoriesView"));
            context.CreatePermission(PermissionNames.Pages_ListFields_View, L("ListFieldsView"));
            context.CreatePermission(PermissionNames.Pages_Lists_View, L("ListsView"));
            context.CreatePermission(PermissionNames.Pages_MacroStates_View, L("MacroStatesView"));
            context.CreatePermission(PermissionNames.Pages_Messages_View, L("MessagesView"));
            context.CreatePermission(PermissionNames.Pages_ProductRules_View, L("ProductRulesView"));
            context.CreatePermission(PermissionNames.Pages_Products_View, L("ProductsView"));
            context.CreatePermission(PermissionNames.Pages_PaymentDelayReasons_View, L("PaymentDelayReasonsView"));
            context.CreatePermission(PermissionNames.Pages_Regions_View, L("RegionsView"));
            context.CreatePermission(PermissionNames.Pages_Routes_View, L("RoutesView"));
            context.CreatePermission(PermissionNames.Pages_ScorecardParameters_View, L("ScorecardParametersView"));
            context.CreatePermission(PermissionNames.Pages_SystemStates_View, L("SystemStatesView"));
            context.CreatePermission(PermissionNames.Pages_SystemSubStates_View, L("SystemSubStatesView"));
            context.CreatePermission(PermissionNames.Pages_VisitPlaces_View, L("VisitPlacesView"));
            context.CreatePermission(PermissionNames.Pages_WorkingHours_View, L("WorkingHoursView"));
            context.CreatePermission(PermissionNames.Pages_AuditLogs_View, L("AuditLogsView"));
            context.CreatePermission(PermissionNames.Pages_AssignRegions_View, L("AssignRegionsView"));
            context.CreatePermission(PermissionNames.Pages_CaseTypes_View, L("CaseTypesView"));
            context.CreatePermission(PermissionNames.Pages_Import, L("DataImportView"));
            context.CreatePermission(PermissionNames.Pages_AgentActivity_View, L("AgentActivityView"));
            context.CreatePermission(PermissionNames.Pages_Reports_View, L("ReportsView"));
            context.CreatePermission(PermissionNames.Pages_Audit_Report_View, L("AuditReportView"));
            context.CreatePermission(PermissionNames.Pages_Cases_Report_View, L("CaseReportView"));
            context.CreatePermission(PermissionNames.Pages_Users_Report, L("UsersReport"));
            context.CreatePermission(PermissionNames.Pages_KPI_Report_View, L("KPIReport"));
            context.CreatePermission(PermissionNames.Pages_ReceivedVsAssignedCases_Report_View, L("ReceivedVsAssignedCasesReport"));
            context.CreatePermission(PermissionNames.Pages_Profiles_Report_View, L("ProfilesReport"));
            context.CreatePermission(PermissionNames.Pages_AssignCases_View, L("AssignCasesView"));
            context.CreatePermission(PermissionNames.Pages_AgentSupervisors_View, L("AgentSupervisorsView"));
            context.CreatePermission(PermissionNames.Pages_BaselineCasesImport_View, L("BaselineCasesImport"));
            context.CreatePermission(PermissionNames.Pages_Roles_Modify, L("RolesModify"));
            context.CreatePermission(PermissionNames.Pages_PostalCodes_Modify, L("PostalCodesModify"));
            context.CreatePermission(PermissionNames.Pages_DelinquencyStates_Modify, L("DelinquencyStatesModify"));
            context.CreatePermission(PermissionNames.Pages_SystemPolicies_Modify, L("SystemPoliciesModify"));
            context.CreatePermission(PermissionNames.Pages_ActivityCodes_Modify, L("ActivityCodesModify"));
            context.CreatePermission(PermissionNames.Pages_AdjusterStates_Modify, L("AdjusterStatesModify"));
            context.CreatePermission(PermissionNames.Pages_Agents_Modify, L("AgentsModify"));
            context.CreatePermission(PermissionNames.Pages_Buckets_Modify, L("BucketsModify"));
            context.CreatePermission(PermissionNames.Pages_Cases_Modify, L("CasesModify"));
            context.CreatePermission(PermissionNames.Pages_ClosingDates_Modify, L("ClosingDatesModify"));
            context.CreatePermission(PermissionNames.Pages_Conditions_Modify, L("ConditionsModify"));
            context.CreatePermission(PermissionNames.Pages_ContactTypes_Modify, L("ContactTypesModify"));
            context.CreatePermission(PermissionNames.Pages_ListFieldCategories_Modify, L("ListFieldCategoriesModify"));
            context.CreatePermission(PermissionNames.Pages_ListFields_Modify, L("ListFieldsModify"));
            context.CreatePermission(PermissionNames.Pages_Lists_Modify, L("ListsModify"));
            context.CreatePermission(PermissionNames.Pages_MacroStates_Modify, L("MacroStatesModify"));
            context.CreatePermission(PermissionNames.Pages_Messages_Modify, L("MessagesModify"));
            context.CreatePermission(PermissionNames.Pages_ProductRules_Modify, L("ProductRulesModify"));
            context.CreatePermission(PermissionNames.Pages_Products_Modify, L("ProductsModify"));
            context.CreatePermission(PermissionNames.Pages_PaymentDelayReasons_Modify, L("PaymentDelayReasonsModify"));
            context.CreatePermission(PermissionNames.Pages_Regions_Modify, L("RegionsModify"));
            context.CreatePermission(PermissionNames.Pages_Routes_Modify, L("RoutesModify"));
            context.CreatePermission(PermissionNames.Pages_ScorecardParameters_Modify, L("ScorecardParametersModify"));
            context.CreatePermission(PermissionNames.Pages_SystemStates_Modify, L("SystemStatesModify"));
            context.CreatePermission(PermissionNames.Pages_SystemSubStates_Modify, L("SystemSubStatesModify"));
            context.CreatePermission(PermissionNames.Pages_VisitPlaces_Modify, L("VisitPlacesModify"));
            context.CreatePermission(PermissionNames.Pages_WorkingHours_Modify, L("WorkingHoursModify"));
            context.CreatePermission(PermissionNames.Pages_AssignRegions_Modify, L("AssignRegionsModify"));
            context.CreatePermission(PermissionNames.Pages_CaseTypes_Modify, L("CaseTypesModify"));
            context.CreatePermission(PermissionNames.Pages_AssignCases_Modify, L("AssignCasesModify"));
            context.CreatePermission(PermissionNames.Pages_AgentSupervisors_Modify, L("AgentSupervisorsModify"));
            context.CreatePermission(PermissionNames.Pages_DemographicFields, L("DemographicFields"));
            context.CreatePermission(PermissionNames.Pages_AddressManagement_Report_View, L("AddressManagementReport"));
            context.CreatePermission(PermissionNames.Pages_UnassignedCases_Report_View, L("UnassignedCasesReport"));
            context.CreatePermission(PermissionNames.Pages_UnfilteredCases_Report_View, L("UnfilteredCasesByMasterReport"));
            context.CreatePermission(PermissionNames.Pages_ResetUserPassword_View, L("ResetUserPassword"));
            context.CreatePermission(PermissionNames.Pages_TransferredCases_Report_View, L("TransferredCasesReport"));
            context.CreatePermission(PermissionNames.Pages_AgentActivity_Report_View, L("AgentActivityReport"));
            context.CreatePermission(PermissionNames.Pages_BucketsDelinquencyMortgage_Report_View, L("BucketsDelinquencyMortgageReport"));
            context.CreatePermission(PermissionNames.Pages_BucketsDelinquencyMortgageWithIndicator_Report_View, L("BucketsDelinquencyMortgageWithIndicatorReport"));
            context.CreatePermission(PermissionNames.Pages_AgentLocationRegistry_Report_View, L("AgentLocationRegistryReport"));
            context.CreatePermission(PermissionNames.Pages_KPISolvedCases_Report_View, L("KPISolvedCasesReport"));
            context.CreatePermission(PermissionNames.Pages_DailyAgentActivityEffectiveness_Report_View, L("DailyAgentActivityEffectivenessReport"));
            context.CreatePermission(PermissionNames.Pages_SummaryAgentActivityEffectiveness_Report_View, L("SummaryAgentActivityEffectivenessReport"));
            context.CreatePermission(PermissionNames.Pages_LoginAuditTrail_Report_View, L("LoginAuditTrailReport"));
            context.CreatePermission(PermissionNames.Pages_UpdateRun_View, L("RunUpdate"));
            context.CreatePermission(PermissionNames.Pages_AssignExternalAgents_View, L("AssignExternalAgents"));
            context.CreatePermission(PermissionNames.Pages_ValidFieldsForLists_View, L("ValidFieldsForListsView"));
            context.CreatePermission(PermissionNames.Pages_ValidFieldsForLists_Modify, L("ValidFieldsForListsModify"));
            context.CreatePermission(PermissionNames.Pages_MasterLists_View, L("MasterLists"));
            context.CreatePermission(PermissionNames.Pages_ChildLists_View, L("ChildLists"));
            context.CreatePermission(PermissionNames.Pages_RiskLists_View, L("RiskLists"));
            context.CreatePermission(PermissionNames.Menu_Security, L("Security"));
            context.CreatePermission(PermissionNames.Menu_Lists, L("Lists"));
            context.CreatePermission(PermissionNames.Menu_Maintenance, L("Maintenance"));
            context.CreatePermission(PermissionNames.Pages_Activity_Report_View, L("ActivityReport"));
            context.CreatePermission(PermissionNames.Pages_InternalAgentDashboard_View, L("InternalAgentDashboard"));
            context.CreatePermission(PermissionNames.Pages_DailyReport_View, L("DailyReport"));
            context.CreatePermission(PermissionNames.Pages_ProyeccionPerdida_View, L("ProyeccionPerdida"));
            context.CreatePermission(PermissionNames.Pages_PortfolioSupervisor_View, L("PortfolioSupervisor"));
            context.CreatePermission(PermissionNames.Pages_AssignAccounts_Report_View, L("AssignAccountsReport"));
            context.CreatePermission(PermissionNames.Pages_DuplicateAccounts_Report_View, L("DuplicateAccountsReport"));
            context.CreatePermission(PermissionNames.Pages_AssignRoutes_Report_View, L("AssignRoutes"));
            context.CreatePermission(PermissionNames.Pages_Users_Modify, L("UsersModify"));
            context.CreatePermission(PermissionNames.Pages_MasterLists_Modify, L("MasterListsModify"));
            context.CreatePermission(PermissionNames.Pages_ChildLists_Modify, L("ChildListsModify"));
            context.CreatePermission(PermissionNames.Pages_RiskLists_Modify, L("RiskListsModify"));
            context.CreatePermission(PermissionNames.Pages_DemographicFields_Modify, L("DemographicFieldsModify"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, SaexConsts.LocalizationSourceName);
        }
    }
}
