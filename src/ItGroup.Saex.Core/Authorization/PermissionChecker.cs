﻿using Abp.Authorization;
using ItGroup.Saex.Authorization.Roles;
using ItGroup.Saex.Authorization.Users;

namespace ItGroup.Saex.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
