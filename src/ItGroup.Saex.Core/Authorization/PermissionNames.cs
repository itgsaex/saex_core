﻿using System.Collections.Generic;

namespace ItGroup.Saex.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_ActivityCodes_Modify = "Pages.ActivityCodes.Modify";

        public const string Pages_ActivityCodes_View = "Pages.ActivityCodes.View";

        public const string Pages_AdjusterStates_Modify = "Pages.AdjusterStates.Modify";

        public const string Pages_AdjusterStates_View = "Pages.AdjusterStates.View";

        public const string Pages_AgentActivity_Modify = "Pages.AgentActivity.Modify";

        public const string Pages_AgentActivity_View = "Pages.AgentActivity.View";

        public const string Pages_Agents_Modify = "Pages.Agents.Modify";

        public const string Pages_Agents_View = "Pages.Agents.View";

        public const string Pages_Users_Report = "Pages.UsersReport";

        public const string Pages_KPI_Report_View = "Pages.KPIReport.View";

        public const string Pages_AuditLogs = "Pages.AuditLogs";

        public const string Pages_ReceivedVsAssignedCases_Report_View = "Pages.ReceivedVsAssignedCasesReport.View";

        public const string Pages_Profiles_Report_View = "Pages.ProfilesReport.View";

        public const string Pages_AgentSupervisors_Modify = "Pages.AgentSupervisor.Modify";

        public const string Pages_AgentSupervisors_View = "Pages.AgentSupervisor.View";

        public const string Pages_AssignCases_Modify = "Pages.AssignCases.Modify";

        public const string Pages_AssignCases_View = "Pages.AssignCases.View";

        public const string Pages_AssignRegions_Modify = "Pages.AssignRegions.Modify";

        public const string Pages_AssignRegions_View = "Pages.AssignRegions.View";

        public const string Pages_Audit_Report_Modify = "Pages.AuditReport.Modify";

        public const string Pages_Audit_Report_View = "Pages.AuditReport.View";

        public const string Pages_AuditLogs_Modify = "Pages.AuditLogs.Modify";

        public const string Pages_AuditLogs_View = "Pages.AuditLogs.View";

        public const string Pages_BaselineCasesImport_View = "Pages.BaselineCasesImport.View";

        public const string Pages_Buckets_Modify = "Pages.Buckets.Modify";

        public const string Pages_Buckets_View = "Pages.Buckets.View";

        public const string Pages_Cases_Modify = "Pages.Cases.Modify";

        public const string Pages_Cases_Report_Modify = "Pages.CasesReport.Modify";

        public const string Pages_Cases_Report_View = "Pages.CasesReport.View";

        public const string Pages_Case_Recent_Activity_Report_View = "Pages.CaseRecentActivityReport.View";

        public const string Pages_Cases_View = "Pages.Cases.View";

        public const string Pages_CaseTypes_Modify = "Pages.CaseTypes.Modify";

        public const string Pages_CaseTypes_View = "Pages.CaseTypes.View";

        public const string Pages_ClosingDates_Modify = "Pages.ClosingDates.Modify";

        public const string Pages_ClosingDates_View = "Pages.ClosingDates.View";

        public const string Pages_Conditions_Modify = "Pages.Conditions.Modify";

        public const string Pages_Conditions_View = "Pages.Conditions.View";

        public const string Pages_ContactTypes_Modify = "Pages.ContactTypes.Modify";

        public const string Pages_ContactTypes_View = "Pages.ContactTypes.View";

        public const string Pages_DelinquencyStates_Modify = "Pages.DelinquencyStates.Modify";

        public const string Pages_DelinquencyStates_View = "Pages.DelinquencyStates.View";

        public const string Pages_DemographicFields = "Pages.DemographicFields";
        public const string Pages_DemographicFields_Modify = "Pages.DemographicFields.Modify";

        public const string Pages_Import = "Pages.Import";

        public const string Pages_ListFieldCategories_Modify = "Pages.ListFieldCategories.Modify";

        public const string Pages_ListFieldCategories_View = "Pages.ListFieldCategories.View";

        public const string Pages_ListFields_Modify = "Pages.ListFields.Modify";

        public const string Pages_ListFields_View = "Pages.ListFields.View";

        public const string Pages_Lists_Modify = "Pages.Lists.Modify";

        public const string Pages_Lists_View = "Pages.Lists.View";

        public const string Pages_MacroStates_Modify = "Pages.MacroStates.Modify";

        public const string Pages_MacroStates_View = "Pages.MacroStates.View";

        public const string Pages_ResetUserPassword_View = "Pages.ResetUserPassword.View";

        public const string Pages_Messages_Modify = "Pages.Messages.Modify";

        public const string Pages_Messages_View = "Pages.Messages.View";

        public const string Pages_PaymentDelayReasons_Modify = "Pages.PaymentDelayReasons.Modify";

        public const string Pages_PaymentDelayReasons_View = "Pages.PaymentDelayReasons.View";

        public const string Pages_PostalCodes_Modify = "Pages.PostalCodes.Modify";

        public const string Pages_PostalCodes_View = "Pages.PostalCodes.View";

        public const string Pages_ProductRules_Modify = "Pages.ProductRules.Modify";

        public const string Pages_ProductRules_View = "Pages.ProductRules.View";

        public const string Pages_Products_Modify = "Pages.Products.Modify";

        public const string Pages_Products_View = "Pages.Products.View";

        public const string Pages_Regions_Modify = "Pages.Regions.Modify";

        public const string Pages_Regions_View = "Pages.Regions.View";

        public const string Pages_Reports_Modify = "Pages.Reports.Modify";

        public const string Pages_Reports_View = "Pages.Reports.View";

        public const string Pages_Roles_Modify = "Pages.Roles.Modify";

        public const string Pages_Roles_View = "Pages.Roles.View";

        public const string Pages_Routes_Modify = "Pages.Routes.Modify";

        public const string Pages_Routes_View = "Pages.Routes.View";

        public const string Pages_ScorecardParameters_Modify = "Pages.ScorecardParameters.Modify";

        public const string Pages_ScorecardParameters_View = "Pages.ScorecardParameters.View";

        public const string Pages_SystemPolicies_Modify = "Pages.SystemPolicies.Modify";

        public const string Pages_SystemPolicies_View = "Pages.SystemPolicies.View";

        public const string Pages_SystemStates_Modify = "Pages.SystemStates.Modify";

        public const string Pages_SystemStates_View = "Pages.SystemStates.View";

        public const string Pages_SystemSubStates_Modify = "Pages.SystemSubStates.Modify";

        public const string Pages_SystemSubStates_View = "Pages.SystemSubStates.View";

        public const string Pages_Tenants_View = "Pages.Tenants";

        public const string Pages_Users_View = "Pages.Users";

        public const string Pages_Users_Modify = "Pages.Users.Modify";

        public const string Pages_VisitPlaces_Modify = "Pages.VisitPlaces.Modify";

        public const string Pages_VisitPlaces_View = "Pages.VisitPlaces.View";

        public const string Pages_WorkingHours_Modify = "Pages.WorkingHours.Modify";

        public const string Pages_WorkingHours_View = "Pages.WorkingHours.View";

        public const string Pages_AddressManagement_Report_View = "Pages.AddressManagementReport.View";

        public const string Pages_UnassignedCases_Report_View = "Pages.UnassignedCasesReport.View";

        public const string Pages_UnfilteredCases_Report_View = "Pages.UnfilteredCasesByMasterReport.View";

        public const string Pages_TransferredCases_Report_View = "Pages.TransferredCasesReport.View";

        public const string Pages_AgentActivity_Report_View = "Pages.AgentActivityReport.View";

        public const string Pages_Activity_Report_View = "Pages.ActivityReport.View";

        public const string Pages_BucketsDelinquencyMortgage_Report_View = "Pages.BucketsDelinquencyMortgageReport.View";

        public const string Pages_BucketsDelinquencyMortgageWithIndicator_Report_View = "Pages.BucketsDelinquencyMortgageWithIndicatorReport.View";

        public const string Pages_AgentLocationRegistry_Report_View = "Pages.AgentLocationRegistryReport.View";

        public const string Pages_KPISolvedCases_Report_View = "Pages.KPISolvedCasesReport.View";

        public const string Pages_DailyAgentActivityEffectiveness_Report_View = "Pages.DailyAgentActivityEffectivenessReport.View";

        public const string Pages_SummaryAgentActivityEffectiveness_Report_View = "Pages.SummaryAgentActivityEffectivenessReport.View";

        public const string Pages_LoginAuditTrail_Report_View = "Pages.LoginAuditTrailReport.View";

        public const string Pages_UpdateRun_View = "Pages.UpdateRun.View";

        public const string Pages_AssignExternalAgents_View = "Pages.AssignExternalAgents.View";

        public const string Pages_ValidFieldsForLists_View = "Pages.ValidFieldsForLists.View";

        public const string Pages_ValidFieldsForLists_Modify = "Pages.ValidFieldsForLists.Modify";

        public const string Pages_MasterLists_View = "Pages.MasterLists";

        public const string Pages_MasterLists_Modify = "Pages.MasterLists.Modify";

        public const string Pages_ChildLists_View = "Pages.ChildLists";

        public const string Pages_ChildLists_Modify = "Pages.ChildLists.Modify";

        public const string Pages_RiskLists_View = "Pages.RiskLists";

        public const string Pages_RiskLists_Modify = "Pages.RiskLists.Modify";

        public const string Menu_Security = "Menu.Security";

        public const string Menu_Lists = "Menu.Lists";

        public const string Menu_Maintenance = "Menu.Maintenance";

        public const string Pages_InternalAgentDashboard_View = "Pages.InternalAgentDashboard.View";

        public const string Pages_DailyReport_View = "Pages.DailyReport.View";

        public const string Pages_ProyeccionPerdida_View = "Pages.ProyeccionPerdida.View";

        public const string Pages_PortfolioSupervisor_View = "Pages.PortfolioSupervisor.View";

        public const string Pages_AssignAccounts_Report_View = "Pages.AssignAccounts.View";
        public const string Pages_DuplicateAccounts_Report_View = "Pages.DuplicateAccounts.View";
        public const string Pages_AssignRoutes_Report_View = "Pages.AssignRoutes.View";

        public static List<string> ReportsPermissionsSection = new List<string>
        {
            //Reports
            Pages_Reports_Modify,
            Pages_Cases_Report_Modify,
            Pages_Cases_Report_View,
            Pages_Case_Recent_Activity_Report_View,
            Pages_KPI_Report_View,
            Pages_Users_Report,
            Pages_Activity_Report_View,
            Pages_AddressManagement_Report_View,
            Pages_TransferredCases_Report_View,
            Pages_BucketsDelinquencyMortgage_Report_View,
            Pages_BucketsDelinquencyMortgageWithIndicator_Report_View,
            Pages_AgentLocationRegistry_Report_View,
            Pages_KPISolvedCases_Report_View,
            Pages_LoginAuditTrail_Report_View,
            Pages_AuditLogs,
            Pages_ReceivedVsAssignedCases_Report_View,
            Pages_Profiles_Report_View,
            Pages_AssignAccounts_Report_View,
            Pages_DuplicateAccounts_Report_View,
            Pages_AssignRoutes_Report_View,

            //Audits
            Pages_Audit_Report_Modify,
            Pages_Audit_Report_View,
            Pages_AuditLogs_Modify,
            Pages_AuditLogs_View,
            Pages_AuditLogs_View,
        };

        public static List<string> SettingsPermissionsSection = new List<string>
        {
            Pages_SystemPolicies_Modify,
            Pages_SystemPolicies_View,
            Pages_SystemStates_Modify,
            Pages_SystemStates_View,
            Pages_SystemSubStates_Modify,
            Pages_SystemSubStates_View,
            Pages_ScorecardParameters_View,
            Pages_ScorecardParameters_Modify
        };

        public static List<string> MessagesPermissionsSection = new List<string>
        {
              Pages_Messages_Modify,
              Pages_Messages_View
        };

        public static List<string> MaintenancePermissionsSection = new List<string>
        {
           //Postal Codes
            Pages_PostalCodes_Modify,
            Pages_PostalCodes_View,

            //Routes
            Pages_Routes_Modify,
            Pages_Routes_View,

            //Regions
            Pages_Regions_Modify,
            Pages_Regions_View,

            //Adjuster
            Pages_AdjusterStates_Modify,
            Pages_AdjusterStates_View,

            //Assigned Region
            Pages_AssignRegions_Modify,
            Pages_AssignRegions_View,

            //Visit Places
            Pages_VisitPlaces_Modify,
            Pages_VisitPlaces_View,

            //Contacts Type
            Pages_ContactTypes_Modify,
            Pages_ContactTypes_View,

            //Payment Delay
            Pages_PaymentDelayReasons_Modify,
            Pages_PaymentDelayReasons_View,

            //Activity Codes
            Pages_ActivityCodes_Modify,
            Pages_ActivityCodes_View,

            //Closing Dates
            Pages_ClosingDates_Modify,
            Pages_ClosingDates_View,

            //Deliquency States
            Pages_DelinquencyStates_Modify,
            Pages_DelinquencyStates_View,

            //Valid Field
            Pages_ValidFieldsForLists_Modify,
            Pages_ValidFieldsForLists_View,

            //Assign Cases
            Pages_AssignCases_Modify,
            Pages_AssignCases_View,

            //Bukets
            Pages_Buckets_Modify,
            Pages_Buckets_View,

            //Coditions
            Pages_Conditions_Modify,
            Pages_Conditions_View,

            //Working Hours
            Pages_WorkingHours_Modify,
            Pages_WorkingHours_View,

            Pages_DemographicFields,
            Pages_DemographicFields_Modify,

            Pages_BaselineCasesImport_View,
            Pages_Import,

            //Macro States
            Pages_MacroStates_Modify,
            Pages_MacroStates_View,

            //Products
            Pages_Products_Modify,
            Pages_Products_View,
            Pages_ProductRules_Modify,
            Pages_ProductRules_View,

            //Tenants
            Pages_Tenants_View,

            //Update Run
            Pages_UpdateRun_View,

            //Cases
            Pages_Cases_Modify,
            Pages_Cases_View,
            Pages_CaseTypes_Modify,
            Pages_CaseTypes_View,
            Pages_UnassignedCases_Report_View,
            Pages_UnfilteredCases_Report_View
        };

        public static List<string> ListsPermissionsSection = new List<string>
        {
            Pages_MasterLists_View,
            Pages_MasterLists_Modify,
            Pages_ChildLists_View,
            Pages_ChildLists_Modify,
            Pages_RiskLists_View,
            Pages_RiskLists_Modify,


            Pages_ListFieldCategories_Modify,
            Pages_ListFieldCategories_View,

            Pages_ListFields_Modify,
            Pages_ListFields_View,

            Pages_Lists_View
        };

        public static List<string> SecurityPermissionsSection = new List<string>
        {
            Pages_Users_View,
            Pages_Users_Modify,
            Pages_Roles_View,
            Pages_Roles_Modify,
            Pages_ResetUserPassword_View
        };

        public static List<string> AgentActivitiesPermissionsSection = new List<string>
        {
            //Agents Activities
            Pages_Agents_Modify,
            Pages_Agents_View,
            Pages_AgentActivity_Modify,
            Pages_AgentActivity_View,
            Pages_AgentActivity_Report_View,
            Pages_DailyAgentActivityEffectiveness_Report_View,
            Pages_SummaryAgentActivityEffectiveness_Report_View
        };

        public static List<string> ManagerSupervisorInternalAgentPermissionsSection = new List<string>
        {
            Pages_AgentSupervisors_Modify,
            Pages_AgentSupervisors_View,
            Pages_AssignExternalAgents_View,
            Pages_InternalAgentDashboard_View,
            Pages_DailyReport_View,
            Pages_ProyeccionPerdida_View,
            Pages_PortfolioSupervisor_View
        };
    }
}
