﻿using System;
using System.Collections.Generic;
using Abp.Authorization.Users;
using Abp.Extensions;
using ItGroup.Saex.DomainEntities;
using ItGroup.Saex.DomainEntities.Messages;

namespace ItGroup.Saex.Authorization.Users
{
    public class User : AbpUser<User>, ICloneable
    {
        public const string DefaultPassword = "123qwe";
        public bool PasswordReset { get; set; }
        public string MotherSurname { get; set; }

        public Agent Agent { get; set; }

        public string AlternatePhone { get; set; }

        public virtual List<SupervisorInternalAgent> SupervisorInternalAgents { get; set; }

        public virtual List<SupervisorInternalAgent> InternalAgentSupervisors { get; set; }

        public virtual List<InternalAgentExternalAgent> InternalAgentExternalAgents { get; set; }

        public virtual List<InternalAgentExternalAgent> ExternalAgentInternalAgents { get; set; }

        public virtual List<InternalAgentComment> InternalAgentComments { get; set; }

        public virtual List<InternalAgentPromise> InternalAgentPromises { get; set; }
        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                EmailAddress = emailAddress,
                Roles = new List<UserRole>()
            };

            user.SetNormalizedNames();

            return user;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}