﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ItGroup.Saex.Enumerations
{
    public class Enumerations
    {
        public enum ActivityCodeType
        {
            [Description("Activity Code")]
            ActivityCode = 1,

            [Description("Reason Code")]
            ReasonCode = 4,

            [Description("Reaction Code")]
            ReactionCode = 3,

            [Description("Action Code")]
            ActionCode = 2,
        }

        public enum AddressType
        {
            [Description("Principal")]
            Principal = 1,

            [Description("Visit")]
            Visit = 2,
        }

        public enum AgentType
        {
            [Description("Internal")]
            Internal = 1,

            [Description("External")]
            External = 2,

            [Description("Supervisor")]
            Supervisor = 3,

            [Description("Manager")]
            Manager = 4,
        }

        public enum AuditLogOperationType
        {
            [Description("View")]
            View = 1,

            [Description("Create")]
            Create = 2,

            [Description("Update")]
            Update = 3,

            [Description("Delete")]
            Delete = 4,
        }

        public enum CaseEventType
        {
            [Description("Activity")]
            Activity = 1,

            [Description("Appointment")]
            Appointment = 2,

            [Description("Current Day")]
            CurrentDay = 3,
        }

        public enum ClosingDateType
        {
            [Description("Closing Date")]
            ClosingDate = 1,

            [Description("Holiday")]
            Holiday = 2,
        }

        public enum DateRangeType
        {
            //[Description("All Dates")]
            //AllDates = -1,

            [Description("Custom")]
            Custom = 0,

            [Description("Last Week")]
            LastWeek = 1,

            [Description("Last Month")]
            LastMonth = 2,

            [Description("Last Trimester")]
            LastTrimester = 3,

            [Description("Last Year")]
            LastYear = 4,

            [Description("This Week")]
            ThisWeek = 5,

            [Description("This Month")]
            ThisMonth = 6,

            [Description("This Year")]
            ThisYear = 7,
        }

        public enum DayOfTheWeek
        {
            [Description("Sunday")]
            Sunday = 1,

            [Description("Monday")]
            Monday = 2,

            [Description("Tuesday")]
            Tuesday = 3,

            [Description("Wednesday")]
            Wednesday = 4,

            [Description("Thursday")]
            Thursday = 5,

            [Description("Friday")]
            Friday = 6,

            [Description("Saturday")]
            Saturday = 7,
        }

        public enum ImportType
        {
            [Description("Unknown")]
            Unknown = 0,

            [Description("Automatic")]
            Automatic = 1,

            [Description("Manual")]
            Manual = 2,
        }

        public enum KpiReportDateRangeType
        {
            [Description("Manual")]
            Manual = 0,

            [Description("Today")]
            Today = 1,

            [Description("Week")]
            Week = 2,

            [Description("Month")]
            Month = 3,
        }

        public enum ListType
        {
            [Description("Unknown")]
            Unknown = 0,

            [Description("Master")]
            Master = 1,

            [Description("Child")]
            Child = 2,

            [Description("Risk")]
            Risk = 3,
        }

        public enum ProductType
        {
            [Description("Unknown")]
            Unknown = 0,

            [Description("KO")]
            KO = 1,

            [Description("CACS")]
            CACS = 2,
        }

        public enum QuestionType
        {
            [Description("Text")]
            Text = 1,

            [Description("Number")]
            Number = 2,

            [Description("Date")]
            Date = 3,

            [Description("Select")]
            Select = 4,

            [Description("Telephone")]
            Telephone = 5,
        }

        public enum RuleType
        {
            [Description("Condition")]
            Condition = 1,

            [Description("Bucket")]
            Bucket = 2,
        }

        public enum SortOrder
        {
            Ascending = 1,

            Descending = 2
        }

        public enum SupervisorType
        {
            [Description("Supervisor")]
            Supervisor = 1,

            [Description("Manager")]
            Manager = 2,

            [Description("InternalAgent")]
            InternalAgent = 3,
        }

        public enum TransferType
        {
            [Description("Unknown")]
            Unknown = 0,

            [Description("Automatic Process")]
            AutomaticProcess = 1,

            [Description("Transfer")]
            Transfer = 2,

            [Description("Shared")]
            Shared = 3,
        }

        public enum UserStatus
        {
            [Description("Active")]
            Active = 1,

            [Description("Inactive")]
            Inactive = 2,

            [Description("Deleted")]
            Deleted = 3,
        }

        public class EnumerationModel
        {
            public EnumerationModel(DayOfTheWeek fake)
            {
                Members = new List<EnumerationModel>();

                foreach (var member in EnumsNET.Enums.GetMembers<DayOfTheWeek>())
                {
                    var enumModel = new EnumerationModel()
                    {
                        Name = member.Name,
                        Value = (int)member.Value,
                    };

                    Members.Add(enumModel);
                }
            }

            public EnumerationModel(ClosingDateType fake)
            {
                Members = new List<EnumerationModel>();

                foreach (var member in EnumsNET.Enums.GetMembers<ClosingDateType>())
                {
                    var enumModel = new EnumerationModel()
                    {
                        Name = member.Name,
                        Value = (int)member.Value,
                    };

                    Members.Add(enumModel);
                }
            }

            public EnumerationModel(RuleType fake)
            {
                Members = new List<EnumerationModel>();

                foreach (var member in EnumsNET.Enums.GetMembers<RuleType>())
                {
                    var enumModel = new EnumerationModel()
                    {
                        Name = member.Name,
                        Value = (int)member.Value,
                    };

                    Members.Add(enumModel);
                }
            }

            public EnumerationModel(AgentType fake)
            {
                Members = new List<EnumerationModel>();

                foreach (var member in EnumsNET.Enums.GetMembers<AgentType>())
                {
                    string name = "";
                    switch (member.Name.ToLower())
                    {
                        case "internal":
                            name = "Agente Interno";
                            break;
                        case "external":
                            name = "Agente Externo";
                            break;
                        case "manager":
                            name = "Gerente";
                            break;
                        default:
                            name = member.Name;
                            break;
                    }
                    var enumModel = new EnumerationModel()
                    {
                        Name = name,
                        Value = (int)member.Value,
                    };

                    Members.Add(enumModel);
                }
            }

            public EnumerationModel(DateRangeType fake)
            {
                Members = new List<EnumerationModel>();

                foreach (var member in EnumsNET.Enums.GetMembers<DateRangeType>())
                {
                    var descriptionAtribute = member.Attributes[0] as DescriptionAttribute;
                    var enumModel = new EnumerationModel()
                    {
                        Name = descriptionAtribute.Description,
                        Value = (int)member.Value,
                    };

                    Members.Add(enumModel);
                }
            }

            public EnumerationModel(ActivityCodeType fake)
            {
                Members = new List<EnumerationModel>();

                foreach (var member in EnumsNET.Enums.GetMembers<ActivityCodeType>())
                {
                    var enumModel = new EnumerationModel()
                    {
                        Name = member.Name,
                        Value = (int)member.Value,
                    };

                    Members.Add(enumModel);
                }
            }

            public EnumerationModel(QuestionType fake)
            {
                Members = new List<EnumerationModel>();

                foreach (var member in EnumsNET.Enums.GetMembers<QuestionType>())
                {
                    var enumModel = new EnumerationModel()
                    {
                        Name = member.Name,
                        Value = (int)member.Value,
                    };

                    Members.Add(enumModel);
                }
            }

            public EnumerationModel(SupervisorType fake)
            {
                Members = new List<EnumerationModel>();

                foreach (var member in EnumsNET.Enums.GetMembers<SupervisorType>())
                {
                    var enumModel = new EnumerationModel()
                    {
                        Name = member.Name,
                        Value = (int)member.Value,
                    };

                    Members.Add(enumModel);
                }
            }

            private EnumerationModel()
            { }

            public string Description { get; set; }

            public List<EnumerationModel> Members { get; set; }

            public string Name { get; set; }

            public int Value { get; set; }
        }
    }
}
