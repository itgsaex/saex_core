﻿using Abp.MultiTenancy;
using ItGroup.Saex.Authorization.Users;

namespace ItGroup.Saex.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
