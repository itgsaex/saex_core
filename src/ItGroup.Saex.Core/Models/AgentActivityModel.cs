﻿using ItGroup.Saex.DomainEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ItGroup.Saex.Models
{
    public class AgentActivityModel
    {
        public AgentActivityModel(CaseActivity caseActivity)
        {
            Type = caseActivity.Case.CaseType.Description;

            if (caseActivity.VisitPlace != null)
                Place = caseActivity.VisitPlace.Description;
            else
                Place = "";

            Date = caseActivity.Date;

            Product = caseActivity.Case.CaseType.ProductNumber;

            IconText = caseActivity.Case.CaseType.IconText;

            AccountNumber = caseActivity.Case.AccountNumber;
            Name = caseActivity.Case.Fullname;

            if (caseActivity.Latitude.HasValue)
                Latitude = caseActivity.Latitude.Value;
            else
                Latitude = 0;

            if (caseActivity.Longitude.HasValue)
                Longitude = caseActivity.Longitude.Value;
            else
                Longitude = 0;

            Activity = "Actividad";
        }

        public AgentActivityModel(AgentGPS agentGPS)
        {
            Type = "";
            Place = "";
            Date = agentGPS.Date;
            Product = "";
            AccountNumber = "";
            Name = "";
            Latitude = agentGPS.Latitude;
            Longitude = agentGPS.Longitude;
            Activity = "Lectura Automatica";
            IconText = "";
        }

        public string Type { get; set; }
        public string Activity { get; set; }
        public string Place { get; set; }
        public DateTime Date { get; set; }
        public string Product { get; set; }
        public string AccountNumber { get; set; }
        public string Name { get; set;}
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string IconText { get; set; }
    }
}