﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.Models
{
    public class CaseReportModel : PagedResultRequestDto
    {
        public CaseReportModel()
        {
        }

        public string SearchText { get; set; }

        public SortOrder SortOrder { get; set; }

        public string ProductNumber { get; set; }
        public string NumeroCuenta { get; set; }
        public string Nombre { get; set; }
        public string ZipCode { get; set; }
        public bool Obligatorio { get; set; }
        public string CasoID { get; set; }
        public decimal MontoVencido { get; set; }
        public string StateCode { get; set; }
        public int DiasVencimiento { get; set; }
        public string EtapasDelincuencia { get; set; }
        public string LastActivityCode { get; set; }
        public string ComentarioExterno { get; set; }
        public string Ruta { get; set; }
        public string TotalDELQAmount { get; set; }
        public string PaymentAmount { get; set; }
        public string ComentarioInterno { get; set; }
        public string LastPaymentDate { get; set; }
        public string EstatusSistema { get; set; }
        public string CaseAssignType { get; set; }
        public string FechaEntradaCACS { get; set; }
    }

    public class CaseReportSearchModel/* : PagedResultRequestDto*/
    {
        public CaseReportSearchModel()
        {
        }

        public string AccountNumber { get; set; }

        public Guid? AgentId { get; set; }

        public string DelinquencyStateId { get; set; }

        public string Name { get; set; }

        public Guid? PostalCodeId { get; set; }

        public Guid? RegionId { get; set; }

        public Guid? RouteId { get; set; }

        public string AssignType { get; set; }

        //public SortOrder SortOrder { get; set; }
    }

    public class InterfacelogModel
    {
        public int Compid { get; set; }
        public string Route { get; set; }
        public string InterfaceName { get; set; }
        public string TableName { get; set; }
        public string BegEnd { get; set; }
        public DateTime InterfaceDate { get; set; }
        public int ProcessStatus { get; set; }
    }

    public class ErrorlogModel
    {
        public string ErrorDescription { get; set; }
        public DateTime ErrorDate { get; set; }
    }

    public class ReadInternalAgentCasesParametersModel
    {
        public ReadInternalAgentCasesParametersModel()
        {
        }

        public long? Supervisor { get; set; }
        public long? InternalAgent { get; set; }
        public string InternalAgentUsername { get; set; }
        public long? ExternalAgent { get; set; }
        public string ExternalAgentUsername { get; set; }
        public int? RegionId { get; set; }
        public int? RutaId { get; set; }
        public string CodigoPostal { get; set; }
        public string ActivityCode { get; set; }
        public string EtapaDeDelincuencia { get; set; }
        public int? AlertId { get; set; }
        public bool? Referido { get; set; }
        public bool? IsWorked { get; set; }
        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public string Estado { get; set; }
        public string Status { get; set; }
    }

    public class InternalAgentDashboardModel
    {
        public string CaseID { get; set; }
        public string V1CaseID { get; set; }
        public string ProductNumber { get; set; }
        public string AccountNumber { get; set; }
        public string Fullname { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public decimal? OwedAmount { get; set; }
        public string EtapasDeDelicuencia { get; set; }
        public string LastActivityCode { get; set; }
        public string LastActivityCodeComment { get; set; }
        public string ComentarioExterno { get; set; }
        public string NewComment { get; set; }
        public string EstatusSistema { get; set; }
        public string FechaEntradaCACS { get; set; }
        public string StateCode { get; set; }
        public string Alerta { get; set; }
        public int? DiasVencimiento { get; set; }
        public string Ruta { get; set; }
        public decimal? TotalDELQAmount { get; set; }
        public decimal? PaymentAmount { get; set; }
        public string TipoPromesa { get; set; }
        public string LastPaymentDate { get; set; }
        public string UltimoComentario { get; set; }
        public bool? EsTrabajada { get; set; }
        public string Comentario { get; set; }
        public string FechaComentario { get; set; }
    }
    public class DailyReportParameterModel
    {
        public string ExternalAgent { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class DailyReportModel
    {
        public DateTime? Fecha { get; set; }
        public string ActivityCode { get; set; }
        public string ActivityCodeDesc { get; set; }
        public string NumeroCuenta { get; set; }
        public string Nombre { get; set; }
        public string ProductNumber { get; set; }
        public Decimal? MontoVencido { get; set; }
        public string Pueblo { get; set; }
        public string ZipCode { get; set; }
        public int? DiasVencimiento { get; set; }
        public string Comentario { get; set; }
        public string TipoProducto { get; set; }
        public string TipoContacto { get; set; }
        public string LugarVisitado { get; set; }
    }

    public class PlannedCaseWitoutVisitsModel
    {
        public DateTime? Fecha { get; set; }
        public string NumeroCuenta { get; set; }
        public string Nombre { get; set; }
        public string ProductNumber { get; set; }
        public Decimal? MontoVencido { get; set; }
        public string Pueblo { get; set; }
        public string ZipCode { get; set; }
        public int? DiasVencimiento { get; set; }
        public string TipoProducto { get; set; }
    }

    public class ProyeccionPerdidaParameterModel
    {
        public long Supervisor { get; set; }
        public string EtapaDelincuencia { get; set; }
    }

    public class TansferModel
    {
        public long SelectedInternalAgent { get; set; }
        public long SelectedInternalToAgent { get; set; }
        public string AsignationType { get; set; }
        public List<string> SelectedCases { get; set; }
    }

    public class ProyeccionPerdidaModel
    {
        public string Agente { get; set; }
        public string Asignacion { get; set; }
        public int TotalCasos { get; set; }
        public Decimal BalanceOriginal { get; set; }

        public string BalanceOriginalFormat
        {
            get
            {
                return BalanceOriginal.ToString("C2");
            }
        }
        public Decimal BalanceActual { get; set; }
        public string BalanceActualFormat
        {
            get
            {
                return BalanceActual.ToString("C2");
            }
        }
        public Decimal BalancePagado { get; set; }
        public string BalancePagadoFormat
        {
            get
            {
                return BalancePagado.ToString("C2");
            }
        }
        public Decimal PromesasFirmes { get; set; }
        public string PromesasFirmesFormat
        {
            get
            {
                return PromesasFirmes.ToString("C2");
            }
        }
        public Decimal PromesasDudosas { get; set; }
        public string PromesasDudosasFormat
        {
            get
            {
                return PromesasDudosas.ToString("C2");
            }
        }
        public int TotalCasosPromesasFirmes { get; set; }
        public int TotalCasosPromesasDudosas { get; set; }
        public Decimal Actual { get; set; }
        public string ActualFormat
        {
            get
            {
                return Actual.ToString("P2");
            }
        }
        public Decimal Real { get; set; }
        public Decimal MejorEscenario { get; set; }
    }

    public class ReadCuentasAsignadasParameterModel
    {
        public string AgentID { get; set; }
        public int? RegionID { get; set; }
        public int? RouteID { get; set; }
    }

    public class ReadCuentasAsignadasModel
    {
        public string ProductNumber { get; set; }
        public string NumeroCuenta { get; set; }
        public string Nombre { get; set; }
        public string ZipCode { get; set; }
        public bool Obligatorio { get; set; }
        public string CasoID { get; set; }
        public string MontoVencido { get; set; }
        public string Pueblo { get; set; }
        public string Estatus { get; set; }
        public string TipoTransferencia { get; set; }
        public string FechaAsignacion { get; set; }
    }

    public class ReadDuplicateAccountsParameterModel
    {
        public string AgentID { get; set; }
        public int? RegionID { get; set; }
        public int? RouteID { get; set; }
    }

    public class ReadDuplicateAccountsModel
    {
        public string ProductNumber { get; set; }
        public string NumeroCuenta { get; set; }
        public string Nombre { get; set; }
        public string ZipCode { get; set; }
        public string CasoID { get; set; }
        public string MontoVencido { get; set; }
        public string Pueblo { get; set; }
        public string Estatus { get; set; }
        public string AgenteID { get; set; }
        public string AgenteNombre { get; set; }
    }

    public class RutasAsignadasParameterModel
    {
        public RutasAsignadasParameterModel()
        {
            FromDate = DateTime.Today;
            ToDate = DateTime.Today;
        }

        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }
    }

    public class RutasAsignadasModel
    {
        public string NumeroCuenta { get; set; }
        public string Descripcion { get; set; }
        public string ZipCode { get; set; }
        public string RutaActual { get; set; }
        public string Agente { get; set; }
        public string Estatus { get; set; }
        public string TransferOrShared { get; set; }
    }
}
