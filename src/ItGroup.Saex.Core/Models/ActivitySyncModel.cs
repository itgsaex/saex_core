﻿using System;
using System.Collections.Generic;
using ItGroup.Saex.DomainEntities;

namespace ItGroup.Saex.Web.Host.Models
{
    public class ActivitySyncModel
    {
        public CaseActivity CaseActivity { get; set; }
        public List<ActivityDetail> ActivityDetails { get; set; }
        public List<ActivityLine> ActivityLines { get; set; }
        public List<ActivityReactionReasonDetail> ActivityReactionReasonDetails { get; set; }
    }
}
