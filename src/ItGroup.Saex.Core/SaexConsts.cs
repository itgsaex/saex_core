﻿namespace ItGroup.Saex
{
    public class SaexConsts
    {
        public const string LocalizationSourceName = "Saex";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = false;
    }
}
