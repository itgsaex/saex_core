﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class RoutePostalCode : Entity<Guid>
    {
        public virtual PostalCode PostalCode { get; set; }

        public Guid PostalCodeId { get; set; }

        public virtual Route Route { get; set; }

        public Guid RouteId { get; set; }
    }
}