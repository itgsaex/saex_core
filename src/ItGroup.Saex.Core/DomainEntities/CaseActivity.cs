﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Repositories;
using ItGroup.Saex.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseActivity : FullAuditedEntity<Guid>, ICloneable
    {
        public virtual IEnumerable<CaseActivityCode> ActivityCodes { get; set; }

        public virtual Agent Agent { get; set; }

        public Guid AgentId { get; set; }

        public virtual Case Case { get; set; }

        public Guid CaseId { get; set; }

        public string Comments { get; set; }

        public virtual ContactType ContactType { get; set; }

        public Guid ContactTypeId { get; set; }

        public virtual PaymentDelayReason PaymentDelayReason { get; set; }

        public Guid PaymentDelayReasonId { get; set; }

        public virtual VisitPlace VisitPlace { get; set; }

        public Guid VisitPlaceId { get; set; }

        public DateTime Date { get; set; }

        public int Status { get; set; }

        public string VisitedPlace { get; set; }

        public string ContactedPerson { get; set; }

        public string DelayReason { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public int TransStatus { get; set; }

        public DateTime? TransferDate { get; set; }

        public DateTime? ExportDate { get; set; }

        public DateTime? VisitEndDate { get; set; }

        public double? VisitEndLatitude { get; set; }

        public double? VisitEndLongitude { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class ActivityDetail : AuditedEntity<Guid>
    {
        public Guid ActivityId { get; set; }

        public string ActivityCodeID { get; set; }

        public string Result { get; set; }

        public string Comment { get; set; }

        public int Type { get; set; }

        public bool Required { get; set; }

        public string Parameters { get; set; }

        public string Question { get; set; }
    }

    public class ActivityLine : AuditedEntity<Guid>
    {
        public Guid ActivityId { get; set; }

        public string ActivityCodeID { get; set; }


        public string VisitedPlace { get; set; }

        public string ContactedPerson { get; set; }

        public string DelayReason { get; set; }

        public string Comment { get; set; }

        public string ReasonCode { get; set; }

        public string ReactionCode { get; set; }
    }

    public class ActivityReactionReasonDetail : AuditedEntity<Guid>
    {
        public Guid ActivityId { get; set; }

        public string ActivityCodeID { get; set; }

        public Guid QuestionId { get; set; }

        public string Question { get; set; }

        public int DetailType { get; set; }

        public string Comment { get; set; }

        public int Type { get; set; }

        public bool Required { get; set; }

        public string Parameters { get; set; }

        public string Result { get; set; }
    }

    public class CaseRecentActivity : Entity<Guid>
    {
        public Guid Id { get; set; }

        public DateTime CreationTime { get; set; }

        public string ActionCode { get; set; }
        
        public string ActionDescription { get; set; }

        public string ReactionCode { get; set; }

        public string ReactionDescription { get; set; }

        public string ReasonCode { get; set; }

        public string ReasonDescription { get; set; }

        public string Comment { get; set; }
    }

    public interface ICaseRecentActivityRepository : IRepository<CaseRecentActivity, Guid>
    {
        Task<List<CaseRecentActivity>> GetRecentActivities(string caseNumber);
        Task<List<CaseReportModel>> GetCases(CaseReportSearchModel model);
        void SetCaseMandatory(string caseNumber, string newValue);
        Task<List<InterfacelogModel>> GetLastRunInterfaceLog();
        Task<List<ErrorlogModel>> GetErrorLog();
        Task<int> ExecuteCaseProcessing();
        Task<List<CaseFieldHelper>> ReadCaseFields(Guid? product);
        void SaveCaseFields(List<CaseFieldHelper> data);
        Task<List<TestQueryHelper>> TestQuery(ListType listType, string tableName, string whereClause, string fieldsToReturn, string fieldsToPivot, string temporaryTable);
        Task<List<InternalAgentDashboardModel>> ReadInternalAgentCases(ReadInternalAgentCasesParametersModel model);
        Task<List<DailyReportModel>> DailyReport(DailyReportParameterModel model);
        Task<List<PlannedCaseWitoutVisitsModel>> PlannedCaseWitoutVisits(DailyReportParameterModel model);
        Task<List<ProyeccionPerdidaModel>> ProyeccionPerdidas(ProyeccionPerdidaParameterModel model);
        Task<List<ReadCuentasAsignadasModel>> ReadCuentasAsignadas(ReadCuentasAsignadasParameterModel model);
        Task<List<ReadDuplicateAccountsModel>> ReadDuplicateAccounts(ReadDuplicateAccountsParameterModel model);
        Task<List<RutasAsignadasModel>> RutasAsignadas(RutasAsignadasParameterModel model);
    }
}
