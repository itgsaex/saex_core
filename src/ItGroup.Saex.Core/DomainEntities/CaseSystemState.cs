﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseSystemState : FullAuditedEntity<Guid>
    {
        public Case Case { get; set; }

        public Guid CaseId { get; set; }

        public SystemState SystemState { get; set; }

        public Guid? SystemStateId { get; set; }

        public string Value { get; set; }
    }
}
