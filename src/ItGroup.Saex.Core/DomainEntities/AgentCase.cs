﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using ItGroup.Saex.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentCase : FullAuditedEntity<Guid>, ICloneable
    {
        public virtual Agent Agent { get; set; }

        public Guid AgentId { get; set; }

        public virtual Case Case { get; set; }

        public Guid CaseId { get; set; }

        public bool IsActive { get; set; }

        [ForeignKey("TransferedById")]
        public User TransferedBy { get; set; }

        public long? TransferedById { get; set; }

        public TransferType Type { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
