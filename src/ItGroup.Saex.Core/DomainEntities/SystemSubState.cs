﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemSubState : FullAuditedEntity<Guid>
    {
        public SystemSubState() { }

        public SystemSubState(string code, string description)
        {
            Code = code;
            Description = description;
        }

        public string Code { get; set; }

        public string Description  { get; set; }

        public virtual IEnumerable<SystemState> SystemStates { get; set; }
    }
}