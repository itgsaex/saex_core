﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class List : FullAuditedEntity<Guid>, ICloneable
    {
        public string Name { get; set; }

        public ListType Type { get; set; }

        public bool IsActive { get; set; }

        public string Query { get; set; }

        public string SerializedQuery { get; set; }

        public int ReferenceId { get; set; }

        public string FieldsToPivot { get; set; }

        public int EstadoRiesgoId { get; set; }
        public string ProductSystemTable { get; set; }

        public virtual IEnumerable<Case> Cases { get; set; }

        public virtual IEnumerable<ListField> Fields { get; set; }

        public IEnumerable<AgentList> Agents { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}