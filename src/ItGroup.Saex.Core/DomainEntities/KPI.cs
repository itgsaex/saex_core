﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;

namespace ItGroup.Saex.DomainEntities
{
    public class KPI : Entity<Guid>
    {
        public KPI() { }

        [MaxLength(50)]
        public string AgenteID { get; set; }
        [MaxLength(50)]
        public string GrupoKPI { get; set; }
        [MaxLength(50)]
        public string NombreKPI { get; set; }

        public float ActualDia { get; set; }

        public float ActualSemana { get; set; }

        public float ActualMes { get; set; }

        public float MetaDia { get; set; }

        public float MetaSemana { get; set; }

        public float MetaMes { get; set; }

        public float PctDia { get; set; }

        public float PctSemana { get; set; }

        public float PctMes { get; set; }
    }
}
