﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldType : FullAuditedEntity<Guid>
    {
        public string Name { get; set; }

        public string DefaultControl { get; set; }

        public string DataType { get; set; }

        public string Properties { get; set; }

        public bool IsActive { get; set; }

        public virtual IEnumerable<DemographicField> Fields { get; set; }
    }
}
