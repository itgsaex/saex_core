﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class Condition : FullAuditedEntity<Guid>, ICloneable
    {
        public string Description { get; set; }

        public string Value { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}