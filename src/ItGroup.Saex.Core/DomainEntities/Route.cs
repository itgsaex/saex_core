﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class Route : FullAuditedEntity<Guid>, ICloneable
    {
        public virtual IEnumerable<AgentRoute> Agents { get; set; }

        public virtual IEnumerable<Case> Cases { get; set; }

        public string Name { get; set; }

        public int? RouteId { get; set; }

        public virtual IEnumerable<RoutePostalCode> PostalCodes { get; set; }

        public virtual IEnumerable<RouteProduct> Products { get; set; }

        public virtual IEnumerable<RegionRoute> Regions { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
