﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using ItGroup.Saex.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class InternalAgentComment : Entity<Guid>
    {
        public long InternalAgentId { get; set; }

        [ForeignKey("InternalAgentId")]
        public virtual User InternalAgent { get; set; }

        public Guid CaseID { get; set; }

        [ForeignKey("CaseID")]
        public virtual Case Case { get; set; }

        [MaxLength(500)]
        public string Comment { get; set; }

        public DateTime Date { get; set; }
    }

    public class InternalAgentPromise : Entity<Guid>
    {
        public long InternalAgentId { get; set; }

        [ForeignKey("InternalAgentId")]
        public virtual User InternalAgent { get; set; }

        public Guid CaseID { get; set; }

        [ForeignKey("CaseID")]
        public virtual Case Case { get; set; }

        [MaxLength(100)]
        public string Promise { get; set; }

        public DateTime Date { get; set; }
    }
}