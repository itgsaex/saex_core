﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ActivityCode : FullAuditedEntity<Guid>, ICloneable
    {
        public string Code { get; set; }

        public string CaseType { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public bool IsGlobal { get; set; }

        public ActivityCodeType Type { get; set; }

        public virtual IEnumerable<Question> Questions { get; set; }

        public virtual IEnumerable<CaseActivityCode> CaseActivities { get; set; }

        public virtual IEnumerable<ActivityCodeDetail> ActivityCodeDetails { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        //TODO: Validate this relationship
        //public virtual ActivityCodeDelayReason DelayReason { get; set; }

        //public virtual CaseActivityCode DelayReason { get; set; }
    }
}