﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseTactic : FullAuditedEntity<Guid>, ICloneable
    {
        public virtual Agent Agent { get; set; }

        public Guid AgentId { get; set; }

        [ForeignKey("CaseId")]
        public virtual Case Case { get; set; }

        public Guid CaseId { get; set; }

        public int CollectionDayOfMonth { get; set; }

        public string CollectionTimeEnd { get; set; }

        public string CollectionTimeStart { get; set; }

        public bool IsPlanned { get; set; }

        public string VisitDays { get; set; }

        public string VisitEndTime { get; set; }

        public virtual VisitPlace VisitPlace { get; set; }

        public Guid VisitPlaceId { get; set; }

        public string VisitStartTime { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
