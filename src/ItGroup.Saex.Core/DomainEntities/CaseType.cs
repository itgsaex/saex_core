﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseType : FullAuditedEntity<Guid>, ICloneable
    {
        public Boolean UseActionCode { get; set; }

        public string IconText { get; set; }

        public string ProductNumber { get; set; }

        public string TipoListaMaestra { get; set; }

        public virtual IEnumerable<Case> Cases { get; set; }

        public string Code { get; set; }

        public string DefaultColor { get; set; }

        public string Description { get; set; }

        public string IconImage { get; set; }

        public string MasterListType { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        public Guid? ProductId { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
