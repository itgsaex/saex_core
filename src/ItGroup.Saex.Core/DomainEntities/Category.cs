﻿using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
namespace ItGroup.Saex.DomainEntities
{
    public class Category : FullAuditedEntity<Guid>
    {
        public int CategoryId { get; set; }
        [MaxLength(100)]
        public string Description { get; set; }
        public int CategorySequence { get; set; }
    }
}
