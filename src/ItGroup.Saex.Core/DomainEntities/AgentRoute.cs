﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentRoute : Entity<Guid>
    {
        public virtual Agent Agent { get; set; }

        public Guid AgentId { get; set; }

        public virtual Route Route { get; set; }

        public Guid RouteId { get; set; }
    }
}