﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class ListFieldCategory : FullAuditedEntity<Guid>, ICloneable
    {
        public string Code { get; set; }

        public string Description  { get; set; }

        public virtual IEnumerable<ListField> ListFields { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}