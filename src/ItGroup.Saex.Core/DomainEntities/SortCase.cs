﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities.Auditing;

namespace ItGroup.Saex.DomainEntities
{
    public class SortCase : FullAuditedEntity<Guid>
    {
        [MaxLength(10)]
        public string AgenteID { get; set; }
        [MaxLength(50)]
        public string CasoID { get; set; }
        public int SortIndex { get; set; }
    }
}
