﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class MacroState : FullAuditedEntity<Guid>, ICloneable
    {
        public string Code { get; set; }

        public string Description  { get; set; }

        public virtual IEnumerable<AdjusterState> AdjusterStates { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}