﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class AuditLog : FullAuditedEntity<Guid>
    {
        public AuditLog()
        {
        }

        public AuditLog(string tableName, string recordId, string operationType, string previousValue, string newValue, string creatorUsername, string description)
        {
            CreatorUsername = creatorUsername;
            Description = description;
            NewValue = newValue;
            OperationType = operationType;
            PreviousValue = previousValue;
            RecordId = recordId;
            TableName = tableName;
        }

        public string CreatorUsername { get; set; }

        public string Description { get; set; }

        public string NewValue { get; set; }

        public string OperationType { get; set; }

        public string PreviousValue { get; set; }

        public string RecordId { get; set; }

        public string TableName { get; set; }
    }
}
