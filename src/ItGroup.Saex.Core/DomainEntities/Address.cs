﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class Address : FullAuditedEntity<Guid>, ICloneable
    {
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public bool IsPrincipal { get; set; }

        public float? Latitude { get; set; }

        public float? Longitude { get; set; }

        public string Name { get; set; }

        public virtual PostalCode PostalCode { get; set; }

        public Guid PostalCodeId { get; set; }

        public string State { get; set; }

        public AddressType Type { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
