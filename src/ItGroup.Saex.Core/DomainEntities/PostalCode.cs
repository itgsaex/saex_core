﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class PostalCode : FullAuditedEntity<Guid>, ICloneable
    {
        public PostalCode()
        {
        }

        public PostalCode(Guid id, string code, string area)
        {
            Id = id;
            Code = code;
            Area = area;
        }

        public string Area { get; set; }

        //  public IEnumerable<Case> Cases { get; set; }

        public string Code { get; set; }

        public IEnumerable<RoutePostalCode> Routes { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
