﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using ItGroup.Saex.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    //public class SupervisedAgents : FullAuditedEntity<Guid>
    //{
    //    public Guid SupervisorId { get; set; }

    //    [ForeignKey("AgentId")]
    //    public virtual Agent Agent { get; set; }

    //    public Guid AgentId { get; set; }

    //    public SupervisorType Type { get; set; }
    //}

    public class SupervisorInternalAgent : Entity<Guid>
    {
        public long SupervisorId { get; set; }

        [ForeignKey("SupervisorId")]
        public virtual User Supervisor { get; set; }

        public long InternalAgentId { get; set; }

        [ForeignKey("InternalAgentId")]
        public virtual User InternaleAgent { get; set; }
    }

    public class InternalAgentExternalAgent : Entity<Guid>
    {
        public long InternalAgentId { get; set; }

        [ForeignKey("InternalAgentId")]
        public virtual User InternaleAgent { get; set; }

        public long ExternalAgentId { get; set; }

        [ForeignKey("ExternalAgentId")]
        public virtual User ExternalAgent { get; set; }
    }
}
