﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseActivityCode : FullAuditedEntity<Guid>
    {
        public virtual ActivityCode ActivityCode { get; set; }

        public Guid ActivityCodeId { get; set; }

        public virtual CaseActivity CaseActivity { get; set; }

        public Guid CaseActivityId { get; set; }
    }
}
