﻿using Abp.Domain.Entities.Auditing;
using System;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicFieldAnswer : FullAuditedEntity<Guid>
    {
        public int DemographicFieldTypeId { get; set; }

        public string CaseId { get; set; }

        public Guid QuestionId { get; set; }

        public string AgentId { get; set; }

        public string Answer { get; set; }
    }
}
