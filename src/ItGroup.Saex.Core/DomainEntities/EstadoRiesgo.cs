﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace ItGroup.Saex.DomainEntities
{
    public class EstadoRiesgo : Entity<Guid>
    {
        [MaxLength(100)]
        public string Descripcion { get; set; }
        public int EstadoRiesgoID { get; set; }
        [MaxLength(50)]
        public string Valor { get; set; }
    }
}
