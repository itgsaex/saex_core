﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static ItGroup.Saex.Enumerations.Enumerations;
namespace ItGroup.Saex.DomainEntities
{
    public class CaseAlert : Entity<Guid> 
    {
        [MaxLength(50)]
        public string CaseNumber { get; set; }
        [MaxLength(10)]
        public string AlertID { get; set; }
        public DateTime Date { get; set; }
        [MaxLength(150)]
        public string Comment { get; set; }
        public int Origen { get; set; }
        [MaxLength(10)]
        public string AgentID { get; set; }
    }

    public class Alert : Entity<int>
    {
        [MaxLength(10)]
        public string AlertID { get; set; }
        [MaxLength(50)]
        public string Description { get; set; } 
    }
}
