﻿using Abp.Domain.Entities.Auditing;
using ItGroup.Saex.Authorization.Users;
using ItGroup.Saex.DomainEntities.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItGroup.Saex.DomainEntities
{
    public class Message : FullAuditedEntity<Guid>
    {
        public string Body { get; set; }

        [ForeignKey("RecipientId")]
        public virtual User Recipient { get; set; }

        public long RecipientId { get; set; }

        public virtual ICollection<MessageReply> Replies { get; set; }

        [ForeignKey("SenderId")]
        public virtual User Sender { get; set; }

        public long SenderId { get; set; }

        public string Subject { get; set; }
    }
}