﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class ScorecardParameter : FullAuditedEntity<Guid>, ICloneable
    {
        public string Description  { get; set; }

        public string StandardValue { get; set; }

        public string PaymentLevel { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}