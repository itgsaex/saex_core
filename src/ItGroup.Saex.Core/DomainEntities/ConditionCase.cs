﻿using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
namespace ItGroup.Saex.DomainEntities
{
    public class ConditionCase : AuditedEntity<Guid>
    {
        [MaxLength(10)]
        public string ConditionCaseId { get; set; }
        [MaxLength(50)]
        public string Description { get; set; }
    }
}
