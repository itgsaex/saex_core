﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class RouteProduct : Entity<Guid>
    {
        public virtual Product Product { get; set; }

        public Guid ProductId { get; set; }

        public virtual Route Route { get; set; }

        public Guid RouteId { get; set; }
    }
}