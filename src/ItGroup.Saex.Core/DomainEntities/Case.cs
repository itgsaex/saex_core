﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Castle.Components.DictionaryAdapter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    //TODO: Perform migrations and test Customer....

    public class Case : FullAuditedEntity<Guid>
    {
        public string CaseID { get; set; }

        public string Fullname { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Fulladdress { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }

        public string Phone { get; set; }

        public string Phone2 { get; set; }

        public string Phone3 { get; set; }

        public string DelicacyStages { get; set; }

        public double BalanceAmount { get; set; }

        public string ExtraFields { get; set; }

        public string Type { get; set; }

        public string SocialSecurity { get; set; }

        public Boolean IsActive { get; set; }

        public string Priority { get; set; }

        public string Contact { get; set; }

        public string Email { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double LatitudeRoute { get; set; }

        public double LongitudeRoute { get; set; }

        // Tactic - Begin
        public bool? IsPlanned { get; set; }

        public bool? Monday { get; set; }

        public bool? Tuesday { get; set; }

        public bool? Wednesday { get; set; }

        public bool? Thursday { get; set; }

        public bool? Friday { get; set; }

        public bool? Saturday { get; set; }

        public DateTime? VisitStartTime { get; set; }

        public DateTime? VisitEndTime { get; set; }

        public string VisitPlace { get; set; }

        public int? CollectionDay { get; set; }

        public DateTime? CollectionStartTime { get; set; }

        public DateTime? CollectionEndTime { get; set; }

        public string CollectionPlace { get; set; }
        // Tactic - End

        public bool IsMandatory { get; set; }

        public string RouteHeader { get; set; }

        public string RouteHeaderOrder { get; set; }

        public int ExpirationDays { get; set; }

        public int EndOfMonthDays { get; set; }

        //old fields
        public string AccountNumber { get; set; } //NumeroCuenta

        public string AccountType { get; set; } //TipoCuenta

        public virtual IEnumerable<AgentCase> Agents { get; set; }

        public string BranchId { get; set; } //Sucursal

        public virtual IEnumerable<CaseActivity> CaseActivities { get; set; }

        public virtual IEnumerable<CaseEvent> CaseEvents { get; set; }

        public string CaseNumber { get; set; } //CaseID

        public virtual CaseType CaseType { get; set; }

        public Guid CaseTypeId { get; set; }

        public double Charges { get; set; } //Cargos

        public string CollectorId { get; set; } //Cobrador

        public virtual IEnumerable<CaseComment> Comments { get; set; }

        public string Condition { get; set; } //Condicion

        public string ContactPerson { get; set; } //PersonaContacto

        public virtual IEnumerable<Customer> Customers { get; set; }

        [ForeignKey("DelinquencyStateId")]
        public virtual CaseDelinquencyState DelinquencyState { get; set; } //EtapasDeDelinquencia

        public Guid? DelinquencyStateId { get; set; } //EtapasDeDelinquencia

        public DateTime? DueDate { get; set; } //FechaVencimiento

        public string ExtraFieldsJson { get; set; }

        public DateTime? ImportedDate { get; set; } //FechaEntradaSAEx

        public ImportType ImportType { get; set; }

        public double InterestRate { get; set; } //Interes

        public DateTime? LastPaymentDate { get; set; } //FechaUltPago

        public ListType ListType { get; set; }

        public DateTime? MaturityDate { get; set; } //MaturityDate

        public double OriginalAmount { get; set; } //MontoOriginal

        public DateTime? OriginDate { get; set; } //FechaOrigen

        public double OwedAmount { get; set; } //MontoVencido

        public double PaidAmount { get; set; } //MontoPago

        public virtual IEnumerable<CasePayment> Payments { get; set; }

        //[ForeignKey("PostalCodeId")]
        //public PostalCode PostalCode { get; set; }

        //public Guid PostalCodeId { get; set; }

        public string PostalCodeId { get; set; }

        [ForeignKey("ProductId")]
        public virtual CaseProduct Product { get; set; } //ProductNumber

        public Guid? ProductId { get; set; } //ProductNumber

        public ProductType ProductType { get; set; }

        [ForeignKey("RouteId")]
        public virtual Route Route { get; set; }

        public Guid? RouteId { get; set; }

        public string StateCode { get; set; } //StateCode

        public string Status { get; set; } //Estatus

        [ForeignKey("SystemStateId")]
        public virtual CaseSystemState SystemState { get; set; } //EstatusSistema

        public Guid? SystemStateId { get; set; }

        [ForeignKey("SystemSubStateId")]
        public virtual CaseSystemSubState SystemSubState { get; set; } //SubStatusSistema

        public Guid? SystemSubStateId { get; set; }

        [ForeignKey("TacticId")]
        public virtual CaseTactic Tactic { get; set; }

        public Guid? TacticId { get; set; }

        public string Terms { get; set; } //Terminos
        public virtual IEnumerable<AgentTransfer> CaseTransfers { get; set; }
        public virtual List<InternalAgentComment> InternalAgentComments { get; set; }
        public virtual List<InternalAgentPromise> InternalAgentPromises { get; set; }
    }

    [Table("vwCases")]
    public class vwCases : Entity<Guid>
    {
        public string Username { get; set; }

        public string CaseID { get; set; }

        public string Fullname { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Fulladdress { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }

        public string Phone { get; set; }

        public string Phone2 { get; set; }

        public string Phone3 { get; set; }

        public string DelicacyStages { get; set; }

        public double BalanceAmount { get; set; }

        public string ExtraFields { get; set; }

        public string Type { get; set; }

        public string SocialSecurity { get; set; }

        public Boolean IsActive { get; set; }

        public string Priority { get; set; }

        public string Contact { get; set; }

        public string Email { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double LatitudeRoute { get; set; }

        public double LongitudeRoute { get; set; }

        // Tactic - Begin
        public bool? IsPlanned { get; set; }

        public bool? Monday { get; set; }

        public bool? Tuesday { get; set; }

        public bool? Wednesday { get; set; }

        public bool? Thursday { get; set; }

        public bool? Friday { get; set; }

        public bool? Saturday { get; set; }

        public DateTime? VisitStartTime { get; set; }

        public DateTime? VisitEndTime { get; set; }

        public string VisitPlace { get; set; }

        public int? CollectionDay { get; set; }

        public DateTime? CollectionStartTime { get; set; }

        public DateTime? CollectionEndTime { get; set; }

        public string CollectionPlace { get; set; }
        // Tactic - End

        public bool IsMandatory { get; set; }

        public string RouteHeader { get; set; }

        public string RouteHeaderOrder { get; set; }

        //old fields
        public string AccountNumber { get; set; } //NumeroCuenta

        public string AccountType { get; set; } //TipoCuenta

        public string BranchId { get; set; } //Sucursal

        public string CaseNumber { get; set; } //CaseID

        public Guid CaseTypeId { get; set; }

        public double Charges { get; set; } //Cargos

        public string CollectorId { get; set; } //Cobrador

        public string Condition { get; set; } //Condicion

        public string ContactPerson { get; set; } //PersonaContacto

        public Guid? DelinquencyStateId { get; set; } //EtapasDeDelinquencia

        public DateTime? DueDate { get; set; } //FechaVencimiento

        public string ExtraFieldsJson { get; set; }

        public DateTime? ImportedDate { get; set; } //FechaEntradaSAEx

        public double InterestRate { get; set; } //Interes

        public DateTime? LastPaymentDate { get; set; } //FechaUltPago

        public DateTime? MaturityDate { get; set; } //MaturityDate

        public double OriginalAmount { get; set; } //MontoOriginal

        public DateTime? OriginDate { get; set; } //FechaOrigen

        public double OwedAmount { get; set; } //MontoVencido

        public double PaidAmount { get; set; } //MontoPago

        public string PostalCodeId { get; set; }

        public Guid? ProductId { get; set; } //ProductNumber

        public Guid? RouteId { get; set; }

        public string StateCode { get; set; } //StateCode

        public string Status { get; set; } //Estatus

        public Guid? SystemStateId { get; set; }

        public Guid? SystemSubStateId { get; set; }

        public Guid? TacticId { get; set; }

        public string Terms { get; set; } //Terminos
    }
}
