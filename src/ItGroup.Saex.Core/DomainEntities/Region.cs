﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class Region : FullAuditedEntity<Guid>, ICloneable
    {
        public IEnumerable<AgentRegion> Agents { get; set; }

        public string Code { get; set; }

        public int? RegionId { get; set; }

        public string Name { get; set; }

        public bool ForReport { get; set; }

        public IEnumerable<RegionRoute> Routes { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    //TODO: Create Many to Many relationship in Region & Route
}
