﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using ItGroup.Saex.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentRegion : Entity<Guid>
    {
        public virtual Agent Agent { get; set; }

        public Guid AgentId { get; set; }

        public DateTime CreationDate { get; set; }

        public long CreatorUserId { get; set; }

        public virtual Region Region { get; set; }

        public Guid RegionId { get; set; }
    }

    public class AgentList : Entity<Guid>
    {
        public virtual Agent Agent { get; set; }

        public Guid AgentId { get; set; }

        public DateTime CreationDate { get; set; }

        public long CreatorUserId { get; set; }

        public virtual List List { get; set; }

        public Guid ListId { get; set; }
    }

    public class AgentTransfer : Entity<Guid>
    {
        public virtual Agent FromAgent { get; set; }

        public Guid FromAgentId { get; set; }

        public DateTime CreationDate { get; set; }

        public long CreatorUserId { get; set; }

        public virtual Agent ToAgent { get; set; }

        public Guid ToAgentId { get; set; }

        public virtual Case Case { get; set; }

        public Guid CaseId { get; set; }

        public TransferType TransferType { get; set; }
    }
}