﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class QuestionParameter : FullAuditedEntity<Guid>
    {
        public string Description  { get; set; }

        public string Value { get; set; }

        public bool IsDefaultValue { get; set; }

        public Guid QuestionId { get; set; }
        public virtual Question Question { get; set; }

    }
}