﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;

namespace ItGroup.Saex.DomainEntities
{
    [Table("vwAgents")]
    public class vwAgent : Entity<Guid>
    {
        [MaxLength(256)]
        public string AgentID { get; set; }
        [MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(128)]
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string HourName { get; set; }
        public TimeSpan MondayStartTime { get; set; }
        public TimeSpan MondayEndTime { get; set; }
        public TimeSpan TuesdayStartTime { get; set; }
        public TimeSpan TuesdayEndTime { get; set; }
        public TimeSpan WednesdayStartTime { get; set; }
        public TimeSpan WednesdayEndTime { get; set; }
        public TimeSpan ThursdayStartTime { get; set; }
        public TimeSpan ThursdayEndTime { get; set; }
        public TimeSpan FridayStartTime { get; set; }
        public TimeSpan FridayEndTime { get; set; }
        public TimeSpan SaturdayStartTime { get; set; }
        public TimeSpan SaturdayEndTime { get; set; }
        public TimeSpan SundayStartTime { get; set; }
        public TimeSpan SundayEndTime { get; set; }
    }
}
