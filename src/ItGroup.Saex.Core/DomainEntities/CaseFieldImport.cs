﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseFieldImport : FullAuditedEntity<Guid>, ICloneable
    {
        public Guid? CaseTypeID { get; set; }

        public int CategoriaID { get; set; }

        public string ColumnName { get; set; }

        public bool DownLoadOption { get; set; }

        public int FieldId { get; set; }

        public string HumanName { get; set; }

        public int Sequence { get; set; }

        public string SourceTargetType { get; set; }

        public string TargetColumn { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class CaseFieldHelper
    {
        public bool Selected { get; set; }

        public int FieldId { get; set; }

        public string ColumnName { get; set; }

        public string HumanName { get; set; }

        public bool MasterList { get; set; }

        public bool ChildList { get; set; }

        public bool RiskList { get; set; }

        public int CategoriaId { get; set; }

        public int Sequence { get; set; }

        public string CaseTypeDescription { get; set; }


        public string TargetColumn { get; set; }

        public string SourceTargetType { get; set; }
    }

    public class TestQueryHelper
    {
        public string ProductNumber { get; set; }
        public string AccountNumber { get; set; }

        public string CustomerName { get; set; }

        public string ZipCode { get; set; }

    }
    public interface ICaseFieldsImportRepository : IRepository<CaseFieldImport, Guid>
    {
        Task<List<CaseFieldHelper>> ReadCaseFields(Guid? product);
    }
}
