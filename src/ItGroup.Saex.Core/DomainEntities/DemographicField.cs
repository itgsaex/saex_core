﻿using Abp.Domain.Entities.Auditing;
using System;

namespace ItGroup.Saex.DomainEntities
{
    public class DemographicField : FullAuditedEntity<Guid>, ICloneable
    {
        public Guid DemographicFieldTypeId { get; set; }

        public string Name { get; set; }

        public string Label { get; set; }

        public bool IsRequired { get; set; }

        public string Properties { get; set; }

        public int SortOrder { get; set; }

        public bool IsActive { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
