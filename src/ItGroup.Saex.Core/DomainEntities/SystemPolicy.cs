﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemPolicy : FullAuditedEntity<Guid>
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }

    }
}