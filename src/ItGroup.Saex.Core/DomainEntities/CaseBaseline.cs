﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using ItGroup.Saex.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseBaseline : Entity<Guid>, ICloneable
    {
		public DateTime UploadDate { get; set; }
		public string AccountNumber { get; set; }
		public string AccountNumber2 { get; set; }
		public string Name { get; set; }
		public int DueDays { get; set; }
		public string Progress { get; set; }
		public DateTime DueDate { get; set; }
		public DateTime Nest { get; set; }
		public decimal DueBalance { get; set; }
		public decimal OriginalAmount { get; set; }
		public decimal LC { get; set; }
		public string City { get; set; }
		public string Product { get; set; }
		public string Employee { get; set; }
		public string Supervisor { get; set; }
		public string Comments { get; set; }
		public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
