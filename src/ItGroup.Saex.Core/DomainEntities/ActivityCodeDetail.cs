﻿using Abp.Domain.Entities.Auditing;
using System;

namespace ItGroup.Saex.DomainEntities
{
    public class ActivityCodeDetail : FullAuditedEntity<Guid>
    {
        public ActivityCode ActivityCode { get; set; }

        public string Question { get; set; }

        public int Type { get; set; }

        public string Parameters { get; set; }

        public string DefaultValue { get; set; }

        public bool? Required { get; set; }

        public int? Order { get; set; }

        public int ActivityCodeType { get; set; }
    }
}