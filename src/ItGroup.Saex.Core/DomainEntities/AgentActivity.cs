﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentActivity : AuditedEntity<Guid>
    {
        public virtual Agent Agent { get; set; }

        public Guid AgentId { get; set; }

        public virtual CaseActivity CaseActivity { get; set; }

        public Guid? CaseActivityId { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }

        public int TransStatus { get; set; }
    }
}
