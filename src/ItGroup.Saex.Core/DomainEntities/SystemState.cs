﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class SystemState : FullAuditedEntity<Guid>, ICloneable
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public bool IsPromise { get; set; }

        public virtual MacroState MacroState { get; set; }

        public Guid MacroStateId { get; set; }

        public virtual SystemSubState SubState { get; set; }

        public Guid? SubStateId { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
