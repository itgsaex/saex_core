﻿using Abp.Domain.Entities.Auditing;
using System;

namespace ItGroup.Saex.DomainEntities
{
    public class AgentGPS : AuditedEntity<Guid>
    {
        public string AgentId { get; set; }

        public virtual DateTime Date { get; set; }

        public int Type { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }
    }
}
