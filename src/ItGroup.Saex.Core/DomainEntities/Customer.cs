﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class Customer : FullAuditedEntity<Guid>
    {
        public virtual IEnumerable<Address> Addresses { get; set; }

        public DateTime? BirthDate { get; set; }

        public virtual Case Case { get; set; }

        public Guid CaseId { get; set; }

        public string CustomerId { get; set; }

        public string EmailAddress { get; set; }

        public string FullName { get; set; }

        public string IdentificationNumber { get; set; }

        public bool IsPrincipal { get; set; }

        public string PhoneNumber { get; set; }

        public string Ssn { get; set; }
    }
}
