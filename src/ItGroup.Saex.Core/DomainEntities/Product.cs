﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class Product : FullAuditedEntity<Guid>, ICloneable
    {
        public string Code { get; set; }

        public virtual IEnumerable<Condition> Conditions { get; set; }

        public string Description { get; set; }

        public string ProductNumber { get; set; }

        public virtual IEnumerable<RouteProduct> Routes { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
