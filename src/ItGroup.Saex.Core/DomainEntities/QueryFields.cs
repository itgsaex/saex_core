﻿using Abp.Domain.Entities;
using System;

namespace ItGroup.Saex.DomainEntities
{
    public class QueryFields : Entity<Guid>
    {
        public int FieldId { get; set; }

        public bool MasterList { get; set; }

        public bool ChildList { get; set; }

        public bool RiskList { get; set; }
    }
}