﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseProduct : FullAuditedEntity<Guid>
    {
        public Case Case { get; set; }

        public Guid CaseId { get; set; }

        public Product Product { get; set; }

        public Guid? ProductId { get; set; }

        public string Value { get; set; }
    }
}
