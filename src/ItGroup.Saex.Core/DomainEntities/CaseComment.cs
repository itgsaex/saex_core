﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseComment : FullAuditedEntity<Guid>, ICloneable
    {
        public virtual Agent Agent { get; set; }

        public Guid AgentId { get; set; }

        public virtual Case Case { get; set; }

        public Guid CaseId { get; set; }

        public string Comment { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
