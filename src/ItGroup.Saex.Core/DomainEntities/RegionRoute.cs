﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class RegionRoute : Entity<Guid>
    {
        public Region Region { get; set; }

        public Guid RegionId { get; set; }

        public Route Route { get; set; }

        public Guid RouteId { get; set; }
    }
}