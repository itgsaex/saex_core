﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class PaymentDelayReason : FullAuditedEntity<Guid>, ICloneable
    {
        public PaymentDelayReason()
        {
        }

        public PaymentDelayReason(Guid id, string code, string description)
        {
            Id = id;
            Code = code;
            Description = description;
        }

        public string Code { get; set; }

        public string Description { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}