﻿using Abp.Domain.Entities.Auditing;
using System;

namespace ItGroup.Saex.DomainEntities
{
    public class SessionLog : AuditedEntity<Guid>
    {
        public Guid AgentId { get; set; }

        public virtual DateTime Date { get; set; }

        public int MileageIn { get; set; }

        public int MileageOut { get; set; }

        public DateTime TimeIn { get; set; }

        public DateTime TimeOut { get; set; }

        public int Status { get; set; }
    }
}
