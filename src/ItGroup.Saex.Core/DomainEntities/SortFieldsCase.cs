﻿using System;
using Abp.Domain.Entities;

namespace ItGroup.Saex.DomainEntities
{
    public class SortFieldsCase : Entity<Guid>
    {
        public string FieldName { get; set; }

        public string FieldLabel { get; set; }

        public string Use { get; set; }

        public string FilterType { get; set; }
    }
}
