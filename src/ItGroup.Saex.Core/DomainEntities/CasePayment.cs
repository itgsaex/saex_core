﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class CasePayment : FullAuditedEntity<Guid>, ICloneable
    {
        public double PaymentAmount  { get; set; }

        public DateTime PaymentDate { get; set; }

        public Guid CaseId { get; set; }
        public virtual Case Case { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}