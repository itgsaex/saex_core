﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseSystemSubState : FullAuditedEntity<Guid>
    {
        public Case Case { get; set; }

        public Guid CaseId { get; set; }

        public SystemSubState SystemSubState { get; set; }

        public Guid? SystemSubStateId { get; set; }

        public string Value { get; set; }
    }
}
