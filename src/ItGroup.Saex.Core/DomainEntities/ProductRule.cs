﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ProductRule : FullAuditedEntity<Guid>, ICloneable
    {
        public RuleType Type { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}