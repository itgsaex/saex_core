﻿using Abp.Domain.Entities.Auditing;
using System;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseEvent : FullAuditedEntity<Guid>, ICloneable
    {
        public CaseEventType Type { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public string Description { get; set; }

        public Guid CaseId { get; set; }
        public virtual Case Case { get; set; }

        public Guid AgentId { get; set; }
        public virtual Agent Agent { get; set; }
        public string Location { get; set; }
        public bool RemindMe { get; set; }
        public Guid ActivityCodeId { get; set; }
        public virtual ActivityCode ActivityCode { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}