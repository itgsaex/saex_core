﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItGroup.Saex.DomainEntities.ProductSystem
{
	public class ProductSystem : Entity<Guid>
	{
		public string SystemName { get; set; }
		public string TableName { get; set; }
	}
}
