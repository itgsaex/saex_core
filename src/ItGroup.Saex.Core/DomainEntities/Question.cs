﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class Question : FullAuditedEntity<Guid>, ICloneable
    {
        public string Description { get; set; }

        public QuestionType Type  { get; set; }

        public bool IsRequired { get; set; }

        public int Position { get; set; }

        public IEnumerable<QuestionParameter> Parameters { get; set; }

        public Guid ActivityCodeId { get; set; }
        public virtual ActivityCode ActivityCode { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}