﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class CaseDelinquencyState : FullAuditedEntity<Guid>
    {
        public Case Case { get; set; }

        public Guid CaseId { get; set; }

        public DelinquencyState DelinquencyState { get; set; }

        public Guid? DelinquencyStateId { get; set; }

        public bool IsSuccesfulCriteria { get; set; }

        public string Value { get; set; }
    }
}
