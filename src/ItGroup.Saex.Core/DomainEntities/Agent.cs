﻿using Abp.Domain.Entities.Auditing;
using ItGroup.Saex.Authorization.Users;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class Agent : FullAuditedEntity<Guid>, ICloneable
    {
        public virtual IEnumerable<CaseActivity> CaseActivities { get; set; }

        public virtual IEnumerable<AgentCase> Cases { get; set; }

        public string CollectorId { get; set; }

        public string JobPosition { get; set; }

        public virtual IEnumerable<AgentRegion> Regions { get; set; }

        public virtual IEnumerable<AgentList> Lists { get; set; }

        public virtual IEnumerable<AgentTransfer> FromTransfers { get; set; }
        public virtual IEnumerable<AgentTransfer> ToTransfers { get; set; }

        public virtual IEnumerable<AgentRoute> Routes { get; set; }

        [ForeignKey("SupervisorId")]
        public virtual Agent Supervisor { get; set; }

        public Guid? SupervisorId { get; set; }

        public SupervisorType? SupervisorType { get; set; }

        public AgentType? Type { get; set; }

        [ForeignKey("UserId"), JsonIgnore]
        public virtual User User { get; set; }
        
        public long UserId { get; set; }

        [ForeignKey("WorkingHourId")]
        public virtual WorkingHour WorkingHour { get; set; }

        public Guid? WorkingHourId { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
