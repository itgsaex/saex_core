﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class DelinquencyState : FullAuditedEntity<Guid>, ICloneable
    {
        public string Condition { get; set; }

        public string Precedence  { get; set; }
        
        public string Rule  { get; set; }
        
        public string SuccessfulRuleValue  { get; set; }

        public string UnsuccessfulRuleValue  { get; set; }
        
        public bool IsRequired  { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}