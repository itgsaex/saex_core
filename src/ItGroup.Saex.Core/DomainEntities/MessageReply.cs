﻿using Abp.Domain.Entities.Auditing;
using ItGroup.Saex.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ItGroup.Saex.DomainEntities.Messages
{
    public class MessageReply : FullAuditedEntity<Guid>
    {
        public string Body { get; set; }

        [ForeignKey("ParentMessageId")]
        public virtual Message ParentMessage { get; set; }

        public Guid ParentMessageId { get; set; }

        [ForeignKey("SenderId")]
        public virtual User Sender { get; set; }

        public long SenderId { get; set; }
    }
}