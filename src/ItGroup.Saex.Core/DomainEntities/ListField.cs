﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ListField : FullAuditedEntity<Guid>, ICloneable
    {
        public string FieldName { get; set; }

        public string FieldId  { get; set; }

        public int Position { get; set; }

        public Guid ProductId { get; set; }
        public virtual Product Product { get; set; }

        public Guid ListId { get; set; }
        public virtual List List { get; set; }

        public Guid ListFieldCategoryId { get; set; }
        public virtual ListFieldCategory ListFieldCategory { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}