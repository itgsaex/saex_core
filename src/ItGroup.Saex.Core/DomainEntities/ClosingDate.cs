﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using static ItGroup.Saex.Enumerations.Enumerations;

namespace ItGroup.Saex.DomainEntities
{
    public class ClosingDate : FullAuditedEntity<Guid>, ICloneable
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate  { get; set; }

        public bool IsAllDay { get; set; }

        public ClosingDateType Type { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}