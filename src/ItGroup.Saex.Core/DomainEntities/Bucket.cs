﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace ItGroup.Saex.DomainEntities
{
    public class Bucket : FullAuditedEntity<Guid>, ICloneable
    {
        public int MinValue { get; set; }

        public int MaxValue { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}